import { provideRouter, RouterConfig } from '@angular/router';

import { AboutRoutes } from './+about/index';
import { DocumentRoutes } from './+document/index';
import { ExperimentRoutes } from './+experiment/index';
import { HomeRoutes } from './+home/index';
import { UserRoutes } from './+user/index';
import { ContactRoutes } from './+contact/index';
import { DemoRoutes } from './+demo/index';

const routes: RouterConfig = [
  ...HomeRoutes,
  ...UserRoutes,
  ...DocumentRoutes,
  ...ExperimentRoutes,
  ...AboutRoutes,
  ...ContactRoutes,
  ...DemoRoutes
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
];
