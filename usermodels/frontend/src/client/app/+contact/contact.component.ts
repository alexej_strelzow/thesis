import { Component, Directive, HostBinding, HostListener, ElementRef } from '@angular/core';
import { FORM_DIRECTIVES, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { NgModel } from '@angular/common';

/**
 * This class represents the lazy loaded ContactComponent.
 */

@Directive({selector: '[ngModel]'})
class NgModelStatus {
  constructor(private control: ElementRef) {
    // noop
  }
  //@HostBinding('class') classes = 'class1 class2 class3';

  @HostBinding('class.valid') valid: boolean = true;
  @HostBinding('class.invalid') invalid: boolean = false;

  /*@HostListener('input', ['$event.target.value'])
    onChange(updatedValue: string) {
      //this.valid = updatedValue.length > 0;
      //this.invalid = !this.valid;
  }*/

  @HostListener('blur', ['$event'])
    onBlur() {
      this.valid = this.control.nativeElement.validity.valid;
      this.invalid = !this.valid;
  }
}

@Component({
  moduleId: module.id,
  selector: 'sd-contact',
  templateUrl: 'contact.component.html',
  providers: [FormBuilder],
  directives: [FORM_DIRECTIVES, NgModelStatus],
  styleUrls: ['contact.component.css']
})
export class ContactComponent {

  // TODO: angular forms

  private _name: FormControl;
  private _email: FormControl;
  private _comment: FormControl;
  private _form: FormGroup;

  constructor(/*private builder: FormBuilder*/) {
    this._name = new FormControl('', Validators.required);
    this._email = new FormControl('', Validators.required);
    this._comment = new FormControl('', Validators.minLength(8));

    /*
    this._form = builder.group({
      name: this._name,
      email: this._email,
      comment: this._comment
    });
    */
  }

  isFormValid(): boolean {
    return this._name.valid && this._email.valid && this._comment.valid;
  };

  send($event: any) {
    if (this.isFormValid()) {
      var link = "mailto:alexej.strelzow@gmail.com"
          + "?cc=myCCaddress@example.com"
          + "&subject=" + "App-Feedback"
          + "&body=" + document.getElementById('comment').innerHTML
        ;

      window.location.href = link;
    }
  };

}
