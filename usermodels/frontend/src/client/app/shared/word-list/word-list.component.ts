import { Component, Input, Output, EventEmitter } from '@angular/core';
import { WordComponent, WordModel } from './entry/index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'word-list',
  directives: [WordComponent],
  templateUrl: 'word-list.component.html',
  styleUrls: ['word-list.component.css']
})
export class WordListComponent {

  @Input() words: Array<WordModel> = undefined;
  @Input() clickable: boolean = true;
  @Output() onClick: EventEmitter<WordModel> = new EventEmitter<WordModel>();

  clicked($event: any): void {
    this.onClick.emit($event);
  }

}
