/**
 * Created by Besitzer on 14.08.2016.
 */
export class WordModel {
  word: string;
  lemma: string;
  pos: string;
  rank: number;
  occurrences: number;
  pages: Array<number>;
}
