/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './word.component';
export * from './word.model';
