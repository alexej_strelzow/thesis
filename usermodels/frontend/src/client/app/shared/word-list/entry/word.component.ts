import { Component, Input, Output, EventEmitter } from '@angular/core';
import { WordModel } from './index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'word',
  templateUrl: 'word.component.html',
  styleUrls: ['word.component.css']
})
export class WordComponent {

  @Input() word: WordModel;
  @Input() clickable: boolean = true;
  @Output() onClick: EventEmitter<WordModel> = new EventEmitter<WordModel>();

  more: boolean = false;

  clicked($event: any): void {
    if (this.clickable) {
      this.onClick.emit(this.word);
    }
  }

}
