/**
 * Created by Besitzer on 15.08.2016.
 */
import { Pipe, PipeTransform  } from '@angular/core';

@Pipe({
  name: 'thousand'
})
export class ThousandSeparatorPipe implements PipeTransform {
  transform(value: string, args: string[]): any {
    if (!value) return value;

    return parseInt(value, 10).toLocaleString('en-GB');
  }
}
