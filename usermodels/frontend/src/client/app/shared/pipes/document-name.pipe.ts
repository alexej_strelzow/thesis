/**
 * Created by Besitzer on 15.08.2016.
 */
import { Pipe, PipeTransform  } from '@angular/core';

@Pipe({
  name: 'document'
})
export class DocumentNamePipe implements PipeTransform {
  transform(value: string, args: string[]): any {
    if (!value) return value;

    return value.replace(/\_/g, ' ');
  }
}
