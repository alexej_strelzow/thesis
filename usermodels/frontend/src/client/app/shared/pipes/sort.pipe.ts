/**
 * Created by Besitzer on 15.08.2016.
 */
import { Pipe, PipeTransform  } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  transform(array: Array<any>, args: boolean): Array<string> {
    if (!array) {
      return array
    }
    array.sort((a: any, b: any) => {
      if (a < b) {
        return -1;
      } else if (a > b) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
