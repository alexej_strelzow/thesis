/**
 * This barrel file provides the exports for the shared resources (services, components).
 */
export * from './navbar/index';
export * from './toolbar/index';
export * from './config/env.config';
export * from './word-list/index';
export * from './explanation-list/index';
export * from './pipes/index';
