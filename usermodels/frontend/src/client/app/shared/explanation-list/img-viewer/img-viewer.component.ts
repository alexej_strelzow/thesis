import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ImageModel } from './../entry/index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'img-viewer',
  templateUrl: 'img-viewer.component.html',
  styleUrls: ['img-viewer.component.css']
})
export class ImgViewerComponent {

  @Input() set images(images: Array<ImageModel>) {
    this._images = images;
    this._currentIndex = 0;
    this.setCurrentImage();
    this._size = images.length;
  }

  @Output() onNextClick: EventEmitter<number> = new EventEmitter<number>();
  @Output() onPreviousClick: EventEmitter<number> = new EventEmitter<number>();

  _images: Array<ImageModel>;
  _currentImage: ImageModel;
  _currentIndex: number;
  _size: number;

  setCurrentImage(): void {
    this._currentImage = this._images[this._currentIndex];
  }

  isNextEnabled(): boolean {
    return this._currentIndex < this._size-1;
  }

  isPreviousEnabled(): boolean {
    return this._currentIndex > 0;
  }

  next($event: any): void {
    if (this.isNextEnabled()) {
      this._currentIndex++;
      this.setCurrentImage();
    }
  }

  previous($event: any): void {
    if (this.isPreviousEnabled()) {
      this._currentIndex--;
      this.setCurrentImage();
    }
  }

}
