import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ExplanationComponent, ExplanationModel } from './entry/index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'explanation-list',
  directives: [ExplanationComponent],
  templateUrl: 'explanation-list.component.html',
  styleUrls: ['explanation-list.component.css']
})
export class ExplanationListComponent {

  @Input() explanations: Array<ExplanationModel> = undefined;
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  clicked($event: any): void {
    this.onClick.emit($event);
  }

}
