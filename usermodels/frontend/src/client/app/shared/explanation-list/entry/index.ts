/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './explanation.component';
export * from './explanation.model';
