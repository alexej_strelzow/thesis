import { Component, Input, Output, EventEmitter, AfterViewInit  } from '@angular/core';
import { ExplanationModel } from './index';
import { SafeResourceUrl, DomSanitizationService } from '@angular/platform-browser';
import { ImgViewerComponent } from "../img-viewer/index";

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'explanation',
  directives: [ImgViewerComponent],
  templateUrl: 'explanation.component.html',
  styleUrls: ['explanation.component.css']
})
export class ExplanationComponent implements AfterViewInit {

  @Input() explanation: ExplanationModel = undefined;
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  more: boolean = true;
  moreGlosses: boolean = true; //true
  moreImages: boolean = false;
  moreTranslations: boolean = false;

  //url: SafeResourceUrl;
  constructor(private sanitizer: DomSanitizationService) {
    // noop
  }

  ngAfterViewInit() {
    //"http://babelnet.org/synset?word={{explanation.babelNetId}}&details=1&lang=EN
    //this.url = this.sanitizer.bypassSecurityTrustResourceUrl('http://babelnet.org/synset?word='+this.explanation.babelNetId+'&details=1&lang=EN');
    //console.log(this.url);
  }

  clicked($event: any): void {
    //this.onClick.emit(this.element.id);
    console.log(this.explanation);
  }

}
