/**
 * Created by Besitzer on 14.08.2016.
 */
export class ExplanationModel {
  word: string;
  lemma: string;
  pos: string;
  rank: number;
  occurrences: number;
  babelNetId: string;
  glosses: Array<GlossaryModel>;
  images: Array<ImageModel>;
  translations: Array<TranslationModel>;
}

export class GlossaryModel {
  gloss: string;
  language: string;
}

export class ImageModel {
  url: string;
  name: string;
  valid: boolean;
}

export class TranslationModel {
  language: string;
  translations: Array<String>;
}
