/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './explanation-list.component';
export * from './entry/index';
