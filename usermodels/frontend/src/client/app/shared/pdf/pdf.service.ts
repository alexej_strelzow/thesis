/**
 * Created by A2051 on 08.08.2016.
 */
/**
 * Created by A2051 on 05.08.2016.
 */

import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { mergeMap } from "rxjs/operator/mergeMap";
import {Base64} from "./base64";

@Injectable()
export class PdfService {

    private _blobSignatureToMimeTypeMapping: {[key: string]: string} = {};
    private _fileExtensionToMimeTypeMapping: {[key: string]: string} = {};
    private base64: Base64;

    constructor(private _http: Http) {
        this.initSignatureMap();
        this.initExtensionMap();
        this.base64 = new Base64();
    }

    private initExtensionMap(): void {
        this._fileExtensionToMimeTypeMapping['pdf'] = 'application/pdf';

        this._fileExtensionToMimeTypeMapping['jpg'] = 'image/jpeg';
        this._fileExtensionToMimeTypeMapping['jpeg'] = 'image/jpeg';
        this._fileExtensionToMimeTypeMapping['png'] = 'image/png';
        this._fileExtensionToMimeTypeMapping['bmp'] = 'image/bmp';
        this._fileExtensionToMimeTypeMapping['hcp'] = 'image/bmp';
        this._fileExtensionToMimeTypeMapping['gif'] = 'image/gif';
        this._fileExtensionToMimeTypeMapping['svg'] = 'image/svg+xml';

        this._fileExtensionToMimeTypeMapping['txt'] = 'text/plain';
        this._fileExtensionToMimeTypeMapping['xml'] = 'text/xml';
        this._fileExtensionToMimeTypeMapping['rtf'] = 'text/rtf';

        this._fileExtensionToMimeTypeMapping['doc'] = 'application/msword';
        this._fileExtensionToMimeTypeMapping['docx'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        this._fileExtensionToMimeTypeMapping['xls'] = 'application/vnd.ms-excel';
        this._fileExtensionToMimeTypeMapping['xlsx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }

    private initSignatureMap(): void {
        // http://filesignatures.net/index.php?page=all&order=SIGNATURE&sort=DESC&alpha=
        // https://en.wikipedia.org/wiki/List_of_file_signatures

        this._blobSignatureToMimeTypeMapping['25504446'] = 'application/pdf';

        this._blobSignatureToMimeTypeMapping['FFD8FFDB'] = 'image/jpeg';
        this._blobSignatureToMimeTypeMapping['FFD8FFE0'] = 'image/jpeg';
        this._blobSignatureToMimeTypeMapping['FFD8FFE1'] = 'image/jpeg';
        this._blobSignatureToMimeTypeMapping['FFD8FFE2'] = 'image/jpeg';

        this._blobSignatureToMimeTypeMapping['89504E47'] = 'image/png';

        this._blobSignatureToMimeTypeMapping['47494638'] = 'image/gif';

        this._blobSignatureToMimeTypeMapping['424D'] = 'image/bmp';
    }

    uploadPdf(file: File): Observable<any> {
      return new Observable<string>((observer: any) => {
        let arrayBuffer:Uint8Array;
        let fileReader = new FileReader();
        fileReader.onload = (event: ProgressEvent) => {
          arrayBuffer = new Uint8Array((<any>event.currentTarget).result);
          let base64String:string = new Base64().encodeBase64(arrayBuffer);

          observer.next(this._http.post('http://localhost:8080/api/files/upload/file', { name: file.name, content: base64String }));
        };
        fileReader.readAsArrayBuffer(file);
      });
    }

    uploadMultipartPdf(file: File): Observable<any> {
      let headers:Headers = new Headers();
      headers.set('Content-Type', 'multipart/form-data');// boundary=----WebKitFormBoundary3jY1WW8tWsuxKGP1
      headers.set('Accept', 'application/json');
      headers.set('Content-Length', '' + file.size);

      return this._http.post('http://localhost:8080/api/files/upload/file', file, headers);
    }

    getFileByTitle(title: string): Observable<any> {
      return this._http.get('http://localhost:8080/api/files/download/title/' + title)
        .map((response: any) => this.base64.decodeBase64String(response._body) );
    }

    getFileById(id: number): Observable<any> {
      return this._http.get('http://localhost:8080/api/files/download/id/' + id)
        .map((response: any) => this.base64.decodeBase64String(response._body) );
    }

    doIt(request: string): Observable<any> {
        // service = persons
        return this._http.get(request)
            .map((response: any) => {
                return new Observable((observer: any) => {
                    let base64DecodedData = this.base64.decodeBase64String(response._body);
                    //let fileName: string = response.fileName;
                    //let fileType: string = null;
                    //let fileExtension: string = null;

                    this.getTypeFromBlob(new Blob([base64DecodedData])).subscribe((type: string) => {
                        // TODO: maybe use https://github.com/rsdoiel/mimetype-js
                        //fileExtension = fileName.indexOf('.') !== -1 ? fileName.substring(fileName.lastIndexOf('.')+1).toLocaleLowerCase() : null;
                        //fileType = type ? type : this._fileExtensionToMimeTypeMapping[fileExtension];

                        //observer.next({data: base64DecodedData, fileName: fileName, fileExtension: fileExtension, fileType: fileType});
                        observer.next({data: base64DecodedData, fileType: 'application/pdf'});
                        observer.complete();
                    });
                });
            })
    }

    getTypeFromBlob(blob: Blob): Observable<string> {
        return new Observable<string>((observer: any) => {
            if (blob.type) {
                observer.next(blob.type);
                observer.complete();

            } else {
                // inspired by: http://stackoverflow.com/questions/18299806/how-to-check-file-mime-type-with-javascript-before-upload
                var fileReader = new FileReader();
                fileReader.onloadend = (e:any) => {
                    var arr:Uint8Array = (new Uint8Array(e.currentTarget.result)).subarray(0, 4);
                    var header:string = "";
                    for (var i = 0; i < arr.length; i++) {
                        header += arr[i].toString(16);
                    }
                    // Check the file signature against known types
                    if (this._blobSignatureToMimeTypeMapping[header]) {
                        observer.next(this._blobSignatureToMimeTypeMapping[header]);
                    } else {
                        observer.next(null);
                    }
                    observer.complete();
                };
                fileReader.readAsArrayBuffer(blob);
            }
        });
    }
}

/*
 getDocumentAsMultipart(title: string): Observable<any> {
 return this._http.get('http://localhost:8080/api/upload/' + title);
 }

 getPdf(title: string): Observable<any> {
 return this._http.get('http://localhost:8080/api/upload/getpdf/' + title);
 }

 getDocumentAsByteArray(title: string): Observable<any> {
 return this._http.get('http://localhost:8080/api/upload/bytes/' + title);
 }

 getPdf(id: string): Observable<any> {
 return this._http.get('http://localhost:8080/api/document/pdf/' + id)
 .map((response: any) => response.json())
 .map((res: any) => {
 return new Observable((observer: any) => {

 });
 });
 }
 */
