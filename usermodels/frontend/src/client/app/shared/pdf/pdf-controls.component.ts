import { Component, EventEmitter, Output } from "@angular/core";
import { CORE_DIRECTIVES } from '@angular/common';

@Component({
    selector: 'pdf-controls',
    directives: [CORE_DIRECTIVES],
    template: `<div id="sidebarContainer" class="pdf-toolbar">
		<div class="splitToolbarButton toggled">
      <i class="maximize icon big cursor-hand" aria-hidden="true" title="Adjust" tabindex="1" (click)="fill()"></i>

			<i class="repeat icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Rotate clockwise" tabindex="2" (click)="rotate(true)"></i>
			
			<i class="undo icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Rotate counter-clockwise" tabindex="3" (click)="rotate(false)"></i>
			
			<span *ngIf="_pageCount > 1">
        <i class="caret left icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Previous page" tabindex="4" (click)="page(-1)"></i>
        
        <span style="font-size: 20px">
            <input id="pageInput" class="pdf-toolbar-page-input" [style.background-color]="_pageInputValid ? 'white' : 'red'" tabindex="5"
            [(ngModel)]="_pageInput" (blur)="pageCustom(_pageInput)"> / <label id="lastPage">{{_pageCount}}</label>
        </span> 
                
			    <i class="caret right icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Next page" tabindex="6" (click)="page(+1)"></i>
			</span>
			
      <i class="plus icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Zoom" tabindex="7" (click)="zoom(true)"></i>

			<i class="minus icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Zoom" tabindex="8" (click)="zoom(false)"></i>

			<i class="save icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Save" tabindex="9" (click)="save()"></i>
			
      <i class="remove icon big cursor-hand pdf-toolbar-element" aria-hidden="true" title="Close" tabindex="10" (click)="close()"></i>
			
			<!--i class="fa fa-print fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Seite drucken" tabindex="8" (click)="print()"></i-->
			
			<!--input id="searchText" class="pdf-toolbar-search-input" tabindex="9" [(ngModel)]="searchText"> 
			<i class="fa fa-search fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Suchen" tabindex="10" (click)="search(searchText)"></i-->
			
		</div>
    </div>`
  //styleUrls: ['pdf.component.css']
})
/**
 * Provides controls like:
 *  -) Adjust PDF to screen
 *  -) Rotate (counter)clockwise
 *  -) Previous/next page
 *  -) Input page to go to
 *  -) Zoom in/out
 *  -) Save PDF
 *  -) (Print current page)
 */
export class PdfControls {

    _pageCount: number = 0;
    _pageInput: number;
    _pageInputValid: boolean = true;
    _searchText: string;

	constructor(){}

    @Output() onFill: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onRotationChanged: EventEmitter<number> = new EventEmitter<number>();
    @Output() onPageChanged: EventEmitter<number> = new EventEmitter<number>();
    @Output() onPageInputChanged: EventEmitter<number> = new EventEmitter<number>();
    @Output() onZoom: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onClose: EventEmitter<boolean> = new EventEmitter<boolean>();
    //@Output() onSearch: EventEmitter<string> = new EventEmitter<string>();

    public setCurrentPage(currentPage: number) {
        this._pageInput = currentPage ? currentPage : 1;
    }

    public setPageCount(pageCount: number) {
        if (pageCount) {
            this._pageCount = pageCount;
        }
    }

    fill() {
        this.onFill.emit(true);
    }

    rotate(right: boolean) {
        if (right) {
            this.onRotationChanged.emit(+1);
        } else {
            this.onRotationChanged.emit(-1);
        }
    }

    page(page: number) {
        this.onPageChanged.emit(page);
    }

    pageCustom(p: string) {
        let page: number = parseInt(p, 10);
        this._pageInputValid = page > 0 && page <= this._pageCount;

        if (this._pageInputValid) {
            this.onPageInputChanged.emit(page);
        }
    }

    save() {
        this.onSave.emit(true);
    }

    close() {
        this.onClose.emit(true);
    }

    zoom(magnify: boolean) {
        if (magnify) {
            this.onZoom.emit(+1);
        } else {
            this.onZoom.emit(-1);
        }
    }

    /*
    print() {
        // TODO: Folgeanforderung
        window.print();
    }
    */

    /*
    search(searchText: string) {
        if (searchText) {
            this.onSearch.emit(searchText);
        }
    }
    */

}
