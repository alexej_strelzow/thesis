/**
 * Created by A2051 on 16.06.2016.
 */

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';

@Injectable()
/**
 * Logic behind the pdf.component. It works with the model of the pdf (PDFDocumentProxy)
 * and executes the actions of pdf-controls.component.
 */
export class PdfPageService  {
    // up-to-date values
    private pageNum: number;
    private pdfDoc: PDFDocumentProxy;
    private currentScale: number;
    private defaultFill: boolean = true; // maybe expose as param?
    private canRender: boolean = true; // rendering blocked in loading state when we enable 'defaultFill', because we need re-rendering with a new scale

    private baseZoom: number; // set when we adjust the content (see fill())

    // scaling
    private defaultScale: number = 1.5;
    private scaleSteps: number = 100; // or 1%
    private viewportScaleOfOne: { width: number, height: number };
    private scaleUnit: { width: number, height: number } = { width: 0, height: 0 };

    // rotating
    private rotationMap: { [pageNum: number]: number; } = <any>{};

    // TEMP
    private tmpPageViewport: PDFPageViewport;

    constructor() {
        this.setScale(this.defaultScale);
    }

    public load(data: Uint8Array): Observable<number> {
        return Observable.create((observer: any) => {
            PDFJS.getDocument(data).then((pdf) => {
                this.pdfDoc = pdf;
                this.pageNum = 1;
                this.viewportScaleOfOne = undefined;
                this.renderPage(this.pageNum);
                observer.next(this.pdfDoc.numPages);
                observer.complete();
            }, function (error) {
                console.log(error);
            });
        });
    }

    public loadUrl(url: string): Observable<number> {
      let src: PDFSource = {url: url};
      // httpHeaders

      return Observable.create((observer: any) => {
        PDFJS.getDocument(src).then((pdf) => {
          this.pdfDoc = pdf;
          this.pageNum = 1;
          this.viewportScaleOfOne = undefined;
          this.renderPage(this.pageNum);
          observer.next(this.pdfDoc.numPages);
          observer.complete();
        }, function (error) {
          console.log(error);
        });
      });
    }

    pdfViewerPluginNames: Array<string> = ['Chrome PDF Viewer', 'Adobe Acrobat'];

    public hasPlugin(type: string): boolean {
        /*
        let name: string = '';
        switch (type) {
            case 'PDF':
                if (is.chrome()) {
                    name = 'Chrome PDF Viewer';
                } else if (is.firefox()) {
                    name = 'Adobe Acrobat';
                }
                break;
            // more
        }
        */
        if (type) { // name
            var plugins: MSPluginsCollection = window.navigator.plugins;
            for (var i = 0; i < plugins.length; i++) {
                if (this.pdfViewerPluginNames.indexOf((<any>plugins)[i].name) > -1) {
                    return true;
                }
            }
        }
        return false;
    }

    public openInBrowser(data: Uint8Array): void {
        var blob = new Blob([data], {type: 'application/pdf'});
        var objectUrl = URL.createObjectURL(blob, { oneTimeOnly: true });
        window.open(objectUrl);
    }

    public rotate(degrees: number) {
        this.pdfDoc.getPage(this.pageNum).then((page: PDFPageProxy) => {
            let rotateAbsolute: number;
            if (this.rotationMap[this.pageNum]) {
                rotateAbsolute = this.rotationMap[this.pageNum] + degrees;
                this.rotationMap[this.pageNum] = rotateAbsolute;
            } else {
                this.rotationMap[this.pageNum] = rotateAbsolute = degrees;
            }

            let viewport: PDFPageViewport = page.getViewport(this.currentScale, rotateAbsolute);
            this.renderPage(this.pageNum, viewport);
        });
    }

    public renderPage(pageNum: number, pageViewport?: PDFPageViewport) {
        this.tmpPageViewport = pageViewport;

        this.pdfDoc.getPage(pageNum).then((page) => {
            var viewport: PDFPageViewport = this.tmpPageViewport ? this.tmpPageViewport : page.getViewport(this.currentScale);

            if (this.viewportScaleOfOne === undefined) {
                this.viewportScaleOfOne = { width: 0, height: 0 };

                let tmpViewport = page.getViewport(1);
                this.viewportScaleOfOne.width = tmpViewport.width;
                this.viewportScaleOfOne.height = tmpViewport.height;

                tmpViewport = page.getViewport(2);
                this.scaleUnit.width = (tmpViewport.width - this.viewportScaleOfOne.width)/this.scaleSteps;
                this.scaleUnit.height = (tmpViewport.height - this.viewportScaleOfOne.height)/this.scaleSteps;

                if (this.defaultFill) {
                    this.canRender = false;
                    setTimeout(() => { // AS: for re-rendering
                        this.fill();
                        this.canRender = true;
                    }, 0);
                }
            }

            if (this.canRender) {
                // Prepare canvas using PDF page dimensions
                var canvas = document.getElementById('the-canvas') as HTMLCanvasElement;
                var context = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                // Render PDF page into canvas context
                var renderContext: PDFRenderParams = {
                    canvasContext: context,
                    viewport: viewport
                };
                page.render(renderContext);
            }
        });
    }

    public isVertical() {
        let rotation = this.rotationMap[this.pageNum];
        if (rotation) {
            return !(rotation && Math.abs(rotation) === 90 || Math.abs(rotation) === 270);
        }
        return true;
    }

    public setScale(newScale: number) {
        this.currentScale = newScale;
    }

    goFirst(): number {
        if (this.pdfDoc) {
            this.pageNum = 1;
            this.renderPage(this.pageNum);
            return this.pageNum;
        } else {
            return 0; // AS: for initial call
        }
    }

    goLast(): number {
        if (this.pdfDoc) {
            this.pageNum = this.pdfDoc.numPages;
            this.renderPage(this.pageNum);
            return this.pageNum;
        } else {
          return 0;
        }
    }

    goPrevious(): number {
        if (this.pdfDoc && this.pageNum > 1) {
            --this.pageNum;
            this.renderPage(this.pageNum);
            return this.pageNum;
        }
        return this.pageNum;
    }

    goNext(): number {
        if (this.pdfDoc && this.pageNum < this.pdfDoc.numPages) {
            ++this.pageNum;
            this.renderPage(this.pageNum);
            return this.pageNum;
        }
        return this.pageNum;
    }

    goTo(page: number): number {
        if (this.pdfDoc) {
            this.pageNum = page;
            this.renderPage(this.pageNum);
            return this.pageNum;
        }
        return this.pageNum;
    }

    save(saveName: string): void {
        if (this.pdfDoc) {
            this.pdfDoc.getData().then((data: Uint8Array) => {
                let mediaType: string = 'application/pdf';
                let blob: Blob = new Blob([data], {type: mediaType});
                saveAs(blob, saveName);
            });
        }
    }

    destroy(): void {
        if (this.pdfDoc) {
            this.pdfDoc.destroy();
        }
    }

    public zoom(zoom: number) {
        if (this.pdfDoc) {
            let newScale: number = this.baseZoom * zoom/100;

            this.setScale(newScale);

            if (this.rotationMap[this.pageNum]) {
                this.pdfDoc.getPage(this.pageNum).then((page: PDFPageProxy) => {
                    this.renderPage(this.pageNum, page.getViewport(newScale, this.rotationMap[this.pageNum]));
                });
            } else {
                this.pdfDoc.getPage(this.pageNum).then((page: PDFPageProxy) => {
                    this.renderPage(this.pageNum, page.getViewport(newScale));
                });
            }
        }
    }

    fill(): void {
        if (this.pdfDoc) {
            let newScale: number = 0; // the key to this fitting-problem

            // how much space do we have?
            let widthAvailable: number =  (window.innerWidth || document.body.clientWidth)/2;

            // how much space we needed with scale=1 (100%)
            let widthNecessary: number = this.isVertical() ? this.viewportScaleOfOne.width : this.viewportScaleOfOne.height;

            if (widthAvailable < widthNecessary) { // scale down
                let widthDiff: number = widthNecessary - widthAvailable;
                let scaleSteps: number = this.isVertical() ? (widthDiff / this.scaleUnit.width) : (widthDiff / this.scaleUnit.height);
                newScale = 1 - (Math.ceil(scaleSteps) / 100);

            } else { // scale up
                let widthDiff: number = widthAvailable - widthNecessary;
                let scaleSteps: number = this.isVertical() ? (widthDiff / this.scaleUnit.width) : (widthDiff / this.scaleUnit.height);
                newScale = 1 + (Math.floor(scaleSteps) / 100);//*1.15
            }

            this.setScale(newScale);
            this.baseZoom = newScale;

            if (this.rotationMap[this.pageNum]) {
                this.pdfDoc.getPage(this.pageNum).then((page: PDFPageProxy) => {
                    this.renderPage(this.pageNum, page.getViewport(newScale, this.rotationMap[this.pageNum]));
                });
            } else {
                this.renderPage(this.pageNum, null);
            }
        }
    }

    search(text: string) {
        // TODO: work with pdf-viewer.js
        this.pdfDoc.getPage(this.pageNum).then(page => {
            page.getTextContent().then((content: TextContent) => {
                content.items.forEach((item: TextContentItem) => {
                    if (item.str.indexOf(text) !== -1) {
                        console.log(item);
                        let canvas = document.getElementById('the-canvas') as HTMLCanvasElement;
                        let ctx = canvas.getContext('2d');
                        ctx.rect(item.transform[4], this.viewportScaleOfOne.height-item.transform[5], item.width, item.height);
                        ctx.stroke();
                    }
                })
            });
        });
    }

    /*
    saveWithoutLib(): void {
        if (this.pdfDoc) {
            this.pdfDoc.getData().then((data: Uint8Array) => {
                let mediaType: string = 'application/octet-stream';
                let blob: Blob = new Blob([data], {type: mediaType});
                let objectUrl: string = URL.createObjectURL(blob);
                window.open(objectUrl);
            });
        }
    }

    getMetaData(): void {
        if (this.pdfDoc) {
            this.pdfDoc.getMetadata().then((data) => {
                //info: PDFInfo, metadata: PDFMetadata
                console.log(data);
                //console.log(info);
                //console.log(metadata);
            });
        }
    }
    */
}
