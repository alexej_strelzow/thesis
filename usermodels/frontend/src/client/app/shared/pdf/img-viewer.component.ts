import { Component, Input, Output, EventEmitter } from "@angular/core";
import { CORE_DIRECTIVES } from '@angular/common';

@Component({
	selector: 'img-viewer',
	directives: [CORE_DIRECTIVES],
    template: `<div id="mainContainer">
                <div id="viewer-img-container" tabindex="0">
                   <img id="viewer-image">
                </div>
             </div>`
})
/**
 * PDF-viewer, contains the canvas, which is used for rendering pages.
 */
export class ImgViewer {

    _defaultWidth: number;
    _mimeType: string;
    _data: ArrayBuffer;

    // rotation
    rotationState: number = 0; // current rotation delta, delta of size +/-1
    rotationStep: number = 90;//°
    prevRotationState: number = 0;

    // zoom
    padding = 20; // one side in px
    defaultZoomIndex: number = 6;
    currentZoomIndex: number = this.defaultZoomIndex; //100%
    prevZoomIndex: number;
    zoomSteps: Array<number>;

    @Input() saveName: string;
    @Input() set mimeType(mimeType: string) {
        if (mimeType) {
            this._mimeType = mimeType;
            if (this._data) {
                this.processData();
            }
        }
    }

    get mimeType(): string {
        return this._mimeType;
    }

    @Input() forceViewer: boolean;

    @Input() set data(data: ArrayBuffer) {
        if (data) {
            this._data = data;
            if (this._mimeType) {
                this.processData();
            }
        }
    }

    get data(): ArrayBuffer {
        return this._data;
    }

    @Output() onOpen: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onImgClosed: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor() {
        this.zoomSteps = [25, 33, 50, 67, 75, 90, 100, 110, 125, 150];
        for (let i = 175 ; i <= 500 ; i += 25) {
            this.zoomSteps.push(i);
        }
	}

    processData() : void {
        // supported types: JPEG, GIF, PNG, APNG, SVG, BMP, BMP ICO, PNG ICO
        // https://developer.mozilla.org/de/docs/Web/HTML/Element/img
        let url = URL.createObjectURL(this.getBlob());
        let img: any = this.getImg();

        img.onload = () => {
            URL.revokeObjectURL(url);  // clean up
            this._defaultWidth = img.width;
            this.onOpen.emit(true);
        };
        img.src = url;
    }

    getImg(): any {
        return document.getElementById("viewer-image");
    }

    public rotate(rotation: number) {
        if (rotation) {
            this.rotationState += rotation;
            let degrees: number = this.rotationStep * (this.rotationState%4);
            degrees = degrees < 0 ? degrees + 360 : degrees; // 90° == -270°, 180° == -180°, 270° == -90°
            this.getImg().className = "rotate" + degrees
            this.prevRotationState = this.rotationState;
        }
    }

    public isVertical(): boolean {
        let img: any = this.getImg();
        return img.width > img.height;
    }

    public fill() {
        let img: any = this.getImg();
        let container: any = document.getElementById('viewer-img-container');

        if (this.isVertical()) { // scale to fit width
            let widthAvailable: number =  container.width || window.innerWidth || document.body.clientWidth;
            img.width = widthAvailable - 2 * this.padding;

        } else { // scale to fit height
            let heightAvailable: number =  container.height || window.innerHeight || document.body.clientHeight;
            img.height = heightAvailable - 2 * this.padding;
        }
        this._defaultWidth = this.getImg().width;
        this.currentZoomIndex = this.defaultZoomIndex;
    }

    public zoom(direction: number) {
        if (direction) {
            this.prevZoomIndex = this.currentZoomIndex;
            this.currentZoomIndex += direction;
            if (this.currentZoomIndex < 0 || this.currentZoomIndex > this.zoomSteps.length-1) {
                this.currentZoomIndex = this.prevZoomIndex;
            } else {
                var resize = this.zoomSteps[this.currentZoomIndex]/100; // resize amount in percentage
                this.getImg().width = this._defaultWidth * resize;
            }
        }
    }

    public getBlob(): Blob {
        return new Blob([this._data], {type: this._mimeType});
    }

    public save() {
        saveAs(this.getBlob(), this.saveName);
    }

    public close() {
        this._data = null;
        this.getImg().src = null;
        this.onImgClosed.emit(true);
    }

}
