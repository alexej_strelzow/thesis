import { Component, EventEmitter, Output } from "@angular/core";
import { CORE_DIRECTIVES } from '@angular/common';

@Component({
    selector: 'img-controls',
    directives: [CORE_DIRECTIVES],
    template: `<div id="sidebarContainer" class="pdf-toolbar">
		<div class="splitToolbarButton toggled">
        <i class="fa fa-arrows-alt fa-lg cursor-hand" aria-hidden="true" title="Anpassen" tabindex="1" (click)="fill()"></i>
  
        <i class="fa fa-repeat fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Drehen im UZS" tabindex="2" (click)="rotate(true)"></i>
        
        <i class="fa fa-undo fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Drehen gegen UZS" tabindex="3" (click)="rotate(false)"></i>
        
        <i class="fa fa-plus fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Zoom" tabindex="4" (click)="zoom(true)"></i>
  
        <i class="fa fa-minus fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Zoom" tabindex="5" (click)="zoom(false)"></i>
  
        <i class="fa fa-floppy-o fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Speichern" tabindex="6" (click)="save()"></i>
        
        <i class="fa fa-times fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Schließen" tabindex="7" (click)="close()"></i>
        
        <!--i class="fa fa-print fa-lg cursor-hand pdf-toolbar-element" aria-hidden="true" title="Seite drucken" tabindex="8" (click)="print()"></i-->
		  </div>
    </div>`

})
/**
 * Provides controls like:
 *  -) Adjust image to screen
 *  -) Rotate (counter)clockwise
 *  -) Zoom in/out
 *  -) Save image
 *  -) (Print image)
 */
export class ImgControls {

	constructor(){}

    @Output() onFill: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onRotationChanged: EventEmitter<number> = new EventEmitter<number>();
    @Output() onZoom: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

    fill() {
        this.onFill.emit(true);
    }

    rotate(right: boolean) {
        if (right) {
            this.onRotationChanged.emit(+1);
        } else {
            this.onRotationChanged.emit(-1);
        }
    }

    save() {
        this.onSave.emit(true);
    }

    close() {
        this.onClose.emit(true);
    }

    zoom(magnify: boolean) {
        if (magnify) {
            this.onZoom.emit(+1);
        } else {
            this.onZoom.emit(-1);
        }
    }

    print() {
        window.print();
    }

}
