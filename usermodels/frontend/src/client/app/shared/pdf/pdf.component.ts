import { Component, Input, EventEmitter, Output } from "@angular/core";
import { CORE_DIRECTIVES } from '@angular/common';
import { PdfControls } from "./pdf-controls.component";
import { PdfViewer } from "./pdf-viewer.component";
import { ImgViewer } from "./img-viewer.component";
import { ImgControls } from "./img-controls.component";

@Component({
  selector: 'pdf-component',
  directives: [CORE_DIRECTIVES, PdfControls, PdfViewer, ImgViewer, ImgControls],
  template: `<div id="outerContainer" *ngIf="visible">
               <div *ngIf="showPdfViewer">
                 <pdf-controls #pdfcontrols class="center"
                  (onRotationChanged)="pdfviewer.rotate($event)" 
                  (onPageChanged)="pdfviewer.gotoPage($event)" 
                  (onPageInputChanged)="pdfviewer.gotoCustomPage($event)"
                  (onSave)="pdfviewer.save()" 
                  (onClose)="pdfviewer.close()" 
                  (onFill)="pdfviewer.fill()"
                  (onZoom)="pdfviewer.zoom($event)"
                  (onSearch)="pdfviewer.search($event)">
                 </pdf-controls>
                 <pdf-viewer class="center" #pdfviewer
                  [data]="_data" 
                  [url]="url"
                  [forceViewer]="forceViewer" 
                  [saveName]="saveName"
                  (onOpen)="open($event)"
                  (onPageChanged)="pdfcontrols.setCurrentPage($event); pageChanged($event)" 
                  (onPageCountChanged)="pdfcontrols.setPageCount($event)" 
                  (onPageClosed)="close($event)">
                  </pdf-viewer>
                </div>
                <div *ngIf="showImgViewer"> <!-- TODO: refactor - does not belong here, or change name of pdf.component -->
                    <img-controls #imgcontrols class="center"
                    (onRotationChanged)="imgviewer.rotate($event)"
                    (onFill)="imgviewer.fill()"
                    (onZoom)="imgviewer.zoom($event)"
                    (onSave)="imgviewer.save()"
                    (onClose)="imgviewer.close()">
                    </img-controls>
                    <img-viewer class="center" #imgviewer
                        [data]="_data" 
                        [forceViewer]="forceViewer" 
                        [saveName]="saveName"
                        [mimeType]="_type"
                        (onOpen)="open($event)"
                        (onImgClosed)="close($event)">
                    </img-viewer>
                </div>
            </div>`
  //styleUrls: ['pdf.component.css']
})
/**
 * Pdf component, that combines controls with viewer.
 */
export class PdfComponent {

  visible: boolean = true;
  _data: ArrayBuffer;
  _type: string;

  showPdfViewer: boolean = false;
  showImgViewer: boolean = false;

  private _mimeTypeWhitelist: Array<string> = [];
  private _fileExtensionWhitelist: Array<string> = []; // not in use, just in case

  @Input('forceViewer') forceViewer: boolean;

  @Input('base64DecodedData') set data(data: ArrayBuffer) {
    if (data) {
      this._data = data;
    }
  }

  @Input('fileName') saveName: string;

  @Input('url') url: string;

  @Input('fileType') set type(type: string) {
    if (type) {
      if (this._mimeTypeWhitelist.indexOf(type) !== -1) {
        this._type = type;
        switch (type) {
          case 'application/pdf':
            this.showPdfViewer = true;
            this.showImgViewer = false;
            break;
          default:
            this.showImgViewer = true;
            this.showPdfViewer = false;
        }
        this.visible = true;
      } else {
        //this.openInBrowser(new Blob([this._data], {type: type}));
        saveAs(new Blob([this._data], {type: type}), this.saveName);
      }
    }
  };

  @Output() onOpen: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onClose: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onPageChanged: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
    this._mimeTypeWhitelist.push('application/pdf');
    this._mimeTypeWhitelist.push('image/jpeg');
    this._mimeTypeWhitelist.push('image/png');
    this._mimeTypeWhitelist.push('image/bmp');
    this._mimeTypeWhitelist.push('image/gif');
    this._mimeTypeWhitelist.push('image/svg+xml');

    this._fileExtensionWhitelist.push('pdf');
    this._fileExtensionWhitelist.push('jpg');
    this._fileExtensionWhitelist.push('jpeg');
    this._fileExtensionWhitelist.push('png');
    this._fileExtensionWhitelist.push('bmp');
    this._fileExtensionWhitelist.push('hcp');
    this._fileExtensionWhitelist.push('gif');
    this._fileExtensionWhitelist.push('svg');
  }

  public open(): void {
    this.onOpen.emit(true);
  }

  public close(): void {
    this._data = null;
    this._type = null;
    this.visible = false;

    this.onClose.emit(true);
  }

  public pageChanged($event: number): void {
    this.onPageChanged.emit($event);
  }

  /*
  // alternative to saveAs, if we need some custom handling of browsers
  public openInBrowser(blob): void {
    if (is.ie()) { // http://stackoverflow.com/questions/24007073/open-links-made-by-createobjecturl-in-ie11
      navigator.msSaveOrOpenBlob(blob);
    } else {
      var objectUrl = URL.createObjectURL(blob);
      window.open(objectUrl);
    }
  }
  */

}
