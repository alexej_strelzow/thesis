import { Component, Input, Output, EventEmitter } from "@angular/core";
import { CORE_DIRECTIVES } from '@angular/common';
import { PdfPageService } from './pdf-page.service';

@Component({
	selector: 'pdf-viewer',
	directives: [CORE_DIRECTIVES],
  providers: [PdfPageService],
  template: `<div id="mainContainer">
              <div id="viewerContainer" tabindex="0">
                 <canvas id="the-canvas"></canvas>
              </div>
           </div>`
  //styleUrls: ['pdf.component.css']
})
/**
 * PDF-viewer, contains the canvas, which is used for rendering pages.
 */
export class PdfViewer {
	// rotation
    rotationState: number = 0; // current rotation delta, delta of size +/-1
    rotationStep: number = 90;//°
	prevRotationState: number = 0;

	// page
	currentPage: number = 0;
    numPages: number = 0;
    defaultPdfName: string = 'unnamed.pdf';

    // zoom
    defaultZoomIndex: number = 6;
    currentZoomIndex: number = this.defaultZoomIndex; //100%
    prevZoomIndex: number;
    zoomSteps: Array<number>;

    @Input() saveName: string;
    @Input() forceViewer: boolean;

    @Input() set data(data: any) {
        if (data) {
            this.processData(data);
        }
    }

    @Input() set url(url: string) {
      if (url) {
        this.page.loadUrl(url).subscribe((numPages:number) => {
          if (numPages !== this.numPages) {
            this.numPages = numPages;
            this.currentPage = 1;
            this.onPageChanged.emit(this.currentPage);
            this.onPageCountChanged.emit(this.numPages);
          }
        });
      }
    }

    @Output() onOpen: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onPageChanged: EventEmitter<number> = new EventEmitter<number>();
    @Output() onPageCountChanged: EventEmitter<number> = new EventEmitter<number>();
    @Output() onPageClosed: EventEmitter<boolean> = new EventEmitter<boolean>();

	constructor(private page: PdfPageService) {
        this.zoomSteps = [25, 33, 50, 67, 75, 90, 100, 110, 125, 150];
        for (let i = 175 ; i <= 500 ; i += 25) {
            this.zoomSteps.push(i);
        }
	}

    public processData(data: any) {
        let hasPlugin: boolean = this.page.hasPlugin('PDF');

        if (hasPlugin && !this.forceViewer) {
            this.page.openInBrowser(data);

        } else {
            // TODO: maybe needed
            var blob = new Blob([data], {type: 'application/pdf'});
            var fileReader = new FileReader();
            fileReader.onload = (event:any) => {
                this.displayInViewer(event.currentTarget.result);
                this.onOpen.emit(true);
            };
            fileReader.readAsArrayBuffer(blob);
        }
    }

    public displayInViewer(data: Uint8Array): void {
        this.page.load(data).subscribe((numPages:number) => {
            if (numPages !== this.numPages) {
                this.numPages = numPages;
                this.currentPage = 1;
                this.onPageChanged.emit(this.currentPage);
                this.onPageCountChanged.emit(this.numPages);
            }
        });
    }

	public gotoPage(page: number) {
        if (page && ((page === -1 && this.currentPage > 1) ||
            (page === +1 && this.currentPage < this.numPages))) {
            this.currentPage = this.page.goTo(this.currentPage + page);
            this.onPageChanged.emit(this.currentPage);
        }
    }

    public gotoCustomPage(page: number) {
        if (page) {
            this.currentPage = this.page.goTo(page);
            this.onPageChanged.emit(this.currentPage);
        }
    }

    public search(text: any) {
        if (text) {
            this.page.search(text);
        }
    }

    public fill() {
        this.currentZoomIndex = this.defaultZoomIndex;
        this.page.fill();
    }

    public rotate(rotation: number) {
        if (rotation) {
            this.rotationState += rotation;
            if (this.rotationState > this.prevRotationState) { // right
                this.page.rotate(this.rotationStep);
            } else if (this.rotationState < this.prevRotationState) { // left
                this.page.rotate(-this.rotationStep);
            } else {
                // noop - should never occur
            }
            this.prevRotationState = this.rotationState;
        }
    }

    public zoom(direction: number) {
        if (direction) {
            this.prevZoomIndex = this.currentZoomIndex;
            this.currentZoomIndex += direction;
            if (this.currentZoomIndex < 0 || this.currentZoomIndex > this.zoomSteps.length-1) {
                this.currentZoomIndex = this.prevZoomIndex;
            } else {
                this.page.zoom(this.zoomSteps[this.currentZoomIndex]);
            }
        }
    }

    public save() {
        this.page.save(this.saveName || this.defaultPdfName);
    }

    public close() {
        this.page.destroy();
        this.onPageClosed.emit(true);
    }
}
