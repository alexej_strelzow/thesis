/**
 * Created by Besitzer on 14.08.2016.
 */

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/subscribeOn'
import 'rxjs/Rx';

@Injectable()
export class ExperimentService {

  constructor(private _http: Http) {
    // noop
  }

  getExperiments(): Observable<any> {
    return this._http.get('http://localhost:8080/api/experiments')
      .map((response: Response) => {
        return response.json();
      });
  }

  getExperiment(id: number): Observable<any> {
    return this._http.get('http://localhost:8080/api/experiments/' + id)
      .map((response: Response)=> response.json());
  }

  perform(userId: number, documentId: number): Observable<any> {
    return this._http.get(`http://localhost:8080/api/experiments/perform/model/${userId}/document/${documentId}`)
      .map((response: Response)=> response.json());
  }

  getWordsFromExperiment(id: number): Observable<any> {
    return this._http.get('http://localhost:8080/api/experiments/' + id)
      .map((response: Response)=> response.json())
      .pluck('words');
  }

  getExplanationForWord(word: string, pos: string) {
    return this._http.get('http://localhost:8080/api/babelnet/' + word + '/' + pos)
      .map((response: Response)=> response.json())
  }
}
