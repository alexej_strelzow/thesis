import { Injectable } from '@angular/core';
@Injectable()
export class Mock {

  mock = {
    all: [{id: 1,  muId: 1, mdId: 1, numWords: 176, numTokens: 176},
      {id: 2,  muId: 1, mdId: 1, numWords: 169, numTokens: 169}],
    model_1: {
      id: 1,
      words: [
        {word: "histologic", lemma: "histologic", pos: "JJ", rank: 34824, occurrences: 1},
        {word: "contributory", lemma: "contributory", pos: "JJ", rank: 47351, occurrences: 1},
        {word: "myelosuppression", lemma: null, pos: "NN", rank: -1, occurrences: 1}
      ],
      numWords: 0,
      numTokens: 0
    },
    explanation: {
      word: "dog",
      lemma: "",
      pos: "NOUN",
      rank: 0,
      occurrences: 0,
      babelNetId: 'bn:00015267n',
      glosses: [
        {
          gloss: "A member of the genus Canis (probably descended from the common wolf) that has been domesticated by man since prehistoric times; occurs in many breeds; \"the dog barked all night\"",
          language: "EN"
        },
        {
          gloss: "A male dog, wolf or fox, as opposed to a bitch (often attributive).",
          language: "EN"
        },
        {
          gloss: "Subspecies of the Canidæ family, frequently kept as a pet",
          language: "EN"
        },
        {
          gloss: "The domestic dog, is a subspecies of the gray wolf, a member of the Canidae family of the mammalian order Carnivora.",
          language: "EN"
        },
        {
          gloss: "A common four-legged animal, especially kept by people as a pet or to hunt or guard things.",
          language: "EN"
        },
        {
          gloss: "Haustier",
          language: "DE"
        },
        {
          gloss: "Der Haushund ist ein Haustier und wird als Heim- und Nutztier gehalten.",
          language: "DE"
        },
        {
          gloss: "Ein bekanntes vierbeiniges Tier, das von Menschen vor allem als Haustier gehalten oder zur Jagd und zum Bewachen von Dingen benutzt wird.",
          language: "DE"
        }
      ],
      images: [
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/2/26/YellowLabradorLooking_new.jpg",
          name: "YellowLabradorLooking_new.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/6/67/File:Linnaeus_-_Regnum_Animale_(1735).png",
          name: "File:Linnaeus_-_Regnum_Animale_(1735).png",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/6/64/Tesem2.jpg",
          name: "Tesem2.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/c/c5/Siberian_Husky_pho_.jpg",
          name: "Siberian_Husky_pho_.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/8/8e/Family_pet.jpg",
          name: "Family_pet.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/b/bc/Dog_in_Clark.jpg",
          name: "Dog_in_Clark.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/2/28/Acd_santa.jpg",
          name: "Acd_santa.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/c/ce/Livre_de_Chasse_40v.jpg",
          name: "Livre_de_Chasse_40v.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/f/fb/Gaegogi-01.jpg",
          name: "Gaegogi-01.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/2/2a/Fried_Dogs.JPG",
          name: "Fried_Dogs.JPG",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/d/d4/Puppy_near_Coltani_-_17_apr_2010.jpg",
          name: "Puppy_near_Coltani_-_17_apr_2010.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/4/4f/Gunnar_Kaasen_with_Balto.jpg",
          name: "Gunnar_Kaasen_with_Balto.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/a/a6/Dog_anatomy_lateral_skeleton_view.jpg",
          name: "Dog_anatomy_lateral_skeleton_view.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/7/73/Auge_Hund_Diagramm_engl.jpg",
          name: "Auge_Hund_Diagramm_engl.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/3/33/Anatomy_and_physiology_of_animals_The_ear.jpg",
          name: "Anatomy_and_physiology_of_animals_The_ear.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/8/84/Dog_nose.jpg",
          name: "Dog_nose.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/8/8c/Poligraf_Poligrafovich.JPG",
          name: "Poligraf_Poligrafovich.JPG",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/a/a5/Cavalier_King_Charles_Spaniel_trio.jpg",
          name: "Cavalier_King_Charles_Spaniel_trio.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/e/ec/Terrier_mixed-breed_dog.jpg",
          name: "Terrier_mixed-breed_dog.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/a/af/Golden_retriever_eating_pigs_foot.jpg",
          name: "Golden_retriever_eating_pigs_foot.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/a/ae/Wilde_huendin_am_stillen.jpg",
          name: "Wilde_huendin_am_stillen.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/b/b1/Border_Collie_liver_portrait.jpg",
          name: "Border_Collie_liver_portrait.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/4/46/Sergeant_Stubby_3.jpg",
          name: "Sergeant_Stubby_3.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/4/43/Timba+1.jpg",
          name: "Timba+1.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/1/1e/Zahnformel_Hund.gif",
          name: "Zahnformel_Hund.gif",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/b/bb/Zahnformel_Welpe.png",
          name: "Zahnformel_Welpe.png",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/7/78/Ears_Dog.jpg",
          name: "Ears_Dog.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/a/ad/Augen_hund.jpg",
          name: "Augen_hund.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/c/cf/Auge_Hund_Diagramm.svg",
          name: "Auge_Hund_Diagramm.svg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/e/ec/Nase_hund.jpg",
          name: "Nase_hund.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/f/fc/Vibrissen_Dog.jpg",
          name: "Vibrissen_Dog.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/8/83/Genome_Boxer.jpg",
          name: "Genome_Boxer.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/a/ae/Wilde_huendin_am_stillen.jpg",
          name: "Wilde_huendin_am_stillen.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/4/4e/Faces_of_Cape_Verde6.jpg",
          name: "Faces_of_Cape_Verde6.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/8/88/Dog_and_human_year_graph.png",
          name: "Dog_and_human_year_graph.png",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/1/14/Timba+2.jpg",
          name: "Timba+2.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/6/64/Tesem2.jpg",
          name: "Tesem2.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/1/11/SVVP_Klikatá,_pes_scházející_schody.jpg",
          name: "SVVP_Klikatá,_pes_scházející_schody.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/6/61/Sheep_at_watering_place_in_Bulgaria.jpg",
          name: "Sheep_at_watering_place_in_Bulgaria.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/f/f2/Golden_Retriever_agility_jump.jpg",
          name: "Golden_Retriever_agility_jump.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/e/ef/DogMeat-Hanoi1.jpg",
          name: "DogMeat-Hanoi1.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/7/78/Akita_inu.jpeg",
          name: "Akita_inu.jpeg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/b/b8/Hunde_gras.jpg",
          name: "Hunde_gras.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/9/9a/Saluki_600.jpg",
          name: "Saluki_600.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/2/2a/Dobermann_red_Flickr.jpg",
          name: "Dobermann_red_Flickr.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/c/c3/Tchiorny_Terrier.jpg",
          name: "Tchiorny_Terrier.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/0/01/Dog-biscuit.jpg",
          name: "Dog-biscuit.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/7/75/DogTags_license_fxwb.jpg",
          name: "DogTags_license_fxwb.jpg",
          valid: false
        },
        {
          url: "http://upload.wikimedia.org/wikipedia/commons/2/2a/Police_dog_Peru_Lima_La_Victoria_Plaza_Manco_Cápac.jpg",
          name: "Police_dog_Peru_Lima_La_Victoria_Plaza_Manco_Cápac.jpg",
          valid: false
        }
      ],
      translations: [
        {
          language: "DE",
          translations: [
            "hund",
            "haushund",
            "chinesische_astrologie#zählung_ab_jahresbeginn"
          ]
        }
      ]
    }
  };
}
