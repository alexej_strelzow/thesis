import { RouterConfig } from '@angular/router';

import { ExperimentComponent } from './index';

export const ExperimentRoutes: RouterConfig = [
  {
    path: 'experiment',
    component: ExperimentComponent
  }
];
