import { Component } from '@angular/core';
import { ExperimentListComponent } from './list/index';
import { ExperimentEntryModel } from "./list/entry/index";
import { ExperimentService } from './experiment.service';
import { WordListComponent, WordModel, ExplanationModel, ExplanationListComponent } from "../shared/index";
import { Mock } from "./mock";

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-document',
  directives: [ExperimentListComponent, WordListComponent, ExplanationListComponent],
  providers: [ExperimentService, Mock],
  templateUrl: 'experiment.component.html',
  styleUrls: ['experiment.component.css']
})
export class ExperimentComponent {
  mock: any;
  offline: boolean;

  allModels: Array<ExperimentEntryModel>;
  currentModel: Array<WordModel>;
  currentExplanations: Array<ExplanationModel>;

  constructor(private _service: ExperimentService, private _mock: Mock) {
    _service.getExperiments().subscribe((experiments: any) => {
      this.allModels = [];
      this.currentExplanations = [];
      // transform experiments to fit Array<ExperimentEntryModel>
      Object.keys(experiments).forEach((key: any) => {
        this.allModels.push((<ExperimentEntryModel> experiments[key]));
      });

      //this.setMockData(_mock);

    }, error => {
      this.setMockData(_mock);
    });
  }

  setMockData(_mock: Mock): void {
    this.offline = true;
    this.mock = _mock.mock;
    this.allModels = this.mock.all;
    this.currentModel = this.mock.model_1.words;
    this.currentExplanations.push(this.mock.explanation);
  }

  clicked($event: any): void {
    // $event = id
    if (!this.offline) {
      this.getExperimentForId($event);
    } else {
      this.currentModel = this.mock.model_1.words;
    }
  }

  getExperimentForId(id: number) {
    this._service.getWordsFromExperiment(id).subscribe((words: Array<WordModel>) => {
      this.currentModel = words;
    });
  }

  getExplanationForWord(word: string, pos: string) {
    this._service.getExplanationForWord(word, pos).subscribe((explanation: ExplanationModel) => {
      this.currentExplanations.push(explanation);
    });
  }

  explain($event: WordModel): void {
    // $event = WordModel
    if (!this.offline) {
      this.getExplanationForWord($event.word, $event.pos)
    } else {
      this.currentExplanations.push(this.mock.explanation);
    }
  }
}
