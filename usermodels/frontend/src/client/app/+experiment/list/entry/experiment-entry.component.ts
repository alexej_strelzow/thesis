import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ExperimentEntryModel } from './index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'experiment-entry',
  templateUrl: 'experiment-entry.component.html',
  styleUrls: ['experiment-entry.component.css']
})
export class ExperimentEntryComponent {

  @Input() element: ExperimentEntryModel = undefined;
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  clicked($event: any): void {
    this.onClick.emit(this.element.id);
  }

}
