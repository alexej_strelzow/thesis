/**
 * Created by Besitzer on 14.08.2016.
 */
export class ExperimentEntryModel {
  id: number;
  words: Array<String>;
  muId: number;
  mdId: number;
  numWords: number;
  numTokens: number;
}
