import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ExperimentEntryComponent } from './entry/index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'experiment-list',
  directives: [ExperimentEntryComponent],
  templateUrl: 'experiment-list.component.html',
  styleUrls: ['experiment-list.component.css']
})
export class ExperimentListComponent {

  @Input() elements = {};
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  clicked($event: any): void {
    this.onClick.emit($event);
  }

}
