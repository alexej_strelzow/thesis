/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './user-model-list.component';
export * from './entry/index'
export * from './detail/index'
