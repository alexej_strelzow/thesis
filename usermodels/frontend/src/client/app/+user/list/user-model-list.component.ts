import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserModelEntryComponent, UserModelEntryModel } from './entry/index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'user-model-list',
  directives: [UserModelEntryComponent],
  templateUrl: 'user-model-list.component.html',
  styleUrls: ['user-model-list.component.css']
})
export class UserModelListComponent {

  @Input() elements: Array<UserModelEntryModel>;
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  clicked($event: any): void {
    this.onClick.emit($event);
  }

}
