import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserModelEntryModel } from './../entry/index';
import { ThousandSeparatorPipe } from "../../../shared/index";

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'user-model-detail',
  templateUrl: 'user-model-detail.component.html',
  styleUrls: ['user-model-detail.component.css'],
  pipes: [ThousandSeparatorPipe]
})
export class UserModelDetailComponent {

  @Input() element: UserModelEntryModel = undefined;
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  clicked($event: any): void {
    this.onClick.emit(this.element.id);
  }

}
