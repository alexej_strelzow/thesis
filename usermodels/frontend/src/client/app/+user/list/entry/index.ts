/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './user-model-entry.component';
export * from './user-model-entry.model';
