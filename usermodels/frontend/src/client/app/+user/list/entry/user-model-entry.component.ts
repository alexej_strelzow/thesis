import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserModelEntryModel } from './index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'user-model-entry',
  templateUrl: 'user-model-entry.component.html',
  styleUrls: ['user-model-entry.component.css']
})
export class UserModelEntryComponent {

  @Input() element: UserModelEntryModel = undefined;
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  clicked($event: any): void {
    this.onClick.emit(this.element.id);
  }

}
