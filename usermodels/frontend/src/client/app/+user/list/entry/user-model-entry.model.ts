/**
 * Created by Besitzer on 14.08.2016.
 */
export class UserModelEntryModel {
  name: string;
  id: number;
  words: Array<String>;
  numDocs: number;
  numWords: number;
  numTokens: number;
  initalTokenSize: number;
  tokenSizeAfterPreprocessing: number;
  avgRemovedTokens: number;
  avgRemovedTokensAfterPreprocessing: number;
  containingDocs: Array<String>;
  avgDocLength: number;
  avgRank: number;
}
