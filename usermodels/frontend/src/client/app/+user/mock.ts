import { Injectable } from '@angular/core';
@Injectable()
export class Mock {
// words + containingDocs = []
  mock = {
    all: [
        {
          name: "Amir K. Khandani",
          id: 1,
          words: '',
          numDocs: 0,
          numWords: 70358,
          numTokens: 4567,
          initalTokenSize: 328293,
          tokenSizeAfterPreprocessing: 269179,
          avgRemovedTokens: 10317,
          avgRemovedTokensAfterPreprocessing: 7952,
          containingDocs: '',
          avgDocLength: 52212.56,
          avgRank: 12739.95860665008
        },
        {
          name: "Andrea Montanari",
          id: 2,
          words: '',
          numDocs: 0,
          numWords: 108276,
          numTokens: 7323,
          initalTokenSize: 523962,
          tokenSizeAfterPreprocessing: 414318,
          avgRemovedTokens: 16627,
          avgRemovedTokensAfterPreprocessing: 12241,
          containingDocs: '',
          avgDocLength: 80248,
          avgRank: 13547.946491599605
      }
    ],
    model_1: {

    }
  };
}
