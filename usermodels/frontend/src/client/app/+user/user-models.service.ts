/**
 * Created by Besitzer on 20.05.2016.
 */

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { UserModelEntryModel } from './list/entry/index';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/subscribeOn'
import 'rxjs/Rx';

@Injectable()
export class UserModelsService {

  getAll: Observable<any>;

  allModels: Array<UserModelEntryModel>;

  constructor(private _http: Http) {
    this.getAll = this.prepareGetAll();
  }

  private prepareGetAll(): Observable<any> {
    return this._http.get('http://localhost:8080/api/usermodel')
      .map((response: Response) => {
        return response.json();
      });
  }

  public getAllUserModels(): Observable<Array<UserModelEntryModel>> {
    if (this.allModels) {
      return Observable.of(this.allModels);
    } else {
      return new Observable<Array<UserModelEntryModel>>((observer: any) => {
        this.getAll.subscribe((userModels:any) => {
          this.allModels = [];
          // transform userModels to fit Array<UserModelEntryModel>
          Object.keys(userModels).forEach((key:any) => {
            this.allModels.push((<UserModelEntryModel> userModels[key]));
          });

          observer.next(this.allModels);
          observer.complete();
        });
      });
    }
  }

  public getUserModel(id: number): Observable<any> {
    return this._http.get('http://localhost:8080/api/usermodel/' + id)
      .map((response: Response) => {
        return response.json();
      });
  }

}
