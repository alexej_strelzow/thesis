import { Component } from '@angular/core';
import { UserModelListComponent } from './list/index';
import { UserModelDetailComponent } from './list/detail/index';
import { UserModelEntryModel } from './list/entry/index';
import { UserModelsService } from './user-models.service';
import { DocumentNamePipe, SortPipe } from "../shared/pipes/index";
import { Mock } from './mock';

/**
 * This class represents the lazy loaded UserComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-user',
  directives: [UserModelListComponent, UserModelDetailComponent],
  providers: [UserModelsService, Mock],
  templateUrl: 'user.component.html',
  styleUrls: ['user.component.css'],
  pipes: [DocumentNamePipe, SortPipe]
})
export class UserComponent {

  mock: any;
  offline: boolean;

  allModels: Array<UserModelEntryModel>;
  currentModel: UserModelEntryModel;

  constructor(private _service: UserModelsService, private _mock: Mock) {
    _service.getAllUserModels().subscribe((models: Array<UserModelEntryModel>) => {
      this.allModels = models;
    }, (error: any) => {
      this.offline = true;
      this.mock = _mock.mock;
      this.allModels = this.mock.all;
      this.currentModel = this.allModels[0];
    });
  }

  clicked($event: number): void {
    // $event = id
    if (!this.offline) {
      this.allModels.forEach((model: UserModelEntryModel) => {
        if (model.id === $event) {
          this.currentModel = model;
        }
      });
    } else {
      this.currentModel = this.mock.all[$event - 1];
    }

    //this.getUserModelForId($event);
  }

  getUserModelForId(id: number) {
    this._service.getUserModel(id).subscribe((userModel: UserModelEntryModel) => {
      let temp: UserModelEntryModel = userModel;
    });
  }

}
