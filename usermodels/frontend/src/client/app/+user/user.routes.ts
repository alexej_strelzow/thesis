import { RouterConfig } from '@angular/router';
import { UserComponent } from './index';

export const UserRoutes: RouterConfig = [
  {
    path: 'user',
    component: UserComponent
  }
];
