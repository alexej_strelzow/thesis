import { RouterConfig } from '@angular/router';

import { DocumentComponent } from './index';

export const DocumentRoutes: RouterConfig = [
  {
    path: 'document',
    component: DocumentComponent
  }
];
