import { Component } from '@angular/core';
import { DocumentModel } from './document.model';
import { DocumentService } from './document.service';
import { DocumentComponent } from './document.component';

/**
 * This class represents the lazy loaded DocumentComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-document2',
  providers: [DocumentService],
  templateUrl: 'document-list.component.html',
  directives: [DocumentComponent],
  styleUrls: ['document.component.css']
})
export class DocumentListComponent {

  private models: Array<DocumentModel>;

  constructor(private service: DocumentService) {
    this.models = service.getMockedDocuments();
  }

}
