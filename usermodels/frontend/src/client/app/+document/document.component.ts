import { Component, Input } from '@angular/core';
import { DocumentModel } from './document.model';
import { WordListComponent } from "../shared/word-list/index";
import { DocumentModelListComponent } from "./list/index";
import { UserModelsService } from "../+user/user-models.service";
import { UserModelEntryModel } from "../+user/list/entry/index";
import { DocumentService } from "./document.service";
import { WordModel } from "../shared/word-list/entry/index";
import { ThousandSeparatorPipe } from "../shared/pipes/index";

/**
 * This class represents the lazy loaded DocumentComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-document',
  directives: [DocumentModelListComponent, WordListComponent],
  providers: [UserModelsService, DocumentService],
  templateUrl: 'document.component.html',
  styleUrls: ['document.component.css'],
  pipes: [ThousandSeparatorPipe]
})
export class DocumentComponent {

  userModels: Array<UserModelEntryModel>;
  currentUserId: number;
  currentUser: UserModelEntryModel;
  currentDocument: DocumentModel;
  rawText: string;

  constructor(private _userModelService: UserModelsService, private _documentModelService: DocumentService) {
    _userModelService.getAllUserModels().subscribe((models: Array<UserModelEntryModel>) => {
      this.userModels = models;
    }, (error: any) => {
      console.log(error);
    });
  }

  onSelectionChanged($event: any): void {
    let id = parseInt($event, 10);
    this.userModels.forEach((userModel: UserModelEntryModel) => {
      if (userModel.id === id) {
        this.currentUser = userModel;
      }
    });
  }

  clicked($event: any): void {
    this._documentModelService.getDocumentByTitle($event)
      .subscribe((document: DocumentModel) => {
        this.currentDocument = document;
        this.getRawDocument(document.rawDocId);
      });
  }

  getRawDocument(rawDocId: number) {
    this._documentModelService.getRawDocument(rawDocId)
      .subscribe((text: any) => {
        this.rawText = text;
      });
  }

}
