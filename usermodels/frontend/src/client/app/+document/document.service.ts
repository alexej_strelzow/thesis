/**
 * Created by Besitzer on 20.05.2016.
 */

import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from '@angular/http';
import { Observer, Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/subscribeOn'
import 'rxjs/Rx';


import { DocumentModel } from './document.model';

@Injectable()
export class DocumentService {

  mockIt: boolean = true;
  documents: Array<DocumentModel> = null;

  constructor(private _http: Http) {
    if (this.mockIt) {
      this.documents = this.getMockedDocuments();
    } else {
      this.getAllDocuments().subscribe();
    }
  }

  getDocumentByTitle(title: string): Observable<any> {
    return this._http.get('http://localhost:8080/api/documents/title/' + title)
      .map((response: Response) => response.json());
  }

  getRawDocument(id: number): Observable<any> {
    return this._http.get('http://localhost:8080/api/documents/raw/' + id)
      .map((response: Response) => response.json())
      .pluck('content');
  }

  getDocument(id: number): Observable<any> /*Observable<Array<DocumentModel>>*/ {
    return this._http.get('http://localhost:8080/api/documents/' + id).map((response: Response) => {
      let document: DocumentModel = response.json();
      return document;
      /*
       let model: DocumentModel = new DocumentModel();
       model.id = response.id;
       model.rawDocId = response.rawDocId;
       model.title = response.title;
       model.wordCount = response.wordCount;
       model.uniqueWordCount = response.uniqueWordCount;
       */
    });
  }

  getAllDocuments(): Observable<any> /*Observable<Array<DocumentModel>>*/ {
    return this._http.get('http://localhost:8080/api/documents').map((response: Response) => {
        let documents: Array<DocumentModel> = response.json();

        this.documents = [];
        Object.keys(documents).forEach((key: any, index: number) => {
          this.documents.push((<DocumentModel> documents[key]));
        });
      });
  }

  getMockedDocuments(): Array<DocumentModel> {
    if (!this.documents) {
      let mockObjects: string = '{"model_7":{"title":"Accurate Prediction of Phase Transitions in Compressed Sensing via a Connection to Minimax Denoising","rawDocId":7,"id":7,"words":[],"wordCount":8895,"uniqueWordCount":1912},"model_26":{"title":"An adaptive block based integrated LDP  GLCM  and Morphological features for Face Recognition.","rawDocId":26,"id":26,"words":[],"wordCount":2497,"uniqueWordCount":816},"model_6":{"title":"Accelerated Time-of-Flight Mass Spectrometry","rawDocId":6,"id":6,"words":[],"wordCount":4044,"uniqueWordCount":1331},"model_27":{"title":"An Improved Feature Descriptor for Recognition of Handwritten Bangla Alphabet.","rawDocId":27,"id":27,"words":[],"wordCount":1405,"uniqueWordCount":607},"model_9":{"title":"Alias and Change Calculi  Applied to Frame Inference.","rawDocId":9,"id":9,"words":[],"wordCount":3667,"uniqueWordCount":1203},"model_24":{"title":"A Generic Framework for Handling Uncertain Data with Local Correlations.","rawDocId":24,"id":24,"words":[],"wordCount":4902,"uniqueWordCount":1302},"model_68":{"title":"Schirmer MA - J Natl Cancer Inst - 2016","rawDocId":68,"id":68,"words":[],"wordCount":3278,"uniqueWordCount":1182},"model_8":{"title":"Accurate Prediction of Phase Transitions in Compressed Sensing via a Connection to Minimax Denoising","rawDocId":8,"id":8,"words":[],"wordCount":8895,"uniqueWordCount":1912},"model_25":{"title":"An adaptive block based integrated LDP  GLCM  and Morphological features for Face Recognition.","rawDocId":25,"id":25,"words":[],"wordCount":2497,"uniqueWordCount":816},"model_69":{"title":"Snyder PJ - N Engl J Med - 2016","rawDocId":69,"id":69,"words":[],"wordCount":3315,"uniqueWordCount":937},"model_28":{"title":"An Improved Feature Descriptor for Recognition of Handwritten Bangla Alphabet.","rawDocId":28,"id":28,"words":[],"wordCount":1405,"uniqueWordCount":607},"model_29":{"title":"Accuracy Improvement for Stiffness Modeling of Parallel Manipulators","rawDocId":29,"id":29,"words":[],"wordCount":3116,"uniqueWordCount":1085},"model_62":{"title":"Pachnio A - J Immunol - 2015","rawDocId":62,"id":62,"words":[],"wordCount":5283,"uniqueWordCount":1411},"model_63":{"title":"Park J - J Biomed Sci - 2015","rawDocId":63,"id":63,"words":[],"wordCount":2613,"uniqueWordCount":1007},"model_60":{"title":"McCullough LE - Clin Epigenetics - 2016","rawDocId":60,"id":60,"words":[],"wordCount":2979,"uniqueWordCount":1064},"model_61":{"title":"Pachnio A - J Immunol - 2015","rawDocId":61,"id":61,"words":[],"wordCount":5283,"uniqueWordCount":1411},"model_22":{"title":"An Efficient Similarity Search Framework for SimRank over Large Dynamic Graphs.","rawDocId":22,"id":22,"words":[],"wordCount":5650,"uniqueWordCount":1308},"model_66":{"title":"Rochtus A - Clin Epigenetics - 2016","rawDocId":66,"id":66,"words":[],"wordCount":3081,"uniqueWordCount":945},"model_23":{"title":"A Generic Framework for Handling Uncertain Data with Local Correlations.","rawDocId":23,"id":23,"words":[],"wordCount":4902,"uniqueWordCount":1302},"model_67":{"title":"Schirmer MA - J Natl Cancer Inst - 2016","rawDocId":67,"id":67,"words":[],"wordCount":3278,"uniqueWordCount":1182},"model_20":{"title":"A Measure of Transaction Processing 20 Years Later","rawDocId":20,"id":20,"words":[],"wordCount":409,"uniqueWordCount":251},"model_64":{"title":"Park J - J Biomed Sci - 2015","rawDocId":64,"id":64,"words":[],"wordCount":2613,"uniqueWordCount":1007},"model_21":{"title":"An Efficient Similarity Search Framework for SimRank over Large Dynamic Graphs.","rawDocId":21,"id":21,"words":[],"wordCount":5650,"uniqueWordCount":1308},"model_65":{"title":"Rochtus A - Clin Epigenetics - 2016","rawDocId":65,"id":65,"words":[],"wordCount":3081,"uniqueWordCount":945},"model_37":{"title":"Algorithms and Complexity Results for Exact Bayesian Structure Learning","rawDocId":37,"id":37,"words":[],"wordCount":2522,"uniqueWordCount":727},"model_38":{"title":"Algorithms and Complexity Results for Exact Bayesian Structure Learning","rawDocId":38,"id":38,"words":[],"wordCount":2522,"uniqueWordCount":727},"model_35":{"title":"An Upper Bound on the Convergence Time for Quantized Consensus","rawDocId":35,"id":35,"words":[],"wordCount":2625,"uniqueWordCount":749},"model_36":{"title":"An Upper Bound on the Convergence Time for Quantized Consensus","rawDocId":36,"id":36,"words":[],"wordCount":2625,"uniqueWordCount":749},"model_39":{"title":"Algorithms and Complexity Results for Persuasive Argumentation","rawDocId":39,"id":39,"words":[],"wordCount":3813,"uniqueWordCount":874},"model_73":{"title":"Tiacci E - N Engl J Med - 2015","rawDocId":73,"id":73,"words":[],"wordCount":4179,"uniqueWordCount":1205},"model_30":{"title":"Accuracy Improvement for Stiffness Modeling of Parallel Manipulators","rawDocId":30,"id":30,"words":[],"wordCount":3116,"uniqueWordCount":1085},"model_74":{"title":"Tiacci E - N Engl J Med - 2015","rawDocId":74,"id":74,"words":[],"wordCount":4179,"uniqueWordCount":1205},"model_71":{"title":"The role of CD44 in fetal and adult","rawDocId":71,"id":71,"words":[],"wordCount":2863,"uniqueWordCount":927},"model_72":{"title":"The role of CD44 in fetal and adult","rawDocId":72,"id":72,"words":[],"wordCount":2863,"uniqueWordCount":927},"model_33":{"title":"An Upper Bound on the Convergence Time for Distributed Binary Consensus","rawDocId":33,"id":33,"words":[],"wordCount":1745,"uniqueWordCount":607},"model_34":{"title":"An Upper Bound on the Convergence Time for Distributed Binary Consensus","rawDocId":34,"id":34,"words":[],"wordCount":1745,"uniqueWordCount":607},"model_31":{"title":"Analyse Comparative des Manipulateurs 3R   Axes Orthogonaux","rawDocId":31,"id":31,"words":[],"wordCount":326,"uniqueWordCount":157},"model_75":{"title":"Turkbey - Multiparametric Prostate Magnetic Resonance Imaging in","rawDocId":75,"id":75,"words":[],"wordCount":4088,"uniqueWordCount":1432},"model_32":{"title":"Analyse Comparative des Manipulateurs 3R   Axes Orthogonaux","rawDocId":32,"id":32,"words":[],"wordCount":326,"uniqueWordCount":157},"model_76":{"title":"Turkbey - Multiparametric Prostate Magnetic Resonance Imaging in","rawDocId":76,"id":76,"words":[],"wordCount":4088,"uniqueWordCount":1432},"model_70":{"title":"Snyder PJ - N Engl J Med - 2016","rawDocId":70,"id":70,"words":[],"wordCount":3315,"uniqueWordCount":937},"model_1":{"title":"An Efficient Adaptive Distributed Space-Time Coding Scheme for Cooperative Relaying","rawDocId":1,"id":1,"words":[],"wordCount":2445,"uniqueWordCount":825},"model_3":{"title":"Asymptotic Analysis of Amplify and Forward Relaying in a Parallel MIMO Relay Network","rawDocId":3,"id":3,"words":[],"wordCount":2517,"uniqueWordCount":700},"model_2":{"title":"An Efficient Adaptive Distributed Space-Time Coding Scheme for Cooperative Relaying","rawDocId":2,"id":2,"words":[],"wordCount":2445,"uniqueWordCount":825},"model_5":{"title":"Accelerated Time-of-Flight Mass Spectrometry","rawDocId":5,"id":5,"words":[],"wordCount":4044,"uniqueWordCount":1331},"model_4":{"title":"Asymptotic Analysis of Amplify and Forward Relaying in a Parallel MIMO Relay Network","rawDocId":4,"id":4,"words":[],"wordCount":2517,"uniqueWordCount":700},"model_48":{"title":"Boers A - Clin Epigenetics - 2016","rawDocId":48,"id":48,"words":[],"wordCount":3581,"uniqueWordCount":998},"model_49":{"title":"Clermont PL - Clin Epigenetics - 2016","rawDocId":49,"id":49,"words":[],"wordCount":3732,"uniqueWordCount":1317},"model_46":{"title":"Bendell JC - Cancer Chemother Pharmacol - 2015","rawDocId":46,"id":46,"words":[],"wordCount":2211,"uniqueWordCount":839},"model_47":{"title":"Boers A - Clin Epigenetics - 2016","rawDocId":47,"id":47,"words":[],"wordCount":3581,"uniqueWordCount":998},"model_40":{"title":"Algorithms and Complexity Results for Persuasive Argumentation","rawDocId":40,"id":40,"words":[],"wordCount":3813,"uniqueWordCount":874},"model_41":{"title":"Baerlocher GM - N Engl J Med - 2015","rawDocId":41,"id":41,"words":[],"wordCount":2797,"uniqueWordCount":905},"model_44":{"title":"Barazeghi E - Clin Epigenetics - 2016","rawDocId":44,"id":44,"words":[],"wordCount":2469,"uniqueWordCount":865},"model_45":{"title":"Bendell JC - Cancer Chemother Pharmacol - 2015","rawDocId":45,"id":45,"words":[],"wordCount":2211,"uniqueWordCount":839},"model_42":{"title":"Baerlocher GM - N Engl J Med - 2015","rawDocId":42,"id":42,"words":[],"wordCount":2797,"uniqueWordCount":905},"model_43":{"title":"Barazeghi E - Clin Epigenetics - 2016","rawDocId":43,"id":43,"words":[],"wordCount":2469,"uniqueWordCount":865},"model_15":{"title":"An Access Control Mechanism Based on the Generalized Aryabhata Remainder Theorem.","rawDocId":15,"id":15,"words":[],"wordCount":1893,"uniqueWordCount":600},"model_59":{"title":"McCullough LE - Clin Epigenetics - 2016","rawDocId":59,"id":59,"words":[],"wordCount":2979,"uniqueWordCount":1064},"model_16":{"title":"An Access Control Mechanism Based on the Generalized Aryabhata Remainder Theorem.","rawDocId":16,"id":16,"words":[],"wordCount":1893,"uniqueWordCount":600},"model_13":{"title":"Adjustment Hiding Method Based on Exploiting Modification Direction.","rawDocId":13,"id":13,"words":[],"wordCount":2425,"uniqueWordCount":613},"model_57":{"title":"Jung M - Clin Epigenetics - 2016","rawDocId":57,"id":57,"words":[],"wordCount":3566,"uniqueWordCount":1005},"model_14":{"title":"Adjustment Hiding Method Based on Exploiting Modification Direction.","rawDocId":14,"id":14,"words":[],"wordCount":2425,"uniqueWordCount":613},"model_58":{"title":"Jung M - Clin Epigenetics - 2016","rawDocId":58,"id":58,"words":[],"wordCount":3566,"uniqueWordCount":1005},"model_19":{"title":"A Measure of Transaction Processing 20 Years Later","rawDocId":19,"id":19,"words":[],"wordCount":409,"uniqueWordCount":251},"model_17":{"title":"A Critique of ANSI SQL Isolation Levels","rawDocId":17,"id":17,"words":[],"wordCount":4239,"uniqueWordCount":1238},"model_18":{"title":"A Critique of ANSI SQL Isolation Levels","rawDocId":18,"id":18,"words":[],"wordCount":4239,"uniqueWordCount":1238},"model_51":{"title":"Cytoskeletal perturbation leads to platelet","rawDocId":51,"id":51,"words":[],"wordCount":3078,"uniqueWordCount":892},"model_52":{"title":"Cytoskeletal perturbation leads to platelet","rawDocId":52,"id":52,"words":[],"wordCount":3078,"uniqueWordCount":892},"model_50":{"title":"Clermont PL - Clin Epigenetics - 2016","rawDocId":50,"id":50,"words":[],"wordCount":3732,"uniqueWordCount":1317},"model_11":{"title":"Automated Fixing of Programs with Contracts.","rawDocId":11,"id":11,"words":[],"wordCount":11717,"uniqueWordCount":2467},"model_55":{"title":"Jia M - Clin Epigenetics - 2016","rawDocId":55,"id":55,"words":[],"wordCount":2406,"uniqueWordCount":736},"model_12":{"title":"Automated Fixing of Programs with Contracts.","rawDocId":12,"id":12,"words":[],"wordCount":11717,"uniqueWordCount":2467},"model_56":{"title":"Jia M - Clin Epigenetics - 2016","rawDocId":56,"id":56,"words":[],"wordCount":2406,"uniqueWordCount":736},"model_53":{"title":"Garfall AL - N Engl J Med - 2015","rawDocId":53,"id":53,"words":[],"wordCount":2118,"uniqueWordCount":784},"model_10":{"title":"Alias and Change Calculi  Applied to Frame Inference.","rawDocId":10,"id":10,"words":[],"wordCount":3667,"uniqueWordCount":1203},"model_54":{"title":"Garfall AL - N Engl J Med - 2015","rawDocId":54,"id":54,"words":[],"wordCount":2118,"uniqueWordCount":784}}';
      let documents: Array<DocumentModel> = JSON.parse(mockObjects);

      this.documents = [];
      Object.keys(documents).forEach((key:any, index:number) => {
        // key: the name of the object key
        // index: the ordinal position of the key within the object
        this.documents.push((<DocumentModel> documents[key]));
      });

    }
    return this.documents;
  }

}
