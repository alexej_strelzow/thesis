/**
 * This barrel file provides the export for the lazy loaded DocumentComponent.
 */
export * from './document.component';
export * from './document.model';
export * from './document-list.component';
export * from './document.routes';
export * from './document.service';

