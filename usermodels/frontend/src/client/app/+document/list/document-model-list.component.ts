import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserModelEntryComponent } from "../../+user/list/entry/index";
import { DocumentNamePipe, SortPipe } from "../../shared/pipes/index";

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'document-model-list',
  directives: [UserModelEntryComponent],
  templateUrl: 'document-model-list.component.html',
  styleUrls: ['document-model-list.component.css'],
  pipes: [DocumentNamePipe, SortPipe]
})
export class DocumentModelListComponent {

  @Input() elements: Array<String>;
  @Output() onClick: EventEmitter<number> = new EventEmitter<number>();

  clicked($event: any): void {
    this.onClick.emit($event);
  }

}
