import { WordModel } from "../shared/word-list/entry/index";
/**
 * Created by Besitzer on 20.05.2016.
 */

export class DocumentModel {

  id: number;
  rawDocId: number;
  title: string;
  words: Array<WordModel>;
  language: string;
  wordCount: number;
  uniqueWordCount: number;

}
