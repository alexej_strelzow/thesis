import { RouterConfig } from '@angular/router';

import { DemoComponent } from './index';

export const DemoRoutes: RouterConfig = [
  {
    path: 'demo',
    component: DemoComponent
  }
];
