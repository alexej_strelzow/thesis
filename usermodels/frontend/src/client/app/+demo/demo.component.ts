import { Component } from '@angular/core';
import { PdfComponent, PdfService } from "../shared/pdf/index";
import { DocumentModel } from "../+document/index";
import { UserModelsService } from "../+user/user-models.service";
import { UserModelEntryModel } from "../+user/list/entry/index";
import { WordListComponent, WordModel, ExplanationModel, ExplanationListComponent } from "../shared/index";
import { ExperimentService } from '../+experiment/experiment.service';

/**
 * This class represents the lazy loaded ContactComponent.
 */

@Component({
  moduleId: module.id,
  selector: 'sd-demo',
  directives: [PdfComponent, WordListComponent, ExplanationListComponent],
  providers: [UserModelsService, PdfService, ExperimentService],
  templateUrl: 'demo.component.html',
  styleUrls: ['demo.component.css']
})
export class DemoComponent {

  userModels: Array<UserModelEntryModel>;
  currentUser: UserModelEntryModel;
  file: File;
  requestInProgess: boolean;

  // viewer
  data: Uint8Array;
  url: string;
  forceViewer: boolean  = true;
  fileName: string;
  fileType: string = 'application/pdf';

  currentPage: number;
  currentUnknownWords: Array<WordModel>;
  pagesToWords: { [pageNumber: number]: Array<WordModel>; } = <any>{};

  documentModel: DocumentModel;
  displayPdf: boolean = false;
  experiment: any;
  unknownWords: Array<WordModel>;
  currentExplanations: Array<ExplanationModel>;

  constructor(private _userModelService: UserModelsService,
              private _service: PdfService,
              private _experimentService: ExperimentService) {
    _userModelService.getAllUserModels().subscribe((models: Array<UserModelEntryModel>) => {
      this.userModels = models;
    }, (error: any) => {
      console.log(error);
    });
    //this.getFileByTitle('strelzow.pdf');
  }

  public isExperimentButtonEnabled(): boolean {
    return !!this.currentUser && !!this.file;
  }

  onSelectionChanged($event: any): void {
    let id = parseInt($event, 10);
    this.userModels.forEach((userModel: UserModelEntryModel) => {
      if (userModel.id === id) {
        this.currentUser = userModel;
      }
    });
  }

  /*
    4. Response arrived -> show document model or just info -> OK
    5. Select -> User Model + Document Model -> do experiment
    6. Open PDF in viewer on button click
    7. Show experiment result
      Open PDF in Viewer 1/2 page
      Open Unknown words
      Open Explanation List
   */

  public selectFile($event: any): void {
    var inputValue: any = $event.target;
    this.file = inputValue.files[0];
  }

  public sendRequest(): void {
    this.requestInProgess = true;
    this._service.uploadPdf(this.file).subscribe((response: any ) => {
      response.map((response:any) => response.json())
        .subscribe((res: DocumentModel) => {
            this.documentModel = res;
            this.getFileByTitle(this.file.name);
          },
          (error: any) => {
            console.log('error occured!');
          },
          () => {
            this.requestInProgess = false;
          });
    });
  }

  public getFileByTitle($event: string): any {
    // 'strelzow.pdf'
    this._service.getFileByTitle($event).subscribe((data: any) => {
      this.fileName = $event;
      this.data = data;
      this.displayPdf = true;
      this.conductExperiment();
    });
  }

  public conductExperiment(): void {
    this._experimentService.perform(this.currentUser.id, this.documentModel.id)
      .subscribe((data: any) => {
        this.experiment = data;
        this.unknownWords = this.experiment.words;

        this.unknownWords.forEach((word: WordModel) => {
          word.pages.forEach((page: number) => {
            if (this.pagesToWords[page]) {
              this.pagesToWords[page].push(word);
            } else {
              this.pagesToWords[page] = [];
              this.pagesToWords[page].push(word);
            }
          });
        });

        this.currentUnknownWords = [];
        this.currentExplanations = [];
        this.currentPage = 1;
        this.pageChanged(1);
    });
  }

  public pageChanged($event: number): void {
    this.currentPage = $event;
    this.currentUnknownWords = this.pagesToWords[this.currentPage];
  }

  public getExplanationForWord(word: string, pos: string) {
    this._experimentService.getExplanationForWord(word, pos)
      .subscribe((explanation: ExplanationModel) => {
        this.currentExplanations.push(explanation);
    });
  }

  public explain($event: WordModel): void {
    // $event = WordModel
    this.getExplanationForWord($event.word, $event.pos);
  }

}
