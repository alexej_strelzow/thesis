import {Component} from "@angular/core";
import {REACTIVE_FORM_DIRECTIVES} from "@angular/forms";
import {Router} from "@angular/router";

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
  directives: [REACTIVE_FORM_DIRECTIVES]
})
export class HomeComponent {

  /**
   * Creates an instance of the HomeComponent with the injected
   *
   * @param {Router} router - The injected Router.
   */
  constructor(private router: Router) {}

  gotoUserModel(e: any): void {
    (<any>$('#user-img'))
      .transition({
        debug     : true,
        animation : 'jiggle',
        duration  : 500,
        interval  : 200
      });
    setTimeout(() => {
      this.router.navigate(['/user']);
    }, 500);
  }

  gotoDocumentModel(e: any): void {
    (<any>$('#document-img'))
      .transition({
        debug     : true,
        animation : 'jiggle',
        duration  : 500,
        interval  : 200
      });
    setTimeout(() => {
      this.router.navigate(['/document']);
    }, 500);
  }

  gotoExperiments(e: any): void {
    (<any>$('#experiment-img'))
      .transition({
        debug     : true,
        animation : 'jiggle',
        duration  : 500,
        interval  : 200
      });
    setTimeout(() => {
      this.router.navigate(['/experiment']);
    }, 500);
  }

}
