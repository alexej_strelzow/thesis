package babelnet.test;

//import it.uniroma1.lcl.babelnet.BabelNet;
//import org.apache.commons.configuration.ConfigurationException;
//import org.apache.commons.configuration.PropertiesConfiguration;

import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.babelnet.BabelSenseSource;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.jlt.util.Language;

import java.io.IOException;
import java.util.List;

/**
 * Created by Alexej Strelzow on 08.10.2014.
 */
public class Test {


    public static void main(String[] args) {
        //babelner.var.properties: babelnet.dir=C:/Users/Besitzer/BabelNet/BabelNet-2.5

        BabelNet bn = BabelNet.getInstance();
        try {

            bn.getSynsets(Language.EN, "gauss");

            bn.getSynsets(Language.EN, "dog", POS.NOUN, BabelSenseSource.WIKI);

            bn.getSynsets(Language.EN, "dog", POS.NOUN, BabelSenseSource.WN);

            bn.getSynsets(Language.EN, "dog", POS.NOUN, BabelSenseSource.WIKIRED);

            bn.getGlosses("bn:00015267n");

            bn.getTranslations(Language.EN, "dog");

            // TODO: also sensekey
            bn.getSenses(Language.EN, "Dog", POS.NOUN, BabelSenseSource.WN);

            final List<BabelSynset> dog = bn.getSynsets(Language.EN, "dog");
            final BabelSynset babelSenses = dog.get(12);
            babelSenses.getGlosses();
            babelSenses.getDBPediaURIs(Language.EN);
            babelSenses.getCategories(Language.EN);
            //bn.getSynsetsFromWikipediaTitle(Language.IT, "Attrezzi", POS.NOUN)
            babelSenses.getDBPediaURIs();
            babelSenses.getRelatedMap();


            babelSenses.getCategories().get(0).getWikipediaURI();

            System.out.println("test");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
