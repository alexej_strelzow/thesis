package at.ac.tuwien.isis.usermodels.rest.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alexej Strelzow on 15.05.2016.
 */
@Data
public class TranslationJson implements Serializable  {

    private String language;

    private List<String> translations;
}
