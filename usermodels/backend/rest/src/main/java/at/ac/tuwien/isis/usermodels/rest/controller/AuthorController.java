package at.ac.tuwien.isis.usermodels.rest.controller;

import at.ac.tuwien.isis.usermodels.persistence.repositories.AuthorRepository;
import at.ac.tuwien.isis.usermodels.service.SecurityContextService;
import at.ac.tuwien.isis.usermodels.persistence.tables.Author;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

//@RestController
//@RequestMapping("/api/authors")
public class AuthorController {
/*
    private final UserRepository userRepository;
    private final AuthorRepository authorRepository;
    private final SecurityContextService securityContextService;

    @Autowired
    public AuthorController(UserRepository userRepository,
                            AuthorRepository authorRepository,
                            SecurityContextService securityContextService) {
        this.userRepository = userRepository;
        this.authorRepository = authorRepository;
        this.securityContextService = securityContextService;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Author get(@PathVariable("id") Long authorId) {
        final Author author = authorRepository.findOne(authorId);

        if (author == null) {

        }

        return author;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Author create(@RequestBody AuthorParams param) {
        //User currentUser = securityContextService.currentUser();

        return authorRepository.save(new Author(param.getFirstName(), param.getLastName()));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        authorRepository.delete(id);
    }

    @Data
    private static class AuthorParams {

        private String firstName;
        private String lastName;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No relationship")
    private class RelationshipNotFoundException extends RuntimeException {
    }
*/
}
