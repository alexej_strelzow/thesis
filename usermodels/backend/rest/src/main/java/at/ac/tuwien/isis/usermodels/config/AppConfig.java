package at.ac.tuwien.isis.usermodels.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = AppConfig.PREFIX)
public class AppConfig {

    public static final String PREFIX = "app";

    private String assetHost;

    public String getAssetHost() {
        return assetHost;
    }
}
