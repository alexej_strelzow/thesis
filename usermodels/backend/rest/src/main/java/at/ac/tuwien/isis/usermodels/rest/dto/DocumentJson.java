package at.ac.tuwien.isis.usermodels.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alexej Strelzow on 07.05.2016.
 */
public class DocumentJson implements Serializable {

    @Getter
    @Setter
    private String language;

    @Getter
    private String title;

    @Getter
    private Long rawDocId;

    @Getter
    private Long id;

    @Getter
    private List<WordJson> words;

    @Getter
    @Setter
    private Integer wordCount;

    @Getter
    @Setter
    private Integer uniqueWordCount;

    @JsonCreator
    public DocumentJson(@JsonProperty("title") String title, @JsonProperty("rawDocId") Long rawDocId,
                        @JsonProperty("id") Long id, @JsonProperty("words") List<WordJson> words) {
        this.title = title;
        this.rawDocId = rawDocId;
        this.id = id;
        this.words = words;
    }

}
