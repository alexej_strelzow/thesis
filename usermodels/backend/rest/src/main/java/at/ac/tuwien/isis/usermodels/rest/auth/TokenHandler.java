package at.ac.tuwien.isis.usermodels.rest.auth;

import at.ac.tuwien.isis.usermodels.persistence.tables.User;
import at.ac.tuwien.isis.usermodels.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class TokenHandler {

    private final String secret;
    private final UserService userService;

    @Autowired
    public TokenHandler(@Value("${app.jwt.secret}") String secret, UserService userService) {
        this.secret = secret;
        this.userService = userService;
    }

    public Optional<UserDetails> parseUserFromToken(String token) {
        String username = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return Optional.ofNullable(userService.loadUserByUsername(username));
    }

    public String createTokenForUser(UserDetails user) {
        return Jwts.builder()
                .setSubject(user.getUsername())
                .claim("userId", ((User) user).getId())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}

