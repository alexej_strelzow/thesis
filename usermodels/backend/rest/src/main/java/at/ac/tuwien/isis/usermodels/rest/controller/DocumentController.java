package at.ac.tuwien.isis.usermodels.rest.controller;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.rest.JsonUtils;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentJson;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentParam;
import at.ac.tuwien.isis.usermodels.rest.dto.RawDocumentJson;
import at.ac.tuwien.isis.usermodels.service.NotPermittedException;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

//import org.springframework.security.access.annotation.Secured;

@Secured("USER")
@RestController
@RequestMapping(value = "/api/documents")
public class DocumentController {

    @Autowired
    private DocumentModelService documentModelService;

    @Autowired
    private FwlService fwlService; //TODO: should not be necessary after TaggedWordWraper gets refactored

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public DocumentJson get(@PathVariable("id") Long documentId) {
        final DocumentModelDTO documentModelDTO = documentModelService.get(documentId);
        return JsonUtils.convertDocumentModelDTO(documentModelDTO, fwlService);
    }

    @RequestMapping(value = "/raw/{id}", method = RequestMethod.GET)
    public RawDocumentJson getRaw(@PathVariable("id") Long documentId) {
        final String content = documentModelService.getRaw(documentId);
        return new RawDocumentJson(content);
    }

    @RequestMapping(value = "/title/{title}", method = RequestMethod.GET)
    public DocumentJson get(@PathVariable("title") String title) {
        final DocumentModelDTO documentModelDTO = documentModelService.get(title);
        return JsonUtils.convertDocumentModelDTO(documentModelDTO, fwlService);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Map<String, DocumentJson> getAllDocuments(){
        Map<String, DocumentJson> response = new LinkedHashMap<>();
        for (DocumentModelDTO documentModelDTO : documentModelService.getAll()) {
            //response.put("model", JsonUtils.convertDocumentModelDTO(documentModelDTO, fwlService));
            response.put("model_" + documentModelDTO.getId(), JsonUtils.convertDocumentModelDTOWithoutWords(documentModelDTO));
        }
        return response;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentJson create(@RequestBody @Valid DocumentParam param) {
        DocumentModelDTO documentModelDTO = null;
        try {
            documentModelDTO = documentModelService.create(param.getTitle(), param.getContent());
        } catch (IOException e) {
            // TODO
            //return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        //return new ResponseEntity(HttpStatus.OK);
        return JsonUtils.convertDocumentModelDTO(documentModelDTO, fwlService);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        documentModelService.delete(id);
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(NotPermittedException.class)
    public void handleNoPermission() {
    }
}
