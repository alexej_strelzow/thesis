package at.ac.tuwien.isis.usermodels.rest.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by Alexej Strelzow on 08.05.2016.
 */
@Data
public class WordExplanationJson extends WordJson {

    private String babelNetId;

    private List<GlossJson> glosses;

    private List<ImageJson> images;

    private List<TranslationJson> translations;

}
