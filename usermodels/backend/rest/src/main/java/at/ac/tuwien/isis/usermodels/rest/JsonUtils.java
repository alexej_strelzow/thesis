package at.ac.tuwien.isis.usermodels.rest;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelGlossDTO;
import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelImageDTO;
import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelNetWordExplanationDTO;
import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelTranslationDTO;
import at.ac.tuwien.isis.usermodels.rest.dto.*;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import edu.stanford.nlp.ling.TaggedWord;
import org.apache.commons.collections4.Bag;

import java.util.*;

/**
 * Created by Alexej Strelzow on 07.05.2016.
 */
public class JsonUtils {

    public static DocumentJson convertDocumentModelDTOWithoutWords(DocumentModelDTO dto) {
        DocumentJson json = new DocumentJson(dto.getTitle().replaceAll("_", " "), dto.getRawDocId(), dto.getId(), new ArrayList<>());
        setDocumentModelJsonProperties(json, dto);
        return json;
    }

    public static DocumentJson convertDocumentModelDTO(DocumentModelDTO dto, FwlService fwlService) {
        DocumentJson json = new DocumentJson(dto.getTitle().replaceAll("_", " "), dto.getRawDocId(), dto.getId(), convertBag(dto.getModel(), fwlService));
        setDocumentModelJsonProperties(json, dto);
        return json;
    }

    private static void setDocumentModelJsonProperties(DocumentJson json, DocumentModelDTO dto) {
        json.setWordCount(dto.getModel().size());
        json.setUniqueWordCount(dto.getModel().uniqueSet().size());
        json.setLanguage("EN");
    }

    public static List<WordJson> convertBag(Bag<TaggedWordWrapper> model) {
        Set<WordJson> treeSet = new TreeSet<>();
        for (TaggedWordWrapper wrapper : model.uniqueSet()) {
            final TaggedWord taggedWord = wrapper.get();
            WordJson word = new WordJson();
            word.setWord(taggedWord.value());
            word.setPos(taggedWord.tag());
            word.setPages(wrapper.getPages());

            treeSet.add(word);
        }
        return new LinkedList<>(treeSet);
    }

    public static List<WordJson> convertBag(Bag<TaggedWordWrapper> model, FwlService fwlService) {
        Set<WordJson> treeSet = new TreeSet<>();
        for (TaggedWordWrapper wrapper : model.uniqueSet()) {
            final TaggedWord taggedWord = wrapper.get();
            WordJson word = new WordJson();
            word.setLemma(fwlService.getLemma(taggedWord.value().toLowerCase()));
            word.setWord(taggedWord.value());
            word.setPos(taggedWord.tag());
            word.setRank(fwlService.getRank(taggedWord.value().toLowerCase(), taggedWord.tag()));
            word.setOccurrences(model.getCount(wrapper));
            word.setPages(wrapper.getPages());

            treeSet.add(word);
        }
        return new LinkedList<>(treeSet);
    }

    public static WordExplanationJson convertWordExplanation(BabelNetWordExplanationDTO explanationDTO,
                                                             boolean includeGloss, boolean includeImage, boolean includeTranslation) {
        WordExplanationJson explanationJson = new WordExplanationJson();
        explanationJson.setBabelNetId(explanationDTO.getSynsetId());
        explanationJson.setWord(explanationDTO.getWord());
        explanationJson.setPos(explanationDTO.getPos());
        //explanationJson.setLanguage(explanationDTO.getLanguage());

        if (includeGloss) {
            List<GlossJson> glosses = new LinkedList<>();
            for (BabelGlossDTO glossDTO : explanationDTO.getGlossDTOs()) {
                GlossJson gloss = new GlossJson();
                gloss.setGloss(glossDTO.getGloss());
                gloss.setLanguage(glossDTO.getSrcLanguage());

                glosses.add(gloss);
            }
            explanationJson.setGlosses(glosses);
        }

        if (includeImage) {
            List<ImageJson> images = new LinkedList<>();
            for (BabelImageDTO imageDTO : explanationDTO.getImageURLs()) {
                ImageJson imageJson = new ImageJson();
                imageJson.setUrl(imageDTO.getImageURL());
                imageJson.setName(imageDTO.getName());

                images.add(imageJson);
            }
            explanationJson.setImages(images);
        }

        if (includeTranslation) {
            List<TranslationJson> translations = new LinkedList<>();
            for (BabelTranslationDTO translationDTO : explanationDTO.getTranslationDTOs()) {
                TranslationJson translation = new TranslationJson();
                translation.setLanguage(translationDTO.getLanguage());
                translation.setTranslations(translationDTO.getTranslations());

                translations.add(translation);
            }
            explanationJson.setTranslations(translations);
        }

        return explanationJson;
    }

    public static UserModelJson emptyUserModelDTO() {
        return new UserModelJson(null, null, null);
    }

    public static WordExplanationJson emptyWordExplanation() {
        return new WordExplanationJson();
    }

    public static ExperimentJson convertExperimentModelDTO(ComparisonResultDTO comparisonResultDTO, FwlService fwlService) {
        // TODO: we have all the data!
        ExperimentJson json = new ExperimentJson(comparisonResultDTO.getExperimentId(), convertBag(comparisonResultDTO.getUnknownWords(), fwlService));
        setExperimentJsonProperties(json, comparisonResultDTO);

        return json;
    }

    public static ExperimentJson convertExperimentModelDTOWithoutWords(ComparisonResultDTO comparisonResultDTO) {
        ExperimentJson json = new ExperimentJson(comparisonResultDTO.getExperimentId(), new ArrayList<>());
        json.setNumWords(comparisonResultDTO.getUnknownWords().size());
        json.setNumTokens(comparisonResultDTO.getUnknownWords().uniqueSet().size());
        setExperimentJsonProperties(json, comparisonResultDTO);

        return json;
    }

    private static void setExperimentJsonProperties(ExperimentJson json, ComparisonResultDTO comparisonResultDTO) {
        json.setMuId(comparisonResultDTO.getUserModelId());
        json.setMdId(comparisonResultDTO.getDocumentModelId());
    }

    public static UserModelJson convertUserModelDTOWithoutWords(UserModelDTO userModelDTO) {
        UserModelJson json = new UserModelJson(userModelDTO.getName(), userModelDTO.getId(), new ArrayList<>());
        setUserModelJsonProperties(json, userModelDTO);

        return json;
    }

    public static UserModelJson convertUserModelDTO(UserModelDTO userModelDTO) {
        UserModelJson json = new UserModelJson(userModelDTO.getName(), userModelDTO.getId(), convertBag(userModelDTO.getModel()));
        setUserModelJsonProperties(json, userModelDTO);

        return json;
    }

    private static void setUserModelJsonProperties(UserModelJson json, UserModelDTO userModelDTO) {
        json.setNumDocs(userModelDTO.getContainingDocs().size());
        json.setNumWords(userModelDTO.getModel().size());
        json.setNumTokens(userModelDTO.getModel().uniqueSet().size());
        json.setInitalTokenSize(userModelDTO.getInitalTokenSize());
        json.setTokenSizeAfterPreprocessing(userModelDTO.getTokenSizeAfterPreprocessing());
        json.setContainingDocs(userModelDTO.getContainingDocs());

        json.setAvgDocLength(userModelDTO.getAvgDocLength());
        json.setAvgRank(userModelDTO.getAvgRank());
        json.setAvgRemovedTokens(userModelDTO.getAvgRemovedTokens());
        json.setAvgRemovedTokensAfterPreprocessing(userModelDTO.getAvgRemovedTokensAfterPreprocessing());
    }
}
