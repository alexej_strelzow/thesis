package at.ac.tuwien.isis.usermodels.rest.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alexej Strelzow on 15.05.2016.
 */
@Data
public class GlossJson implements Serializable  {

    private String gloss;

    private String language;

}
