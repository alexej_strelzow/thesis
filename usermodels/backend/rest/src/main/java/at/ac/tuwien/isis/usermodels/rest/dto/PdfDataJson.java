package at.ac.tuwien.isis.usermodels.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Alexej Strelzow on 21.08.2016.
 */
@Data
public class PdfDataJson implements Serializable {

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String data;

    @JsonCreator
    public PdfDataJson(@JsonProperty("title") String title, @JsonProperty("data") String data) {
        this.title = title;
        this.data = data;
    }
}
