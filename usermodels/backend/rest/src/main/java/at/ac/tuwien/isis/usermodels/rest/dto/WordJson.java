package at.ac.tuwien.isis.usermodels.rest.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 08.05.2016.
 */
@Data
public class WordJson implements Serializable, Comparable<WordJson> {

    //private String language;

    private String word;

    private String lemma;

    private String pos;

    private long rank;

    private int occurrences;

    private Set<Integer> pages;

    @Override
    public String toString() {
        return word + "#" + pos;
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public int compareTo(WordJson o) {
        return toString().compareTo(o.toString());
    }

}
