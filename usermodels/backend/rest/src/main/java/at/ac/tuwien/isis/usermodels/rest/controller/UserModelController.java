package at.ac.tuwien.isis.usermodels.rest.controller;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.rest.JsonUtils;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentParam;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentJson;
import at.ac.tuwien.isis.usermodels.rest.dto.UserModelJson;
import at.ac.tuwien.isis.usermodels.rest.dto.WordJson;
import at.ac.tuwien.isis.usermodels.service.NotPermittedException;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.UserModelService;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
//import org.springframework.security.access.annotation.Secured;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@Secured("USER")
@RestController
@RequestMapping(value = "/api/usermodel")
public class UserModelController {

    //@Autowired
    //private UserModelRepository userModelRepository;

    @Autowired
    private UserModelService userModelService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public UserModelJson get(@PathVariable("id") Long userModelId) {
        final UserModelDTO userModelDTO = userModelService.get(userModelId);

        if (userModelDTO == null) {
            return JsonUtils.emptyUserModelDTO();
            //return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return JsonUtils.convertUserModelDTO(userModelDTO);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Map<String, UserModelJson> getAll(){
        Map<String, UserModelJson> response = new LinkedHashMap<>();
        for (UserModelDTO userModelDTO : userModelService.getAll()) {
            response.put("model_" + userModelDTO.getId(), JsonUtils.convertUserModelDTOWithoutWords(userModelDTO));
        }
        return response;
    }





    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public UserModelJson get() {
        List<WordJson> twr = new LinkedList<>();
        twr.add(new WordJson());
        UserModelJson testJson = new UserModelJson("test model", 666L, twr);
        testJson.setNumDocs(25);
        testJson.setNumWords(4000);
        testJson.setNumTokens(1000);
        return testJson;
    }
/*
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public UserModelJson get(@PathVariable("id") Long userModelId) {
        final UserModel userModel = userModelRepository.findOne(userModelId);

        if (userModel == null) {
            return JsonUtils.emptyUserModelDTO();
            //return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        final UserModelDTO userModelDTO = ModelUtils.convertUserModel(userModel);
        return JsonUtils.convertUserModelDTO(userModelDTO);
    }
*/
    @RequestMapping(value = "/add", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserModelJson create(@RequestBody @Valid DocumentParam param) {
        UserModelDTO userModelDTO = null;
        /*
        try {
            userModelDTO = userModelService.create(param.getTitle(), param.getContent());
        } catch (IOException e) {
            // TODO
        }
        */
        return JsonUtils.convertUserModelDTO(userModelDTO);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        userModelService.delete(id);
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(NotPermittedException.class)
    public void handleNoPermission() {
    }
}
