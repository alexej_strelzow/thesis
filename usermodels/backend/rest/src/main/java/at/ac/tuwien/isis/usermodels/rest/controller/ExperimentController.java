package at.ac.tuwien.isis.usermodels.rest.controller;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.rest.JsonUtils;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentJson;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentParam;
import at.ac.tuwien.isis.usermodels.rest.dto.ExperimentJson;
import at.ac.tuwien.isis.usermodels.rest.dto.RawDocumentJson;
import at.ac.tuwien.isis.usermodels.service.NotPermittedException;
import at.ac.tuwien.isis.usermodels.service.experiment.ExperimentService;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

//import org.springframework.security.access.annotation.Secured;

@Secured("USER")
@RestController
@RequestMapping(value = "/api/experiments")
public class ExperimentController {

    @Autowired
    private ExperimentService experimentService;

    @Autowired
    private FwlService fwlService; //TODO: should not be necessary after TaggedWordWraper gets refactored

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ExperimentJson get(@PathVariable("id") Long experimentId) {
        final ComparisonResultDTO comparisonResultDTO = experimentService.get(experimentId);
        return JsonUtils.convertExperimentModelDTO(comparisonResultDTO, fwlService);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Map<String, ExperimentJson> getAllExperiments() {
        Map<String, ExperimentJson> response = new LinkedHashMap<>();
        for (ComparisonResultDTO comparisonResultDTO : experimentService.getAll()) { // TODO: which user and document model
            response.put("model_" + comparisonResultDTO.getExperimentId(), JsonUtils.convertExperimentModelDTOWithoutWords(comparisonResultDTO));
        }
        return response;
    }

    @RequestMapping(value = "/perform/model/{userId}/document/{docId}", method = RequestMethod.GET)
    public ExperimentJson perform(@PathVariable("userId") Long userModelId, @PathVariable("docId") Long documentModelId) {
        try {
            final ComparisonResultDTO comparisonResultDTO = experimentService.run(userModelId, documentModelId, null);
            return JsonUtils.convertExperimentModelDTO(comparisonResultDTO, fwlService);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
