package at.ac.tuwien.isis.usermodels.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 07.05.2016.
 */
public class UserModelJson implements Serializable {

    // TODO: extends ResponseEntity<UserModelJson>

    @Getter
    private String name;

    @Getter
    private Long id;

    @Getter
    private List<WordJson> words;

    @Getter
    @Setter
    int numDocs;

    @Getter
    @Setter
    int numWords;

    @Getter
    @Setter
    int numTokens;

    @Getter
    @Setter
    private double initalTokenSize;

    @Getter
    @Setter
    private double tokenSizeAfterPreprocessing;

    @Getter
    @Setter
    private double avgRemovedTokens;

    @Getter
    @Setter
    private double avgRemovedTokensAfterPreprocessing;

    @Getter
    @Setter
    private Set<String> containingDocs;

    @Getter
    @Setter
    private double avgDocLength;

    @Getter
    @Setter
    private double avgRank;

    @JsonCreator
    public UserModelJson(@JsonProperty("name") String name, @JsonProperty("id") Long id,
                         @JsonProperty("words") List<WordJson> words) {
        this.name = name;
        this.id = id;
        this.words = words;
    }

}
