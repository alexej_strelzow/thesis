package at.ac.tuwien.isis.usermodels.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alexej Strelzow on 07.05.2016.
 */
public class RawDocumentJson implements Serializable {

    @Getter
    private String content;

    @JsonCreator
    public RawDocumentJson(@JsonProperty("content") String content) {
        this.content = content;
    }

}
