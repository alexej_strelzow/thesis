package at.ac.tuwien.isis.usermodels.rest.controller;

import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelNetWordDTO;
import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelNetWordExplanationDTO;
import at.ac.tuwien.isis.usermodels.rest.JsonUtils;
import at.ac.tuwien.isis.usermodels.rest.dto.WordExplanationJson;
import at.ac.tuwien.isis.usermodels.service.NotPermittedException;
import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetService;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.exception.ServiceException;
import at.ac.tuwien.isis.usermodels.service.utils.BabelNetUtils;
import edu.mit.jwi.item.POS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

//import org.springframework.security.access.annotation.Secured;

@Secured("USER")
@RestController
@RequestMapping(value = "/api/babelnet")
public class BabelNetController {

    @Autowired
    private BabelNetService babelNetService;

    @RequestMapping(value = "/{word}/{pos}", method = RequestMethod.GET)
    public ResponseEntity<WordExplanationJson> get(@PathVariable("word") String word, @PathVariable("pos") String pos) {
        final POS convertedPOS = BabelNetUtils.getPosForTag(pos);
        BabelNetWordDTO wordDTO = new BabelNetWordDTO(word, convertedPOS == null ? null : convertedPOS.name());

        final BabelNetConfiguration configuration = babelNetService.getConfiguration();
        final String explainDefaultSrcLanguage = configuration.getExplainDefaultSrcLanguage();
        if (explainDefaultSrcLanguage != null) {
            wordDTO.setSrcLanguage(explainDefaultSrcLanguage);
        }

        final String[] explainDefaultDestLanguages = configuration.getExplainDefaultDestLanguages();
        if (explainDefaultDestLanguages != null) {
            wordDTO.setDestLanguages(Arrays.asList(explainDefaultDestLanguages));
        }

        try {
            final BabelNetWordExplanationDTO explanationDTO = babelNetService.explain(wordDTO);
            return ResponseEntity.ok(JsonUtils.convertWordExplanation(explanationDTO, configuration.isExplainIncludeGloss(),
                    configuration.isExplainIncludeImage(), configuration.isExplainIncludeTranslation()));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /*
    @RequestMapping(method = RequestMethod.GET)
    public Map<String, Object> getAllDocuments(){
        final List<DocumentModel> all = documentModelRepository.findAll();
        for (DocumentModel document : all) {
            DocumentModelDTO documentModelDTO = ModelUtils.convertDocumentModel(document);
            JsonUtils.convertDocumentModelDTO(documentModelDTO);
        }
        return null;
    }
    */

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(NotPermittedException.class)
    public void handleNoPermission() {
    }
}
