package at.ac.tuwien.isis.usermodels.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 07.05.2016.
 */
public class ExperimentJson implements Serializable {

    // TODO: extends ResponseEntity<UserModelJson>

    @Getter
    private Long id;

    @Getter
    @Setter
    private Long muId; // user model Id

    @Getter
    @Setter
    private Long mdId; // document model id

    @Getter
    private List<WordJson> words;

    @Getter
    @Setter
    int numWords;

    @Getter
    @Setter
    int numTokens;


    @JsonCreator
    public ExperimentJson(@JsonProperty("id") Long id, @JsonProperty("words") List<WordJson> words) {
        this.id = id;
        this.words = words;
    }

}
