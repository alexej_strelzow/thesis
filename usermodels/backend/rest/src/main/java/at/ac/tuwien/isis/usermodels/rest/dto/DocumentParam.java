package at.ac.tuwien.isis.usermodels.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Value;

/**
 * Created by Alexej Strelzow on 08.05.2016.
 */
@Data
@Value
public class DocumentParam {
    //private Long authorId;
    //private String dblpId;
    private String title;
    //private String doi;
    //private String ee;
    //private String contentType;
    private String content;

    @JsonCreator
    public DocumentParam(@JsonProperty("title") String title, @JsonProperty("content") String content) {
        this.title = title;
        this.content = content;
    }

}
