package at.ac.tuwien.isis.usermodels.rest.controller;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.rest.JsonUtils;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentJson;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by Alexej Strelzow on 21.08.2016.
 */

@Secured("USER")
@RestController
@RequestMapping(value = "/api/files")
public class FileController {

    public static final String ROOT = System.getProperty("user.dir") + "/rest/upload-dir";

    private final ResourceLoader resourceLoader;

    @Autowired
    private DocumentModelService documentModelService;

    @Autowired
    private FwlService fwlService; //TODO: should not be necessary after TaggedWordWraper gets refactored

    @Autowired
    public FileController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @RequestMapping(method = RequestMethod.POST, headers = "content-type=multipart/form-data", value = "/upload")
    public DocumentJson handleFileUpload(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                //Files.copy(file.getInputStream(), Paths.get(ROOT, file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);

                File tmp = new File(ROOT, file.getOriginalFilename());
                file.transferTo(tmp);
                final DocumentModelDTO documentModelDTO = documentModelService.create(tmp);
                return JsonUtils.convertDocumentModelDTO(documentModelDTO, fwlService);
            } catch (IOException | RuntimeException e) {
                e.printStackTrace();
            }
        }
        return null;//ResponseEntity.unprocessableEntity();
    }

    @RequestMapping(method = RequestMethod.POST, headers = "content-type=application/json", value = "/upload/file")
    public DocumentJson handleFileUpload(@RequestBody String file) {
        if (!file.isEmpty()) {
            try {
                JSONObject jsonFile = new JSONObject(file);
                final String fileName = jsonFile.getString("name");
                final String base64Content = jsonFile.getString("content");

                final byte[] bytes = Base64Utils.decodeFromString(base64Content);
                File tmp = new File(ROOT, fileName);
                FileUtils.writeByteArrayToFile(tmp, bytes);

                final DocumentModelDTO documentModelDTO = documentModelService.create(tmp);
                return JsonUtils.convertDocumentModelDTO(documentModelDTO, fwlService);
            } catch (IOException | RuntimeException e) {
                e.printStackTrace();
            }
        }
        return null;//ResponseEntity.unprocessableEntity();
    }



    @RequestMapping(method = RequestMethod.GET, value = "/download/id/{id}")
    @ResponseBody
    public ResponseEntity<?> getBase64EncodedFileById(@PathVariable int id) {
        try {
            return null;
            //return ResponseEntity.ok(resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString()));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/download/title/{filename:.+}")
    @ResponseBody
    public ResponseEntity<String> getBase64EncodedFileByTitle(@PathVariable String filename) {
        try {
            final Resource resource = resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString());
            final byte[] bytes = IOUtils.toByteArray(resource.getURI());
            String base64File = Base64Utils.encodeToString(bytes);

            return ResponseEntity.ok(base64File);
            //return new PdfDataJson(filename, base64File);
        } catch (Exception e) {
            return null;
        }
    }
}

/*
@RequestMapping(method = RequestMethod.GET, value = "/{filename:.+}")
    @ResponseBody
    public ResponseEntity<?> getFile(@PathVariable String filename) {
        try {
            HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "pdf"));

            final Resource resource = resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString());
            return new ResponseEntity(resource, header, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/bytes/{filename:.+}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getFileAsBytes(@PathVariable String filename) throws IOException {

        final Resource resource = resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString());
        final byte[] bytes = IOUtils.toByteArray(resource.getURI());

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "pdf"));
        header.set("Content-Disposition", "inline; filename=" + filename);
        header.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        header.setContentLength(bytes.length);

        return new ResponseEntity<byte[]>(bytes, header, HttpStatus.OK);
    }

    @RequestMapping(value="/getpdf/{filename:.+}", method=RequestMethod.GET)
    public ResponseEntity<byte[]> getPDF(@PathVariable String filename) {

        try {
            final Resource resource = resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString());
            final byte[] bytes = IOUtils.toByteArray(resource.getURI());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

            return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
 */