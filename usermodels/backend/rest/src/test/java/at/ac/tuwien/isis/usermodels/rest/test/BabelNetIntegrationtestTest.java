package at.ac.tuwien.isis.usermodels.rest.test;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelNetWordDTO;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentParam;
import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetService;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.BabelNet;
import org.apache.commons.httpclient.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;

/**
 * Created by Alexej Strelzow on 08.03.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@org.springframework.boot.test.IntegrationTest("server.port:0")

@ActiveProfiles("test")
public class BabelNetIntegrationtestTest {

    private static final String DOCUMENTS_RESOURCE = "/api/babelnet";

    @Value("${local.server.port}")
    int port;

    @Autowired
    private BabelNetService babelNetService;

    //Test RestTemplate to invoke the APIs.
    private RestTemplate restTemplate = new TestRestTemplate();

    @BeforeClass
    public static void setUp() {
        BabelNet.getInstance();
    }

    @Before
    public void before() {
        RestAssured.port = port;
    }

    @Test
    public void contextLoads() {
        Assert.assertNotNull(babelNetService);
    }

    @Test
    public void testWithRestTemplate() {
        // TODO: http://www.javabeat.net/spring-boot-testing/
    }

    @Test
    public void testGet_shouldWork() {
        when()
            .get(DOCUMENTS_RESOURCE + "/dog#NOUN")
        .then()
            .statusCode(HttpStatus.SC_OK)
            .body("word", Matchers.is("dog"))
            .body("pos", Matchers.is("NOUN"));
    }

}
