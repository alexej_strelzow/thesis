package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.SourceType;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.Utils;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import org.apache.commons.collections4.Bag;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Alexej Strelzow on 05.05.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
public class DocumentModelIntegrationTest {

    @Autowired
    private DocumentModelService documentModelService;

    @Autowired
    private DocumentModelRepository documentRepository;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(documentModelService);
        Assert.assertNotNull(documentRepository);
        Assert.assertNotNull(rawDocumentRepository);
    }

    @Test
    public void testDocumentPersists_shouldPersist() throws IOException, ClassNotFoundException {
        String title = "test";
        String text = "Hello, how are you?";

        try {
            final DocumentModelDTO modelDTO = documentModelService.create(title, text);

            final RawDocument theOne = rawDocumentRepository.findOne(modelDTO.getRawDocId());

            Assert.assertEquals(title, theOne.getTitle());
            Assert.assertEquals(SourceType.TXT.name(), theOne.getContentType());
            Assert.assertEquals(text, Utils.stringFromBytes(theOne.getContent()));

            final DocumentModel doc = documentRepository.findOne(modelDTO.getId());

            Assert.assertNotNull(doc.getDocument());
            Assert.assertNotEquals(Utils.stringFromBytes(doc.getContent()), text);
            final Bag<TaggedWordWrapper> model = ModelUtils.convert(doc.getContent());
            Assert.assertEquals(model.size(), modelDTO.getModel().size());

        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
