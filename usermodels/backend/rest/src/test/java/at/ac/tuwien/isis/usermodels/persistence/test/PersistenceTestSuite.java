package at.ac.tuwien.isis.usermodels.persistence.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Alexej Strelzow on 06.05.2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        DocumentModelPersistenceTest.class,
        UserModelPersistenceTest.class,
        ExperimentPersistenceTest.class
})
public class PersistenceTestSuite {
}
