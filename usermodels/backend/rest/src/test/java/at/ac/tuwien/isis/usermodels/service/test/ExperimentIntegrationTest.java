package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.*;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.ExperimentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.Experiment;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.service.experiment.ExperimentService;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.UserModelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 05.05.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
public class ExperimentIntegrationTest {

    @Autowired
    private DocumentModelService documentModelService;

    @Autowired
    private UserModelService userModelService;

    @Autowired
    private ExperimentService experimentService;

    @Autowired
    private ExperimentRepository experimentRepository;

    @Autowired
    private DocumentModelRepository documentRepository;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Autowired
    private UserModelRepository userModelRepository;

    @Before
    public void init() {
        experimentRepository.deleteAll();
    }

    @Test
    public void contextLoads() {
        Assert.assertNotNull(documentModelService);
        Assert.assertNotNull(userModelRepository);
        Assert.assertNotNull(experimentService);
        Assert.assertNotNull(experimentRepository);
        Assert.assertNotNull(rawDocumentRepository);
        Assert.assertNotNull(documentRepository);
        Assert.assertNotNull(userModelRepository);
    }

    @Test
    public void testExperiment_shouldPersist() throws IOException {
        Set<RawDocument> documents = new HashSet<>();
        String title = "title";
        String text = getText();

        Map<String, String> articles = new HashMap();
        for (int i = 0; i < 3; i++) {
            String t = title + i;
            RawDocument rawDocument = new RawDocument(t, SourceType.TXT.name(), Utils.stringToBytes(text));
            documents.add(rawDocumentRepository.save(rawDocument));

            articles.put(title + i, text + i);
        }

        final UserModelDTO userModelDTO = userModelService.createFromArticles("test", articles);
        final DocumentModelDTO documentModelDTO = documentModelService.create(title, text);

        final ComparisonResultDTO diff = experimentService.run(userModelDTO, documentModelDTO, null);

        final Experiment one = experimentRepository.findOne(diff.getExperimentId());
        Assert.assertNotNull(one);
    }

    private static String getText() {
        return "The XOR-satisfiability (XORSAT) problem requires finding an\n" +
                "assignment of n Boolean variables that satisfym exclusive OR (XOR)\n" +
                "clauses, whereby each clause constrains a subset of the variables.\n" +
                "We consider random XORSAT instances, drawn uniformly at ran-\n" +
                "dom from the ensemble of formulae containing n variables and m\n" +
                "clauses of size k. This model presents several structural similarities\n" +
                "to other ensembles of constraint satisfaction problems, such as k-\n" +
                "satisfiability (k-SAT), hypergraph bicoloring and graph coloring. For\n" +
                "many of these ensembles, as the number of constraints per variable\n" +
                "grows, the set of solutions shatters into an exponential number of\n" +
                "well-separated components. This phenomenon appears to be related\n" +
                "to the difficulty of solving random instances of such problems.\n" +
                "\n" +
                "We prove a complete characterization of this clustering phase tran-\n" +
                "sition for random k-XORSAT. In particular, we prove that the clus-\n" +
                "tering threshold is sharp and determine its exact location. We prove\n" +
                "that the set of solutions has large conductance below this threshold\n" +
                "and that each of the clusters has large conductance above the same\n" +
                "threshold.\n" +
                "\n" +
                "Our proof constructs a very sparse basis for the set of solutions (or\n" +
                "the subset within a cluster). This construction is intimately tied to\n" +
                "the construction of specific subgraphs of the hypergraph associated\n" +
                "with an instance of k-XORSAT. In order to study such subgraphs,\n" +
                "we establish novel local weak convergence results for them." +
                "" +
                "Theorem 1. Let k > 2 be fixed. For n/m = ? and m??,\n" +
                "(a) if ? > 1 then a random formula from ?km,n is unsatisfiable with high probability.\n" +
                "(b) if ? < 1 then a random formula from ?km,n is satisfiable with high probability.\n" +
                "\n" +
                "The proof of Theorem 1 in Section 3 of [10] uses a first moment method argument for the simple direction\n" +
                "(part (a)). Part (b) is significantly more complicated, and is based on the second moment method. Essentially\n" +
                "the same problem has also arisen in coding theoretic settings; analysis and techniques can be found in for\n" +
                "example [21]. It has been suggested by various readers of earlier drafts of this paper that previous proofs of\n" +
                "Theorem 1 have been insufficiently complete, particularly for k > 3. We therefore provide a detailed proof\n" +
                "in Appendix C for completeness.\n" +
                "\n" +
                "We have shown that the edge density is concentrated around a specific value depending on the initial\n" +
                "ratio c of hyperedges (equations) to nodes (variables). Let ck,2 be the value of c such that the resulting\n" +
                "edge density is concentrated around 1. Then Proposition 3 and Theorem 1 together with the preceding\n" +
                "consideration implies:\n" +
                "\n" +
                "Corollary 1. Let k > 2 and consider ?km,n. The satisfiability threshold with respect to the edge density\n" +
                "c = n/m is ck,2.\n" +
                "\n" +
                "Again, up to this point, everything we have stated was known from previous work. We now provide the\n" +
                "connection to cuckoo hashing, to show that we obtain the same threshold values for the success of cuckoo\n" +
                "hashing. That is, we argue the following:\n" +
                "\n" +
                "Theorem 2. For k > 2, ck,2 is the threshold for k-ary cuckoo hashing to work. That is, and with n keys to\n" +
                "be stored and m buckets, with c = n/m fixed and m??,\n" +
                "(a) if c > ck,2, then k-ary cuckoo hashing does not work with high probability.\n" +
                "(b) if c < ck,2, then k-ary cuckoo hashing works with high probability.\n" +
                "\n" +
                "Proof : Assume a set of n keys S is given, and for each x ? S a random set Ax ? {1, . . . ,m} of size k of\n" +
                "possible buckets is chosen.\n" +
                "\n" +
                "To prove part (a), note that the setsAx for x ? S can be represented by a random hypergraph from Gkm,n.\n" +
                "If n/m = c > ck,2 and m??, then with high probability the edge density in the 2-core is greater than 1.\n" +
                "The hyperedges in the 2-core correspond to a set of keys, and the nodes in the 2-core to the buckets available\n" +
                "for these keys. Obviously, then, cuckoo hashing does not work.\n" +
                "\n" +
                "To prove part (b), consider the case where n/m = c < ck,2 and m ? ?. Picking for each x a\n" +
                "random bx ? {0, 1}, the sets Ax, x ? S, induce a random system of equations from ?km,n. Specifically,\n" +
                "Ax = {j1, . . . , jk} induces the equation xj1 + � � �+ xjk = bx.\n" +
                "\n" +
                "By Corollary 1 a random system of equations from ?km,n is satisfiable with high probability. This implies\n" +
                "that the the matrix M made up from the left-hand sides of these equations consists of linearly independent\n" +
                "rows with high probability. This is because a given set of left-hand sides with dependent rows is only satis-\n" +
                "fiable with probability at most 1/2 when we pick the bx at random.\n" +
                "\n" +
                "Therefore we have an n � n-submatrix in M with a nonzero determinant. The expansion of the de-\n" +
                "terminant of this submatrix as a sum of products by the Leibniz formula must contain a product with all\n" +
                "factors being variables xij (as opposed to 0). This product term corresponds to a permutation mapping keys\n" +
                "to buckets, showing that cuckoo hashing is indeed possible. ?\n" +
                "\n" +
                "We make some additional remarks. We note that the idea of using the rank of the key-bucket matrix to\n" +
                "obtain lower bounds on the cuckoo hashing threshold is not new either; it appears in [11]. There the authors";
    }
}
