package at.ac.tuwien.isis.usermodels.generator;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.*;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.ExperimentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.Experiment;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.service.experiment.ExperimentService;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.UserModelService;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alexej Strelzow on 30.07.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("dev")public class DataGenerator {

    // -Xmx6144M -d64

    FileFilter filter = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            return pathname.getName().endsWith(".txt");
        }
    };

    @Autowired
    private DocumentModelService documentModelService;

    @Autowired
    private UserModelService userModelService;

    @Autowired
    private ExperimentService experimentService;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Autowired
    private DocumentModelRepository documentRepository;

    @Autowired
    private UserModelRepository userModelRepository;

    @Autowired
    private ExperimentRepository experimentRepository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(userModelRepository);
        Assert.assertNotNull(rawDocumentRepository);
        Assert.assertNotNull(documentRepository);
        Assert.assertNotNull(experimentRepository);
    }

    private void deleteAll() {
        experimentRepository.deleteAll();
        userModelRepository.deleteAll();
        documentRepository.deleteAll();
        rawDocumentRepository.deleteAll();
    }

    @Test
    public void popuplateAll(){
        deleteAll();

        try {
            popuplateDocuments();
        } catch (IOException e) {
            System.err.println("Error - popuplateRawDocuments " + e.getLocalizedMessage());
        }

        try {
            popuplateMedicalDocuments();
        } catch (IOException e) {
            System.err.println("Error - popuplateMedicalDocuments " + e.getLocalizedMessage());
        }

        try {
            populateUserModels();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error - populateUserModels " + e.getLocalizedMessage());
        }

        try {
            populateExperiments();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error - populateExperiments " + e.getLocalizedMessage());
        }
    }

    @Test
    public void popuplateDocuments() throws IOException {
        final int AMOUNT_EACH_AUTHOR = -1; // -1 == all

        File dir = new File("C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\txt\\");

        if (dir.exists()) {
            for (File authorDir : dir.listFiles()) {
                if (!authorDir.isDirectory()) {
                    continue;
                }

                int counter = 0;
                for (File f : authorDir.listFiles(filter)) {
                    if (AMOUNT_EACH_AUTHOR != -1 && counter == AMOUNT_EACH_AUTHOR) {
                        break;
                    }

                    String title = f.getName();
                    title = title.substring(0, title.length()-".txt".length());

                    documentModelService.create(title, StringUtils.join(Files.readAllLines(f.toPath(),
                            Charset.forName("ISO-8859-1")).toArray(), "\r\n"));
                    counter++;
                }
            }
        }
    }

    @Test
    public void popuplateMedicalDocuments() throws IOException {
        File dir = new File("C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\txt_med\\");

        if (dir.isDirectory()) {
            for (File f : dir.listFiles(filter)) {
                String title = f.getName();
                title = title.substring(0, title.length()-".txt".length()); // TODO: give category field -> db to distinguish -> DDLB

                documentModelService.create(title, StringUtils.join(Files.readAllLines(f.toPath(),
                        Charset.forName("ISO-8859-1")).toArray(), "\r\n"));
            }
        }
    }

    @Test
    public void populateUserModels() throws IOException, ClassNotFoundException {
        String txtDir = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\user_models\\";
        File dir = new File(txtDir);

        if (dir.isDirectory()) {
            for (File authorDir : dir.listFiles()) {
                if (!authorDir.isDirectory()) {
                    continue;
                }

                final File model = authorDir.listFiles()[0];
                ObjectInputStream is = new ObjectInputStream(new FileInputStream(model));
                UserModelDTO userModelDTO = (UserModelDTO) is.readObject();
                is.close();

                Set<String> ppTitles = new TreeSet<>();

                for (String title : userModelDTO.getContainingDocs()) {
                    ppTitles.add(title.substring(0, title.length() - ".txt".length()));
                }

                //final Set<RawDocument> rawDocumentsForModel = getRawDocumentsForModel(userModelDTO);
                final Set<RawDocument> rawDocumentsForModel = rawDocumentRepository.getDocumentsForTitles(ppTitles);
                final byte[] content = ModelUtils.convert(userModelDTO.getModel());

                UserModel userModel = new UserModel(authorDir.getName(), rawDocumentsForModel, content);
                userModel.setAvgDocLength(userModelDTO.getAvgDocLength());
                userModel.setAvgRemovedTokens(userModelDTO.getAvgRemovedTokens());
                userModel.setAvgRank(userModelDTO.getAvgRank());
                userModel.setAvgRemovedTokensAfterPreprocessing(userModelDTO.getAvgRemovedTokensAfterPreprocessing());
                userModel.setInitalTokenSize(userModelDTO.getInitalTokenSize());
                userModel.setTokenSizeAfterPreprocessing(userModelDTO.getTokenSizeAfterPreprocessing());

                userModelRepository.save(userModel);
            }
        }
    }

    private Set<String> getMedicalDocuments() {
        File dir = new File("C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\txt_med\\");
        Set<String> set = new HashSet<>();
        if (dir.isDirectory()) {
            for (File f : dir.listFiles(filter)) {
                String title = f.getName();
                title = title.substring(0, title.length()-".txt".length());
                set.add(title);
            }
        }
        return set;
    }

    @Test
    public void populateExperiments() throws IOException, ClassNotFoundException {
        // 1) collect documents
        final Set<DocumentModel> medicalDocuments = documentRepository.getDocumentsForTitles(getMedicalDocuments());
        Map<String, Set<DocumentModel>> randomDocsFromAuthor = new LinkedHashMap<>();
        for (UserModel userModel : userModelRepository.findAll()) {
            final Set<DocumentModel> randomDocs = getRandomDocs(userModel, 2);

            Assert.assertTrue(randomDocs.size() == 2);

            randomDocsFromAuthor.put(userModel.getName(), randomDocs);
        }

        Assert.assertTrue(randomDocsFromAuthor.keySet().size() == 10);

        // 2) execute
        for (UserModel userModel : userModelRepository.findAll()) {
            // equi-domain
            for (String author : randomDocsFromAuthor.keySet()) {
                if (!userModel.getName().equals(author)) {
                    for (DocumentModel documentModel : randomDocsFromAuthor.get(author)) {
                        experimentService.run(ModelUtils.convertUserModel(userModel), ModelUtils.convertDocumentModel(documentModel), null);
                    }
                }
            }
            // cross-domain
            for (DocumentModel documentModel : medicalDocuments) {
                experimentService.run(ModelUtils.convertUserModel(userModel), ModelUtils.convertDocumentModel(documentModel), null);
            }
        }
    }

    private Set<DocumentModel> getRandomDocs(UserModel userModel, int amount) {
        Set<DocumentModel> returnSet = new LinkedHashSet<>();
        final Set<RawDocument> documents = userModel.getDocuments();
        if (amount <= 0 || amount > documents.size()-1) {
            return null;
        }
        final RawDocument[] docs = documents.toArray(new RawDocument[documents.size()]);
        for (Integer randomIndex : getRandomIndizes(amount, docs.length)) {
            final RawDocument doc = docs[randomIndex];
            final DocumentModel model = documentRepository.getByName(doc.getTitle());
            if (model != null) {
                returnSet.add(model);
            }
        }
        return returnSet;
    }

    private Set<Integer> getRandomIndizes(int amount, int upperBound) {
        Set<Integer> randomNumbers = new TreeSet<>();
        while (randomNumbers.size() != amount) {
            final int randomIndex = ThreadLocalRandom.current().nextInt(0, upperBound);
            if (!randomNumbers.contains(randomIndex)) {
                randomNumbers.add(randomIndex);
            }
        }
        return randomNumbers;
    }

    @Test
    public void populateExperiments2() throws IOException, ClassNotFoundException {

        String usrModelDirPath = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\user_models\\";
        File usrModelDir = new File(usrModelDirPath);
        String docModelDirPath = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\doc_models\\medicine\\";
        File docModelDir = new File(docModelDirPath);

        if (!docModelDir.exists()) {
            System.err.println("Directory does not exist: " + docModelDirPath);
            return;
        }

        if (!usrModelDir.exists()) {
            System.err.println("Directory does not exist: " + usrModelDirPath);
            return;
        }

        DocumentModelDTO documentModelDTO;
        UserModelDTO userModelDTO;

        for (File docModel : docModelDir.listFiles()) {
            documentModelDTO = getDocumentModelDTO(docModel);

            for (File authorDir : usrModelDir.listFiles()) {
                if (!authorDir.isDirectory()) {
                    continue;
                }

                for (File usrModel : authorDir.listFiles()) {
                    userModelDTO = getUserModelDTO(usrModel);

                    final Long umID = userModelDTO.getId();
                    final Long dmID = documentModelDTO.getId();

                    experimentService.run(userModelDTO, documentModelDTO, null);

                    /*final ComparisonResultDTO result =

                    // TODO: write converter for DTO to Table
                    UserModel userModel = userModelRepository.findOne(userModelDTO.getId());
                    DocumentModel documentModel = documentRepository.findOne(documentModelDTO.getId());

                    // TODO: store more from the DTO
                    Experiment experiment = new Experiment(userModel, documentModel, ModelUtils.convert(result.getUnknownWords()));

                    experimentRepository.save(experiment);*/
                }
            }
        }
    }

    private DocumentModelDTO getDocumentModelDTO(File docModel) throws IOException, ClassNotFoundException {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(docModel));
        DocumentModelDTO documentModelDTO = (DocumentModelDTO) is.readObject();
        is.close();
        return documentModelDTO;
    }

    private UserModelDTO getUserModelDTO(File usrModel) throws IOException, ClassNotFoundException {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(usrModel));
        UserModelDTO usrModelDTO = (UserModelDTO) is.readObject();
        is.close();
        return usrModelDTO;
    }
}
