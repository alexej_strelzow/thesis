package at.ac.tuwien.isis.usermodels.persistence.test;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.SourceType;
import at.ac.tuwien.isis.usermodels.dto.Utils;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.ExperimentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.Experiment;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 05.05.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
public class ExperimentPersistenceTest {

    @Autowired
    private ExperimentRepository experimentRepository;

    @Autowired
    private DocumentModelRepository documentRepository;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Autowired
    private UserModelRepository userModelRepository;

    @Before
    public void init() {
        experimentRepository.deleteAll();
    }

    @Test
    public void contextLoads() {
        Assert.assertNotNull(experimentRepository);
        Assert.assertNotNull(rawDocumentRepository);
        Assert.assertNotNull(documentRepository);
        Assert.assertNotNull(userModelRepository);
    }

    @Test
    public void testExperiment_shouldPersist() throws IOException {
        Set<RawDocument> documents = new HashSet<>();
        String title = "title";
        String text = "text";
        String result = "some random content";

        for (int i = 0; i < 10; i++) {
            String t = title + i;
            RawDocument rawDocument = new RawDocument(t, SourceType.TXT.name(), Utils.stringToBytes(text));
            documents.add(rawDocumentRepository.save(rawDocument));
        }

        UserModel userModel = new UserModel("testTitle", documents, Utils.stringToBytes(text));
        userModel = userModelRepository.save(userModel);

        RawDocument rawDocument = new RawDocument(title, SourceType.TXT.name(), Utils.stringToBytes(text));
        DocumentModel documentModel = new DocumentModel(rawDocument, "none", Utils.stringToBytes(text));
        documentRepository.save(documentModel);

        Experiment experiment = new Experiment(userModel, documentModel, Utils.stringToBytes(result));
        experiment = experimentRepository.save(experiment);

        final Experiment one = experimentRepository.findOne(experiment.getId());
        Assert.assertNotNull(one);

        Assert.assertEquals(Utils.stringFromBytes(one.getResult()), result);
    }

    private RawDocument getRawDocument() {
        String text = "Hello, how are you?";
        byte[] content = Utils.stringToBytes(text);
        return new RawDocument("test", SourceType.TXT.name(), content);
    }

}
