package at.ac.tuwien.isis.usermodels.rest.test;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.rest.dto.DocumentParam;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.apache.commons.httpclient.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.jayway.restassured.RestAssured.*;

/**
 * Created by Alexej Strelzow on 08.03.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@org.springframework.boot.test.IntegrationTest("server.port:0")

@ActiveProfiles("test")
public class DocumentIntegrationtestTest {

    private static final String DOCUMENTS_RESOURCE = "/api/documents/";

    @Value("${local.server.port}")
    int port;

    @Autowired
    private DocumentModelService documentModelService;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Autowired
    private DocumentModelRepository documentRepository;

    private DocumentModelDTO modelDTO;

    //Test RestTemplate to invoke the APIs.
    private RestTemplate restTemplate = new TestRestTemplate();

    @Before
    public void setUp() {
        documentRepository.deleteAll();

        String title = "test";
        String text = "Hello, how are you?";

        try {
            modelDTO = documentModelService.create(title, text);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RestAssured.port = port;
    }

    @Test
    public void contextLoads() {
        Assert.assertNotNull(documentModelService);
        Assert.assertNotNull(documentRepository);
    }

    @Test
    public void testWithRestTemplate() {
        // TODO: http://www.javabeat.net/spring-boot-testing/
    }

    @Test
    public void testCreate_shouldWork() {
        //final Long id = modelDTO.getId();
        //final Long rawDocId = modelDTO.getRawDocId();
        //DocumentParam
        // TODO: test creation -> post request
        // http://g00glen00b.be/spring-boot-rest-assured/
        // https://www.jayway.com/2014/07/04/integration-testing-a-spring-boot-application/

        DocumentParam param = new DocumentParam("title", "this content is a test content");
        given()
                //.body("'title': 'test', 'content': 'this content is a test content'")
                .body(param)
                .contentType(ContentType.JSON)
        .when()
                .post(DOCUMENTS_RESOURCE + "add")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("rawDocId", Matchers.is(2))
                .body("id", Matchers.is(2));
    }

    @Test
    public void testGetById_shouldWork() {
        final Long id = modelDTO.getId();
        final Long rawDocId = modelDTO.getRawDocId();

        when()
            .get(DOCUMENTS_RESOURCE + id)
        .then()
            .statusCode(HttpStatus.SC_OK)
            .body("rawDocId", Matchers.is(rawDocId.intValue()))
            .body("id", Matchers.is(id.intValue()));
    }

    @Test
    public void testGetByTitle_shouldWork() {
        final String title = modelDTO.getTitle();
        final Long rawDocId = modelDTO.getRawDocId();

        when()
                .get(DOCUMENTS_RESOURCE + "title/" + title)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("rawDocId", Matchers.is(rawDocId.intValue()))
                .body("title", Matchers.is(title));
    }

    @Test
    public void testGetRaw_shouldWork() {
        final Long rawDocId = modelDTO.getRawDocId();

        when()
                .get(DOCUMENTS_RESOURCE + "raw/" + rawDocId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("content", Matchers.not(null));
    }

    @Test
    public void testGetAll_shouldWork() {
        when()
                .get(DOCUMENTS_RESOURCE)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testDelete_shouldWork() {
        final Long id = modelDTO.getId();

        when()
            .delete(DOCUMENTS_RESOURCE + id)
        .then()
            .statusCode(HttpStatus.SC_OK);
    }

}
