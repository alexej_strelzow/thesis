package at.ac.tuwien.isis.usermodels.persistence.test;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.SourceType;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.dto.Utils;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.rest.JsonUtils;
import at.ac.tuwien.isis.usermodels.rest.dto.WordJson;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import org.apache.commons.collections4.Bag;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 05.05.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
public class UserModelPersistenceTest {

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Autowired
    private UserModelRepository userModelRepository;

    @Before
    public void init() {
        userModelRepository.deleteAll();
        rawDocumentRepository.deleteAll();
    }

    @Test
    public void contextLoads() {
        Assert.assertNotNull(userModelRepository);
        Assert.assertNotNull(rawDocumentRepository);
    }

    @Test
    public void testUserModelPersists_shouldPersist() throws IOException {
        Set<RawDocument> documents = new HashSet<>();
        String title = "title";
        String text = "text";

        for (int i = 0; i < 10; i++) {
            String t = title + i;
            RawDocument rawDocument = new RawDocument(t, SourceType.TXT.name(), Utils.stringToBytes(text));
            documents.add(rawDocumentRepository.save(rawDocument));
        }

        UserModel userModel = new UserModel("testTitle", documents, Utils.stringToBytes("some random content"));
        userModel = userModelRepository.save(userModel);

        final UserModel theOne = userModelRepository.findOne(userModel.getId());

        Assert.assertEquals(userModel.getName(), theOne.getName());
        Assert.assertEquals(Utils.stringFromBytes(userModel.getContent()), Utils.stringFromBytes(theOne.getContent()));

        final Set<RawDocument> docs = userModel.getDocuments();
        Assert.assertTrue(docs.size() == documents.size());
    }

    @Test
    public void testSortingOfWordJson() throws Exception{
        String path = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\user_models\\Bertrand Meyer\\Bertrand Meyer.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(path));
        UserModelDTO userModelDTO = (UserModelDTO) is.readObject();
        is.close();
        final Bag<TaggedWordWrapper> model = userModelDTO.getModel();

        final List<WordJson> wordJsons = JsonUtils.convertBag(model);
        for (WordJson word : wordJsons) {
            System.out.println(word);
        }
    }

    /*
    @Test
    public void testUserModelPersists_shouldPersist() throws IOException {
        Set<DocumentModel> documents = new HashSet<>();
        String title = "title";
        String text = "text";

        for (int i = 0; i < 10; i++) {
            String t = title + i;
            RawDocument rawDocument = new RawDocument(t, SourceType.TXT.name(), ModelUtils.stringToBytes(text));
            DocumentModel documentModel = new DocumentModel(rawDocument, "none", ModelUtils.stringToBytes(text));
            documents.add(documentRepository.save(documentModel));
        }

        UserModel userModel = new UserModel("testTitle", documents, ModelUtils.stringToBytes("some random content"));
        userModel = userModelRepository.save(userModel);

        final UserModel theOne = userModelRepository.findOne(userModel.getId());

        Assert.assertEquals(userModel.getTitle(), theOne.getTitle());
        Assert.assertEquals(ModelUtils.stringFromBytes(userModel.getContent()), ModelUtils.stringFromBytes(theOne.getContent()));

        final Set<DocumentModel> docs = userModel.getDocuments();
        Assert.assertTrue(docs.size() == documents.size());
    }
    */

}
