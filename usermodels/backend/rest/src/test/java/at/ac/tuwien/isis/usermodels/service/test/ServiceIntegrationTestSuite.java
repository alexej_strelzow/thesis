package at.ac.tuwien.isis.usermodels.service.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Alexej Strelzow on 06.05.2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        DocumentModelIntegrationTest.class,
        UserModelIntegrationTest.class,
        ExperimentIntegrationTest.class
})
public class ServiceIntegrationTestSuite {
}
