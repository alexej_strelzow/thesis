package at.ac.tuwien.isis.usermodels.persistence.test;

import at.ac.tuwien.isis.usermodels.Application;
import at.ac.tuwien.isis.usermodels.dto.SourceType;
import at.ac.tuwien.isis.usermodels.dto.Utils;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 05.05.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
public class DocumentModelPersistenceTest {

    @Autowired
    private DocumentModelRepository documentRepository;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Before
    public void init() {
        // TODO: concurrency lets some tests fail!!!
        documentRepository.deleteAll();
        rawDocumentRepository.deleteAll();
    }

    @Test
    public void contextLoads() {
        Assert.assertNotNull(documentRepository);
        Assert.assertNotNull(rawDocumentRepository);
    }

    @Test
    public void testRawDocumentPersists_shouldPersist() {
        String text = "Hello, how are you?";
        byte[] content = Utils.stringToBytes(text);
        RawDocument rawDocument = new RawDocument("test", SourceType.TXT.name(), content);
        rawDocument = rawDocumentRepository.save(rawDocument);
        final RawDocument theOne = rawDocumentRepository.findOne(rawDocument.getId());

        Assert.assertEquals(rawDocument.getTitle(), theOne.getTitle());
        Assert.assertEquals(rawDocument.getContentType(), theOne.getContentType());

        try {
            Assert.assertEquals(Utils.stringFromBytes(theOne.getContent()), text);
        } catch (IOException e) {
            Assert.fail();
        }
    }

    /**
     * If we persist RawDocument before, and pass it to DocumentModel, we get an error -> only works with CASCADE.MERGE
     */
    @Test
    public void testDocumentModelPersists_shouldPersist() {
        final RawDocument rawDocument = getRawDocument();
        try {
            String text = "Hello";
            DocumentModel documentModel = new DocumentModel(rawDocument, "none", Utils.stringToBytes(text));
            documentModel = documentRepository.save(documentModel);

            final DocumentModel theOne = documentRepository.findOne(documentModel.getId());

            Assert.assertNotNull(theOne.getDocument());
            Assert.assertEquals(documentModel.getOptions(), theOne.getOptions());
            Assert.assertEquals(Utils.stringFromBytes(theOne.getContent()), text);

        } catch (IOException e) {
            Assert.fail();
        }
    }

    /**
     * ONLY WORKS WITH CASCADE.ALL
     * @throws IOException
     */
    @Test
    public void testDocumentModel_shouldPersistBoth() throws IOException {
        String title = "title";
        String text = "text";

        RawDocument rawDocument = new RawDocument(title, SourceType.TXT.name(), Utils.stringToBytes(text));
        DocumentModel documentModel = new DocumentModel(rawDocument, "none", Utils.stringToBytes(text));

        documentModel = documentRepository.save(documentModel);
        Assert.assertNotNull(documentModel.getDocument());

        final List<RawDocument> all = rawDocumentRepository.findAll();
        Assert.assertTrue(all.size() == 1);

        final RawDocument rawDocument1 = all.get(0);
        Assert.assertEquals(rawDocument1.getTitle(), title);
        Assert.assertEquals(rawDocument1.getContentType(), SourceType.TXT.name());
        Assert.assertEquals(Utils.stringFromBytes(rawDocument1.getContent()), text);

    }

    @Test
    public void testRawDocument_getDocumentForTitles_shouldReturnDocuments() throws IOException {
        Set<String> filteredTitles = new HashSet<>();
        String title = "title";
        String text = "text";

        for (int i = 0; i < 10; i++) {
            String t = title + i;
            RawDocument rawDocument = new RawDocument(t, SourceType.TXT.name(), Utils.stringToBytes(text));
            rawDocumentRepository.save(rawDocument);

            if (i < 5) {
                filteredTitles.add(t);
            }
        }

        final Set<RawDocument> documentForTitles = rawDocumentRepository.getDocumentsForTitles(filteredTitles);
        for (RawDocument doc : documentForTitles) {
            Assert.assertTrue(filteredTitles.contains(doc.getTitle()));
            filteredTitles.remove(doc.getTitle());
        }

        Assert.assertTrue(filteredTitles.isEmpty());
    }

    /*
    @Test
    public void testDocumentModel_getDocumentForTitles_shouldReturnDocuments() throws IOException {
        Set<String> filteredTitles = new HashSet<>();
        String title = "title";
        String text = "text";

        for (int i = 0; i < 10; i++) {
            String t = title + i;
            RawDocument rawDocument = new RawDocument(t, SourceType.TXT.name(), ModelUtils.stringToBytes(text));
            DocumentModel documentModel = new DocumentModel(rawDocument, "none", ModelUtils.stringToBytes(text));
            documentRepository.save(documentModel);

            if (i < 5) {
                filteredTitles.add(t);
            }
        }

        final Set<DocumentModel> documentForTitles = documentRepository.getDocumentsForTitles(filteredTitles);
        for (DocumentModel doc : documentForTitles) {
            Assert.assertTrue(filteredTitles.contains(doc.getDocument().getTitle()));
            filteredTitles.remove(doc.getDocument().getTitle());
        }

        Assert.assertTrue(filteredTitles.isEmpty());
    }
    */

    private RawDocument getRawDocument() {
        String text = "Hello, how are you?";
        byte[] content = Utils.stringToBytes(text);
        return new RawDocument("test", SourceType.TXT.name(), content);
    }

}
