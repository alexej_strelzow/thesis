Module overview:

babelnet:
    Src from BabelNet, downloaded in fall 2014 (Version 2.5)
    
babelnet-wrapper:
    Own wrapper for customization puprposes, mainly resource loading,
    so that we can use BabelNet in Web Applications too.
    
db:
    Contains the database
    
dto:
    Data Transfer Objects - objects shared across modules
    
evaluation:
    Contains code to:
        - identify user model candidates
        - download documents from those candidates
        - compute document - and user models
        - run experiments and evaluate the computed models
        
persistence:
    Contains O/R-mapper code
    
rest:
    REST-API
    
service:
    Contains services, which implement the business logic


Start the backend with: mvn spring-boot:run


Main classes:
    ModelGenerator
        -) *.txt from *.pdf
        -) generates user models (*.ser)
        
    DataGenerator
        -) start Spring Boot Application
        -) populate DB (-> all tables)
    
    FinalExperiments
        -) same as DataGenerator, but without any Spring Boot Framework support