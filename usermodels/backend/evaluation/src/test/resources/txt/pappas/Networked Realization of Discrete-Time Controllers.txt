
University of Pennsylvania
ScholarlyCommons

Real-Time and Embedded Systems Lab (mLAB) School of Engineering and Applied Science

3-1-2013

Networked Realization of Discrete-Time
Controllers
Fei Miao
University of Pennsylvania, miaofei@seas.upenn.edu

Miroslav Pajic
University of Pennsylvania, pajic@seas.upenn.edu

Rahul Mangharam
University of Pennsylvania, rahulm@seas.upenn.edu

George Pappas
University of Pennsylvania, pappasg@seas.upenn.edu

BibTeX:
@INPROCEEDINGS{MiaoEtAl12acc,
author = {Fei Miao and Miroslav Pajic and Rahul Mangharam and George J. Pappas},
title = {Networked Realization of Discrete-Time Controllers},
booktitle = {American Control Conference (ACC)},
year = {2013},
month = { June}
}
� 2013 IEEE. Personal use of this material is permitted. Permission from IEEE must be obtained for all other uses, in any current or future media,
including reprinting/republishing this material for advertising or promotional purposes, creating new collective works, for resale or redistribution to
servers or lists, or reuse of any copyrighted component of this work in other works.

This paper is posted at ScholarlyCommons. http://repository.upenn.edu/mlab_papers/56
For more information, please contact repository@pobox.upenn.edu.








Networked Realization of Discrete-Time Controllers

Abstract
We study the problem of mapping discrete-time linear controllers into potentially higher order linear
controllers with predefined structural constraints. Our work has been motivated by the Wireless Control
Network (WCN) architecture, where the network itself behaves as a distributed, structured dynamical
compensator. We make connections to model reduction theory to derive a method for the controller
embedding based on minimization of the H?-norm of the error system. This allows us to frame the problem
as synthesis of optimal structured linear controllers, which enables the utilization of design-time iterative
procedures for systems� approximation. Finally, we illustrate the use of the mapping procedure by embedding
PID controllers into the WCN substrate, and show how to reduce the computation overhead of the
approximation procedure.

Keywords
networked control, pid control, wireless control network

Disciplines
Controls and Control Theory

Comments
BibTeX:

@INPROCEEDINGS{MiaoEtAl12acc,
author = {Fei Miao and Miroslav Pajic and Rahul Mangharam and George J. Pappas},
title = {Networked Realization of Discrete-Time Controllers},
booktitle = {American Control Conference (ACC)},
year = {2013},
month = { June}
}

� 2013 IEEE. Personal use of this material is permitted. Permission from IEEE must be obtained for all other
uses, in any current or future media, including reprinting/republishing this material for advertising or
promotional purposes, creating new collective works, for resale or redistribution to servers or lists, or reuse of
any copyrighted component of this work in other works.

This conference paper is available at ScholarlyCommons: http://repository.upenn.edu/mlab_papers/56




Networked Realization of Discrete-Time Controllers

Fei Miao Miroslav Pajic Rahul Mangharam George J. Pappas

Abstract� We study the problem of mapping discrete-time
linear controllers into potentially higher order linear controllers
with predefined structural constraints. Our work has been mo-
tivated by the Wireless Control Network (WCN) architecture,
where the network itself behaves as a distributed, structured dy-
namical compensator. We make connections to model reduction
theory to derive a method for the controller embedding based on
minimization of the H?-norm of the error system. This allows
us to frame the problem as synthesis of optimal structured
linear controllers, which enables the utilization of design-time
iterative procedures for systems� approximation. Finally, we
illustrate the use of the mapping procedure by embedding PID
controllers into the WCN substrate, and show how to reduce
the computation overhead of the approximation procedure.

I. INTRODUCTION

In this paper, we address the problem of embedding
discrete-time linear controllers into structured computational
substrate � i.e., potentially higher order linear controllers
with structural constraints. This work has been motivated by
a recently introduced distributed control scheme from [1].
Unlike traditional networked control schemes that use the
standard network architecture �sensor ? channel ? con-
troller/estimator ? channel ? actuator�, the Wireless Con-
trol Network (WCN) employs a fully distributed control
scheme where the entire network itself acts as a controller.

Each node in a WCN behaves as a dynamical controller
instead of routing information to and from the controller; it
maintains a state, and at each time-step the state value is
updated to be a linear combination of the node�s previous
state and the states of the neighboring nodes and sensors.
As shown in [1], this scheme causes the entire network to
behave as a structured linear compensator, with sparsity
constraints imposed by the underlaying network topology.
Besides introducing a very small overhead, making it suitable
for implementation on resource constrained wireless nodes,
the proposed scheme has several additional benefits (see
Section II). However, to accelerate proliferation of this tech-
nology, it is necessary to provide a method to map existing
controllers, like PID controller or the ones developed using
the standard networked control system techniques (e.g., [2],
[3], [4]), into the WCN computational substrate.

The exact realization of linear controllers using a system
of networked, spatially interconnected systems has drawn a
lot of attention in the past decade (e.g., [5], [6], [7], [8]).
However, the main focus has been on specific networked

This research has been partially supported by the NSF-CPS 0931239
grant.

F. Miao, M. Pajic, R. Mangharam and G. J. Pappas are
with the Department of Electrical and Systems Engineering,
University of Pennsylvania, Philadelphia, PA, USA 19014. Email:
{miaofei,pajic,rahulm,pappasg}@seas.upenn.edu.

topologies (i.e., structural constraints) or topological condi-
tions for which the controller can be implemented on the
provided structured computational substrate. In this work,
we consider the general case of structural constraints, which
usually means that it is not possible to exactly match the
initial and derived controllers. Consequently, instead of the
exact mapping, we focus on the approximation of linear sys-
tems with potentially higher order, structurally constrained
systems. This problem effectively presents a dual of the
standard model reduction problem � approximating a high-
order system by a lower-order one (according to certain
criteria). Thus, similarly to [9], we employ model reduction
techniques to specify an error system using the H?-norm.
Note that the H?-norm of the difference of the two systems
has been a widely used measure of the model reduction
error, receiving considerable attention in the literature (e.g.,
see survey [10]). Grigoriadis derived necessary and sufficient
conditions for the existence of a solution to the H? model
reduction problems [11].

The specified error system allows for the problem formu-
lation as synthesis of an optimal structured linear controller
(SLC). The difference is that our structured controller (e.g.,
the WCN) has some freedom in the system dimension to
compensate for the structural constraints � i.e., to reduce
the approximation error we can increase the sizes of states
maintained by some nodes in the network. The SLC design
problem was addressed in [12], [13], [14], where iterative
procedures were used to solve the corresponding optimiza-
tion problems formulated using Linear Matrix Inequalities
(LMIs). We apply these algorithms to obtain the error system
with the local minimal H? norm. Furthermore, to reduce
the approximation error, we provide a computationally more
efficient method to expand the derived structured controller
by increasing its size (while maintaining the structure).

This paper is organized as follows. In Section II, we
present an overview of the WCN. In Section III, we formulate
the structural mapping problem as an H?-based optimization
problem, before we present (in Section IV) an iterative
method that can be used to solve it. Section V illustrates the
mapping of PID controllers into the WCN and provides an
approach to reduce the computation overhead of the mapping
procedure. Finally, Section VI provides concluding remarks.

1) Notation: Consider a standard linear time-invariant
(LTI) system ? = (A,B,C,D)

x[k + 1] = Ax[k] + Bu[k]

y[k] = Cx[k] + Du[k],
(1)

with A ? Rn�n, B ? Rn�m, C ? Rp�n and D ? Rp�m.
We denote the structural constraints of the system by



?? = ?A � ?B � ?C � ?D, (2)
where ?A, ?B, ?C, ?D specify the constraints on the ma-
trices A,B,C and D, respectively. For example, we say
that A ? ?A ? Rn�n satisfies the structural constraints
when [A]i,j = 0 if, for any k, the value of xj [k] does not
affect xi[k+1]. Consequently, other matrix elements are free
parameters.

II. MOTIVATION - WIRELESS CONTROL NETWORKS

We consider the setup presented in Figure 1, where an LTI
plant is to be controlled by a multi-hop wireless network.
The network is described by a graph G{V, E} where V =
{v1, . . . , vN} denotes the set of nodes, while E is the edge
set. To model the WCN, let zi[k] denote node vi�s state at
time step k. The linear update procedure can be described as

zi[k+1] = wiizi[k]+
?

vj?Nvi
wijzj [k]+

?
sj?Nvi

hijyj [k], (3)

where Nvi denotes the neighborhood of the node vi � i.e.,
the set of nodes and sensors whose transmissions can be
received by vi.

Furthermore, to control the plant, each control input ui[k]
is derived as a linear combination of the states from the nodes
in the neighborhood Nai of the actuator ai (that controls the
input):

ui[k] =
?
j?Nai

gijzj [k]. (4)

Therefore, the behavior of the WCN is specified by the link
weights wij , hij and gij , which define the linear combina-
tions computed by the nodes and actuators in the network. If
the states of all nodes at time step k are aggregated into the
state vector z[k] =

[
z1[k] z2[k] ... zN [k]

]T
, the behavior of

the entire network can be modeled as:

z[k + 1] = Wz[k] + Hy[k] ,

u[k] = Gz[k]
(5)

for all k ? N. It is important to note here that in the above
model W ? RN�N ,H ? RN�p,G ? Rm�N , and for all
i ? {1, . . . , N}, wij = 0 if vj /? Nvi ? {vi}, hij = 0 if
sj /? Nvi , and gij = 0 if vj /? Nai . Thus, the matrices W,H
and G are structured, with sparsity constraints imposed by
the underlying network topology. Throughout the rest of the
paper, we will also use ? as in (2) to denote the set of all
tuples (W,H,G) ? RN�N�RN�p�Rm�N that satisfy the
aforementioned sparsity constraints. Consequently, the WCN
scheme causes the network to act as a structured dynamical
compensator.

The WCN scheme has several benefits; it imposes low
computation and communication overhead to the nodes in
the network, allows easy scheduling of wireless transmis-
sions, and enables compositional design (that effectively
decouples the design and analysis of several control loops
implemented on the same network). In [1], [15], WCN
synthesis procedures were proposed, which ensure system
stability/optimality for a provided network topology. On the

??

??

?? ??

??

?? ??

??

??	

?A

??

??

??

??

??

??

??

??

			

AB?CD

???

Fig. 1: A multi-hop Wireless Control Network, where the
network itself acts as a distributed controller [15].

other hand, in [16] a set of topological conditions for the
existence of a stabilizing WCN configuration was derived.1

In the above model of the WCN we assumed a scenario
where each node maintains a scalar state. As shown in [1],
the more general case, where each node can maintain a vector
state with possibly different dimensions, can also be covered
with the model from (5). In this case, zi ? Rni and the size
of the network�s state vector is Z =

N?
i=1

ni. In addition, for

every pair of nodes vi, vj , the weight wij ? Rni�nj , while
sparsity pattern ? describes the constraints on the weight
matrices (for example, wij = 0 ? Rni�nj if vj /? Nvi ?
{vi}).
A. Mapping Discrete-Time Controllers into the WCN

The fact that the WCN acts as a structured dynamical
controller allows for the use of the network as computational
resource. Compositionality of the WCN makes it suitable
for control of large-scale systems, such as industrial process
control and building automation systems. Thus, a procedure
to map existing controllers into this computational substrate
would enable a direct utilization of the well-known con-
trollers already deployed in practice. In addition, it would
allow for the use of WCNs in hierarchical control systems,
where upper levels of control implement optimization based
procedures (e.g., Model Predictive Control) to determine set
points for the low level controllers that, in our case, would be
implemented using the WCN. This way, the implementation
and compositional benefits of the WCN would be married
with the performance benefits of pre-designed centralized
controllers (e.g., the use of the existing controller tuning
algorithms for WCN configurations).

Although the WCN acts as a linear dynamical con-
troller (5), the challenge is that the sparsity constraints
imposed by the network topology prevent the controller from
being mapped directly to the WCN. However, this constraint
is counteracted by the fact that the state vector of the WCN
is of the size Z, which may be significantly larger than the
size of the state vector for the original controller. We will
leverage these additional degrees of freedom to perform the
mapping (see Section V). As shown in the next section, to
find a configuration (i.e., matrices W,G and H) that causes

1In this work, the link weights wij , hij and gij for all nodes in the
network are referred to as a WCN configuration.



the WCN to mimic the behavior of a given controller, we
will first define appropriate metrics to measure �closeness�.
In this work, our goal is to minimize the widely applied H?
metric, and map the problem into optimization of structured
linear controllers [12].

III. STRUCTURED LINEAR CONTROLLER REALIZATION

In this section we present an approach to formulate our
design problem as an optimization problem. We focus on
control of discrete-time LTI plants of the form:

x[k + 1] = Ax[k] + Bu[k] + Bdud[k]

y[k] = Cx[k],
(6)

where u[k] ? Rm denotes plant inputs (provided by actu-
ators a1, ..., am), ud[k] ? Rm specifies disturbance inputs,
y[k] ? Rp denotes measurements of the plant state vector
x ? Rn (provided by the sensors s1, ..., sp), while matrices
A,B,Bd,C have appropriate dimensions.

In this work, we investigate the problem of approximating
any discrete LTI controller ?1 = (A1,B1,C1,D1), with a
structured linear controller ?2 = (A2,B2,C2,D2) that may
have a higher order. Note that for the structured linear con-
troller we assume that the new controller has to satisfy struc-
tural constraints ?2 imposed by the computational substrate.
With ?1c ,?

2
c we specify the closed-loop systems, where the

plant is controlled by the controller ?1 and ?2, respectively.
By denoting its state as xic[k] =

[
x[k]T xi[k]

T
]T

, the
closed loop system ?ic (i = 1, 2) can be described as:

xic[k + 1] =

[
A + BDiC BCi

BiC Ai

]
xic[k] +

[
Bd
0

]
ud[k]

,Aicxic[k] + Bicud[k],
yic[k] =

[
C 0

]
xic[k] , Cicxic[k]

Now, we can formulate the problem as synthesis of a con-
troller (A2,B2,C2,D2) ? ?2 that minimizes the difference
between the performances of the closed-loop systems ?1c
and ?2c (i.e., when the plant is controlled by the initial or
the structured controller). To evaluate the error or distance
between the two closed-loop systems we employ the standard
H?-norm, which is widely used in model reduction. This
allows us to formulate the mapping problem as minimization
of the approximation error between two systems. Exploiting
the similarities between our problem and the model reduction
problem, we start by using the similar approach as in [9] to
construct the error system ?ce. This system is described by

the state xce =
[
x1c

T
x2c

T
]T

and transfer function Hce(z) =
H1c(z) ? H2c(z), where H1c(z) and H2c(z) are the transfer
functions of ?1c and ?

2
c , respectively. The error system ?ce

evolves as:

xce[k + 1] = Acexce[k] + Bceud[k]

yce[k] = Ccexce[k],
(7)

where

Ace =

[
A1c 0
0 A2c

]
,Bce =

[
B1c
B2c

]
,Cce =

[
C1c ?C2c

]
. (8)

The H? norm of a stable discrete time system is defined
as ??ce?H? = sup ?�(Hce(ej?)), where ?� is the maximum
singular value of the matrix Hce(ej?). Note that the error
system parameters described as ?ce = (Ace,Bce,Cce,0)
present a linear mapping of the structured controller param-
eters ?2 = (A2,B2,C2,D2).

Consequently, the problem of deriving feasible controller
variables (A2, B2, C2, D2) ? ?2, while minimizing the
H? norm of the error system can be specified as the
following optimization problem:

minimize ?
subject to ??ce?H? 6 ?,

(A2, B2, C2, D2) ? ?2
(9)

IV. APPROXIMATION ALGORITHMS

Since we have been able to formulate the problem of
mapping discrete-time controllers into structured substrates
as an optimization problem, in this section we continue by
transforming the constraint for H? norm of the error system
to an LMI. We start by introducing the following result:

Lemma 1 ([17]): For asymptotically stable system (8),
the following statements are equivalent:

(i) There exist matrices X ,? and (A2, B2, C2, D2) ?
?2 such that

X ?AceXATce + BceBTce
? ?CceXCTce

(10)

(ii) There exist matrices X ,?,Z and
(A2, B2, C2, D2) ? ?2 such that[X Z
Z ?

]
?
[
Ace Bce
Cce 0

] [X 0
0 I

] [
Ace Bce
Cce 0

]T
(11)

From Lemma 1, we construct the bound of the �perfor-
mance� of the error system.

Theorem 1 ([17]): Suppose that ? ? ?I, where ? ? 0,
then ??ce?H? 6

?
? if and only if there exist X ? 0,

? ? 0, Z = 0 such that (11) holds, which is equivalent to:

R(X ,0,?,X?1) =

????
X 0 Ace Bce
0 ? Cce 0

ATce C
T
ce X?1 0

BTce 0 0 I

???? ? 0 (12)
Therefore, our objective is to derive (A2, B2, C2, D2) ?

?2, along with feasible X , ? that minimize the scalar
variable ? under the constraints specified in Theorem 1.

A. Linearization Algorithm

From Theorem 1, the obtained optimization problem has
only one non-convex term (X?1), which is used to specify
the constraint in (12). To be able to solve the problem, we
approximate the term with a suitable convex expression. One
convexifying, and more specifically, a linearization method
�around� any matrix Xk (described in [14], [12]) is:
LIN(X?1,Xk) = Xk?1 ?Xk?1(X?Xk)Xk?1. (13)



This linearization provides a sufficient (and conservative)
condition for the optimal solution, while enabling us to
specify the constraint (12) as an LMI. Furthermore, we
can now extend the approach from [12] to define iterative
algorithms for embedding linear controllers into structured
substrates.

B. Initialization and Optimal Algorithms

According to (8), the set of eigenvalues of Ace is the
union of eigenvalues of A1c and A

2
c . Thus, ?ce is stable

if and only both A1c and A
2
c are stable. We assume that

the initial controller guarantees stability of the closed-loop
system ?1c , and that there exists a stable configuration for
the imposed structural constraints.2 Therefore, there exist
feasible X ,?, and (A2, B2, C2, D2) ? ?2, such that?
? = ??c?H? is the optimal solution (i.e., cost) of the

problem (9).
To solve the optimization problem we define the fol-

lowing iterative algorithms (Algorithm 1 and 2). Since the
linearization LIN(X?1, Xk) provides a sufficient problem
constraint, the iterative optimization procedure specified in
Algorithm 1 may not work with a randomly generated initial
point Xk (i.e., for k = 0). Therefore, to obtain a feasible
initial point for Algorithm 1, we replace X?1 in (12) with
a new variable (Y), and relax the constraint first. This
relaxation results in the initialization Algorithm 2 that starts
from a random selected matrix X0 (i.e., X0 = I + RTR,
where R is a random matrix with appropriate dimensions).

Algorithm 1 Mapping linear controllers into structured
substrate
1) Set ? > 0, kstop > 0, and k = 0.
2) Solve the following convex optimization problem.
minimize ?

subject to R(X ,0,?, LIN(X?1, Xk) ? 0,
X ? 0,? ? 0,? ? ?I, (A2, B2, C2, D2) ? ?2,

3) If ? < ? or k > kstop, stop. Otherwise, set k = k + 1,
Xk+1 = X and go back to step 2.

The convexifying and linearization method ensures that
at each iteration k, the optimal solution ?k+1 satisfies that
0 ? ?k+1 ? ?k, so the sequence converges to a local optimal
solution [12].

It is worth noting here that the problem formulation and
algorithms derived in this paper have been based on the
closed-loop system model. Another possible approach would
be to reduce the distance between the two controller systems
directly. However, this would require that the initial (i.e.,
given) controller is stable.

V. APPROXIMATING PID WITH THE WCN

PID controllers are the most commonly used type of linear
controllers. Therefore, in this section, we illustrate how to

2A set of topological conditions for the WCN such that there exists a
stabilizing WCN configuration is specified in [16].

3?Ace represents eigenvalues of Ace.

Algorithm 2 Iterative algorithm used to obtain a feasible
initial point
1) Set ? > 0, and k = 0.
2) Solve the following convex optimization problem.

minimize ?
subject to R(X ,0,?,Y) ? 0,[X I

I Y
]
? 0,? ? ?I,X ? 0,Y ? 0,? ? 0,

Y ? LIN(X?1, Xk) + ?I,
(A2, B2, C2, D2) ? ?2,

3) If ? < ?, ??Ace? < 1,3stop. Otherwise, set k = k + 1,
Xk+1 = X and go back to step 2.

     Plant
S1 S2A1A2

WCNv2v4

v3v5

v6

v1

Fig. 2: An example of the WCN topology with 6 nodes,
where each node can maintain a scalar or vector state.

employ the algorithms from the previous section to synthe-
size WCN configurations. The goal is to minimize the error
between initial Multiple-Input Multiple-Output (MIMO) PID
controllers and the derived WCNs. We consider the case
when the initial linear PID controller has states x1 ? R4,
while the WCN scheme is implemented on the network with
the topology shown in Figure 2. Furthermore, the controlled
plant has states x ? R4, two inputs and two outputs.

Initially, we consider the scalar WCN setup where each
node in the WCN maintains a scalar state, i.e., zi[k] ?
R in (3). Consequently, in this case the WCN acts as a
structured dynamical controller with z[k] ? R6 in (5). Using
Algorithms 1 and 2, which were implemented using CVX,
a package for specifying and solving convex optimization
programs [18], after less than 60 iterations we obtained
? = 0.1294, meaning that ??ce?H? =

?
? ? 0.3597 (see

Figure 3). In addition, we compared the step (Figure 4(a))
and frequency (Figure 4(b)) responses for the closed-loop
systems controlled by the initial PID and the obtained WCN
configuration.

From the above results, we can conclude that the structural
constraints for a scalar state WCN (from Figure 2) imposed
a significant limitation on our capabilities to approximate
behavior of the initial MIMO PID controller. However, the
error between these closed-loop systems (i.e., ?1c and ?

2
c) can

be decreased if each node in the WCN maintains a larger
state (i.e., a vector state). We will refer to these systems
as vector WCNs. By incorporating different memory and
computing capabilities to each node, the vector WCN where
each node vi maintains a (vector) state from Rni can be
modeled as in (3), (4), where zi ? Rni , ni > 1, wij ?



?2

0

2

4

6
From:In(1)

To
:O

ut
(1

)

0 20 40 60 800

1

2

3

4

5

6

To
:O

ut
(2

)

From:In(2)

0 20 40 60 80

Step response

Am
pl

itu
de

PID System
WCN System

Time (second)

(a) Step response

?20

0

20
From: In(1)

To
:O

ut(
1)

?360
?180

0
180

To
:O

ut
(1

)

0

5

10

15

To
:O

ut(
2)

10?3 10?2 10?1 100 101
?270
?180
?90

0

To
:O

ut(
2)

From:In(2)

10?3 10?2 10?1 100 101

Bode Diagram

Ma
gn

itu
de

(dB
);

Ph
as

e(
de

g)

PID System
WCN System

Frequency (rad/s)

(b) Frequency response

Fig. 4: Comparison of the closed-loop systems controlled by the initial PID and the WCN, where each node in the network
from Figure 2 maintains a scalar state. Final system approximation error is ? = 0.1294, ??ce?H? =

?
? ? 0.3597.

0 10 20 30 40 50 60
0

0.2

0.4

0.6

0.8

1

1.2

1.4

1.6

1.8

2

Iteration Step

Op
tim

al 
Va

lue

Fig. 3: Obtained values for ?k at each iteration step of
Algorithm 1, when each node in the WCN from Figure 2
maintains a scalar state.

Rni�nj , hij ? Rni , gij ? R1�nj .
Algorithms 1 and 2 can be easily extended to cover this

vector state scenario, as in this case only the structural
constraints have to be generalized. A straightforward ex-
tension would require running both Algorithms 2 and 1 to
obtain initial (feasible) and optimal points. However, it is
possible to significantly speed up the initialization procedure
for vector WCNs. This is achieved by running Algorithm 2
for the scalar WCN with the same topology, and then
expanding the obtained feasible configuration to derive an
initialization point for the vector WCN. More precisely,
assume Algorithm 2 provides a scalar WCN configuration
(W0,H0,G0) with closed-loop transfer function H . Then,
an initial feasible vector WCN configuration with closed-
loop transfer function H , and where nodes maintain states
of size n�, can be derived from the vector weights defined
using the initial scalar weights as:

w1ij = w
0
ijIn�,h

1
ij = cn�(h

0
ij),g

1
ij =

1

n�
rn�(g

0
ij). (14)

Here, cn�(a) (rn�(b)) denotes a size n� column (row) vector
where every element equals to a (b), while In� is the identity
matrix of size n�. From (14), a new feasible error system
?ne = (Ane,Bne,Cne) (from (7), (8)) can be obtained.

Therefore, by solving a single convex optimization prob-
lem (15), instead of a sequence of optimization problems
(Algorithm 2), we can obtain a feasible initial point X1 for

the Algorithm 1 of vector WCN

minimize ?0
subject to X1 ? AneX1ATne + BneBTne,

? ? CneX1CTne,
X1 ? 0,? ? ?0I.

(15)

To summarize, the speed up is obtained by avoiding to
directly use Algorithm 2 to derive a feasible vector WCN
configuration, as this would require solving a large convex
optimization problem at each iteration. Instead, we iteratively
solve a smaller problem (for scalar WCN configurations).

Note that the result ?0 of the optimization problem (15) is
an upper bound for the approximation error when a vector
WCN is used, since the Algorithm 1 provides a decreasing
sequence of ?k�s after the initialization step. Table 1 presents
a comparison of the times required to compute the initial
points for Algorithm 1 when each node maintains a state
zi ? R3 in the WCN from Figure 2. We compared the
following methods: 1) when the Algorithm 2 is used to obtain
a scalar WCN configuration, then extended into the �vector�
WCN by (14), (15); or 2) when Algorithm 2 is used to
directly compute a feasible vector configuration. As can be
seen, both the time of every iteration step and the number of
iteration steps are significantly increased when the �vector�
(i.e., direct) form of Algorithm 2 is used. This is caused
by the fact that the size of the problem directly affects the
performance of the LMI solver (i.e., CVX).

Finally, we showed that this more powerful, vector WCN,
can reduce the approximation error. Figure 5(a) and Fig-
ure 5(b) show comparisons of the closed-loop system per-
formances when the plant is controlled by the initial PID
controller and the WCN where each node maintains a state
zi ? R3. In this case, using the aforementioned procedure
we obtained ? = 0.0823, thus reducing the approximation
error by almost 50%.

VI. CONCLUSION
In this paper, we have presented a method for mapping

discrete-time linear controllers into structured computation



?2

0

2

4

6
From:In(1)

To
:O

ut
(1

))

0 20 40 60 800

1

2

3

4

5

6

To
:O

ut
(2

))

From:In(2)

0 20 40 60 80

Step Response

Am
pli

tud
e

Time(second)

WCN System
PID System

(a) Step response

?20

0

20

 To
:O

ut
(1

)

?180

0

180

To
:O

ut
(1

)

?10

0

10

20

To
:O

ut
(2

)

10?2 100
?270
?180
?90

0

To
:O

ut
(2

)

From: In(2)

10?2 100

Bode Diagram

Ma
gn

itu
de

(d
B)

;P
ha

se
(d

eg
)

Frequency (rad/s)

From:In(2)

PID System
WCN System

(b) Frequency response

Fig. 5: Comparison of the closed-loop systems controlled by the initial PID and the WCN, where each node in the network
from Figure 2 maintains a state from R3. Algorithm 1 gets ? = 0.0823, and the approximation error between two closed
loop systems are smaller than what we get from the scalar state WCN (each node maintains a state from R).

Method to derive the initial Number of Time for
vector WCN configuration iteration steps each step

Expand a feasible �scalar� configuration 58 1s
Direct use of Algorithm 2 675 9s
TABLE I: comparison of computation speed

substrates. This problem has been motivated by the Wireless
Control Network, a recently introduced distributed scheme
for control over multi-hop wireless networks. By exploiting
the similarities with the model reduction problem, we have
formulated the structured approximation problem as an opti-
mal design of structured linear controllers, and specified the
algorithms that can be used for the networked system realiza-
tion. In addition, we have illustrated their use on the mapping
of MIMO PID controllers into the WCN, and showed how to
reduce the computation overhead of the mapping when the
WCN nodes maintain vector states. Finally, we have shown
that an increase in sizes of the states maintained by the nodes
in the WCN, decreases the approximation error. However, it
would be beneficial to provide a way to estimate mapping
improvements with the increase of the nodes� state sizes.
This is an avenue for future work.

REFERENCES
[1] M. Pajic, S. Sundaram, G. Pappas, and R. Mangharam, �The

wireless control network: A new approach for control over
networks,� IEEE Transactions on Automatic Control, vol. 56,
no. 10, pp. 2305 �2318, 2011.

[2] O. C. Imer, S. Yuksel, and T. Basar, �Optimal control of LTI
systems over unreliable communication links,� Automatica,
vol. 42, no. 9, pp. 1429�1439, 2006.

[3] L. Schenato, B. Sinopoli, M. Franceschetti, K. Poolla, and
S. S. Sastry, �Foundations of control and estimation over lossy
networks,� Proceedings of the IEEE, vol. 95, no. 1, pp. 163�
187, 2007.

[4] V. Gupta, A. F. Dana, J. Hespanha, R. M. Murray, and B. Has-
sibi, �Data transmission over networks for estimation and
control,� IEEE Transactions on Automatic Control, vol. 54,
no. 8, pp. 1807�1819, 2009.

[5] B. Bamieh, F. Paganini, and M. Dahleh, �Distributed control
of spatially invariant systems,� IEEE Transactions on Auto-
matic Control, vol. 47, no. 7, pp. 1091 �1107, 2002.

[6] R. D�Andrea and G. Dullerud, �Distributed control design
for spatially interconnected systems,� IEEE Transactions on
Automatic Control, vol. 48, no. 9, pp. 1478 � 1495, 2003.

[7] M. Rotkowitz and S. Lall, �A characterization of convex
problems in decentralized control,� IEEE Transactions on
Automatic Control, vol. 51, no. 2, pp. 274 �286, 2006.

[8] A. Vamsi and N. Elia, �Optimal realizable networked con-
trollers for networked systems,� in American Control Confer-
ence (ACC), 2011, 2011, pp. 336�341.

[9] P. Van Dooren, K. A. Gallivan, and P.-A. Absil, �H2-optimal
model reduction of MIMO systems,� Applied Mathematics
Letters, vol. 21, no. 12, pp. 1267�1273, 2008.

[10] S. Gugercin and A. C. Antoulas, �A survey of model reduction
by balanced truncation and some new results,� International
Journal of Control, vol. 77, no. 8, pp. 748�766, 2004.

[11] K. Grigoriadis, �Optimal H? model reduction via linear
matrix inequalities: continuous and discrete-time cases,� in
Proceedings of the 34th IEEE Conference on Decision and
Control, vol. 3, 1995, pp. 3074 �3079.

[12] J. Han and R. Skelton, �An LMI optimization approach for
structured linear controllers,� in Proc. 42nd IEEE Conference
on Decision and Control, vol. 5, 2003, pp. 5143 � 5148.

[13] L. El Ghaoui, F. Oustry, and M. AitRami, �A cone comple-
mentarity linearization algorithm for static output-feedback
and related problems,� IEEE Transactions on Automatic Con-
trol, vol. 42, no. 8, pp. 1171 �1176, 1997.

[14] M. de Oliveira, J. Camino, and R. Skelton, �A convexifying
algorithm for the design of structured linear controllers,� in
Proceedings of the 39th IEEE Conference on Decision and
Control, vol. 3, 2000, pp. 2781 �2786.

[15] M. Pajic, S. Sundaram, J. Le Ny, G. J. Pappas, and R. Mang-
haram, �Closing the loop: a simple distributed method for
control over wireless networks,� in Proceedings of the 11th
international conference on Information Processing in Sensor
Networks, ser. IPSN �12, 2012, pp. 25�36.

[16] M. Pajic, S. Sundaram, G. J. Pappas, and R. Mangharam,
�Topological conditions for in-network stabilization of dynam-
ical systems,� IEEE Journal on Selected Areas in Communi-
cations, vol. 31, no. 4, pp. 1�15, 2013.

[17] R. E. Skelton, T. Iwasaki, and K. Grigoriadis, A unified
algebraic approach to linear control design. CRC Press,
1998.

[18] M. Grant and S. Boyd, �CVX: Matlab software for disciplined
convex programming, ver. 2.0,� http://cvxr.com/cvx, 2012.


	University of Pennsylvania
	ScholarlyCommons
	3-1-2013

	Networked Realization of Discrete-Time Controllers
	Fei Miao
	Miroslav Pajic
	Rahul Mangharam
	George Pappas
	Networked Realization of Discrete-Time Controllers
	Abstract
	Keywords
	Disciplines
	Comments



