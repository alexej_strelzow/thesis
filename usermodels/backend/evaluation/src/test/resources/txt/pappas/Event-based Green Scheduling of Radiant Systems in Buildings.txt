
Event-based Green Scheduling of Radiant Systems in Buildings
Truong X. Nghiem, George J. Pappas and Rahul Mangharam

Department of Electrical and Systems Engineering
University of Pennsylvania

{nghiem, pappasg, rahulm}@seas.upenn.edu

Abstract�This paper looks at the problem of peak power
demand reduction for intermittent operation of radiant sys-
tems in buildings. Uncoordinated operation of the circulation
pumps of a multi-zone hydronic radiant system can cause
temporally correlated electricity demand surges when multiple
pumps are activated simultaneously. Under a demand-based
electricity pricing policy, this uncoordinated behavior can result
in high electricity costs and expensive system operation. We
have previously presented Green Scheduling with the periodic
scheduling approach for reducing the peak power demand
of electric radiant heating systems while maintaining indoor
thermal comfort. This paper develops an event-based state
feedback scheduling strategy that, unlike periodic scheduling,
directly takes into account the disturbances and is thus more
suitable for building systems. The effectiveness of the new
strategy is demonstrated through simulation in MATLAB.

I. INTRODUCTION

Radiant heating and cooling systems, or radiant systems
for short, serve as an alternative to the conventional forced-
air heating, ventilating and air conditioning (HVAC) systems
for buildings. In radiant systems, heat is supplied to or
removed from building elements such as floors, ceilings and
walls by circulating water, air or electric current through a
circuit embedded in or attached to the structure [1]. Radiant
systems depend largely on radiant heat transfer between the
thermally controlled building elements and the conditioned
space. The benefits of radiant systems over forced-air HVAC
systems for residential and commercial buildings have been
well-studied [2]. Essentially, there are three major benefits:
human comfort, reduced heat loss, and peak energy demand
reduction. Because the building elements used in radiant
systems have high thermal mass, they serve as energy storage
whose slow thermal behaviour is exploited to provide cooling
or heating. Hence, the building�s thermal mass can be utilized
to flatten out peaks in energy demand. Nowadays, radiant
systems are widely used in buildings in Korea, Germany,
Austria, Denmark and some parts of the United States [3].

Two-position control is the simplest control method for ra-
diant systems, where the system is switched on/off when the
zone temperature reaches certain thresholds. Outdoor reset
control, which sets the supply water temperature according to
the ambient air temperature by a predetermined rule, and PID
control are also used in practice [1]. More advanced control
methods for radiant systems have also been proposed, e.g.,
model predictive control was shown to improve the comfort
and energy consumption of radiant systems in [4].

This material is based upon work supported by the DoE Energy Efficient
Buildings (EEB) HUB sponsored by the U.S. Department of Energy under
Award Number DE-EE0004261.

Recently, intermittent operation of the circulation pumps in
hydronic radiant systems was investigated in [5] for reducing
the electricity consumption of radiant systems. The focus of
[5] was on maintaining the thermal comfort in a single zone.
However, intermittent operation can cause highly fluctuating
electricity demand of the pumps, which results in high peaks
in the electricity demand. Particularly, in a system of multiple
zones with multiple pumps, temporally correlated electricity
demand spikes can occur when multiple pumps are activated
simultaneously. High peaks in electricity demand are costly
under a demand-based electricity pricing policy, in which a
customer is charged for both its electricity consumption and
its peak demand over the billing cycle. The unit price of the
peak demand charge is usually very high to discourage the
use of electricity under peak load conditions. Many com-
mercial electricity customers are subject to demand-based
pricing. Therefore reducing the peak electricity demand of
intermittent operation of radiant systems is desirable.

In our recent paper [6], the Green Scheduling approach
was proposed to schedule the operation of electric radiant
systems to reduce the aggregate peak power demand while
ensuring that indoor thermal comfort is always maintained.
We studied periodic scheduling, which was simple and
scalable but did not take into account disturbances (which are
often abundant in building systems). The major contribution
of this paper is to develop an event-based state feedback
scheduling strategy, which directly handles the disturbances
and is therefore more suitable for buildings. Through simula-
tion in MATLAB of a 10-zone hydronic radiant system, our
approach was shown to achieve a 77.8% reduction in peak
demand and a 31.2% reduction in total energy consumption,
compared to uncoordinated operation of the pumps.

This paper is organized as follows. Section II presents
the hydronic radiant system�s model and formulates the
Green Scheduling problem. We develop the new scheduling
algorithm in Sections III and IV and demonstrate it through
MATLAB simulations in Section V. We discuss related work
in Section VI. Finally, Section VII concludes the paper with
a summary and future research directions.

II. SYSTEM�S MODEL AND PROBLEM DEFINITION
In hydronic radiant systems, hot or chilled supply water is

pumped through a system of tubes laid in a pattern inside the
radiant building elements. Consider two zones equipped with
a hydronic radiant system as depicted in Fig. 1. Embedded in
a slab under the floor or above the ceiling of each zone is a
piping system that carries water from a supply source, e.g., a
boiler for heating or a chiller for cooling. The piping systems
for the zones are separate from each other. As illustrated in



Supply

Return

Pump Pump

Zone pipes

Zone 1 Zone 2

Fig. 1: Diagram of a hydronic radiant system for two zones.
Ta

1/K1 1/K2Zone 1 Zone 2
qg,1 + qs,1 qg,2 + qs,2T1 T2

C1 C21/Kr,1 1/Kr,2

1/K12

Slab SlabCr,1

Tw,1

Cr,2

1/Kw,2

Tw,2

1/Kw,1

Tc,1 Tc,2

Fig. 2: RC network model of the radiant system in Fig. 1.

Fig. 1, each zone has its own supply and return pipes as
well as a circulation pump. This hydronic circuit topology
is similar to that used in [5].
A. Intermittent Operation of Radiant Systems

For the hydronic radiant system in Fig. 1, the supply water
temperature and the mass flow rate by the pump are the two
manipulatable variables for low level control. In practice,
typically one of these two controllable variables is fixed or
only changed infrequently. That is, either the supply water
temperature is fixed and variable speed control is used for
the pump, or the pump runs at constant speed and the supply
water temperature is varied. However, both options require
continuous operation of the circulation pump, which can
result in high electricity cost of the radiant system.

Intermittent operation of the circulation pump was stud-
ied in [5] for reducing the energy consumption of radiant
systems. Essentially, the supply water temperature is fixed
and the pump is either switched off or operated at a constant
speed. Because of the high thermal inertia of the radiant sys-
tems, this simple control method is appropriate for regulating
the zone temperature. Furthermore, as the pump no longer
runs continuously, the electricity consumption and cost of
the system can be reduced. In this paper, we assume this
intermittent control method for the circulation pumps.
B. Thermal Model for Radiant Systems

Similar to [7], we assume that the slab is uniformly heated
and there is no lateral temperature difference or heat transfer.
For each zone, the thermal dynamics from the supply water
temperature to the zone temperature is then modeled using
a Resistance�Capacitance (RC) network model as shown in
Fig. 2. The model for each zone i, i = 1, 2, has 4 nodes:
Ta is the common ambient air temperature, Ti is the zone
temperature, Tc,i is the core temperature, and Tw,i is the
supply water temperature. The parameters and variables of
the RC network model are summarized in Table I. We

TABLE I: Parameters of the model in Fig. 2, for i = 1, 2.
Ta ambient air temperature (?C)
Ti air temperature of zone i (?C)
Tc,i core temperature of the slab of zone i (?C)
Tw,i supply water temperature for zone i (?C)
qg,i internal heat gain of zone i (W/m2)
qs,i heat gain due to solar radiation of zone i (W/m2)
Ki thermal conductance between Ti and Ta (W/(K m2))
Kr,i thermal conductance between Tc,i and Ti (W/(K m2))
Kw,i piping thermal conductance of zone i (W/(K m2))
Kij thermal conductance between zone i and zone j (W/(K m2))
Ci thermal capacitance of zone i (J/K)
Cr,i thermal capacitance of the slab of zone i (J/K)

can now write the differential equations for the thermal
dynamics of the zones and their radiant systems. When the
pump of zone i is circulating water in its piping system, the
differential equation for the core temperature node Tc,i is

Cr,i
dTc,i
dt = Kr,i(Ti ? Tc,i) +Kw,i(Tw,i ? Tc,i) (1)

When the pump is not running, equivalently the supply water
temperature node Tw,i is removed, we have

Cr,i
dTc,i
dt = Kr,i(Ti ? Tc,i). (2)

The differential equation for the zone temperature Ti is
Ci

dTi
dt = Kr,i(Tc,i ? Ti) +Ki(Ta ? Ti)

+
?
j 6=iKij(Tj ? Ti) + qg,i + qs,i. (3)

The model for each zone i has two state variables Ti and
Tc,i. It also has three disturbance variables Ta, qg,i and qs,i.
The supply water temperature Tw,i can be regulated or fixed,
depending on the control method for the radiant system.

Let us consider n > 1 zones instead of two zones and
suppose that Tw,i are fixed for all i. Define the state vector
x = [T1, Tc,1, . . . , Tn, Tc,n]

T ? X = R2n and the distur-
bance vector d = [qg,1, qs,1, . . . , qg,n, qs,n, Ta]T ? R2n+1.
Let binary variable ui ? {0, 1} denote the status of pump i:
ui = 1 if the pump is running and ui = 0 otherwise. Define
the binary vector u = [u1, . . . , un] ? {0, 1}n. Then Eqs. (1)
to (3) for all zones can be combined in a state-space model:

x?(t) = (A0 +
?n
i=1Aiui(t))x(t) +Bu(t) +Wd(t)

= A(u(t))x(t) +Bu(t) +Wd(t) (4)

where A(u) = A0 +
?n
i=1Aiui. We note that:

� The state matrix A(u) depends on u (i.e., switching
state matrix) because the differential equation of Tc,i
changes with respect to ui (Eqs. (1) and (2)).

� From the thermal Eqs. (1) to (3), it is evident that the
state matrix A(u) is always strictly diagonally dominant
with negative diagonal entries, hence it is Hurwitz [8].

C. Green Scheduling for Peak Demand Reduction
Though radiant systems have the capability to flatten

out peaks in energy demand, the intermittent operation of
the circulation pump causes significant fluctuations in the
electricity demand, hence neutralizes this peak demand re-
duction benefit for a single zone. Furthermore, in a system of
multiple zones, temporally correlated energy demand spikes
can occur when multiple circulation pumps are activated
simultaneously. This phenomenon will be demonstrated in
the simulation in Section V, particularly in Fig. 8.



As we discussed in Section I, high peaks in electricity
demand can be costly for commercial electricity customers.
Therefore, it is desirable to reduce the peak aggregated
electricity demand of the pumps, which must be achieved
through coordination of the pumps� operations. At the same
time, thermal comfort must be maintained in all the zones,
specifically the zone temperature Ti should be kept within
a comfortable range [li, hi] where li < hi are given. In
our previous work [6], we proposed Green Scheduling as
an approach for reducing peak energy demand. Essentially,
in Green Scheduling, multiple interacting control systems
are coordinated within a constrained peak demand envelope
while ensuring that safety and operational conditions are
facilitated. The peak demand envelope is formulated as a
constraint on the number of binary control inputs that can
be activated simultaneously, that is

?n
i=1 ui(t) ? k ?t ? 0

for some given k ? {0, 1, . . . , n}. Evidently, this approach
can be applied to coordinate the intermittent operations of
the pumps, as stated in the following problem.

Problem 1: Given model (4) of the radiant system, com-
fort regions [li, hi] for i = 1, . . . , n, and peak constraint
0 ? k ? n, schedule the operations of the circulation pumps
so that the peak demand constraint is satisfied at all time
while thermal comfort in each zone is maintained.

A control signal u(�) can be thought of as a schedule that
switches on-off and coordinates the individual sub-systems.
Hence, we will use interchangeably the terms control signal
and schedule, and the terms controller and scheduler.

III. STATE FEEDBACK GREEN SCHEDULING
Periodic scheduling was utilized in our previous work

[6] for Green Scheduling. While being simple and scalable,
periodic scheduling does not take into account the influence
of disturbances nor feedback information from the plant,
hence it is unsuitable when large disturbances are present.
In this section, we will develop a state feedback Green
Scheduling strategy that can handle large disturbances.

We first define several essential notions. Consider the
dynamical system in Eq. (4). In practice, the disturbances
d are always bounded. Therefore, we assume that d is
constrained in a convex and compact subset D. The peak
demand constraint k on the control inputs u is generalized
as a non-empty finite constraint set U ? {0, 1}n of valid
control inputs, meaning that u(t) ? U for all t ? 0. When u
is constrained by k, U := {u ? {0, 1}n : ?u?1 ? k}.

A disturbance signal d(�) is admissible if it satisfies d(t) ?
D ?t ? 0. Similarly, an admissible control signal u(�)
satisfies u(t) ? U ?t ? 0. Given admissible signals d(�) and
u(�), a state trajectory x(�) of the system is a continuous
signal that satisfies Eq. (4) at all time. For any initial state
x(0) ? Rn, the state trajectory x(�) exists and is unique [9].

Because we are only interested in the zone temperatures,
let us define the output vector y = [T1, . . . , Tn]T . Obviously
y = Cx with an appropriate matrix C ? Rn�2n. Let Safe ?
Rn denote a compact set of safe, or desired, outputs of the
system. For the radiant system in Section II, Safe specifies
the desired zone temperature ranges: Safe := [l1, h1]׷ � ��
[ln, hn]. Our goal is to devise a scheduling strategy so that

from any initial state x(0), the output y(�) is always driven to
Safe in finite time and is maintained inside Safe indefinitely,
regardless of the disturbances. Such an output trajectory is
said to be (eventually always) safe and is defined below.

Definition 1: An output trajectory y(�) is (eventually al-
ways) safe if there exists a finite time 0 ? ? < +? such
that y(t) ? Safe for all t ? ? .
A. State Feedback Scheduling Strategy

A state feedback scheduling strategy is a feedback law
? : X ? U . The resulting state trajectory x(�) satisfies the
closed-loop differential equation x?(t) = A(?(x(t)))x(t) +
B?(x(t)) +Wd(t), ?t ? 0, with initial state x(0). To solve
Problem 1, we aim to find a feedback law such that from
any initial state and for any admissible disturbance signal,
the resulting output trajectory is safe. Such a feedback law
is called a safe feedback law (or safe feedback scheduling
strategy). To this end, we use the concept of attracting sets
of control systems [10]. However, to satisfy the finite-time
requirement in Definition 1, we modify the definition of
attracting sets as follows.

Definition 2: A set A ? X is a (finite-time) attracting set
of control system (4) and a set B ? X is a basin of attraction
of A if there exists a feedback law ? : X ? U such that
for any initial state x(0) ? B and any admissible disturbance
signal d(�), the resulting state trajectory x(�) satisfies x(t) ?
A, ?t ? ? for some finite ? ? 0.

It is readily seen that if an attracting set A satisfies
CA = {Cx : x ? A} ? Safe then there exists a safe
state feedback scheduling strategy for all x(0) ? B. The
following result allows us to determine an attracting set of
control system (4) and an associated safe feedback law.

Theorem 1: Let xc ? X be a given point. Suppose there
exist M ? R2n�2n, ? > 0 and ? > 0 such that
M ? CTC, M ? 0, (5a)
A(u)TM +MA(u) ? ?2?M ?u ? U , (5b)
? > 1? max

zTMz=1
min
u?U

max
d?D

zTM(A0xc + B�u+Wd) (5c)

where B� = [b1+A1xc, . . . , bn+Anxc] and bi is the ith col-
umn of B. Then A := {x ? Rn : (x? xc)T M (x? xc) ?
?2} is an attracting set of control system (4) with basin
B = X and with state feedback law ?(�) given by

?(x) = argminu?U (x? xc)TMB�u, ?x ? X . (6)
In addition, if the ball B(Cxc, ?) = {y : ?y ? Cxc?2 ? ?}
is a subset of Safe then ?(�) is a safe state feedback law.

Due to the page limit, the proof of Theorem 1 is omitted.
M and ? satisfying Eqs. (5a) and (5b) can be computed
by solving a Generalized Eigenvalue Problem (GEVP) [11],
e.g., using the function gevp of the Robust Control Toolbox
of MATLAB [12]. We remark that the function V (x) :=
(x? xc)T M (x? xc) is a robust control Lyapunov function
of the control system [13]. Eq. (5c) essentially means that
for any x outside A, i.e., V (x) > ?2, u = ?(x) is such that,
for all d ? D, V always decays along the system�s flow at a
rate not slower than ??, for some constant ? > 0. A detailed
argument can be found in [14].



x? = x(t)

BM(x
?, r)

x(t?)
with u?

BM(x(t
?), r?)

with u?

Fig. 3: Illustration of the event-based scheduling strategy.

Theorem 1 immediately gives us a safe feedback schedul-
ing strategy. Note that we only need to re-compute u when
x is outside A. Once x is inside A, we can simply keep
the current control vector u until x hits the boundary of A.
Therefore, a basic feedback scheduling strategy is given by

u(t)=

{
argminu?U (x(t)?xc)TMB�u if V (x(t))??2
u(t?) otherwise

(7)

in which u(t?) denotes the currently used control vector. We
note that M , xc and ? are pre-computed and fixed.

The basic scheduling strategy has several limitations:
1) It requires solving the minimization (7) continuously

in real time, which is impractical.
2) It usually results in a high-frequency sliding mode

control signal [15], when u switches rapidly and re-
peatedly between control vectors u? and u??, causing
x to slide along the boundary of two regions of state:
one in which u? is optimal and the other in which u??

is optimal. This effect is often undesirable in practice.

IV. EVENT-BASED GREEN SCHEDULING
To overcome the aforementioned limitations of the basic

feedback law, we will develop in this section an event-based
scheduling strategy based on the basic strategy.

Consider any x outside A. The feedback law in Eq. (7)
essentially finds a control that always reduces V (x(t)) at the
fastest rate possible. However, it is certainly sufficient to use
a control that reduces V (x(t)) at a rate not slower than ??.
That is, as long as the current control, denoted u?, satisfies
maxd?D V? (x(t)) ? ?? then we do not need to compute and
switch to a new control. This observation leads to an event-
triggered scheme where we only switch the control when it
does not satisfy the previous inequality at the current state.

Define the norm ?x?M :=
?
xTMx. Let t be the current

time and x? = x(t) be the current state. Instead of solving
the optimization maxd?D V? (x(t)) continuously in real time,
we find a ball BM (x?, r) := {x : ?x? x??M ? r} with
radius r > 0 around x? so that for all x ? BM (x?, r),
max
d?D

V? =max
d?D

2(x?xc)TM(A(u?)x+Bu?+Wd)???. (8)
Once r is determined, we can keep u(t?) = u? for t? ? t as
long as x(t?) ? BM (x?, r) and only compute a new control
by Eq. (7) when the event ?x(t?)? x??M ? r is detected.
This event-triggered scheme is illustrated in Fig. 3. The
radius r can be estimated using the following propositions.

Proposition 1: The radius r is bounded below by

r1 = ? +
1
2

(
? ?

?
?2 + 4?? + 4?? +

2?
?

)
(9)

where ?= maxd?D(x? ? xc)TM(A(u?)xc +Bu? +Wd),
?=?x? ? xc?M , ?= 1? maxd?D ?A(u?)xc+Bu?+Wd?M .

TABLE II: Parameter values for a zone in the simulation.

Space length, width, height: 6m� 6m� 3m
External/internal pipe diameter: 20/15mm
Mass flow rate (per slab area): 15 kg/(h m2)
1/Kij (only for adjacent zones): [0.16, 0.20] (K m2/W)
Desired zone temperature range: [22, 26] (?C)
Fac�ade area: 18m2 Internal-wall area: 36m2
Thickness of concrete slab: 250mm Pipe spacing: 200mm
1/Kw,i ? [0.05, 0.07] (K m2/W) 1/Ki ? [2.1, 2.2] (K m2/W)
1/Kr,i ? [0.124, 0.130] (K m2/W) Tw,i = 18 ?C
Ci ? [1900, 2100] (kJ/K) Cr,i ? [3000, 4000] (kJ/K)

Proposition 2: The radius r is bounded below by

r2 = ? ?/2 + ??(R?1)TATM(x? ? xc)?2 + ?
(10)

in which nonsingular upper-triangular matrix R satisfying
RTR =M is determined by the Cholesky decomposition of
M , ? = maxd?D(x??xc)TM(A(u?)x?+Bu?+Wd), and
? = maxd?D ?A(u?)x? +Bu? +Wd?M .
Although Eq. (10) has a negation sign, it is usually the case
that ? < ??/2, hence r2 is usually positive. The radius r
is then estimated as the larger value of r1 and r2. Note that
M , ?, ?, xc, u?, and x? are all fixed. Thus the optimization
programs involved in those results are convex quadratic and
linear programs, which can be solved efficiently.

A. Improved Scheduling with Disturbance Prediction

If short-term disturbance predictions are available, they
can be utilized to improve Green Scheduling. Specifically,
suppose that at any time t ? 0, a prediction of the disturbance
constraint set can be obtained for a finite time horizon
h > 0, that is d(?) ? D[t,t+h] ?? ? [t, t + h], where
D[t,t+h] is known and is not larger than D. Then the event-
based scheduling strategy can be modified to exploit this
information by replacing D by D[t,t+h] in calculations of r1
and r2, which often results in larger estimates of r;

V. SIMULATION EXAMPLE

We consider a building of 10 zones of equal size. Five of
them face north while the other five face south. The building
is cooled by a hydronic radiant cooling system with the same
configuration as described in Section II and illustrated in
Fig. 1. The supply water temperature is the same for all
zones and is fixed at a predetermined value Tw.

For each zone, we used the parameter values from [7] for
a typical office building, which are summarized in Table II.
Because the parameters in [7] are for a single zone, while
the considered building has 10 zones, we varied several
parameter values for each zone uniformly randomly around
the nominal values in [7]. The ranges for these parameters are
also reported in Table II. For the corner zones that have two
external walls, we increased Ki accordingly. The comfort
range of zone temperature is 22 ?C to 26 ?C for all zones.

A. Disturbances

We assume that weather forecasts give us the ranges of
Ta and qs,i for each zone i at any given time during the
day. The constraint of qg,i for each zone i can be calculated



25

30

35

T
a
R
an
ge

(?
C
)

0

5

10

q g
,i
R
an
ge

(W
/m

2
)

0 2 4 6 8 10 12 14 16 18 20 22 24
0

5

10

Hour of Day

q s
,i
R
an
ge

(W
/m

2
)

North zones
South zones

Fig. 4: Time-varying constraints of the disturbances Ta (top),
qg,i (middle) and qs,i (bottom) affecting each zone.

0 2 4 6 8 10 12 14 16 18 20 22 24

20

22

24

26

28

A B

Hour of Day

Te
m
pe
ra
tu
re

(?
C
) Zone air Core

Fig. 5: Air and core temperatures of zone 1 for the unsafe
uncoordinated intermittent operation.

based on the power rates of the equipment and lights in the
zone as well as its occupants� schedules. In this example,
the time-varying constraints of Ta, qg,i and qs,i, for every
i ? {1, . . . , 10}, are given in Fig. 4. Note that between the
north zones and the south zones, their solar radiation heat
gain profiles are different, as clearly displayed in Fig. 4.
Each disturbance signal for each zone was then generated
uniformly randomly within its given time-varying constraint.

B. Uncoordinated Intermittent Operation

We considered the uncoordinated intermittent operation of
the pumps as the baseline. For each zone i, its pump is
switched on/off with hysteresis independently of the other
zones as follows: whenever Ti ? 26 ?C, ui = 1; whenever
Ti ? 22 ?C, ui = 0. This simple control strategy was
simulated in MATLAB for 24 hours. The initial temperatures
for all zones were set to 27 ?C. The resulting air temperature
T1 and core temperature Tc,1 of zone 1 are plotted in Fig. 5.

As obviously seen in Fig. 5, T1 dropped below 22 ?C, to
as low as about 21 ?C, twice during the day. For example,
between points A and B in Fig. 5, T1 was below 22 ?C for
more than 5 hours. This phenomenon can be explained by the
high thermal inertia of the radiant system. When the pump
was switched off (point A), the slab stopped being cooled;
however the core temperature was still significantly below the
zone temperature, causing the air to continue being cooled.

To avoid this issue, we increased the pumps� switching-
off thresholds by 1.15 ?C to 23.15 ?C. The new simulation

20

22

24

26

28

Te
m
pe
ra
tu
re

(?
C
) Zone air Core

0 2 4 6 8 10 12 14 16 18 20 22 24
0

1

Hour of Day

Pu
m
p
st
at
us

Fig. 6: Temperatures (top) and pump�s status (bottom) of
zone 1 for the safe uncoordinated intermittent operation.

results are reported in Fig. 6. Clearly T1 was driven to and
maintained within the desired range (gray-filled area).

Because the pumps� power information was not provided
in [7], we assumed a normalized power demand of 1 (power
unit) for each pump. Fig. 8 shows the normalized total power
demand of the pumps (dashed line). Evidently, the demand
fluctuated significantly during the day and attained a high
peak of 9 during the on-peak hours between 3:30 PM and
5:45 PM. A high peak of 10 was attained before 3:00 AM due
to the uncoordinated initialization of the zonal controllers.
Thus, under a demand-based electricity tariff, it would incur
a high demand cost. The total energy consumption of the
pumps was 62.5 (power unit � hour).

C. Event-based Green Scheduling

To flatten out the high peak demand incurred by the
uncoordinated intermittent operation, the event-based Green
Scheduling strategy was applied. We assumed that at any
time, one-hour predictions of the disturbance constraints
were available to the scheduler. Therefore, we used the
improved scheduling strategy with disturbance prediction,
presented in Section IV-A. The peak constraint on the
control inputs was chosen to be k = 2 at all time. A
MATLAB simulation was performed for 24 hours with the
same disturbance profiles and the same initial temperatures
as in Section V-B. Fig. 7 plots the simulation results. It is
clear that T1 was safe as it was driven to and maintained
inside the desired range (gray-filled area). Compared to the
uncoordinated scheduling, the pump of zone 1 was switched
on and off twice as often, however its switching frequency
was still slow (less than once every 3 hours). Each iteration
of the event-based scheduling algorithm took an average of
about 16ms on MATLAB on a MacbookPro computer with
2.26GHz Core 2 Duo processor and 4GB RAM.

The total power demand of the pumps is plotted in Fig. 8
in comparison with that for the uncoordinated operation.
Evidently, the peak demand was flattened out and signif-
icantly reduced. In fact, the demand of Green Scheduling
was constant at 2 for most of the day. The normalized peak
demand and total energy consumption of both scheduling
strategies are compared in Table III. Note that we only report
the peaks during the on-peak hours. By applying Green
Scheduling, we saved 77.8% in peak demand and 31.2% in



20

22

24

26

28

Te
m
pe
ra
tu
re

(?
C
) Zone air Core

0 2 4 6 8 10 12 14 16 18 20 22 24
0

1

Hour of Day

Pu
m
p
st
at
us

Fig. 7: Temperatures (top) and pump�s status (bottom) of
zone 1 for the event-based Green Scheduling algorithm.

0 2 4 6 8 10 12 14 16 18 20 22 24
0

5

10

Hour of Day

N
or
m
al
iz
ed

Pu
m
p

Po
w
er

D
em

an
d

Fig. 8: Normalized total power demand for Green Scheduling
(solid) and uncoordinated scheduling (dashed) strategies.

total energy consumption. Under a demand-based electricity
tariff, this would amount to a large saving in electricity cost.

VI. RELATED WORK

There are various approaches to balance the power con-
sumption in buildings and to flatten out peaks in power
demand, e.g., load shifting and load shedding [16]. However,
they operate on coarse grained time scales and do not
guarantee any thermal comfort. Model Predictive Control
(MPC) has received increasing attention in energy efficient
control and peak electricity demand reduction for commer-
cial buildings [17], [18]. Although MPC can be used for
the Green Scheduling problem, the combinatorial nature of
the control inputs usually results in mixed integer programs,
which can be expensive computationally to solve. Hence we
opted for a simple scheduling approach in this paper.

Essentially, the Green Scheduling problem is a safe control
problem for switched systems [19]. There has been a vast
literature on safe switching controller synthesis for switched
systems (see e.g., [20]). However, most of these synthesis
methods are not scalable in terms of the number of discrete
modes and the dimension of the state space.

Event-triggering has been used in control theory since the
end of the nineties for efficient implementations of control
laws in situations where limited resources, such as actua-
tion and network communication, are shared among several
subsystems [21]. Similar to this paper, control Lyapunov

TABLE III: Normalized peak demand and total energy
consumption of both scheduling strategies.

Peak Consumption

Uncoordinated scheduling 9 62.5
Green Scheduling 2 (-77.8%) 43 (-31.2%)

functions were usually used in the literature for deriving the
event-triggering conditions.

VII. CONCLUSION
In this paper, we developed an event-based state feedback

scheduling strategy for the Green Scheduling problem. Un-
like the periodic scheduling approach used in our previous
work, this new scheduling algorithm directly takes into
account the influence of disturbances. It was applied to
scheduling intermittent operations of pumps of hydronic
radiant systems, and was shown through simulations in
MATLAB to be effective for reducing the peak electricity
demand of the pumps.

For future work, we aim to develop distributed and hier-
archical scheduling algorithms for peak demand reduction in
extremely large systems. We are also investigating scheduling
strategies based on dynamic pricing models that can lead to
demand cost reduction in a more on-line fashion.

REFERENCES
[1] R. Watson and K. Chapman, Radiant Heating and Cooling Handbook,

ser. McGraw-Hill Handbooks. McGraw-Hill, 2002.
[2] D. Saelens, W. Parys, and R. Baetens, �Energy and comfort per-

formance of thermally activated building systems including occupant
behavior,� Building & Environment, vol. 46, no. 4, pp. 835�848, 2011.

[3] B. Olesen, �Radiant floor heating in theory and practice,� ASHRAE
Journal, vol. 44, no. 7, pp. 19�24, 2002.

[4] T. Y. Chen, �Application of adaptive predictive control to a floor
heating system with a large thermal lag,� Energy and Buildings,
vol. 34, no. 1, pp. 45�51, 2002.

[5] M. Gwerder, J. To�dtli, B. Lehmann, V. Dorer, W. Gu�ntensperger, and
F. Renggli, �Control of thermally activated building systems (TABS) in
intermittent operation with pulse width modulation,� Applied Energy,
vol. 86, no. 9, pp. 1606�1616, 2009.

[6] T. X. Nghiem, M. Behl, G. J. Pappas, and R. Mangharam, �Green
scheduling for radiant systems in buildings,� in IEEE CDC, 2012.

[7] M. Gwerder, B. Lehmann, J. To�dtli, V. Dorer, and F. Renggli, �Control
of thermally activated building systems (TABS),� Applied Energy,
vol. 85, pp. 565�581, 2008.

[8] R. A. Horn and C. R. Johnson, Matrix analysis. Cambridge University
Press, 1990.

[9] W. J. Rugh, Linear System Theory. Prentice-Hall, 1996.
[10] H. K. Khalil, Nonlinear systems. Macmillan New York, 1992.
[11] S. Boyd, L. E. Ghaoui, E. Feron, and V. Balakrishnan, Linear matrix

inequalities in system and control theory. SIAM, 1994.
[12] The MathWorks, �Robust control toolbox,� 2012.
[13] Z. Artstein, �Stabilization with relaxed controls,� Nonlinear Analysis:

Theory, Methods & Applications, vol. 7, no. 11, pp. 1163�1173, 1983.
[14] T. X. Nghiem, �Green scheduling of control systems,� Ph.D. disserta-

tion, University of Pennsylvania, 2012.
[15] C. Edwards and S. Spurgeon, Sliding Mode Control: Theory And

Applications. Taylor & Francis, 1998.
[16] K. ho Lee and J. E. Braun, �Development of methods for determining

demand-limiting setpoint trajectories in buildings using short-term
measurements,� Building & Environment, vol. 43, no. 10, 2008.

[17] Y. Ma, F. Borrelli, B. Hencey, B. Coffey, S. Bengea, and P. Haves,
�Model predictive control for the operation of building cooling sys-
tems,� in Proc. ACC, 2010, pp. 5106�5111.

[18] F. Oldewurtel, A. Ulbig, A. Parisio, G. Andersson, and M. Morari,
�Reducing peak electricity demand in building climate control using
real-time pricing and model predictive control,� in Proceedings of the
IEEE Conference on Decision and Control, 2010, pp. 1927�1932.

[19] H. Lin and P. J. Antsaklis, �Stability and stabilizability of switched
linear systems: A survey of recent results,� IEEE Trans. Autom.
Control, vol. 54, no. 2, pp. 308�322, 2009.

[20] E. Asarin, O. Bournez, T. Dang, O. Maler, and A. Pnueli, �Effective
synthesis of switching controllers for linear systems,� Proc. IEEE,
vol. 88, no. 7, pp. 1011�1025, 2000.

[21] P. Tabuada, �Event-triggered real-time scheduling of stabilizing control
tasks,� IEEE Trans. Autom. Control, vol. 52, no. 9, 2007.


