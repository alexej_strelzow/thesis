package at.ac.tuwien.isis.usermodels.evaluation.test;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.evaluation.test.config.TestConfiguration;
import org.apache.commons.collections4.Bag;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * Created by Alexej Strelzow on 19.07.2016.
 */
public class ModelEqualityTest {

    @Test
    public void testModelEquality_shouldBeEqual() throws Exception{
        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + "/equality_test/");

        DocumentModelDTO modelPdfDTO = null;
        DocumentModelDTO modelTxtDTO = null;

        for(File model : modelsDir.listFiles()) {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(model));
            if (model.getName().contains("pdf")) {
                modelPdfDTO = (DocumentModelDTO) is.readObject();
            } else {
                modelTxtDTO = (DocumentModelDTO) is.readObject();
            }
        }
        final Bag<TaggedWordWrapper> model1 = modelPdfDTO.getModel();
        final Bag<TaggedWordWrapper> model2 = modelTxtDTO.getModel();

        Assert.assertEquals(model1.size(), model2.size());

        model2.removeAll(model1);

        Assert.assertTrue(model1.isEmpty());
        // todo: investigate Disclosure vs disclosure
    }

}
