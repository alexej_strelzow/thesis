package at.ac.tuwien.isis.usermodels.evaluation.test;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.test.config.TestConfiguration;
import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetServiceImpl;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.config.ModelGenerationConfiguration;
import at.ac.tuwien.isis.usermodels.service.langmodel.*;
import at.ac.tuwien.isis.usermodels.service.ner.NerServiceImpl;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserService;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserServiceImpl;
import at.ac.tuwien.isis.usermodels.service.protocol.DummyProtocolServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.TokenizerServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
public class ModelGeneratorTest {

    private PdfParserService pdfExtractor;
    private UserModelServiceImpl userModelService;
    private DocumentModelServiceImpl documentModelService;
    private ModelComparatorServiceImpl comparatorService;

    @Before
    public void init() {
        pdfExtractor = new PdfParserServiceImpl();

        userModelService = new UserModelServiceImpl();
        userModelService.setModelGenerationConfiguration(new ModelGenerationConfiguration());
        final BabelNetServiceImpl babelNetService = new BabelNetServiceImpl();
        babelNetService.setBabelNetConfiguration(new BabelNetConfiguration());
        userModelService.setBabelNet(babelNetService);
        userModelService.setConfiguration(new Configuration());
        userModelService.setNerService(new NerServiceImpl());
        userModelService.setTokenizer(new TokenizerServiceImpl());
        userModelService.setFwlService(new FwlServiceImpl());
        userModelService.setPreprocessor(new PreprocessorServiceImpl());
        userModelService.setProtocolService(new DummyProtocolServiceImpl());

        documentModelService = new DocumentModelServiceImpl();
        documentModelService.setModelGenerationConfiguration(new ModelGenerationConfiguration());
        userModelService.setBabelNet(babelNetService);
        documentModelService.setConfiguration(new Configuration());
        documentModelService.setNerService(new NerServiceImpl());
        documentModelService.setTokenizer(new TokenizerServiceImpl());
        documentModelService.setFwlService(new FwlServiceImpl());
        documentModelService.setPreprocessor(new PreprocessorServiceImpl());
        documentModelService.setProtocolService(new DummyProtocolServiceImpl());

        comparatorService = new ModelComparatorServiceImpl();
        comparatorService.setConfiguration(new Configuration());
        comparatorService.setFwlService(new FwlServiceImpl());
        comparatorService.setProtocolService(new DummyProtocolServiceImpl());
        comparatorService.setBabelNetService(babelNetService);
    }

    @Test
    public void extractAndSavePappas() throws IOException {
        File rootDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.PDF);
        if (!rootDir.isDirectory()) {
            Assert.fail();
        }

        File papersDir = new File(rootDir + "/pappas");
        if (!papersDir.isDirectory()) {
            Assert.fail();
        }

        File targetDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT + "/pappas");
        if (!targetDir.exists()) {
            targetDir.mkdir();
        }

        for (File paper : papersDir.listFiles()) {
            File newPaper = new File(targetDir, paper.getName().replace(".pdf", ".txt"));
            newPaper.setWritable(true);
            newPaper.delete();

            PrintWriter pw = new PrintWriter(new FileOutputStream(newPaper), true);
            pw.print(pdfExtractor.extract(paper).getText());
            pw.flush();
            pw.close();
        }

    }

    @Test
    public void createUserModels_Pappas() throws IOException, ClassNotFoundException {
        final UserModelDTO userModel = userModelService.create(TestConfiguration.RESOURCE_ROOT + TestConfiguration.PDF + "/pappas");

        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS + "/pappas");
        if (!modelsDir.exists()) {
            modelsDir.mkdirs();
        }

        create(modelsDir, "pappas.ser", userModel, null);
    }

    @Test
    public void createDocumentModel_Cancer() throws IOException, ClassNotFoundException {
        final DocumentModelDTO documentModel = documentModelService.create(TestConfiguration.RESOURCE_ROOT + TestConfiguration.PDF + "/medicine/cancer_paper.pdf");

        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.DOCUMENT_MODELS + "/medicine/");
        if (!modelsDir.exists()) {
            modelsDir.mkdirs();
        }

        create(modelsDir, "cancer.ser", null, documentModel);
    }

    private void create(File modelsDir, String modelName, UserModelDTO userModel, DocumentModelDTO documentModel) throws IOException, ClassNotFoundException {
        FileOutputStream fout = new FileOutputStream(new File(modelsDir, modelName));
        ObjectOutputStream oos = new ObjectOutputStream(fout);

        if (userModel != null) {
            oos.writeObject(userModel);

            ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(modelsDir, modelName)));
            UserModelDTO model = (UserModelDTO) is.readObject();

            Assert.assertEquals(userModel.getModel().size(), model.getModel().size());
            Assert.assertEquals(userModel.getModel().uniqueSet().size(), model.getModel().uniqueSet().size());
            Assert.assertEquals(String.valueOf(userModel.getUnseenProbability()), String.valueOf(model.getUnseenProbability()));

        } else {
            oos.writeObject(documentModel);

            ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(modelsDir, modelName)));
            DocumentModelDTO model = (DocumentModelDTO) is.readObject();

            Assert.assertEquals(documentModel.getModel().size(), model.getModel().size());
            Assert.assertEquals(documentModel.getModel().uniqueSet().size(), model.getModel().uniqueSet().size());
        }
    }
}
