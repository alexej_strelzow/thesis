package at.ac.tuwien.isis.usermodels.evaluation.test;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.service.experiment.ExperimentServiceImpl;
import at.ac.tuwien.isis.usermodels.service.protocol.FileProtocolServiceImpl;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
public class TestExperiments {

    private static final String USER_MODELS = TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS;
    private static final String DOCUMENT_MODELS = TestConfiguration.RESOURCE_ROOT + TestConfiguration.DOCUMENT_MODELS;

    @Test
    public void extractPappas() throws Exception {
        final ModelGeneratorTest generatorUtils = new ModelGeneratorTest();
        generatorUtils.extractAndSavePappas();
    }

    @Test
    public void createPappas() throws Exception {
        new ModelGeneratorTest().createUserModels_Pappas();

        testPappasDeserialization();
    }

    @Test
    public void testPappasDeserialization() throws Exception {
        final UserModelDTO pappas = getPappas();

        //final String userName = pappas.getUserName();
        //Assert.assertEquals("pappas", userName);

        final Set<String> containingDocs = pappas.getContainingDocs();
        Assert.assertNotNull(containingDocs);
        Assert.assertTrue(containingDocs.size() == 5);

        final double avgDocLength = pappas.getAvgDocLength();
        Assert.assertTrue(avgDocLength > 5000);
        System.out.println("avgDocLength: " + avgDocLength);

        final double removedTokens = pappas.getAvgRemovedTokens();
        Assert.assertTrue(removedTokens > 100);
        System.out.println("getAvgRemovedTokens: " + removedTokens);
    }

    @Test
    public void testPappas() throws Exception {
        final ComparisonResultDTO resultDTO = new ExperimentServiceImpl().run(getPappas(), getCancer(), new FileProtocolServiceImpl());

        Assert.assertTrue(resultDTO.getUnknownWords().size() > 0);
        System.out.println("getUnknownWords: " + resultDTO.getUnknownWords().size());

        Assert.assertTrue(resultDTO.getKnownWordsUserModel().size() > 0);
        System.out.println("getKnownWordsUserModel: " + resultDTO.getKnownWordsUserModel().size());

        Assert.assertTrue(resultDTO.getKnownWordsCollection().size() > 0);
        System.out.println("getKnownWordsCollection: " + resultDTO.getKnownWordsCollection().size());

        // TODO: access protocol for experiment and get all statistics
    }

    private static UserModelDTO getPappas() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/pappas/pappas.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static DocumentModelDTO getCancer() throws IOException, ClassNotFoundException {
        String path = DOCUMENT_MODELS + "/medicine/cancer.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (DocumentModelDTO) is.readObject();
    }

    protected Map<String, String> getArticlesForUser(String user) throws IOException {
        File extractedTextDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT);

        for (File userDir : extractedTextDir.listFiles()) {
            if (userDir.isDirectory() && user.equals(userDir.getName())) {
                Map<String, String> articles = new HashMap<>();

                for (File paper : userDir.listFiles()) {
                    final List<String> strings = Files.readAllLines(paper.toPath(), Charset.forName("ISO-8859-1"));
                    articles.put(paper.getName(), StringUtils.join(strings.toArray(), "\r\n"));
                }

                return articles;
            }
        }

        return null;
    }

}
