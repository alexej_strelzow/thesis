
1 3

Cancer Chemother Pharmacol (2015) 76:925�932
DOI 10.1007/s00280-015-2850-4

ORIGINAL ARTICLE

Phase 1 study of oral TAS?102 in patients with refractory 
metastatic colorectal cancer

Johanna C. Bendell1 � Lee S. Rosen2 � Robert J. Mayer3 � Jonathan W. Goldman2 � 
Jeffrey R. Infante1 � Fabio Benedetti4 � Donghu Lin5 � Hirokazu Mizuguchi4 � 
Christopher Zergebel4 � Manish R. Patel6 

Received: 14 July 2015 / Accepted: 14 August 2015 / Published online: 14 September 2015 
� The Author(s) 2015. This article is published with open access at Springerlink.com

neutropenia). Therefore, RD was identified as 35 mg/m2 
bid. At RD, fatigue (63 %), gastrointestinal disturbances 
and nausea (46 %), vomiting (46 %), and diarrhea (42 %) 
were common but rarely grade 3/4. Grade 3/4 nausea, vom-
iting, and diarrhea occurred at 4 % each. Grade 3/4 toxic-
ity was predominantly hematologic [neutropenia (71 %), 
anemia (25 %)]; febrile neutropenia was observed in two 
patients. Stable disease lasting ?6 weeks was achieved 
by 16 evaluable patients (70 %); median progression-free 
survival and overall survival were 5.3 and 7.5 months, 
respectively.
Conclusions TAS-102 has an acceptable safety profile 
and preliminary evidence of disease stabilization in West-
ern patients with refractory mCRC. Results from a rand-
omized phase 3 study have shown survival benefit with dis-
ease stabilization evidence in this population.

Keywords Antimetabolite � Metastatic colorectal cancer � 
Phase 1 study � Recommended dose � Safety � TAS-102

Introduction

Colorectal cancer is the third most common malignancy 
and the second leading cause of cancer death in men and 
women in the USA [1, 2]. Fifty to sixty percent of patients 
diagnosed with colorectal cancer will develop metastatic 
disease [3]. For patients with unresectable metastatic colo-
rectal cancer (mCRC), initial treatment typically consists of 
an infusional 5-fluorouracil (5-FU) and leucovorin regimen 
containing irinotecan (FOLFIRI), oxaliplatin (FOLFOX), 
or both (FOLFOXIRI) [3�6]. Use of 5-FU, irinotecan, and 
oxaliplatin, regardless of their sequence in frontline and sec-
ond-line treatment, is associated with significantly improved 
survival [7]. The addition of monoclonal antibodies 

Abstract 
Purpose To evaluate safety of TAS-102 administered 
twice daily (bid) on days 1�5 and 8�12 of a 4-week cycle, 
confirm feasibility of the Japanese recommended dose 
(RD), 35 mg/m2, in Western patients with metastatic colo-
rectal cancer (mCRC) refractory to standard chemothera-
pies, and describe preliminary antitumor activity.
Methods This open-label, dose-escalation phase 1 study 
was conducted at four US centers. Patients were enrolled 
into two sequential cohorts [30 (cohort 1) or 35 mg/m2/
dose bid (cohort 2)]; dose-limiting toxicities (DLT) were 
evaluated during cycle 1 in dose-escalation cohorts. At RD, 
15 additional patients were enrolled in an expansion cohort.
Results Patients (N = 27) with refractory mCRC received 
TAS-102; 74 % had received ?4 prior regimens. DLT 
was not observed in three patients in cohort 1, and was 
in one out of nine patients in cohort 2 (grade 3 febrile 

 * Johanna C. Bendell 
 jbendell@tnonc.com

1 Sarah Cannon Research Institute/Tennessee Oncology, 
PLLC, 250 25th Avenue North, Suite 200, Nashville,  
TN 37203, USA

2 University of California, 2020 Santa Monica Blvd.,  
Suite 600, Los Angeles, CA 90404, USA

3 Dana-Farber Cancer Institute, 450 Brookline Ave.,  
Dana 1602, Boston, MA 02215, USA

4 Taiho Oncology Inc., 212 Carnegie Center, Suite 201, 
Princeton, NJ 08540, USA

5 Taiho Pharmaceutical of Beijing Co., Ltd., 9F-9A3,  
Hanwei Plaza, 7th, Guanghua Rd., Chaoyang Dist.,  
Beijing City 100004, China

6 Sarah Cannon Research Institute/Florida Cancer Specialists, 
600 N. Cattlemen Road, Suite 200, Sarasota, FL 34232, USA




926 Cancer Chemother Pharmacol (2015) 76:925�932

1 3

targeting vascular endothelial growth factor (bevacizumab) 
or epidermal growth factor receptor (cetuximab or pani-
tumumab) to chemotherapy has also led to improved out-
comes [8�10]. In the refractory setting, regorafenib has 
shown modest improvement in overall survival (OS) com-
pared with placebo [11]. More treatment options are needed 
for patients with disease progression after 5-FU, irinotecan, 
oxaliplatin, and available monoclonal antibodies.

TAS-102 is a novel, orally active, antineoplastic com-
bination drug consisting of trifluridine (FTD) and tipiracil 
hydrochloride (TPI) in a molar ratio of 1:0.5. FTD is a 
thymidine-based nucleoside analogue, which, in its triphos-
phate form, is incorporated into DNA causing single-strand 
and double-strand breaks (TS) [12, 13]. TPI is a potent 
inhibitor of thymidine phosphorylase, which prevents rapid 
degradation of FTD, thereby allowing sustained FTD con-
centrations to be maintained after oral administration [14]. 
The incorporation of FTD into DNA appears to be the pri-
mary mechanism of antitumor activity of TAS-102 com-
pared with 5-FU and other fluoropyrimidines that produce 
cytotoxic effects primarily via TS inhibition [15]. Consist-
ent with this difference, TAS-102 antitumor effects have 
been observed in colorectal cancer cell lines and xenograft 
models resistant to 5-FU and other fluoropyrimidines [16, 
17]. In preclinical models, TAS-102 induces higher levels 
of apoptosis compared with 5-FU and, unlike 5-FU, does 
not elicit an autophagic survival response in colorectal can-
cer cell lines [18].

Treatment on days 1�5 and 8�12 of a 4-week cycle was 
chosen as the recommended phase 2 schedule for TAS-102 
administration based on phase 1 clinical studies in patients 
with advanced solid tumors [19�21]. In Japanese patients, 
most of whom had mCRC, the maximum tolerated dose 
(MTD) on this schedule was 35 mg/m2/dose twice daily [22].

However, a previous phase 1 study conducted in the 
USA in heavily pretreated patients with breast cancer 
identified 25 mg/m2/dose twice daily as the MTD [23], 
although higher daily doses were found to be tolerable 
with other dosing schedules in patients with other advanced 
tumors, particularly gastrointestinal tumors [21]. Therefore, 
the present study was designed to evaluate the safety and 
determine whether the dose and schedule of TAS-102 used 
in the Japanese mCRC phase 2 trial is feasible in similar 
patients with refractory mCRC in the USA, and to obtain 
preliminary information about its antitumor activity.

Methods

Patients

Male or female patients aged ?18 years of age with his-
tologically or pathologically confirmed refractory mCRC 

were eligible if they had received at least two prior lines 
of conventional chemotherapy for metastatic disease, 
including a fluoropyrimidine, oxaliplatin, and irinotecan, 
had Eastern Cooperative Oncology Group (ECOG) perfor-
mance status 0 or 1, were able to take medications orally, 
had adequate organ function, and had a life expectancy of 
at least 3 months. Adjuvant therapy with a fluoropyrimidine 
and oxaliplatin could count as one line of prior therapy if 
disease recurred within 12 months of completion. Previ-
ous treatment with biologics targeting vascular endothelial 
growth factor, epidermal growth factor, or their receptors 
was allowed. Patients with known brain metastasis, acute 
systemic infection, grade ?3 ascites, or significant cardi-
ovascular disease; those who underwent major surgery or 
radiotherapy within the previous 4 weeks, had radiation to 
>25 % of marrow-bearing bone, or had received anticancer 
therapy within 3 weeks; and women who were pregnant or 
lactating, were excluded.

Study design

This open-label, non-randomized, sequential-group, dose-
escalation phase 1 study was conducted at four centers in 
the USA from September 2011 to April 2013. The data cut-
off date was April 3, 2013, which was 12 months after the 
last enrolled patient started treatment. The study was con-
ducted in accordance with ethical principles originating in 
the Declaration of Helsinki (2008) and in compliance with 
Good Clinical Practice and all local and national regulatory 
guidelines. Study approval was obtained from the institu-
tional review board at each site before that site enrolled any 
patients. All patients provided written informed consent 
before any screening procedures were conducted.

Eligible patients were assigned sequentially to two 
cohorts corresponding to TAS-102 doses of 30 mg/m2/dose 
twice daily (cohort 1) or 35 mg/m2/dose twice daily (cohort 
2) on days 1�5 and 8�12 of each 4-week cycle. Doses were 
taken orally within 1 h after completing morning and even-
ing meals.

Three patients were assigned initially to the first dose 
level using a conventional 3 + 3 dose-escalation design. 
If a dose-limiting toxicity (DLT) occurred during the first 
cycle in ?2 patients, the study was to be stopped. If DLTs 
were not observed in the first three patients (or in ?1 of 6 
patients), then six patients were to be enrolled at the sec-
ond dose level, and additional patients were allowed to be 
enrolled in any cohort at the discretion of the investigator 
and sponsor if further safety evaluation was warranted. 
DLTs were defined as grade 4 neutropenia lasting >7 days, 
febrile neutropenia (absolute neutrophil count <1000/mm3 
with a single body temperature of >38.3 �C [101 �F] or sus-
tained temperature of ?38 �C [100.4 �F] for more than 1 h), 
grade 4 thrombocytopenia or grade 3 thrombocytopenia 



927Cancer Chemother Pharmacol (2015) 76:925�932 

1 3

with bleeding, grade 3/4 nausea or vomiting lasting >48 h 
and uncontrolled by aggressive antiemetic therapy, grade 
3/4 diarrhea lasting >48 h and unresponsive to antidiar-
rheal medication, other grade 3/4 non-hematologic toxicity, 
any drug-related toxicity resulting in >1 week delay in ini-
tiation of cycle 2, or any drug-related toxicity preventing 
administration of ?80 % of the planned cumulative dose 
during cycle 1. The recommended dose (RD) for further 
clinical development was defined as the highest dose level 
at which <33 % of patients experienced a DLT during the 
first cycle. After establishing the RD, additional patients 
were enrolled into the expansion cohort in order to confirm 
safety and assess preliminary antitumor activity.

TAS-102 treatment was discontinued if patients experi-
enced disease progression, unacceptable toxicity, withdrew 
consent, or at the investigator�s discretion if felt to be clini-
cally indicated. Study treatment was supplied as 15- and 
20-mg immediate-release film-coated tablets; the number 
of tablets needed to achieve the assigned dose was deter-
mined according to the patient�s body surface area. Dose 
reductions were permitted in 5 mg/m2 decrements to a 
minimum dose of 20 mg/m2/dose in the event of treatment-
related toxicity (grade ?3 non-hematologic toxicity, grade 
4 neutropenia, or grade ?3 thrombocytopenia). Patients 
who did not meet resumption criteria within 14 days of the 
scheduled initiation of the next cycle were discontinued 
from treatment.

Other anticancer treatments, including palliative radio-
therapy, were not allowed. Prophylactic administration of 
granulocyte colony-stimulating factor (G-CSF), antidiar-
rheal agents, or antiemetic agents were not permitted dur-
ing cycle 1.

Assessments

Patients were monitored for adverse events throughout the 
study and for up to 30 days after the last dose. Adverse 
events were graded according to the National Cancer Insti-
tute Common Terminology Criteria for Adverse Events 
(version 4.03) and coded using the Medical Dictionary for 
Regulatory Activities (version 14.0). Other safety assess-
ments were made before each cycle and at the 30-day 
safety follow-up visit, including hematology and clinical 
chemistry (also on days 8, 15, and 22 of cycle 1 and days 
1 and 22 of subsequent cycles), physical examination, vital 
signs, performance status, and electrocardiogram (also on 
day 12 of cycle 1).

Tumor assessments and imaging studies of the chest, 
abdomen, and pelvis as clinically indicated were obtained 
at baseline (within 28 days before first dose) and then every 
two cycles. KRAS status was determined locally during 
screening. Tumor response was determined by the inves-
tigator according to RECIST criteria (version 1.1) [24]. 

Patient survival was determined every 2 months after the 
end of treatment.

Statistical analysis

The sample size was not based on formal hypothesis test-
ing, but was selected to obtain sufficient safety data in 
order to identify the RD for future clinical studies.

Safety was evaluated in all patients who received at least 
one dose of TAS-102. DLTs were evaluated in all patients 
in cohorts 1 and 2 who received at least 80 % of the planned 
study medication during cycle 1 (or lesser amounts due to 
occurrence of a DLT). Efficacy was assessed in all patients 
who completed at least one cycle of treatment and had clin-
ical or radiologic assessments for disease progression.

Statistical analyses were performed using SAS� soft-
ware (version 9.1; SAS Institute, Inc., Cary, NC). Safety 
parameters were summarized descriptively. Efficacy 
parameters were determined for all patients and by KRAS 
status. The best overall response was based on investiga-
tor assessment of radiologic images and classified as com-
plete response (CR), partial response (PR), stable disease 
(SD), or progressive disease (PD) according to RECIST 
criteria (version 1.1) [24]. SD required disease maintenance 
for at least 6 weeks without evidence of progression. Dis-
ease control rate (DCR) was defined as the proportion of 
patients with objective evidence of CR, PR, or SD and was 
summarized descriptively. Progression-free survival (PFS, 
defined as the time from the first dose of TAS-102 until 
radiologic disease progression or death) and OS were eval-
uated by the Kaplan�Meier method, and 95 % confidence 
intervals (CI) for median survival values were determined 
by the methods of Brookmeyer and Crowley [25].

Role of the sponsor

This study was sponsored, designed, and analyzed by Taiho 
Oncology, Inc. The investigators enrolled and treated the 
patients and collected the data. All authors had access to 
the data and vouch for its accuracy. A draft of the manu-
script was written by a medical writer who was compen-
sated by Taiho Oncology, Inc. The authors provided guid-
ance for the draft, comments, and feedback on subsequent 
revised versions. The corresponding author made the final 
decision to submit the manuscript for publication.

Results

Patient disposition and characteristics

A total of 27 patients received TAS-102, including three 
patients in cohort 1 (30 mg/m2/dose twice daily), nine 



928 Cancer Chemother Pharmacol (2015) 76:925�932

1 3

patients in cohort 2 (35 mg/m2/dose twice daily), and 15 
patients in the expansion cohort (35 mg/m2/dose twice 
daily) (Fig. 1). All treated patients from cohorts 1 and 2 
were included in the DLT evaluable population, and all 
patients were included in the safety evaluation. One patient 
without any post-baseline tumor assessments was excluded 
from the efficacy evaluation. At the time of data cutoff, one 
patient in the expansion cohort remained on TAS-102 treat-
ment. Most patients discontinued study treatment due to 
radiologic progression.

The study population had a median age of 64 years 
(range, 44�88), most were men (63 %) and white (93 %), 
and all had ECOG performance status of 0�1 (Table 1). 
The study population was heavily pretreated; the majority 
(74 %) had received ?4 prior regimens, and besides a fluo-
ropyrimidine, irinotecan, and oxaliplatin, most had received 
bevacizumab (96 %) and either cetuximab or panitumumab 
(52 %). KRAS status was determined for 25 patients [12 
(48 %) wild-type and 13 (52 %) KRAS mutant].

Dose?limiting toxicity

DLT was not observed among the three patients who 
received TAS-102 at a dose of 30 mg/m2/dose twice daily; 
therefore, the dose was escalated to 35 mg/m2/dose twice 
daily. Among the first six patients in cohort 2, one patient 
had a DLT of grade 3 febrile neutropenia, which was con-
sidered related to study treatment and resolved during cycle 
1. Three additional patients were enrolled in cohort 2 to 
confirm tolerability, and none had DLTs. Therefore, the RD 
was defined as 35 mg/m2/dose twice daily, and the expan-
sion cohort was enrolled to obtain additional safety data at 
this dose.

Exposure

Patients received TAS-102 for a median of three cycles 
(range, 1�14). Seven patients (26 %) initiated at least eight 
cycles of study treatment. Among the 24 patients who 
received 35 mg/m2 twice daily, the median cumulative dose 
was 2052 mg/m2 (range, 706�8949), median dose intensity 
was 154.2 mg/m2/week (range, 108.5�176.6), and relative 
dose intensity (i.e., ratio of actual to planned dose inten-
sity) was 0.89 (range, 0.62�1.01).

There were no dose reductions in cohort 1, although two 
of the three patients had the start of a cycle delayed by ?4 
(but <8) days. Ten patients (42 %) in the combined 35 mg/
m2 twice daily cohort had dose reductions by their last 
treatment cycle, including six patients with a single reduc-
tion to 30 mg/m2 twice daily and four patients with two 
dose reductions to 25 mg/m2 twice daily. Fourteen patients 
(58 %) had at least one cycle delayed by ?4 days (median, 
two cycles delayed per patient; range 1�4 cycles), and 
seven patients had delays of ?8 days (median, one cycle 
delayed per patient; range 1�2 cycles). Grade 3/4 neutrope-
nia was the most common reason for dose reductions and 
delays in cycle initiation.

Safety

Adverse events were observed in all 27 patients. Grade 3/4 
toxicity was predominantly hematologic, consisting mostly 
of neutropenia (70 %) and anemia (22 %) (Table 2). Febrile 
neutropenia was observed in only two patients (7 %). Pro-
phylactic G-CSF use was not permitted during cycle 1, but 
was allowed to treat hematologic toxicity according to the 
American Society of Clinical Oncology guidelines [26]; its 

3 pa?ents enrolled at dose 
level 1 (30 mg/m2 bid)

3 pa?ents discon?nued
- Radiologic progression (n=3)

Safety popula?on (n=3)
DLT evaluable popula?on (n=3)
Efficacy popula?on (n=3)

9 pa?ents enrolled at dose 
level 2 (35 mg/m2 bid)

9 pa?ents discon?nued
- Radiologic progression (n=4)
- Clinical disease progression (n=2)
- Adverse event (n=1)
- Elevated tumor markers (n=1)
- Physician decision (n=1)

Safety popula?on (n=9)
DLT evaluable popula?on (n=9)
Efficacy popula?on (n=8)
- Excluded due to no postbaseline

tumor assessments (n=1)

15 pa?ents enrolled in expansion 
cohort (35 mg/m2 bid)

14 pa?ents discon?nued
- Radiologic progression (n=9)
- Clinical disease progression (n=2)
- Adverse event (n=1)
- Progression (not specified) (n=1)
- Physician decision (n=1)

Safety popula?on (n=15)
Efficacy popula?on (n=15)

Fig. 1  Patient disposition



929Cancer Chemother Pharmacol (2015) 76:925�932 

1 3

use ranged from 20 % of patients (5/25) in cycle 2�67 % 
of patients (8/12) in cycle 4. Non-hematologic toxicity con-
sisted mainly of fatigue and gastrointestinal disturbances, 
but rarely was grade 3 or higher (Table 2). Major grade 3/4 
toxicities were neutropenia (70 %), anemia (22 %), and 
leukopenia (19 %). Most of the frequently reported adverse 
events, including hematologic abnormalities and gastroin-
testinal disturbances, were considered by the investigator to 
be related to study treatment.

There were no treatment-related deaths. One patient died 
of septic shock due to methicillin-resistant Staphylococcus 
aureus during cycle 12, which was considered unrelated to 
TAS-102 treatment. Serious adverse events occurred in six 
patients; two had treatment-related hematologic toxicity, 
and four had adverse events secondary to metastatic disease 
or other underlying conditions. Two patients discontinued 
TAS-102 due to adverse events (grade 3 bile duct stenosis 
and grade 2 diarrhea, respectively), and only one patient 
discontinued due to a treatment-related adverse event 
(grade 2 diarrhea).

Efficacy

The efficacy evaluation was conducted in 26 patients 
with post-baseline tumor assessments. Although no 
patients had a CR or PR, 17 patients achieved an SD last-
ing for >6 weeks, resulting in a DCR of 65.4 %. Seven of 
11 patients with wild-type KRAS (64 %) and eight of 13 
patients with mutant KRAS (62 %) achieved SD. Among 
the patients receiving 35 mg/m2 twice daily (RD level), 16 
out of 23 patients achieved an SD, resulting in a DCR of 
69.6 % (Fig. 2).

In the entire study population, median PFS and OS were 
4.1 months (95 % CI, 2.0�7.9) and 8.9 months (95 % CI, 
4.9�14.4), respectively (Fig. 3). Seven patients receiving 
TAS-102 at a dose of 35 mg/m2 twice daily did not have 
survival events by the data cutoff date and were censored 
at the date they were last known to be alive. Among the 
24 patients receiving 35 mg/m2, median PFS and OS were 
5.3 months (95 % CI, 2.4�8.0) and 7.5 months (95 % CI, 
4.6�14.4), respectively.

Table 1  Patient demographics and baseline characteristics

ECOG Eastern Cooperative Oncology Group
a Agents other than a fluoropyrimidine, irinotecan, and oxaliplatin; none had previously received regorafenib
b Mutations were identified in codon 12 [including G12V (n = 5), G12D (n = 4), and G12S (n = 1)] and in codon 13 [G13D (n = 3)]

Characteristic Dose level 1 (30 mg/m2) 
(n = 3)

Dose level 2 (35 mg/m2) 
(n = 9)

Expansion cohort (35 mg/m2) 
(n = 15)

All patients (N = 27)

Median age, years (range) 55 (51�64) 48 (44�75) 68 (50�88) 64 (44�88)

Gender, n (%)

 Male 0 7 (77.8) 10 (66.7) 17 (63.0)

 Female 3 (100) 2 (22.2) 5 (33.3) 10 (37.0)

Race, n (%)

 White 2 (66.7) 8 (88.9) 15 (100) 25 (92.6)

 Black/African-American 1 (33.3) 1 (11.1) 0 2 (7.4)

ECOG performance status, n (%)

 0 3 (100) 6 (66.7) 7 (46.7) 16 (59.3)

 1 0 3 (33.3) 8 (53.3) 11 (40.7)

Number of prior regimens, n (%)

 2 0 1 (11.1) 2 (13.3) 3 (11.1)

 3 0 1 (11.1) 3 (20.0) 4 (14.8)

 ?4 3 (100) 7 (77.8) 10 (66.7) 20 (74.1)
Prior therapy, n (%)

 Bevacizumab 3 (100) 8 (88.9) 15 (100) 26 (96.3)

 Cetuximab, or panitumumab 1 (33.3) 6 (66.7) 7 (46.7) 14 (51.9)

 Other agentsa 3 (100) 9 (100) 14 (93.3) 26 (96.3)

KRAS status, n (%)

 Wild type 1 (33.3) 5 (55.6) 6 (40.0) 12 (44.4)

 Mutantb 2 (66.7) 3 (33.3) 8 (53.3) 13 (48.1)

 Not determined 0 1 (11.1) 1 (6.7) 2 (7.4)



930 Cancer Chemother Pharmacol (2015) 76:925�932

1 3

Among patients with KRAS wild-type and mutant 
tumors, median PFS was 3.5 months (95 % CI, 2.0�7.9) 
and 4.1 months (95 % CI, 2.0�8.0), respectively, and 
median OS was 7.3 months (95 % CI, 4.9�11.5) and 
6.0 months (95 % CI, 4.0�14.4), respectively.

Discussion

In the initial phase 1 trial of TAS-102 in Japanese patients, 
the RD of 35 mg/m2 twice daily on days 1�5 and 8�12 

of a 28-day cycle produced a well-tolerated safety profile 
[22]. Common grade 3/4 adverse events were hematologic, 
including neutropenia (38 %) and leukopenia (33 %). This 
was similar to the safety profile observed in previous phase 
1 trials in Western patients, though one of these trials con-
ducted in patients with heavily pretreated breast cancer 
determined an MTD of 25 mg/m2 twice daily [19, 20, 23].

Based on preliminary antitumor activity observed in the 
cohort of patients with colorectal cancer treated in the Jap-
anese phase 1 study (DCR of 50 % [22]), a randomized, 
double-blind, placebo-controlled, phase 2 trial in Japan 
was conducted. Patients with metastatic colorectal adeno-
carcinoma who had received ?2 prior standard chemo-
therapy regimens were randomized 2:1 to receive either 
TAS-102 35 mg/m2 or placebo twice daily (days 1�5 and 
8�12 of 28-day cycles). Compared with placebo, TAS-102 
significantly improved OS (9.0 vs. 6.6 months, HR 0.56, 
P = 0.0011) and PFS with a manageable safety profile 
[27]. The present trial was designed to confirm the safety of 
the 35 mg/m2 twice daily dose used in the Japanese phase 
2 trial in the Western population of patients with colorectal 
cancer prior to embarking on a global phase 3 trial of TAS-
102 in patients with refractory mCRC. In this study, we 
treated a total of 27 patients, 74 % of whom had received 
?4 prior treatment regimens, in two cohorts: 30 mg/m2/
dose twice daily and 35 mg/m2/dose twice daily. There 
were no DLTs observed in the first cohort. One patient 
in the second cohort had dose-limiting grade 3 febrile 
neutropenia.

Similar to previous studies of TAS-102 conducted in 
both Japanese and Western populations, grade 3/4 toxic-
ity observed in this trial was predominantly hematologic. 
Compared with the Japanese randomized phase 2 trial in 
patients with colorectal cancer, rates of grade 3/4 neutro-
penia (71 vs. 50 %), febrile neutropenia (8 vs. 4 %), and 
anemia (25 vs. 17 %) were modestly increased among 
Western patients, though these findings may be due to the 

Table 2  Adverse events occurring at incidence ?20 % or grade 3/4 
adverse events occurring at incidence ?5 % regardless of relation to 
study treatment

ALP alkaline phosphatase

Adverse event, n (%) 35 mg/m2 (n = 24) All patients (N = 27)
Any grade Grade 3/4 Any grade Grade 3/4

Hematologic

 Neutropenia 19 (79.2) 17 (70.8) 21 (77.8) 19 (70.4)

 Anemia 11 (45.8) 6 (25.0) 11 (40.7) 6 (22.2)

 Thrombocytopenia 6 (25.0) 1 (4.2) 6 (22.2) 1 (3.7)

 Leukopenia 5 (20.8) 4 (16.7) 6 (22.2) 5 (18.5)

 Lymphopenia 3 (12.5) 2 (8.3) 3 (11.1) 2 (7.4)

 Febrile neutropenia 2 (8.3) 2 (8.3) 2 (7.4) 2 (7.4)

Non-hematologic

 Fatigue 15 (62.5) 0 16 (59.3) 0

 Nausea 11 (45.8) 1 (4.2) 13 (48.1) 1 (3.7)

 Vomiting 11 (45.8) 1 (4.2) 12 (44.4) 1 (3.7)

 Diarrhea 10 (41.7) 1 (4.2) 10 (37.0) 1 (3.7)

 Decreased appetite 10 (41.7) 0 10 (37.0) 0

 Constipation 6 (25.0) 0 6 (22.2) 0

 Blood ALP increased 4 (16.7) 2 (8.3) 4 (14.8) 2 (7.4)

 Hyponatremia 4 (16.7) 2 (8.3) 4 (14.8) 2 (7.4)

Fig. 2  Waterfall plot of target 
lesion change and best overall 
response. The asterisks denote 
patients who received TAS-
102 at a dose of 30 mg/m2 
twice daily. Four patients in 
the efficacy population did not 
have all target lesions evaluated 
post-baseline and are excluded 
from this graph. The second bar 
from the right denotes a patient 
who had progressive disease 
due to development of a new 
lesion despite shrinkage of the 
target lesion

*

TAS-102 (N=22)

Growth

Shrink
*

SD PD

C
ha

ng
e 

fr
om

 B
as

el
in

e 
(%

)

-30

-20

-10

0

10

20

30

40



931Cancer Chemother Pharmacol (2015) 76:925�932 

1 3

sample size differences between the two trials. In general, 
toxicities were manageable by appropriate dose delay and/
or reduction. Only one patient discontinued study treatment 
due to a drug-related adverse event. Scheduling prophylac-
tic G-CSF (except during cycle 1) may have avoided dose 
reductions in some patients who experienced a dose delay. 
The preliminary efficacy endpoints observed in this phase 1 
trial were a DCR of 65 % and a median OS of 8.9 months, 
which were similar to the results of the Japanese phase 2 
trial.

Subsequently, the global phase 3 RECOURSE trial of 
TAS-102 in patients with refractory mCRC who had at 
least two prior lines of treatment was initiated based on the 
RD established in this phase 1 trial. Results from this mul-
ticenter trial showed improvement in the primary endpoint 
of OS compared with placebo [28]. In conclusion, the dose 
of 35 mg/m2 twice daily is generally well tolerated in the 

treatment of patients with refractory mCRC in a Western 
population.

Acknowledgments The authors thank the patients and their families 
and study site personnel who participated in this study. This study was 
sponsored by Taiho Oncology Inc. Medical writing assistance was 
provided by Taiho Oncology Inc. Many thanks to Paul Bebeau, Gihan 
Atalla, Susan Darrah, and Hiroshi Ambe of Taiho, and John Ilgenfritz 
of United Biosources whose help was instrumental in the conduct of 
this study.

Compliance with ethical standards 

Conflict of interest J. C. Bendall, R. J. Mayer, J. W. Goldman, J. R. 
Infante, and M. R. Patel declare that they have no conflict of interest. 
L. S. Rosen�s institution (University of California, Los Angeles) has 
received funding to support this trial from Taiho Oncology. F. Bened-
etti, H. Mizuguchi, and C. Zergebel are employees of Taiho Oncology 
Inc. D. Lin is an employee of Taiho Pharmaceutical of Beijing Co., 
Ltd.

Fig. 3  a Kaplan�Meier plot 
of progression-free survival. b 
Kaplan�Meier plot of overall 
survival

27 N at Risk

Months from First Dose

%
 S

ur
vi

va
l

%
 S

ur
vi

va
l

= Censored Observations

26 18 14 12 10 6 6 5 2 2 2 2 0

0

10

20

30

40

50

60

70

80

90

100a

a

0 1 2 3 4 5 6 7 8 9 10 11 12 13

Months from First Dose

%
 S

ur
vi

va
l

b



932 Cancer Chemother Pharmacol (2015) 76:925�932

1 3

Open Access This article is distributed under the terms of the 
Creative Commons Attribution 4.0 International License (http://crea-
tivecommons.org/licenses/by/4.0/), which permits unrestricted use, 
distribution, and reproduction in any medium, provided you give 
appropriate credit to the original author(s) and the source, provide a 
link to the Creative Commons license, and indicate if changes were 
made.

References

 1. Siegel R, Ma J, Zou Z, Jemal A (2014) Cancer statistics, 2014. 
CA Cancer J Clin 64:9�29

 2. Siegel R, Desantis C, Jemal A (2014) Colorectal cancer statis-
tics, 2014. CA Cancer J Clin 64:104�117

 3. National Comprehensive Cancer Network (2014). NCCN clini-
cal practice guidelines in oncology (NCCN Guidelines�). Colon 
cancer, version 3. http://www.nccn.org. Accessed 26 March 2014

 4. Douillard JY, Cunningham D, Roth AD, Navarro M, James RD, 
Karasek P, Jandik P, Iveson T, Carmichael J, Alakl M, Gruia G, 
Awad L, Rougier P (2000) Irinotecan combined with fluorouracil 
compared with fluorouracil alone as first-line treatment for meta-
static colorectal cancer: a multicentre randomised trial. Lancet 
355:1041�1047

 5. Tournigand C, Andr� T, Achille E, Lledo G, Flesh M, Mery-Mig-
nard D, Quinaux E, Couteau C, Buyse M, Ganem G, Landi B, 
Colin P, Louvet C, de Gramont A (2004) FOLFIRI followed by 
FOLFOX6 or the reverse sequence in advanced colorectal can-
cer: a randomized GERCOR study. J Clin Oncol 22:229�237

 6. Falcone A, Ricci S, Brunetti I et al (2007) Phase III trial of infu-
sional fluorouracil, leucovorin, oxaliplatin, and irinotecan (FOL-
FOXIRI) compared with infusional fluorouracil, leucovorin, and 
irinotecan (FOLFIRI) as first-line treatment for metastatic colo-
rectal cancer: the Gruppo Oncologico Nord Ovest. J Clin Oncol 
25:1670�1676

 7. Grothey A, Sargent D, Goldberg RM, Schmoll HJ (2004) Sur-
vival of patients with advanced colorectal cancer improves with 
the availability of fluorouracil-leucovorin, irinotecan, and oxali-
platin in the course of treatment. J Clin Oncol 22:1209�1214

 8. Hurwitz H, Fehrenbacher L, Novotny W, Cartwright T, Hains-
worth J, Heim W, Berlin J, Baron A, Griffing S, Holmgren E, 
Ferrara N, Fyfe G, Rogers B, Ross R, Kabbinavar F (2004) Bev-
acizumab plus irinotecan, fluorouracil, and leucovorin for meta-
static colorectal cancer. N Engl J Med 350:2335�2342

 9. Van Cutsem E, K�hne CH, Hitre E, Zaluski J, Chang Chien 
CR, Makhson A, D�Haens G, Pint�r T, Lim R, Bodoky G, Roh 
JK, Folprecht G, Ruff P, Stroh C, Tejpar S, Schlichting M, Nip-
pgen J, Rougier P (2009) Cetuximab and chemotherapy as ini-
tial treatment for metastatic colorectal cancer. N Engl J Med 
360:1408�1417

 10. Peeters M, Price TJ, Cervantes A, Sobrero AF, Ducreux M, 
Hotko Y, Andr� T, Chan E, Lordick F, Punt CJ, Strickland AH, 
Wilson G, Ciuleanu TE, Roman L, Van Cutsem E, Tzekova V, 
Collins S, Oliner KS, Rong A, Gansert J (2010) Randomized 
phase III study of panitumumab with fluorouracil, leucovorin, 
and irinotecan (FOLFIRI) compared with FOLFIRI alone as sec-
ond-line treatment in patients with metastatic colorectal cancer. J 
Clin Oncol 28:4706�4713

 11. Grothey A, Van Cutsem E, Sobrero A et al (2013) Regorafenib 
monotherapy for previously treated metastatic colorectal cancer 
(CORRECT): an international, multicenter, randomised, pla-
cebo-controlled phase 3 trial. Lancet 381:303�312

 12. Emura T, Suzuki N, Yamaguchi M, Ohshimo H, Fukushima M 
(2004) A novel combination antimetabolite, TAS-102, exhibits 
antitumor activity in FU-resistant human cancer cells through a 

mechanism involving FTD incorporation in DNA. Int J Oncol 
25:571�578

 13. Suzuki N, Nakagawa F, Nukatsuka M, Fukushima M (2011) Tri-
fluorothymidine exhibits potent antitumor activity via the induc-
tion of DNA double-strand breaks. Exp Ther Med 2:393�397

 14. Fukushima M, Suzuki N, Emura T, Yano S, Kazuno H, Tada Y, 
Yamada Y, Asao T (2000) Structure and activity of specific inhibi-
tors of thymidine phosphorylase to potentiate the function of antitu-
mor 2?deoxyribonucleosides. Biochem Pharmacol 59:1227�1236

 15. Longley DB, Harkin DP, Johnston PG (2003) 5-fluorouracil: mech-
anisms of action and clinical strategies. Nat Rev Cancer 3:330�338

 16. Murakami Y, Kazuno H, Emura T, Tsujimoto H, Suzuki N, 
Fukushima M (2000) Different mechanisms of acquired resist-
ance to fluorinated pyrimidines in human colorectal cancer cells. 
Int J Oncol 17:277�283

 17. Emura T, Murakami Y, Nakagawa F, Fukushima M, Kitazato K 
(2004) A novel antimetabolite, TAS-102 retains its effects on 
FU-related resistant cancer cells. Int J Mol Med 13:545�549

 18. Bijnsdorp IV, Peters GJ, Temmink OH, Fukushima M, Kruyt FA 
(2010) Differential activation of cell death and autophagy results in 
an increased cytotoxic potential for trifluorothymidine compared 
with 5-fluorouracil in colon cancer cells. Int J Cancer 126:2457�2468

 19. Hong DS, Abbruzzese JL, Bogaard K, Lassere Y, Fukushima M, 
Mita A, Kuwata K, Hoff PM (2006) Phase I study to determine 
the safety and pharmacokinetics of oral administration of TAS-
102 in patients with solid tumors. Cancer 107:1383�1390

 20. Overman MJ, Varadhachary G, Kopetz S, Thomas MB, Fukush-
ima M, Kuwata K, Mita A, Wolff RA, Hoff P, Xiong H, Abbruzz-
ese JL (2008) Phase 1 study of TAS-102 administered once daily 
on a 5-day-per-week schedule in patients with solid tumors. 
Invest New Drugs 26:445�454

 21. Overman MJ, Kopetz S, Varadhachary G, Fukushima M, Kuwata 
K, Mita A, Wolff RA, Hoff P, Xiong H, Abbruzzese JL (2008) 
Phase I clinical study of three times a day oral administration of 
TAS-102 in patients with solid tumors. Cancer Invest 26:794�799

 22. Doi T, Ohtsu A, Yoshino T, Boku N, Onozawa Y, Fukutomi A, 
Hironaka S, Koizumi W, Sasaki T (2012) Phase I study of TAS-
102 treatment in Japanese patients with advanced solid tumors. 
Br J Cancer 107:429�434

 23. Green MC, Pusztai L, Theriault RL, Adinin RB, Hofweber M, 
Fukushima M, Mita A, Bindra N, Hortobagyi GN (2006) Phase 
I study to determine the safety of oral administration of TAS-
102 on a twice daily (BID) schedule for five days a week (wk) 
followed by two days rest for two wks, every (Q) four wks in 
patients (pts) with metastatic breast cancer (MBC). J Clin Oncol 
24(18S):10576

 24. Eisenhauer EA, Therasse P, Bogaerts J, Schwartz LH, Sargent D, 
Ford R, Dancey J, Arbuck S, Gwyther S, Mooney M, Rubinstein 
L, Shankar L, Dodd L, Kaplan R, Lacombe D, Verweij J (2009) 
New response evaluation criteria in solid tumours: revised 
RECIST guideline (version 1.1). Eur J Cancer 45:228�247

 25. Brookmeyer R, Crowley J (1982) A confidence interval for the 
median survival time. Biometrics 38:29�41

 26. Smith TJ, Khatcheressian J, Lyman GH et al (2006) 2006 update 
of recommendations for the use of white blood cell growth fac-
tors: an evidence-based clinical practice guideline. J Clin Oncol 
24:3187�3205

 27. Yoshino T, Mizunuma N, Yamazaki K, Nishina T, Komatsu Y, 
Baba H, Tsuji A, Yamaguchi K, Muro K, Sugimoto N, Tsuji Y, 
Moriwaki T, Esaki T, Hamada C, Tanase T, Ohtsu A (2012) TAS-
102 monotherapy for pretreated metastatic colorectal cancer: a 
double-blind, randomised, placebo-controlled phase 2 study. 
Lancet Oncol 13:993�1001

 28. Mayer RJ, Van Cutsem E, Falcone A et al (2015) Randomized 
trial of TAS-102 for refractory metastatic colorectal cancer. N 
Engl J Med 372:1909�1919





	Phase 1 study of�oral TAS-102 in�patients with�refractory metastatic colorectal cancer
	Abstract 
	Purpose 
	Methods 
	Results 
	Conclusions 

	Introduction
	Methods
	Patients
	Study design
	Assessments
	Statistical analysis
	Role of�the sponsor

	Results
	Patient disposition and�characteristics
	Dose-limiting toxicity
	Exposure
	Safety
	Efficacy

	Discussion
	Acknowledgments 
	References




