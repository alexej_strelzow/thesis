
International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 617

An Integratable Verifiable Secret Sharing
Mechanism

Yanjun Liu1,2 and Chin-Chen Chang2,3

(Corresponding author: Chin-Chen Chang)

Key Laboratory of Intelligent Computing and Signal Processing of Ministry of Education,

School of Computer Science and Technology, Anhui University1

No. 111 Jiulong Rd., Hefei 230601, China

Department of Computer Science and Information Engineering, Asia University2

No. 500, Lioufeng Rd., Wufeng, Taichung 413, Taiwan

Department of Information Engineering and Computer Science, Feng Chia University3

No. 100 Wenhwa Rd., Seatwen, Taichung 407, Taiwan

(Email: alan3c@gmail.com)

(Received Nov. 26, 2013; revised and accepted Mar. 5 & May 13, 2014)

Abstract

Threshold secret sharing (SS), also denoted as (t, n) SS,
has been used extensively in the area of information se-
curity, such as for group authentication, cloud storage
schemes, secure parallel communication and wireless mul-
tipath routing protocols. However, a (t, n) SS cannot de-
tect any deceptions among the dealer and shareholders.
Verifiable secret sharing (VSS) overcomes the weakness of
(t, n) SS in such a way that it is able to detect cheaters
by verifying the validity of shares or the correctness of
the recovered secret under the condition that both shares
and the secret are not compromised. Recently, two non-
interactive VSSs based on Asmuth-Bloom�s SS were pro-
posed by Harn et al. and Liu et al., respectively. Both
VSSs require shareholders to examine the range of values
of some integers related to the secret before recovering
the secret, which is a time-consuming operation. In this
paper, we propose a novel integratable VSS mechanism
that integrates the concepts of the generalized Chinese
remainder theorem (GCRT), Shamir�s SS and Asmuth-
Bloom�s SS. Our proposed VSS can verify that the secret
reconstructed by any t or more shareholders is the same
as the one that the dealer has generated. Analysis shows
that our proposed VSS can provide perfect secrecy and
better efficiency.

Keywords: Generalized Chinese remainder theorem
(GCRT), hash function, secret sharing (SS), verifiable se-
cret sharing (VSS)

1 Introduction

Threshold secret sharing (SS) [1, 3, 6, 9, 10, 11, 12, 20, 21,
22, 23, 25, 26] is a widely-used cryptographic mechanism

for managing a secret or a key among a set of partici-
pants. A threshold SS is also denoted as a (t, n) SS in
which a dealer does not release the secret itself, but di-
vides the secret into n shares that are distributed among
n shareholders. By using a specific algorithm, any sub-
set of t shares can recover the original secret. There are
two security goals that a (t, n) SS should achieve: 1) the
secret can be recovered by any t or more than t shares;
and 2) the secret cannot be determined by fewer than t
shares.

Shamir�s (t, n) SS [25] is the first (t, n) SS and was
proposed in 1979. It is based on the Lagrange interpolat-
ing polynomial and can ensure perfect secrecy. Perfect
secrecy means that even if no computational assump-
tion is made, both security goals can still be achieved.
Later in 1983, Mignotte [22] introduced another (t, n)
SS that is based on the Chinese remainder theorem
(CRT) [5, 9, 20]. Mignotte�s (t, n) SS generates a par-
ticular integer sequence and selects the secret in the t-
threshold range [11, 12, 21]. According to the sequence,
any t or more than t shares can recover the secret by
using the CRT. However, Mignotte�s (t, n) SS is not per-
fectly secure since it cannot accomplish the second secu-
rity goal. In the same year, Asmuth and Bloom [1] pro-
posed an enhanced version of Mignotte�s (t, n) SS which
can guarantee perfect secrecy. Nowadays, Shamir�s (t, n)
SS, Mignotte�s (t, n) SS and Asmuth-Bloom�s (t, n) SS
have become fundamental tools applied in many areas of
information security, such as for key distribution proto-
cols, group authentication, cloud storage schemes, secure
parallel communication and multipath routing protocols
in wireless networks [6, 9, 10, 20, 23, 26].

A (t, n) SS assumes that the dealer and shareholders
are all honest, but this is not always the case. There-



International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 618

fore, the weakness of a (t, n) SS is that it cannot discover
whether the dealer has transmitted inconsistent shares
to shareholders or whether shareholders have released in-
valid shares when recovering the secret. An incorrect se-
cret may be reconstructed without detection in these two
cases. In order to overcome this weakness, in 1985, Chor
et al. [7] introduced the concept of verifiable secret sharing
(VSS). Verifiability is the property of detecting cheaters
by verifying the validity of shares or the correctness of the
recovered secret under the condition that both the shares
and the secret are not compromised. Interactive and non-
interactive VSSs are two types of VSS. Interactive VSSs
require shareholders to interact with the dealer to exe-
cute the verification, which consumes a large amount of
communication time. To reduce the communication cost,
non-interactive VSSs have been proposed to replace inter-
active VSSs.

VSS expands the range of applications of SS and has
been researched deeply in a great number of recently pub-
lished literature [2, 8, 11, 12, 13, 14, 21, 24]. Benaloh [2]
defined the concept of t-consistency and proposed an
interactive VSS to verify that shares generated by the
dealer are t-consistent (i.e. any subset of t shares defines
the same secret). Feldman [8] proposed the first non-
interactive VSS using encrypted functions. The security
of Feldman�s VSS depends on the hardness of solving the
discrete logarithm. Qiong et al. [24] and Iftene [13] pre-
sented non-interactive VSSs based on Asmuth-Bloom�s SS
and Mignotte�s SS, respectively. Kaya et al. [14] pointed
out the security weaknesses in these two VSSs and de-
veloped a VSS based on Asmuth-Bloom�s SS. Harn and
Lin [11] extended a (t, n) VSS to a (n, t, n) VSS in
which each shareholder also acts as a dealer. Based on
Benaloh�s VSS [2], their VSS can verify that shares satisfy
the requirement of strong t-consistency. In 2013, Harn et
al. [12] proposed a non-interactive VSS based on Asmuth-
Bloom�s SS in which additional verification secrets are
used during the verification. Later, Liu et al. [21] pro-
posed a more efficient VSS by also using Asmuth-Bloom�s
SS as a building block. Both VSSs require shareholders
to examine the range of values of some integers related to
the secret before recovering the secret, which is a time-
consuming operation.

In this paper, we propose a novel integratable VSS
mechanism based on the generalized Chinese remainder
theorem (GCRT) [4, 15, 16, 17, 18, 19]. Our proposed
VSS can verify that the secret reconstructed by any t or
more shareholders is the same as the one that the dealer
has generated. The contributions of our proposed VSS
are listed below:

1) Our proposed VSS integrates the concepts of
Shamir�s SS, Asmuth-Bloom�s SS, and GCRT. To
the best of our knowledge, no research on VSS has
adopted this approach. Thus, we are the first to com-
bine these three fundamental elements in a VSS.

2) A one-way hash function is used to verify the correct-
ness of the secret, thereby removing the operation of

examining the range of values of additional integers.

3) Our proposed VSS can provide perfect secrecy.

4) Our proposed VSS simplifies two related works [12,
21] on VSS and achieves better efficiency.

The rest of this paper is organized as follows. Section 2
addresses some background knowledge related to VSS.
Our proposed VSS is described in Section 3. Section 4
gives security and performance analyses of our proposed
VSS. Finally, conclusions appear in Section 5.

2 Preliminaries

This section introduces some background knowledge
related to VSS. We first introduce two famous SSs:
Shamir�s [25] and Asmuth-Bloom�s (t, n) SS [1]. Then,
we address the principle and features of the GCRT [4, 15,
16, 17, 18, 19]. Finally, we review two recently developed
VSSs [12, 21].

2.1 Shamir�s (t, n) SS

Shamir�s (t, n) SS [25] is one of the most famous SSs,
which is based on the Lagrange interpolating polynomial.
Shamir�s (t, n) SS has been adopted widely in the design
of VSSs since it was proposed in 1979. Assume that there
is one dealer D and n users U = {u1, u2, ..., un}. Dealer D
first generates a secret s and divides it into n shares, and
then issues these shares to n users secretly, in such a way
that each user obtains one share. To achieve the objective
that any t users (also called shareholders) can collaborate
with each other by using their shares to recover the secret
s generated by dealer D, Shamir�s (t, n) SS executes the
following two phases as follows.
Share Generation:

Step 1. Dealer D randomly selects a polynomial g(x) of
degree t-1: g(x) = s+a1x+a2x

2+...+at?1xt?1modp,
where s = g(0) is the secret, and t coefficients
s, a1, a2, ..., at?1 are in the finite field GF (p).

Step 2. Dealer D generates n shares si = g(xi) for
i = 1, 2, ..., n, where xi can be considered as some
information of shareholder ui, such as ui�s ID num-
ber.

Step 3. Dealer D sends share si to shareholder ui in a
private channel.

Secret Reconstruction:
Any t shareholders can use their received shares to recon-
struct the secret s generated by dealer D. Supposing that
slj ? {s1, s2, ..., sn} for j = 1, 2, ..., t denote shares of t
shareholders, secret s can be reconstructed by computing
s = g(0) =

?t
j=1 g(xlj)

?t
m=1,m6=j

xlm
xlm?xlj modp.

According to these two phases, parameter t is usually
regarded as a threshold value that defines the fewest num-
ber of shares for recovering the secret. Shamir�s (t, n) SS



International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 619

is quite simple and can ensure perfect secrecy. Due to
this merit in security, Shamir�s (t, n) SS has become a
practical tool in realizing secret sharing and a common
building block in VSSs.

2.2 Asmuth-Bloom�s (t, n) SS

Different from Shamir�s (t, n) SS that is based on the La-
grange interpolating polynomial, Asmuth and Bloom [1]
proposed a novel SS based on the CRT. Asmuth-Bloom�s
(t, n) SS can also provide perfect secrecy, which has
gathered increasing attention in VSS research. If a dealer
D and n shareholders U = {u1, u2, ..., un} participate in
this SS, it can be described as follows.

Share Generation:

Step 1. Dealer D selects n+1 pairwise, co-prime inte-
gers, p0, p1, p2, ..., pn, that satisfy two requirements,
i.e., p1 < p2 < ... < pn and p0�pn?t+2�pn?t+3�...�pn <
p1 � p2 � ... � pt.

Step 2. Dealer D generates an integer s as the secret
such that 0 ? s < p0.

Step 3. Dealer D generates another integer A = s+ bp0,
where b is an arbitrary integer such that 0 ? A <?t
i=1 pi.

Step 4. Dealer D creates n shares si = A(modpi) for
i = 1, 2, ..., n, and then sends si to shareholder ui in
a private channel.

Secret Reconstruction:

Any t shareholders can use their received shares to recon-
struct secret s generated by dealer D. Supposing that
slj ? {s1, s2, ..., sn} for j = 1, 2, ..., t denote shares of t
shareholders, the secret s can be reconstructed according
to the following steps:

Step 1. Integer A is recovered by using the CRT. First,
the following system of equations is constructed:

sl1 = A(modpl1),

sl2 = A(modpl2),

...

slt = A(modplt).

Then, the unique integer A can be computed as A =?t
j=1Mj �M

?
j �slj(modP ), where P =

?t
j=1 plj , Mj =

P
plj

, and Mj �M ?j ? 1(modplj).

Step 2. Secret s is reconstructed by computing s =
A(modp0).

However, Harn et al. [12] pointed out that if integer
A selected by dealer D is in the range of [0, p1 � p2 � ... �
pt), Asmuth-Bloom�s (t, n) SS is actually not perfectly
secure. This is because in this case, secret s could be
recovered by fewer than t shares, which indicates that
both security goals cannot be fulfilled at one time. Harn
et al. modified Asmuth-Bloom�s (t, n) SS by confining A
in a smaller range, (pn?t+2 � pn?t+3 � ... � pn, p1 � p2 � ... � pt).
They called this range as the t-threshold range [12] and
denoted it as Zpn?t+2�pn?t+3�...�pn,p1�p2�...�pt . They proved
that only if A is selected in the t-threshold range, Asmuth-
Bloom�s (t, n) SS can ensure perfect secrecy.

2.3 Generalized Chinese Remainder The-
orem (GCRT)

The generalized Chinese remainder theorem (GCRT) is
an extension of CRT that adds a parameter k into the
theorem. GCRT uses the following basic elements:

1) n positive integers, x1, x2, ..., xn;

2) n positive, pairwise, co-prime integers, p1, p2, ..., pn;

3) an integer k satisfying Max{xi}1?i?n < k <
Min{pi}1?i?n.

To build the system of equations below:

x1 = bX/p1c(modk),

x2 = bX/p2c(modk),
...

xn = bX/pnc(modk).

From the GCRT, the unique integer X can be com-
puted as X =

?n
i=1Ni � N

?
i � Bi(modk � P ), where

P =
?n
i=1 pi, Ni = k � Ppi , Ni � N

?
i ? k(modk � pi), and

Bi = dxi�pik e.
The GCRT can accomplish the same functionality as

the CRT. However, the flexibility of the GCRT is better
than the CRT due to the use of integer k. In the GCRT,
only a change of k can generate a new integer X. On the
contrary, if integer X needs to be updated, all parameters,
such as x1, x2, ..., xn, and p1, p2, ..., pn, need to be mod-
ified. Therefore, the GCRT is regarded as an enhanced
version of the CRT and it has been extensively applied in
the fields of cryptography. As stated in Subsection 2.2,
the CRT is used to recover the secret in Asmuth-Bloom�s
(t, n) SS. In 2012, Guo and Chang [9] analyzed the cor-
rectness of Asmuth-Bloom�s (t, n) SS based on the GCRT.
Inspired by their approach, we will use the GCRT-based
Asmuth-Bloom�s (t, n) SS as one of the building blocks of
our novel VSS.



International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 620

2.4 Review of Related VSS Work

In this subsection, we review two VSSs, one by Harn et
al. [12] and the other by Liu et al. [21]. Both VSSs can ver-
ify whether the shares received by shareholders are con-
sistent under the condition that the secrecy of both shares
and the secret are not compromised. Their common char-
acteristics are listed below:

1) Both VSSs rely on the assumption that dealer D may
transmit a fake share to a shareholder; however, all
shareholders behave honestly;

2) They are based on Asmuth-Bloom�s (t, n) SS that
depends on the CRT;

3) In the verification, all shareholders work together to
verify that their shares are t-threshold consistent [12]
by examining the range of values of some integers
related to secret s;

4) They can verify all shareholders� shares simultane-
ously and conclude whether there exist any invalid
shares, but invalid shares cannot be identified.

Next, we investigate the detailed processes of these two
VSSs, respectively. In Harn et al.�s VSS, dealer D gener-
ates secret s and n shares according to Asmuth-Bloom�s
(t, n) SS. Moreover, the dealer selects additional r se-
crets (also called verification secrets) in the t-threshold
range and creates their corresponding shares. Afterwards,
the dealer distributes one share of the secret s along with
one share of each verification secret to each shareholder
ui secretly. In order to verify the validity of shares, share-
holders should first open (recover) r/2 verification secrets
and inspect whether they are in the t-threshold range.
If this holds, it indicates that the remaining, unopened
r/2 verification secrets are also in the t-threshold range.
Based on (r/2+1) shares owned by each shareholder, the
liner combinations of secret s and each unopened verifi-
cation secret can be recovered by using the CRT. If the
recovered values are in a certain range [12], the secret can
be proven as in the t-threshold range, thereby verifying
the t-threshold consistency of shares. If the verification
is passed, any t or more than t shareholders can recon-
struct secret s by using the CRT; otherwise, shareholders
require that the dealer redistribute shares.

In comparison, Liu et al.�s VSS is an improvement over
that proposed by Harn et al. It uses a similar, but simpler
method to accomplish share verification. In Liu et al.�s
VSS, each shareholder generates an adjustment value in-
stead of receiving r verification secrets as in Harn et al.�s
VSS. All shareholders combine their shares with adjust-
ment values to recover an integer that has a relationship
with secret s by using the CRT. Consequently, this strat-
egy saves considerable time by eliminating the recovery
of opened and unopened verification secrets. If the re-
covered integer is in the modified t-threshold range, the
secret is proven to be in the t-threshold range, thereby
verifying the t-threshold consistency of shares. In addi-
tion, the process of secret reconstruction in Liu et al.�s

VSS is the same as that in Harn et al.�s VSS. Accord-
ing to the performance analysis in [21], Liu et al.�s VSS
reduces both computational and communication costs.

3 Our Proposed VSS Mechanism

In this section, we first discuss the motivations for im-
proving the two previously described VSSs, and then pro-
pose an integratable VSS that is based on the concepts of
Shamir�s SS, Asmuth-Bloom�s SS, and the GCRT.

3.1 Motivations

In Subsection 2.4, we described the main properties of
Harn et al.�s VSS [12] and Liu et al.�s VSS [21]. In these
two VSSs, share verification and secret reconstruction are
two separate phases. In the share verification, all share-
holders must collaborate with each other to recover some
integers that have a relationship with the secret. Then the
range of value of the recovered integer is investigated to
ensure the validity of shares. If the verification is passed,
it implies that each shareholder received a correct share
from the dealer. Thus, t distinct shares can reconstruct
the real secret. However, if the verification is not passed,
there is no need to reconstruct the secret and the VSS
stops at this point.

From the process in these two VSSs, it can be in-
ferred that an integer related to the secret and the se-
cret itself must be reconstructed by the CRT in the share
verification and the secret reconstruction, respectively, if
the shareholder shares are valid. These are two time-
consuming operations where the efficiency can be im-
proved. In fact, the phases of share verification and secret
reconstruction can be integrated into a single phase that
verifies whether the secret reconstructed by any t or more
shareholders is the same as the one that the dealer has
generated. Therefore, the validity of shares can also be
verified without recovering another integer before secret
reconstruction. This strategy can increase the efficiency
to some extent. Moreover, the two previously proposed
VSSs can only detect the cheating behavior of the dealer
based on the assumption that all shareholders act hon-
estly. This can be improved to detect the honesty of either
the dealer or any shareholder.

3.2 Proposed VSS

Inspired by the VSSs presented by Harn et al. and Liu
et al., we propose a novel integratable VSS that improves
on their work. The word �integratable� means that the
proposed mechanism integrates three fundamental meth-
ods used in secret sharing: Shamir�s SS, Asmuth-Bloom�s
SS and the GCRT. In the following, we first address the
model of our design and then give the detailed VSS mech-
anism.

Like the two previously discussed VSSs, our proposed
VSS involves two parties: a dealer D and n sharehold-
ers U = {u1, u2, ..., un}. Dealer D generates a secret



International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 621

s and divides it into n shares that are shared among n
shareholders. t or more shareholders are responsible for
reconstructing secret s. However, dealer D may deceive
shareholders and deliver an invalid share to a shareholder.
On the other hand, shareholders may also act dishonestly
by releasing invalid shares when performing the recon-
struction of secret s. Consequently, our proposed VSS
must be able to verify the correctness of the reconstructed
secret to check whether there exists any deception among
either the dealer or shareholders.

In our proposed VSS, dealer D selects the secret s and
then computes a one-way hash function k = h(s). The
hash code k is used as a parameter in the GCRT and n
shares of the secret s are generated by the approach used
in the GCRT-based Asmuth-Bloom (t, n) SS. In addition,
dealer D constructs a Shamir (t, n) SS scheme in which
the dealer selects a polynomial g(x) of degree t-1 such
that g(0) = k. Then, dealer D distributes shares of s
and shares of k to shareholders. After receiving all the
messages sent by dealer D, t shareholders recover k and
then use k to recover the secret s via the GCRT. In the
end, we check whether h(s) is equal to the recovered k.
If it is true, shareholders can conclude that the recovered
secret s is identical to the real secret generated by the
dealer.

Our proposed VSS consists of two phases, a setup
phase and a verification phase. Figure 1 illustrates the
flowchart of the setup phase and the detailed steps are
presented as follows.

Setup Phase:

Step 1. Dealer D selects n+1 pairwise, co-prime inte-
gers, p0, p1, p2, ..., pn, that satisfy two require-
ments: (1) p0 < p1 < p2 < ... < pn, and (2)
p0 � pn?t+2 � pn?t+3 � ... � pn < p1 � p2 � ... � pt.

Step 2. Dealer D generates the secret s such that 0 ?
s < p0.

Step 3. Dealer D computes k = h(s), where h is a
collision-free, one-way hash function and 0 < k <
Min{pi}1?i?n.

Step 4. Dealer D generates an integer A = s+bp0, where
b is an arbitrary integer which should make sure that
A ? Zk�pn?t+2�pn?t+3�...�pn,k�p1�p2�...�pt .

Step 5. Dealer D creates n shares si = bA/pic(modk)
for i = 1, 2, ..., n.

Step 6. Dealer D selects a polynomial g(x) of degree t-1:
g(x) = k + a1x + a2x

2 + ... + at?1xt?1modq, where
k = g(0) and t coefficients k, a1, a2, ..., at?1 are in the
finite field GF (q).

Step 7. Dealer D generates n shares of k as s
?
i = g(xi)

for i = 1, 2, ..., n.

Step 8. Dealer D sends si and s
?
i to shareholder ui in a

private channel.

Verification Phase:

Assume that ul1, ul2, ..., ult ? U are t shareholders and
shareholder ulj (j = 1, 2, ..., t) received slj and s

?
lj in the

setup phase.

Step 1. ul1, ul2, ..., ult use s
?
l1, s

?
l2, ..., s

?
lt to recover k fol-

lowing Shamir�s (t, n) SS.

Step 2. ulj uses slj , plj , and the recovered k to release

Cj = Nlj �N ?lj �Blj(modk �P
?
), where P

?
=
?t
j=1 plj ,

Nlj = k � P
?

plj
, Nlj � N ?lj ? k(modk � plj), and Blj =

d slj �pljk e.

Step 3. ul1, ul2, ..., ult work together to compute A =?t
j=1 Cj(modk � P

?
) following the GCRT. Then,

the secret s is reconstructed by computing s =
A(modp0).

Step 4. Check whether h(s) is equal to k. If these two
values are identical, we can conclude that the re-
constructed secret s is correct; otherwise, the recon-
structed s is not a valid value.

Remark 1. In the setup phase, dealer D needs to gen-
erate two secret messages: s and k. s is the real secret
needed to recover and k is used to verify the correctness
of s. More specifically, k has multiple functionalities that
can be described as follows: (1) Dealer D makes k as the
hash code of the one-way hash function h(s) in the setup
phase. (2) k is an important parameter in the GCRT to
generate shares of secret s as si = bA/pic(modk). Later,
shareholders will use their shares and k to recover s ac-
cording to the GCRT-based Asmuth-Bloom (t, n) SS. (3)
To increase the degree of security, dealer D does not trans-
mit k directly to shareholders, but establishes Shamir�s
(t, n) SS scheme in which k is considered as the secret.
Then, in the verification phase, shareholders can recover
k easily according to the method of Shamir�s (t, n) SS. (4)
We can check whether h(s) is equal to k to verify the cor-
rectness of the recovered secret s. In summary, the use of
k provides an additional level of security for our proposed
VSS.

Remark 2. The verification phase of our proposed VSS
can verify that the recovered secret s is identical to the real
secret generated by the dealer. This verification process is
completed by the one-way hash function h(s). Thus, it is
unnecessary to recover some integers related to the secret
and to examine the range of this integer like in the two
VSSs mentioned before. This can simplify the verification
process. Furthermore, if the verification fails, we can con-
clude that either the dealer or the shareholder is dishonest.
However, it is impossible to identify two situations: (1)
the dealer sends invalid shares to shareholders; and (2)
shareholders release invalid shares to recover the secret.
Lastly, similar to Harn et al. and Liu et al.�s VSSs, our
proposed VSS can verify all shares at one time.



International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 622

Figure 1: Flowchart of the setup phase

4 Security and Performance Anal-
yses

In this section, we first give security analysis of our pro-
posed VSS, and then compare the performance among our
proposed VSS and two other VSSs.

4.1 Security Analysis

Now we analyze that our proposed VSS can provide per-
fect secrecy. Our proposed VSS uses Shamir�s (t, n) SS to
share k among n shareholders, and later, t shareholders
recover k and use it in the GCRT to recover secret s. Since
Shamir�s (t, n) SS is perfectly secure, fewer than t shares
cannot recover k. Therefore, secret s cannot be recovered
from fewer than t shares. Moreover, each shareholder ulj
releases a value Cj = Nlj � N ?lj � Blj(modk � P

?
), which

combines slj , plj , and k. Thus, hidden in the value of Cj ,
share slj cannot be compromised during the verification
phase.

In the following, we will analyze a situation where even
if k is compromised, our proposed VSS is still perfectly
secure, since fewer than t shareholders cannot obtain any
useful information about secret s. Firstly, if fewer than
t shareholders know the value of k, they cannot obtain
secret s from h(s) = k due to the intrinsic characteristic
of the hash function. Next, we will prove that fewer than
t shareholders cannot recover secret s from the GCRT.

Assume that ul1, ul2, ..., ul(t?1) are t-1 shareholders
and each shareholder ulj receives share slj . According
to the GCRT, these t-1 shareholders cooperate to com-
pute an integer A

?
=
?t?1

j=1Nlj � N
?
lj � Blj(modk � P

??
),

where P
??

=
?t?1
j=1 plj , Nlj = k � P

??

plj
, Nlj � N ?lj ?

k(modk � plj), and Blj = d slj �pljk e. However, A
?

is

not equal to the real secret A since the range of A
?

is
Zk�p1�p2�...�pt?1 , which is quite different from that of A
as Zk�pn?t+2�pn?t+3�...�pn,k�p1�p2�...�pt . Consequently, fewer
than t shareholders cannot reconstruct the secret directly
by using the GCRT. However, this does not mean fewer
than t shareholders cannot obtain the real secret A from
the recovered A

?
. The use of GCRT implies that A and

A
?

have a relation as A = A
?

+ ? � p1 � p2 � ... � pt?1.
Thus, A can be computed from A

?
if ? can be deter-

mined by fewer than t shareholders. Unfortunately, it
is very hard to determine the correct ?. This is because
(p1 �p2 � ... �pt?pn?t+2 �pn?t+3 � ... �pn)/p1 �p2 � ... �pt?1 > p0
values of ? can make A

?
+? � p1 � p2 � ... � pt?1 in the range

of Zk�pn?t+2�pn?t+3�...�pn,k�p1�p2�...�pt , but only one value of

A
?

+ ? � p1 � p2 � ... � pt?1 is equal to A. Therefore, the
probability of finding the exact value of ? is not greater
than the probability of guessing the secret A. Based on
this security analysis, our proposed VSS ensures perfect
security based on the fact that fewer than t shareholders
cannot obtain the real secret A from the recovered A

?
.

4.2 Performance Analysis

In this section, we provide performance analysis of our
proposed VSS and compare it with the other two related
VSSs [12, 21] in terms of computational and communica-
tion costs.

Now we analyze the computational cost. In the setup
phase, Harn et al.�s VSS creates and distributes shares
of the real secret and r additional verification secrets to
n shareholders. In contrast, the VSS by Liu et al. and
our proposed VSS do not need to generate verification
secrets. The difference between our VSS and Liu et al.�s
VSS is that our VSS generates an additional polynomial
of degree t-1 according to Shamir�s SS. In the verification
phase of the other two VSSs, n shareholders need to first
cooperate to recover one or several integers related to the
secret, and then t out of n shareholders recover the real
secret, both by using the CRT. [21] analyzes that the time
complexity of the VSSs proposed by Harn et al. and Liu
et al. are O(rn2d2) and O(n2d2), respectively, where d is
the number of bits of pi and r is the number of verification
secrets.

In comparison, in the verification phase of our proposed
VSS, t out of n shareholders first recover the secret k in
Shamir�s (t, n) SS and then reconstruct the real secret
with the recovered k by the GCRT. The time complexity
of recovering k is O(tlog2t) [25] and the time complexity
of recovering the real secret is analyzed as follows. Secret
A is computed as A =

?t
j=1Nlj � N

?
lj � Blj(modk � P

?
)

by the GCRT, where P
?

=
?t
j=1 plj , Nlj = k � P

?

plj
,

Nlj � N ?lj ? k(modk � plj), and Blj = d slj �pljk e. Assume
that d is the number of bits of both the operands, plj and

k. Considering that k � P ? and Nlj �N ?lj can be computed
o?ine, this computational process totally contains (t-1)
additions, t divisions, 2t multiplications, and one mod-
ular operation. Therefore, the number of bit operations
required is (t? 1)� d + t� d2 + 2t� d2 + ((t + 1)� d)2,
and the time complexity is O(t2d2).

Tables 1 and 2 summarize the communication and com-
putational costs of our proposed VSS and the other two
VSSs [12, 21]. From the comparisons among these VSSs,
we can imply that our proposed VSS has better compu-



International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 623

Table 1: Comparison of communication cost in setup phase

Scheme Dealer sends messages Each ui sends messages Each ui receives messages
VSS in [12] n(r + 1) - r + 1
VSS in [21] n - 1

Our VSS 2n - 2

tational efficiency than the other two VSSs and our com-
munication efficiency is also satisfactory.

Table 2: Comparison of computational cost

Scheme Setup phase Verification phase
VSS in [12] O(rn) O(rn2d2)
VSS in [21] O(n) O(n2d2)

Our VSS O(n) O(t2d2)

Note: n is the number of shares; d is the number of bits
of operands; and r is the number of verification secrets.

5 Conclusions

In the paper, we propose a novel integratable VSS mech-
anism that integrates the concepts of the generalized
Chinese remainder theorem (GCRT), Shamir�s SS and
Asmuth-Bloom�s SS. Our proposed VSS improves Harn
et al.�s VSS and Liu et al.�s VSS by using a one-way hash
function to verify the correctness of the secret. While
maintaining the advantages of the other two related VSSs,
our proposed VSS is more efficient. In addition, we proved
that our proposed VSS is perfectly secure.

Acknowledgments

This research was supported in part by the National
Nature Science Foundation of China (grant number:
61202228) and the College Natural Science Key Project of
Anhui Province of China (grant number: KJ2012A008).
The authors gratefully acknowledge the anonymous re-
viewers for their valuable comments.

References

[1] C. Asmuth and J. Bloom, �A modular approach to
key safeguarding,� IEEE Transactions on Informa-
tion Theory, vol. IT-29, no. 2, pp. 208�210, 1983.

[2] J. C. Benaloh, �Secret sharing homomorphisms:
Keeping shares of a secret,� in Advances in Cryptol-
ogy (Crypto�86), pp. 251�260, Santa Barbara, USA,
August 1986.

[3] G. R. Blakley, �Safeguarding cryptographic keys,� in
Proceedings of American Federation of Information

Processing Societies National Computer Conference,
pp. 313�317, New York, USA, June 1976.

[4] C. C. Chang and Y. P. Lai, �A fast modular
square computing method based on the generalized
Chinese remainder theorem for prime module,� Ap-
plied Mathematics and Computation, vol. 161, no. 1,
pp. 181�194, 2005.

[5] C. C. Chang, J. S. Yeh, and J. H. Yang, �Gener-
alized Aryabhata remainder theorem,� International
Journal of Innovative Computing, Information and
Control, vol. 6, no. 4, pp. 1865�1871, 2010.

[6] S. Chen and M. Wu, �Anonymous multipath rout-
ing protocol based on secret sharing in mobile ad
hoc networks,� Journal of Systems Engineering and
Electronics, vol. 22, no. 3, pp. 519�527, 2011.

[7] B. Chor, S. Goldwasser, S. Micali, and B. Awerbuch,
�Verifiable secret sharing and achieving simultaneity
in the presence of faults,� in Proceedings of the 26th
IEEE Symposium on the Foundations of Computer
Science, pp. 383�395, Portland, USA, Oct. 1985.

[8] P. Feldman, �A practical scheme for non-interactive
verifiable secret sharing,� in Proceedings of 28th
IEEE Symposium on Foundations of Computer Sci-
ence, pp. 427�437, Los Angeles, USA, Oct. 1987.

[9] C. Guo and C. C. Chang, �An authenticated group
key distribution protocol based on the generalized
Chinese remainder theorem,� International Journal
of Communication Systems, vol. 27, no. 1, pp. 126�
134, 2014.

[10] L. Harn, �Group authentication,� IEEE Transac-
tions on Computers, vol. 62, no. 9, pp. 1893�1898,
2013.

[11] L. Harn and C. Lin, �Strong (n, t, n) verifiable se-
cret sharing scheme,� Information Sciences, vol. 180,
no. 16, pp. 3059�3064, 2010.

[12] L. Harn, F. Miao, and C. C. Chang, �Verifiable secret
sharing based on the Chinese remainder theorem,�
Security and Communication Networks, vol. 7, no. 6,
pp. 950�957, 2014.

[13] S. Iftene, Secret Sharing Schemes with Applications
in Security Protocols, Technical Report TR 07-01,
Oct. 2006.

[14] K. Kaya and A. A. Selcuk, �A verifiable secret shar-
ing scheme based on the Chinese remainder theo-
rem,� in Advances in Cryptology (INDOCRYPT�08),
pp. 414�425, Kharagpur, India, Dec. 2008.

[15] Y. P. Lai and C. C. Chang, �Parallel computational
algorithms for generalized Chinese remainder theo-
rem,� Computers and Electrical Engineering, vol. 29,
no. 8, pp. 801�811, 2003.



International Journal of Network Security, Vol.18, No.4, PP.617-624, July 2016 624

[16] C. H. Lin, C. C. Chang, and R. C. T. Lee, �A record-
oriented cryptosystem for database sharing,� The
Computer Journal, vol. 35, no. 6, pp. 658�660, 1992.

[17] Y. Liu, C. C. Chang, and S. C. Chang, �An effi-
cient oblivious transfer protocol using residue num-
ber system,� International Journal of Network Secu-
rity, vol. 15, no. 3, pp. 212�218, 2013.

[18] Y. Liu, C. C. Chang, and S. C. Chang, �A resid-
ual number system oriented group key distribution
mechanism,� International Journal of Information
Processing and Management, vol. 4, no. 3, pp. 146�
155, 2013.

[19] Y. Liu, C. C. Chang, and S. C. Chang, �An ac-
cess control mechanism based on the generalized
Aryabhata remainder theorem,� International Jour-
nal of Network Security, vol. 16, no. 1, pp. 58�64,
2014.

[20] Y. Liu, L. Harn, and C. C. Chang, �An authenti-
cated group key distribution mechanism using theory
of numbers,� International Journal of Communica-
tion Systems, vol. 27, no. 11, pp. 3502�3512, Nov.
2014.

[21] Y. Liu, L. Harn, and C. C. Chang, �A novel verifi-
able secret sharing mechanism using theory of num-
bers and a method for sharing secrets,� International
Journal of Communication Systems, vol. 28, no. 7,
pp. 1282�1292, May 2015.

[22] M. Mignotte, �How to share a secret,� in Proceedings
of the Workshop on Cryptography, pp. 371�375, 1983.

[23] A. Parakh and S. Kak, �Space efficient secret shar-
ing for implicit data security,� Information Sciences,
vol. 181, no. 2, pp. 335�341, 2011.

[24] L. Qiong, W. Zhifang, N. Xiamu, and S. Shenghe,
�A non-interactive modular verifiable secret sharing
scheme,� in Proceedings of International Conference
on Communications, Circuits and Systems (ICCCAS
2005), pp. 84�87, Hong Kong, May 2005.

[25] A. Shamir, �How to share a secret,� Communications
of the ACM, vol. 22, no. 11, pp. 612�613, 1979.

[26] H. Zhu, T. Liu, D. Zhu, and H. Li, �Robust and sim-
ple n-party entangled authentication cloud storage
protocol based on secret sharing scheme,� Journal of
Information Hiding and Multimedia Signal Process-
ing, vol. 4, no. 2, pp. 110�117, 2013.

Yanjun Liu received her B.S. degree in 2005, in School
of Computer Science and Technology from Anhui Univer-
sity, Hefei, China. She received her Ph.D. degree in 2010,
in School of Computer Science and Technology from
University of Science and Technology of China, Hefei,
China. She is currently serving in Anhui University.
Meanwhile, she is a postdoctor at Asia University,
Taichung, Taiwan. Her current research interests include
information security and computer cryptography.

Chin-Chen Chang received his Ph.D. degree in com-
puter engineering from National Chiao Tung University.
His first degree is Bachelor of Science in Applied Mathe-
matics and master degree is Master of Science in computer
and decision sciences. Both were awarded in National Ts-
ing Hua University. Dr. Chang served in National Chung
Cheng University from 1989 to 2005. His current title is
Chair Professor in Department of Information Engineer-
ing and Computer Science, Feng Chia University, from
Feb. 2005. Prior to joining Feng Chia University, Pro-
fessor Chang was an associate professor in Chiao Tung
University, professor in National Chung Hsing University,
chair professor in National Chung Cheng University. He
had also been Visiting Researcher and Visiting Scientist
to Tokyo University and Kyoto University, Japan. Dur-
ing his service in Chung Cheng, Professor Chang served
as Chairman of the Institute of Computer Science and
Information Engineering, Dean of College of Engineering,
Provost and then Acting President of Chung Cheng Uni-
versity and Director of Advisory Office in Ministry of Ed-
ucation, Taiwan. Professor Chang has won many research
awards and honorary positions by and in prestigious orga-
nizations both nationally and internationally. He is cur-
rently a Fellow of IEEE and a Fellow of IEE, UK. And
since his early years of career development, he consec-
utively won Outstanding Talent in Information Sciences
of the R. O. C., AceR Dragon Award of the Ten Most
Outstanding Talents, Outstanding Scholar Award of the
R. O. C., Outstanding Engineering Professor Award of
the R. O. C., Distinguished Research Awards of National
Science Council of the R. O. C., Top Fifteen Scholars
in Systems and Software Engineering of the Journal of
Systems and Software, and so on. On numerous occa-
sions, he was invited to serve as Visiting Professor, Chair
Professor, Honorary Professor, Honorary Director, Hon-
orary Chairman, Distinguished Alumnus, Distinguished
Researcher, Research Fellow by universities and research
institutes. His current research interests include database
design, computer cryptography, image compression and
data structures.


