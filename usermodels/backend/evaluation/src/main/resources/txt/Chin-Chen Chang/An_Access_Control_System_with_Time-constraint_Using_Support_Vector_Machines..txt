
International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 150

An Access Control System with Time-constraint

Using Support Vector Machines

Chin-Chen Chang1, Iuon-Chang Lin2, and Chia-Te Liao3

(Corresponding author: Iuon-Chang Lin)

Department of Information Engineering and Computer Science Feng Chia University1,

Taichung, Taiwan, R.O.C. (Email: ccc@cs.ccu.edu.tw)

Department of Management Information Systems, National Chung Hsing University2,

250 Kuo Kuang Rd., Taichung, Taiwan 402, R.O.C. (Email: iclin@nchu.edu.tw)

Department of Computer Science and Information Engineering, National Chung Cheng University3,

160 San-Hsing, Min-Hsiung, Chiayi 621 Taiwan, R.O.C.

(Received Sep. 21, 2005; revised and accepted Oct. 4, 2005)

Abstract

Access control is an important issue in information secu-
rity. It is a necessary mechanism for protecting data in
a computer system. In this paper, we apply support vec-
tor machines to construct an access control system. The
security management framework is a pattern classifica-
tion system using well trained support vector machines.
The proposed system can not only be applied to broad
access policies, but the access rights of users can also be
associated with a time period. The proposed scheme is
useful to make predictions on real-time data and it is bet-
ter adapted to complex computer systems.

Keywords: Access control, information protection, neural
network, security, support vector machines

1 Introduction

Resource sharing is one of the most important benefits in
the client/server architecture or distributed environment.
For example, a file server provides a centralized man-
agement and shares the storage of remote data among a
number of workstations. If distributed workstations want
to access some files, they send their requests to the file
server via a communication network. The file server facil-
itates resource sharing among the autonomous worksta-
tions and has some obvious benefits, such as easy manage-
ment, economy, and so on. On the other hand, a database
is also a very important element in resource sharing in
modern computer usage. It allows sharing of data among
many users or applications. Now, almost every business
application is supported by some sort of database in order
to work, and thus more and more secret data are being
stored in databases for easy access.

However, the resource sharing systems are dangerous if
they are without information protection mechanisms. Be-
cause the amount of secret information, such as business
plans, military maps, and so on, is available to illegitimate
or unauthorized users, more and more threats to secret
data stored in computer systems are increasing exponen-
tially with the growth of network users and technological
developments. Therefore, how to achieve privacy and in-
tegrity of information is becoming an important issue in
modern computer security.

Access control is one of the efficient ways to prevent
information from being destroyed, altered, disclosed, or
copied by unauthorized users. Access control makes de-
cisions on all users accessing a system. The policies of
access control define the rules which permit certain sub-
jects (such as users) to invoke objects (such as programs
or files) in a computer system and which give them access
right to operations (such as to executions, own, reading,
or writing). Typically, the policies of access control can
be defined according to five components [12].

1) Users: The policy can identify what a user is allowed
to do. The term �users� could refer to the people
sitting at a terminal or workstation or the processes
which run the computer system.

2) Resources: The policy can specify what resources a
user can access. The term �resources� could refer to
the programs, services, and data accessed by users.

3) Operations: The policy can specify what operations
the user is permitted to invoke from the resource.
The term �operations� refer to the actions that can
be performed on a resource. For instance, one user
may be permitted to write a file, whereas another
user may only read the file.



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 151

4) Authority: The authority policy is to ensure that
the access control is implemented according to the
policies of the management of an organization. The
term �authority� refers to the legitimate power to
make policy decisions.

5) Domain: The policy can specify the boundary of the
resources or users. For instance, a manager usually
has higher authority than the resources and people
in a department.

On the other hand, authorization control also can be
divided into three categories [9]. The first category is
called discretionary access control (DAC). In this cate-
gory, the system leaves the specification of access control
policies to individual users and controls the access of users
by authenticating the identity after proper verification.

The second category is called mandatory access control
(MAC). The subjects and objects in MAC are classified
into many clearances and classifications. Furthermore,
the access right for each subject is defined by a system
administrator. Typically, the policies of MAC must sat-
isfy two restrictions, namely, (1) no read up and (2) no
write down.

However, the DAC and MAC models are not flexi-
ble enough to provide a variety of access control policies.
Since the access policies of these models are pre-defined
and have been built into the access control mechanisms,
they cannot support the dynamic requirements needed in
modern application environments.

In order to deal with this problem, the third category
of an access control model, called role-based access con-
trol (RBAC), is presented. Currently, RBAC is the most
popular access control model. RBAC can not only sup-
port the policies of DAC and MAC but can also support
more complex policies. The roles of RBAC are created
for various job functions in an organization, and users are
assigned roles based on their responsibilities and qualifi-
cations. In this model, users can easily be reassigned from
one role to another. This greatly simplifies the manage-
ment of access policies [14].

So far, many schemes in the three categories have
been proposed for controlling access to computer systems
[7, 9, 14, 15]. In these schemes, once a user tries to access
a protected file, the user needs to provide proof to the sys-
tem that he/she is the authorized user. Password or key
verification is the most popular technique for identifying
an authorized user. Once an outside user wants to access
certain resources in a system, the user has to submit a
secret password or key for verifying the access privilege
to the resource.

However, some complex computer systems may need
to change the access right with time. Thus, a file can
sometimes be accessed and sometimes not. For example,
a file may be updated every morning by the system ad-
ministrator, and we wish that no one can access this file
during this time period to avoid any data inconsistency.
The property is very flexible and useful in many modern

applications. In this paper, we shall propose a novel ac-
cess control scheme, called time-constraint access control,
which can satisfy such application environments. In the
time-constraint access control model, the access policies
are associated with the time period. In different time
periods, a user has different access rights.

Our time-constraint access control scheme is con-
structed from support vector machines. Support Vector
Machines (SVMs) is a useful classification tool based on
statistical learning theory [17]. Because of the good gen-
eralization ability of SVMs, SVMs have been widely and
successfully applied in a number of fields, such as hand-
written digital recognition, face detection, particle iden-
tification, and text categorization [13]. In this work, we
shall construct an access control verification system by
using the technology of SVMs and make it applicable to
a time constraint access control model. In this system, a
user only needs to remember one password and present
it to the verification system to prove that he/she has the
access right during the defined time period. The input
pattern of the verification system is the user�s password
and the system-assigned time period, and the output is
the object domain and the access right during the time
period that can be acquired by the user.

Before describing the proposed scheme, we will briefly
introduce the related works on access control in Section 2.
It is worthwhile to note that the reviewed schemes have
already solved the problems of access control in computer
systems. However, these schemes are not applicable to
time-constraint access control policies. Following this re-
view, an overview of support vector machines with differ-
ent goals is described, as well as the technologies that will
be used in our scheme. Section 4 provides a detailed de-
scription of our new time-constraint access control scheme
using SVMs. Then, our experimental results and exten-
sive discussions are given in Section 5. Finally, we shall
summarize the benefits that our scheme provides in Sec-
tion 6.

2 Previous Works

In this section, we shall briefly review some related
schemes in the area of access control in computer systems.
Typically, an access control system involves two entities,
namely, (1) identity and (2) identifier. Identity is the user
who approaches the access control system and claims to
obtain access privilege to the system. Identifier is the au-
thentication mechanism that authenticates whether the
identity is a legitimate user. So far, many authentication
mechanisms have been proposed to authenticate a user�s
access privileges. In 1972, Graham and Denning [8] first
introduced an abstract protection model. In their model,
the security state of an access control system is defined by
an access control matrix, where rows correspond to users,
columns correspond to the protected files, and the entries
correspond to the lists of users with access rights to the
protected files. A simple access control matrix for a file



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 152

system is shown in Figure 1. The entry aij stands for the
access right of the user Ui with respect to the file Fj . If
one user has no access right to a file, the corresponding
entity aij in the matrix is assigned a �null� value. For
example, the user U1 has access rights to execute the files
F1 and F3 and to write the file F4. When a user tries to
access a file, the system will check the matrix to verify the
user�s access right. The access control model is simple and
easy to implement. In general, however, there is a huge
amount of data stored in a file system. Therefore, the
size of the access control matrix would be very large, and
most of the entities in the matrix are nulls because each
user is usually only allowed access to a subject of the files.
If we store the whole access control matrix, the memory
needed for the whole matrix becomes impractically large,
and the utilization of storage is very low. On the other
hand, the system must maintain and protect the matrix
from being modified by an intruder.

Figure 1: Access control matrix

To overcome the problems faced by the above scheme,
Wu and Hwang [18] proposed a single-key-lock (SKL)
model for implementing the access control system. In
their model, each user Ui is associated with a key vector
Ki, and each file Fj is associated with a lock vector Lj.
Therefore, the access right aij for Ui to Fj can be formu-
lated as aij = f(Ki, Lj), where f() is a predefined func-
tion. Since then, several methods have been developed
for a single-key-lock access control model. Chang [1, 2, 3]
proposed three of them based on the Chinese remainder
theorem, Euler�s theorem, and prime factorization. Laih
et al. [11] proposed a single-key-lockmodel based on New-
ton�s interpolating polynomials. The previous schemes
work well, but they are impractical for complex computer
systems. A drawback to this kind of model is that it is
not flexible for dynamically controlling the access rights
of users to protected files.

Role-base access control provides a way to efficiently
protect information in complex systems with many users
and resources. In the RBAC model, roles are created
for various job functions in an organization and users are
assigned roles based on their responsibilities and qualifi-
cations [7, 9, 14, 15]. Hence, RBAC allows a wide range
of policies to be implemented. Furthermore, in some com-

plex application environments, the access rights of users
to the protected files should be changed according to a
defined time period. Therefore, the access policies for
all users to the resources should be associated with time.
For this reason, time-constraint role-based access control
is more useful and practical than traditional access con-
trol models in complex computer systems. As far as us
aware, no one has ever proposed a time-constraint access
control model. However, in this paper, we apply support
vector machines to construct a practical time-constraint
access control scheme.

3 The Review of Support Vector

Machines

We have seen that more and more researchers are cur-
rently engaging in studies of SVMs. In addition, in re-
cent years, there have been many interesting applications
with the good abilities of SVMs to meet some desired
requirements. It is also that most of them have attained
very successful results in fields such as text categorization,
hand-written pattern recognition, bioinformatics, as well
as others. Basically, SVMs are systems of linear learning
machines with good generalization ability that makes use
of the optimization theory for efficient training in kernel-
induced space. At the same time, the generalization the-
ory provides insights to control the complexity and keep
the consistency of the hypotheses [10, 16].

To make it easy to understand, we can simply de-
scribe an SVM problem as follows: Suppose that we
have a given set of instances that satisfy the require-
ment (xi, yi) ? S, where (xi, yi) denotes an instance in
S. Here, S = {(x1, y1), (x2, y2), � � � (xi, yi)} ? (X � Y )

l

is the training set of examples for i = 1, 2, � � � , l. The
notation l is the number of training instances in S. For
each xi ? X and yi ? Y , X ? R

d is the input space
while Y is the output domain. Note that we will use d
to refer to the dimension of the input space X . There-
fore, for cases of binary classification, the output domain
we have is Y = {1,?1}; for cases of m-class classifica-
tion, the output domain we have is Y = {1, 2, � � � ,m};
and for cases of regression, the output domain we have is
Y ? R. SVMs act as the learning methodology used to
find the hyperplanes which divide all the examples so that
all the instances with the same labels stand on the same
side when it comes to classification, or it is used to get a
linear function that best interpolates the set of examples
in regression cases. The basic concept of SVMs with the
simplest binary classification is shown in Figure 2 [6]. In
this section, we will briefly review the various conditions
of SVMs in the following subsections.

3.1 Support Vector Classification Cases

Let us start with the simplest conditions for classifica-
tion. Assume that we want to separate the set with
Y = {1,?1} for the binary classification cases. In other



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 153

Figure 2: The optimal separating hyperplan (OSH) and
its margin

words, our job is to find the hyperplane f of w and b,
subject to

yi(w � xi + b) > 0, (1)

for i = 1, 2, � � � , l, where the notations w and b are the
weight and bias, respectively. The notation l denotes the
number of training instances. If there is a hyperplane f
which satisfies Equation (1), then the set of examples S
is said to be separable. Besides, the hyperplane f of w
and b is always able to be scaled such that

yi(w � xi + b) ? 1,

for i = 1, 2, � � � , l. In Figure 2, each point is an exam-
ple instance in S, and the notation ? = 1?w? denotes the

distance from an arbitrary point closest to f . The quan-
tity ? = 2?w? in Figure 2 is called the margin, and it can

be treated as a benchmark of the generalization ability
of hyperplanes [17]. By minimizing the quantity of ?w?2,
we can obtain the Optimal Separating Hyperplane (OSH)
with the largest margin, i.e. the best generalization abil-
ity. It is an optimization problem. Assume that for the
training set S, a solution of OSH f? in the form (w?, b?)
has been derived. Then the decision function h(x) will
be:

h(x) = sign(w? � x+ b?).

With this decision function, therefore, we are able to
classify an unclassified data instance z ? X by employ-
ing h(z) to determine which class it belongs to. However,
sometimes in real-life situations, the instances are not so
easily classified. To handle that, just as we select the ar-
chitecture for a neural network, we often adopt kernel rep-
resentations in order to increase the computational power
of linear SVMs when we design our scheme [5]. Kernel
representations will project data onto a more meaningful
and higher dimensional space to provide non-linear ap-
proximation abilities. This is extremely useful when we
handle problems in real life that are not so easy to solve.
The sketch map of kernel representations is shown in Fig-
ure 3.

Figure 3: Simplifying the classification task by feature
mapping

3.2 Support Vector Regression Cases

The goal of regression problems in SVMs is to find a
function f that best interpolates a given set of examples.
That is, we can apply support vector methods such that it
learns a non-linear function in the kernel-induced feature
space. To get better generalization abilities, we will ig-
nore the errors within a certain distance ? of a true value.
The ?-insensitive loss function L?(x, y, f)is thus defined
as:

L?(x, y, f) = max(0, |y ? f(x)| ? ?).

The notation f expresses a real number function in
the input space X . Note that x ? X and y ? R, which
is the same as we defined previously. In brief, a simple
regression problem now can be formulated as:

minimize ? w ?2 +C

l?

i=1

(?2i + ?�
2

i ),

subject to ((w � xi) + b? yi ? ?+ ?i, and

yi ? ((w � xi) + b) ? ?+ ?�i,

where ?i, ?�i ? 0 for i = 1, 2, � � � , l. The parameter C used
here is to exhibit the penalty, and it will perform the
function when f makes errors. Just as Figure 4 depicts,
corresponding to each instance xi the slack variable ?�i is
for that which exceeds the target value by more than ?,
and the other slack variable ?i is for that which is below
the target with a distance more than ?. We can convert
the optimization problem into another form with the La-
grange multipliers induced: maximize

W (?) =
l?

i=1

yi?i ? ?
l?

i=1

|?i| ?
1

2

l?

i,j=1

?i?jK(xi, xj) (2)

subject to
?l

i=1 ?i = 0, ?C ? ?i ? C, for i = 1, 2, � � � , l.
Note that the notation K(a, b) in Equation (2) is to rep-
resent a projection of two vectors a and b into the desired
meaningful space, that is, the kernel representation was
induced here to make the prediction task more easily.

Assume now that we have found a solution ?? satis-
fying Equation (2). Then the regression function best
interpolating S will be:

f(x) =

l?

i=1

??iK(xi, x) + b
?.



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 154

Figure 4: The insensitive band of a non-linear regression
function

And, finally, we are able to apply the found regression
function f to further predict an unknown instance which
has the same similarities as the training examples.

4 The Proposed Scheme

Let us start to describe the proposed scheme with a sim-
ple example. Assume that we have an enterprise orga-
nized into several different departments. All this com-
pany�s secret business information is stored in a server
or a database, and employees can directly access these
secret files according to their positions in the company.
However, due to security reasons, the privileges of sys-
tem users must be restricted according to their identities;
for example, it is only natural that a sales manager can
not access technical reports which belong to the depart-
ment of engineering. Moreover, if it is not the time that
an employee is supposed to work, he/she, of course, is
not allowed to open these secret files. So, here, we give
the proposed scheme for access control that satisfies all
the security requirements mentioned above. The excellent
prediction abilities of SVMs will be exploited to classify
system users into different levels of security according to
the input passwords. And we also include the time fac-
tors to guarantee that users can not overdo except during
the office hours. This means that the users� security lev-
els should be changed according to the users� login times.
Briefly speaking, first, all system users will be separated
into different groups (i.e. departments). Then, to vary
the access rights within a group of people in different job
positions, various passwords will be given corresponding
to the groups and their different security levels. For in-
stance, a manager should have a higher security level than
his/her assistant, but he/she still can not access restricted
files from other departments. Finally, after the training
procedure is used to feed the expected output values into
SVMs, we are able to apply SVMs to classify users into
their appropriate groups and security levels.

As Figure 5 depicts, the proposed access control sys-
tem can be roughly divided into three portions: (1) the
input pattern transforming phase, (2) the training phase
for SVMs, and (3) the authority decision phase. We will

give a detailed explanation of the three parts in the flow-
ing three subsections.

Figure 5: Block diagram of the proposed access control
system

4.1 Input Pattern Transforming Phase

In this subsection, we shall describe how to make the used
input patterns in the proper form for SVMs. For conve-
nience, it is usual that users� passwords are composed
of English words and/or numerical characters. To make
these textual data suitable for SVM problems, first, we
have to convert these passwords from characters into num-
bers so that we can further train out an appropriate model
of SVMs. To do so, a mapping of characters to numbers
should be constructed in advance so that every textual
input instance can be transformed into a numerical form
before being fed into SVMs. In the proposed scheme, we
directly adopted the most commonly used ASCII code as
the relationship to build this mapping, which is shown in
Table 1.

For example, if a user�s password is �kMX-
oeUZR,� then the converted input pattern will
be (t, 107, 77, 88, 111, 101, 85, 90, 82), which is a 9-
dimensional vector. Here, the additional element t
denotes the number used to represent this user�s login
time section. We will use it with an explanation in the
flowing subsection.

4.2 The Training Phase for SVMs

In this subsection, all the input patterns of users will be
taken into the training procedure in order to build the
SVM models. Suppose that we have a set of collected
passwords for users in advance. Before the transforming
procedure, all the input patterns have to be converted
into the desired numerical formats, as we showed in Sub-
section 4.1. Then, considering the group numbers and
security levels of the users� identities, we assemble these
training examples at first in order to further train the



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 155

Table 1: The mapping table from characters to numbers

needed SVM models. Every training example here is com-
posed of a 9-dimensional vector with the features of xi,
together with one single bit from the desired target la-
bel. Being the same as the simple example we just gave,
these 9-dimensional feature vectors are composed of one
element to represent the users� login times and 8 elements
of the transformed passwords. Every desired target label
here will be treated as a series of bits and processed sep-
arately in order to reduce the complexities of the SVM
training. Note that in the proposed scheme, an unbroken
target label can be viewed as two various parts to express
the number of user groups and its corresponding security
levels.

Figure 6: The sketch of the pattern training procedure

As we show in Figure 6, in the implemented architec-
ture, we use one support vector machine to predict one
bit of the desired target values. In other words, there are
more than one SVMs used in the proposed scheme. Actu-
ally, there will be a total of log(mn) SVMs adopted, where
the notation m is the number representing a group and
n stands for the total number of different security levels.
Formally, what we mentioned above can be formulated
into an SVM problem as:

S = ((x1, y1), (x2, y2), � � � , (xl, yl)) ? (X � Y )
l,

where S is the set of given training examples, and l de-
notes the number of instances in S. Here, X ? Z9 is the
input space and Y is the output domain. Notice that in

the implementation, we will take one support vector ma-
chine which will try to predict one single bit of the target
label yi so that yi must be detached into a series of bits
being predicted separately. For each i from 1 to l, where
xi ? X and yi ? Y , getting the linear functions for each
bit of yi that best interpolates the examples is the even-
tual goal. In short, we are going to apply the support
vector regression function in each SVM node and try to
predict a value close to 0 or 1 according to the given tar-
gets. Finally, after the training procedure, we can obtain
the models used for the prediction of SVMs.

4.3 Authority Decision Phase

After the training procedure, now with the trained mod-
els, we can classify the users into their groups and give
them corresponding security levels using their passwords.
First, the system administrators (SA) have to finish the
training procedure in advance to obtain the models used
by the SVMs. Then, in this phase, every user will be
given a password by the SA to use as the key directly
related to his/her access rights. Moreover, in the pro-
posed scheme, another important thing is that by the use
of the precise classification abilities of the SVMs, the ac-
cess rights of each user is able to be changed according
to the login time. For example, a company�s SA can give
specific employees permission to modify some files only in
the morning, but they do not have permission to write
when evening comes. Therefore, with the proposed ac-
cess control system, the SA can easily build his/her su-
pervision policy using SVMs; the system users will be
precisely classified into their own groups and given their
security levels according to the login time. Therefore, in
the proposed scheme, we can give the convenience that
successfully makes use of the excellent prediction abilities
of SVMs to achieve a fluctuant access control policy with
the time factors involved.

5 Experimental Results

We will practically evaluate the performance of the pro-
posed scheme in this section. As described above, the
security levels of system users need to be varied accord-
ing to the different zones of login time. So, to meet this



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 156

requirement, in the experiments, we divided the hours in
a day into 8 distinct time zones for simulation; that is, by
assigning a distinct number from 0 7 to each time period
consisting of 3 hours, we can easily tell when the users
log in. Besides, in order to test the correctness of the
proposed access control system using SVMs, the testing
patterns (composed of passwords and the numbers repre-
senting the time zones) in the simulation procedure were
entirely the same as those we took in the training stage.
Note that for every security level within a group, there
can be more than one password. By feeding these pat-
terns into the SVMs with the models trained previously,
therefore, we were able to see if the resulting outputs equal
to the expected values.

Library of Support Vector Machines (LIBSVM), which
are available in [4], is an efficient and easy-to-use soft-
ware implementing support vector learning. It not only
solves problems with ease by using SVMs, but it also
provides several selective kernels in order to fit the data
with different types. We made use of LIBSVM as the
tool for our SVM simulating process. In order to ob-
tain the most suitable kernel type of SVMs, we have
tested several kinds of kernels in the experiments which
are linear kernel K(x, y) = (x � y), polynomial kernel
K(x, y) = (?x � y+ c)d and RBF (Radial Basis Function)

kernel K(x, y) = e???x?y?
2

, respectively. Actually, in the
experiments, we used the RBF kernel representation for
our data type because of its better performance in train-
ing speed. Note that the type of kernel used has to be the
same as those in both the training and predicting proce-
dures. We took a total of 8 groups for the simulation.
Moreover, there were 8 different security levels within a
group in the simulation. In other words, along with the
8 time zones in a day, there were a total of 512 testing
instances in the evaluation. Table 2 illustrates the organi-
zation of the used instances in our experiments. They are
composed of the number of login time zones, passwords,
group numbers, and the expected values of security level.
Note that for each password, it is capable to give the dif-
ferent kinds of security levels for the variant login time
zones. As shown in Table 2, the first three bits of the
expected output values are meant to express 8 different
groups in the experiments, and the last three bits stand
for the values of security level corresponding to the 8 dif-
ferent time zones.

In Table 3, we have shown the resulting values de-
rived from 6 units of SVMs which performed the func-
tion of support vector regression. We see that the pre-
dicted errors are bounded very tightly (< 0.01) compared
to the expected values; that is, these resulting values can
be transformed extremely easily the desired bits of secu-
rity levels. Therefore, by simply quantizing these output
values to the desired bits of group numbers and security
levels, we can perfectly achieve the purpose of access con-
trol. Note here that we can arbitrarily scale the range
of the errors made by the prediction of the SVMs; this
is only a simple task of tuning the parameters. However,
the smaller the range of the errors, the more time will

be taken when the SVMs try to converge in the training
phase. Actually, after the procedure of quantizing these
output values with a threshold, a perfect correctness per-
centage is achieved by using the all 512 testing instances.

6 Conclusions

In this paper, we proposed a time-constraint access con-
trol scheme using support vector machines. In this
scheme, the access control system does not store or main-
tain an access control matrix. Only the trained support
vector machines need to be stored in the system. Using
support vector machines, the system can authenticate the
access rights of users to the protected files. The main ad-
vantage of this scheme is that it is applicable to broad
access policies, such as the access rights of users that can
be associated with the time. The system user only holds
one password and then he/she can access various files with
various access rights at various times. As the experimen-
tal results show, the system indeed can authenticate the
access rights of users by using all the 512 testing instances.
Therefore, our scheme is practical such that it can be im-
plemented in more flexible applications.

Acknowledgement

The authors are grateful to Chih-Chung Chang and Chih-
Jen Lin for making LIBSVM software available on the
website for researchers.

References

[1] C. C. Chang, �On the design of a key-lock-pair mech-
anism in information protection system,� BIT, vol.
26, pp. 410-417, 1986.

[2] C. C. Chang, �An information protection scheme
based upon number theory,� Computer Journal, vol.
30, no. 3, pp. 249-253, 1987.

[3] C. C. Chang and D. C. Lou, �A binary access con-
trol method using prime factorization,� Information
Science, vol. 96, no.1-2, pp. 15-26, 1997.

[4] C. C. Chang and C. J. Lin. (2001).
LIBSVM: a library for support vec-
tor machines. [Online]. Available WWW:
http://www.csie.ntu.edu.tw/ cjlin/libsvm.

[5] N. Cristianini and J. S. Taylor, An Introduction to
Support Vector Machines and Other Kernel-based
Learning Methods, Cambridge University Press,
2000.

[6] C. Cortes and V. Vapnik, �Support-vector network,�
Machine Learning, vol. 20, no. 3, pp. 273-297, 1995,

[7] D. Ferraiolo, J. Barkley, and R. Kuhn, �A role based
access control model and reference implementation
within a corporate,� ACM Transactions on Informa-
tion and System Security, vol.2, no.1, pp. 34-64, Feb.
1999.



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 157

Table 2: The set of training instances and their expected outputs



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 158

Table 3: The classification results of group numbers and security levels



International Journal of Network Security, Vol.2, No.2, PP.150-159, Mar. 2006 (http://isrc.nchu.edu.tw/ijns/) 159

[8] G. S. Graham and P. L. Denning, �Protection-
principles and practices,� Proceedings of Spring Jt.
Computer Conference, vol. 40, pp. 417-429, 1972.

[9] M. Hitchens and V. Varadharajan, �Design and spec-
ification of role based access control policies,� IEE
Proceedings of Software, vol. 147, no. 4, pp. 117-129,
2000.

[10] C. W. Hsu and C. J. Lin, �A comparison of meth-
ods for multi-class support vector machines,� IEEE
Transactions on Neural Networks, vol. 13, no. 2, pp.
415-425, 2002.

[11] C. S. Laih, L. Harn, and J. Y. Lee, �On the design of
a single-key-lock mechanism based on Newton�s in-
terpolating polynomial,� IEEE Transactions on Soft-
ware Engineering, vol. 15, no. 9, pp. 1135-1137, 1989.

[12] J. D. Moffett and M. S. Sloman, �The source of au-
thority for commercial access control,� IEEE Com-
puter Journal, vol. 21, no. 2, pp. 59-69, 1988.

[13] S. Raudys, �How good are support vector ma-
chines?,� Neural Networks, vol. 13, no. 1, pp. 17-19.

[14] R. Sandhu, V. Bhamidipati, and Q. Munawer, �The
ARBAC97 model for role-based administration of
roles,� ACM Transactions on Information and Sys-
tem Security, vol. 2, no. 1, pp. 105-135, Feb. 1999.

[15] R. Sandhu, E. J. Coyne, and H. L. Feinstein, �Role
based access control models,� IEEE Computer, vol.
29, no. 2, pp. 38-47, Feb. 1996.

[16] S. K. Shevade, S. S. Keerthi, C. Bhattaacharyya, and
K. R. K. Murthy, �Improvements to the SMO algo-
rithm for SVM regression,� IEEE Transactions on
Neural Networks, vol. 11, no. 5, pp.1188-1193, 2000.

[17] V. N. Vapnik, Statistical Learning Theory, Wiley-
Interscience Publication, 1998.

[18] M. L. Wu and T. Y. Hwang, �Access control with
single-key-lock,� IEEE Transactions on Software En-
gineering, vol. 10, no. 2, pp. 185-191, 1984.

Chin-Chen Chang was born in
Taichung, Taiwan on Nov. 12th, 1954.
He obtained his Ph.D. degree in com-
puter engineering from National Chiao
Tung University. He�s first degree is
Bachelor of Science in Applied Mathe-
matics and master degree is Master of
Science in computer and decision sci-

ences. Both were awarded in National Tsing Hua Univer-
sity. Dr. Chang served in National Chung Cheng Univer-
sity from 1989 to 2005. His current title is Chair Professor
in Department of Information Engineering and Computer
Science, Feng Chia University, from Feb. 2005.

Prior to joining Feng Chia University, Professor Chang
was an associate professor in Chiao Tung University, pro-
fessor in National Chung Hsing University, chair professor
in National Chung Cheng University. He had also been
Visiting Researcher and Visiting Scientist to Tokyo Uni-
versity and Kyoto University, Japan. During his service
in Chung Cheng, Professor Chang served as Chairman of

the Institute of Computer Science and Information Engi-
neering, Dean of College of Engineering, Provost and then
Acting President of Chung Cheng University and Director
of Advisory Office in Ministry of Education, Taiwan.

Professor Chang�s specialties include, but not limited
to, data engineering, database systems, computer cryp-
tography and information security. A researcher of ac-
claimed and distinguished services and contributions to
his country and advancing human knowledge in the field
of information science, Professor Chang has won many
research awards and honorary positions by and in pres-
tigious organizations both nationally and internationally.
He is currently a Fellow of IEEE and a Fellow of IEE, UK.
On numerous occasions, he was invited to serve as Visit-
ing Professor, Chair Professor, Honorary Professor, Hon-
orary Director, Honorary Chairman, Distinguished Alum-
nus, Distinguished Researcher, Research Fellow by univer-
sities and research institutes. He also published over 850
papers in Information Sciences. In the meantime, he par-
ticipates actively in international academic organizations
and performs advisory work to government agencies and
academic organizations.

Iuon-Chang Lin received the B.S.
in Computer and Information Sciences
from Tung Hai University, Taichung,
Taiwan, Republic of China, in 1998;
the M.S. in Information Management
from Chaoyang University of Technol-
ogy, Taiwan, in 2000. He received his
Ph.D. in Computer Science and Infor-

mation Engineering in March 2004 from National Chung
Cheng University, Chiayi, Taiwan. He is currently an
assistant professor of the Department of Management
Information Systems, National Chung Hsing University,
Taichung, Taiwan, ROC. His current research interests
include electronic commerce, information security, cryp-
tography, and mobile communications.

Chia-Te Liao received the M.S.
in Computer Science and Informa-
tion Engineering from National Chung
Cheng University, Chiayi, Taiwan, Re-
public of China. His current research
interests include information security
and cryptography.


