
ar
X

iv
:c

on
d-

m
at

/0
61

23
65

v2
  [

co
nd

-m
at.

sta
t-m

ec
h]

  2
6 F

eb
 20

07

Gibbs states and the set of
solutions of random constraint
satisfaction problems
Florent Krzakala ?, Andrea Montanari � ��, Federico Ricci-Tersenghi�, Guilhem Semerjian � , and Lenka Zdeborova� ?
?Lab. PCT, ESPCI, Paris, France,�Depts of Electrical Engineering and Statistics, Stanford University, USA,�LPTENS, Ecole Normale Supe�rieure and UPMC, Paris,
France,�Dipartimento di Fisica and CNR-INFM, Universita` �La Sapienza�, Roma, Italy, and ?LPTMS, Universite� de Paris-Sud, Orsay, France

Submitted to Proceedings of the National Academy of Sciences of the United States of America

An instance of a random constraint satisfaction problem defines
a random subset S (the set of solutions) of a large product space
XN (the set of assignments). We consider two prototypical prob-
lem ensembles (random k-satisfiability and q-coloring of random
regular graphs), and study the uniform measure with support on
S. As the number of constraints per variable increases, this mea-
sure first decomposes into an exponential number of pure states
(�clusters�), and subsequently condensates over the largest such
states. Above the condensation point, the mass carried by the n
largest states follows a Poisson-Dirichlet process.
For typical large instances, the two transitions are sharp. We de-
termine for the first time their precise location. Further, we provide
a formal definition of each phase transition in terms of different
notions of correlation between distinct variables in the problem.
The degree of correlation naturally affects the performances of
many search/sampling algorithms. Empirical evidence suggests
that local Monte Carlo Markov Chain strategies are effective up to
the clustering phase transition, and belief propagation up to the
condensation point. Finally, refined message passing techniques
(such as survey propagation) may beat also this threshold.

Phase transitions | Random graphs | Constraint satisfaction problems |
Message passing algorithms

Constraint satisfaction problems (CSPs) arise in a large spectrumof scientific disciplines. An instance of a CSP is said to be satisfi-
able if there exists an assignment of N variables (x1, x2, . . . , xN ) ?
x, xi ? X (X being a finite alphabet) which satisfies all the con-
straints within a given collection. The problem consists in find-
ing such an assignment or show that the constraints are unsatisfi-
able. More precisely, one is given a set of functions ?a : X k ?
{0, 1}, with a ? {1, . . . ,M} ? [M ] and of k-tuples of indices
{ia(1), . . . , ia(k)} ? [N ], and has to establish whether there exists
x ? XN such that ?a(xia(1), . . . , xia(k)) = 1 for all a�s. In this arti-
cle we shall consider two well known families of CSP�s (both known
to be NP-complete [1]):

(i) k-satisfiability (k-SAT) with k ? 3. In this case
X = {0, 1}. The constraints are defined by fix-
ing a k-tuple (za(1), . . . , za(k)) for each a, and set-
ting ?a(xia(1), . . . , xia(k)) = 0 if (xia(1), . . . , xia(k)) =
(za(1), . . . , za(k)) and = 1 otherwise.

(ii) q-coloring (q-COL) with q ? 3. Given a graph G with N ver-
tices and M edges, one is asked to assign colors xi ? X ?
{1, . . . , q} to the vertices in such a way that no edge has the
same color at both ends.

The optimization (maximize the number of satisfied constraints)
and counting (count the number of satisfying assignments) versions
of this problems are defined straightforwardly. It is also convenient
to represent CSP instances as factor graphs [2], i.e. bipartite graphs
with vertex sets [N ], [M ] including an edge between node i ? [N ]

x

x
x

xx 1

5
7

32

x6

x4

a b

c

Fig. 1. The factor graph of a small CSP allows to define the distance d(i, j)
between variables xi and xj (filled squares are constraints and empty circles
variables). Here, for instance, d(6, 1) = 2 and d(3, 5) = 1.

and a ? [M ] if and only if the i-th variable is involved in the a-th
constraint, cf. Fig. 1. This representation allows to define naturally a
distance d(i, j) between variable nodes.

Ensembles of random CSP�s (rCSP) were introduced (see e.g. [3])
with the hope of discovering generic mathematical phenomena that
could be exploited in the design of efficient algorithms. Indeed several
search heuristics, such as Walk-SAT [4] and �myopic� algorithms [5]
have been successfully analyzed and optimized over rCSP ensembles.
The most spectacular advance in this direction has probably been the
introduction of a new and powerful message passing algorithm (�sur-
vey propagation�, SP) [6]. The original justification for SP was based
on the (non-rigorous) cavity method from spin glass theory. Subse-
quent work proved that standard message passing algorithms (such as
belief propagation, BP) can indeed be useful for some CSP�s [7, 8, 9].
Nevertheless, the fundamental reason for the (empirical) superiority
of SP in this context remains to be understood and a major open prob-
lem in the field. Building on a refined picture of the solution set of
rCSP, this paper provides a possible (and testable) explanation. We
consider two ensembles that have attracted the majority of work in the
field: (i) random k-SAT: each k-SAT instance with N variables and
M = N? clauses is considered with the same probability; (ii) q-COL
on random graphs: the graph G is uniformly random among the ones
over N vertices, with uniform degree l (the number of constraints is
therefore M = Nl/2).
Phase transitions in random CSP. It is well known that rCSP�s
may undergo phase transitions as the number of constraints per vari-
able ? is varied1. The best known of such phase transitions is the
SAT-UNSAT one: as ? crosses a critical value ?s(k) (that can, in
principle, depend on N ), the instances pass from being satisfiable to

Conflict of interest footnote placeholder

Abbreviations: rCSP, random constraint satisfaction problem; 1RSB, one-step replica symme-
try breaking
�To whom correspondence should be addressed. E-mail: montanari@stanford.edu
c�2006 by The National Academy of Sciences of the USA

1For coloring l-regular graphs, we can use l = 2? as a parameter. When considering a
phase transition defined through some propertyP increasing in l, we adopt the convention of
denoting its location through the smallest integer such that P holds.
2The term �with high probability� (whp) means with probability approaching one as N ??.

www.pnas.org � � PNAS Issue Date Volume Issue Number 1�8




Table 1. Critical connectivities for the dynamical, con-
densation and satisfiability transitions in k-SAT and q-COL
SAT ?d ?c ?s[11] COL ld[16] lc ls[14]
k = 4 9.38 9.547 9.93 q = 4 9 10 10
k = 5 19.16 20.80 21.12 q = 5 14 14 15
k = 6 36.53 43.08 43.4 q = 6 18 19 20

unsatisfiable with high probability2 [10]. For k-SAT, it is known that
?s(2) = 1. A conjecture based on the cavity method was put forward
in [6] for all k ? 3 that implied in particular the values presented in Ta-
ble 1 and ?s(k) = 2k log 2? 12 (1+log 2)+O(2

?k) for large k [11].
Subsequently it was proved that ?s(k) ? 2k log 2?O(k) confirming
this asymptotic behavior [12]. An analogous conjecture for q-coloring
was proposed in [13] yielding, for regular random graphs [14], the val-
ues reported in Table 1 and ls(q) = 2q log q?log q?1+o(1) for large
q (according to our convention, random graphs are whp uncolorable if
l ? ls(q)). It was proved in [15, 12] that ls(q) = 2q log q?O(log q).

Even more interesting and challenging are phase transitions in the
structure of the set S ? XN of solutions of rCSP�s (�structural� phase
transitions). Assuming the existence of solutions, a convenient way
of describing S is to introduce the uniform measure over solutions
�(x):

�(x) =
1

Z

MY
a=1

?a(xia(1), . . . , xia(k)) , [1]

where Z ? 1 is the number of solutions. Let us stress that, since S
depends on the rCSP instance, �( � ) is itself random.

We shall now introduce a few possible �global� characterizations
of the measure �( � ). Each one of these properties has its counter-
part in the theory of Gibbs measures and we shall partially adopt that
terminology here [17].

In order to define the first of such characterizations, we let i ? [N ]
be a uniformly random variable index, denote as x? the vector of vari-
ables whose distance from i is at least ?, and by �(xi|x?) the marginal
distribution of xi given x?. Then we say that the measure [1] satisfies
the uniqueness condition if, for any given i ? [N ],

E sup
x
?
,x?
?

X
xi?X

??
�(xi|x?)? �(xi|x

?
?)
??
? 0 . [2]

as ??? (here and below the limitN ?? is understood to be taken
before ???). This expresses a �worst case� correlation decay con-
dition. Roughly speaking: the variable xi is (almost) independent of
the far apart variablesx? irrespective is the instance realization and the
variables distribution outside the horizon of radius ?. The threshold for
uniqueness (above which uniqueness ceases to hold) was estimated
in [9] for random k-SAT, yielding ?u(k) = (2 log k)/k[1 + o(1)]
(which is asymptotically close to the threshold for the pure literal
heuristics) and in [18] for coloring implying lu(q) = q for q large
enough (a �numerical� proof of the same statement exists for small q).
Below such thresholds BP can be proved to return good estimates of
the local marginals of the distribution [1].

Notice that the uniqueness threshold is far below the SAT-UNSAT
threshold. Furthermore, several empirical studies [19, 20] pointed out
that BP (as well as many other heuristics [4, 5]) is effective up to much
larger values of the clause density. In a remarkable series of papers
[21, 6], statistical physicists argued that a second structural phase
transition is more relevant than the uniqueness one. Following this
literature, we shall refer to this as the �dynamic phase transition� (DPT)
and denote the corresponding threshold as ?d(k) (or ld(q)). In order
to precise this notion, we provide here two alternative formulations

?d,+ ?d ?c ?s

Fig. 2. Pictorial representation of the different phase transitions in the set of
solutions of a rCSP. At?d,+ some clusters appear, but for?d,+ < ? < ?d they
comprise only an exponentially small fraction of solutions. For?d < ? < ?c the
solutions are split among about eN?? clusters of size eNs? . If ?c < ? < ?s
the set of solutions is dominated by a few large clusters (with strongly fluctuating
weights), and above ?s the problem does not admit solutions any more.

corresponding to two distinct intuitions. According to the first one,
above ?d(k) the variables (x1, . . . , xN) become globally correlated
under �( � ). The criterion in [2 ] is replaced by one in which far apart
variables x? are themselves sampled from� (�extremality� condition):

E

X
x
?

�(x?)
X
xi

|�(xi|x?)? �(xi)| ? 0 . [3]

as ? ? ?. The infimum value of ? (respectively l) such that
this condition is no longer fulfilled is the threshold ?d(k) (ld(k)).
Of course this criterion is weaker than the uniqueness one (hence
?d(k) ? ?u(k)).

According to the second intuition, above ?d(k), the measure
[1 ] decomposes into a large number of disconnected �clusters�.
This means that there exists a partition {An}n=1...N of XN (de-
pending on the instance) such that: (i) One cannot find n such
that �(An) ? 1; (ii) Denoting by ??A the set of configurations
x ? XN\Awhose Hamming distance fromA is at mostN?, we have
�(??An)/�(An)(1? �(An))? 0 exponentially fast in N for all n
and ? small enough. Notice that the measure � can be decomposed as

�( � ) =
NX
n=1

wn �n( � ) , [4]

where wn ? �(An) and �n( � ) ? �( � |An). We shall always refer
to {An} as the �finer� partition with these properties.

The above ideas are obviously related to the performance of algo-
rithms. For instance, the correlation decay condition in [3 ] is likely to
be sufficient for approximate correctness of BP on random formulae.
Also, the existence of partitions as above implies exponential slowing
down in a large class of MCMC sampling algorithms3.

Recently, some important rigorous results were obtained support-
ing this picture [22, 23]. However, even at the heuristic level, several
crucial questions remain open. The most important concern the dis-
tribution of the weights {wn}: are they tightly concentrated (on an
appropriate scale) or not? A (somewhat surprisingly) related ques-
tion is: can the absence of decorrelation above ?d(k) be detected by
probing a subset of variables bounded in N?

SP [6] can be thought as an inference algorithm for a modified
graphical model that gives unit weight to each cluster [24, 20], thus
tilting the original measure towards small clusters. The resulting per-
formances will strongly depend on the distribution of the cluster sizes
wn. Further, under the tilted measure, ?d(k) is underestimated be-
cause small clusters have a larger impact. The correct value was never
determined (but see [16] for coloring). The authors of [25] undertook

3One possible approach to the definition of a MCMC algorithm is to relax the constraints by
setting?a(� � � ) = ? instead of 0 whenever the a-th constraint is violated. Glauber dynamics
can then be used to sample from the relaxed measure �?( � ).

2 www.pnas.org � � Footline Author



?(s)

s

?s(k)?c(k)

m(?)

1

0.5

0

Fig. 3. The Parisi 1RSB parameter m(?) as a function of the constraint den-
sity ?. In the inset, the complexity ?(s) as a function of the cluster entropy for
? = ?s(k) ? 0.1 (the slope at ?(s) = 0 is ?m(?)). Both curves have been
computed from the large k expansion.

the technically challenging task of determining the cluster size distri-
bution, without however clarifying several of its properties.

In this paper we address these issues, and unveil at least two un-
expected phenomena. Our results are described in the next Section
with a summary just below. Finally we will discuss the connection
with the performances of SP. Some technical details of the calculation
are collected in the last Section.

Results and discussion
The formulation in terms of extremality condition, cf. Eq. [3 ], allows
for an heuristic calculation of the dynamic threshold ?d(k). Previous
attempts were based instead on the cavity method, that is an heuristic
implementation of the definition in terms of pure state decomposition,
cf. Eq. [4]. Generalizing the results of [16], it is possible to show that
the two calculations provide identical results. However, the first one
is technically simpler and under much better control. As mentioned
above we obtain, for all k ? 4 a value of ?d(k) larger than the one
quoted in [6, 11].

Further we determined the distribution of cluster sizes wn, thus
unveiling a third �condensation� phase transition at ?c(k) ? ?d(k)
(strict inequality holds for k ? 4 in SAT and q ? 4 in coloring, see
below). For ? < ?c(k) the weights wn concentrate on a logarith-
mic scale (namely ? logwn is ?(N) with ?(N1/2) fluctuations).
Roughly speaking the measure is evenly split among an exponential
number of clusters.

For ? > ?c(k) (and < ?s(k)) the measure is carried by a subex-
ponential number of clusters. More precisely, the ordered sequence
{wn} converges to a well known Poisson-Dirichlet process{w?n}, first
recognized in the spin glass context by Ruelle [26]. This is defined by
w?n = xn/

P
xn, where xn > 0 are the points of a Poisson process

with rate x?1?m(?) andm(?) ? (0, 1). This picture is known in spin
glass theory as �one step replica symmetry breaking� (1RSB) and has
been proven in Ref. [27] for some special models. The �Parisi 1RSB
parameter� m(?) is monotonically decreasing from 1 to 0 when ?
increases from ?c(k), to ?s(k), cf. Fig. 3.

Remarkably the condensation phase transition is also linked to
an appropriate notion of correlation decay. If i(1), . . . , i(n) ? [N ]
are uniformly random variable indices, then, for ? < ?c(k) and any
fixed n:

E

X
{xi(�)}

??
�(xi(1) . . . xi(n))? �(xi(1)) � � ��(xi(n))

??
? 0 [5]

as N ? ?. Conversely, the quantity on the left hand side remains
positive for ? > ?c(k). It is easy to understand that this condition is
even weaker than the extremality one, cf. Eq. [3 ], in that we probe

correlations of finite subsets of the variables. In the next two Sections
we discuss the calculation of ?d and ?c.
Dynamic phase transition and Gibbs measure extremality.
A rigorous calculation of ?d(k) along any of the two definitions pro-
vided above, cf. Eqs. [3 ] and [4] remains an open problem. Each of
the two approaches has however an heuristic implementation that we
shall now describe. It can be proved that the two calculations yield
equal results as further discussed in the last Section of the paper.

The approach based on the extremality condition in [3 ] relies on
an easy-to-state assumption, and typically provides a more precise
estimate. We begin by observing that, due to the Markov structure of
�( � ), it is sufficient for Eq. [3] to hold that the same condition is ver-
ified by the correlation between xi and the set of variables at distance
exactly ? from i, that we shall keep denoting as x?. The idea is then to
consider a large yet finite neighborhood of i. Given ?� ? ?, the factor
graph neighborhood of radius ?� around i converges in distribution to
the radius�?� neighborhood of the root in a well defined random tree
factor graph T .

For coloring of random regular graphs, the correct limiting tree
model T is coloring on the infinite l-regular tree. For random k-SAT,
T is defined by the following construction. Start from the root vari-
able node and connect it to l new function nodes (clauses), l being a
Poisson random variable of mean k?. Connect each of these function
nodes with k ? 1 new variables and repeat. The resulting tree is infi-
nite with non-vanishing probability if ? > 1/k(k ? 1). Associate a
formula to this graph in the usual way, with each variable occurrence
being negated independently with probability 1/2.

The basic assumption within the first approach is that the ex-
tremality condition in [3 ] can be checked on the correlation between
the root and generation-? variables in the tree model. On the tree,
�( � ) is defined to be a translation invariant Gibbs measure [17] asso-
ciated to the infinite factor graph4 T (which provides a specification).
The correlation between the root and generation-? variables can be
computed through a recursive procedure (defining a sequence of dis-
tributions P ?, see Eq. [15] below). The recursion can be efficiently
implemented numerically yielding the values presented in Table 1 for
k (resp. q)= 4, 5, 6. For large k (resp. q) one can formally expand
the equations on P? and obtain

?d(k) =
2k

k

�
log k + log log k + ?d +O

�
log log k

log k

��
[6]

ld(q) = q [log q + log log q + ?d + o(1)] [7]

with ?d = 1 (under a technical assumption on the structure of P?).
The second approach to the determination of ?d(k) is based on

the �cavity method� [6, 25]. It begins by assuming a decomposition in
pure states of the form [4 ] with two crucial properties: (i) If we denote
by Wn the size of the n-th cluster (and hence wn = Wn/

P
Wn),

then the number of clusters of size Wn = eNs grows approximately
as eN?(s); (ii) For each single-cluster measure �n( � ), a correlation
decay condition of the form [3 ] holds.

The approach aims at determining the rate function ?(s), �com-
plexity�: the result is expressed in terms of the solution of a distri-
butional fixed point equation. For the sake of simplicity we describe
here the simplest possible scenario5 resulting from such a calcula-
tion, cf. Fig. 4. For ? < ?d,??(k) the cavity fixed point equation
does not admit any solution: no clusters are present. At ?d,??(k) a
solution appears, eventually yielding, for ? > ?d,+ a non-negative

4More precisely�( � ) is obtained as a limit of free boundary measures (further details in [28]).
5The precise picture depends on the value of k (resp. q) and can be somewhat more compli-
cated.

Footline Author PNAS Issue Date Volume Issue Number 3



l = 20

l = 19

l = 18

l = 17

s

?(s)

0.140.120.10.080.060.040.020

0.15

0.1

0.05

0

-0.05

Fig. 4. The complexity function (the number of clusters with entropy density
s is eN?(s)) for the 6-colorings of l-regular graphs with l ? {17, 18, 19, 20}.
Circles indicate the dominating states with entropy s?; the dashed lines have
slopes ??(s?) = ?1 for l = 18 and ??(s?) = ?0.92 for l = 19. The
dynamic phase transition is ld(6) = 18, the condensation one ld(6) = 19, and
the SAT-UNSAT one ls(6) = 20.

9.559.59.45

0.004

0.002

0

?

C?

250200150100500

0.6

0.4

0.2

0

Fig. 5. Correlation function [3 ] between the root and generation ? variables in
a random k-SAT tree formula. Here k = 4 and (from bottom to top) ? = 9.30,
9.33, 9.35, 9.40 (recall that ?d(4) ? 9.38). In the inset, the complexity ?(s?)
of dominant clusters as a function of ? for 4-SAT.

complexity ?(s) for some values of s ? R+. The maximum and
minimum such values will be denoted by smax and smin. At a strictly
larger value ?d,0(k), ?(s) develops a stationary point (local max-
imum). It turns out that ?d,0(k) coincides with the threshold com-
puted in [6, 11, 14]. In particular ?d,0(4) ? 8.297, ?d,0(5) ? 16.12,
?d,0(6) ? 30.50 and ld,0(4) = 9, ld,0(5) = 13, ld,0(6) = 17. For
large k (resp. q), ?d,0(k) admits the same expansion as in Eqs. [6 ],
[7 ] with ?d,0 = 1? log 2. However, up to the larger value ?d(k), the
appearance of clusters is irrelevant from the point of view of �( � ). In
fact, within the cavity method it can be shown that eN[s+?(s)] remains
exponentially smaller than the total number of solutions Z: most of
the solutions are in a single �cluster�. The value ?d(k) is determined
by the appearance of a point s? with ??(s?) = ?1 on the complexity
curve. Correspondingly, one has Z ? eN[?(s?)+s?]: most of the so-
lutions are comprised in clusters of size about eNs? . The entropy per
variable ? = limN??N?1 logZ remains analytic at ?d(k).

Condensationphase transition. As? increases above?d ,?(s?)
decreases: clusters of highly correlated solutions may no longer sat-
isfy the newly added constraints. In the inset of Fig. 5 we show
the ? dependency of ?(s?) for 4-SAT. In the large k limit, with
? = ? 2k we get ?(s?) = log 2 ? ? ? log 2 e?k? + O(2?k), and
s? = log 2 e

?k? +O(2?k).
The condensation point ?c(k) is the value of ? such that ?(s?)

vanishes: above ?c(k), most of the measure is contained in a sub-

N = 5 � 103
N = 104
N = 2 � 104

4-SAT ? = 9.5

4-SAT ? = 9

fraction of fixed variables

s

10.80.60.40.20

0.1

0.08

0.06

0.04

0.02

0

Fig. 6. Performance of BP heuristics on random 4-SAT formulae. The residual
entropy per spin N?1 logZ (here we estimate it within Bethe approximation) as
a function of the fraction of fixed variables. tmax = 20 in these experiments.

exponential number of large clusters6. Our estimates for ?c(k) are
presented in Table 1 (see also Fig. 4 for ?(s) in the 6-coloring) while
in the large-k limit we obtain ?c(k) = 2k log 2? 32 log 2 +O(2

?k)

[recall that the SAT-UNSAT transition is at ?s(k) = 2k log 2 ?
1+log 2

2
+O(2?k)] and lc(q) = 2q log q?log q?2 log 2+o(1) [with

the COL-UNCOL transition at ls(q) = 2q log q? log q? 1+ o(1)].
Technically the size of dominating clusters is found by maximiz-
ing ?(s) + s over the s interval on which ?(s) ? 0. For ? ?
[?c(k), ?s(k)], the maximum is reached at smax, with ?(smax) = 0
yielding ? = smax. It turns out that the solutions are comprised
within a finite number of clusters, with entropy eNsmax+?, where
? = ?(1). The shifts ? are asymptotically distributed according to
a Poisson point process of rate e?m(?)? with m(?) = ???(smax).
This leads to the Poisson Dirichlet distribution of weights discussed
above. Finally, the entropy per variable ? is non-analytic at ?c(k).

Let us conclude by stressing two points. First, we avoided the
3-SAT and 3-coloring cases. These cases (as well as the 3-coloring
on Erdo�s-Re�nyi graphs [25]) are particular in that the dynamic tran-
sition point ?d is determined by a local instability (a Kesten-Stigum
[29] condition, see also [21]), yielding ?d(3) ? 3.86 and ld(3) = 6
(the case l = 5, q = 3 being marginal). Related to this is the fact that
?c = ?d: throughout the clustered phase, the measure is dominated
by a few large clusters (technically, ?(s?) < 0 for all ? > ?d). Sec-
ond, we did not check the �local stability� of the 1RSB calculation.
By analogy with [30], we expect that an instability can modify the
curve ?(s) but not the values of ?d and ?c.
Algorithmic implications. Two message passing algorithms were
studied extensively on random k-SAT: belief propagation (BP) and
survey propagation (SP) (mixed strategies were also considered in
[19, 20]). A BP message ?u?v(x) between nodes u and v on the
factor graph is usually interpreted as the marginal distribution of xu
(or xv) in a modified graphical model. An SP message is instead a
distribution over such marginals Pu?v(?). The empirical superiority
of SP is usually attributed to the existence of clusters [6]: the distri-
bution Pu?v(?) is a �survey� of the marginal distribution of xu over
the clusters. As a consequence, according to the standard wisdom, SP
should outperform BP for ? > ?d(k).

This picture has however several problems. Let us list two of
them. First, it seems that essentially local algorithms (such as mes-
sage passing ones) should be sensitive only to correlations among

6Notice that forq-coloring, since l is an integer, the �condensated� regime [lc(q), ls(q)]may be
empty: This is the case forq=4. On the contrary,q=5 is always condensated for ld < l < ls .
7This paradox was noticed independently by Dimitris Achlioptas (personal communication).

4 www.pnas.org � � Footline Author



finite subsets of the variables7, and these remain bounded up to the
condensation transition. Recall in fact that the extremality condition
in [3 ] involves a number of variables unbounded in N , while the
weaker in [5] is satisfied up to ?c(k).

Secondly, it would be meaningful to weight uniformly the solu-
tions when computing the surveys Pu?v(?). In the cavity method
jargon, this corresponds to using a 1RSB Parisi parameter r = 1
instead of r = 0 as is done in [6]. It is a simple algebraic fact of
the cavity formalism that for r = 1 the means of the SP surveys
satisfy the BP equations. Since the means are the most important
statistics used by SP to find a solution, BP should perform roughly
as SP. Both arguments suggest that BP should perform well up to the
condensation point ?c(k). We tested this conclusion on 4-SAT at
? = 9.5 ? (?d(4), ?c(4)), through the following numerical experi-
ment, cf. Fig. 6. (i)Run BP for tmax iterations. (ii) Compute the BP
estimates ?i(x) for the single bit marginals and choose the one with
largest bias. (iii) Fix xi = 0 or 1 with probabilities ?i(0), ?i(1).
(iv) Reduce the formula accordingly (i.e. eliminate the constraints
satisfied by the assignment of xi and reduce the ones violated). This
cycle is repeated until a solution is found or a contradiction is en-
countered. If the marginals ?i( � ) were correct, this procedure would
provide a satisfying assignment sampled uniformly from �( � ). In
fact we found a solution with finite probability (roughly 0.4), despite
the fact that ? > ?d(4). The experiment was repeated at ? = 9 with
a similar fraction of successes (more data on the success probability
will be reported in [31]).

Above the condensation transition, correlations become too strong
and the BP fixed point no longer describes the measure �. Indeed the
same algorithm proved unsuccessful at ? = 9.7 ? (?c(4), ?s(4)).
As mentioned above, SP can be regarded as an inference algorithm in
a modified graphical model that weights preferentially small clusters.
More precisely, it selects clusters of size eNs� with s� maximizing the
complexity ?(s). With respect to the new measure, the weak corre-
lation condition in [5] still holds and allows to perform inference by
message passing.

Within the cavity formalism, the optimal choice would be to take
r ? m(?) ? [0, 1). Any parameter corresponding to a non-negative
complexity r ? [0, m(?)] should however give good results. SP cor-
responds to the choice r = 0 that has some definite computational
advantages, since messages have a compact representation in this case
(they are real numbers).

Cavity formalism, tree reconstruction and SP
This Section provides some technical elements of our computation.
The reader not familiar with this topic is invited to further consult
Refs. [6, 11, 25, 32] for a more extensive introduction. The expert
reader will find a new derivation, and some hints of how we overcame
technical difficulties. A detailed account shall be given in [31, 33].

On a tree factor graph, the marginals of�( � ), Eq. [1 ] can be com-
puted recursively. The edge of the factor graph from variable node i
to constraint node a (respectively from a to i) carries �message� ?i?a
(?a?i), a probability measure on X defined as the marginal of xi in
the modified graphical model obtained by deleting constraint node a
(resp. all constraint nodes around i apart from a). The messages are
determined by the equations

?i?a(xi) =
1

zi?a({?b?i})

Y
b??i\a

?b?i(xi) , [8]

?a?i(xi) =
1bza?i({?j?a})

X
x
?a\i

?a(x?a)
Y

j??a\i

?j?a(xj) , [9]

where ?u is the set of nodes adjacent to u, \ denotes the set sub-
traction operation, and xA = {xj : j ? A}. These are just the BP
equations for the model [1 ]. The constants zi?a, bza?i are uniquely
determined from the normalization conditions

P
xi
?i?a(xi) =P

xi
?a?i(xi) = 1. In the following we refer to these equations

by introducing functions fi?a( � ), fa?i( � ) such that

?i?a = fi?a({?b?i}b??i\a) , ?a?i = fa?i({?j?a}j??a\i) ,
[10]

The marginals of � are then computed from the solution of these equa-
tions. For instance �(xi) is a function of the messages ?a?i from
neighboring function nodes.

The log-number of solutions, logZ, can be expressed as a sum
of contributions which are local functions of the messages that solve
Eqs. [8 ], [9 ]

logZ =
X
a

log za({?i?a}) +
X
i

log zi({?a?i})+

?
X
(ai)

log zai(?i?a, ?a?i) [11]

where the last sum is over undirected edges in the factor graph and

za ?
X
x
?a

?a(x?a)
Y
i??a

?i?a(xi) ,

zi ?
X
xi

Y
a??i

?a?i(xi) , zai ?
X
xi

?i?a(xi)?a?i(xi) .

Each term z gives the change in the number of solutions when merging
different subtrees (for instance log zi is the change in entropy when
the subtrees around i are glued together). This expression coincides
with the Bethe free-energy [34] as expressed in terms of messages.

In order to move from trees to loopy graphs, we first consider an
intermediate step in which the factor graph is still a tree but a subset of
the variables, xB = {xj : j ? B} is fixed. We are therefore replacing
the measure �( � ), cf. Eq. [1 ], with the conditional one �( � |xB). In
physics terms, the variables in xB specify a boundary condition.

Notice that the measure �( � |xB) still factorizes according to (a
subgraph of) the original factor graph. As a consequence, the condi-
tional marginals �(xi|xB) can be computed along the same lines as
above. The messages ?xBi?a and ?

xB
a?i obey Eqs. [10], with an appro-

priate boundary condition for messages from B. Also, the number of
solutions that take valuesxB on j ? B (call itZ(xB)) can be computed
using Eq. [11].

Next, we want to consider the boundary variables themselves as
random variables. More precisely, given r ? R, we let the boundary
to be xB with probability

e�(xB) = Z(xB)rZ(r) , [12]
where Z(r) enforces the normalization

P
xB
e�(xB) = 1. Define

Pi?a(?) as the probability density of ?xBi?a when xB is drawn frome�, and similarly Qa?i(?). One can show that Eq. [8] implies the
following relation between messages distributions

Pi?a(?) =
1

Zi?a

Z Y
b??i\a

dQb?i(?b) ?[??fi?a({?b})] zi?a({?b})
r,

[13]
where fi?a is the function defined in Eq. [10], zi?a is determined
by Eq. [8 ], and Zi?a is a normalization. A similar equation holds
for Qa?i(?). These coincide with the �1RSB equations� with Parisi
parameter r. Survey propagation (SP) corresponds to a particular pa-
rameterization of Eq. [13] (and the analogous one expressing Qa?i
in terms of the P �s) valid for r = 0.

Footline Author PNAS Issue Date Volume Issue Number 5



The log-partition function?(r) = logZ(r) admits an expression
that is analogous to Eq. [11],

logZ(r) =
X
a

logZa({Pi?a}) +
X
i

logZi({Qa?i})+

?
X
ai

logZai(Pi?a, Qa?i) [14]

where the �shifts� Z(� � � ) are defined through moments of order r
of the z�s, and sums run over vertices not in B. For instance Zai is
the expectation of zai(?, ?)r when ?, ? are independent random vari-
ables with distribution (respectively)Pi?a andQa?i. The (Shannon)
entropy of the distribution e� is given by ?(r) = ?(r)? r??(r).

As mentioned, the above derivation holds for tree factor graphs.
Nevertheless, the local recursion equations [10], [13] can be used
as an heuristics on loopy factor graphs as well. Further, although we
justified Eq. [13] through the introduction of a random boundary
condition xB, we can take B = ? and still look for non-degenerate
solutions of such equations.

Starting from an arbitrary initialization of the messages, the re-
cursions are iterated until an approximate fixed point is reached. After
convergence, the distributionsPi?a,Qa?i can be used to evaluate the
potential ?(r), cf. Eq. [14]. From this we compute the complexity
function ?(r) ? ?(r) ? r??(r), that gives access to the decompo-
sition of �( � ) in pure states. More precisely, ?(r) is the exponential
growth rate of the number of states with internal entropy s = ??(r).
This is how curves such as in Fig. 4 are traced.

In practice it can be convenient to consider the distributions of
messages Pi?a, Qa?i with respect to the graph realization. This
approach is sometimes referred to as �density evolution� in coding
theory. If one consider a uniformly random directed edge i ? a
(or a ? i) in a rCSP instance, the corresponding message will be
a random variable. After t parallel updates according to Eq. [13],
the message distribution converges (in the N ? ? limit) to a well
defined law Pt (for variable to constraint messages) or Qt (for con-
straint to variable). As t??, these converge to a fixed point P , Q
that satisfy the distributional equivalent of Eq. [13].

To be definite, let us consider the case of graph coloring. Since
the compatibility functions are pairwise in this case (i.e. k = 2 in
Eq. [1]), the constraint-to-variable messages can be eliminated and

Eq. [13] takes the form

Pi?j(?) ?

Z Y
l??i\j

dPl?i(?l) ? [? ? f({?l})] z({?l})
r ,

where f is defined by ?(x) = z?1
Q

l 1 ? ?l(x) and z by normal-
ization. The distribution of Pi?j is then assumed to satisfy a distri-
butional version of the last equation. In the special case of random
regular graphs, a solution is obtained by assuming that Pi?j is indeed
independent of the graph realization and of i, j. One has therefore
simply to set Pi?j = P in the above and solve it for P .

In general, finding messages distributions P , Q that satisfy the
distributional version of Eq. [13] is an extremely challenging task,
even numerically. We adopted the population dynamics method [32]
which consists in representing the distributions by samples (this is
closely related to particle filters in statistics). For instance, one rep-
resents P by a sample of P �s, each encoded as a list of ?�s. Since
computer memory drastically limits the samples size, and thus the pre-
cision of the results, we worked in two directions: (1)We analytically
solved the distributional equations for large k (in the case of k-SAT) or
q (q-coloring); (2) We identified and exploited simplifications arising
for special values of r.

Let us briefly discuss point (2). Simplifications emerge for r = 0
and r = 1. The first case correspond to SP: Refs. [6, 11] showed
how to compute efficiently ?(r = 0) through population dynamics.
Building on this, we could show that the clusters internal entropy
s(r = 0) can be computed at a small supplementary cost (see [31]).

The value r = 1 corresponds instead to the �tree reconstruction�
problem [35]: In this case e�(xB), cf. Eq. [12], coincides with the
marginal of �. Averaging Eq. [13] (and the analogous one forQa?i)
one obtains the BP equations [8], [9 ], e.g. R dPi?a(?) ? = ?i?a.
These remark can be used to show that the constrained averages

P (?, ?) =

Z
dP [P ] P (?) ?

�
? ?

Z
dP (??)??

�
, [15]

and Q(?, ?) (defined analogously) satisfy closed equations which are
much easier to solve numerically.
We are grateful to J. Kurchan and M. Me�zard for stimulating discussions.
This work has been partially supported by the European Commission under
contracts EVERGROW and STIPCO.

1. Garey MR and Johnson DS (1979) Computers and Intractability: A Guide to the Theory of NP-
Completenes (Freeman, New York).

2. Kschischang FR, Frey BJ and Loeliger H-A (2001) IEEE Trans. Inform. Theory 47, 498-519.
3. Franco J and Paull M (1983) Discrete Appl. Math 5, 77-87.
4. Selman B, Kautz HA and Cohen B (1994) Proc. of AAAI-94, Seattle.
5. Achlioptas D and Sorkin GB (2000) Proc. of the Annual Symposium on the Foundations of Computer

Science, Redondo Beach, CA.

6. Me�zard M, Parisi G and Zecchina R (2002) Science 297, 812-815.
7. Bayati M, Shah D and Sharma M (2006) Proc. of International Symposium on Information Theory,

Seattle.

8. Gamarnik D and Bandyopadhyay A (2006) Proc. of the Symposium on Discrete Algorithms, Miami.
9. Montanari A and Shah D (2007) Proc. of the Symposium on Discrete Algorithms, New Orleans.

10. Friedgut E (1999) J. Amer. Math. Soc. 12, 1017-1054.
11. Mertens S, Me�zard M and Zecchina R (2006) Rand. Struct. and Alg. 28, 340-373.
12. Achlioptas D, Naor A and Peres Y (2005) Nature 435, 759-764.
13. Mulet R, Pagnani A, Weigt M and Zecchina R (2002) Phys. Rev. Lett. 89, 268701.
14. Krzakala F, Pagnani A and Weigt M (2004) Phys. Rev. E 70, 046705.
15. Luczak T (1991) Combinatorica 11, 45-54.
16. Me�zard M and Montanari A (2006) J. Stat. Phys. 124, 1317-1350.
17. Georgii HO (1988) Gibbs Measures and Phase Transitions (De Gruyter, Berlin).

18. Jonasson J (2002) Stat. and Prob. Lett. 57, 243-248.
19. Aurell E, Gordon U and Kirkpatrick S (2004) Proc. of Neural Information Processing Symposium,

Vancouver.
20. Maneva EN, Mossel E and Wainwright MJ (2005) Proc. of the Symposium on Discrete Algorithms,

Vancouver.
21. Biroli G, Monasson R and Weigt M (2000) Eur. Phys. J. B 14, 551-568.
22. Me�zard M, Mora T and Zecchina R (2005) Phys. Rev. Lett. 94, 197205.
23. Achlioptas D and Ricci-Tersenghi F (2006) Proc. of the Symposyum on the Theory of Computing,

Washington.
24. Braunstein A and Zecchina R (2004) J. Stat. Mech., P06007.
25. Me�zard M, Palassini M and Rivoire O (2005) Phys. Rev. Lett. 95, 200202.
26. Ruelle D (1987) Commun. Math. Phys. 108, 225-239.
27. Talagrand M (2000) C. R. Acad. Sci. Paris, Ser. I 331, 939-942.
28. Montanari A and Semerjian G, In preparation.
29. Kesten H and Stigum BP (1966) Ann. Math. Statist. 37, 1463-1481. Kesten H and Stigum BP (1966)

J. Math. Anal. Appl. 17, 309-338.
30. Montanari A and Ricci-Tersenghi F (2004) Phys. Rev. B 70, 134406.
31. Montanari A, Ricci-Tersenghi F and Semerjian G, In preparation.
32. Me�zard M and Parisi G (2001) Eur. Phys. J. B 20, 217-233.
33. Krzakala F and Zdeborova� L, In preparation.
34. Yedidia J, Freeman WT and Weiss Y (2005) IEEE Trans. Inform. Theory 51, 2282-2312.
35. Mossel E and Peres Y (2003) Ann. Appl. Probab. 13, 817-844.

6 www.pnas.org � � Footline Author


	Phase transitions in random CSP
	Results and discussion
	Dynamic phase transition and Gibbs measure extremality
	Condensation phase transition
	Algorithmic implications

	Cavity formalism, tree reconstruction and SP

