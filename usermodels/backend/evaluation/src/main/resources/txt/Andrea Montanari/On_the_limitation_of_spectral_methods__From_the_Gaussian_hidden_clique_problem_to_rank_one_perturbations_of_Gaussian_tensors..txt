
ar
X

iv
:1

41
1.

61
49

v1
  [

ma
th.

ST
]  

22
 N

ov
 20

14

On the limitation of spectral methods:

From the Gaussian hidden clique problem to rank one

perturbations of Gaussian tensors

Andrea Montanari? Daniel Reichman� Ofer Zeitouni�

November 18, 2014

Abstract

We consider the following detection problem: given a realization of a symmetric matrix X
of dimension n, distinguish between the hypothesis that all upper triangular variables are i.i.d.
Gaussians variables with mean 0 and variance 1 and the hypothesis where X is the sum of such
matrix and an independent rank-one perturbation.

This setup applies to the situation where under the alternative, there is a planted principal
submatrix B of size L for which all upper triangular variables are i.i.d. Gaussians with mean 1
and variance 1, whereas all other upper triangular elements of X not in B are i.i.d. Gaussians
variables with mean 0 and variance 1. We refer to this as the �Gaussian hidden clique problem.�

When L = (1 + ?)
?
n (? > 0), it is possible to solve this detection problem with probability

1 ? on(1) by computing the spectrum of X and considering the largest eigenvalue of X. We
prove that this condition is tight in the following sense: when L < (1? ?)?n no algorithm that
examines only the eigenvalues of X can detect the existence of a hidden Gaussian clique, with
error probability vanishing as n??.

We prove this result as an immediate consequence of a more general result on rank-one
perturbations of k-dimensional Gaussian tensors. In this context we establish a lower bound on
the critical signal-to-noise ratio below which a rank-one signal cannot be detected.

1 Introduction

Consider the following detection problem. One is given a symmetric matrix X = X(n) of dimension
n, such that the

(
n
2

)
+ n entries (Xi,j)i?j are mutually independent random variables. Given (a

realization of) X one would like to distinguish between the hypothesis that all random variables
Xi,j have the same distribution F0 to the hypothesis where there is a set U ? [n] so that all random
variables in the submatrix XU := (Xs,t : s, t ? U) have a distribution F1 which is different from
the distribution of all other elements in X which are still distributed as F0.

?Department of Electrical Engineering and Department of Statistics, Stanford University.
montanari@stanford.edu. Partially supported by the NSF grant CCF-1319979 and the grants AFOSR/DARPA
FA9550-12-1-0411 and FA9550-13-1-0036

�Computer Science department, Cornell University, Ithaca, NY, 14853. daniel.reichman@gmail.com. Work done
at the Weizmann Institute and supported in part by The Israel Science Foundation (grant No. 621/12)

�Faculty of Mathematics, Weizmann Institute, Rehovot 76100, Israel and Courant Institute, New York University.
ofer.zeitouni@weizmann.ac.il. Partially supported by a grant from the Israel Science Foundation and the Herman
P. Taubman chair of Mathematics at the Weizmann Institute.

1




The same problem was recently studied in [1, 11] and, for the of the asymmetric case (where
no symmetry assumption is imposed on the independent entries of X), in [7, 26, 28]. We refer
to Section 6 for further discussion of the related literature. An intriguing outcome of these works
is that, while the two hypothesis are statistically distinguishable as soon as L ? C log n (for C a
sufficiently large constant) [8], practical algorithms require significantly larger L. This motivates
the study of restricted classes of tests. In this paper we study the class of spectral (or eigenvalue-
based) tests detecting the signal. Our proof technique naturally allow to consider two further
generalizations of this problem that are of independent interests. We briefly summarize our results
below.

The Gaussian hidden clique problem. This is a special case of the above hypothesis testing
setting, whereby F0 = N(0, 1) and F1 = N(1, 1) (entries on the diagonal are defined slightly differ-
ently for simplifying calculations). Here and below N(m,?2) denote the Gaussian distribution of
mean m and variance ?2. Equivalently, let Z be a random matrix from the Gaussian Orthogonal
Ensemble (GOE) i.e. Zij ? N(0, 1/n) independently for i < j, and Zii ? N(0, 2/n). Then, under
hypothesis H1,L we have X = n

?1/21U1TU + Z (1U being the indicator vector on U , and |U | = L),
and under hypothesis H0, X = Z (the factor n in the normalization is for technical convenience).

We then consider the following restricted hypothesis testing question. Let ?1 ? ?2 ? � � � ? ?n be
the ordered eigenvalues of X. Is there a test that depends only on ?1, . . . , ?n and that distinguishes
H0 from H1,L �reliably,� i.e. with error probability converging to 0 as n ? ?? Notice that the
eigenvalues distribution does not depend on U as long as this is independent from the noise Z. We
can therefore think of U as fixed for this question.

If L ? (1 + ?)?n then [17] implies that a simple test checking whether ?1 ? 2 + ? for some
? = ?(?) > 0 is reliable. We prove that this result is tight, in the sense that no spectral test is
reliable for L ? (1? ?)?n.
Rank-one matrices in Gaussian noise. Our proof technique builds on a simple remark. Since
the noise Z is invariant under orthogonal transformations1, the above question is equivalent to the
following testing problem. For ? ? R?0, and v ? Rn, ?v?2 = 1 a uniformly random unit vector,
test H0: X = Z versus H1, X = ?vv

T +Z. (The correspondence between the two problems yields
? = L/

?
n.)

Again, this problem (and a closely related asymmetric version [32]) has been studied in the
literature, and it follows from [17] that a reliable test exists for ? ? 1 + ?. We provide a simple
proof (based on the second moment method) that no test is reliable for ? < 1 + ?.

Rank-one tensors in Gaussian noise. It turns our that the same proof applies to an even
more general problem: detecting a rank-one signal in a noisy tensor. We carry out our analysis
in this more general setting for two reasons. First, we think that this clarifies the what aspects of
the model are important for our proof technique to apply. Second, the problem estimating tensors
from noisy data has attracted significant interest recently within the machine learning community
[22, 31].

More precisely, we consider a noisy tensor X ??k Rn, of the form X = ? v?k + Z, where Z is
Gaussian noise, and v is a random unit vector. We consider the problem of testing this hypothesis
against H0: X = Z. We establish a threshold ?

2nd
k such that no test can be reliable for ? < ?

2nd
k

(in particular ?2nd2 = 1). We establish an analogous result for the asymmetric case as well. Two
differences are worth remarking for k ? 3 with respect to the more familiar matrix case k = 2.

1By this we mean that, for any orthogonal matrix R ? O(n), independent of Z, RZRT is distributed as Z.

2



First, we do not expect the second moment bound ?2ndk to be tight, i.e. a reliable test to exist for all
? > ?2ndk . On the other hand, we can show that it is tight up to a universal (k and n independent)
constant. Second, below ?2ndk the problem is more difficult than the matrix version below ?

2nd
2 = 1:

not only no reliable test exists but, asymptotically, any test behaves asymptotically as random
guessing.

2 Main result for spectral detection

Let Z be a GOE matrix as defined in the previous section. Equivalently if G is an (asymmetric)
matrix with i.i.d. entries Gi,j ? N(0, 1),

Z =
1?
2n

(
G+GT

)
. (1)

For a deterministic sequence of vectors v(n), ?v(n)?2 = 1, we consider the two hypotheses{
H0 : X = Z ,

H1,? : X = ?vv
T + Z .

(2)

A special example is provided by the Gaussian hidden clique problem in which case ? = L/
?
n and

v = 1U/
?
L for some set U ? [n], |U | = L,{

H0 : X = Z ,

H1,L : X =
1?
n
1U1

T

U + Z .
(3)

Observe that the distribution of eigenvalues of X, under either alternative, is invariant to the choice
of the vector v (or subset U), as long as the norm of v is kept fixed. Therefore, any successful
spectral algorithm will distinguish between H0 and H1,? but not give any information on the vector
v (or subset U , in the case of H1,L).

We let Q0 = Q0(n) (respectively, Q1 = Q1(n)) denote the distribution of the eigenvalues of X
under H0 (respectively H1 = H1,? or H1,L).

A spectral statistical test for distinguishing between H0 and H1 (or simply a spectral test) is
a measurable map Tn : (?1, . . . , ?n) 7? {0, 1}. To formulate precisely what we mean by the word
distinguish, we introduce the following notion.

Definition 1. For each n ? N, let P0,n, P1,n be two probability measures on the same measure space
(?n,Fn). We say that the sequence (P1,n) is contiguous with respect to (P0,n) if, for any sequence
of events An ? Fn,

lim
n??P0,n(An) = 0 ? limn??P1,n(An) = 0 . (4)

Note that contiguity is not in general a symmetric relation.
In the context of the spectral statistical tests described above, the sequences An in Definition

1 (with Pn = Q0(n) and Qn = Q1(n)) can be put in correspondence with spectral statistical tests
Tn by taking An = {(?1, . . . , ?n) : Tn(?1, . . . , ?n) = 0}. We will thus say that H1 is spectrally
contiguous with respect to H0 if Qn is contiguous with respect to Pn.

Our main result on the Gaussian hidden clique problem is the following.

3



Theorem 1. For any sequence L = L(n) satisfying lim supn??L(n)/
?
n < 1, the hypotheses H1,L

are spectrally contiguous with respect to H0.
Equivalently for any sequence of vectors v(n), and any ? = ?(n) satisfying lim supn?? ?(n) <

1, the hypotheses H1,? are spectrally contiguous with respect to H0.

Several remarks are in order with respect to Definition 1 and Theorem 1. First, we rule out
arbitrary spectral tests - not just tests that have polynomial running time (in n). Second, we do not
rule out the existence of a spectral test (or even a spectral test running in polynomial time) that
distinguishes between H0 and H1,L with total probability of error P0(T = 1) + P1(T = 0) ? 1? ?.
Indeed, as discussed in Section 4.1, it is quite easy to construct such a test.

Finally Theorem 1 provides an example of two family of distributions H0 and H1,L such that
the total variation distance between H0 and H1,L is 1? on(1) whereas H0 and H1,L are spectrally
contiguous.

2.1 Contiguity and integrability

Contiguity is related to a notion of uniform absolute continuity of measures. Recall that a prob-
ability measure � on a measure space is absolutely continuous with respect to another probability
measure ? if for every measurable set A, ?(A) = 0 implies that �(A) = 0, in which case there exists
a ?-integrable, non-negative function f ? d�d? (the Radon-Nikodym derivative of � with respect to
?), so that �(A) =

?
A f d? for every measurable set A. We then have the following known useful

fact, which will be the basis for proving contiguity, and whose proof is given for completeness.

Lemma 2. Within the setting of Definition 1, assume that P1,n is absolutely continuous with respect

to P0,n, and denote by ?n ? dP1,ndP0,n its Radon-Nikodym derivative.
(a) If lim supn?? E0,n(?2n) <?, then (P1,n) is contiguous with respect to (P0,n).
(b) If limn?? E0,n(?2n) = 1, then limn?? ?P0,n ? P1,n?TV = 0, where ? � ?TV denotes the total
variation distance, i.e.

?P0,n ? P1,n?TV ? sup
A
|P0,n(A)? P1,n(A)?.

Proof. (a) Let An be any sequence of events such that P0,n(An)? 0. Then

P1,n(An) = E0,n{?n1An} ? E0,n{?2n}1/2P0,n(An)1/2 ? 0 , (5)

where the last limit follows since E0,n{?2n} ? C for all n large enough.
(b) Note that E0,n?n = 1, whence

2?P0,n ? P1,n?TV = E0,n{|?n ? 1|} ? E0,n{(?n ? 1)2}1/2 =
?

E0,n(?2n)? 1 . (6)

2.2 Method and structure of the paper

Consider problem (2). We use the fact that the law of the eigenvalues under both H0 and H1,?
are invariant under conjugations by a orthogonal matrix. Once we conjugate matrices sampled

4



under the hypothesis H1,? by an independent orthogonal matrix sampled according to the Haar
distribution, we get a matrix distributed as

X = ?vvT + Z , (7)

where u is uniform on the n-dimensional sphere, and Z is a GOE matrix (with off-diagonal entries
of variance 1/n). Letting P1,n denote the law of ?uu

T + Z and P0,n denote the law of Z, we show
that P1,n is contiguous with respect to P0,n, which implies that the law of eigenvalues Q1(n) is
contiguous with respect to Q0(n).

To show the contiguity, we consider a more general setup, of independent interest, of Gaussian
tensors of order k, and in that setup show that the Radon-Nikodym derivative ?n,L =

dP1,n
dP0,n

is

uniformly square integrable under P0,n; an application of Lemma 2 then quickly yields Theorem 1.
The structure of the paper is as follows. In the next section, we define formally the detection

problem for a symmetric tensor of order k ? 2. We show the existence of a threshold under which
detection is not possible (Theorem 3), and show how Theorem 1 follows from this. Section 4 is
devoted to the proof of Theorem 3, and concludes with some additional remarks and consequences
of Theorem 3. Section 5 treats the case of asymmetric Gaussian tensors. Finally, Section 6 is
devoted to a description of the relation between the Gaussian hidden clique problem and hidden
clique problem in computer science, and related literature.

3 A symmetric tensor model and a reduction

Exploiting rotational invariance, we will reduce the spectral detection problem to a detection prob-
lem involving a standard detection problem between random matrices. Since the latter generalizes
to a tensor setup, we first introduce a general Gaussian hypothesis testing for k-tensors, which is of
independent interest. We then explain how the spectral detection problem reduces to the special
case of k = 2.

3.1 Preliminaries and notation

We use lower-case boldface for vectors (e.g. u, v, and so on) and upper-case boldface for matrices
and tensors (e.g. X,Z, and so on). The ordinary scalar product and ?p norm over vectors are
denoted by ?u,v? =?ni=1 uivi, and ?v?p. We write Sn?1 for the unit sphere in n dimensions

S
n?1 ? {x ? Rn : ?x?2 = 1} . (8)

Given X ??k Rn a real k-th order tensor, we let {Xi1,...,ik}i1,...,ik denote its coordinates. The
outer product of two tensors is X?Y, and, for v ? Rn, we define v?k = v ? � � � ? v ??k Rn as
the k-th outer power of v. We define the inner product of two tensors X,Y ??k Rn as

?X,Y? =
?

i1,��� ,ik?[n]
Xi1,��� ,ikYi1,��� ,ik . (9)

We define the Frobenius (Euclidean) norm of a tensor X by ?X?F =
??X,X?, and its operator

norm by

?X?op ? max{?X,u1 ? � � � ? uk? : ?i ? [k] , ?ui?2 ? 1}. (10)

5



It is easy to check that this is indeed a norm. For the special case k = 2, it reduces to the ordinary
?2 matrix operator norm (equivalently, to the largest singular value of X).

For a permutation ? ? Sk, we will denote by X? the tensor with permuted indices X?i1,��� ,ik =
X?(i1),��� ,?(ik). We call the tensor X symmetric if, for any permutation ? ? Sk, X? = X. It is
proved [33] that, for symmetric tensors, for symmetric tensors we have the equivalent representation

?X?op ? max{|?X,u?k?| : ?u?2 ? 1}. (11)
We define R ? R ?? with the usual conventions of arithmetic operations.

3.2 The symmetric tensor model and main result

We denote by G ??k Rn a tensor with independent and identically distributed entries Gi1,��� ,ik ?
N(0, 1) (note that this tensor is not symmetric).

We define the symmetric standard normal noise tensor Z ??k Rn by
Z =

1

k!

?
2

n

?
??Sk

G? . (12)

Note that the subset of entries with unequal indices form an i.i.d. collection {Zi1,i2,...,ik}i1<���<ik ?
N(0, 2/(n(k!))).

With this normalization, we have, for any symmetric tensor A ??k Rn
E
{
e?A,Z?

}
= exp

{ 1
n
?A?2F

}
. (13)

We will also use the fact that Z is invariant in distribution under conjugation by orthogonal trans-
formations, that is, that for any orthogonal matrix U ? O(n), {Zi1,...,ik} has the same distribution
as {?j1,...,jk (?k?=1 Ui?,j?) � Zj1,...,jk}.

Given a parameter ? ? R?0, we consider the following model for a random symmetric tensor
X:

X ? ? v?k + Z , (14)
with Z a standard normal tensor, and v uniformly distributed over the unit sphere Sn?1. In the
case k = 2 this is the standard rank-one deformation of a GOE matrix.

We let P? = P
(k)
? denote the law of X under model (14).

Theorem 3. For k ? 2, let

?2ndk ? inf
q?(0,1)

?
? 1
qk

log(1? q2) . (15)

Assume ? < ?2ndk . Then, for any k ? 3, we have
lim
n??

??P? ? P0??TV = 0 . (16)
Further, for k = 2 and ? < ?2ndk = 1, P? is contiguous with respect to P0.

The notation ?2ndk refers to the fact that this is the threshold for the second moment method to
work.

6



3.3 Reduction of spectral detection to the symmetric tensor model, k = 2, and

proof of Theorem 1

Recall that in the setup of Theorem 1, Q0,n is the law of the eigenvalues of X under H0 and Q1,n
is the law of the eigenvalues of X under H1,L. Then Q1,n is invariant by conjugation of orthogonal
matrices. Therefore, the detection problem is not changed if we replace X = n?1/21U1TU + Z by

X? ? RXRT = 1?
n
R1U (R1U )

T +RZRT , (17)

where R ? O(n) is an orthogonal matrix sampled according to the Haar measure. A direct calcu-
lations yields

X? = ?uuT + Z�, (18)

where u is uniform on the n dimensional sphere, ? = L/
?
n, and Z� is a GOE matrix (with off-

diagonal entries of variance 1/n). Furthermore, u and Z� are independent of one another.

Let P1,n be the law of X?. Note that P1,n = P
(k=2)
? with ? = L/

?
n. We can relate the detection

problem of H0 vs. H1,L to the detection problem of P0,n vs. P1,n as follows.

Lemma 4. (a) If P1,n is contiguous with respect to P0,n then H1,L is spectrally contiguous with
respect to H0.
(b) We have

?Q0,n ?Q1,n?TV ? ?P0,n ? P1,n?TV.
Proof. (a) Let F be the ?-algebra generated by all rotation-invariant events, and let G be the
?-algebra generated by the ordered eigenvalues. Then F = G.

Let An ? G be (spectral) events so that Q0,n(An) ?n?? 0. Note that (with some abuse
of notation) we have that Q0,n coincides with P0,n and Q1,n coincides with P1,n on G. Then
P1,n(An)?n?? 0 by assumption and therefore Q1,n(An) = P1,n(An)?n?? 0, proving the conti-
guity of Q1,n with respect to Q0,n.
(b) We have

sup
A
|P0,n(A)? P1,n(A)| ? sup

A?F
|P0,n(A)? P1,n(A)| = sup

A?G
|P0,n(A)? P1,n(A)|

= sup
A?B

|Q1,n(A)?Q0,n(A)|, (19)

where B is the Borel ?-algebra in ?n := {?1 ? ?2 . . . ? ?n}.
Remark: The first inequality in (19) (and hence the inequality in point (b) of the statement) is
actually an equality, but we will not use this fact.

In view of Lemma 4, Theorem 1 is an immediate consequence of Theorem 3.

4 Proof of Theorem 3

The proof uses the following large deviations lemma, which follows, for instance, from [13, Propo-
sition 2.3].

7



Lemma 5. Let v a uniformly random vector on the unit sphere Sn?1 and let ?v, e1? be its first
coordinate. Then, for any interval [a, b] with ?1 ? a < b ? 1

lim
n??

1

n
logP(?v, e1? ? [a, b]) = max

{1
2
log(1? q2) : q ? [a, b]

}
. (20)

Proof of Theorem 3. We denote by ? the Radon-Nikodym derivative of P? with respect to P0. By
definition E0? = 1. It is easy to derive the following formula

? =

?
exp

{
? n?

2

4
+
n?

2
?X,v?k?

}
�n(dv) . (21)

where �n is the uniform measure on S
n?1. Squaring and using (13), we get

E0?
2 = e?n?

2/2

?
E0 exp

{n?
2
?X,v1?k + v2?k?

}
�n(dv1)�n(dv2)

= e?n?
2/2

?
exp

{n?2
4

??v1?k + v2?k??2F}�n(dv1)�n(dv2)
=

?
exp

{n?2
2
?v1,v2?k

}
�n(dv1)�n(dv2)

=

?
exp

{n?2
2
?v, e1?k

}
�n(dv) , (22)

where in the first step we used (13) and in the last step, we used rotational invariance.
Let F? : [?1, 1]? R be defined by

F?(q) ? ?
2qk

2
+
1

2
log(1? q2) . (23)

Using Lemma 5, for any ?1 ? a < b ? 1,?
exp

{n?2
2
?v, e1?k

}
I(?v, e1? ? [a, b])�n(dv) = exp

{
n max
q?[a,b]

F?(q) + o(n)
}
. (24)

It follows from the definition of ?2ndk that max|q|??F?(q) < 0 for any ? > 0. Hence

E0?
2 ?

?
exp

{n?2
2
?v, e1?k

}
I(|?v, e1?| ? ?)�n(dv) + e?c(?)n , (25)

for some c(?) > 0 and all n large enough. Next notice that, under �n, ?v, e1? d= G/(G2 + Zn?1)1/2
where G ? N(0, 1) and Zn?1 is a ?2 with n? 1 degrees of freedom independent of G. Then, letting
Zn ? G2 + Zn?1 (a ?2 with n degrees of freedom)

E0?
2 ? E

{
exp

(n?2
2

|G|k
Z
k/2
n

)
I(|G/Z1/2n | ? ?)

}
+ e?c(?)n

? E
{
exp

(n?2
2

|G|k
Z
k/2
n

)
I(|G/Z1/2n | ? ?) I(Zn?1 ? n(1? ?))

}
+ en?

2?k/2
P
{
Zn?1 ? n(1? ?)

}
+ e?c(?)n

? E
{
exp

( n1?(k/2)?2
2(1? ?)k/2 |G|

k
)
I(|G|2 ? 2?n)

}
+ en?

2?k/2
P
{
Zn?1 ? n(1? ?)

}
+ e?c(?)n

=
2?
2?

? 2?n
0

eC(?,?)n
1?k/2xk?x2/2dx+ en?

2?k/2
P
{
Zn?1 ? n(1? ?)

}
+ e?c(?)n , (26)

8



where C(?, ?) = ?2/(2(1 ? ?)k/2). Now, for any ? > 0, we can (and will) choose ? small enough
so that both en?

2?k/2
P
{
Zn?1 ? n(1 ? ?)

} ? 0 exponentially fast (by tail bounds on ?2 random
variables) and, if k ? 3, the argument of the exponent in the integral in the right hand side of (26)
is bounded above by ?x2/4, which is possible since the argument vanishes at x? = 2C(?, ?)n1/2.
Hence, for any ? > 0, and all n large enough, we have

E0?
2 ? 2?

2?

? 2?n
0

eC(?,?)n
1?k/2xk?x2/2dx+ e?c(?)n , (27)

for some c(?) > 0.
Now, for k ? 3 the integrand in (27) is dominated by e?x2/4 and converges pointwise (as n??)

to 1. Therefore, since E0?
2 ? (E0?)2 = 1,

k ? 3 : lim
n??E0?

2 = 1 . (28)

For k = 2, the argument is independent of n and can be integrated immediately, yielding (after
taking the limit ? ? 0)

k = 2 : lim sup
n??

E0?
2 ? 1?

1? ?2
. (29)

(Indeed, the above calculation implies that the limit exists and is given by the right-hand side.)
The proof is completed by invoking Lemma 2.

4.1 Some remarks and consequences

Threshold values. In the table below we report the numerical values of ?2ndk for a few values of
k. The exact value ?2ndk = 1 for the matrix case k = 2 follows from log(1? q2) ? ?q2.

k ?2ndk
2 1
3 1.398841
4 1.566974
5 1.67676
6 1.757589
10 1.955118
100 2.595874

Also, it is not difficult to derive the asymptotics ?2ndk =
?
log(k/2) + ok(1) for large k.

Tightness of the threshold values. As mentioned in the introduction, for k = 2 and ? > 1, it
is known that the largest eigenvalue of X, ?1(X) converges almost surely to (? + 1/?) [17]. As a
consequence ?P0 ? P??TV ? 1 for all ? > 1: the second moment bound is tight.

For k ? 3, it follows by the triangular inequality that ?X?op ? ? ? ?Z?op, and further
lim supn?? ?Z?op ? �k almost surely as n ? ? [27, 6] for some bounded �k. It follows that
?P0 ? P??TV ? 1 for all ? > 2�k [31]. Hence, the second moment bound is off by a k-dependent
factor. For large k, 2�k =

?
2 log k +Ok(1) and hence the factor is indeed bounded in k.

9



Behavior below the threshold. Let us stress an important qualitative difference between k = 2
and k ? 3, for ? < ?2ndk . For k ? 3, the two models are indistinguishable and any test is essentially
as good as random guessing. Formally, for any measurable function T : ?kRn ? {0, 1}, we have

lim
n??

[
P0(T (X) = 1) + P?(T (X) = 0)

]
= 1 . (30)

For k = 2, our result implies that, for ? < 1, ?P0 ? P??TV is bounded away from 1. On the other
hand, it is easy to see that it is bounded away from 0 as well, i.e.

0 < lim inf
n?? ?P0 ? P??TV ? lim supn?? ?P0 ? P??TV < 1 . (31)

Indeed, consider for instance the statistics S = Tr(X). Under P0, S ? N(0, 2), while under P?,
S ? N(?, 2). Hence

lim inf
n?? ?P0 ? P??TV ? ?N(0, 1) ? N(?/

?
2, 1)?TV = 1? 2?

(
? ?
2
?
2

)
> 0 (32)

(Here ?(x) =
? x
?? e

?z2/2dz/
?
2? is the Gaussian distribution function.) The same phenomenon

for rectangular matrices (k = 2) is discussed in detail in [32].

5 Asymmetric tensor model

As before, we denote by G ??k Rn a tensor with independent and identically distributed entries
Gi1,��� ,ik ? N(0, 1). We define the asymmetric standard normal noise tensor Z ?

?k
R
n by

Z =

?
1

n
G . (33)

In particular, all entries are i.i.d. Zi1,...,ik ? N(0, 1/n). With this normalization, we have of course

E
{
e?A,Z?

}
= exp

{ 1
2n
?A?2F

}
. (34)

Given ? ? R?0, we consider observations X ?
?k

R
n given by:

X ? ?v1 ? v2 ? � � � ? vk + Z , (35)
with Z a an asymmetric standard normal tensor, and v1,. . . , vk independent and uniformly dis-
tributed over the unit sphere Sn?1. In the case k = 2 we recover, again, the classical spiked model.
Note that a further layer of generalization would be obtained by considering a �rectangular� tensor
X ? Rn1?� � �?Rnk . We prefer to assume n1 = � � � = nk to limit inessential technical complications.

We denote by P? = P
(k)
? the law of X under the model (35)

Theorem 6. For k ? 2, let ?2ndk ? (k/2)1/2?2ndk , i.e.

?2ndk ? inf
q?(0,1)

?
? k
2qk

log(1? q2) . (36)

Assume ? < ?2ndk . Then, for any k ? 3, we have
lim
n?? ?P? ? P0?TV = 0 . (37)

Further, for ? < ?2nd2 = 1, P? is contiguous with respect to P0.

10



Proof. The proof is very similar to the one of Theorem 3. We will therefore limit ourselves to
outline the main steps, and the differences with respect to the symmetric case. First we compute
the Radon-Nikodym derivative of P? with respect to P0:

? =

?
exp

{
? n?

2

2
+ n? ?X,v1 ? � � � ? vk?

}
�?kn (dv) . (38)

Here we introduced the notation �?kn (dv) = �n(dv1) � � � �n(dvk) Proceeding as in the proof of
Eq. (22), we get

E0?
2 =

?
exp

{
n?2

k?
i=1

?vi, e1?k
}
�?kn (dv) . (39)

Now, let G? : [?1, 1]k ? R be defined by

G?(q1, q2, . . . , qk) = ?
2

k?
i=1

qi +
1

2

k?
i=1

log(1? q2i ) . (40)

Invoking again Lemma 5, we obtain that for any open set J ? [?1, 1]k?
exp

{
n?2

k?
i=1

?vi, e1?k
}
I

(
(?v1, e1?, . . . , ?vk, e1?) ? J

)
�?kn (dv) = exp

{
nmax

q?J
G?(q) + o(n)

}
.

(41)

The key observation is that, for ? < ?2ndk , we have maxq?[?1,1]k G?(q) = 0, with the maximum
being uniquely achieved at q = 0. Once this claim is proved, we can restrict the integral (39) to
a neighborhood of (?v1, e1?, � � � , ?vk, e1?) = 0 and obtain by an argument completely analogous to
the symmetric case

k ? 3? lim
n??E0?

2 = 1 . k = 2? lim sup
n??

E0?
2 ? 1?

1? ?4 .

To prove the above claim, and hence complete the proof, note that: q = 0 is a local maximum
of G?(q); G?(|q1|, . . . , |qk|) ? G?(q1, . . . , qk); G?(q)? ?? if ?q?? ? 1. It is therefore sufficient to
prove that any other local maximum q? ? [0, 1)k \ {0} has G?(q?) < 0. The stationarity condition
of G? reads

?2
?

j?[k]\i
qj =

qi
1? q2i

, ?i ? [k] , (42)

whence (after multiplying by qi)

q21
1? q21

=
q22

1? q22
= � � � = q

2
k

1? q2k
. (43)

since x 7? x2/(1 ? x2) is strictly monotone increasing on (0, 1], we deduce that q1 = q2 = � � � = qk.
It is therefore sufficient to check G?(q, q, . . . , q) < 0 for all q ? (0, 1). This is guaranteed by
? < ?2ndk .

11



6 Related work

Detection problems with similar flavor to the Gaussian hidden clique problem have been studied
over the years in several fields including computer science, physics and statistics. Typically, in such
problems there is �planted� object with special properties along with random noise which makes
the detection of the planted object a nontrivial task. In the classical G(n, 1/2) planted clique
problem, the computational problem is to find the planted clique (of cardinality k) efficiently (e.g.,
in polynomial time) where we assume the location of the planted clique is hidden and is not part
of the input. There are several algorithms that recover the planted clique in polynomial time when
k = C

?
n where C > 0 is a constant independent of n [2, 12, 15, 16]. In [11] it is proven that a

planted clique can be recovered in time O(n2 log(n)), whenever C = e?1/2 + ?. The work of [2]
demonstrates that it is possible to find a planted clique of size c

?
n in time nO(log(1/c)). Despite

significant effort, no polynomial time algorithm for this problem is known when k = o(
?
n). In the

decision version of the planted clique problem, one seeks an efficient algorithm that distinguishes
between a random graph distributed as G(n, 1/2) or a random graph containing a planted clique
of size k ? (2 + ?) log n (for ? > 0; the natural threshold for the problem is the size of the largest
clique in a random sample of G(n, 1/2), which is asymptotic to 2 log n [20]). No polynomial time
algorithm is known for this decision problem if k = o(

?
n). There are several hardness results for

computational problems in game theory [30] (see also [21]) and statistics [9] which are based on
the alleged hardness of the the problem of distinguishing between a random graph distributed as
G(n, 1/2) to a random graph with a planted clique of size k(n) (with (2 + ?) log n < k(n)? ?n).

As another example, consider the following setting introduced by [5] (see also [1]): one is given
a realization of a n-dimensional Gaussian vector x := (x1, ..,xn) with i.i.d. entries. The goal
is to distinguish between the following two hypotheses. Under the first hypothesis, all entries
in x are i.i.d. standard normals. Under the second hypothesis, one is given a family of subsets
C := {S1, ..., Sm} such that for every 1 ? k ? m,Sk ? {1, ..., n} and there exists an i ? {1, . . . ,m}
such that, for any ? ? Si, x? is a Gaussian random variable with mean � > 0 and unit variance
whereas for every ? /? Si, x? is standard normal. (The second hypothesis does not specify the
index i, only its existence). The main question is how large � must be such that one can reliably
distinguish between these two hypotheses. In [5], one considers two situations. In the first, ? are
vertices in a two dimensional grid of side length n and the family C is the set of all directed (i.e.,
with north or east steps only) paths of length l, starting from the bottom-left corner. In the second
situation treated in [5], the ?s correspond to the vertices of a binary tree, and again the family
C consists of loopless paths starting at the root. In [5], both the min-max and Bayesian (with
uniform choice of i in C) setups are considered. Other choices of C are considered in [1]: the family
of all subsets of size k, the set of all perfect matching in a given graph and other examples. These
detection problems have practical applications-see [5] for details.

The Gaussian hidden clique problem is related to various applications in statistics and compu-
tational biology [7, 26]. That detection is statistically possible when L? log n was established in
[1] (the authors consider the case where all diagonal elements are zero, but since the detection algo-
rithm can simply ignore the diagonal elements, their results apply to our setting as well). Similar
results for the asymmetric case were obtained by [28]. In terms of polynomial time detection, [11]
show that detection is possible when L = ?(

?
n) for the symmetric cases. As noted, no polynomial

time algorithm is known for the Gaussian hidden clique problem when k = o(
?
n). In [1] it was

hypothesized that the Gaussian hidden clique problem should be difficult when L ? ?n. More
specifically [1, Pg. 16, second paragraph] comment that �it seems likely that designing an efficient

12



test in the normal setting will prove as difficult as it proved for planted cliques�. Supporting ev-
idence for this assertion was provided in [28], who proved that distinguishing between a planted
model similar to the Gaussian planted clique model studied in our work and the random case (with
entries being independent standard Gaussians) is at least as hard as distinguishing between a graph
containing a planted clique and a graph distributed the random graph G(n, 1/2).

There is a large body of work in RMT that addresses the effect of low rank perturbations on
various properties the spectrum and eigenvectors of Wigner matrices (e.g., [17, 18, 25]), mostly in
studying the almost sure limits and limit distributions of extremal eigenvalues and eigenvectors.

The closest results to ours are the ones of [32]. In the language of the present paper, these
authors consider a rectangular matrix of the form X = ?v1v

T
2 + Z ? Rn1�n2 whereby Z has

i.i.d. entries Zij ? N(0, 1/n1), v1 is deterministic of unit norm, and v2 has entries which are i.i.d.
N(0, 1/n1), independent of Z. They consider the problem of testing this distribution against ? = 0.
Setting c = limn?? n1n2 , it is proved in [32] that the distribution of the singular values of X under
the null and the alternative are mutually contiguous if ? <

?
c and not mutually contiguous if

? >
?
c. This (almost) correspond to the asymmetric model of Section 5, for the matrix case k = 2.

While [32] derive some more refined results, their proofs rely on advanced tools from random matrix
theory [19], while our proof is simpler, and generalizable to other settings (e.g. tensors).

7 Conclusion

In this work we considered detection problems for GOE matrices perturbed by a deterministic
rank 1 matrix, including the Gaussian hidden clique problem. We have established that spectral
methods stop being effective when the norm of the perturbation drops below a threshold, which
translates in the Gaussian hidden clique problem to the size of the planted submatrix being smaller
than (1??)?n. In identifying this threshold we have also addressed detectability issues in rank-one
perturbations of matrices and tensors which might be of independent interest.

There are several open problems that arise from the current work. First, in the context of the
Gaussian hidden clique problem, it would be interesting to provide an efficient algorithm for finding
a planted submatrix when L = o(

?
n) or rule out certain algorithmic (non spectral) approaches for

this problem. One direction is to study optimization methods such as semidefinite programming
and various types of hierarchies (e.g., Lasserre, Sherali-Adams) in dealing with the hidden clique
problem for L = o(

?
n).

A natural question is whether one can use the spectrum in order to distinguish between a graph
distributed as G(n, 1/2) and a random graph with a planted clique of size L < (1? ?)?n. Proving
that one can or cannot distinguish between these cases using eigenvalues is a challenging open
problem.

Finally, it might be interesting to study the limitations of spectral techniques for other problems
such as coloring [3] and satisfiability [14].

Acknowledgments

We thank Iain Johnstone for bringing [32] to our attention.

13



References

[1] L. Addario-Berry, N. Broutin, L. Devroye, G. Lugosi. On combinatorial testing problems.
Annals of Statistics 38(5) (2011), 3063�3092.

[2] N. Alon, M. Krivelevich and B. Sudakov. Finding a large hidden clique in a random graph.
Random Structures and Algorithms 13 (1998), 457�466.

[3] N. Alon and N. Kahale, A spectral technique for coloring random 3-colorable graphs SIAM
Journal on Computing 26 (1997), 1733�1748.

[4] G. W. Anderson, A. Guionnet and O. Zeitouni. An introduction to random matrices. Cam-
bridge University Press (2010).

[5] E. Arias-Castro, E. J., Cande`s, H. Helgason and O. Zeitouni. Searching for a trail of evidence
in a maze. Annals of Statistics 36 (2008), 1726�1757.

[6] A. Auffinger, G. Ben Arous, and J. Cerny. Random matrices and complexity of spin glasses.
Communications on Pure and Applied Mathematics 66(2) (2013), 165�201.

[7] S. Balakrishnan, M. Kolar, A. Rinaldo, A. Singh, and L. Wasserman. Statistical and compu-
tational tradeoffs in biclustering. NIPS Workshop on Computational Trade-offs in Statistical
Learning (2011).

[8] S. Bhamidi, P.S. Dey, and A.B. Nobel. Energy landscape for large average submatrix detection
problems in Gaussian random matrices. arXiv:1211.2284.

[9] Q. Berthet and P. Rigollet. Complexity theoretic lower bounds for sparse principal component
detection. COLT (2013), 1�21.

[10] R. V. Bopanna. Eigenvalues and graph bisection: An average-case analysis In FOCS (1985),
280�285.

[11] Y. Deshpande and A. Montanari. Finding hidden cliques of size
?
N/e in nearly linear time.

Foundations of Computational Mathematics (2014), 1�60

[12] Y. Dekel, O. Gurel-Gurevich, and Y. Peres. Finding hidden cliques in linear time with high
probability. Combinatoircs Probability and Computing 23(1) (2014), 29�49.

[13] A. Dembo and O. Zeitouni. Matrix optimization under random external fields. arXiv:1409.4606

[14] A. Flaxman. A spectral technique for random satisfiable 3CNF formulas. In SODA (2003),
357�363.

[15] U. Feige and R. Krauthgamer. Finding and certifying a large hidden clique in a semi-random
graph. Random Struct. Algorithms 162(2) (1999), 195�208.

[16] U. Feige and D. Ron. Finding hidden cliques in linear time. DMTCS Proceedings (2010),
189�204.

[17] D. Fe�ral and S. Pe�che�. The largest eigenvalue of rank one deformation of large Wigner matrices.
Comm. Math. Phys. 272 (2007), 185�228.

14





[18] Z. Fu�redi and J. Komlo�s, The eigenvalues of random symmetric matrices. Combinatorica 1
(1981), 233�241.

[19] A. Guionnet and M. Maida. A Fourier view onR-transform and related asymptotics of spherical
integrals. Journal of Functional Analysis 222 (2005), 435�490.

[20] G. R. Grimmett and C. J. H. McDiarmid. On colouring random graphs. Math. proc. Cambridge
Philos. Soc. 77 (1975), 313�324.

[21] E. Hazan and R. Krauthgamer. How hard is it to approximate the best nash equilibrium?
SIAM Journal on Computing 40(1) (2011), 79�91.

[22] D. Hsu, S. M. Kakade, and T. Zhang. A spectral algorithm for learning hidden Markov models.
Journal of Computer and System Sciences 78.5 (2012): 1460-1480.

[23] M. Jerrum. Large cliques elude the Metropolis process. Random Struct. Algorithms 3(4)
(1992), 347�360.

[24] R. Kannan and S. Vempala. Spectral Algorithms. Foundations and Trends in Theoretical
Computer Science 4 (2008), 132�288.

[25] A. Knowles and J. Yin, The isotropic semicircle law and deformation of Wigner matrices.
Communications on Pure and Applied Mathematics 66(11) (2013), 1663�1749.

[26] M. Kolar, S. Balakrishnan, A. Rinaldo, and A. Singh. Minimax localization of structural
information in large noisy matrices. In Advances in NIPS (2011), 909�917.

[27] M. Talagrand. Free energy of the spherical mean field model. Probability theory and related
fields 134(3) (2006), 339�382.

[28] Z Ma and Y Wu. Computational barriers in minimax submatrix detection. arXiv:1309.5914.

[29] F. McSherry. Spectral partitioning of random graphs. In FOCS (2001), 529�537.

[30] L. Minder and D. Vilenchik. Small clique detection and approximate Nash equilibria. In
RANDOM (2009).

[31] A. Montanari and E. Richard. A Statistical Model for Tensor PCA. Neural Information
Processing Systems (NIPS), 2014.

[32] A. Onatski, M. J. Moreira, M. Hallin, et al. Asymptotic power of sphericity tests for high-
dimensional data. The Annals of Statistics 41(3) (2013), 1204�1231.

[33] W. C. Waterhouse. The absolute-value estimate for symmetric multilinear forms. Linear
Algebra and its Applications 128 (1990), 97�105.

15



	1 Introduction
	2 Main result for spectral detection
	2.1 Contiguity and integrability
	2.2 Method and structure of the paper

	3 A symmetric tensor model and a reduction
	3.1 Preliminaries and notation
	3.2 The symmetric tensor model and main result
	3.3 Reduction of spectral detection to the symmetric tensor model, k=2, and proof of Theorem ??

	4 Proof of Theorem ??
	4.1 Some remarks and consequences

	5 Asymmetric tensor model
	6 Related work
	7 Conclusion

