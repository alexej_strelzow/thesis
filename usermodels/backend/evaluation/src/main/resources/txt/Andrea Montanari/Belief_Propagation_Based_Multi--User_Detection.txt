
ar
X

iv
:c

s/0
51

00
44

v2
  [

cs
.IT

]  
22

 M
ay

 20
06

Belief Propagation Based Multi�User Detection

Andrea Montanari

Laboratoire de Physique The�orique

Ecole Normale Supe�rieure

75005 Paris, FRANCE

montanar@lpt.ens.fr

Balaji Prabhakar

Dept. of Electrical Engineering

Stanford University

Stanford, CA 94305

balaji@stanford.edu

David Tse

Dept. of Electrical Engineering and Computer Sciences

University of California, Berkeley

dtse@eecs.berkeley.edu

Abstract

We apply belief propagation (BP) to multi�user detection in a spread spectrum
system, under the assumption of Gaussian symbols. We prove that BP is both
convergent and allows to estimate the correct conditional expectation of the input
symbols. It is therefore an optimal �minimum mean square error� detection algo-
rithm. This suggests the possibility of designing BP detection algorithms for more
general systems.

As a byproduct we rederive the Tse-Hanly formula for minimum mean square
error without any recourse to random matrix theory.

1 Introduction

Consider the multiuser detection problem of K users, each spreading its symbol
onto N chips in a spread spectrum system

y =

K?
i=1

xisi + w . (1)

Here xi ? R is the symbol transmitted by user i, si ? RN is the N -chip long
signature sequence of user i, y is the received vector, and w is an N � 1 vector
of i.i.d. mean 0 and variance ?2 Gaussians. We assume that the xi are i.i.d. with
Exi = 0 and Ex

2
i = 1. Finally, we shall denote by S the N �K signature matrix

whose columns are the vectors si.
Several years ago, Tse and Hanly [1], and Verdu� and Shamai [2] considered the

case in which the symbols xi�s are Gaussian random variables, which models the
situation when they are ideal Shannon-coded symbols. The signature sequences
were assumed themselves random, but known both at the receiver. More precisely
(here and in the following A� denotes the transpose of matrix, or vector, A)

si =
1?
N
(si1, ..., siN )

T , for i = 1, ...,K, (2)




1
2

3

K

a

b

c

z

K users N chips

s1a
s1b

s1c

s1z

1

2

3

K

a

b

c

z

?1?a
?1?a

??a?1?
?a?1

L1
G1

Figure 1: Left: A graphical representation of the multiuser communication problem.
Right: Messages notation in the BP detection algorithm.

where the sia�s are i.i.d. random variables with zero mean and unit variance. These
authors considered a minimum mean square error (MMSE) single user receiver
which generates soft estimates x�i�s and analyzed its performance in the large system
limit N,K ? ?, with ? ? K/N constant. The analysis was heavily based on
random matrix theory and, in particular, on the Marcenko-Pastur theorem on
the eigenvalue distribution of large random matrices. As a consequence, it did
not suggest any generalization to the practically interesting case of non-Gaussian
symbols xi�s.

Recently, Tanaka [3] applied the replica method from statistical mechanics to
the case of uncoded binary antipodal signals: xi ? {+1,?1}. He was able to
compute the asymptotic bit error rate and conditional entropy per bit, in the large
system limit. Given the relationship between the replica and cavity method from
statistical mechanics and message passing algorithms, such as belief propagation
(BP) and various generalizations thereof, several authors [4, 5] studied the use of
BP as a detection algorithm in this context. If proved to be correct, this approach
would provide a low complexity, asymptotically optimal receiver.

Although the results obtained via replica method are likely to be correct1, the
method itself is non-rigorous. Also, the studies of BP detection algorithm were
essentially empirical. No convergence or correctness guarantees exist.

In this paper we follow the opposite route to the one sketched above. We
consider BP detection and rigorously prove convergence and correctness. In a
second step, we analyze BP to compute the system performance. This approach
solves both the problems stressed above (non rigorous character of the replica
method, and lack of guarantees on BP detection). This paper deals with the case
of Gaussian symbols xi�s, with zero mean and unit variance. Following our strategy,
we are able to recover several of Tse and Hanly�s results without any recourse to
random matrix theory. We conjecture however that the same approach can be
developed for non-Gaussians symbols allowing to recover Tanaka�s results without
any recourse to the replica method.

In order to apply BP, it is convenient to formulate multiuser detection as an
inference problem on a probabilistic graphical model. The underlying graph is
depicted in Fig. 1. It is a complete bipartite graph on K left nodes (users) and N
right nodes (chips). We associate real variables x ? (x1, . . . , xK) to the user nodes

1Tanaka�s results are derived under the �replica symmetry� assumption. There are arguments sup-
porting this assumption in the problem as described so far. On the other hand, the same hypothesis
needs to be replaced by more refined ones for generalizations of this problem (for instance if the noise
variance is not known at the receiver).



and ? ? (?1, . . . , ?N ) to the chips, and consider the (complex) weight

d�N,Ky (x, ?) =
1

ZN,Ky

N?
a=1

e?
1

2
?2?2

a
+jya?a

K?
i=1

d?(xi)
?
i,a

exp

{
? j?

N
sai?axi

}
d?. (3)

where d?(xi) = exp(?12x2i )/2pi is the a priori distribution of the symbol Xi, and
j =

??1. Elementary calculus shows that d�N,Ky (x) ?
?
? d�

N,K
y (x, ?) is in fact

the conditional distribution of the transmitted symbols x given y. The detection
problem amounts to compute the marginal d�N,Ky (xi), i.e. to integrate Eq. (3)

over all the variables but xi. Since, the weight (3) is Gaussian, d�
N,K
y (xi) will be

Gaussian as well and can be parameterized as

d�N,Ky (xi) =

?
Li
2pi

exp

{
?1
2
Lix

2
i +Gixi

}
dx , (4)

The weight (3) factorizes according to the complete bipartite graph in Fig. 1, left
frame. BP can therefore be used to computing the marginals d�N,Ky (xi) through a
message passing procedure. Messages are exchanged as in Fig. 1, right frame, and
computed according to the update equations

?
(t+1)
i?a = 1 +

1

N

?
b6=a

s2ib

??
(t)
b?i

, ??
(t)
a?i = ?

2 +
1

N

?
k 6=i

s2ka

?
(t)
k?a

(5)

?
(t+1)
i?a =

1?
N

?
b6=a

sib

??
(t)
b?i

??
(t)
b?i , ??

(t)
a?i = ya ?

1?
N

?
k 6=i

ska

?
(t)
k?a

?
(t)
k?a . (6)

with initial conditions ?
(0)
i?a = 1, ?

(0)
i?a = 0. The parameters in the marginal

distribution d�N,Ky (xi), cf. Eq. (4), are estimated as

G
(t+1)
i =

1?
N

?
b?[N ]

sib

??
(t)
b?i

??
(t)
b?i , and L

(t+1)
i = 1 +

1

N

?
b?[N ]

s2ib

??
(t)
b?i

. (7)

In the next Section we will show that this procedure is convergent and provides
the asymptotically correct marginal distributions, thus implementing allowing to
implement a MMSE estimator. In Section 3 we show that the usual prescription
for a MMSE receiver can be recovered from the fixed point of BP. Finally, in
Section 4, we investigate the rate of convergence of our algorithm through numerical
simulations.

2 Main result: convergence and correctness

Throughout this Section we assume that sia = �1 with equal probability. We
believe that our main results, as well as the idea of the proof, remain valid for a
considerably more general distribution. However, this assumption allows to avoid
several technical complications. In particular, it is immediate to prove the following
result

Lemma 1 If sia ? {+1,?1} for any i ? [K], a ? [N ], then ?(t)i?a = ?(t), ??(t)a?i =
??(t), where ?(t), ??(t) are given by the iterations

?(t+ 1) = 1 +
N ? 1
N

1

??(t)
, ??(t) = ?2 +

K ? 1
N

1

?(t)
, (8)



with ?(0) = 1.
Moreover ?(t)? ?(?), ??(t)? ??(?) as t??, with ?(?), ??(?) > 0 the unique

positive fixed point of the above equations. More precisely, we have ?(?) = 1 + ?,
??(?) = 1/?, where

1

?
= ?2 +

?

1 + ?
. (9)

The reader will recognize that Eq. (9) is nothing but the Tse-Hanly asymptotic
formula for the signal to interference ratio in the large system limit.

We shall state two separate results for the mean of the marginal distribution
(4), x?i = Gi/Li and its variance 1/Li.

Theorem 1 Assume the sia�s are i.i.d. uniformly random in {+1,?1}. Then,
there exists a set SN ? {+1,?1}K�N of signatures with P(SN ) ? 1 ? O(N?a)
(with a an arbitrary positive number) such that, for any S ? SN and for every
{ya}, L(t)i ? L(?)i and G(t)i ? G(?)i as t ? ?. Furthermore, the conditional
means estimated by Belief Propagation are correct, i.e., x?

(?)
i = G

(?)
i /L

(?)
i = x?i.

Sketch of proof: For Gaussian graphical models, it was proved in [7, 8] that,
if BP converges for a �generic initial condition�, then it correctly estimates the
mean of the associated probability distribution. Referring to the present case, this

immediately implies x?
(?)
i = x?i if convergence holds for any initial condition of the

form ?
(0)
i?a = ci?a, ?

(0)
i?a = 1.

Because of Lemma 1, it is in fact sufficient to prove convergence for the messages

{?(t)i?a}. We consider therefore the homogeneous recursion

?
(t+1)
i?a =

1?
N??(t)

?
b6=a

sib??
(t)
b?i , ??

(t)
a?i = ?

1?
N?(t)

?
k 6=i

ska ?
(t)
k?a . (10)

We shall show that for a typical realization of the signatures S, ?
(t)
i?a and ??

(t)
a?i ? 0

for any initial condition ?
(0)
i?a and ??

(0)
a?i. This in turns imply that ?

(t)
i?a and ??

(t)
a?i

converge. We begin by eliminating ??
(t)
a?i from the above relations to get

?
(t+1)
i?a = ?

1

N?(t)??(t)

?
k?b

?ia,kb?
(t)
k?b (11)

where

?ia,kb =

{
0 if i = k or a = b,
sibskb otherwise.

(12)

The random matrix ? = {?ia,kb : i, k ? [K], a, b ? [N ]} has dimensions NK �NK
and is a function of the signature sequence S.

We make use of the following property of ?, whose proof is omitted.

Lemma 2 For each ? > 0, and each ? > 1 there exist a positive integer N0 =
N0(?, ?) such that, if N > N0 and t ? (N/1000)1/6 then

ETr
{
(?t)�?t

}
? N2t+2?t+1

(
1 +

C(?, ?)

N
t6 ?t

)
, (13)

where C(?, ?) is N� and t�independent.



Let ?max be the eigenvalue of ? with the largest absolute value, and denote by
SN (?) ? {+1,?1}NK the set of signature sequences, such that |?max| < N??(?)??(?).
Applying Markov inequality to the random variable |?max|2t ? Tr

{
(?t)�?t

}
, it is

easy to show that

P

{
SN (?)

}
? N2?

( ?
?

??(?)??(?)

)2t(
1 +

C(?, ?)

N
t6 ?t

)
. (14)

By properly choosing ? and t (and setting SN = SN (?)), this in turns imply
P
{SN} ? O(N?a) with a > 0.
We must now prove that, for any signature sequence in SN , ?(t) ? [?(t)i?a]? 0.

Since ?(s)? ?(?), ??(s)? ??(?), there exists s?, such that |?max| < N??(s?)??(s?),
with ? < 1. Defining ?? = ??/N?(s?)??(s?), we have

?(t) =

t?1?
s=0

[
?1

N?(s)??(s)
?

]
?(0) =

{
t?1?
s=s?

?(s?)??(s?)

?(s)??(s)

}
??t?s

?

?(s?) . (15)

Since the largest eigenvalue of ?? has modulus smaller than one, ??t?s
?

?(s?) ? 0.
Furthermore, since ?(s), ??(s) increases with s, the number in curly brackets is
smaller than one. Therefore ?(t) ? 0 as well. ?

Unlike for the means x?i, the BP estimate of variances is only correct in the
large system limit N,K ? ?. In order to quantify the error made, we define
?i = (1/L

(?)
i )? (1/Li) and

D ? 1
K

K?
i=1

?2i . (16)

Theorem 2 Assume the sia�s are i.i.d. uniformly random in {+1,?1}, then D ?
0 in probability as N ??.
Sketch of proof: Freeman and Weiss [7] provide an explicit expression for the
discrepancies ?i. In the present case, their expression can be shown to be equivalent
to the following one

?i =
1

L(?)

?
pi:i?i

( ?1
N?(?)??(?)

)|pi|/2 ?
(k,b)?pi

skb . (17)

Here the sum runs over all the closed non-reversing paths pi, on the bipartite graph
in Fig. 1, starting and ending at i.

In order to convey the basic idea of the proof, let us restrict the above sum to

the paths of a given fixed length 2t, and call ?
(t)
i the corresponding quantity. It is

easy to show that

E(?
(t)
i )

2 =
1

(L(?))2

(
1

N?(?)??(?)

)2t ??JiN,K(2t)?? , (18)
where JiN,K(2t) is the set of couples of closed paths starting at i on the bipartite
graph in Fig. 1, such that each edge in the graph is visited an even number of times.
The dominating contribution to |JiN,K(2t)| comes, in the large system limit, from
couples of coincident paths. Their number scales like N2t?1: we are free to choose

each step but the last one among O(N) vertices. Therefore E(?
(t)
i )

2 = O(N?1)? 0.
In order to complete the proof, one has to control those paths in Eq. (17), whose

length diverges withN . ?



3 MMSE receiver recovered

The per-iteration complexity of the BP algorithm considered above, scales N3.
In fact the number of messages is equal to the number of edges in the complete
bipartite graph of Fig. 1, i.e. O(N2), and each message update involves O(N)
sums. This may be too much for some applications. Also, although we proved that
BP returns the correct MMSE estimate x?, the relation between the two approaches
is quite puzzling. Recall that MMSE is given by the explicit formula

x?(y) = [?2I + S�S]?1S�y . (19)

where S is the N by K matrix whose columns are the signature sequences of the
users. This formula only involve vectors of size O(N). BP, on the other hand,

involved linear operations on the vector [?
(t)
i?a], of size NK.

In order to clarify the relationship between the two approaches, let us write

?
(t)
i?a = G

(t)
i + ??

(t)
i?a , ??

(t)
a?i = G?

(t)
a + ???

(t)
a?i . (20)

The BP update equations for ?
(t)
i?a, cf. Eq. (6), read in the new variables (to lighten

the formulae we assume here sia ? {+1,?1})

G
(t+1)
i =

1

??(t)
?
N

N?
b=1

sibG?
(t)
b +

1

??(t)
?
N

N?
b=1

sib???
(t)
b?i , (21)

??
(t+1)
i?a = ?

1

??(t)
?
N
siaG?

(t)
b ?

1

??(t)
?
N
sia???

(t)
a?i . (22)

Analogously, the update equations for ??
(t)
a?i become

G?(t)a = ya ?
1

?(t)
?
N

K?
k=1

skaG
(t)
k ?

1

?(t)
?
N

K?
k=1

ska??
(t)
k?a , (23)

???
(t)
a?i =

1

?(t)
?
N
siaG

(t)
i +

1

?(t)
?
N
sia???

(t)
i?a . (24)

From Eqs. (22), (24), it follows that ??
(t)
i?a, ??

(t)
i?a = O(N

?1/2) and are given ap-
proximately by

??
(t)
i?a = ?

1

??(t? 1)?N
siaG?

(t?1)
a +O(N

?1) , ???
(t)
a?i =

1

?(t)
?
N
siaG

(t)
i +O(N

?1) .

(25)

Substituting in Eqs. (21), (23), and neglecting O(N?1) terms, we get, with a slight
abuse of notation

G
(t+1)
i =

1

?(t)??(t)
G

(t)
i +

1

??(t)
?
N

N?
b=1

sibG?
(t)
b , (26)

G?(t)a = ya +
?

?(t)??(t? 1)
G?(t?1)a ?

1

?(t)
?
N

K?
k=1

skaG
(t)
k . (27)

These update equations will be referred to as �approximate belief propagation�. As
opposed to ordinary BP, they involve quantities associated to the vertices of the
underlying graphical model. Their complexity per iteration is O(N2), which can
be compared to the O(N3) complexity of ordinary BP for the present problem.



Let us look for a fixed point G(?) = [G
(?)
i ], G?

(?) = [G?
(?)
a ] of Eqs. (26), (27)

and neglect O(N?1) terms for the time being. Recall that, at the fixed point
?(?) = 1 + ?, and ??(?) = 1/?, where ? solves Eq. (9). Substituting in Eqs. (26),
(27), and adopting matrix notation we obtain the fixed point conditions

G(?) =
?

1 + ?
G(?) + ?S�G?(?) , (28)

G?(?) =
??

1 + ?
G(?) + y ? 1

1 + ?
SG(?) . (29)

As explained in Sec. 1, the conditional expectations estimates are given by x?(?) =
G(?)/L(?). It is not hard to realize that L(?) = ?(?)+O(N?1) = 1+?+O(N?1).
By solving Eqs. (28) and (29) for G(?) and using this formula, one finally gets
x?(?) = x?(y), where x?(y) is given by Eq. (19).

The above discussion has important practical implications. One can replace
the original BP equations, with Eqs. (26), (27), which have lower complexity. This
provides an �approximate BP� algorithm. A more careful examination of the above
steps allows to prove the following statement.

Proposition 1 Assume the sia�s are i.i.d uniformly random in {+1,?1}. Then,
with high probability, approximate BP is convergent, G(t) ? G(?), L(t) ? L(?),
and provides correct estimates of the conditional means, i.e. x?(?) ? G(?)/L(?) =
x?(y).

4 Numerical simulations and convergence rate

In the previous Section we discussed the complexity per iteration of the BP detec-
tion algorithm. We found that it can be as small as O(N2) if the �approximate�
update equations (26), (27) are used. Optimal (MMSE) detection is achieved after
convergence to the BP fixed point.

One may wonder how many iterations are required for the algorithm to get
�reasonably close� to its fixed point. A first answer is provided by the proof of
Theorem 1. Each BP iteration involves the multiplication of the vector of messages
by the matrix ?, and the division by N?(t)??(t). Lemma 2 suggests in turn that
the eigenvalues of ? have absolute value not larger than N

?
?. Therefore, at each

iteration, the distance between current vector of messages and the the fixed point
messages is, roughly speaking, rescaled by a factor

?
?

?(t)??(t)
?
?
??

1 + ?
, (30)

where we approximate the quantities on the left hand side by their value at the
fixed point. An equivalent way of formulating this result consists in defining

t?(?, ?) = ?
(
log

?
??

1 + ?

)?1
, (31)

where ? is understood to be the solution of Eq. (9). After t? = t?(?, ?) iterations,
the distance from the fixed point is rescaled by a constant factor e?1. Any precision
? can be achieved within a number of iterations t(?) ? t?(?, ?) log(?/?). where ?
is the distance between the initial and fixed point messages.

It can be shown that, the BP estimate after the first iteration is equal to the
one provided by the traditional matched filter receiver. As the number of iterations



 0

 0.1

 0.2

 0.3

 0.4

 0.5

 0.6

 0  5  10  15  20

M
SE

iteration

?=0.1

?=0.2

?=0.4

?=0.8

 0
 0.1
 0.2
 0.3
 0.4
 0.5
 0.6
 0.7
 0.8

 0  5  10  15  20

M
SE

iteration

?=0.1

?=0.2

?=0.4

?=0.8

Figure 2: Mean square error for BP based multiuser detection, as a function of the
number of iterations. The asymptotic values coincide with the ones for MMSE detection.
Continuous lines refer to ordinary BP, and dashed lines to approximate BP. Left: K =
100 users and N = 200 chips (? = 0.5). Right: K = 100 users and N = 100 chips
(? = 1).

increases, BP moves from the matched filter to the MMSE estimate. After about
t?(?, ?) log(1/?) iterations, the distance from the MMSE estimate is reduced by a
factor ? with respect to a matched filter receiver.

In order to confirm this analysis and obtain some concrete feeling of the ac-
tual number of required BP iterations, we plot in Fig. 2 the results of numerical
simulations of the BP based detection algorithm. Each couple of curves refers to
the application of BP (continuous curves) or approximate BP (dashed curves) to a
single signature/noise realization. Convergence to the MMSE receiver was achieved
in each case.

A small number of iterations (about 5) provides a large improvement over the
matched filter receiver. The number of iterations required for convergence is always
moderate and consistent with the estimate of Eq. (31). For ? = 0.5 (left frame),
we obtain, for instance, t? ? 2.7, 2.4, 1.7 and 1.0 for (respectively) ? = 0.1, 0.2,
0.4 and 0.8. If ? = 1 (right frame), we obtain t? ? 10.0, 5.0, 2.5 and 1.3 for the
same values of ?.

5 Conclusion and generalizations

We can envision two types of generalizations of the present work. First, one can
refine the model (1) by considering, for instance, different power constraints for
different users, or signature sequences with more general distributions than the one
considered in this paper. While such generalizations can be technically cumber-
some, they should possible along the same lines exposed here.

The second direction consists in modifying the a priori distribution of the signals
Xi, by considering, for instance, binary antipodal signals, xi ? {+1,?1}. The
proves of Theorems 1 and 2 are heavily based on the general results on belief
propagation for Gaussian graphical models in [7] and [8]. These results do not
extend to symbols xi�s with a general a priori distribution. However, we expect
the thesis (convergence and correctness of belief propagation) to remain true in the
large system limit. We plan to use the ideas of [9] to prove this claim.



Acknowledgments

AM has been partially supported by EVERGROW, i.p. 1935 of the EU Sixth
Framework.

References

[1] D. Tse and S. V. Hanly, Linear Multiuser Receivers: Effective Interference,
Effective Bandwidth and User Capacity, IEEE Trans. Inform. Theory, vol. 45,
pp. 641-657, 1999

[2] S. Verdu� and S. Shamai, Spectral Efficiency of CDMA with Random Spreading,
IEEE Trans. Inform. Theory, vol. 45, pp. 622-640, 1999

[3] T. Tanaka, A Statistical�Mechanics Approach to Large�Systems Analysis of
CDMA Multiuser Detectors, IEEE Trans. Inform. Theory, vol. 48, pp. 2888-
2910, 2002

[4] Y. Kabashima, A CDMA Multiuser Detection Algorithm on the Basis of Belief
Propagation, J. Phys. A: Math. Gen., vol. 36, pp. 11111�11121, 2003

[5] T. Tanaka and M. Okada, Approximate Belief Propagation, Density Evolution,
and Neurodynamics for CDMAMultiuser Detection, IEEE Trans. Inform. The-
ory, vol. 51, pp. 700-706, 2005

[6] J.P. Neirotti and D. Saad, Improved Message Passing for Inference in Densely
Connected Systems, Europhys. Lett. vol. 71, pp. 866�872, (2005).

[7] Y. Weiss and W. T. Freeman, Correctness of belief propagation in Gaus-
sian graphical models of arbitrary topology, Neural Computation 13:2173-2200
(2001)

[8] P. Rusmevichientong and B. Van Roy, An Analysis of Belief Propagation on the
Turbo Decoding Graph with Gaussian Densities, IEEE Trans. Inform. Theory,
vol. 47, pp. 745-765, 2001

[9] C. Me�asson, A. Montanari, T. Richardson, and R. Urbanke, Life Above
Threshold: From List Decoding to Area Theorem and MSE, Proc. of the IEEE
Inform. Theory Workshop, San Antonio, Texas, October 24-29 2004. Available
online at http://arxiv.org/abs/cs.IT/0410028.



	Introduction
	Main result: convergence and correctness
	MMSE receiver recovered
	Numerical simulations and convergence rate
	Conclusion and generalizations

