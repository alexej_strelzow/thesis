
ar
X

iv
:0

90
1.

23
67

v2
  [

cs
.IT

]  
18

 Ja
n 2

00
9

An Implementable Scheme for Universal Lossy
Compression of Discrete Markov Sources

Shirin Jalali,? Andrea Montanari? and Tsachy Weissman?�,
?Department of Electrical Engineering, Stanford University, Stanford, CA 94305,

� Department of Electrical Engineering, Technion, Haifa 32000, Israel
{shjalali, montanar, tsachy}@stanford.edu

Abstract

We present a new lossy compressor for discrete sources. For coding a source sequence xn, the encoder starts
by assigning a certain cost to each reconstruction sequence. It then finds the reconstruction that minimizes this
cost and describes it losslessly to the decoder via a universal lossless compressor. The cost of a sequence is given
by a linear combination of its empirical probabilities of some order k + 1 and its distortion relative to the source
sequence. The linear structure of the cost in the empirical count matrix allows the encoder to employ a Viterbi-like
algorithm for obtaining the minimizing reconstruction sequence simply. We identify a choice of coefficients for
the linear combination in the cost function which ensures that the algorithm universally achieves the optimum
rate-distortion performance of any Markov source in the limit of large n, provided k is increased as o(log n).

I. INTRODUCTION
Let X = {Xi : i ? 1} represent a discrete-valued stationary ergodic process with unknown statistics,

and consider the problem of compressing X at rate R such that the incurred distortion is minimized. Let
X and X� denote finite source and reconstruction alphabets respectively. The performance of the described
coding scheme is measured by its average expected distortion between source and reconstruction blocks,
i.e.

D = E dn(X
n, X�n) ,

1

n

n?
i=1

E d(Xi, X�i), (1)

where d : X � X ? R+ is a single-letter distortion measure. For any R ? 0, the minimum achievable
distortion (cf. [4] for exact definition of achievability) is characterized as [1], [2], [3]

D(X, R) = lim
n??

min
p(X�n|Xn):I(Xn;X�n)?R

E dn(X
n, X�n). (2)

A sequence of codes at rate R is called universal if for every stationary ergodic source X its asymptotic
performance converges to D(X, R), i.e.,

lim sup
n??

E dn(X
n, X�n) ? D(X, R). (3)

For lossless compression where the source is to be recovered without any errors, there already exist
well-known implementable universal schemes such as Lempel-Ziv coding [5] or arithmetic coding [6]. In
contrast to the situation of lossless compression, for D > 0, there are no well-known practical schemes that
universally achieve the rate-distortion curve. In recent years, there has been progress towards designing
universal lossy compressor especially in trying to tune some of the existing universal lossless coders to
work in the lossy case as well [7], [8], [9]. All of these algorithms are either provably suboptimal, or
optimal but with exponential complexity.

Another approach for lossy compression, which is very well-studied in the literature and even imple-
mented in JPEG 2000 image compression standard, is Trellis coded quantization, i.e. Trellis structured
code plus Viterbi encoding (c.f. [10], [11] and references therein). This method is in general suboptimal
for coding sources that have memory [11]. In [12], an algorithm for fixed-slope Trellis source coding is




proposed, and is shown to be able to get arbitrary close to the rate-distortion curve for continuous-valued
stationary ergodic sources. The proposed method is efficient in low rate region.

In a recent work [13], a new implementable algorithm for lossy compression of discrete-valued stationary
ergodic sources was proposed. Instead of fixing rate (or distortion) and minimizing distortion (or rate),
the new algorithm fixes Lagrangian coefficient ?, and minimizes R + ?D. This is done by assigning
energy E(yn) representing R+?D to each possible reconstruction sequence and finding the sequence that
minimizes the cost by simulated annealing. The algorithm starts by letting yn = xn, and at each iteration
chooses an index i ? {1, . . . , n} uniformly at random, and probabilistically changes yi to some y ? X�
such that there is a positive probability (which goes to zero as the number of iterations increases) that the
resulting sequence has higher energy than the original sequence. Allowing the energy to increase especially
at initial steps prevents the algorithm from being entrapped in a local minimum. It was shown that using
a universal lossless compressor to describe the reconstruction sequence resulting from this process to the
decoder results in a scheme which is universal in the limit of many iterations and large block length. The
drawback of the proposed scheme is that although its computational complexity per iteration is independent
of the block length n and linear in a parameter kn = o(logn), there is no useful bound on the number
of iterations required for convergence. In this paper, inspired by the previous method, we propose yet
another approach for lossy compression of discrete Markov sources which universally achieves optimum
rate-distortion performance for any discrete Markov source. We start by assigning the same cost that
was defined for each possible reconstruction sequence in [13]. The cost of each sequence is a linear
combination of two terms: its empirical conditional entropy and its distance to the source sequence to be
coded. We show that there exists proper linear approximation of the first term such that minimizing the
linearized cost results in the same performance as minimizing the original cost. But the advantage is that
minimizing the modified cost can be done via Viterbi algorithm in lieu of simulated annealing which was
used for minimizing the original cost.

The organization of the paper is as follows. In Section II, we set up the notation, and define the
count matrix and empirical conditional entropy of a sequence. Section III describes a new coding scheme
for fixed-slope lossy compression which universally achieves the rate-distortion curve for any discrete
Markov source and IV describes how to compute the coefficients required by the algorithm outlined in
the previous section. Section V explains how Viterbi algorithm can be used for implementing the coding
scheme described in Section III. Section VI presents some simulations results, and finally, Section VII
concludes the paper with a discussion of some future directions.

Proofs that are not presented in the paper will appear in the full version.

II. NOTATIONS AND REQUIRED DEFINITIONS
Let X and X� denote the source and reconstruction alphabets respectively. Let matrix m(yn) ? R|X� | �

R
|X� |k represent (k + 1)th order empirical count of yn defined as

m?,b(y
n) =

1

n

??{1 ? i ? n : yi?1i?k = b, yi = ?]}?? . (4)
In (4), and throughout we assume a cyclic convention whereby yi , yn+i for i ? 0. Let Hk(yn) denote
the conditional empirical entropy of order k induced by yn, i.e.

Hk(y
n) = H(Yk+1|Y

k), (5)
where Y k+1 on the right hand side of (5) is distributed according to

P(Y k+1 = [b, ?]) = m?,b(y
n), (6)



where ? ? X� , and b ? X� k, and [b, ?] represents the vector made by concatenation of b and ?. We will
use the same notation throughout the paper, namely, ?, ? ?, . . . ? X� , and b,b?, . . . ? X� k. The conditional
empirical entropy in (5) can be expressed as a function of m(yn) as follows

Hk(y
n) = Hk(m(y

n)) :=
1

n

?
?,b

H (m�,b(y
n))1Tm�,b(y

n), (7)

where 1 and m�,b(yn) denote the all-ones column vector of length |X� |, and the column in m(yn)
corresponding to b respectively. For a vector v = (v1, . . . , v?)T with non-negative components, we let
H(v) denote the entropy of the random variable whose probability mass function (pmf) is proportional
to v. Formally,

H(v) =

??
?

??
i=1

vi
?v?1

log ?v?1
vi

if v 6= (0, . . . , 0)T

0 if v = (0, . . . , 0)T .
(8)

III. LINEARIZED COST FUNCTION
Consider the following scheme for lossy source coding at fixed slope ? > 0. For each source sequence

xn let the reconstruction block x�n be

x�n = arg min
yn?X�n

[Hk(y
n) + ?dn(x

n, yn)] . (9)

The encoder, after computing x�n, losslessly conveys it to the decoder using LZ compression. Let k grow
slowly enough with n so that

lim sup
n??

max
yn

[
1

n
?LZ(y

n)?Hk(y
n)

]
? 0, (10)

where ?LZ(yn) denotes the length of the LZ representation of yn. Note that Ziv�s inequality guarantees that
if k = kn = o(logn) then (10) holds.

Theorem 1: [13] Let X be a stationary and ergodic source, let R(X, D) denote its rate distortion
function, and let X�n denote the reconstruction using the above scheme for coding Xn. Then

E

[
1

n
?LZ(X�

n) + ?dn(X
n, X�n)

]
n??
?? min

D?0
[R(X, D) + ?D] . (11)

In other words, conveying the reconstruction sequence to the decoder via universal lossless compression
(selection of LZ algorithm here is for concreteness, but other universal lossless methods can be used as
well) achieves optimum fixed-slope rate-distortion performance universally.

As proposed in [13], the exhaustive search required by this algorithm can be tackled through simulated
annealing Gibbs sampling. Here assuming the source is a discrete Markov source, we propose another
method for finding a sequence achieving the minimum in (9). The advantage of the new method is that
its computational complexity is linear in n for fixed k.

Before describing the new scheme, consider the problems (P1) and (P2) described below.

(P1) : min
yn

[Hk(m(y
n)) + ?dn(x

n, yn)] , (12)

and

(P2) : min
yn

[?
?

?
b

??,bm?,b(y
n) + ?dn(x

n, yn)

]
. (13)



Comparing (P1) with (9) reveals that it is the optimization required by the exhaustive search coding
scheme described before. The question is whether it is possible to choose a set of coefficients {??,b}?,b,
? ? X� and b ? X� k, such that (P1) and (P2) have the same set of minimizers or at least, the set of
minimizers of (P2) is a subset of minimizers of (P1). If the answer to this question is affirmative, then
instead of solving (P1) one can solve (P2), which, as we describe in Section V, can be done simply via
the Viterbi algorithm.

Let S1 and S2 denote the set of minimizers of (P1) and (P2). Consider some zn ? S1, and let m?n =
m(zn). Since H(m) is concave in m,1 for any empirical count matrix m, we have

H(m) ? H(m?n) +
?
?,b

?

?m?,b
H(m)|m?n(m?,b ?m

?
?,b) (14)

, H�(m). (15)
Now assume that in (P2), the coefficients are chosen as follows

??,b =
?

?m?,b
H(m)

??
m?n

. (16)

Lemma 1: (P1) and (P2) have the same minimum value, if the coefficients are chosen according to
(16). Moreover, if all the sequences in S1 have the same type, then S1 = S2.

Proof: For any yn ? X� n,
H(m(yn)) + ?dn(x

n, yn) ? H�(m(yn)) + ?dn(x
n, yn). (17)

Therefore,

min
yn

[H(m(yn)) + ?dn(x
n, yn)] ? min

yn
[H�(m(yn)) + ?dn(x

n, yn)] (18)
? H�(m(zn)) + ?dn(x

n, zn) (19)
= min

yn
[H(m(yn)) + ?dn(x

n, yn)]. (20)

This shows that (P1) and (P2) have the same minimum values. For any sequence yn with m(yn) 6= m?n,
by strict concavity of H(m),

H�(m(yn)) + ?dn(x
n, yn) > H(m(yn)) + ?dn(x

n, yn), (21)
? min

yn
[H(m(yn)) + ?dn(x

n, yn)]. (22)

As a result all the sequences in S2 should have the empirical count matrix equal to m?n. Since for these
sequences H(m) = H�(m), we also conclude that S2 ? S1. If there is a unique minimizing type m?n, then
S1 = S2.

This shows that if we knew the optimal type m?n, then we could compute the optimal coefficients via
(16), and solve (P2) instead of (P1). The problem is that m?n is not known to the encoder (since knowledge
of m?n requires solving (P1) which is the problem we are trying to avoid). In the next section, we describe
a method for approximating m?n, and hence the coefficients {??,b}.

1As proved in Appendix B.



IV. HOW TO CHOOSE THE COEFFICIENTS?
For a given stationary ergodic source X, and for any given count matrix m define D(m) to be the

minimum average expected distortion among all processes Y that are jointly stationary ergodic with X
and their (k+1)th order stationary distribution is according to m.2 D(m) can equivalently be defined as

D(m) = lim
k1??

min
p(xk1 ,yk1 )?M(k1)

Ep d(x
k1 , yk1), (23)

where M(k1) is the set of all jointly stationary distributions p(xk1 , yk1) of (Xk1, Y k1) with marginal
distributions with respect to x coinciding with the kth1 order distribution of X process, and with marginal
distributions with respect to y coinciding with m, i.e., having the (k + 1)th order marginal distribution
described by m.

Lemma 2: If the source is ?th order Markov, then

D(m) = min
p(xk1 ,yk1)?M(k1)

Ep d(x
k1, yk1), (24)

where k1 = max(?, k + 1).
Proof: [outline] Using the technique described in Appendix A, for any legitimate given joint distri-

bution p(xk1, yk1) with the marginal distribution with respect to x coinciding with the source distribution
and with marginal distribution with respect to y coinciding with some given distribution m, it is possible
to construct a process which is jointly stationary and ergodic with our source process and also has the
(k+1)th order joint distribution as p(xk1, yk1). Using this gives us an achievable distortion, i.e., an upper
bound on D(m). On the other hand, the limit given in (23) is approaching D(m) from below. Combining
the upper and lower bounds yields the desired equality.

Since by assumption the encoder does not know ?, therefore it can not compute max(?, k + 1). But
letting k1 = k + 1, where k = o(log n), for any fixed order ?, k1 will eventually for n large enough,
exceed ?, and hence be equal to max(?, k + 1). Having this observation in mind, consider the following
optimization problem,

min H(m) + ?D(m)

s.t. m ?M(k1). (25)
By Lemma 2, an equivalent representation of (25) is

min H(m) + ?
?

?,??,b,b?

dk1(?
?
b
?, ?b)px(?

?
b
?)qy|x(?b|?

?
b
?)

s.t. m?,b =
?
??b?

px(?
?
b
?)qy|x(?b|?

?
b
?), ? ?,b,

0 ? qy|x(?b|?
?
b
?) ? 1, ? ?, ? ?,b,b?,?

?,b

qy|x(?b|?
?
b
?) = 1, ? ? ?,b?,

?
?,??

px(?
?
b
?)qy|x(?b|?

?
b
?) =

?
?,??

px(b
?? ?)qy|x(b?|b

?? ?) ?b,b?. (26)

The last constraint in (26) is the stationarity condition defined in (A-1), and ensures that the joint
distribution defined by px(?b)qy|x(? ?b?|?,b?) over (xk+1, yk+1) corresponds to (k + 1)th order marginal
distribution of some jointly stationary processes (X,Y). Note that the variables in (26) are conditional
distributions qy|x(yk1|xk1), but we are only interested in the m that they induce.

2As discussed in Appendix A, the set of such processes is non-empty for any legitimate m.



Lemma 3: If for each n, (P1) has a unique minimizing type m?n, then
?m?n ? m�

?
n?TV ? 0, a.s., (27)

where m�?n is the solution of (26).
Remark: In (26), the only dependence on n is through k1.
Therefore, if the encoder knew the distribution of the source, it could solve (26), find a good approx-

imation of m?n, and then use (16) to compute the coefficients required by (P2). The problem is that the
encoder does not have this information, and only knows that the source is Markov (but does not know
its order). To overcome its lack of information, a reasonable step is to use empirical distribution of the
source instead of the true unknown distribution in (26). For ak1 ? X k1 , define the kth1 order empirical
distribution of the source as

p�(k1)x (a
k1) ,

|{i : (xi?k1, . . . , xi?1) = a
k1}|

n
. (28)

The following lemma shows that for k1 = o(log n), p�(k1) converges to the actual kth1 order distribution of
the source, and therefore can be considered as a good approximation for it.

Lemma 4: For k1 = o(logn), and any stationary ergodic Markov source,

?p�(k1) ? p(k1)?TV ? 0 a.s., (29)
where p(k1) is the true kth1 order distribution of the Markov source.

Assume xn is generated by a discrete Markov source, and let p�(k1)x be its empirical distribution defined
in (28). Consider the following optimization problem

min H(m) + ?
?

?,??,b,b?

dk1(?
?
b
?, ?b)p�(k1)x (?

?
b
?)qy|x(?b|?

?
b
?)

s.t. m?,b =
?
??b?

p�(k1)x (?
?
b
?)qy|x(?b|?

?
b
?), ? ?,b,

0 ? qy|x(?b|?
?
b
?) ? 1, ? ?, ? ?,b,b??

?,b

qy|x(?b|?
?
b
?) = 1, ? ? ?,b?,

?
?,??

p�(k1)x (?
?
b
?)qy|x(?b|?

?
b
?) =

?
?,??

)p�(k1)x (b
?? ?)qy|x(b?|b

?? ?), ?b,b?. (30)

and let m�?n denote the output of the above optimization problem.
Lemma 5: For k1 = k1(n) = o(log n),

?m�?n ? m�
?
n?TV ? 0, a.s.

Proof: [outline] The input parameters of the optimization problem (30) are {p�(k1)(ak1)}ak1?Xk1 ,
therefore m�?n = m�?n({p�(k1)(ak1)}ak1?Xk1 ). On the other hand, both the cost function and the constraints
of (30) are continuous both in input parameters and optimization variables. This means that m�?n in turn
is a continuous function of {p�(xk1)}xk1?Xk1 .

Let {??,b(n)}?,b denote the optimal values of the coefficients defined at m?n (as given in (16)), and let
{?�?,b(n)}?,b be coefficients computed at m�?n, then

Lemma 6:

max
?,b

|??,b(n)? ?�?,b(n)| ? 0 as n??. (31)
These results suggest that for computing the coefficients we can solve the optimization problem given

in (30) (whose complexity can be controlled with the rate of increase of k1), and then substitute the result



in (16) to obtain the approximate coefficients. After that (P2) defined by these coefficients can be solved
using the Viterbi algorithm in a way that will be detailed in the next section. The succession of lemmas
detailed in the previous sections then allow us to prove the following theorem.

Theorem 2: Let X let a stationary and ergodic Markov source, and R(X, D) denote its rate distortion
function. Let X�n be the reconstruction sequence obtained using the above scheme for coding Xn choosing
k1 = k + 1, where k = o(logn). Then

E

[
1

n
Hk(m(X�

n)) + ?dn(X
n, X�n)

]
n??
?? min

D?0
[R(X, D) + ?D] . (32)

Remark: Theorem 2 implies the fixed-slope universality of the scheme which does the lossless com-
pression of the reconstruction by first describing its count matrix (costing a number of bits which is
negligible for large n) and then doing the conditional entropy coding.

V. VITERBI CODER
As proved in Section III, instead of solving (P1), one can solve (P2) for proper choices of coefficients

{?b,?}. Note that ?
?,b

[??,bm?,b(y
n) + ?dn(x

n, yn)] =
1

n

n?
i=1

[
?yi,yi?1i?k

+ ?d(xi, yi)
]
. (33)

This alternative representation of the cost function suggests that instead of using simulated annealing, we
can find the sequence that minimizes the cost function by the Viterbi algorithm. For i = k+ 1, . . . , n, let
si = y

i
i?k be the state at time i, S be the set of all 2k+1 possible states, and for s = bk+1 define

w(s, i) := ?bk+1,bk + ?d(xi, bk+1).

From our definition of the states si = g(si?1, yi), where g : S � X� ? S. This representation leads
to a Trellis diagram corresponding to the evolution of the states {si}mi=k+1 in which each state has |X� |
states leading to it and |X� | states branching from it. Assume that weight w(si, i) is assigned to the edge
connecting states si?1 and si, i.e., the cost of each edge only depends on the tail state.

It is clear that in our representation, there is a 1-to-1 correspondence between sequences yn ? X� n and
sequences of states {si}mi=k+1, and minimizing (33) is equivalent to finding the path of minimum weight
in the corresponding Trellis diagram, i.e., the path {si}ni=k+1 that minimizes

?n
i=k+1 w(si, i). Solving this

minimization can readily be done by Viterbi algorithm which can be described as follows. For each state
s, let L(s) be the two states leading to it, and for any i > 1,

C(s, i) := min
s??L(s)

[w(s, i) + C(s?, i? 1)]. (34)

For i = 1 and s = bk+1, let C(s, 1) := ?bk+1,bk + ?dk+1(xk+1, bk+1). Using this procedure, each state s
at each time j has a path of length j ? k ? 1 which is the minimum path among all the possible paths
between i = k + 1 and i = j such that sj = s. After computing {C(s, i)} s?S

i?{k+1,...,n}
, at time i = n, let

s? = argmin
s?S

C(s, n). (35)

It is not hard to see that the path leading to s? is the path of minimum weight among all possible paths.
Note that the computational complexity of this procedure is linear in n but exponential in k because

the number of states increases exponentially with k.



0 0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1
0.25

0.3

0.35

0.4

0.45

0.5

0.55

0.6

0.65

0.7

0.75

D

R

 

 

R(D)

Shannon lower bound

? = 4

? = 3.5

? = 3

? = 2.5

? = 2

Fig. 1. (dn(xn, x�n),Hk(x�n)) of output points of Viterbi encoder when the coefficients are computed at m[xn]. For each value of ?, the
algorithm is run L = 20 times. Here n = 5000, k = 7, and the source is binary Markov with q = 0.2

VI. SIMULATION RESULTS
In this section, some preliminary simulation results of the application of Viterbi encoder described in the

previous section is presented. In our simulations, instead of computing the coefficients {??,b} from (16)
at the optimal point m?n, we compute them at the count matrix of the input sequence xn, m(xn). Fig. 1
demonstrates (dn(xn, yn), Hk(m(yn))) of output points of the described algorithm. The block length is
n = 5000, k = 7 and the source is 1st order binary symmetric Markov with transition probability q = 0.2.
For each value of ? the algorithm is applied to L = 20 different randomly generated sequences. The
reason of getting some points below the rate-distortion curve is that the actual number of bits required
for describing x�n losslessly to the decoder is larger than nHk(x�n), but converges to it as n grows. For
example, for the simple scheme of separately describing the subsequences corresponding to different
preceding contexts, this surplus is of order 2k logn/n. The effect of this excess rate is not reflected in
the figure, which explains why some points appear below the rate-distortion curve.

It can be observed that for larger values of ? the output points are closer to the curve. The reason is
that large values of ? correspond to small values of distortion, and if the distortion is small then m(xn)
is a good approximation of m(yn).

Finally, Fig. 2 compares the performance of the new Viterbi encoder and the MCMC encoder described
in [13]. Here the source is again binary symmetric Markov with q = 0.2, and the other parameters are:
k = 7, n = 5, 000, ?t = n log t, r = 10n, where ?t determines the cooling schedule of the MCMC coder
and r is its number of iterations. Each point is the figure corresponds to the average performance of
L = 10 random realizations of the source. It can be observed that even for this simplistic choice of the
coefficients the performance of the algorithms are comparable, while the Viterbi encoder for example in
this example runs at least 40 times faster.

VII. CONCLUSIONS AND CURRENT DIRECTIONS
In this paper, a new method for universal fixed-slope lossy compression of discrete Markov sources was

proposed. The new method achieves the rate-distortion curve for any discrete Markov source. Extending



0 0.005 0.01 0.015 0.02 0.025 0.03 0.035 0.04 0.045 0.05
0.4

0.45

0.5

0.55

0.6

0.65

0.7

0.75

D

R

 

 

R(D)

Shannon lower bound

Viterbi coder: ? = 4

MCMC coder: ? = 4

Viterbi coder: ? = 3.5

MCMC coder: ? = 3.5

Viterbi coder: ? = 3

MCMC coder: ? = 3

Viterbi coder: ? = 2.5

MCMC coder: ? = 2.5

Viterbi coder: ? = 2

MCMC coder: ? = 2

Fig. 2. Comparing the performances of Viterbi encoder and MCMC encoder proposed in [13]

the algorithm to work on any stationary ergodic source is under current investigation. We believe that in
fact the same algorithm works for the general class of stationary ergodic sources, and only the proof should
be extended to work in this case as well. Another direction for future work is finding a simple method for
approximating the optimal coefficients that would alleviate the need for solving the optimization problem
(30).

APPENDIX A: STATIONARITY CONDITION
Assume that we are given a |X� | � |X� |k matrix m with all elements positive and summing up to one.

The question is under what condition(s) this matrix can be (k + 1)th order stationary distribution of a
stationary process. For the ease of notations, instead of matrix m consider p(xk+1) as a distribution defined
on X� k+1. We show that a necessary and sufficient condition is the so-called stationarity condition which
is ?

??X�

p(?xk) =
?
??X�

p(xk?). (A-1)

- Necessity: The necessity of (A-1) is just a direct result of the definition of stationarity of a process.
If p(xk+1) is to represent the (k + 1)th order marginal distribution of a stationary process, then it
should be consistent with the kth order marginal distribution as satisfy (A-1).

- Sufficiency: In order to prove the sufficiency, we assume that (A-1) holds, and build a stationary
process with (k + 1)th order marginal distribution of p(xk+1). Consider a kth order Markov chain
with transition probabilities of

q(xk+1|x
k) =

p(xk+1)

p(xk)
. (A-2)

Note that p(xk) is well-defined by (A-1). Moreover, again from (A-1), p(xk+1) is the stationary



distribution of the defined Markov chain, because?
x1

q(xk+1|x
k)p(xk) =

?
x1

p(xk+1) = p(xk+12 ). (A-3)

Therefore we have found a stationary process that has the desired marginal distribution.
Finally we show that if m is the count matrix of a sequence yn, then there exist a stationary process

with the marginal distribution coinciding with m. From what we just proved, we only need to show that
(A-1) holds, i.e., ?

?

m?,b =
?
?

mbk,[?,b1...,bk?1]. (A-4)

But this is true because both sides of (A-4) are equal to |{i : yi+ki+1 = b}|/n.
APPENDIX B: CONCAVITY OF H(m)

For simplicity assume that X = X� = {0, 1}. By definition

H(m) =
?

b?{0,1}k

(m0,b +m1,b)h(
m0,b

m0,b +m1,b
), (B-1)

where h(?) = ? log? + ?� log ?� and ?� = 1? ?. We need to show that for any ? ? [0, 1], and empirical
count matrices m(1) and m(2),

?H(m(1)) + ?�H(m(2)) ? H(?m(1) + ?�m(2)). (B-2)
From the concavity of h, it follows that

?(m
(1)
0,b +m

(1)
1,b)h(

m
(1)
0,b

m
(1)
0,b +m

(1)
1,b

) + ?�(m
(2)
0,b +m

(2)
2,b)h(

m
(2)
0,b

m
(2)
0,b +m

(2)
2,b

)

= (?(m
(1)
0,b +m

(1)
1,b) + ?�(m

(2)
0,b +m

(2)
1,b))

?
i?{1,2}

?i(m
(i)
0,b +m

(i)
1,b)

(?(m
(1)
0,b +m

(1)
1,b) + ?�(m

(2)
0,b +m

(2)
1,b))

h(
m

(i)
0,b

m
(i)
0,b +m

(i)
1,b

)

? (?(m
(1)
0,b +m

(1)
1,b) + ?�(m

(2)
0,b +m

(2)
1,b))h(

?m
(1)
0,b + ?�m

(2)
0,b

?(m
(1)
0,b +m

(1)
1,b) + ?�(m

(2)
0,b +m

(2)
1,b)

), (B-3)

where ?1 = 1? ?2 = ?. Now summing up both sides of (B-3) over all b ? X� k, yields the desired result.
REFERENCES

[1] C. Shannon, �Coding theorems for a discrete source with a fidelity criterion,� IRE Nat. Conv. Rec, part 4, pp. 142-163, 1959.
[2] R.G. Gallager, �Information Theory and Reliable Communication,� New York, NY: John Wiley & Sons, 1968.
[3] T. Berger, Rate-distortion theory: A mathematical basis for data compression, Englewood Cliffs, NJ: Prentice-Hall, 1971.
[4] T. M. Cover, and J. A. Thomas, Elements of Information Theory, New York: Wiley, 1991.
[5] J. Ziv and A. Lempel, �Compression of individual sequences via variable-rate coding,� IEEE Trans. on Inf. Theory, 24(5):530-536,

Sep. 9178.
[6] I. H. Witten, R. M. Neal, and J. G. Cleary, �Arithmetic coding for data compression�, Commun. Assoc. Comp. Mach., vol. 30, no. 6,

pp. 520-540, 1987.
[7] I. Kontoyiannis, �An implementable lossy version of the Lempel Ziv algorithm-Part I: optimality for memoryless sources,� IEEE Trans. on

Inform. Theory, vol. 45, pp. 2293-2305, Nov. 1999.
[8] E. Yang, Z. Zhang, and T. Berger, �Fixed-slope universal lossy data compression,� , IEEE Trans. on Inform. Theory, vol. 43, no. 5,

pp. 1465-1476, Sep. 1997.
[9] E. H. Yang and J. Kieffer, �Simple universal lossy data compression schemes derived from the Lempel-Ziv algorithm,� IEEE Trans. on

Inform. Theory, vol. 42, no. 1, pp. 239-245, 1996.
[10] T. Berger, J.D. Gibson, �Lossy source coding,� IEEE Trans. on Inform. Theory, vol. 44, no. 6, pp. 2693-2723, 1998.
[11] A. Gersho, R.M. Gray, Vector Quantization and Signal Compression Springer, 1992.
[12] E. Yang, and Z. Zhang, �Variable-Rate Trellis Source Encoding�, IEEE Trans. on Inform. Theory, vol. 45, no. 2, pp. 586-608, 1999.
[13] S. Jalali, T. Weissman, �Lossy coding via Markov chain Monte Carlo,� IEEE International Symposium on Information Theory, Toronto,

Canada, 2008.


	Introduction
	Notations and required definitions
	Linearized cost function
	How to choose the coefficients?
	Viterbi coder
	Simulation results
	Conclusions and Current Directions
	References

