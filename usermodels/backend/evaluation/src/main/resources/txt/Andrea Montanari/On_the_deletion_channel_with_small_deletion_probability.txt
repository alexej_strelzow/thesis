
ar
X

iv
:0

91
2.

51
76

v3
  [

cs
.IT

]  
7 J

un
 20

10

On the deletion channel with small deletion
probability

Yashodhan Kanoria? and Andrea Montanari?�
Departments of Electrical Engineering? and Statistics�, Stanford University

Email: {ykanoria, montanari}@stanford.edu

Abstract�The deletion channel is the simplest point-to-point
communication channel that models lack of synchronization.
Despite significant effort, little is known about its capacity, and
even less about optimal coding schemes. In this paper we initiate a
new systematic approach to this problem, by demonstrating that
capacity can be computed in a series expansion for small deletion
probability. We compute two leading terms of this expansion, and
show that capacity is achieved, up to this order, by i.i.d. uniform
random distribution of the input.

We think that this strategy can be useful in a number of
capacity calculations.

I. INTRODUCTION

The (binary) deletion channel accepts bits as inputs, and
deletes each transmitted bit independently with probability
d. Computing or providing systematic approximations to its
capacity is one of the outstanding problems in information
theory [1]. An important motivation comes from the need to
understand synchronization errors and optimal ways to cope
with them.

In this paper we suggest a new approach. We demonstrate
that capacity can be computed in a series expansion for small
deletion probability, by computing the first two orders of such
an expansion. Our main result is the following.

Theorem I.1. Let C(d) be the capacity of the deletion channel
with deletion probability d. Then, for small d and any ? > 0,

C(d) = 1 + d log d?A1 d+O(d
3/2??) , (1)

where A1 ? log(2e) ?
??

l=1 2
?l?1l log l. Further, the iid

Bernoulli(1/2) process achieves capacity up to corrections of
order O(d3/2??).

Logarithms here (and in the rest of the paper) are understood
to be in base 2. The constant A1 can be easily evaluated to
yield A1 ? 1.154163765. While one might be skeptical about
the concrete meaning of asymptotic expansions of the type
(1), they often prove surprisingly accurate. For instance at 10%
deletion probability, Eq. (1) is off the best lower bound proved
in [5] by about 0.010 bits. More importantly they provide
useful design insight. For instance, the above result shows that
Bernoulli(1/2) is an excellent starting point for the optimal
input distribution. Next terms in expansion indicate how to
systematically modify the input distribution for d > 0 [2].

We think the strategy adopted here might be useful in other
information theory problems. The underlying philosophy is
that whenever capacity is known for a specific value of the

 0.2

 0.4

 0.6

 0.8

 1

 0  0.1  0.2  0.3  0.4  0.5  0.6

C(
d)

d

Fig. 1. Comparison of the asymptotic formula (1) (continuous line) with
upper bounds from [6] (stars ?) and lower bounds from [5] (squares, ?). The
O(d3/2??) term in (1) was simply dropped.

channel parameter, and the corresponding optimal input dis-
tribution is unique and well characterized, it should be possible
to compute an asymptotic expansion around that value. Here
the special channel is the perfect channel, i.e. the deletion
channel with deletion probability d = 0. The corresponding
input distribution is the iid Bernoulli(1/2) process.

A. Related work
Dobrushin [3] proved a coding theorem for the deletion

channel, and other channels with synchronization errors. He
showed that the maximum rate of reliable communication is
given by the maximal mutual information per bit, and proved
that this can be achieved through a random coding scheme.
This characterization has so far found limited use in proving
concrete estimates. An important exception is provided by the
work of Kirsch and Drinea [4] who use Dobrushin coding
theorem to prove lower bounds on the capacity of channels
with deletions and duplications. We will also use Dobrushin
theorem in a crucial way, although most of our effort will be
devoted to proving upper bounds on the capacity.

Several capacity bounds have been developed over the last
few years, following alternative approaches, and are surveyed
in [1]. In particular, it has been proved that C(d) = ?(1? d)
as d ? 1. However determining the asymptotic behavior in
this limit (i.e. finding a constant B1 such that C(d) = B1(1?
d)+ o(1? d)) is an open problem. When applied to the small
d regime, none of the known upper bounds actually captures
the correct behavior (1). As we show in the present paper, this




behavior can be controlled exactly.
When this paper was nearing submission, a preprint by

Kalai, Mitzenmacher and Sudan [7] was posted online, proving
a statement analogous to Theorem I.1. The result of [7] is
however not the same as in Theorem I.1: only the d log d term
of the series is proved in [7]. Further, the two proofs are based
on very different approaches.

II. PRELIMINARIES
For the reader�s convenience, we restate here some known

results that we will use extensively, along with with some
definitions and auxiliary lemmas.

Consider a sequence of channels {Wn}n?1, where Wn
allows exactly n inputs bits, and deletes each bit independently
with probability d. The output of Wn for input Xn is a binary
vector denoted by Y (Xn). The length of Y (Xn) is a binomial
random variable. We want to find maximum rate at which
we can send information over this sequence of channels with
vanishingly small error probability.

The following characterization follows from [3].
Theorem II.1. Let

Cn =
1

n
max
pXn

I(Xn;Y (Xn)) . (2)
Then, the following limit exists

C = lim
n??

Cn = inf
n?1

Cn , (3)
and is equal to the capacity of the deletion channel.

Proof: This is just a reformulation of Theorem 1 in [3],
to which we add the remark C = infn?1 Cn, which is of
independent interest. In order to prove this fact, consider the
channel Wm+n, and let Xm+n = (Xm1 , Xm+nm+1 ) be its input.
The channel Wm+n can be realized as follows. First the input
is passed through a channel W�m+n that introduces deletions
independently in the two strings Xm1 and Xm+nm+1 and outputs
Y� (Xm+n1 ) ? (Y (X

m
1 ), |, Y (X

m+n
m+1 )) where | is a marker.

Then the marker is removed.
This construction proves that Wm+n is physically degraded

with respect to W�m+n, whence

(m+ n)Cm+n ? max
p
Xm+n

I(Xm+n; Y� (Xm+n1 ))

? mCm + nCn .

Here the last inequality follows from the fact that W�m+n is
the product of two independent channels, and hence the mutual
information is maximized by a product input distribution.

Therefore the sequence {nCn}n?1 is sub-additive, and the
claim follows from Fekete�s lemma.

A last useful remark is that, in computing capacity, we can
assume (X1, . . . , Xn) to be n consecutive coordinates of a
stationary ergodic process.

Lemma II.2. Let X = {Xi}i?Z be a stationary and ergodic
process, with Xi taking values in {0, 1}. Then the limit I(X) =
limn??

1
nI(X

n;Y (Xn)) exists and
C = max

X stat. erg.
I(X) . (4)

Proof: Take any stationary X, and let In =
I(Xn;Y (Xn)). Notice that Y (Xn1 ) ? Xn1 ? Xn+mn+1 ?
Y (Xn+mn+1 ) form a Markov chain. Define Y� (Xn+m) as
in the proof of Theorem II.1. As before we have
In+m ? I(X

n+m, Y� (Xn+m)) ? I(Xm1 ; Y� (X
m
1 )) +

I(Xm+nm+1 ;Y (X
m+n
m+1 )) = Im + In. (the last identity follows

by stationarity of X). Thus Im+n ? In + Im and the limit
limn?? In/n exists by Fekete�s lemma, and is equal to
infn?1 In/n.

Clearly, In ? Cn for all n. Fix any ? > 0. We will construct
a process X such that

IN/N ? C ? ? ? N > N0(?) , (5)
thus proving our claim.

Fix n such that Cn ? C??/2. Construct X with iid blocks
of length n with common distribution p?(n) that achieves
the supremum in the definition of Cn. In order to make this
process stationary, we make the first complete block to the
right of the position 0 start at position s uniformly random in
{1, 2, . . . , n}. We call the position s the offset. The resulting
process is clearly stationary and ergodic.

Now consider N = kn + r for some k ? N and
r ? {0, 1, . . . , n? 1}. The vector XN1 contains at least k ? 1
complete blocks of size n, call them X(1), X(2), . . . , X(k?1)
with X(i) ? p?(n). The block X(1) starts at position s. There
will be further r + n ? s + 1 bits at the end, so that XN1 =
(Xs?11 , X(1), X(2), . . . , X(k?1), X

N
s+kn). Abusing notation,

we write Y (i) for Y (X(i)). Given the output Y , we define
Y� = (Y (Xs?11 )|Y (1)|Y (2)| . . . |Y (k? 1)|Y (X

N
s+(k?1)n)), by

introducing k synchronization symbols |. There are at most
(n+1)k possibilities for Y� given Y (corresponding to potential
placements of synchronization symbols). Therefore we have

H(Y ) = H(Y� )?H(Y� |Y )

? H(Y� )? log((n+ 1)k)

? (k ? 1)H(Y (1))? k log(n+ 1) ,

where we used the fact that the (X(i), Y (i))�s are iid. Further

H(Y |XN ) ? H(Y� |XN ) ? (k ? 1)H(Y (1)|X(1)) + 2n ,

where the last term accounts for bits outside the blocks. We
conclude that

I(XN ;Y (XN )) = H(Y )?H(Y |XN )

? (k ? 1)nCn ? k log(n+ 1)? 2n

? N(Cn ? ?/2) ,

provided log(n + 1)/n < ?/10, N > N0 ? 10n/?. Since
Cn ? C ? ?/2, this in turn implies Eq. (5).

III. PROOF OF THE MAIN THEOREM: OUTLINE
In this section we provide the proof of Theorem I.1. We

defer the proof of several technical lemmas to the next section.
The first step consists in proving achievability by estimating

I(X) for the iid Bernoulli(1/2) process.



Lemma III.1. Let X? be the iid Bernoulli(1/2) process. For
any ? > 0, we have

I(X?) = 1 + d log d?A1 d+O(d
2??) . (6)

Lemma II.2 allows us to restrict our attention to stationary
ergodic processes in proving the converse. In light of Lemma
III.1, we can further restrict consideration to processes X
satisfying I(X) > 1+2d log d and hence H(X) > 1+2d log d
(here and below, for a process X, we denote by H(X) its
entropy rate).

Given a (possibly infinite) binary sequence, a run of 0�s (of
1�s) is a maximal subsequence of consecutive 0�s (1�s), i.e.
an subsequence of 0�s bordered by 1�s (respectively, of 1�s
bordered by 0�s). Denote by S the set of all stationary ergodic
processes and by SL the set of stationary ergodic processes
such that, with probability one, no run has length larger than L.
The next lemma shows that we don�t lose much by restricting
ourselves to SL? for large enough L?.

Lemma III.2. For any ? > 0 there exists d0 = d0(?) > 0 such
that the following happens for all d < d0. For any X ? S such
that H(X) > 1 + 2d log d and for any L? > log(1/d), there
exists XL? ? SL? such that

I(X) ? I(XL?) + d
1/2??(L?)?1 logL? . (7)

We are left with the problem of bounding I(X) from above
for all X ? SL? . The next lemma establishes such a bound.

Lemma III.3. For any ? > 0 there exists d0 = d0(?) > 0 such
that the following happens. For any L? ? N and any X ? SL?
if d < d0(?), then

I(X) ? 1 + d log d?A1d+ d
2??(1 + d1/2L?) . (8)

Proof of Theorem I.1: Lemma III.1 shows achievability.
The converse follows from Lemmas III.2 and III.3 with L? =
b1/dc.

IV. PROOFS OF THE LEMMAS
In Section IV-A we characterize any stationary ergodic X in

terms of its �bit perspective� and �block perspective� run-length
distributions, and show that these distributions must be close
to the distributions obtained for the iid Bernoulli(1/2) process.
In Section IV-B we construct a modified deletion process that
allows accurate estimation of H(Y |Xn) in the small d limit.
Finally, in Section IV-C we present proofs of the Lemmas
quoted in Section III using the tools developed.

We will often write Xba for the random vector
(Xa, Xa+1, . . . , Xb) where the Xi�s are distributed according
to the process X.

A. Characterization in terms of runs
Consider a stationary ergodic process X. Without loss of

generality we can assume that almost surely all runs have finite
length (by ergodicity and stationarity this only excludes the
constant 0 and constant 1 processes). Let L0 be the length of
the run containing position 0 in X. Let L1 be the length of first
run to occur to the right of position 0 in X and, in general,

let Li be the length of the i-th run to the right of position
0. Let pL,X denote the limit of the empirical distribution of
L1, L2, . . . , LK , as K ? ?. By ergodicity pL,X is a well
defined probability distribution on N. We call pL,X the block-
perspective run length distribution for obvious reasons, and
use L to denote a random variable drawn according to pL,X.

It is not hard to see that, for any l ? 1,

P(L0 = l) =
lpL,X(l)

E[L]
. (9)

In other words L0 is distributed according to the size biased
version of pL,X. We call this the bit perspective run length
distribution, and shall often drop the subscript X when clear
from the context. Notice that since L0 is a well defined and
almost surely finite, we have E[L] < ?. It follows that the
empirical distribution of run lengths in Xn1 also converges to
pL,X almost surely, since the first and last run do not matter
in the limit.

If L+0 , L1, . . . , LK are the run lengths in the block Xn0 , it
is clear that H(Xn0 ) ? 1 +H(L1, . . . , LKn ,Kn) (where one
bit is needed to remove the 0, 1 ambiguity). By ergodicity
Kn/n? 1/E[L] almost surely as n??. This also implies
H(Kn)/n ? 0. Further, lim supn??H(L1, . . . , LKn)/n ?
limn??H(L)Kn/n = H(L)/E[L]. If H(X) is the entropy
rate of the process X, by taking the n ? ? limit, it is easy
to deduce that

H(X) ?
H(L)

E[L]
, (10)

with equality if and only if X consists of iid runs with common
distribution pL.

For convenience of notation, define �(X) ? E[L]. We know
that given E[L] = �, the probability distribution with largest
possible entropy H(L) is geometric with mean �, i.e. pL(l) =
(1? 1/�)l?11/� for all l ? 1, leading to

H(L)

E[L]
? ?

(
1?

1

�

)
log
(
1?

1

�

)
?

1

�
log

1

�
? h(1/�) .

(11)
Here we introduced the notation h(p) = ?p log p ? (1 ?
p) log(1? p) for the binary entropy function.

In light of Lemma III.1 we can restrict ourselves to H(X) >
1 + 2 d log d. Using this, we are able to obtain sharp bounds
on pL and �(X).

Lemma IV.1. There exists d0 > 0 such that, for any X ? S
with H(X) > 1 + 2d log d,

|�(X)? 2| ?
?
100 d log(1/d) . (12)

for all d < d0.
Proof: By Eqs. (10) and (11), we have h(1/�) ? 1 +

2d log d. By Pinsker�s inequality h(p) ? 1?(1?2p)2/(2 ln 2),
and therefore |1 ? (2/�)|2 ? (4 ln 2)d log(1/d). The claim
follows from simple calculus.



Lemma IV.2. There exists K ? <? and d0 > 0 such that, for
any X ? S with H(X) > 1 + 2d log d, and any d < d0,

??
l=1

????pL(l)? 12l
???? ? K ?

?
d log(1/d) . (13)

Proof: Let p?L(l) = 1/2l, l ? 1 and recall that �(X) =
E[L] =

?
l?1 pL(l)l. An explicit calculation yields

H(pL) = �(X)?D(pL||p
?
L) . (14)

Now, by Pinsker�s inequality,

D(pL||p
?
L) ?

2

ln 2
||pL ? p

?
L||

2
TV . (15)

Combining Lemma IV.1, and Eqs. (10), (14) and (15), we get
the desired result.

Lemma IV.3. There exists K ?? < ? and d0 > 0 such that,
for any X ? S with H(X) > 1 + 2d log d, and any d < d0,

??
l=1

????P(L0 = l)? l2l+1
???? ? K ??

?
d(log(1/d))3 . (16)

Proof: Let l0 = b? log(K ?
?
d log(1/d))c. It follows

from Lemma IV.2 that
l0?
l=1

????pL(l)? 12l
???? ? K ?

?
d log(1/d) , (17)

which in turn implies
l0?
l=0

lpL(l) ?

l0?1?
l=0

l

2l
. (18)

Summing the geometric series, we find that there exists a
constant K1 <? such that

??
l=l0

l

2l
= (l0 + 1)2

1?l0 ? K1
?
d(log(1/d))3 . (19)

Using the identity
??

l=0 l 2
?l = 2, together with Eqs. (18)

and (19), we get
l0?
l=0

lpL(l) ? 2?K1
?
d(log(1/d))3 . (20)

Combining this result with Lemma IV.1, we conclude (even-
tually enlarging the constant K1)

??
l=l0+1

lpL(l) ? 2K1
?
d(log(1/d))3 . (21)

Using this result together with Eq. (19), we get
??

l=l0+1

|lpL(l)?
l

2l
| ? 4K1

?
d(log(1/d))3 . (22)

From a direct application of Lemma IV.2 it follows that
there exists a constant K2 <?, such that

l0?
l=1

???lpL(l)? l
2l

??? ? K2?d(log(1/d))3 . (23)

and therefore summing Eqs. (23) and (22)
??
l=1

??? lpL(l)
2

?
l

2l+1

??? ? 2(K1 +K2)?d(log(1/d))3 . (24)

We know that P(L0 = l) = lpL(l)/�(X). The proof is
completed by using Eq. (24) and bounding �(X) with the
Lemma IV.1.

B. A modified deletion process
We define an auxiliary sequence of channels W?n whose

output �denoted by Y? (Xn)� is obtained by modifying the
deletion channel output in the following way. If an �extended
run� (i.e. a run R along with one additional bit at each end
of R) undergoes more than one deletion under the deletion
channel, then R will experience no deletion in channel W?n,
i.e. the corresponding bits are present in Y? (Xn). Note that
(deletions in) the additional bits at the ends are not affected.

Formally, we construct this sequence of channels as follows
when the input is a stationary process X. Let D be an iid
Bernoulli(d) process, independent of X, with Dn1 being the
n-bit vector that contains a 1 if and only if the corresponding
bit in Xn is deleted by the channel Wn. We define D?(D,X) to
be the process containing a subset of the 1s in D. The process
D? is obtained by deterministically flipping some of the 1s in D
as described above, simultaneously for all runs. The output of
the channel W?n is simply defined by deleting from Xn those
bits whose positions correspond to 1s in D?.

Notice that (X,D, D?) are jointly stationary. The sequence
of channels Wn are defined by D, and the coupled sequence
of channels W?n are defined by D?. We emphasize that D? is a
function of (X,D). Let Z ? D?D? (where? is componentwise
sum modulo 2). The process Z is stationary with P(Z0 = 1) ?
z = E[d ? d(1 ? d)L0+1] ? 2 d2 E[L0]. Note that z = O(d2)
for E[L0] = O(1).

The following lemma shows the utility of the modified
deletion process.

Lemma IV.4. Consider any X ? S such that E[L0 logL0] <
?. Then

lim
n??

1

n
H(D?n|Xn, Y? n) = dE[logL0]? ? , (25)

where 0 ? ? = ?(d,X) ? 2d2E[L0 logL0].

Proof: Fix a channel input xn and any possible output
y? = y?(xn) (i.e. an output that occurs with positive probability
under W?n). The proof consists in estimating (the logarithm
of) the number of realizations of D?n that might lead to the
input/ouput pair (xn, y?), and then taking the expectation over
(xn, y?).

Proceeding from left to right, and using the constraint on
D?, we can map unambiguously each run in y? to one or more
runs in xn, that gave rise to it through the deletion process.
Consider a run of length ` in y?. If there is a unique �parent�
run, it must have length ` or `+1. If the length of the parent
run is `, then no deletion occurred in this run, and hence
the contribution to H(D?n|xn, y?) of such runs vanishes. If the



length of the parent run is `+ 1, one bit was deleted by W?n
and each of the `+1 possibilities is equally likely, leading to
a contribution log(` + 1) to H(D?n|xn, y?).

Finally, if there are multiple parent runs of lengths
l1, l2, . . . , lk, they must be separated by single bits of taking
the opposite value in xn, all of which were deleted. It also
must be the case that

?k
i=1 li = ` i.e. there is no ambiguity

in D?n. This also implies l1 < `.
Notice that the three cases described corresponds to three

different lengths for the run in y?. This allows us to sequentially
associate runs in y? with runs in xn, as claimed.

By the above argument, H(D?n|xn, y?n) =
?

r?D log(`r)
where D is the set of runs on which deletions did occur, and
`r are their lengths. Using the definition of D?, the sum can
be expressed as

?n
i=1 D?i log(`(i)), with `(i) the length of the

run containing the i-th bit. Using the definition of D?, we get
P(D?i = 1) = d(1?d)

`(i)+1 ? (d? (`(i)+1)d
2, d) (except for

the last and first block in xn, that can be disregarded). Taking
expectation and letting n?? we get the claim.

Corollary IV.5. Under the assumptions of the last Lemma,
and denoting by h(p) the binary entropy function, we have

lim
n??

1

n
H(Y (Xn)|Xn) = h(d)? dE[logL0] + ? ,

where ?2h(z) ? ? = ?(d,X) ? 2d2E[L0 logL0] + 2h(z) and
z = d? E[d(1 ? d)L0+1].

Proof: By definition, Dn is independent of Xn. We have,
for Y = Y (Xn),

H(Y |Xn) = H(Dn|Xn)?H(Dn|Xn, Y )

= nh(d)?H(D?n|Xn, Y? ) + n?1 ,

with |?1(d,X)| ? 2H(Zn)/n ? 2h(z). In the second equality
we used the fact that the pairs ((Xn, Y,Dn), (Xn, Y? , D?n))
and ((Xn, Y ), (Xn, Y? )) are both of the form (A,B) such that
A is a function of (B,Zn) and B is a function of (A,Zn),
? |H(A)?H(B)| ? H(Zn).

C. Proofs of Lemmas III.1, III.2 and III.3
Proof of Lemma III.1: Clearly, X? has run length

distribution pL(l) = 2?l, l ? 1. Moreover, Y (X?,n) is also
a iid Bernoulli(1/2) string of length ? Binomial(n, 1 ? d).
Hence, H(Y ) = n(1?d)+O(log n). We now use the estimate
of H(Y |X?,n) from Corollary IV.5. We have z = O(d2) and
E[L0 logL0] <?, leading to

H(Y |X?,n) = n(h(d)? dE[logL0] +O(d
2??)) + o(n) .

Computing H(Y )?H(Y |X?,n), we get the claim.
Proof of Lemma III.2: We construct XL? by flipping a

bit each time it is the (L? + 1)-th consecutive bit with the
same value (either 0 or 1). The density of such bits in X
is upper bounded by ? = P(L0 > L?)/L?. The expected
fraction of bits in the channel output YL? = Y (XnL?) that
have been flipped relative to Y = Y (Xn) (output of the same
channel realization with different input) is also at most ?. Let
F = F (X,D) be the binary vector having the same length as

Y , with a 1 wherever the corresponding bit in YL? is flipped
relative to Y , and 0s elsewhere. The expected fraction of 1�s
in F is at most ?. Therefore

H(F ) ? n(1? d)h(?) + log(n+ 1) . (26)
Notice that Y is a deterministic function of (YL? , F ) and YL?
is a deterministic function of (Y, F ), whence

|H(Y )?H(YL?)| ? H(F ) . (27)
Further, X?XL??XnL??YL? form a Markov chain, and XL? ,
XnL? are deterministic functions of X. Hence, H(YL? |XnL?) =
H(YL? |X). Similarly, H(Y |Xn) = H(Y |X). Therefore (the
second step is analogous to Eq. (27))

|H(YL? |X
n
L?)?H(Y |X

n)| = (28)
= |H(YL? |X)?H(Y |X)| ? H(F ) .

It follows from Lemma IV.3 and L? > log(1/d) that
? ? 2K ??

?
d(log(1/d))3/L? for sufficiently small d. Hence,

h(?) ? d1/2?? logL?/(2L?) for d < d0(?), for some
d0(?) > 0. The result follows by combining Eqs. (26), (27)
and (28) to bound |I(X)? I(XL?)|.

Proof of Lemma III.3: If H(X) ? 1 + 2d log d, we
are done. Else we proceed as follows. We know that Y (Xn)
contains Binomial(n, 1? d) bits, leading immediately to

H(Y ) ? n(1? d) + log(n+ 1) . (29)
We use the lower bound on H(Y |Xn) from Corollary IV.5. We
have z ? 2d2E[L0]. It follows from Lemma IV.3 that E[L0] ?
K1(1 +

?
d(log(1/d))3L?), leading to h(z) ? .5d2??(1 +

(1/2)d1/2L?) for all d < d0, where d0 = d0(?) > 0. Thus,
we have the bound

lim
n??

1

n
H(Y |Xn) ? h(d)? dE[logL0]? d

2??(1 + .5d1/2L?)

Using Lemma IV.3, we have |E[logL0]?
??

l=1 2
?l?1l log l| =

o(d(1/2)?? logL?). The result follows.

Acknowledgments. Y. Kanoria is supported by a 3Com
Corporation Stanford Graduate Fellowship. Y. Kanoria and A.
Montanari were supported by NSF, grants CCF-0743978 and
CCF-0915145, and a Terman fellowship.

REFERENCES
[1] M. Mitzenmacher, �A survey of results for deletion channels and related

synchronization channels,� Probab. Surveys, 6 (2009), 1-33
[2] Y. Kanoria and A. Montanari, �Optimal coding for the deletion channel

with small deletion probability,� journal version in preparation (2010)
[3] R. L. Dobrushin, �Shannon�s Theorems for Channels with Synchroniza-

tion Errors,� Problemy Peredachi Informatsii, 3 (1967), 18-36
[4] A. Kirsch and E. Drinea, �Directly Lower Bounding the Information

Capacity for Channels with I.I.D. Deletions and Duplications,� Proc. of
2007 IEEE Intl. Symp. on Inform. Theory (ISIT) 2007

[5] E. Drinea and M. Mitzenmacher, �Improved lower bounds for the
capacity of i.i.d. deletion and duplication channels,� IEEE Trans. Inform.
Theory, 53 (2007) 2693-2714

[6] D. Fertonani and T.M. Duman, �Novel bounds on the capacity of binary
channels with deletions and substitutions,� Proc. of 2009 IEEE Intl.
Symp. on Inform. Theory (ISIT) 2009

[7] A. Kalai, M. Mitzenmacher and M. Sudan, �Tight Asymptotic Bounds
for the Deletion Channel with Small Deletion Probabilities�, preprint,
December 23, 2009


	I Introduction
	I-A Related work

	II Preliminaries
	III Proof of the main theorem: Outline
	IV Proofs of the Lemmas
	IV-A Characterization in terms of runs
	IV-B A modified deletion process
	IV-C Proofs of Lemmas III.1, III.2 and III.3

	References

