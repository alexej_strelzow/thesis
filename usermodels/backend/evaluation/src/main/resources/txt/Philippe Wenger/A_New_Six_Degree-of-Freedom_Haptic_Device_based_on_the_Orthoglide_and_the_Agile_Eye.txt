
Proceedings of Virtual Concept 2005 
Biarritz, France, November 8th � November 10th, 2005 

Paper Number -1- Copyright Virtual Concept 

A New Six Degree-of-Freedom Haptic Device  
based on the Orthoglide and the Agile Eye  

Chablat D., Wenger P. 

 Institut de Recherche en Communications et Cybern�tique de Nantes 
1, rue de la No�, 44321 Nantes, France 
Phone : 0240376948 Fax : 0240376930 

E-mail : {Damien.Chablat, Philippe.Wenger }@irccyn.ec-nantes.fr 
 

Abstract: The aim of this paper is to present a new six 
degree-of-freedom (dof) haptic device using two parallel 
mechanisms. The first one, called orthoglide, provides the 
translation motions and the second one produces the rotational 
motions. These two motions are decoupled to simplify the 
direct and inverse kinematics, as it is needed for real-times 
control. To reduce the inertial load, the motors are fixed on the 
base and a transmission with two universal joints is used to 
transmit the rotational motions from the base to the end-
effector. The main feature of the orthoglide and of the agile eye 
mechanism is the existence of an isotropic configuration. The 
length of the legs and the range limits of the orthoglide are 
optimized to have homogeneous performance throughout the 
Cartesian workspace, which has a nearly cubic workspace. 
These properties permit to have a high stiffness throughout the 
workspace and workspace limits that are easily understandable 
by the user. 

Key words: Haptic device, interface, parallel mechanism, 
orthoglide, agile eye. 

1- Introduction 

The aim of this paper is to introduce a new haptic device that 
can be used as a new interface in a Virtual Reality system. 
Virtual Reality, characterized by real-time simulation, allows a 
human to interact with a Virtual 3D world and to understand its 
behavior by sensory feedback [1]. Associated with this 
definition, four key elements are usually considered: (i) virtual 
world, (ii) immersion (of human), (iii) interactivity, and (iv) 
sensory feedback. Most often, Virtual Reality systems provide 
only visual sensory feedback to the user (mono or stereo 
vision). However, aural or haptic interfaces exist that allow the 
user to hear or to feel the virtual environment. We want our 
device to be able to feel the virtual environment.  
The haptic feedback is associated with the sense of touch. For a 
user, the existence of an object in a virtual world is verified by 
coming into physical contact or touch. Usually, a haptic device 
enables input and output communication between the computer 
and the user.  
Haptic devices play important roles in the recognition of 

virtual objects. With these devices, the user can feel the 
rigidity or weight of virtual objects. Most robotic 
manipulators have large-scale and high-cost hardware, which 
inhibits their application to human-computer interaction.  
Our device was specifically developed for desktop use. It 
provides haptic feedback, which strongly enhances human 
capabilities in the major application areas of virtual reality, 
such as scientific visualization and 3D shape modeling. 
The six-dof Orthoglide features two parallel mechanisms 
mounted serially with a suitable system, which makes it 
possible to know and control the orientation of a stylus, even 
though all the motors are fixed. These features contribute to a 
significant reduction of the inertia when compared to serial 
architectures commonly used in haptic devices [2]. Thus, the 
inertia of the manipulator's moving parts is so small that 
compensation is not needed. This hardware design is 
compact, and it has the ability to carry a relatively large 
payload. Furthermore, the parallel kinematic architecture 
increases drastically the stiffness of the structure. 

2- State of the art of haptic devices 

Basically, haptic devices that enable the user to touch an 
object can be classified into two main types: the first one 
provides only one single point of contact and the second one 
provides a single point of contact with a torque or multiple 
points of contact. In the next subsection, we will characterize 
their properties and introduce several examples.  

2.1- Single point of contact 

Many haptic devices have only three degrees of freedom, 
such as the Phantom Desktop [2] depicted in Fig. 1. With 
such a device, the user drives a stylus in space and can feel a 
single point of contact with an object. The force display 
provides stimuli to fingertip but no torque is provided.  



Virtual Concept 2005 A New 6DOF Haptic Device based on the Orthoglide and Agile Eye 
 

Paper Number -2- Copyright Virtual Concept 
 

 

Figure 1: The Phantom Desktop provides one single point of 
contact to the user 

To provide high stiffness and a low inertia, parallel 
mechanisms can be also experimented as depicted in Fig. 2. 
This design has been optimized to avoid the singular 
configuration inside the workspace [3]. 

 

Figure 2: The �syntaxeur� from ONERA/CERT [3] 

2.2- Single point of contact with torque or multiple 
points of contact 

With a three-dof haptic device, the user can only manage the 
motion of one point. However, in virtual environment, he may 
also want to handle objects. For example, when one assembles 
a screw in a hole, it is necessary to produce a feedback by 
forces and torques to feel the contact. This feature is related to 
the problem of grasping.  
Only a six-dof devices can provide forces and torques in the 
same times and there are three types of mechanisms: 
-Serial: The main problem of the serial type is the lack of 
stiffness. Therefore, the position and the rotation are coupled. 
An example of the first type, depicted in Fig. 3(a), was 
originally designed at McGill University [4] and is now 
commercialized by MPB [5]. The Phantom Premium six-dof 
devices is also of the serial type but with a closed loop in the 
serial chain [2].  
-Parallel: An example of this type, depicted in Fig. 3(b), was 
designed at Tsukuba University [6]. The main advantage of 
this device is that all the actuators are fixed on the base. 
However, its rotation motions are limited and are function of 
the position. The computation of the inverse and direct 
kinematic models is not simple, which is not compatible with 
real time applications.  
-Hybrid: An example of this type, depicted in Fig. 3(c), was 
designed at �cole F�d�rale de Lausanne and is now 
commercialized by [7]. A parallel architecture provides high 
stiffness but the shape of its Cartesian workspace is generally 
not simple and few of them have an isotropic configuration. 

  
(a) (b) 

 

 

(c) 
Figure 3: Three haptic devices with six-dof with (a) serial 

architecture, (b) parallel architecture and (c) hybrid 
architecture  

There is also a haptic device [8] made of a Delta [9] a two-
dof agile eye [10] mounted serially with a revolute joint. 
Such a design enables to decouple the translation motions 
and the rotation motions. The kinematics models are simple 
but its main drawbacks are the actuated joints of the wrist, 
which increase the inertia in motion and its volume. The aim 
of our paper is to propose a design with the same advantage 
but without these drawbacks.  

 

Figure 4: A six-dof haptic device from Tohoku University 

3- Constraints to design a new haptic interface 

The main desirable characteristics are quick motion and a 
regular workspace shape. In addition, compactness is also 
desirable, since the haptic interface should be treated in the 
same way as a conventional computer mouse. Quick motions 
could be achieved with a parallel mechanism since the mass 
of the end-effecter can be reduced [11]. 



Virtual Concept 2005 A New 6DOF Haptic Device based on the Orthoglide and Agile Eye 
 

Paper Number -3- Copyright Virtual Concept 
 

It is possible to built a compact haptic device with a six-dof 
parallel mechanism, as shown by Long [12]. But in general, 
fully six-dof parallel mechanisms (e.g. Stewart Platform [13], 
HEXA [14]) have a restricted orientation workspace. To cope 
with this problem, it is possible to mount a spherical wrist on a 
translating mechanism. 
Without a force/torque sensor in the haptic interface, a fully 
isotropic mechanism would be needed to ensure uniform force 
controllability in each direction. Unfortunately, six-dof 
isotropic mechanisms do not exist because isotropy cannot be 
defined due to the non-homogeneity of the jacobian matrix 
[16]. Conversely, using a force/torque sensor would increase 
the inertia and the volume of the device.  
In general, the size of the workspace of haptic devices is 
relatively small compared to the size of the simulated virtual 
environment. Thus, there are several problems relating to the 
correspondence between displacements in these two spaces as 
scale factor, reachable space and directions of the translations 
or senses of rotations. To simplify these problems, the shape of 
the workspace of haptic devices must be close to a cube 
because its projection of the plan of view is the screen of the 
computer. 
To summarize these constraints, we can define an ideal haptic 
interface with the following constraints: 
� To be able to display motions in 6-dof, 
� To be able to realize quick motion (low inertia), 
� To have a regular dextrous workspace like a cube 
� To be compact 
Other design constraints, taking into account the control and 
the software can be found in Hayward [17]. 

3.1- Fully isotropic mechanisms 

A three-dof fully isotropic parallel mechanism was defined in 
[18, 19, 20]. This device was firstly designed for milling 
applications. The linear joints can be actuated by means of 
linear motors or by conventional rotary motors with ball 
screws. The output body is connected to the linear joints 
through a set of three RRR identical chains where R stands 
Revolute joints.  
Figure 5 depicts an example of such a mechanism where the 
three prismatic joints c are orthogonal, and the three joint axes 
d, e and f, are parallel. Its workspace is defined by the 
intersection of three cylinders and no singular configurations 
exist inside. 
We could use this mechanism as a haptic device with three-dof 
because full isotropy is clearly an outstanding property. 
Unfortunately, bulky legs are required to assure stiffness 
because these legs are subject to bending. To increase the 
stiffness means to increase the inertia. Moreover, it would very 
difficult to mount a wrist on this mechanism and to actuate if 
from fixed motors. Another solution would be to add one or 
two legs [21, 23] but mechanical interferences would reduce 
the size of the workspace. 

c 

d 

e 

f 

g 

 

Figure 5: A fully isotropic parallel mechanism 

3.2- Orthoglide 

The Orthoglide is part of the Delta linear family. It was 
optimized to have an isotropic configuration. Thus, its leg 
lengths as well as its range limits were also defined to have 
homogeneous performances throughout its Cartesian 
workspace [23]. The criterion used was related the Jacobian 
matrix and made it possible to characterize the velocity 
factor amplifications. The resulting machine, the Orthoglide, 
features three fixed parallel linear joints, which are mounted 
orthogonally and a mobile platform, which moves in the 
Cartesian x-y-z space with fixed orientation. The interesting 
features of the Orthoglide are a regular Cartesian workspace 
shape and good compactness. 

B1

i1

P
x

z
y

j1
A1

C1

A2

B2

C2

A3

C3

B3

 

Figure 6: Orthoglide 

The three legs are PRPaR identical chains, where P, R and 
Pa stands for Prismatic, Revolute and Parallelogram joint, 
respectively. The arrangement of the joints in the PRPaR 
chains was defined to eliminate any special singularity [24]. 
Each base point Ai is fixed on the ith linear axis such that 
A1A2 = A1A3 = A2A3. The points Bi and Ci are located on the 
ith parallelogram as shown in Fig. 7. 



Virtual Concept 2005 A New 6DOF Haptic Device based on the Orthoglide and Agile Eye 
 

Paper Number -4- Copyright Virtual Concept 
 

ii

ji

ri

P

?i
?i

Bi

Ai

Ci
L

 

Figure 7: Legs kinematics of the Orthoglide 

3.3- Wrist mechanism 

The architecture of the wrist can be either serial or parallel. In 
some haptic devices, the actuators are fixed to the base and the 
motion is transmitted to the end-effector by using cable. In 
other cases, the motors are embedded.  
To have high stiffness, the parallel architectures are suitable 
but it suffers from a limited workspace. An exhaustive study of 
such mechanisms can be found in [25]. Some of them are able 
to have a fixed center point in the same loci of the center of the 
user hand. Moreover, particular designs can provide unlimited 
rotations about this central point, allowing very large 
workspace, limited only by the low-scaled dexterity and 
mechanical interferences. An example of such a device has 
been tested at Laval University [26].  

 

Figure 8: The ShaDe device  

A parallel wrist with actuation redundancy can be employed to 
avoid the singularities [15]. An example of such a device is 
depicted in Fig. 9. But it would be very difficult to actuate this 
device with four motors fixed on the base of a translating 
mechanism. 

Two coincident
motors 

Two coincident
motors 

 

Figure 9: Redundant spherical wrist 

An hybrid architecture derived from the two-dof agile eye is 
depicted in Fig. 10. It is the same one that is used in [8]. Its 
main property is that there are unlimited motions about its last 
joint. Unfortunately, there is no way to place the last motors on 
the base.  

c
d 

e 

 

Figure 10: The hybrid agile eye with two-dof in serial with a 
revolute actuated joint e 

The kinematic behavior of this mechanism is good because 
there is a set of orientations where the Jacobian matrix is 
isotropic as shown in Fig. 10. In the home posture, the joint 
axis e is orthogonal to the joint axes c and d. 

3.4- Design of the legs 

The aim of our design is to find a mechanism that allows us 
to avoid to carry the motors of the wrist and to reduce its 
volume. 
We have chosen to use the agile eye with two or three dofs 
because with such a mechanism we can easily achieve the 
orientation range needed for haptic devices. We can also 
notice that this design can be simplified to have a six-dof 
mechanism with only three-dof haptic feedback.  
The kinematics of the new leg is depicted in Fig. 11. On the 
neutral fiber of a leg, a mechanism with two universal joints 
is used to transmit the rotary motion from the base of the leg 
to the spherical wrist. From the kinematics of the Orthoglide 
depicted in Fig. 7, we have added a revolute actuated joint c 
fixed on the base followed by a prismatic joint and a revolute 
joint, called d and e, respectively. This former is carried by 
the prismatic actuated joints. Two universal joints f, located 
on Bi and Ci, are placed on the neutral fiber of the 
parallelogram and finally, a revolute joint g allows us to 
mount the agile eye on the previous end-effector of the 
Orthoglide. 

ii

ji

?i

?i
?i

Bi

Ai

Ci
L

c 
d 
e 

f 

g  
Figure 11: New design for the legs of the Orthoglide 



Virtual Concept 2005 A New 6DOF Haptic Device based on the Orthoglide and Agile Eye 
 

Paper Number -5- Copyright Virtual Concept 
 

To have a three-dof agile eye, all the legs are changed and to 
have a two-dof agile eye, only two are. 

3.5- New haptic device 

The main problem to design a six-dof haptic device is to 
couple the wrist mechanism and the position mechanism. With 
the leg design, we can produce two types of mechanisms using 
either a three-dof parallel spherical wrist or a hybrid spherical 
wrist based on the two-dof agile eye. We use the fact that on 
connection point of the three legs, the joint axes g in Fig. 11 
are orthogonal. Such a feature allows us to assemble the two 
versions of the agile eye for which there are isotropic 
configurations.  
To compare the two designs, it is necessary to recall the shape 
of the Cartesian workspace of the Orthoglide as it is depicted 
in Fig. 12. The largest cubic workspace is reached whenever 
the faces of the cube are parallel to the xy, yz and zx plane, 
respectively. 

x

z

y

 

Figure 12: The workspace of the orthoglide and  
the largest cubic workspace included inside 

Figures 13 and 14 depict two new haptic devices. In the first 
one, it is made by the assembly the orthoglide and the three-dof 
agile eye (3T+3R). Thus, no revolute actuated joints are 
embedded. An isotropic configuration exists whenever the 
three legs are orthogonal and the last axis of the wrist is along 
the axis equal to x = y = z, i.e. on the diagonal of the cubic 
workspace defined in Fig. 12. This feature can be a drawback 
for the user because the home posture of the wrist does not lie 
on the axis of the cubic workspace. 

Three-dof agile eye
Linear actuators

Transmission

Wrist

 

 

Figure 13: A six-dof haptic device 3T+3R 

We propose a second mechanism in Fig. 14 which is made of 
the assembly of the orthoglide, the two-dof agile eye and 
finally a revolute actuated joint (3T+2R+1R). This device 
has an isotropic configuration for the translation and the 
rotation part whenever the three legs are orthogonal and the 
last axis of the wrist is collinear with z-direction. In this case, 
one revolute actuated joint is embedded. If this feature 
increases the inertia, it provides unlimited motions about this 
axis. The wrist is able to produce pitching and yaw equal to 
� 45 degrees and unlimited rolling. 
The two architectures have been firstly designed for milling 
machine applications and a patent is currently requested [27]. 

4- Conclusions 

In this paper, two haptic devices with six-dof have been 
defined. The first one uses two fully parallel mechanisms 
(3T+3R) and the second one two fully parallel mechanims 
(3T+2R+1R) in serial with a terminal joint. In both cases, a 
specific transmission is used to avoid carrying the motors of 
the wrist and to reduce its volume. The volume and the shape 
of the translation workspace were compared with the rotation 
workspace to have the best solution.  
 



Virtual Concept 2005 A New 6DOF Haptic Device based on the Orthoglide and Agile Eye 
 

Paper Number -6- Copyright Virtual Concept 
 

Two-dof agile eyeLinear actuators

Transmission

Wrist

 
Wrist

Two-dof 
agile eye

 

Figure 14: Six-dof haptic device 3T+2R+1R 

5- References 

[1] Sherman W. R. and Craig A. B., �Understanding Virtual 
Reality: Interface, Application, and Design�, Morgan 
Kaufmann Publishers, 2003. 
[2] http://www.sensable.com  
[3] Leguay-Durand S. and Reboulet C., �Design of a 3-dof 
parallel translating manipulator with u-p-u joints kinematics 
chains�, Proc. Int. Conf. On Intelligent Robots and Systems, 
pp. 1637-1642, 1997. 
[4] Hayward, V., �Toward A Seven Axis Haptic Interface�, 
Proc. IROS'95, Int. Workshop on Intelligent Robots and 
Systems. IEEE Press, Vol. 2, pp. 133-139, 1995. 
[5] MPB, http://www.mpb-technologies.ca/ 
[6] Iwata, H., Yano, H. & Hashimoto, W., �LHX: An 
Integrated Software Tool For Haptic Interface�, Computers & 
Graphics,Vol.21,No.4,413-420, 1997. 
[7] http://www.forcedimension.com/ 
[8] Tsumaki, Y, Naruse, H., Nenchev, D. N. and Uchiyama M., 
�Design of a Compact 6-DOF Haptic Interface�, Proceedings 
of the 1998 IEEE International Conference on Robotics and 
Automation, Leuven, Belgium, pp. 2580-2585, 1998. 
[9] Clavel, R., �DELTA, a fast robot with parallel geometry�, 
Proceedings of the 18th International Symposium of Robotic 
Manipulators, IFR Publication, pp. 91-100, 1988. 
[10] Gosselin C., Hamel J.F., �The agile eye: a high 
performance three-degree-of-freedom camera-orienting 
device,� IEEE Int. conference on Robotics and Automation, 
pp.781-787. San Diego, 8-13 Mai, 1994. 
[11] Birglen, L., �Haptic Devices Based on Parallel 
Mechanisms. State of the Art�, in 
http://www.parallemic.org/Reviews/Review003.html 
[12] Long, G. L. and Collins, C. L., �A Pantograph Linkage 
Parallel Platform Master Hand Controller for Force-

Reflection,� Proc. 1992 IEEE Int. Conf. on Robotics and 
Automation, Nice, France, pp. 390�395, 1992. 
[13] Merlet J-P, �Parallel Robots,� Kluwer Academic 
Publishers, 2000. 
[14] Pierrot F., Uchiyama M., Dauchez P. and Fournier A., 
�A New Design of a 6-DOF Parallel Robot�, Proc. 23rd 
International Symposium on Industrial Robots, J. of Robotics 
and Mechatronics, 2-4, pp. 308-315, 1990.  
[15] Reboulet C. and Leguay S., �The Interest of 
Redundancy for the Design of a Spherical Parallel 
Manipulator�, Recent Advances on Research Kinematics, pp. 
369-378, Kluwer Academic Publishers, 1996. 
[16] Angeles, J., �Fundamentals of Robotic Mechanical 
Systems�, Springer-Verlag, New-York, 2nd edition, 2002. 
[17] Hayward, V. Astley, O.R. �Performance measures for 
haptic interfaces�. In Robotics Research: The 7th 
International Symposium, Giralt, G., Hirzinger, G., (Eds.), 
Springer Verlag. pp. 195-207, 1996.  
[18] Kong, X. and Gosselin, C.M., �Type synthesis of linear 
translational parallel manipulators�, in Lenarvic, J. and 
Thomas, F. (editors), Advances in Robot Kinematic, Kluwer 
Academic Publishers, June, pp. 453-462, 2002. 
[19] Kim, H.S. and Tsai, L.W., �Evaluation of a Cartesian 
manipulator,'' in Lenarvic, J. and Thomas, F. (editors), 
Advances in Robot Kinematic, Kluwer Academic Publishers, 
June, pp. 21-38, 2002. 
[20] Carricato, M., Parenti-Castelli, V., �Singularity-free 
fully isotropic translational parallel manipulators�, The Int. J. 
Robotics Res., Vol. 21, No. 2, pp. 161-174, 2002. 
[21] Gogu, G, �Fully-isotropic T3R1-type parallel 
manipulators�, in Lenarvic J. and Galletti C. (editors), 
Advances in Robot Kinematic, Kluwer Academic Publishers, 
June, pp. 265-272, 2004. 
[22] Chablat D. and Wenger P., �A New Concept of Modular 
Parallel Mechanism for Machining Applications�, 
Proceeding IEEE International Conference on Robotics and 
Automation, 14-19 September 2003. 
[23] Chablat D. and Wenger Ph., �Architecture Optimization 
of a 3-DOF Parallel Mechanism for Machining Applications, 
the Orthoglide�, IEEE Transactions On Robotics and 
Automation, Vol. 19/3, pp. 403-410, June, 2003. 
[24] Majou F., Wenger, P. and Chablat, D., �Design of a 3-
Axis Parallel Machine Tool for High Speed Machining: The 
Orthoglide�, 4th Int. Conf. on Integrated Design and 
Manufacturing in Mechanical Engineering, Clermont-
Ferrand, France, 2002. 
[25] Karouia, M., �Conception structurale de mecanismes 
paralleles spheriques,� Th�se de doctorat, RI 2003-26, �cole 
Centrale de Paris, 2003. 
[26] Birglen L., Gosselin C., Pouliot N., Monsarrat B. and 
Lalibert� T., 2002, SHaDe, a new 3-dof haptic device, IEEE 
Transactions on Robotics and Automation, Vol. 18, No. 2, 
pp. 166-175, 2002. 
[27] Chablat D. and Wenger P., �Dispositif de d�placement 
et d'orientation d'un objet dans l'espace et utilisation en 
usinage rapide�, Brevet fran�ais : FR2850599, demande 
PCT : WO2004071705, D�posant: Centre National de la 
Recherche Scientifique CNRS/Ecole Centrale de Nantes, 
2004. 
 


