
ar
X

iv
:0

70
5.

10
37

v1
  [

cs
.R

O]
  8

 M
ay

 20
07

November 6, 2013 5:39

Proceedings of DETC�99
1999 ASME Design Engineering Technical Conferences

September 12-15, 1999, Las Vegas, Nevada, USA

DETC99/DAC-8645

REGIONS OF FEASIBLE POINT-TO-POINT TRAJECTORIES IN THE CARTESIAN
WORKSPACE OF FULLY-PARALLEL MANIPULATORS

Damien Chablat
INRIA Rocquencourt

Domaine de Voluceau, B.P. 105
78 153 Le Chesnay France

Email: Chablat@cim.mcgill.ca

Philippe Wenger
Institut de Recherche en Cyberne�tique de Nantes

1, rue de la Noe�,
44321 Nantes, France

Email: Philippe.Wenger@lan.ec-nantes.fr

ABSTRACT
The goal of this paper is to define the n-connected re-

gions in the Cartesian workspace of fully-parallel manipulators,
i.e. the maximal regions where it is possible to execute point-
to-point motions. The manipulators considered in this study
may have multiple direct and inverse kinematic solutions. The
N-connected regions are characterized by projection, onto the
Cartesian workspace, of the connected components of the reach-
able configuration space defined in the Cartesian product of the
Cartesian space by the joint space. Generalized octree models are
used for the construction of all spaces. This study is illustrated
with a simple planar fully-parallel manipulator.

INTRODUCTION
The Cartesian workspace of fully-parallel manipulators is

generally defined as the set of all reachable configurations of the
moving platform. However, this definition is misleading since
the manipulator may not be able to move its platform between
two prescribed configurations in the Cartesian workspace. This
feature is well known in serial manipulators when the environ-
ment includes obstacles (Wenger, 91). For fully-parallel manipu-
lators, point-to-point motions may be infeasible even in obstacle-
free environments. For manipulators with one unique solution to
their inverse kinematics (like Gough-platforms), one configura-
tion of the moving platform is associated with one unique joint
configuration and the connected-components of the singularity-
free regions of the Cartesian workspace are the maximal regions
of point-to-point motions (Chablat, 98a). Unfortunately, this re-

sult does not hold for manipulators which have multiple solutions
to both their direct and inverse kinematics. For such manipula-
tors which are the subject of this study, the singularity locus in
the Cartesian workspace depends on the choice of the inverse
kinematic solution (Chablat, 97) and the actual reachable space
must be firstly defined in the Cartesian product of the Cartesian
space by the joint space. The goal of this paper is to define the
N-connected regions in the Cartesian workspace of fully-parallel
manipulators, i.e., the maximal regions where it is possible to
execute any point-to-point motion. The N-connected regions are
characterized by projection, onto the Cartesian space, of the con-
nected components of the manipulator configuration space de-
fined in the Cartesian product of the Cartesian space by the joint
space. Generalized Octree models are used for the construction
of all spaces. This study is illustrated with a simple planar fully-
parallel manipulator.

1 Preliminaries
Some useful definitions are recalled in this section.

1.1 Fully-parallel manipulators
Definition 1. A fully-parallel manipulator is a mechanism that
includes as many elementary kinematic chains as the moving
platform does admit degrees of freedom. In addition, every
elementary kinematic chain possesses only one actuated joint
(prismatic, pivot or kneecap). Besides, no segment of an ele-
mentary kinematic chain can be linked to more than two bodies
(Merlet, 97).

1 Copyright ? 1999 by ASME




In this study, kinematic chains, also called �leg� (Angeles, 97),
will be always independent.

1.2 Kinematics
The input vector q (the vector of actuated joint values) is

related to the output vector X (the vector of configuration of the
moving platform) through the following general equation :

F(X,q) = 0 (1)

Vector (X, q) will be called manipulator configuration and X
is the platform configuration and will be more simply termed
configuration. Differentiating equation (1) with respect to time
leads to the velocity model

At + Bq? = 0 (2)

With t = [w, c?]T , for planar manipulators (w is the scalar angular-
velocity and c? is the two-dimensional velocity vector of the op-
erational point of the moving platform), t = [w]T , for spherical
manipulators and t = [w, c?]T , for spatial manipulators (c? is the
three-dimensional velocity vector and w? is the three-dimensional
angular velocity-vector of the operational point of moving plat-
form).

Moreover, A and B are respectively the direct-kinematics
and the inverse-kinematics matrices of the manipulator. A sin-
gularity occurs whenever A or B, (or both) can no longer be in-
verted. Three types of singularities exist (Gosselin, 90):

det(A) = 0
det(B) = 0
det(A) = 0 and det(B) = 0

1.3 Parallel singularities
Parallel singularities occur when the determinant of the di-

rect kinematics matrix A vanishes. The corresponding singular
configurations are located inside the Cartesian workspace. They
are particularly undesirable because the manipulator can not re-
sist any effort and control is lost.

1.4 Serial singularities
Serial singularities occur when the determinant of the in-

verse kinematics matrix B vanishes. By definition, the inverse-
kinematic matrix is always diagonal: for a manipulator with n
degrees of freedom, the inverse kinematic matrix B can be writ-
ten like in equation (3). Each term B j j is associated with one leg.

A serial singularity occurs whenever at least one of these terms
vanishes.

B = Diag [B11, ...,B j j, ...,Bnn] (3)

When the manipulator is in serial singularity, there is a direction
along which no Cartesian velocity can be produced.

1.5 Postures
The postures are defined for fully-parallel manipulators with

multiple inverse kinematic solutions (Chablat, 97). Let W be
the reachable Cartesian workspace, that is, the set of all reach-
able configurations of the moving platform ((Kumar, 92) and
(Pennock, 93)). Let Q be the reachable joint space, that is, the
set of all joint vectors reachable by the actuated joints.
Definition 2. For a given configuration X in W , a posture is
defined as a solution to the inverse kinematics of the manipulator.
According to the joint limit values, all postures do not necessarily
exist. Changing posture is equivalent to changing the posture of
one or several legs.

1.6 Point-to-point trajectories
There are two major types of tasks to consider : point-to-

point motions and continuous path tracking. Only point-to-point
motions will be considered in this study.

Definition 3. A point-to-point trajectory T is defined by a
set of p configurations in the Cartesian workspace : T =
{X1, ...,Xi, ....,Xp}.

By definition, no path is prescribed between any two configura-
tions Xi and X j.

Hypothesis : In a point-to-point trajectory, the moving plat-
form can not move through a parallel singularity.

Although it was shown recently that in some particular cases
a parallel singularity could be crossed (Nenchev, 97), hypothesis
1 is set for the most general cases.

A point-to-point trajectory T will be feasible if there exists
a continuous path in the Cartesian product of the Cartesian space
by the joint space which does not meet a parallel singularity and
which makes the moving platform pass through all prescribed
configurations Xi of the trajectory T .

Remark : A fully-parallel manipulator with several inverse
kinematic solutions can change its posture between two pre-
scribed configurations. Such a manoeuver may enable the ma-
nipulator to avoid a parallel singularity (Figure 1). More gener-
ally, the choice of the posture for each configuration Xi of the
trajectory T can be established by any other criteria like stiff-
ness or cycle time (Chablat, 98b). Note that a change of posture
makes the manipulator run into a serial singularity, which is not
redhibitory for the feasibility of point-to-point trajectories.

2 Copyright ? 1999 by ASME



Figure 1. Singular (left) and a regular (right) configurations (the actuated
joints are A and B)

1.7 The generalized octree model
The quadtree and octree models are hierachical data struc-

tures based on a recursive subdivision of the plane and the space,
respectively (Meagher, 81). There are useful for representing
complex 2-D and 3-D shapes. In this paper, we use a gener-
alization of this model to dimension k, with k > 3, the 2k-tree
(Chablat, 98a). This model is suitable for Boolean operations
like union, difference and intersection. Since this structure has
an implicit adjacency graph, path-connectivity analyses and tra-
jectory planning can be naturally achieved.

When k > 3, it is not possible to represent graphically the
2k-tree. It is necessary to project this structure onto a lower di-
mensional space (quadtree or octree). For a n-dof fully-parallel
manipulator, the Cartesian product of the Cartesian space by the
joint space defines generalized octree with dimension 2n. When
n = 3 (respectively n = 2), the projection onto the Cartesian space
and the joint space yields octree models (respectively quadtree
models).

2 The moveability in the Cartesian workspace
Definition 4. The N-connected regions of the Cartesian
workspace are the maximal regions where any point-to-point tra-
jectory is feasible.
For manipulators with multiple inverse and direct kinematic so-
lutions, it is not possible to study the joint space and the Cartesian
space separately. First, we need to define the regions of manip-
ulator reachable configurations in the Cartesian product of the
Cartesian space by the joint space W.Q.
Definition 5. The regions of manipulator reachable configura-
tions R j are defined as the maximal sets in W.Q such that

R j ?W.Q,
R j is connected,
R j = {X,q} such that det(A) 6= 0

In other words, the regions R j are the sets of all configurations
(X, q) that the manipulator can reach without meeting a parallel
singularity and which can be linked by a continuous path in W.Q.

Proposition : A trajectory T = {X1, ...,Xp} defined in the
Cartesian workspace W is feasible if and only if :

{
?X ? {X1, ...,Xp}
?qi ? Q,?R j such that (Xi,qi) ? R j

In other words, for each configuration Xi in T , there exists at least
one posture qi and one region of manipulator reachable config-
urations R j such that the manipulator configuration (Xi,q) is in
R j.

Proof : Indeed, if for all configurations Xi, there is one joint
configuration qi such that (Xi,qi) ? R j then the trajectory is fea-
sible because, by definition, a region of manipulator reachable
configurations is connected and free of parallel singularity. Con-
versely, if for a given configuration Xi, it is not possible to find
a posture qi such that (Xi,qi) ? R j, then no continuous, paral-
lel singularity-free path exists in W.Q which can link the other
prescribed configurations.

Theorem : The N-connected regions WN j are the projec-
tion ?W of the region of manipulator reachable configurations
R j onto the Cartesian space :

WN j = ?W R j

Proof : This results is a straightforward consequence of the
above proposition.

The N-connected regions cannot be used directly for plan-
ning trajectories in the Cartesian workspace since it is necessary
to choose one joint configuration q for each configuration X of
the moving platform such that (X,q) is included in the same
region of manipulator reachable configurations R j. However,
the N-connected regions provide interesting global information
with regard to the performances of a fully-parallel manipula-
tors because they define the maximal regions of the Cartesian
workspace where it is possible to execute any point-to-point tra-
jectory.

A consequence of the above theorem is that the Cartesian
workspace W is N-connected if and only if there exists a N-
connected region WN j which is coincident with the Cartesian
workspace :

WN j = W

3 Example: A Two-DOF fully-parallel manipulator
For more legibility, a planar manipulator is used as illus-

trative example in this paper. This is a five-bar, revolute (R)-
closed-loop linkage, as displayed in figure 2. The actuated joint
variables are ?1 and ?2, while the Output values are the (x, y) co-
ordinates of the revolute center P. The passive joints will always

3 Copyright ? 1999 by ASME



be assumed unlimited in this study. Lengths L0, L1, L2, L3, and
L4 define the geometry of this manipulator entirely. The dimen-
sions are defined in table 1 in certain units of length that we need
not specify.

y

A

P(x,y)

B

x
q

2

D

C

L
3

q

1

L
4

L
2

L
1

L0

Figure 2. A two-dof fully-parallel manipulator

L0 L1 L2 L3 L4 ?1min ?1max ?2min ?2max
7 8 5 8 5 0 pi 0 pi

Table 1. The dimensions of the RR-RRR studied

As shown in table 1, the actuated joints are limited. The
Cartesian workspace is shown in figure 3. We want to know
whether this manipulator can execute any point-to-point motion
in the Cartesian workspace. To answer this question, we need to
determine the the N-connected regions.

Y

X

X
2

X
1

Figure 3. The Cartesian workspace

3.1 Singularities
For the manipulator studied, the parallel singularities occur

whenever the points C, D, and P are aligned (Figure 4). Manipu-
lator postures whereby ?3??4 = kpi denote a singular matrix A,
and hence, define the boundary of the joint space of the manip-
ulator. For the manipulator at hand, the serial singularities occur

Figure 4. Example of par-
allel singularity

Figure 5. Example of serial
singularity

whenever the points A, C, and P or the points B, D, and P are
aligned (Figure 5). Manipulator postures whereby ?3 ? ?1 = kpi
or ?4??2 = kpi denote a singular matrix B, and hence, define the
boundary of the Cartesian workspace of the manipulator.

3.2 Postures
The manipulator under study has four postures, as depicted

in figure 6. According to the posture, the parallel singularity
locus changes in the Cartesian workspace, as already shown in
figure 1.

Figure 6. The four postures

4 Copyright ? 1999 by ASME



3.3 The N-connected regions
It turns out that the Cartesian workspace of the manipulator

at hand is not N-connected, e.g. the manipulator cannot move
its platform between any set of configurations in the Cartesian
workspace. In effect, due to the existence of limits on the actu-
ated joints, not all postures are accessible for any configuration
in the Cartesian workspace. Thus, the manipulator may loose
its ability to avoid a parallel singularity when moving from one
configuration to another. This is what happens between points
X1 and X2 (Figure 3). These two points cannot be linked by the
manipulator although they lie in the Cartesian workspace which
is connected in the mathematical sense (path-connected) but not
N-connected. In fact, there are two separate N-connected regions
which do not coincide with the Cartesian workspace and the two
points do not belong to the same N-connected region (Figures 7
and 8).

Y

X

X
2

X
1

Figure 7. The first N-connected region of the Cartesian workspace when
0.0 ? ?1,?2 ? pi

Physically, any attempt in moving the point P from X1 to X2
will cause the manipulator either cross a parallel singularity or
reach a joint limit.

In effect, point X1 is accessible only in the manipulator con-
figuration shown in figure 9a because of the joint limits. When
moving towards point X4, the manipulator cannot remain in its
initial posture because it would meet a parallel singularity (Fig-
ure 9b). Thus, it must change its posture, let say at X3 (Figure
9c). The only new posture which can be chosen is the one de-
picted in figure 9d because any other posture would make the
manipulator meet a parallel singularity (Figure 9e). Then, it is
apparent that the manipulator cannot reach X1 from X4 since
joint A attains its limits (figure 9f).

If we change the values of the joint limits (?1min = ?2min =
?pi), the Cartesian workspace is now N-connected since the

Y

X
2

X
1

X

Figure 8. The second N-connected region of the Cartesian workspace
when 0.0? ?1,?2 ? pi

A B
P

C D

(a) (b)

A B

P

C

D

(c)

A B

PC

D

(d)

A B

P
C D

(e)

A B

P

C

D

(f)
Figure 9. Moving from X1 to X4

computed N-connected regions are coincident with the Cartesian
workspace (Figure 10). In effect, it can be verified in this case
that for every configuration of the moving platform, there are
four postures which define two regions of accessible configura-
tions whose projection onto the Cartesian space yields the full

5 Copyright ? 1999 by ASME



Figure 10. The N-connected regions of the Cartesian workspace when
?pi ? ?1,?2 ? pi

Cartesian workspace.

4 Conclusions
The aim of this paper was the characterization of the N-

connected regions in the Cartesian workspace of fully-parallel
manipulators, i.e. the regions of feasible point-to-point trajec-
tories. The word feasible means that the manipulator should be
able to move between all prescribed configurations while never
meeting a parallel singularity. The manipulators considered in
this study have multiple solutions to their direct and inverse kine-
matics. The N-connected regions were defined by first determin-
ing the maximum path-connected, parallel singularity-free re-
gions in the Cartesian product of the Cartesian workspace by the
joint space. The projection of these regions onto the Cartesian
workspace were shown to define the N-connected regions.

The N-connectivity analysis of the Cartesian workspace is
of high interest for the evaluation of manipulator global perfor-
mances as well as for off-line task programming.

Further research work is being conducted by the authors to
take into account the collisions and to characterize the maximum
regions of the Cartesian workspace where the manipulator can
track any continuous trajectory.

REFERENCES
Wenger, Ph., Chedmail, P. �Ability of a Robot to Travel

Through its Free Workspace� The International Journal of
Robotic Research, Vol. 10:3, June 1991.

Chablat, D. �Domaines d�unicite� et parcourabilite� pour
les manipulateurs pleinement paralle`les� PhD thesis, Nantes,
November 1998.

Chablat, D. and Wenger, Ph. �Working modes and aspects in
fully-parallel manipulators� Proceeding IEEE International Con-
ference of Robotic and Automation, pp. 1964-1969, May 1998.

Merlet, J-P. �Les robots paralle`les� HERMES, seconde
e�dition, Paris, 1997.

Angeles, J. �Fundamentals of Robotic Mechanical Systems�
SPRINGER 97.

Gosselin, C. and Angeles, J. �Singularity analysis of closed-
loop kinematic chains� IEEE Transactions On Robotics And Au-
tomation, Vol. 6, No. 3, June 1990.

Kumar V. �Characterization of workspaces of parallel ma-
nipulators� ASME J. Mechanical Design, Vol. 114, pp 368-375,
1992.

Pennock, G.R. and Kassner, D.J. �The workspace of a gen-
eral geometry planar three-degree-of-freedom platform-type ma-
nipulator� ASME J. Mechanical Design, Vol. 115, pp 269-276,
1993.

Nenchev, D.N., Bhattacharya, S., and Uchiyama, M., �Dy-
namic Analysis of Parallel Manipulators under the Singularity-
Consistent Parameterization� Robotica, Vol. 15, pp. 375-384.
1997.

Chablat, D., Wenger, Ph. , Angeles, J. �The isocondition-
ing Loci of A Class of Closed-Chain Manipulators� Proceeding
IEEE International Conference of Robotic and Automation, pp.
1970-1975, May 1998.

Meagher, D. �Geometric Modelling using Octree Encoding�
Technical Report IPL-TR-81-005, Image Processing Laboratory,
Rensselaer Polytechnic Institute, Troy, New York 12181, 1981.

6 Copyright ? 1999 by ASME




	Preliminaries
	Fully-parallel manipulators
	Kinematics
	Parallel singularities
	Serial singularities
	Postures
	Point-to-point trajectories
	The generalized octree model

	The moveability in the Cartesian workspace
	Example: A Two-DOF fully-parallel manipulator
	Singularities
	Postures
	The N-connected regions

	Conclusions

