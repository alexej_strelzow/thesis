
Premier Congr�s International Conception et Mod�lisation des Syst�mes M�caniques, CMSM�2005 

 

Analyse Comparative des Manipulateurs 
3R � Axes Orthogonaux 
 
Maher Baili � Damien Chablat � Philippe Wenger 
 
Institut de Recherche en Communications et Cybern�tique de Nantes 
UMR CNRS 6597 
1 rue de la No�, BP 92101, 44321 Nantes Cedex 03 France 
 Damien.chablat@irccyn.ec-nantes.fr 

 
R�SUM�. Une famille de manipulateurs � 3 articulations roto�des et � axes orthogonaux 
sans d�calage sur le troisi�me segment se divise en neuf topologies d�espace de travail 
diff�rentes. Une topologie est d�finie par le nombre de points cusps et de n�uds qui 
apparaissent sur les surfaces de singularit�. En se basant sur cette classification, on �value 
les manipulateurs �tudi�s selon deux indices de performances dont le premier est le 
conditionnement qui est  relatif � l�espace articulaire tandis que le second est la proportion 
de la r�gion � 4 solutions par rapport � une sph�re englobant l�espace de travail � l�espace 
de travail. On d�termine la topologie d�espace de travail dans � laquelle se trouve 
appartiennent les manipulateurs ayant les meilleurs indices de performances. 
ABSTRACT. A family of 3R orthogonal manipulators without offset on the third body can be 
divided into exactly nine workspace topologies. The workspace is characterized in a half-
cross section by the singular curves. The workspace topology is defined by the number of 
cusps and nodes that appear on these singular curves. Based on this classification, we 
evaluate theses manipulators by the condition number related to the joint space and the 
proportion of the region with four inverse kinematic solutions compared to a sphere 
containing all the workspace. This second performance number is in relation with the 
workspace. We determine finally le topology of workspace to which belong manipulators 
having the best performance number values. 
MOTS-CL�S : manipulateur orthogonal, classification, singularit�, cusp, n�ud,  topologie 
d�espace de travail, conditionnement. 
KEYWORDS: orthogonal manipulator, classification, singularity, cusp, node, workspace 
topology, condition number. 

 



Premier Congr�s International Conception et Mod�lisation des Syst�mes M�caniques,CMSM�2005       2

1. Introduction 

Malgr� les progr�s technologiques, les industriels n�utilisent que des 
manipulateurs � standards � pour lesquels les axes des articulations successives 
peuvent �tre parall�les. Pour r�pondre � des besoins d�implantation particuliers, les 
concepteurs de sites robotis�s ont �t� demandeurs de solutions innovantes. Par 
cons�quent, des fabricants de robots ont �t� amen�s � employer des morphologies 
originales avec porteur � axes orthogonaux. Leur mise en �uvre a fait appara�tre des 
comportements inattendus (Hemmingson et al.,  1996) : L�utilisation des lois de 
commande usuelles conduisait � des r�ponses erratiques. Les roboticiens ont 
longtemps pris pour acquis que, pour changer de posture, il fallait franchir une 
singularit�. Borrel pensait l�avoir d�montr� th�oriquement en 1986 (Borrel et al., 
1986). Mais un contre-exemple a remis en cause cette propri�t� : un manipulateur 
6R peut changer de posture sans passer par une singularit� (Parenti, 1988). Un 
r�sultat analogue a �t� publi� en 1991 pour des manipulateurs 3R (Burdick, 1991). 
De tels manipulateurs sont appel�s manipulateurs cuspidaux. Une condition 
n�cessaire et suffisante et donn�e dans (El Omri, 1996), un manipulateur peut 
changer de posture sans franchir une singularit�, si et seulement si, il existe dans son 
espace de travail, un point o� trois solutions du mod�le g�om�trique inverse 
co�ncident : Un point cusp. Une condition d�pendant des param�tres g�om�triques 
pour qu�un manipulateur � 3 articulations roto�des, � axes orthogonaux et sans 
d�calage entre les deux derniers axes est donn�e sous forme symbolique (Baili, 
2004). En outre, une classifiaction selon les topologies d�espace de travail de ces 
manipulateurs est �tablie. Cette classification reste destin�e � des analyses globales 
d�accessibilit� ou de parcourabilit�. Le but de ce papier est de proposer un 
compl�ment de cette �tude en �valuant les manipulateurs selon deux indices de 
performances dont l�un est relatif � l�espace articulaire et l�autre � l�espace de 
travail. 

Ce papier est organis� comme suit. Dans la prochaine section, on pr�sentera la 
famille de manipulateurs �tudi�e et on rappellera la classification �tablie dans (Baili, 
2004). Dans la section 3, on analysera cette classification selon les deux indices de 
performances choisis. Dans la derni�re section, on r�sumera l�apport du travail 
pr�sent� dans ce papier. 

2. Pr�liminaires 

2.1. Pr�sentation de la famille de manipulateurs �tudi�e 

La figure 1 repr�sente l�architecture cin�matique des manipulateurs �tudi�s dans 
leur configuration de r�f�rence. L�effecteur repr�sent� par le point P est rep�r� par 
ses trois coordonn�es cart�siennes x, y et z d�finies dans le rep�re de r�f�rence (O, 



Analyse Comparative des manipulateurs 3R � Axes Orthogonaux      3

X, Y, Z) attach� � la base du manipulateur. Les manipulateurs �tudi�s dans ce 
papier n�ont pas de d�calage entre les deux derniers axes. Les param�tres 
g�om�triques restants � consid�rer sont d2, d3, d4 et r2. Les angles ?2 et ?3 sont 
�gaux � -90� et 90� respectivement. On ne consid�rera pas de but�es articulaires, par 
suite, les variables articulaires ?1, ?2 et ?3 sont illimit�es.  

z y 

O

x 

P?1

? 2 

? 3 
d 2 

r 2 

d 4 

d 3 .
 

Figure 1. Famille de manipulateurs 
�tudi�e 

4 Postures 

2 Postures 

? (m) ?2 (degr�) 
? 3 

(d
eg

r�
) 

Z(
m

) 

d2 = 1, d3 = 2, d4 = 1,5, r2 = 1

S1

S2

WS2 

WS1 
Aspect A1

Aspect A2

 
Figure 2. les branches de singularit�s dans 
l�espace articulaire (� gauche) et les 
surfaces de singularit�s dans l�espace de 
travail (� droite) 

2.2. Propri�t�s des manipulateurs 

Les manipulateurs �tudi�s dans cet article poss�dent les propri�t�s suivantes :Ils  
peuvent �tre cuspidal (il peut changer de posture sans franchir une singularit�) ; Il 
peuvent avoir ou pas une cavit� toro�dale dans son espace de travail (un trou � 
l�int�rieur de l�espace de travail) et ils peuvent �tre g�n�rique1 / non g�n�rique et 
binaire2 / quaternaire3. Ces propri�t�s sont directement li�es � la topologie des 
surfaces de singularit� dans l�espace de travail. 

Le d�terminant de la matrice Jacobienne d�un manipulateur de cette famille est 
donn� par l��quation [1]. 

[ ]4 3 4 3 2 3 3 3 2 3 2det( ) = ( ) ( - )+ +d d d c d s d s r c cJ  [1] 
o� ci = cos(?i) et si = sin(?i). Les singularit�s sont d�finies par det(J) = 0. Le 

trac� de det(J) = 0 forme deux ou quatre branches de singularit�s dans - ? ? ?2 ? ? 
et - ? ? ?3 ? ? selon que d3 > d4 ou pas. 

                             
1  Un manipulateur est g�n�rique si ses branches de singularit� dans l�espace articulaire ne se 
coupent pas 
2 Un manipulateur est binaire s�il a deux solutions au Mod�le G�om�trique Inverse (MGI) 
3 Un manipulateur est quaternaire s�il a quatre solutions au MGI 



Premier Congr�s International Conception et Mod�lisation des Syst�mes M�caniques,CMSM�2005       4

Lorsque d3 > d4, le premier facteur du d�terminant donn� par l��quation [1] ne 
s�annule pas, par cons�quent, on obtient deux branches de singularit�s distinctes S1 
et S2 dans l�espace articulaire (El Omri, 1996) qui divisent l�espace articulaire en 
deux domines A1 et A2 d�pourvus de singularit�s appel�s aspects (Borrel et al., 
1988), voir figure 2. 

Lorsque d3 ? d4, l�effecteur rencontre l�axe 2 et on a ?3 = � arccos (- d3 / d4). 
Dans ces conditions, deux droites suppl�mentaires apparaissent dans l�espace 
articulaire d�fini tels que -? ? ?2 ? ? et -? ? ?3 ? ?. Ces deux droites peuvent couper 
ou pas les branches de singularit� S1 et S2 dans l�espace articulaire. Le nombre 
d�aspects d�pend ainsi de ces intersections. Notons que dans le cas d3 ? d4, aucune 
surface de singularit� suppl�mentaire autre que la surface de singularit� int�rieure 
WS1 (image de S1) et la surface de singularit� ext�rieure WS2 (image de S2) 
n�apparaissent dans l�espace de travail. Ceci s�explique par le fait que lorsque 
l�effecteur rencontre l�axe 2, la rotation selon ?2 n�entra�ne pas un changement de la 
position de l�effecteur. 

Le figure 2 correspond � un manipulateur cuspidal : il a 4 cusps sur la surface de 
singularit� int�rieure WS1. Cette surface d�limite une r�gion dont le degr� 
d�accessibilit� est de 4 (le nombre de solutions au MGI ou le nombre de postures). 
Toutefois, ce manipulateur n�a par de n�uds et pas de cavit� toro�dale dans son 
espace de travail. Sur la base de la topologie des surfaces de singularit� dans 
l�espace de travail d�finie par le nombre de points singuliers particuliers qui 
apparaissent (les cusps et les n�uds), une classification exhaustive de l�espace des 
param�tres en diff�rentes topologies d�espace de travail a �t� �tablie (Baili et al., 
2004). Cette classification se base sur les propri�t�s : g�n�rique / non g�n�rique, 
binaire / quaternaire, le nombre d�aspects dans l�espace articulaire, le nombre de 
points cusps, le nombre de n�uds et la pr�sence ou pas de cavit� toro�dale dans 
l�espace de travail. 

2.3. Classification selon le nombre de cusps 

L�objectif est de partitionner l�espace des param�tres en plusieurs cellules o� le 
nombre de points cusps reste constant. Afin de diminuer le nombre d�inconnues du 
probl�me et sans perte de g�n�ralit�s, on normalise tous les param�tres par d2. Les 
param�tres � consid�rer sont d�sormais d3, d4 et r2. En utilisant des outils pouss�s de 
calcul alg�brique, (Corvez et al., 2002) montre que l�espace des param�tres peut se 
diviser en 105 cellules o� le nombre de points cusps est constant. (Baili et al., 2003) 
ont regroup� ces 105 cellules en seulement 5 domaines o� les manipulateurs ne 
peuvent avoir que 0, 2 ou 4 cusps. Ceci montre que parmi les 5 surfaces donn�es 
dans (Corvez et al., 2002), une ou plusieurs ne sont pas pertinentes. Toutefois, 
(Baili et al., 2003) ne pr�cise pas les surfaces pertinentes parmi les 5. C�est dans 
(Baili et al., 2004) que les �quations des 4 surfaces pertinentes C1, C2, C3 et C4 ont 



Analyse Comparative des manipulateurs 3R � Axes Orthogonaux      5

�t� donn�es sous forme symbolique d�pendant des param�tres g�om�triques. Les 
�quations [2], [3], [4] et [5] repr�sentent les 4 surfaces pertinentes. 

2 2 2 22
2 2 3 2 3 2

1 4 1 1 3 2
( )1:  o� 

2d d
d r d rC d C C d r

AB
? ?+ ? += = + ?? ?? ?? ?

 [2] 

3
2 4 2 2

3

:  o� 
1d d

dC d C C A
d

= = ?+  [3] 

3
3 4 3 3 3

3

:  o� et 1
1d d

dC d C C B d
d

= = ? >?  [4] 

3
4 4 4 4 3

3

:  o� et 1
1d d

dC d C C B d
d

= = ? <?  [5] 
o� 2 22 23 2 3 2( 1) et ( 1)A d r B d r= + + = ? +  
La figure 3 repr�sente les 4 surfaces de s�parations et les 5 domaines. Les 

domaines 1, 2, 3, 4 et 5 sont associ�s aux manipulateurs ayant 0, 4, 2, 4 et 0 cusps 
dans leurs espaces de travail respectivement. 

 

d3
0

1

2

3

4

5

1 2 3 4 5

2 cusps

4 cusps

4 cusps

0 cusp

0 cusp

d4 Domaine 2

Domaine 1

D
om

ai
ne

 5

Domaine 3

Domaine 4

C1

C2
C3C4

 
Figure 3. Les 4 surfaces de s�parations et les 5 domaines dans une section (d3, d4) 

pour r2 = 1 

2.4. Classification selon le nombre de n�uds 

On affine maintenant l��tude en d�terminant toutes les topologies d�espace de 
travail possibles. Pour cela, on part de chaque domaine ayant un nombre de points 
cusps constant et on le partage en plusieurs topologies d�espace de travail. Chaque 



Premier Congr�s International Conception et Mod�lisation des Syst�mes M�caniques,CMSM�2005       6

sous domaine d�finit une topologie d�espace de travail que l�on note WTi. 
(Baili, 2004) montre que les manipulateurs appartenant � la famille �tudi�e ne 
peuvent avoir que 0, 1, 2, 3 ou 4 n�uds et qu�il existe exactement 9 topologies 
d�espace de travail diff�rentes. Les 3 surfaces suppl�mentaires E1, E2 et E3 
permettant de subdiviser les 5 domaines en 9 topologies d�espace de travail 
correspondent aux �quations [6], [7], et [8] respectivement. 

1 4 1 1
1:  o� E ( )
2d d

E d E A B= = ?  [6] 

2 4 2 2 3:  o� Ed dE d E d= =  [7] 

3 4 3 3
1:  o� E ( )
2d d

E d E A B= = +  [8] 
La figure 4 repr�sente les 7 surfaces de s�parations et les 9 topologies d�espace 

de travail. Dans chaque topologie d�espace de travail, on pr�cise le nombre de cusps 
et le nombre de n�uds. 

0

1

2

3

4

5

1 2 3 4 5
d3

d4

C1

C2C3C4

E3

E2

E1WT
5(2

, 1
)

W
T

8(0
, 0

)
W

T
9(0

, 2
) WT 7(4, 4)

WT 6(2, 3)

WT 3(4, 0)

WT 4(4, 2)

WT 2(4, 2)
WT 1(0, 0)

WT i(nombre de cusps, nombre de noeuds)  
Figure 4. Les 7 surfaces de s�parations et les 9 topologies d�espace de travail dans 

une section (d3, d4) pour r2 = 1 

La figure 5 repr�sente un arbre de classification de la famille de manipulateurs 
�tudi�e en 9 topologies d�espace de travail diff�rentes. Pour chaque topologie 
d�espace de travail, on indique le nombre de cusps, le nombre de n�uds, le nombre 
d�aspects, si le manipulateur est binaire ou quaternaire. On note que le triplet 
(1, 1, 0) veut dire que le manipulateur a une cavit� toro�dale, une r�gion � 2 
solutions au MGI et 0 r�gion � 4 solutions au MGI. 



Analyse Comparative des manipulateurs 3R � Axes Orthogonaux      7

 
Figure 5. Arbre de classification en 9 topologies d�espace de travail 

3. Analyse de la classification et indices de performances 

La classification de la famille de manipulateurs selon leurs topologies d�espace 
de travail repr�sente un outil pour l�ing�nieur dans sa recherche de manipulateurs 
innovants. Cette classification propose des informations souvent destin�es � des 
analyses globales d�accessibilit� ou de parcourabilit�. Si l�on veut compl�ter cette 
�tude et aider le concepteur � affiner son choix, on se retourne vers l��valuation des 
performances en un point de l�espace de travail ou en une configuration articulaire 
donn�e. Dans cette partie, on va analyser donc la famille de manipulateurs �tudi�e 
selon deux crit�res de performances. Le premier crit�re est le conditionnement, il est 
relatif � l�espace articulaire. Le deuxi�me indice de performance est relatif � 
l�espace de travail, il permet de d�terminer les proportions des r�gions � 2 et � 4 
solutions au MGI et de toute la r�gion accessible par rapport � une sph�re dont le 
rayon est la port�e du manipulateur4. 

                             
4 La port� d�un manipulateur est la distance la plus �loign�e que peut atteindre son effecteur 



Premier Congr�s International Conception et Mod�lisation des Syst�mes M�caniques,CMSM�2005       8

3.1. Analyse selon l�espace articulaire 

Le conditionnement ou indice d�isotropie d�un manipulateur est d�finie comme 
le conditionnement de sa matrice jacobienne cin�matique (Salisbury et al., 1982). 
C�est le rapport entre la plus grande et la plus petite valeur singuli�re de J 
(�quation [9]). 

max

min
K ?= ?  [9] 
Notons que cette d�finition est valable uniquement lorsque les �l�ments de la 

matrice jacobienne J ont la m�me dimension. Dans le cas o� la t�che est d�finie en 
position et en orientation, il est possible d�homog�n�iser les dimensions en divisant 
les lignes de J correspondant aux positions par � la longueur caract�ristique � du 
manipulateur �tudi� (Angeles, 1997). 

Le calcul du conditionnement de la matrice jacobienne repose sur la 
connaissance de la position articulaire (?1, ?2, ?3) et de la position de l�effecteur 
(x, y, z). Pour atteindre cet objectif, deux possibilit�s nous sont offertes : un 
balayage de l�espace articulaire ou un balayage de l�espace de travail. Dans le 
travail pr�sent� dans ce papier, on a opt� pour un balayage dans l�espace articulaire. 

Pour des raisons de pr�sentation, on calculera l�inverse du conditionnement de la 
matrice jacobienne cin�matique qui �volue entre 0 et 1. On le notera K-1 pour 
exprimer le conditionnement inverse de la matrice jacobienne. On a choisi de 
calculer deux indices � partir du K-1 : K-1 maximum et la K-1 moyen. 

Le premier indice est un indice local qui permet de trouver un ensemble de 
manipulateurs poss�dant une configuration isotrope (Angeles, 1997) alors que le 
second est une mesure globale. Le travail pr�sent� dans ce papier ne peut donner 
tous les r�sultats possibles car on est oblig� de faire des coupes dans l�espace des 
param�tres (pour une valeur de r2 donn�e, par exemple). On recherchera dans 
chaque coupe les exemples les plus repr�sentatifs. Ces courbes d�iso-valeurs 
permettent de d�finir l��volution des indices vers les valeurs les plus �lev�es par 
rapport aux topologies d�espace de travail d�finies dans les pr�liminaires. 

Pour calculer K-1  maximum et K-1 moyen, on va utiliser une technique de 
discr�tisation de l'espace de travail. D'autres m�thodes telles que l'analyse par 
intervalles ou Monte-Carlo auraient pu �tre utilis�es. Notre choix a �t� guid� par un 
souci de simplicit� et de rapidit� de mise en �uvre. 

Afin de pouvoir obtenir des r�sultats facilement compr�hensibles, on effectue un 
balayage dans une section (d3, d4) de l�espace des param�tres pour un r2 donn�. Pour 
chaque point de la section (d3, d4) repr�sentant un manipulateur, on calcule les 
valeurs de K-1 moyen et maximal. 



Analyse Comparative des manipulateurs 3R � Axes Orthogonaux      9

On obtient dans une section de l�espace des param�tres, des courbes d�iso-
valeurs dont chacune repr�sente les manipulateurs ayant la m�me valeur de K-1 
moyen ainsi que la m�me valeur de K-1 maximal. 

La figure 6 repr�sente les courbes d�iso-valeurs de K-1  moyen dans une section 
(d3, d4) de l�espace des param�tres pour r2 = 1. Par ailleurs, figure 7 repr�sente les 
courbes d�iso-valeurs de K-1 maximum dans la m�me section (d3, d4).  

Les fl�ches que l'on voit sur les sections illustr�es par la figure 6  
(respectivement la figure 7) repr�sentent dans chaque topologie d�espace de travail, 
les �volutions vers les valeurs de K-1 moyen (respectivement maximum) les plus 
�lev�es. Dans chaque section, la zone contenant des manipulateurs ayant une valeur 
de K-1 moyen (respectivement maximum) �lev�e est marqu�e par (�). On choisit un 
manipulateur dans chacune de ces zones et on calcule son K-1 moyen et son K-1 
maximum. Les valeurs de K-1 calcul�es ont une pr�cision de 10-4. Toutefois et apr�s 
arrondissement, on note deux chiffres apr�s la virgule. Ces valeurs sont donn�es 
apr�s une v�rification de leurs faibles variations par rapport � la variation du pas de 
calcul. En effet, si on divise le pas de calcul par 102, les valeurs de K-1 ne varient 
que de l�ordre de 10-4. 

C1 

E1

C3C4 C2
E2 

E3 

d3 

d4 

�

� 

W T 3

W T 8 

 
Figure 6. K-1 moyen dans une section(d3, d4)  pour r2 = 1 

Le manipulateur de topologie d�espace de travail WT3 dont les param�tres 
g�om�triques sont : d2 = 1 ; d3 = 3 ; d4 = 1,7 et r2 = 1 a un conditionnement inverse 
moyen de 0,23 et un conditionnement inverse maximum de 0,81. Le deuxi�me 
manipulateur ayant une valeur de conditionnement inverse moyen �lev�e est de 
topologie d�espace de travail WT8, ses param�tres g�om�triques sont : d2 = 1 ; 
d3 = 0,1 ; d4 = 1,2 et r2 = 1. son conditionnement inverse moyen est de 0,21 tandis 
que son conditionnement inverse maximum est de 0,83. 



Premier Congr�s International Conception et Mod�lisation des Syst�mes M�caniques,CMSM�2005       10

C1 

E1 

C3C4 C2
E2 

E3 

d3 

d4  

�

� 
�

W T 4 

 
Figure 7. K-1 maximum dans une section(d3, d4)  pour r2 = 1 

En analysant la figure 7, on remarque que les manipulateurs ayant valeur de 
conditionnement inverse maximum �lev�e appartiennent aux topologies d�espace de 
travail WT3 et WT8. On remarque aussi qu�un troisi�me manipulateur situ� sur la 
surface E2 a une valeur de conditionnement maximum �lev�e. En calculant cette 
valeur, on s�aper�oit qu�elle atteint la valeur maximale de 1 d�o� l�isotropie de ce 
manipulateur. Toutefois son conditionnement inverse moyen n�est que de 0,18. 

3.2. Analyse selon l�espace de travail 

Cet indice permet, pour un manipulateur donn�, de d�terminer la proportion du 
volume de la r�gion � 4 solutions accessible par l�effecteur par rapport au volume 
d�une sph�re de rayon ?max (repr�sentant la port�e du manipulateur) et centr�e sur 
l�origine. Cette sph�re englobe tout l�espace de travail. Dans le cas des 
manipulateurs �tudi�s dans ce papier et par une analyse g�om�trique simple, la 
port�e du manipulateur est d�finie par l��quation [10]. 

( )22max 4 2 3 1d r d? = + + +  [10] 
Pour des raisons de sym�trie de l�espace de travail, le balayage sera effectu� 

dans la section (?, z) et pour des valeurs de ? et z positives. 
On obtient une section de l�espace des param�tres repr�sentant des courbes 

d�iso-valeurs de manipulateurs ayant la m�me proportion du volume de la r�gion � 4 
solutions par rapport � la sph�re englobant l�espace de travail. 



Analyse Comparative des manipulateurs 3R � Axes Orthogonaux      11

La figure 8 repr�sente, dans une section (d3, d4) pour r2 = 1, les proportions de la 
r�gion � 4 solutions au MGI par rapport � la sph�re englobant l�espace de travail. 

De la m�me mani�re, on utilise des fl�ches pour montrer les �volutions dans 
chaque topologie d�espace de travail vers les manipulateurs dont la proportion de la 
r�gion � 4 solutions est la plus importante. 

C1

E1 

C3C4 C2
E2 

E3 

d3 

d4 �

� 

W T 3

W T 9 

 
Figure 8. Proportion de la r�gion � 4 solutions dans une section(d3, d4)  pour r2 = 1 

Le premier manipulateur ayant une proportion de r�gion � 4 solutions importante 
appartient � la topologie d�espace de travail WT3. Ses param�tres g�om�triques 
sont : d2 = 1 ; d3 = 4,5 ; d4 = 2,9 et r2 = 1. La valeur de sa proportion de la r�gion � 4 
solutions est de 0,26, celle de la r�gion � 2 solutions est de 0,48 ce qui donne une 
proportion de la r�gion accessible de 0,74 par rapport � la sph�re englobant l�espace 
de travail. 

Le second manipulateur est de topologie d�espace de travail WT9. Le 
manipulateur dont les param�tres g�om�triques sont : d2 = 1 ; d3 = 0,1 ; d4 = 2,25 et 
r2 = 1 a une proportion de la r�gion � 4 solutions de 0,49 et une proportion de la 
r�gion � 2 solutions de 0,11. Par cons�quent, la proportion de la r�gion accessible 
par l�effecteur est de 0,6. 

4. Conclusions 

Les manipulateurs �tudi�s se classifient en 9 topologies d�espace de travail 
diff�rentes. Bien que cette classification soit d�une aide pr�cieuse pour un ing�nieur 
recherchant des manipulateurs innovants, elle ne propose que des informations 
destin�es � des analyses globales d�accessibilit� ou de parcourabilit�. Afin de la 
compl�ter, on analyse ces manipulateurs selon deux crit�res de performances. Le 



Premier Congr�s International Conception et Mod�lisation des Syst�mes M�caniques,CMSM�2005       12

premier est relatif � l�espace articulaire et se r�sume � calculer les K-1 moyen et 
maximum ; on a remarqu� que les manipulateurs ayant les valeurs les plus �lev�es 
appartiennent aux topologies d�espace de travail WT3 et WT8 pour K-1 moyen et aux 
topologies WT3, WT4 et WT8 pour K-1 maximum. Le second indice est relatif � 
l�espace de travail et permet de calculer la proportion de la r�gion � 4 solutions au 
MGI par rapport � une sph�re dont le rayon est �gal � la port�e du manipulateur. 
Pour ce crit�re, les topologies d�espace de travail WT3 et WT9 contiennent les 
manipulateurs ayant les meilleures proportions. En r�sum�, les manipulateurs ayant 
les meilleurs indices de performances appartiennent n�cessairement � la topologie 
d�espace de travail WT3. 

5. Bibliographie 

Angeles J., Fundamental of robotic mechanical system, theory, methods and algorithms, 
Springer, 1997. 

Baili M., Analyse et classification des manipulateurs 3R � axes orthogonaux, th�se de 
doctorat, Ecole Centrale de Nantes et Universit� de Nantes, d�cembre 2004. 

Baili M., Wenger Ph., Chablat D., � Classification of one family on 3R positioning 
manipulators �, The 11th International Conference on Advanced Robotics, 30 juin-03 
juillet 2003, University of Co�mbra, p. 1849-1854. 

Baili M., Wenger Ph., Chablat D., � Classification of one family on 3R positioning 
manipulators �, The 11th International Conference on Advanced Robotics, 26 avril-01 mai 
2004, New Orleans, p. 1933-1938. 

Borrel P., Liegeois A., � A Study of manipulator inverse kinematic solutions with application 
to trajectory planning and workspace determination �, Proceeding IEEE International 
Conference on Robotics and Automation, 1986, p. 1180-1185. 

Burdick J. W., � A Classification of 3R regional manipulator Singularities and Geometries �, 
Proceeding IEEE International Conference on Robotics and Automation, 1991, 
Sacramento, California, p. 2670-2675. 

Corvez S., Rouillier F., � Using computer algebra tool to classify serial manipulators �, 
Proceeding 4th International Workshop on Automated Deduction in Geometry, Linz, 
2002. 

El Omri J., Analyse g�om�trique et cin�matique des m�canismes de type manipulateurs, th�se 
de doctorat, Ecole Centrale de Nantes et Universit� de Nantes, f�vrier 1996. 

Hemmingson E., Ellqvist S., Pauxels J., � New Robot Improves Cost-Efficiency of Spot 
Welding �, ABB Review, Spot Welding Robots, p. 4-6, 1996. 

Parenti C. V., Innocenti C., � Spatial Open Kinematic Chains: Singularities, Regions and 
Subregions �, Proceeding, 7th CISM-IFTOMM Romansy, 1988, Udine, Italy, p. 400-407. 

Salisbury J. K., Craig J., � Articulated hand: force and kinematic issues �, The International 
journal of Robotics research, Vol. 1, n� 1, 1982, p. 4-17. 


