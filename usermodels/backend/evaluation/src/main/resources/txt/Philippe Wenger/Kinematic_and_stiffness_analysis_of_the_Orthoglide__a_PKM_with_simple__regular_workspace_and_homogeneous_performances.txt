
Paskhevich A., Wenger P. et Chablat D., �Kinematic and stiffness analysis of the Orthoglide, a PKM with simple, regular workspace and homogeneous 
performances�, IEEE International Conference On Robotics And Automation, Rome, Italie, Avril, 2007. 

 

 
 

Kinematic and stiffness analysis of the Orthoglide,  
a PKM with simple, regular workspace and homogeneous performances 

Anatoly Pashkevich, Philippe WENGER, Damien CHABLAT 

Robotic Laboratory, Department of Control Syatems,  
Belarusian State University of Informatics and Radioelectronics 

6 P.Brovka St., Minsk 220027, Belarus 
pap@bsuir.unibel.by  

Institut de Recherche en Communications et Cybern�tique de Nantes UMR CNRS 6597 
1, rue de la Noe, BP 92101, 44321 Nantes Cedex 03 France 

Philippe.Wenger@irccyn.ec-nantes.fr, Damien.Chablat@irccyn.ec-nantes.fr  
 
 
 

Abstract� The Orthoglide is a Delta-type PKM dedicated to 
3-axis rapid machining applications that was originally 
developed at IRCCyN in 2000-2001 to meet the advantages of 
both serial 3-axis machines (regular workspace and 
homogeneous performances) and parallel kinematic 
architectures (good dynamic performances and stiffness). This 
machine has three fixed parallel linear joints that are mounted 
orthogonally. The geometric parameters of the Orthoglide were 
defined as function of the size of a prescribed cubic Cartesian 
workspace that is free of singularities and internal collision. 
The interesting features of the Orthoglide are a regular 
Cartesian workspace shape, uniform performances in all 
directions and good compactness. In this paper, a new method 
is proposed to analyze the stiffness of overconstrained Delta-
type manipulators, such as the Orthoglide. The Orthoglide is 
then benchmarked according to geometric, kinematic and 
stiffness criteria: workspace to footprint ratio, velocity and 
force transmission factors, sensitivity to geometric errors, 
torsional stiffness and translational stiffness. 

I. INTRODUCTION 
The question whether a parallel-kinematic machine 

(PKM) is globally more suitable for rapid machining than a 
serial machine or not, is difficult to answer [1, 2] and still 
open. PKMs and serial machines have their own merits and 
drawbacks. Today, most industrial 3-axis machine-tools 
have a serial kinematic architecture with orthogonal linear 
joint axes along the x, y and z directions. Thus, the motion 
of the tool in any of these directions is linearly related to the 
motion of one of the three actuated axes. Also, the 
performances are constant throughout the Cartesian 
workspace, which is a parallelepiped. The main drawback is 
inherent to the serial arrangement of the links, namely, poor 
dynamic performances. The Orthoglide is a translational 3-
axis Delta-type [3, 4] PKM that was designed to meet the 
advantages of serial machine tools but without their 
drawbacks [3]. Starting from a Delta-type architecture with 
three fixed linear joints and three articulated parallelograms, 
an optimization procedure was conducted on the basis of 
two kinematic criteria (i) the conditioning of the Jacobian 

matrix of the PKM and (ii) the transmission factors. The first 
criterion was used to impose an isotropic configuration 
where the tool forces and velocities are equal in all 
directions. The second criterion was used to define the 
actuated joint limits and the link lengths with respect to a 
desired Cartesian workspace size and prescribed limits on 
the transmission factors. The Orthoglide has a Cartesian 
workspace shape that is close to a cube whose sides are 
parallel to the planes xy, yz and xz respectively. 

In this paper, a new method is proposed to analyze the 
stiffness of overconstrained Delta-type manipulators, such 
as the Orthoglide. The Orthoglide is then benchmarked 
according to geometric, kinematic and stiffness criteria: 
workspace to footprint ratio, velocity and force transmission 
factors, sensitivity to geometric errors, torsional stiffness 
and translational stiffness. Next section recalls the geometric 
and kinematic parameters of the Orthoglide prototype. The 
main contribution compared to our previous results [1], [3], 
[5] � [8] is related to the Orthoglide stiffness analysis, which 
is based on the developed analytical pseudo-rigid model. 
This allows comparing the Orthoglide to the similar 
performances of other manipulators [9, 10]. 

II. GEOMETRY AND KINEMATICS OF THE ORTHOGLIDE 
Figure. 1 shows a photography (left) and a CAD-model 

(right) of the Orthoglide prototype. This machine has three 
parallel PRPaR identical chains (where P, R and Pa stand 
respectively for prismatic, revolute, and parallelogram 
joints). The actuated joints are the three orthogonal 
prismatic ones. The output body is connected to the 
prismatic joints through a set of three parallelograms, so that 
it can move only in translation. Note that because only two 
parallelograms would be sufficient to restrict the motion in 
translation, the Orthoglide is overconstrained. The 
Orthoglide has been designed so that it has an isotropic 
configuration in its workspace, that is, a configuration where 
the Jacobian matrix is isotropic. This configuration is 
reached when all parallelograms are orthogonal to each 



Paskhevich A., Wenger P. et Chablat D., �Kinematic and stiffness analysis of the Orthoglide, a PKM with simple, regular workspace and homogeneous 
performances�, IEEE International Conference On Robotics And Automation, Rome, Italie, Avril, 2007. 

 

other (Fig. 2). To have a kinematic behavior close to the one 
of a serial 3-axis machine tool, we have also imposed that, in 
the isotropic configuration, the velocity transmission factors 
must be equal to 1. 

1
2
3

P

 
Figure 1: The Orthoglide prototype (�CNRS Phototh�que/CARLSON Leif) 

This condition implies that for each leg, the axis of the 
linear joint and the axis of the parallelogram are collinear. 
Since at the isotropic configuration, the parallelograms are 
orthogonal, this implies that the linear joints are orthogonal 
(Fig. 2). An optimization scheme was developed to calculate 
automatically the geometric parameters as function of the 
size Lworkspace of a prescribed Cartesian workspace, which is 
a cube [3]. The three main steps of this scheme are briefly 
recalled. 

x

z

y LWorkspace

Q2

Q1

 
Figure 2: Isotropic configuration and Cartesian workspace of the Orthoglide 
mechanism and points Q1 and Q2. 

First, two points Q1 and Q2 are determined in the 
prescribed cubic Cartesian workspace (Fig. 2) such that if 
the transmission factor bounds are satisfied at these points, 
they are satisfied in all the prescribed Cartesian workspace. 
These points are then used to define the leg length as 
function of Lworkspace. Finally, the positions of the base points 

and the linear actuator range are calculated such that the 
prescribed cubic Cartesian workspace is fully included in the 
Cartesian workspace of the Orthoglide. The prototype built 
at IRCCyN was designed using this optimization scheme on 
the basis of the following data: the size of the prescribed 
workspace is Lworkspace = 200 mm and the minimal and 
maximal velocity transmission factors ? are � and 2, 
respectively as shown in Fig. 3 [7]. These factors are defined 
as the eigenvalues of the product of the Jacobian matrix J by 
its transpose The resulting length of the three parallelograms 
is 310 mm and the resulting range of the linear joints is 257 
mm. The values of the transmission factors inside the 
prescribed cubic workspace were confirmed using interval 
analysis [8]. Fig. 4 shows a view of the orthoglide 
workspace computed with interval analysis, in which we 
could verify the inclusion of the prescribed cubic 
workspace. 

 
Figure 3: Computing the joint limits of the velocity transmission factors 
along Q1 and Q2 



Paskhevich A., Wenger P. et Chablat D., �Kinematic and stiffness analysis of the Orthoglide, a PKM with simple, regular workspace and homogeneous 
performances�, IEEE International Conference On Robotics And Automation, Rome, Italie, Avril, 2007. 

 

 

x 

y 

z 

 
Figure 4 Workspace calculated with interval analysis for [ ]1/ 2,2? ?  

III. SENSIBILITY ANALYSIS 
Two complementary methods were used to analyze the 

sensitivity of the Orthoglide to its dimensional and angular 
variations [5]. The first method was used to have a rough 
idea of the influence of the dimensional variations on the 
location of the end-effector. It showed that variations in the 
design parameters of the same type from one leg to the other 
have the same influence on the end-effector (Fig. 5). The 
second method takes into account the variations in the 
parallelograms. It uses a differential vector method to study 
the influence of the dimensional and angular variations in 
the parts of the manipulator on the position and orientation 
of the end-effector, and particularly the influence of the 
variations in the parallelograms. 

It turns out that the kinematic isotropic configuration of 
the manipulator is the least sensitive one to its dimensional 
and angular variations. On the contrary, the closest 
configurations to its kinematic singular configurations are 
the most sensitive ones to geometrical variations. 

 
Figure 5: Basic kinematic architecture of the Orthoglide 

The position of the end-effector is sensitive to variations 
in the lengths of the parallelograms, ?Li, and to the position 
errors of points Ai, Bi, and Ci along axis xi. Conversely, the 
influence of ?li and ?mi, the parallelism errors of the 
parallelograms, is low and even negligible in the kinematic 
isotropic configuration. The orientation errors of the 

prismatic joints are the most influential angular errors on the 
position of the end-effector. Besides, the position of the end-
effector is not sensitive to angular variations in the isotropic 
configuration. ?li and ?mi are the only dimensional 
variations, which are responsible for the orientation error of 
the end-effector. However, the influence of the parallelism 
error of the small sides of the parallelograms, depicted by 
?li, is more important than the one of the parallelism error of 
their long sides, depicted by ?mi. The sensitivity of the 
position and the orientation of the end-effector is generally 
null in the kinematic isotropic configuration, and is a 
maximum when the manipulator is close to a kinematic 
singular configuration, i.e. P= Q2. 

Indeed, only two kinds of design parameters variations 
are responsible for the variations in the position of the end-
effector in the isotropic configuration: ?Li and ?eix. 
Likewise, two kinds of variations are responsible for the 
variations in its orientation in this configuration: ?li, the 
parallelism error of the small sides of the parallelograms, 
??Aiy and ??iy. Moreover, the sensitivities of the pose 
(position and orientation) of the end-effector to these 
variations are a minimum in this configuration, except for ?i. 
On the contrary, Q2 configuration is the most sensitive 
configuration of the manipulator to variations in its design 
parameters. Variations in the pose of the end-effector 
depend on all the design parameters variations and are a 
maximum in this configuration. 

We are able to compute the variations in the position and 
orientation of the end-effector with knowledge of the 
amount of variations in the design parameters. For instance, 
let us assume that the parallelism error of the small sides of 
the parallelograms is equal to 10 ?m, then, the position error 
of the end-effector will be equal about to 3?m in Q1 
configuration. Likewise, if the orientation error of the 
direction of the ith prismatic joint about axis yi of Ri is equal 
to 1 degree, the position error of the end-effector will be 
equal about to 4.8 mm in Q2 configuration. 

Table 1 shows that the probability to get a position error 
lower than 0.3 mm is higher in the kinematic isotropic 
configuration than in Q1 and Q2 configurations, if we assume 
that the length and angular tolerances are equal to 0.05 mm 
and 0.03 deg, respectively. However, the probability to get 
an orientation error lower than 0.25 deg is the same in Q1 
and the isotropic configurations, but is lower in Q2 
configuration. 

TABLE 1. PROBABILITIES TO GET A POSITION ERROR LOWER THAN 0.3 MM 
AND AN ORIENTATION ERROR LOWER THAN 0.25 DEG IN Q1, Q2 AND 

ISOTROPIC CONFIGURATIONS 
 Configuration 
 Q1 Isotropic  Q2 ( )P 2Prob 0.3mm? ?  0.8468  0.9683  0.7276 

( )Prob 0.25deg?? ?  0.9691  0.9690  0.9453 



Paskhevich A., Wenger P. et Chablat D., �Kinematic and stiffness analysis of the Orthoglide, a PKM with simple, regular workspace and homogeneous 
performances�, IEEE International Conference On Robotics And Automation, Rome, Italie, Avril, 2007. 

 

IV. STIFFNESS ANALYSIS 
For Parallel Kinematic Machines, stiffness is an essential 

performance measure since it is directly related to the 
positioning accuracy and the payload capability. 
Mathematically, this benchmark is defined by the stiffness 
matrix, which describes the relation between the 
linear/angular displacements of the end-effector and the 
external forces/torques applied to the tool. At present there 
are three main methods for the computation of the stiffness 
matrix: the Finite Element Analysis (FEA) [11], the matrix 
structural analysis (SMA) [12], and the virtual joint method 
(VJM) that is often called the lumped modeling [13].  

The first of them, FEA, is proved to be the most accurate 
and reliable, however it is usually applied at the final design 
stage because of the high computational expenses required 
for the repeated re-meshing of the complicated 3D structure 
over the whole workspace. The second method, SMA, also 
incorporates the main ideas of the FEA, but operates with 
rather large elements � 3D flexible beams describing the 
manipulator structure. This obviously leads to the reduction 
of the computational expenses, but does not provide with 
clear physical relations required for the parametric stiffness 
analysis. And finally, the VJM method is based on the 
expansion of the traditional rigid model by adding the 
virtual joints (localized springs), which describe the elastic 
deformations of the links. The VJM technique is widely 
used at the pre-design stage and is also convenient for the 
PKM benchmarking. 

Let us apply the VJM method to evaluate the stiffness of 
the Orthoglide. However, in contrast to the previous work 
[14], let us consider the model, which does not include 
auxiliary revolute joints that are usually added to ensure 
feasibility of the traditional VJM technique. Thus, each leg 
includes an actuated joint ? and three passive joints q1, q2, q3 
presented in Fig. 6 (assuming that the parallelogram posture 
is defined by a single joint variable q2). The flexibility of 
each leg is modeled by seven virtual joints ?0 � ?6 
presented in Table 2 where Lf, bf, hf are the geometrical 
parameters of the foot (length and cross-sectional 
dimensions), LB, SB and d are the parallelogram geometrical 
parameters, E and G are Young�s & Coulomb�s modulus of 
the link material, and If1, If2, I0 are the quadratic and polar 
moments for the corresponding cross-sections. Under this 
assumption, the differential kinematic model of each leg can 
be presented as 

 i
q

iii qJ?Jt ??+??=? ? )()(  (1) 
where the superscript i?{ x, y, z } refers to the particular 

manipulator leg, Tiii ],,[ 60 ????=? K?  is vector of the 
reactive joint coordinates (which respond to the external 

force), Tiii qq ],,,[ 31 ??=? Kq  is the vector of the passive 
joint coordinates (which do not respond to the force), 

T
zyxzyx ppp ],,,,,[ ?????????? =t  is the deviation of 

the end-platform location (which includes both the position 
px, py, pz and orientation ?x, ?y, ?z), and J? and Jq are the 
Jacobians of the size 6�7 and 6�3 respectively. 

 

P

? 

x

y 
z 

q3 

? 

LB e 

Lf 

q2 -q2 

q1 d 

 
Figure 6: Geometry of the Orthoglide leg and location of the passive joints 

The static equilibrium condition gives supplementary 
relations, which can be derived using the principle of virtual 
work. According to it, the external force fi applied to the leg 
end-point satisfies the following equations 

 0; )()()( =?= ?? iqiiiii
TT

fJ?KfJ  (2) 

where ( ) 0 6( , )i i idiag k k
? =K K  is the stiffness matrix of the 

reactive joints. Hence, after elimination of ??I , the 
combined kinematic/static leg model can be written as 

TABLE 2. VIRTUAL JOINTS AND THEIR STIFFNESS PARAMETERS  

No Feature Stiffness Figure 

?0 Actuator stiffness actkk =0  
 

?1 Foot bending due to force F  
ff LEIk 11 3=

 
 

?2 Foot bending due to torque T  
ff LEIk 22 2=

 
 

?3 Foot tension due to torque T  
ff LGIk 03 =

 
 

?4 Foot rotation due to torque T  
ff LEIk 24 =

 
 

?5 
Parallelogram 
tension due to 

force F  
bb LESk 25 =  

 



Paskhevich A., Wenger P. et Chablat D., �Kinematic and stiffness analysis of the Orthoglide, a PKM with simple, regular workspace and homogeneous 
performances�, IEEE International Conference On Robotics And Automation, Rome, Italie, Avril, 2007. 

 

?6 
Parallelogram 
tension due to 

torque T  
bdESk cos

22
5 =

 
 

 

 
191999

)(

)()(

���

?
??
???

??=??
???

?
?????

?
???
?

00
t

q
f

J
JS

i
i

q
i

q
ii

T . (3) 

where 1( ) ( ) ( ) ( )
T

i i i i
? ? ? ??=S J K J  is the matrix of the size of 6�6 

defining influence of the reactive joints at the end-effector. 
From (3), the leg stiffness may be computed by direct 
inversion of relevant 9�9 matrix and selection of its 
diagonal part of size 6�6. However, the most reliable 
solution is based on the SVD-decomposition of the Jacobian 

( ) ( ) ( ) ( )Tq q q q
i i i i= ? ?J U V? , which yields the following 

expression: 

 ( ) ( ) ( ) ( ) 1 ( )( )
T Tq q q q

i id id i id id
? ?=K U U S U U  (4) 

where ( )qdiU  is the 6�3 matrix, which is obtained by the 
partitioning of the orthogonal matrix ( ) ( ) ( )[ ]q q qi ri di=U U U  into 
two parts corresponding to the non-zero and zero singular 
values respectively. 

Taking into account all kinematic chains (i.e. legs X, Y, 
Z) and applying the superposition principle, the stiffness 
matrix of the whole manipulator structure can be expressed 
as  

 
{ , , }

i
i x y z?

= ?K K  
Compared to our previous results on the Orthoglide 

stiffness analysis [14], the obtained expressions gives more 
clear physical meaning of the leg stiffness matrix. In 
particular, it includes the matrix product, which is usual for 
the manipulators without passive joints. Obviously, the latter 
is the primary source of the manipulator reaction to the 
external force/torque. But this reaction is slackened by the 
passive joints taken into account by the matrix Ud, which 
eliminates force/torque components in the directions u1, � 
ur that are accepted by the passive joints. Apparently, for the 
case without passive joints, this expression is reduced to the 
known one. It should be also noted that for a single 
manipulator leg, the stiffness matrix K is the singular (since 
the leg is able to respond to end-platform displacements in 
the directions ur+1, � u6 only). For instance, the rotation 
around the additional joint axis q4 used in [14] is completely 
accepted by this joint without any contribution of the 
remaining ones (both reactive and passive). The latter is 
reflected in the matrix K by having the zero row and the zero 
column, but these column/row locations within the matrix 
are different for the Orthoglide legs X, Y, Z. 

For the entire manipulator, the stiffness matrix is the non-
singular within the dextrous workspace. It is because the 
union of the �reactive� subspaces of three legs (defined by 

the linear spans of the corresponding vectors ur+1, � u6) 
completely covers the entire 6-dimentional space of the 
force/torque. For instance, the rotation around the X-axis, 
which is non-reactive for the X-leg because of the passive 
joint q4, causes the reaction of the remaining legs Y and Z.  
Hence, for the non-singular manipulator postures (when r = 
4), each leg produces an independent 2-dimensional reactive 
subspace, which in the conjunction yield the full 
force/torque space of the dimension 6.  

Using the derived expression for the matrix K, we 
computed the stiffness benchmarks for the Orthoglide 
prototype described in Section 2. As follows from relevant 
results presented in Table 3, within the dexterous workspace 
of size 200�200�200 mm3 the translational and rotational 
stiffness varies by 30�40% only, which is a good result for 
parallel manipulators that usually posses non-isotropic 
rigidity.  

The model validity has been evaluated using the FEA 
method. The obtained results demonstrated rather high 
accuracy (10�15%) allowing easy integration of the 
developed stiffness model in the design optimizations 
routines. However, experimental study produced higher 
errors, which are caused by low stiffness of the prototype 
base frame. This motivates further research, which must take 
into account additional sources of the stiffness. 

TABLE 3. STIFFNESS OF THE ORTHOGLIDE PROTOTYPE  

Translational stiffness 
[N/mm ] 

Rotational stiffness 
[N?mm/rad] 

Isotropic point (x, y, z = 0) 

310
71.200
071.20
0071.2 ?

???
?

???
?

 610
37.800
037.80
0037.8 ?

???
?

???
?

 

Point Q1 (x, y, z = -73.65 mm) 

310
65.165.065.0
65.065.165.0
65.065.065.1 ?

???
?

???
?

?? ??
??

 10
95.816.116.1
16.195.816.1
16.116.195.8

????

?
???

?

 

Point Q2 (x, y, z = +126.35 mm) 

310
61.218.218.2
18.261.218.2
18.218.261.2 ?
???
?

???
?

 610
07.1109.009.0
09.007.1109.0
09.009.007.11

????

?
???

?

 
 
Another benchmark is based on the tool displacement for the 
standard groove milling operations along the y-axis (detailed 
technical data on the manufacturing conditions are given in 
[15]). The corresponding external forces and torques are 
equal to:  



Paskhevich A., Wenger P. et Chablat D., �Kinematic and stiffness analysis of the Orthoglide, a PKM with simple, regular workspace and homogeneous 
performances�, IEEE International Conference On Robotics And Automation, Rome, Italie, Avril, 2007. 

 

 [ , , , , , 0 ]Tx y z y z x zF F F F h F h= ?F  
where  

215 ; 10 ; 25x y zF N F N F N= = ? = ?  and 100zh mm=  
The displacement vector ? t  at the point P can be found 
from the expression 
 ? = ?t K F  

with  

 [ , , , , , ]Tx y z x y zp p p? ? ? ? ?? ?? ??=t . 
However, because of the tool offset [ ]Tx y yh h h=h , the 

displacement at the point TCP (Tool Centre Point) must be 
adjusted using the expression 

 

0
0

0
0 0 0 1 1

z y x x

z x y y
TCP

y x z z

p h
p h
p h

?? ?? ?
?? ?? ?? ?? ?? ?

?? ? ? ?? ? ? ??? ? ? ?= ?? ? ? ??? ? ? ?? ? ? ?? ? ? ?
t  

which for 0x yh h= =  yields 

 

TCP
x x z y

TCP
y y z x

TCP
z z

p p h

p p h

p p

? ? ??
? ? ??
? ?

= +
= ?
=

 

Numerical results for the examined case study are 
presented in Table 4. As follows from them, the largest 
position error is achieved in the x-axis direction, it is rather 
high and is equal to 0.67 mm and 0.35 mm for the non-
constrained and over-constrained models respectively. These 
results show that the values of the design parameters that 
could not be optimized by the kinematic criteria, namely link 
sections, must be larger. 

TABLE 4. POSITION AND ORIENTATION ERRORS FOR THE MILLING 
OPERATION (ISOTROPIC CONFIGURATION) 

 ?px,  
mm 

?py 
mm 

?pz 
mm 

??x 
rad 

??y 
rad 

??z 
rad 

P-
point 

0.0792 -0.0037 -0.0092 -0.0003 0.0027 -0.0004

TCP 0.3482 0.0239 -0.0092 -0.0003 0.0027 -0.0004

 

V. CONCLUSIONS 
This paper analyzed kinematic and stiffness properties of the 
Orthoglide, a 3-axis Delta-type, overconstrained PKM that 
was designed and optimized to meet the performances of 
both serial and parallel machines. A new method was 
developed to analyse the translational and rotational 
stiffness, taking into account the overconstrained model. 
The Orthoglide was benchmarked against workspace to 
footprint ratio, velocity and force transmission factors, 
sensitivity to geometric errors, and translational and 
rotational stiffness. The isotropic design with bounded 

transmission factors, which was originally conducted to 
provide a regular workspace shape with homogeneous force 
and velocities distribution throughout the workspace, also 
proved to lead to stable stiffness and sensitivity 
performances. 

ACKNOWLEDGMENTS 
This work was partially supported by the European project 
NEXT, acronyms for �Next Generation of Productions 
Systems�, Project no� IP 011815. 

REFERENCES 
[1] J. Tlusty , J. Ziegert and S. Ridgeway, �Fundamental Comparison of the 

Use of Serial and Parallel Kinematics for Machine Tools,� Annals of 
the CIRP, Vol.48:1, pp.351-356, 1999. 

[2] P. Wenger, C. Gosselin, and B. Maille, �A comparative study of serial 
and parallel mechanism topologies for machine tools,� in Proc. 
PKM�99, Milan, Italy, 1999, pp. 23�32. 

[3] D. Chablat and P. Wenger., �Architecture Optimization of a 3-DOF 
Parallel Mechanism for Machining Applications, the Orthoglide,� 
IEEE Transactions On Robotics and Automation, Vol. 19/3, pp. 403-
410, June 2003. 

[4] R. Clavel. Delta, �A fast robot with parallel geometry,� In 18th Int. 
Symp. On Industrial Robots, pages 91�100, Lausanne, April 1988. IFS 
Publications. 

[5] S. Caro, P. Wenger, F. Bennis and Chablat D., �Sensitivity Analysis of 
the Orthoglide, a 3-DOF Translational Parallel Kinematic Machine,� 
ASME Journal of Mechanical Design, Vol. 128, pp. 392-402, March 
2006. 

[6] A. Pashkevich D. Chablat and P. Wenger, �Kinematics and Workspace 
Analysis of a Three-Axis Parallel Manipulator: the Orthoglide,� 
Robotica, january 2006. 

[7] A. Pashkevich, P. Wenger and D. Chablat, �Design Strategies for the 
Geometric Synthesis of Orthoglide-type Mechanisms,� Journal of 
Mechanism and Machine Theory, Volume 40, Issue 8, pp. 907-930, 
August 2005. 

[8] D. Chablat, P. Wenger, F. Majou and J-P Merlet, �An Interval 
Analysis Based Study for the Design and the Comparison of 3-DOF 
Parallel Kinematic Machines,� International Journal of Robotics 
Research, 2004. 

[9] O. Company, F. Pierrot, and J.C. Fauroux, �A Method for Modeling 
Analytical Stiffness of a Lower Mobility Parallel Manipulator,� In: 
IEEE International Conference on Robotics and Automation (ICRA), 
pp. 3243�3248, Barcelona, Spain, April 2005. 

[10] R. Rizk, N. Andreff, J.C. Fauroux, J.M. Lavest and G. Gogu, 
�Precision study of a decoupled four degrees of freedom parallel robot 
including manufacturing and assembling errors,� In: 6th International 
Conference on Integrated Design and Manufacturing in Mechanical 
Engineering (IDMME 2006), 12 pp, Grenoble, France, May 2006, 12p 
(CD-ROM Proceedings). 

[11] B.C. Bouzgarrou, J.C. Fauroux, G. Gogu and Y. Heerah, �Rigidity 
analysis of T3R1 parallel robot uncoupled kinematics,� in: Proc. of the 
35th International Symposium on Robotics (ISR), Paris, France, March 
2004, 6 p. 

[12] D. Deblaise, X.Hernot and P.Maurine, �A Systematic Analytical 
Method for PKM Stiffness Matrix Calculation,� In: IEEE 
International Conference on Robotics and Automation (ICRA), 
pp.4213-4219, Orlando, Florida - May 2006.  

[13] C.M. Gosselin, �Stiffness mapping for parallel manipulators,� IEEE 
Transactions on Robotics and Automation, Vol. 6, 1990, pp. 377�382. 

[14] F. Majou, C. Gosselin, P. Wenger, D. Chablat. �Parametric stiffness 
analysis of the Orthoglide,� Mechanism and Machine Theory, 2006, in 
press.  

[15] F. Majou, C. Gosselin, P. Wengerand D. Chablat, �Parametric 
Stiffness Analysis of the Orhtoglide,� International Symposium on 
Robotics, 2004. 

 


