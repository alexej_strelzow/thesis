
ar
X

iv
:0

70
7.

20
06

v1
  [

cs
.R

O]
  1

3 J
ul 

20
07

Working modes and aspects in fully parallel manipulators

Damien Chablat Philippe Wenger

Institut de Recherche en Cyberne�tique de Nantes

E�cole Centrale de Nantes

1, rue de la Noe�, 44321 Nantes, France

Damien.Chablat@lan.ec-nantes.fr Philippe.Wenger@lan.ec-nantes.fr

Abstract

The aim of this paper is to characterize the notion
of aspect in the workspace and in the joint space for
parallel manipulators. In opposite to the serial manip-
ulators, the parallel manipulators can admit not only
multiple inverse kinematic solutions, but also multiple
direct kinematic solutions. The notion of aspect in-
troduced for serial manipulators in [1], and redefined
for parallel manipulators with only one inverse kine-
matic solution in [2], is redefined for general fully par-
allel manipulators. Two Jacobian matrices appear in
the kinematic relations between the joint-rate and the
Cartesian-velocity vectors, which are called the �in-
verse kinematics� and the �direct kinematics� matri-
ces. The study of these matrices allow to respectively
define the parallel and the serial singularities. The no-
tion of working modes is introduced to separate inverse
kinematic solutions. Thus, we can find out domains
of the workspace and the joint space exempt of sin-
gularity. Application of this study is the moveability
analysis in the workspace of the manipulator as well
as path-planing and control. This study is illustrated
in this paper with a RR-RRR planar parallel manipu-
lator.

KEY WORDS : Kinematics, Fully Parallel Manip-
ulator, Aspects, Working modes, Singularity.

1 Introduction

A well known feature of parallel manipulators is the
existence of multiple solutions to the direct kinematic
problem. That is, the mobile platform can admit sev-
eral positions and orientations (or configurations) in
the workspace for one given set of input joint values
[3]. Moreover, parallel manipulators exist with multi-
ple inverse kinematic solutions. This means that the
mobile platform can admit several input joint values
corresponding to one given configuration of the end-

effector. To cope with the existence of multiple inverse
kinematic solutions in serial manipulators, the notion
of aspects was introduced in [1]. The aspects equal the
maximal singularity-free domains in the joint space.
For usual industrial serial manipulators, the aspects
were found to be the maximal sets in the joint space
where there is only one inverse kinematic solution.

A definition of the notion of aspect was given by [2]
for parallel manipulators with only one inverse kine-
matic solution. These aspects were defined as the
maximal singularity-free domains in the workspace.
For instance, this definition can apply to the Stewart
platform [4].

First of all, the working modes are introduced to al-
low the separation of the inverse kinematic solutions.
Then, a general definition of the notion of aspect is
given for all fully parallel manipulators. The new
aspects are the maximal singularity-free domains of
the Cartesian product of the workspace with the joint
space.

A possible use of these aspects are the determina-
tion of the best working mode. It allows to achieve
complex task in the workspace or to make path-
planing without collision. As a matter of fact, cur-
rently, the parallel manipulators possessing multiple
inverse kinematic solutions evolve only in one working
mode. For a given working mode, the aspect associ-
ated is different. It is possible to choose one or several
working modes to execute the tasks expected in the
maximal workspace of the manipulator.

2 Preliminaries

In this paragraph, some definitions permitting to
introduce the general notion of aspect are quoted.

2.1 The fully parallel manipulators

Definition 1 A fully parallel manipulator is a mech-
anism that includes as many elementary kinematic




chains as the mobile platform does admit degrees of
freedom. Moreover, every elementary kinematic chain
possesses only one actuated joint (prismatic, pivot or
kneecap). Besides, no segment of an elementary kine-
matic chain can be linked to more than two bodies [3].

In this study, kinematic chains are always indepen-
dent. This condition is necessary to find the working
modes. Also, the elementary kinematic chains can be
called �legs of the manipulator� [5].

2.2 Kinematic relations

For a manipulator, the relation permitting the con-
nection of input values (q) with output values (X) is
the following

F (X,q) = 0 (1)

This definition can be applied to serial or parallel ma-
nipulators. Differentiating equation (1) with respect
to time leads to the velocity model

At+Bq? = 0 (2)

With

t =

[
w

c?

]
For planar manipulators.

t =
[
w
]
For spherical manipulators.

t =

[
w

c?

]
For spatial manipulators.

Where w is the scalar angular-velocity and c? is the
two-dimensional velocity vector of the operational
point of the moving platform for the planar manip-
ulator. For the spherical and the spatial manipulator,
w is the three-dimensional angular velocity-vector of
the moving platform. And c? is the three-dimensional
velocity vector of the operational point of the moving
platform for the spatial manipulator.

Moreover, A and B are respectively the direct-
kinematics and the inverse-kinematics matrices of the
manipulator. A singularity occurs whenever A or B,
(or both) that can no longer be inverted. Three types
of singularities exist [6]:

det(A) = 0

det(B) = 0

det(A) = 0 and det(B) = 0

2.3 Parallel singularities

Parallel singularities occur when the determinant of
the direct kinematics matrix A vanishes. The corre-
sponding singular configurations are located inside the

workspace. They are particularly undesirable because
the manipulator can not resist any force and control
is lost.

2.4 Serial singularities

Serial singularities occur when the determinant of
the inverse kinematics matrix B vanishes. When the
manipulator is in such a singularity, there is a direction
along which no Cartesian velocity can be produced.

2.5 Postures and assembling modes

The multiple inverse kinematic solutions induce
multiple postures for each leg.

Definition 2 A posture changing trajectory is equi-
valent to a trajectory between two inverse kinematic
solutions.

The multiple direct kinematic solutions induce multi-
ple assembling modes for the mobile platform.

Definition 3 An assembling mode changing trajec-
tory is equivalent to a trajectory between two direct
kinematic solutions.

As an example, the 3-RRR planar parallel manip-
ulator (the first joints are actuated joints), a posture
changing trajectory exists between two inverse kine-
matic solutions (Fig. 1) and an assembling mode tra-
jectory exits between two direct kinematic solutions
(Fig. 2). In these trajectories, the mobile platform
can meet a singular configuration.

Figure 1: Two postures

2.6 Working Modes

The working modes are defined for fully parallel ma-
nipulators (Def. 1). From this definition, the inverse-
kinematic matrix is always diagonal. For a manipula-
tor with n degrees of freedom, the inverse kinematic
matrixB is like in eq. (3). Each term Bjj is associated



Figure 2: Two assembling modes

with one leg. Its vanishing induces the apparition of
a serial singularity.

B =

?
????????

B11 0 � � � � � � 0

0
. . .

. . .
...

... Bjj
...

...
. . .

. . . 0
0 � � � � � � 0 Bnn

?
????????

(3)

Let W be the reachable workspace, that is, the set of
all positions and orientations reachable by the moving
platform ([7] and [8]). Let Q be the reachable joint
space, that is, the set of all joint vectors reachable by
actuated joints.

Definition 4 A working mode, noted Mfi, is the set
of postures for which the sign of Bjj (j = 1 to n) does
not change and Bjj does not vanish.

Mfi =

??
?(X,q) ?W �Q \

sign(Bjj) = constant
for(j = 1 to n)
and det(B) 6= 0

??
?
(4)

Therefore, the set of working modes (Mf = {Mfi},
i ? I) is obtained while using all permutations of sign
of each term Bjj.

The Cartesian product of W by Q is noted W � Q.
According to the joint limit values, all working modes
do not necessarily exist. Changing working mode is
equivalent to changing the posture of one or several
given legs. The working modes are defined in W � Q
because the terms Bjj depend on both X and q.

Theorem 1 The working modes separate inverse
kinematic solutions if and only if the legs are not cus-
pidal (see [9]).

Proof 1 If one leg is cuspidal then this leg can make
a changing posture trajectory without meeting a se-
rial singularity. In this case no Bjj vanishes during

this trajectory. Reciprocally, if no leg is cuspidal, then
the changing posture trajectory of one leg induces that
some Bjj can vanish.

In this study, the legs are not cuspidal so that the
working modes allow the separation of the inverse
kinematic solutions. The list of the most current non-
cuspidal serial chains is given in [9].

Example 1 For the robot Delta [10], a 3-dof manip-
ulator (Fig. 3), there are 8 working modes (3 legs
and 2 postures for each leg, with 23 = 8 working
modes). And for the Hexa robot [11], a 6-dof manipu-
lator (Fig. 4), there are 64 working modes (26 = 64).
For these manipulators, the serial singularities occur
when one or more legs are outstretched.

Figure 3: The Delta ma-
nipulator

Figure 4: The Hexa ma-
nipulator

2.7 Notion of aspect for fully parallel ma-
nipulators: General definition

The notion of aspect was introduced by [1] to cope
with the existence of multiple inverse kinematic so-
lutions in serial manipulators. Recently, the notion
of aspect was defined for parallel manipulators with
only one inverse kinematic solution [2] to cope with
the existence of multiple direct kinematic solutions.

In this section, the notion of aspect is redefined
formally for fully parallel manipulators with multiple
inverse and direct kinematic solutions.

Definition 5 The generalized aspects Aij are defined
as the maximal sets in W �Q so that

� Aij ?W �Q;

� Aij is connected;



� Aij = {(X,q) ?Mfi \ det(A) 6= 0}

In other words, the generalized aspects Aij are the
maximal singularity-free domains of the Cartesian
product of the reachable workspace with the reach-
able joint space.

Definition 6 The projection of the generalized as-
pects in the workspace yields the parallel aspects WAij
so that

� WAij ?W ;

� WAij is connected.

The parallel aspects are the maximal singularity-free
domains in the workspace for one given working mode.

Definition 7 The projection of the generalized as-
pects in the joint space yields the serial aspects QAij
so that

� QAij ? Q;

� QAij is connected.

The serial aspects are the maximal singularity-free do-
mains in the joint space for one given working mode.

3 A Two-DOF Closed-Chain Manipu-

lator

For more legibility, a planar manipulator is used
as illustrative example in this paper. This is a five-
bar, revolute (R)-closed-loop linkage, as displayed in
Fig. 5. The actuated joint variables are ?1 and ?2,
while the Output values are the (x, y) coordinates of
the revolute center P . The passive joints will always
be assumed unlimited in this study. Lengths L0, L1,
L2, L3, and L4 define the geometry of this manipulator
entirely. We assume here the dimensions L0 = 9,
L1 = 8, L2 = 5, L3 = 5 and L4 = 8 , in certain
units of length that we need not specify.

3.1 Kinematic Relations

The velocity p? of point P , of position vector p, can
be obtained in two different forms, depending on the
direction in which the loop is traversed, namely,

p? = c?+ ??3E(p? c) (5a)

p? = d?+ ??4E(p? d (5b)

Figure 5: A two-dof closed-chain manipulator

with matrix E defined as

E =

[
0 ?1
1 0

]

and c and d denoting the position vectors, in the frame
indicated in Fig. 5, of points C and D, respectively.
Furthermore, note that c? and d? are given by

c? = ??1Ec, d? = ??2E(d? b)

We would like to eliminate the two idle joint rates ??3
and ??4 from eqs.(5a) and (5b), which we do upon dot-
multiplying the former by p?c and the latter by p?d,
thus obtaining

(p? c)T p? = (p? c)T c? (6a)

(p? d)T p? = (p? d)T d? (6b)

Equations (6a) and (6b) can now be cast in vector
form, namely,

Ap? = B?? (7a)

with ?? defined as the vector of actuated joint rates,
of components ??1 and ??2. Moreover A and B are,
respectively, the direct-kinematics and the inverse-
kinematics matrices of the manipulator, defined as

A =

[
(p? c)T

(p? d)T

]
(7b)

and

B =

[
L1L2 sin(?3 ? ?1) 0

0 L3L4 sin(?4 ? ?2)

]
(7c)

3.2 Parallel singularities

For the manipulator studied, the parallel singulari-
ties occur whenever the points C, D, and P are aligned
(6). Manipulator postures whereby ?3 ? ?4 = kpi de-
note a singular matrixA, and hence, define the bound-
ary of the Joint space of the manipulator.



Figure 6: Example of parallel singularity

3.3 Serial singularities

For the manipulator at hand, the serial singularity
occur whenever the points A, C, and P or the points
B, D, and P are aligned (7). Manipulator postures
whereby ?3 ? ?1 = kpi or ?4 ? ?2 = kpi denote a singu-
lar matrix B, and hence, define the boundary of the
Cartesian workspace of the manipulator.

Figure 7: Example of serial singularity

3.4 The Working Mode

The manipulator under study has a diagonal
inverse-kinematics matrix B, as shown in eq. (7c).
There are four working modes, as depicted in
Fig. 8. The different working modes in the Carte-
sian workspace and in the Joint space are displayed in
figures 9, 10, 11 and 12.

3.5 The generalized aspects

For the manipulator at hand, the generalized as-
pects are defined with the definition 5. Figures 13-
20 depict the different serial and parallel aspects ob-
tained. Table 1 shows that there are 10 serial/parallel
aspects for the manipulator (N and P stand for nega-
tive and positif, respectively).

Figure 8: The four working modes

Figure 9: Mf1 Figure 10: Mf2

Figure 11: Mf3 Figure 12: Mf4

In opposite to the aspects defined by [1] or by [2],
the generalized aspects are not disjoint. Only, the as-
pects belonging to the same working mode are disjoint.
In the sample in which the manipulator has only one
inverse kinematic solution, the notion of generalized
aspect is equivalent to the notion of aspect given by
[2]. Indeed, if the manipulator has only one working
mode (like the 2-RPR planar manipulator), then the
parallel aspects are disjoint and represent the maximal
singularity-free domain in the Cartesian workspace.

4 Conclusions

In this paper, the notion of aspect was defined for
parallel manipulators with multiple inverse and direct
kinematic solutions. The working modes were intro-
duced to define this notion. For one working mode, we
can find out the maximal singularity-free domains of
the Cartesian product of the workspace with the joint



Figures 13 14 15 16 17 18 19 20
det(A) P P P P N N N N
B11 P P N N P P N N
B22 P N N P N P P N
Nb of

generalized

aspects

1 1 1 2 1 2 1 1

Table 1: The generalized aspects

space. This work brings material to further investi-
gations like trajectory planning and kinematic design
which are the subject of current research work from
the authors.

The generalized aspect are not the uniqueness do-
main in any case as it was shown in [2]. So, in a future
study, the authors will define uniqueness domains for
general fully parallel manipulators. In such domains,
there are only one inverse and direct kinematic solu-
tion. These domains are of interest for the control of
manipulator.

References

[1] Borrel, P. �A study of manipulator inverse kine-
matic solutions with application to trajectory
planning and workspace determination� Proceed-
ing IEEE International Conference on Robotic
And Automation, pp 1180-1185, 1986.

[2] Wenger, Ph. and Chablat, D. �Uniqueness Do-
mains in the Workspace of Parallel Manipulators�
IFAC-SYROCO, Vol. 2, pp 431-436, 3-5 Sept.,
1997, Nantes.

[3] Merlet J-P. �Les robots paralle`les� HERMES,
seconde e�dition, Paris, 1997.

[4] Stewart, D. �A platform with 6 degree of free-
dom� Proc. of the Institution of mechanical en-
gineers, 180 (Part 1,15):371-386, 1965.

[5] Angeles, J. �Fundamentals of Robotic Mechani-
cal Systems� SPRINGER 97.

[6] Gosselin, C. and Angeles, J. �Singularity analysis
of closed-loop kinematic chains� IEEE Transac-
tions On Robotics And Automation, Vol. 6, No. 3,
June 1990.

[7] Kumar V. �Characterization of workspaces of
parallel manipulators� ASME J. Mechanical De-
sign, Vol. 114, pp 368-375, 1992.

Figure 13: Aspect 1 Figure 14: Aspects 2

Figure 15: Aspect 3 Figure 16: Aspects 4 and 5

Figure 17: Aspect 6 Figure 18: Aspects 7 and 8

Figure 19: Aspect 9 Figure 20: Aspect 10

[8] Pennock, G.R. and Kassner, D.J. �The workspace
of a general geometry planar three-degree-of-
freedom platform-type manipulator� ASME J.
Mechanical Design, Vol. 115, pp 269-276, 1993.

[9] Wenger, Ph. �A classification of Manipulator Ge-
ometries Based on Singularity Avoidance Ability�
ICAR�93, pp 649-654, Tokyo, Japan, Nov. 1993.

[10] Clavel, R. �A fast robot with parallel geometry�
In 18th International Symposium on Industrial
Robot, pages 91-100, Lausanne, 26-28 Avril 1988.

[11] Pierrot, F. �Robot Pleinement Paralle`les Le�gers :
Conception, Mode�lisation et Commande� Doc-
torat thesis, 1991, Montpellier.


	Introduction
	Preliminaries
	The fully parallel manipulators
	Kinematic relations
	Parallel singularities
	Serial singularities
	Postures and assembling modes
	Working Modes
	Notion of aspect for fully parallel manipulators: General definition

	A Two-DOF Closed-Chain Manipulator
	Kinematic Relations
	Parallel singularities
	Serial singularities
	The Working Mode
	The generalized aspects

	Conclusions

