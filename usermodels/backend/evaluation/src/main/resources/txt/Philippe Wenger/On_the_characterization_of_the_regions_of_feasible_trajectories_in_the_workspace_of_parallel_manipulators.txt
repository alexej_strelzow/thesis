
ar
X

iv
:0

91
0.

55
59

v1
  [

cs
.R

O]
  2

9 O
ct 

20
09

TENTH WORLD CONGRESS ON THE THEORY OF

MACHINES AND MECHANISMS

Oulu, Finland, June 20-24, 1999

ON THE CHARACTERIZATION OF THE REGIONS OF FEASIBLE

TRAJECTORIES IN THE WORKSPACE OF PARALLEL

MANIPULATORS

Damien Chablat1 Philippe Wenger2

1McGill Centre for Intelligent Machines 1INRIA Rocquencourt 2Institut de Recherche

McGill University Domaine de Voluceau en Cyberne�tique de Nantes

817 Sherbrooke Street West B.P. 105 1, rue de la Noe�

Montreal, Quebec, Canada H3A 2K6 78 153 Le Chesnay France 44321 Nantes, France

Chablat@cim.mcgill.ca Philippe.Wenger@ircyn.ec-nantes.fr

Key words : Fully Parallel Manipulator, Singularity, Point-to-Point Trajectory, Continuous

Trajectory, Octree

1 Introduction

It was shown recently that parallel manipulators with several inverse kinematic solutions have
the ability to avoid parallel singularities [Chablat 1998a] and self-collisions [Chablat 1998b] by
choosing appropriate joint configurations for the legs. In effect, depending on the joint configu-
rations of the legs, a given configuration of the end-effector may or may not be free of singularity
and collision. Characterization of the collision/singularity-free workspace is useful but may be
insufficient since two configurations can be accessible without collisions nor singularities but it
may not exist a feasible trajectory between them.

The goal of this paper is to define the maximal regions of the workspace where it is possible
to execute trajectories. Two different families of regions are defined : 1. those regions where
the end-effector can move between any set of points, and 2. the regions where any continuous
path can be tracked. These regions are characterized from the notion of aspects and free-aspects
recently defined for parallel manipulators [Chablat 1998b]. The construction of these regions
is achieved by enrichment techniques and using an extension of the octree structures to spaces
of dimension greater than three. Illustrative examples show the interest of this study to the
optimization of trajectories and the design of parallel manipulators.

2 Preliminaries

2.1 Kinematics

The input vector q (the vector of actuated joint values) is related to the output vector X (the
vector of configuration of the moving platform) through the following general equation :

F (X,q) = 0 (1)




Vector (X, q) will be called manipulator configuration and X is the platform configuration and
will be more simply termed configuration. Differentiating equation (1) with respect to time leads
to the velocity model

At+Bq? = 0 (2)

With t = [w, c?]T , for planar manipulators (w is the scalar angular-velocity and c? is the two-
dimensional velocity vector of the operational point of the moving platform), t = [w]T , for
spherical manipulators and t = [w, c?]T , for spatial manipulators (c? is the three-dimensional
velocity vector and w is the three-dimensional angular velocity-vector of the operational point
of the moving platform).

Moreover, A and B are respectively the direct-kinematics and the inverse-kinematics matrices
of the manipulator. A singularity occurs whenever A or B, (or both) can no longer be inverted.
Three types of singularities exist [Gosselin 1990]:

(1) det(A) = 0 (2) det(B) = 0 (3) det(A) = 0 and det(B) = 0

2.2 Parallel and serial singularities

Parallel singularities occur when the determinant of the direct kinematics matrix A vanishes.
The corresponding singular configurations are located inside the workspace. They are particu-
larly undesirable because the manipulator can not resist any effort and control is lost.

Serial singularities occur when the determinant of the inverse kinematics matrix B vanishes.
By definition, the inverse-kinematic matrix is always diagonal: for a manipulator with n degrees
of freedom, the inverse kinematic matrix B can be written as B = Diag [B11, ...,Bjj , ...,Bnn],
each term Bjj being associated with one leg. A serial singularity occurs whenever at least one of
these terms vanishes. When the manipulator is in a serial singularity, there is a direction along
which no Cartesian velocity can be produced.

2.3 Point-to-point trajectory

There are two major types of tasks to consider : point-to-point trajectories and continuous
trajectories. We consider in this section point-to-point trajectories.
Definition : A point-to-point trajectory Tpp is defined by a set of p configurations in the

workspace : Tpp = {X1, ...,Xi, ....,Xp}. By definition, no path is prescribed between any two
configurations Xi and Xj .
Hypothesis : In a point-to-point trajectory, the moving platform can not move through a

parallel singularity.
Although it was shown recently that in some particular cases a parallel singularity could be

crossed [Nenchev 1997], hypothesis 1 is set for the most general cases.
A point-to-point trajectory Tpp will be feasible if there exists a continuous path in the Carte-

sian product of the workspace by the joint space which does not meet a parallel singularity and
which makes the moving platform pass through all prescribed configurations Xi of the trajectory
Tpp.

2.4 Continuous Trajectory

Definition : A continuous trajectory Tc is defined by a parametric curve in the operational
space as : Tc = ?([0, 1]) where ? is a continuous function defined on [0, 1] and differentiable by
parts on this interval.
Hypothesis : In a continuous trajectory, the moving platform cannot meet a parallel

singularity nor a serial singularity.
A continuous trajectory Tc will be feasible if there exists a continuous path in the Cartesian

product of the workspace by the joint space which is free of parallel and serial singularities and
which makes the platform move along the continuous trajectory Tc.



Remark : A fully parallel manipulator with several in-
verse kinematic solutions can change its joint configuration
between two prescribed configurations. Such a manoeuver
may enable the manipulator to avoid a parallel singularity
(Figure 1). More generally, the choice of the joint configu-
ration for each configuration Xi of the trajectory Tpp can be
established by any other criteria like stiffness or cycle time
[Chablat et al. 1998]. Note that a change of joint configu-
ration makes the manipulator run into a serial singularity,
which is not redhibitory for the feasibility of point-to-point
trajectories.

A B

PC
D

C

Fig. 1: Singular (solid) and a regu-

lar (dotted) configurations (the ac-

tuated joints are A and B)

3 The N-connected regions

Definition : The N-connected regions of the workspace are the maximal regions where
any point-to-point trajectory is feasible. For manipulators with multiple inverse and direct
kinematic solutions, it is not possible to study the joint space and the workspace separately.
First, we need to define the regions of manipulator reachable configurations in the Cartesian
product of the workspace by the joint space W.Q.
Definition : The regions of manipulator reachable configurations Rj are defined as the

maximal sets in W.Q such that

� Rj ?W.Q,

� Rj is connected,

� Rj = {X,q} such that det(A) 6= 0

In other words, the regions Rj are the sets of all configurations (X, q) that the manipulator can
reach without meeting a parallel singularity and which can be linked by a continuous path in
W.Q.
Proposition : A trajectory Tpp = {X1, ...,Xp} defined in the workspace W is feasible if

and only if :

?X ? {X1, ...,Xp}, ?qi ? Q,?Rj such that (Xi,qi) ? Rj

In other words, for each configuration Xi in Tpp, there exists at least one joint configuration qi
and one region of manipulator reachable configurations Rj such that the manipulator configu-
ration (Xi,q) is in Rj .
Proof : Indeed, if for all configurations Xi, there is one joint configuration qi such that

(Xi,qi) ? Rj then the trajectory is feasible because, by definition, a region of manipulator
reachable configurations is connected and free of parallel singularity. Conversely, if for a given

configuration Xi, it is not possible to find a joint configuration qi such that (Xi,qi) ? Rj , then
no continuous, parallel singularity-free path exists in W.Q which can link the other prescribed

configurations.

Theorem : The N-connected regions WNj are the projection ?W of the regions of manip-
ulator reachable configurations Rj onto the workspace:

WNj = ?WRj

Proof : This results is a straightforward consequence of the above proposition.

The N-connected regions cannot be used directly for planning trajectories in the workspace
since it is necessary to choose one joint configuration q for each configuration X of the moving



platform such that (X,q) is included in the same region of manipulator reachable configurations
Rj . However, the N-connected regions provide interesting global information with regard to the
performances of a fully parallel manipulators because they define the maximal regions of the
workspace where it is possible to execute any point-to-point trajectory.

A consequence of the above theorem is that the workspace W is N-connected if and only if
there exists a N-connected region WNj which is coincident with the workspace :

WNj =W

4 The T-connected regions

Definition : The T-connected regions of the workspace are the maximal regions where any
continuous trajectory is feasible.

When the mobile platform moves along a continuous trajectory, a fully parallel manipula-
tor cannot pass through a serial or a parallel singularity. The aspects Aij [Chablat 1998b]
characterize the maximal domains without serial and parallel singularity.

We recall below the definition of an aspect :

� Aij ?W �Q;

� Aij is path-connected;

� Aij = {(X,q) ?W �Q such that det(A) 6= 0 and det(B) 6= 0}

Proposition : A continuous trajectory Tc = {X?, ? ? [0, 1])} defined in the workspace W
is feasible if and only if :

?? ? [0, 1],?q ? Q such that (X?,q) ? Aij

In other words, for all configurations X? in Tc, there exists at least one joint configuration q
such that the manipulator configuration (X?,q) is in Aij .
Proof : Indeed, if for all configurations X?, there is one joint configuration q such that

(X?,q) ? Aij then the trajectory is feasible because, by definition, an aspect is connected and
free of serial and parallel singularity. Conversely, if for a given configuration X?, it is not

possible to find a joint configuration q such that (X?,q) ? Aij, then no continuous singularity-
free path exists in W.Q which can link the other prescribed configurations.

Theorem : The T-connected regions WTj are the projection ?W of the aspect Aij onto
the workspace:

WTj = ?WAij

Proof : This results is a straightforward consequence of the above proposition. A consequence
of the above theorem is that the workspace W is T-connected if and only if there exists a T-
connected region WTj which is coincident with the workspace :

WTj =W

5 Example: A Two-DOF fully parallel manipulator

For more legibility, a planar manipulator is used as illustrative example in this paper. This is a
five-bar, revolute (R)-closed-loop linkage, as displayed in figure 2. The actuated joint variables
are ?1 and ?2, while the Output values are the (x, y) coordinates of the revolute center P . The
passive joints will always be assumed unlimited in this study. Lengths L0 = 7, L1 = 8, L2 = 5,



L3 = 8, and L4 = 5 define the geometry of this manipulator entirely, in certain units of length
that we need not specify. The actuated joints are not unlimited ?1, ?2 ? [0, pi]. In order to point
out that the workspace of a parallel manipulator may not be N-connected even when there is no
obstacle obstructions, it is assumed here that the manipulator is free of self-collisions. Examples
with collision analyses have been investigated and can be found in the PhD thesis of the first
author of this paper [Chablat 1998c].

The workspace is shown in figure 3. We want to know whether this manipulator can execute
any point-to-point motion or continuous trajectory in the workspace. To answer this question,
we need to determine the the N-connected regions and the T-connected regions.

5.1 N-connected regions

It turns out, although there is no obstacles obstructions,

y

A

P(x,y)

B

x
q

2

DC

L
3

q
1

L
4L2

L
1

L0

Fig. 2: A two-dof fully parallel ma-

nipulator

that the workspace of the manipulator at hand is not N-
connected, e.g. the manipulator cannot move its platform
between any set of configurations in the workspace. In ef-
fect, due to the existence of limits on the actuated joints, not
all joint configurations are accessible for any configuration in
the workspace. Thus, the manipulator may lose its ability to
avoid a parallel singularity when moving from one configu-
ration to another. This is what happens between points X1
and X2 (Figure 3). These two points cannot be linked by the
manipulator although they lie in the workspace which is con-
nected in the mathematical sense (path-connected) but not
N-connected. In fact, there are two separate N-connected
regions which do not coincide with the workspace and the
two points do not belong to the same N-connected region
(Figures 4 and 5). We know that the workspace of serial manipulators is always N-connected
when there is no collision [Wenger 1991]. The example treated in this section shows that this is
false when the manipulator is parallel.

Y

X

X2 X1

Fig. 3: The workspace

Y

X

X2 X1

Fig. 4: The first N-connected re-

gion of the workspace

Y

X2 X1

X

Fig. 5: The second N-connected

region of the workspace

5.2 T-connected regions

The T-connected regions are the projection onto the workspace of the aspects. These regions are
smaller than the N-connected regions. According to the joint configuration of the manipulator,
i.e. according to the choice of the inverse kinematic solution, the singularity-free regions differ.
Figures 6, 7, 8 and 9 depict the T-connected regions corresponding to det(A) > 0. Their union
forms the first N-connected region (Figure 4). Figures 10, 11, 12 and 13 show the T-connected
regions such that det(A) < 0 and their union is the second N-connected region (Figure 5).



X
Y

0

Fig. 6: Joint configura-

tion 1

X

Y

0

Fig. 7: Joint configura-

tion 2

X

Y

0

Fig. 8: Joint configura-

tion 3

X

Y

0

Fig. 9: Joint configura-

tion 4

X

Y

0

Fig. 10: Joint configu-

ration 5

X

Y

0

Fig. 11: Joint configu-

ration 6

X

Y

0

Fig. 12: Joint configu-

ration 7

X

Y

0

Fig. 13: Joint configu-

ration 8

6 Conclusions

In this paper, the N-connected and the T-connected regions have been defined to characterize
the maximal regions of the workspace where any point-to-point and continuous trajectories are
feasible, respectively.

The manipulators considered in this study have multiple solutions to their direct and inverse
kinematics. The N-connected regions were defined by first determining the maximum path-
connected, parallel singularity-free regions in the Cartesian product of the workspace by the
joint space. The projection of these regions onto the workspace were shown to define the N-
connected regions. The T-connected regions were defined by the projection onto the workspace
of the aspects, i.e. the maximal serial and parallel singularity-free domains in the Cartesian
product of the workspace by the joint space.

The N-connectivity and the T-connected analysis of the workspace are of high interest for the
evaluation of manipulator global performances as well as for off-line task programming.

7 References

Chablat, D. and Wenger, Ph., �Working Modes and Aspects in Fully-Parallel Manipulator�,
IEEE Int. Conf. On Robotics and Automation, pp. 1964-1969, 1998.

Chablat, D. and Wenger, Ph., �Moveability and Collision Analysis for Fully-Parallel Manip-
ulators�, 12th CISM-IFTOMM Symposium, RoManSy, 1998.

Wenger, Ph., Chedmail, P., �Ability of a Robot to Travel Through its Free Workspace�, The
Int. Jour. of Robotic Research, Vol. 10:3, June 1991.

Gosselin, C. and Angeles, J., �Singularity analysis of closed-loop kinematic chains�, IEEE
Trans. On Robotics And Automation, Vol. 6, No. 3, June 1990.

Nenchev, D.N., Bhattacharya, S., and Uchiyama, M.,�Dynamic Analysis of Parallel Manipu-
lators under the Singularity-Consistent Parameterization�, Robotica, Vol. 15, pp.375-384,1997.

Chablat, D., Wenger, Ph. , Angeles, J., �The isoconditioning Loci of A Class of Closed-Chain
Manipulators�, IEEE Int. Conf. On Robotics and Automation, pp. 1970-1975, May 1998.

Chablat, D., �Domaines d�unicite� et parcourabilite� pour les manipulateurs pleinement par-
alle`les�, PhD thesis, Nantes, November 1998.


	Introduction
	Preliminaries
	Kinematics
	Parallel and serial singularities
	Point-to-point trajectory
	Continuous Trajectory

	The N-connected regions
	The T-connected regions
	Example: A Two-DOF fully parallel manipulator
	N-connected regions
	T-connected regions

	Conclusions
	References

