
Assises MUGV  Nantes, 5-6 juin 2008 
 

1 
 

 

ANALYSE DE LA RIGIDIT� DES MACHINES OUTILS 3 AXES 
D'ARCHITECTURE PARALL�LE HYPERSTATIQUE 

 
Anatol Pashkevich, Damien Chablat, Philippe Wenger  
Institut de Recherche en Communications et Cybern�tique de Nantes 
1 rue de la No�, 44321 Nantes 
{Anatol.Pashkevich, Damien.Chablat, Philippe.Wenger}@irccyn.ec-nantes.fr 
 

R�sum� : Cet article pr�sente une nouvelle m�thode pour mod�liser la rigidit� des machines outils � structure 
parall�le bas�e sur une mod�lisation de la machine par des corps rigides et des flexibilit�s localis�es. Nous 
ajoutons des articulations virtuelles au mod�le rigide, qui d�crivent les d�formations �lastiques des composants 
du m�canisme (membrures, articulations et actionneurs). Cette approche provient des travaux de Cl�ment 
Gosselin, qui a �valu� la rigidit� des m�canismes parall�les, en tenant compte uniquement des actionneurs et qui 
les a mod�lis�s comme des ressorts lin�aires unidimensionnels (les membrures �taient suppos�es rigides, et les 
articulations passives parfaites). Contrairement aux �tudes pr�c�dentes, notre m�thode peut s'appliquer aux 
m�canismes hyperstatiques et l'�valuation des flexibilit�s localis�es est r�alis�e en utilisant une mod�lisation par 
�l�ments finis.  

Mots cl�s : Machines outils, rigidit�, flexibilit�s localis�es 

Abstract: The paper presents a new stiffness modelling method for overconstrained parallel manipulators, which 
is applied to 3-d.o.f. translational mechanisms. It is based on a multidimensional lumped-parameter model that 
replaces the link flexibility by localized 6-d.o.f. virtual springs. In contrast to other works, the method includes a 
FEA-based link stiffness evaluation and employs a new solution strategy of the kinetostatic equations, which 
allows computing the stiffness matrix for the overconstrained architectures and for the singular manipulator 
postures. The advantages of the developed technique are confirmed by application examples, which deal with 
comparative stiffness analysis of two translational parallel manipulators. 

Keywords: Parallel manipulators, stiffness analysis, and virtual springs.  



Assises MUGV  Nantes, 5-6 juin 2008 
 

2 
 

1 Introduction  
Comparativement aux manipulateurs s�riels, les manipulateurs parall�les (MP) sont 

r�put�s pour avoir un meilleur rapport rigidit�/poids et une meilleure pr�cision. Cette 
caract�ristique les rend attrayants pour la conception de nouvelles machines-outils pour 
l'usinage � grande vitesse [1, 2, 3]. Quand un manipulateur parall�le est utilis� comme 
machine outil, la rigidit� devient une contrainte tr�s importante lors de sa conception [4, 5, 6, 
7]. Cet article pr�sente une m�thode g�n�rale pour �valuer la rigidit� des manipulateurs � trois 
degr�s de libert� hyperstatique produisant des mouvements de translation pure. G�n�ralement, 
l'analyse de la rigidit� est bas�e sur une mod�lisation cin�tostatique [8], qui propose une carte 
de la rigidit� en prenant en compte la rigidit� des articulations motoris�es. Cependant, cette 
m�thode n'est pas appropri�e pour les MP, dont les jambes sont soumises � la flexion [9]. 

Trois mod�les existent qui permettent de prendre en compte la complaisance d'une MP 
afin d'analyser son comportement flexible : les m�thodes par �l�ments finis (MEF) [10], 
mod�lisation avec membrures rigides (MMR) [11], et mod�lisation rigide avec flexibilit�s 
localis�es (MRFL) [8 ]. Cette derni�re m�thode est bas�e sur l'expansion du mod�le 
traditionnel rigide en ajoutant les articulations virtuelles, qui d�crivent les d�formations 
�lastiques des composants du m�canisme (membrures, articulations et actionneurs). Cette 
approche provient des travaux de Gosselin [8], qui a �valu� la rigidit� des m�canismes 
parall�les, en tenant compte uniquement des actionneurs et qui les a mod�lis�s comme des 
ressorts lin�aires unidimensionnels (les membrures �taient suppos�es rigides, et les 
articulations passives parfaites). Ces hypoth�ses ont permis de r�duire l�analyse de la rigidit� 
d�un m�canisme � l�analyse de sa matrice jacobienne. Une �volution de ce mod�le, tenant 
compte des flexibilit�s dans les membrures a �t� pr�sent�e dans [10] en utilisant des corps 
rigides et des flexibilit�s par des ressorts en torsion ou en flexion. G�n�ralement, cette 
mod�lisation fournit une pr�cision suffisante et les temps de calcul sont courts. On peut donc 
l�utiliser en phase de conception pr�liminaire, en particulier pour les analyses param�triques. 
Cependant, nous verrons que des hypoth�ses de simplification qui ne prennent pas en compte 
le couplage entre la translation et de rotation peuvent aboutir � d�importantes erreurs. De plus, 
il existe �galement d'autres restrictions, qui limitent ses applications � l'absence de 
m�canismes hyperstatiques. 

Dans le paragraphe suivant, nous pr�sentons une m�thode g�n�rale pour d�duire la mod�le 
cin�matique et le mod�le de raideur. Le paragraphe 3 d�crit les matrices de complaisance de 
chaque �l�ment et leur assemblage pour d�terminer la rigidit� globale du m�canisme. 
Finalement, dans le paragraphe 4, nous appliquons notre m�thode sur deux exemples. 

2 M�thodologie 

2.1 Architecture des manipulateurs �tudi�s  
Soit un manipulateur parall�le g�n�ral � 3 degr�s de libert� en translation, poss�dant une 

plate-forme mobile connect�e � une base fixe par trois cha�nes cin�matiques identiques 
(Fig. 1).  

 Base 

Ps

Ps

Ps Ps

Ps

Ps

Ac

Mobile platform

Ps

Ps

Ps Ps

Ps

Ps

Ac

Ps

Ps

Ps Ps

Ps

Ps

Ac

L L L 

FFF

 
Fig. 1. Sch�ma d'ordre g�n�ral d'un manipulateur parall�le � 3-ddl de translation  

(Ac � articulation motoris�e, Ps � articulation passive, F - pied, L � jambe) 



Assises MUGV  Nantes, 5-6 juin 2008 
 

3 
 

Chaque jambe comprend une articulation motoris�e "Ac" (prismatique ou roto�de), suivi 
d'un pied et d'une jambe poss�dant un certain nombre d'articulation passive "P". Des 
conditions g�om�triques existent sur les articulations passivent pour permettre d'�liminer les 
mouvements de rotation de la plate-forme et d'assurer ainsi la stabilit� de ses mouvements en 
translation. Nous pr�sentons trois exemples de ces manipulateurs : 
� le robot 3-PUU (Figure 2a), o� chaque jambe est compos�e d'une tige termin�e par deux 

cardans (avec des conditions de parall�lismes entre les axes des cardans), et des 
articulations prismatiques motoris�es [13] ; 

� le robot Delta (Figure 2b), qui est une architecture de type 3-RRPaR avec un 
parall�logramme dans chaque jambe et des articulations roto�des [14] ; 

� le robot parall�le Orthoglide (Fig 2c), qui est une architecture de type 3-PRPaR avec un 
parall�logramme dans chaque jambe et des articulations prismatique motoris�es [10].  
Les lettres R, P, U et Pa repr�sentent les articulations roto�des, prismatiques, les cardans et 

les parall�logrammes, respectivement. Les exemples (b) et (c) illustrent des m�canismes 
hyperstatiques, o� certaines contraintes cin�matiques sont redondantes, mais n'affectent pas le 
nombre de degr�s de libert�. Cependant, la plupart de ces m�thodes d'analyse de la rigidit� 
des manipulateurs parall�les ne s'applique qu'� des manipulateurs isostatique [8].  

(a) Robot 3-PUU [13] (b) Robot Delta[14] (c) Robot Orthoglide [10] 

 
 

Fig. 2. Exemples de manipulateur parall�le � 3 ddl  

2.2 Hypoth�ses de base  
Pour �valuer la rigidit� d'un manipulateur, nous appliquons une petite modification � la 

m�thode des travaux virtuelle (MTV), qui est bas�e sur la m�thode des flexibilit�s localis�es 
[8, 10]. Selon cette approche, le mod�le rigide de d�part est modifi� en ajoutant des 
articulations virtuelles, qui d�crivent les d�formations �lastiques des corps rigides. De plus, 
des ressorts virtuels sont inclus dans les articulations motoris�es pour tenir compte de la 
rigidit� de la boucle de contr�le. Pour surmonter les difficult�s de mod�lisation des 
parall�logrammes, nous allons dans un premier temps les remplacer (voir Fig. 2) par des corps 
rigides dont la rigidit� d�pend de la configuration. Cette modification transforme l'architecture 
g�n�rale en un m�canisme 3-xUU et nous permet ainsi d'�tudier plusieurs manipulateurs de 
cette famille. Avec cette hypoth�se, chaque cha�ne cin�matique du manipulateur peut �tre 
d�crite par une structure s�rielle (Fig. 3), qui comprend successivement: 

 Ac

Effecteur  
(rigide)  

Base  
(rigide)

U U

ressort 
� 1 ddl

ressort 
� 6 ddl

ressort 
� 6 ddl  

Fig. 3. Mod�le flexible d'une seule cha�ne cin�matique 
� un corps rigide entre la base fixe du manipulateur et la i�me articulation motoris�e d�crite 

par une matrice de transformation homog�ne constante ibaseT  ; � une articulation motoris�e avec un degr� de libert� avec flexibilit� localis�e, qui est 
d�crite par une matrice homog�ne 0 0( )i ia q ?+V  o� 0iq  sont les coordonn�es des actionneurs 
et 0i?  les coordonn�es de l'articulation virtuelle ; � un corps rigide, le pied, reliant les actionneurs de la jambe et la jambe, qui est d�crite par 
une matrice de transformation homog�ne constante footT  ; 



Assises MUGV  Nantes, 5-6 juin 2008 
 

4 
 

� une articulation virtuelle � 6 degr�s de libert� d�finissant trois mouvement de translation 
et de rotations, qui sont d�crits en fonction d'une matrice homog�ne 1 6( , )

i i
s ? ?V K , o� 

1 2 3{ , , }
i i i? ? ?  et 4 5 6{ , , }i i i? ? ?  correspondent aux translations et rotations �l�mentaires, 

respectivement ; 
� une articulation passive de type cardan � 2 degr�s de libert� plac�e � l'origine de la jambe 

permettant deux rotations d'angles 1 2{ , }
i iq q , qui est d�crite par la matrice de 

transformation homog�ne 1 1 2( , )
i i

u q qV  ; � un corps rigide, la jambe, reliant le pied de la plate-forme mobile, qui est d�crite par la 
matrice de transformation constante homog�ne legT  ; � une articulation virtuelle � 6 degr�s de libert� d�finissant trois translations et trois 
rotations de la flexibilit� localis�e, qui sont d�crits en fonction de la matrice homog�ne 

7 12( , )
i i

s ? ?V K , o� 7 8 9{ , , }i i i? ? ?  et 10 12 12{ , , }i i i? ? ?  correspondent aux translations et aux 
rotations �l�mentaires, respectivement ; 

� une articulation passive de type cardan � 2 degr�s de libert� plac�e � l'extr�mit� de la 
jambe permettant deux rotations d'angles 3 4{ , }

i iq q , qui est d�crite par la matrice de 
transformation homog�ne 2 3 4( , )i iu q qV  ; � d'un lien rigide entre la jambe et l'effecteur (partie de la plate-forme mobile) d�crite par la 
matrice de transformation homog�ne constante itoolT  ;  

La position de l'effecteur est soumise aux variations de toutes les coordonn�es d'une seule 
cha�ne cin�matique qui peut �tre �crite de la mani�re suivante : 

0 0 1 6 1 1 2 7 12 2 3 4( ) ( , ) ( , ) ( , ) ( , )
i i i i i i i i i i i i

i base a foot s u leg s u toolq q q q q? ? ? ? ?= + ?T T V T V V T V V TK K  (1) 
o� la matrice (.)aV  est soit une fonction �l�mentaire de rotations ou de translations, les 

matrices 1(.)uV  et 2 (.)uV sont des composantes de deux rotations successives, et la matrice 
(.)sV  est compos�e de six transformations �l�mentaires. Dans le cas de corps rigides, les 

coordonn�es des articulations virtuelles sont �gales � z�ro, tandis que les autres angles (actifs 
ou passives) sont obtenus en utilisant le mod�le g�om�trique inverse. Des expressions 
particuli�res de tous les composants du produit (1) peuvent �tre facilement obtenues � l'aide 
des techniques standards utilis�s pour d�finir les matrices de transformation homog�nes. Il 
convient de noter que le mod�le cin�matique (1) comprend 18 variables (1 pour l'articulation 
active et, 4 pour les articulations passive, et 13 pour les ressorts virtuels). Toutefois, certaines 
flexibilit�s localis�es sont redondantes, car elles sont compens�es par des articulations 
passives correspondant � l'alignement des axes ou par combinaison d'articulations passives. 
N�anmoins, il n'est pas n�cessaire de les d�tecter ni de les supprimer pour permettre notre 
analyse, parce que notre technique permet facilement et efficacement d'�liminer cette 
redondance. 

2.3 Mod�le cin�matique 
Pour �valuer les capacit�s du manipulateur pour r�pondre aux forces et aux couples 

ext�rieurs, nous �crivons les �quations diff�rentielles qui d�crivent les relations entre la 
position de l'effecteur et les petites variations des articulations. Pour chaque i�me cha�ne 
cin�matique, cette �quation peut �tre g�n�ralis�e comme suit  

, 1,2,3i ii i q i i?? = ?? + ?? =t J ? J q  (2) 
o� le vecteur ? (? , ? , ? , ? , ? , ? )Ti xi yi zi xi yi zip p p ? ? ?=t  d�crit la translation 
? (? , ? , ? )Ti xi yi zip p p=p  et la rotation ? (? , ? , ? )Ti xi yi zi? ? ?=?  de l'effecteur dans le rep�re 
cart�sien; le vecteur 0 12( , )

i i T
i ? ?? = ? ?? K  comprend toutes les coordonn�es des articulations 

virtuelles, le vecteur 1 4( , )
i i T

i q q? = ? ?q K  comprend toutes les coordonn�es des articulations 
passives, le symbole ' '?  repr�sente les variations par rapport au cas rigide, ??J  et q?J  sont des 
matrices de tailles 6�13 et 6�4 respectivement. Il convient de noter que la d�riv�e des 
coordonn�es articulaires motoris�es 0iq  n'est pas inclus dans q

?J  mais elle est repr�sent�e dans 
la premi�re colonne de ??J  � travers la variable 0

i? . Les matrices ??J , q?J , qui sont les seuls 
param�tres de l'�quation diff�rentielle (2), peuvent �tre calcul�es � partir de l'�quation (1) de 
mani�re analytique en utilisant un logiciel de calcul formel comme Maple, MathCAD ou 



Assises MUGV  Nantes, 5-6 juin 2008 
 

5 
 

Mathematica. Toutefois, une simple diff�renciation de ces expressions donne des expressions 
complexes � manipuler pour des calculs suppl�mentaires. D'autre part, la d�composition de 
(1), o� toutes les variables sont s�par�es, nous permet d'appliquer des m�thodes de 
d�composition semi-analytique. Pour pr�senter cette technique, nous supposons que les 
articulations virtuelles 0

i? , le mod�le (1) peut �tre r��crit par, 
1 2( )ii ij j j ij?= ? ? ?T H V H  (3) 

o� la premi�re et la troisi�me matrices sont des matrices de homog�nes constantes, et le 
second est une matrice de transformation homog�ne comprenant des translations et de 
rotations �l�mentaires. Ensuite, les d�riv�es partielles de la matrice homog�ne iT  pour les 
variables ij?  pour 0ij? =  peuvent �tre calcul�es � partir d'un produit similaire o� le terme 

(.)j??V  est remplac� par son expression analytique. En particulier, pour les translations et les 
rotations �l�mentaires autour de l'axe X, ces matrices peuvent s'�crire : 

0 0 0 1
0 0 0 0
0 0 0 0
0 0 0 0

xTran

? ?? ?? = ? ?? ?? ?
V ;   

0 0 0 0
0 0 1 0
0 1 0 0
0 0 0 0

xRot

? ?? ?= ?? ?? ?? ?
V . (4)  

En outre, � partir de la d�rivation de la matrice homog�ne, 1 2( )ii ij j j ij?? ?= ? ? ?T H V H  peut �tre 
pr�sent�es par  

0
0

0
0 0 0 0

iz iy ix

iz ix iy
i

iy ix iz

p
p
p

? ?
? ?
? ?

? ? ??? ?? ? ?? ??? = ? ?? ? ??? ?? ?
T  (5) 

Alors la j�me colonne de ??J  peut �tre extraite � partir de i?T  (en utilisant les �l�ments des 
matrices 14T ? , 24T ? , 34T ? , 23T ? , 31T ? , 12T ? ). Les matrices jacobiennes q?J  peuvent �tre calcul�e de la 
m�me mani�re, mais elles sont �valu�es au voisinage de leurs position nominales ij nomq  qui 
correspond au cas rigide (ces valeurs sont fournies par le mod�le cin�matique inverse). 
Toutefois, la simple transformation i i ij j jnomq q q?= +  et la factorisation de 

( ) ( ) ( )i i iq j j q j j q j jnomq q q?= ?V V V  permettent l'application de l'approche ci-dessus. Il convient 
�galement de mentionner que cette technique peut �tre utilis�e dans des calculs analytiques et 
permet d'�viter des �quations volumineuses produites par une simple diff�renciation 
automatique. 

2.4 Mod�lisation de la rigidit� d'un manipulateur 
Pour le mod�le de rigidit�, qui d�crit la relation entre les efforts et le d�placement, il est 
n�cessaire d'introduire de nouvelles �quations qui d�finissent les r�actions des articulations 
virtuelles aux d�formations des flexibilit�s localis�es. Conform�ment � notre mod�le de 
raideur, trois types de flexibilit�s localis�es sont inclus dans chaque cha�ne cin�matique : 
� une flexibilit� localis�e � 1-ddl d�crivant la complaisance de l'actionneur ;  
� une flexibilit� localis�e � 6-ddl d�crivant la complaisance du pied ; 
� une flexibilit� localis�e � 6-ddl d�crivant la complaisance de la jambe. 
En supposant que les d�formations des flexibilit�s localis�es sont petites, les relations peuvent 
�tre exprim�es par des �quations lin�aires. 

0
i i

actK??0? ? ? ?= ?? ? ? ? ; 
1

6

i i

Foot
i i

?
?

?1

?6

? ? ?? ?? ? ? ?=? ? ? ??? ?? ?
KM M ; 7

12

i i

Leg
i i

?
?

?7

?12

? ? ?? ?? ? ? ?=? ? ? ??? ?? ?
KM M  (6)  

o� i j??  est la force g�n�ralis�e de la j�me articulation virtuelle de la i�me cha�ne cin�matique, 
actK  est la raideur de l'actionneur, et FootK , LegK sont des matrices de raideur de dimensions 

6�6 associ�es aux jambes et aux pieds respectivement. Il convient de souligner que, 
contrairement � d'autres �tudes, ces matrices sont suppos�es �tre non-diagonales. Cela nous 
permet de prendre en compte les couplages entre les d�formations angulaires et de 



Assises MUGV  Nantes, 5-6 juin 2008 
 

6 
 

translations, qui sont souvent ignor�es [8]. Pour facilit� l'analyse, les expressions (6) peuvent 
�tre regroup�es dans une seule matrice sous la forme, 

? , 1, 2,3
i

i i? = ?? =? K ?   (7) 
o� 0 12( , )

i i i T? ?? ? ?=? K  est le vecteur regroupant les r�actions des articulations virtuelles, et 
diag( , , )act Foot LegK=?K K K  est la matrice de rigidit� des flexibilit�s localis�es de dimension 

13�13. De la m�me mani�re, on peut d�finir le vecteur regroupant les r�actions des 
articulations passives 1 4( , )

i i i T
q q q? ?=? K , mais tous ses �l�ments doivent �tre �gaux � z�ro : 

, 1,2,3iq i= =? 0  (8) 
Pour trouver les �quations correspondant au d�placement de l'effecteur i?t , nous appliquons 
le principe des travaux virtuels en supposant que les d�placement des articulations sont petites 
et que ces d�placements ( , )i i? ?? q  sont autour de la position d'�quilibre. Ensuite, le travail 
virtuel de la force ext�rieure if  appliqu�e sur l'effecteur i ii i q i?? = ?? + ??t J ? J q  est �gale � la 
somme ( ) ( )T i T ii i i q i? ?? + ??f J ? f J q . Pour les forces internes, le travail virtuel est ?Ti i? ??? ?  
puisque les articulations passives ne produisent pas la force ni de couples r�sistants (le signe 
moins prend en compte les orientations adopt�es pour la d�finition des flexibilit�s localis�es). 
Par cons�quent, pour avoir un �quilibre statique, les travaux virtuels doivent �tre �gaux � z�ro 
pour les articulations virtuel. Les conditions d'�quilibre peuvent alors s'�crire  

Ti i
i? ?? =J f ? ;     Tiq i? =J f 0 . (9) 

Cela donne d'autres expressions d�crivant la transformation des forces et des couples entre les 
articulations et l'effecteur. Ainsi, le mod�le cin�tostatique complet se compose de cinq 
matrices (2), (7)�(9) o� if  et i?t  sont consid�r�s comme connus, et les autres variables sont 
consid�r�es comme inconnues. De toute �vidence, puisque les cha�nes cin�matiques sont 
distinctes et poss�dent certains degr�s de libert�, ce syst�me ne peut pas �tre r�solu 
uniquement en connaissant if . Cependant, pour un d�placement donn� de l'effecteur i?t , il est 
possible de calculer les forces externes correspondants if  et de les variables internes i?? , i?? , 

i?q  (c'est-�-dire les d�placements de flexibilit�s localis�es et les d�placements des 
articulations passive). Comme cette matrice est non singuli�re, i??  peut �tre exprim�e � partir 
de if  � l'aide des �quations ?

i
i? = ??? K ?  et Ti ii? ?? =J f ? . Cela nous permet de r�duire notre 

syst�me � cette �quation 
1

?( )
Ti i i

i q i i
?

? ? ? + ?? = ?J K J f J q t ;     Tiq i? =J f 0  (10) 
avec les inconnues if  et i?q . Ce syst�me peut �galement �tre r��crit sous forme de matrice  

?
i i

q i i
Ti iq

? ? ?? ? ? ?? =? ? ? ? ?? ? ? ?? ?? ?? ?
S J f t

q 0J 0
 (11) 

o� 1? ?
Ti i i?

? ?=S J K J  d�crit le complaisance par rapport � l'effecteur, et iqJ  tient compte des 
articulations passives sur le mouvement de l'effecteur. Par cons�quent, pour une cha�ne 
cin�matique, la matrice de raideur iK  d�finissant la relation entre les efforts ext�rieurs et les 
d�placements de l'effecteur est  

i i i= ??f K t  (12) 
Cette matrice peut �tre calcul�e en inversion la matrice de dimension 10�10 en extrayant une 
sous-matrice de dimension 6�6 qui correspondant � ?iS . Il est �galement important de 
mentionner que notre m�thode ne n�cessite que l'inversion d'une 6�6 puise que 

1 1 1 1diag( , , )act Foot LegK? ? ? ?? =K K K .  
Dans le cas g�n�ral c'est-�-dire pour tout i?J  et 

i
qJ , il n'est pas possible de prouver que 

l'�quation (11) est inversible. De plus, si iqJ  est singuli�re, les coordonn�es des articulations 
passives iq  ne sont pas uniques. D'un point de vue physique, cela signifie que si la cha�ne 
cin�matique est situ�e dans une posture singuli�re, certains d�placements peuvent �tre g�n�r�s 
par un d�placement infinit�simal des articulations passives. Mais pour un effort donn� if , la 
solution correspondante est unique (puisque i?J  est non singuli�re s'il existe au moins une 
flexibilit� localis�e dans une cha�ne cin�matique). D'autre part, une configuration singuli�re 
peut �tre associ�e � une infinit� de matrices de raideur pour la m�me localisation de l'effecteur 



Assises MUGV  Nantes, 5-6 juin 2008 
 

7 
 

et pour diff�rentes iq  calcul�s par le mod�le g�om�trique inverse. Pour r�soudre ce probl�me 
nous avons programm� une m�thode bas�e sur une d�composition en valeurs singuli�res. 
Lorsque la matrice de raideur de chaque jambe iK  a �t� �valu�e, nous obtenons la matrice de 
raideur du manipulateur par une simple addition  

3
1im i== ?K K  (13) 

Ceci d�coule du principe de superposition, parce que la force ext�rieure totale correspondant 
au d�placement de l'effecteur ?t  (la m�me pour toutes les cha�nes cin�matiques) peut �tre 
exprim� sous la forme 3 1i i== ?f f  avec i i= ??f K t .  
Il convient de souligner que la matrice r�sultant n'est pas inversible, puisque certains 
mouvements de l'effecteur ne produisent pas les r�actions sur les flexibilit�s localis�es (� 
cause de l'influence des articulations passives). Cependant, pour l'ensemble de manipulateur, 
la matrice de raideur est d�finie positive et inversible pour toutes les postures non singuli�res.  

3 D�finitions des �l�ments complaisants 
Pour chaque jambe, notre mod�le de rigidit� comprend trois �l�ments, qui sont d�crits par une 
flexibilit� localis�e � 1ddl et deux autres � 6-ddl qui correspondes � la complaisances de 
l'actionneur, du pied et de la jambe (voir Fig. 3). Nous allons maintenant d�crire une m�thode 
pour �valuer les matrices de complaisance. 

3.1 Complaisance des actionneurs  
La complaisance de l'actionneur est un scalaire qui d�pend de la m�canique et de la boucle 
d'asservissement. Comme la plupart des actionneurs sont command�s par un PID num�rique, 
la principale source d'erreur est li�e � la transmission m�canique qui souvent en dehors de la 
boucle de contr�le (vis � billes, engrenages, courroies, ...) dont la complaisance est 
comparable � la complaisance des autres �l�ments du manipulateur. En raison de la 
complexit� de la structure m�canique des servomoteurs, ce param�tre est souvent �valu� � 
partir des exp�riences en statique. 

3.2 Complaisance des membrures 
Dans notre �tude, la complaisance des membrures ou son inverse, la rigidit�, est d�finie par 
une matrice sym�trique d�finie positive de dimension 6�6 correspondant � une flexibilit� 
localis�e � 6-ddl avec un couplage entre les d�formations de translation et de rotation. Le 
moyen le plus simple pour obtenir ces matrices est de les assimiler � des poutres pour lesquels 
les �l�ments non-nul de la matrice de raideur sont :  

11
Lk

EA
= ;

3

22 3 z

Lk
EI

= ;
3

33 3 y

Lk
EI

= ; 44 Lk GJ= ; 55 y
Lk

EI
= ; 66

z

Lk
EI

= ;
2

35 2 y

Lk
EI

=? ;
2

26 2 z

Lk
EI

=  (14) 
avec L la longueur de la poutre, A sa surface de la section transversale, Iy, Iz et J sont les 
inerties quadratiques et polaire de la section transversale, et E et G sont les modules d'Young 
et de Coulomb, respectivement. Cependant, pour certaines g�om�tries complexes des 
membrures, une simple approximation par une poutre n'est pas suffisante. Dans ce cas, nous 
pouvons assimiler une membrure � une s�rie de poutres assembl�es en s�rie par encastrement 
et � utiliser la matrice de rigidit� r�sultante. 

3.3 Complaisance des membrures �valu�e par une mod�lisation par �l�ments finis 
Pour des membrures poss�dant une forme complexes, des r�sultats plus pr�cis peuvent �tre 
obtenus en utilisant une mod�lisation par �l�ments finis. Pour appliquer cette approche, nous 
ajoutons au mod�le CAO de chaque membrure, une pi�ce de r�f�rence "pseudo-rigide", qui 
est utilis� comme r�f�rence pour l'�valuation de la complaisance et l'origine de chaque 
membrure est fixe. Puis, en appliquant des efforts et des couples unitaires sur la pi�ce de 
r�f�rence, il est possible d'�valuer les d�placements angulaires et lin�aires, ce qui permet 
l'�valuation des colonnes de la matrice de rigidit�. La principale difficult� ici est d'obtenir les 



Assises MUGV  Nantes, 5-6 juin 2008 
 

8 
 

valeurs exactes des d�placements en utilisant un bon maillage. De plus, pour accro�tre la 
pr�cision, les d�placements doivent �tre �valu�s en utilisant des donn�es redondantes qui 
d�crivent le d�placement du corps de r�f�rence. C'est pour cela que nous une d�composition 
en valeur singuli�re. � partir de notre �tude, nous avons constat� qu'une approximation du 
pied par une seule poutre donne une pr�cision de 50%, et les quatre poutres am�liore jusqu'� 
30% seulement. Cependant, bien que la m�thode bas�e sur des �l�ments finis soit la plus 
pr�cise elle est aussi la co�teuse en temps de calcul. Toutefois, contrairement � une simple 
mod�lisation par �l�ment finis de l'ensemble du manipulateur, qui exige un nouveau maillage 
des conditions aux limites � chaque fois que nous changeons de posture, notre m�thode ne 
demande qu'un calcul d'�valuation de la raideur des membrures. 

4 Exemples d'application 
Pour d�montrer l'efficacit� de notre m�thode, nous allons l'appliquer pour comparer la rigidit�  
de deux m�canismes � 3ddl bas�s sur l'architecture de l'Orthoglide. Les mod�les CAO de ces 
m�canismes sont repr�sent�s dans la Fig. 4. 

x
z

y B1

P

A1

C1

A2

C2

A3

C3

B3

 

B1

i1

P
x

z
y

j1

A1

C1

A2
B2

C2

A3

C3

B3

 

x

z

y

Q2

Q1

 
(a) Orthoglide avec des 

cardans 
(b) Orthoglide avec des 

parall�logrammes 
(c) Points critiques Q1 et Q2 

de l'espace de travail 
Fig. 4. Cin�matique de 2 m�canismes bas�s sur l'Orthoglide 

4.1 Rigidit� de l'Orthoglide avec des cardans  
Nous �crivons un mod�le de rigidit� simplifi�e de l'Orthoglide o� les jambes sont de type 
PUU c'est-�-dire avec deux cardans. Cependant, pour conserver les propri�t�s de l'Orthoglide, 
la section de la membrure entre les deux cardans est doubl�e pour �tre �quivalent aux bars des 
parall�logrammes. En faisant l'hypoth�se que l'origine de notre syst�me de coordonn�e est 
situ� sur le rep�re de l'effecteur lorsque le manipulateur est en posture isotrope (quand les 
jambes sont mutuellement perpendiculaires et parall�les aux axes de prismatiques). Avec cette 
hypoth�se, les mod�les g�om�triques de diff�rentes cha�nes cin�matiques peuvent �tre d�crit 
par l'expression (1), o� { , , }i x y z?  et les diff�rentes matrices sont d�finies par les op�rateurs 
de translation et de rotation (.), (.), (.)x y zT T RK  suivants :  

1 0 0
0 1 0 0
0 0 1 0
0 0 0 1

x
base

L r? ?? ?? ?? ?=? ?? ?? ?? ?

T  

1 0 0
0 1 0 0
0 0 1 0
0 0 0 1

x
tool

r? ?? ?? ?=? ?? ?? ?? ?

T  

0 0 1 0
1 0 0
0 1 0 0
0 0 0 1

y
base

L r
? ?? ?? ?? ?=? ?? ?? ?? ?

T  (15) 

0 1 0
0 0 1 0
1 0 0 0
0 0 0 1

y
tool

r? ?? ?? ?= ? ?? ?? ?? ?

T  

0 1 0 0
0 0 1 0
1 0 0
0 0 0 1

z
base L r

? ?? ?? ?=? ?? ?? ?? ?? ?

T

0 0 1
1 0 0 0
0 1 0 0
0 0 0 1

z
base

r? ?? ?? ?=? ?? ?? ?? ?

T  (16) 

0 0 0 0( ) ( )a xq q+ ? = + ?V T ;  Foot =T I ;   ( )leg x L=T T ;  (17) 
1 6 1 2 3 4 5 6( ,... ) ( ) ( ) ( ) ( ) ( ) ( )s x y z x y z? ? = ? ? ? ? ? ?V T T T R R R  (18) 

1 1 2 1 2( , ) ( ) ( )u z yq q q q=V R R ; 2 3 4 3 4( , ) ( ) ( )u y zq q q q=V R R  (19) 



Assises MUGV  Nantes, 5-6 juin 2008 
 

9 
 

Avec L et r sont les param�tres g�om�triques du manipulateur (longueur de la jambe et 
d�calage de l'effecteur respectivement), et les autres variables sont les m�mes que dans 
l'�quation (1). Comme dans le cas d'un manipulateur rigide, l'effecteur produit uniquement 
des mouvements de translation, les articulations passives sont soumis � des contraintes 
sp�cifiques 3 2 4 1;q q q q= ? = ? , qui sont implicitement utilis�es dans le calcul du mod�le 
g�om�trique direct et inverse. Toutefois, le mod�le flexible permet des variations de toutes les 
articulations passives. En utilisant la mod�lisation de la raideur des membrures calcul�e avec 
les �l�ments finis et en appliquant notre m�thode, nous avons calcul� les matrices de raideur 
pour trois postures typiques du manipulateur et les r�sultats sont dans le tableau 1. On les 
compare ensuite avec un manipulateur avec des parall�logrammes. 

4.2 Rigidit� de l'Orthoglide avec des parall�logrammes 
Avant d'�valuer la complaisance de manipulateur, nous devons �valuer la matrice de raideur 
des parall�logrammes. En utilisant les notations adopt�es, nous pouvons �crire le mod�le 
�quivalent d'un parall�logramme  

2 2 7 12( ) ( ) ( ) ( , )Plg y x y sq L q ? ?= ? ? ? ?T R T R V K  (20) 
O�, par rapport au cas pr�c�dent, la troisi�me articulation passive est �limin� (il est 
implicitement admis que 3 2q q= ? ). D'autre part, chaque parall�logramme peut �tre divis� en 
deux cha�nes cin�matiques s�rielles (le �haut� et le �bas�)  

1 1 6 2( /2) ( ) ( ) ( , ) ( ) ( /2)
up up up up

haut z y x s y zd q q L q q d? ?= ? ? +? ? ? ? ? + ? ?T T R T V R TK  (21) 
1 1 6 2( /2) ( ) ( ) ( , ) ( ) ( /2)
dn dn dn dn

bas z y x s y zd q q L q q d? ?= ? + ? ? ? ? ? + ? ? ?T T R T V R TK  (22) 
O� L, d sont les param�tres g�om�triques des parall�logrammes, 1 2, , { , }

i iq q i up dn? ? ?  sont 
les variations des articulations passives. Ainsi, les matrices de complaisance du 
parall�logramme peuvent �tre �galement obtenues � l'aide de notre technique qui donne une 
expression analytique, 

11

22 26

2 2 2
22 2 22

44

2 2
11

2 2 2
2 22 22

26 66

0 0 0 0 0
0 0 0 0
0 0 0 0 0 0

0 0 0
2 4 8

0 0 0 0 0
4

0 0 0
8 4

q q

Plg
q

q q

K
K K

d C K d S K
K

d C K

d S K d S K
K K

? ?? ?? ?? ?+? ?= ? ?? ?? ?? ?+? ?? ?? ?

K  avec cos( ); sin( )q qC q S q= =  

En utilisant ce mod�le et en appliquant la technique propos�e, on a calcul� les matrices de 
rigidit� pour trois postures particuli�res du manipulateur (voir tableau 1). Comme pour la 
comparaison avec l'utilisation des cardans, les parall�logrammes augment la raideur en torsion 
d'environ 10 fois. Cela justifie l'utilisation de cette architecture dans la conception du 
prototype de l'Orthoglide [15]. 

TYPE D'ARCHITECTURE 
POINT Q0 ( , , 0.00x y z mm= ) POINT Q1 ( , , 73.65x y z mm= ? ) POINT Q2 ( , , 126.35x y z mm= + ) 
trank  [MM/N] rotk  [RAD/N.MM] trank  [MM/N] rotk  [RAD/N.MM] trank  [MM/N] rotk  [RAD/N.MM] 

MANIPULATEUR 3-PUU  2.78?10-4 20.9?10-7 10.9?10-4 24.1?10-7 71.3?10-4 25.8?10-7 
MANIPULATEUR 3-PRPAR 2.78?10-4 1.94?10-7 9.86?10-4 2.06?10-7 21.2?10-4 2.65?10-7 

Tableau 1 : Raideur de l'Orthoglide avec des jambes 3-PUU et 3-PRPaR 

5 Conclusion 
Dans cet article, nous avons propos� une nouvelle m�thode syst�matique pour le calcul de la 
matrice de rigidit� des manipulateurs hyperstatiques parall�les. Cette m�thode est bas�e sur 
une mod�lisation rigide avec des flexibilit�s localis�es, dont les param�tres sont �valu�s par 
l'interm�diaire d'une mod�lisation par �l�ments finis et d�crivent la complaisance en 
translation et en rotation ainsi que les couplages entre eux. Contrairement aux travaux 



Assises MUGV  Nantes, 5-6 juin 2008 
 

10 
 

pr�c�dents, notre m�thode utilise une nouvelle strat�gie pour �crire les �quations 
cin�matiques, qui consid�re en m�me temps les relations cin�matiques et les conditions 
d'�quilibre de chaque cha�ne cin�matique, puis regroupe les solutions partielles. Cela permet 
de calculer de la matrice de raideur des m�canismes hyperstatique dans n'importe quelle 
posture du manipulateur, y compris dans les postures singuli�res ou � leurs voisinages.  
Un autre avantage de notre m�thode est la simplicit� des calculs qui exigent l'inversion de 
matrices de faible dimension par rapport � d'autres techniques. En outre, notre m�thode ne 
n�cessite pas l'�limination manuelle des flexibilit�s localis�es redondantes associ�es aux 
articulations virtuelles, car cette op�ration est intrins�quement incluse dans l'algorithme 
num�rique. L'efficacit� de notre m�thode a �t� d�montr�e par des exemples d'application, qui 
traitent de l'analyse comparative de la raideur de deux manipulateurs parall�les de la famille 
de l'Orthoglide. Les r�sultats de nos calculs ont confirm� les avantages des architectures 
utilisant des parall�logrammes et ainsi valid� la conception du prototype de l'Orthoglide. Une 
autre contribution importante de notre article est l'�criture d'un mod�le analytique de la 
matrice de raideur des parall�logrammes, qui a �t� d�riv�e en utilisant la m�me m�thodologie. 
Bien que nous ayons appliqu� notre m�thode � des m�canismes � 3 degr�s de libert� de 
translation, notre m�thode peut �tre �tendue � d'autres architectures parall�les, compos� de 
plusieurs cha�nes cin�matiques avec des articulations roto�des et prismatiques et des jambes 
diff�rentes. Par la suite, nous allons appliquer notre m�thode � des manipulateurs plus 
complexes comme la machine Verne [16], v�rifier exp�rimentale notre mod�le de rigidit� sur 
le robot Orthoglide et prendre en compte l'influence de la gravit�. 

R�f�rences 
[1] J. Tlusty, J. Ziegert et S. Ridgeway, �Fundamental Comparison of the Use of Serial and Parallel Kinematics for Machine Tools,� In: 

Annals of the CIRP, vol. 48(1), 1999. 
[2] P. Wenger, C. M. Gosselin et B. Maill�, �A Comparative Study of Serial and Parallel,� In: Mechanism Topologies for Machine Tools, 

PKM�99, pp. 23-32, Milano, 1999. 
[3] F. Majou, P. Wenger et D. Chablat, �The design of Parallel Kinematic Machine Tools using Kinetostatic Performance Criteria,� In: 3rd 

International Conference on Metal Cutting and High Speed Machining, Metz, France, Juin 2001.  
[4] G. Pritschow et K.-H. Wurst, �Systematic Design of Hexapods and Other Parallel Link Systems,� In: Annals of the CIRP, vol. 46(1), 

pp 291�295, 1997. 
[5] O. Company et F. Pierrot, �Modelling and Design Issues of a 3-axis Parallel Machine-Tool,� Mechanism and Machine Theory, vol. 37, 

pp. 1325�1345, 2002. 
[6] T. Brogardh, �PKM Research - Important Issues, as seen from a Product Development Perspective at ABB Robotics,� In: Workshop on 

Fundamental Issues and Future Research Directions for Parallel Mechanisms and Manipulators, Quebec, Canada, Octobre 2002. 
[7] A. Paskhevich, P. Wenger et D. Chablat, �Kinematic and stiffness analysis of the Orthoglide, a PKM with simple, regular workspace 

and homogeneous performances,� In: IEEE International Conference On Robotics And Automation, Rome, Italie, Avril 2007 
[8] C.M. Gosselin, �Stiffness mapping for parallel manipulators,� IEEE Transactions on Robotics and Automation, vol. 6, pp. 377�382, 

1990.  
[9] X. Kong et C. M. Gosselin, �Kinematics and Singularity Analysis of a Novel Type of 3-CRR 3-DOF Translational Parallel 

Manipulator,� The International Journal of Robotics Research, vol. 21(9), pp. 791�798, Septembre 2002. 
[10] F. Majou, C. Gosselin, P. Wenger et D. Chablat. �Parametric stiffness analysis of the Orthoglide,� Mechanism and Machine Theory, 

vol. 42(3), pp. 296�311, Mars 2007.  
[11] B.C. Bouzgarrou, J.C. Fauroux, G. Gogu et Y. Heerah, �Rigidity analysis of T3R1 parallel robot uncoupled kinematics,� In: Proc. of 

the 35th International Symposium on Robotics (ISR), Paris, France, Mars 2004.  
[12] D. Deblaise, X. Hernot et P. Maurine, �A Systematic Analytical Method for PKM Stiffness Matrix Calculation,� In: IEEE International 

Conference on Robotics and Automation (ICRA), pp. 4213�4219, Orlando, Floride, Mai 2006. 
[13] Y. Li et Q. Xu, "Stiffness Analysis for a 3-PUU Parallel Kinematic Machine", Mechanism and Machine Theory, (In press: Available 

online 6 Avril 2007) 
[14] R. Clavel, �DELTA, a fast robot with parallel geometry,� Proceedings, of the 18th International Symposium of Robotic Manipulators, 

IFR Publication, pp. 91�100, 1988. 
[15] D. Chablat et Ph. Wenger, �Architecture Optimization of a 3-DOF Parallel Mechanism for Machining Applications, the Orthoglide,� 

IEEE Transactions On Robotics and Automation, Vol. 19/3, pp. 403�410, 2003. 
[16] D. Kanaan, P. Wenger et D. Chablat, �Kinematics analysis of the parallel module of the VERNE machine,� In: 12th World Congress in 

Mechanism and Machine Science, IFToMM, Besan�on, Juin 2007. 


