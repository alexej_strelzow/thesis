
ar
X

iv
:0

90
9.

27
77

v1
  [

cs
.IT

]  
15

 Se
p 2

00
9

1

On the Symmetric Gaussian Interference
Channel with Partial Unidirectional Cooperation

Hossein Bagheri, Abolfazl S. Motahari, and Amir K. Khandani

Abstract�A two-user symmetric Gaussian Interference Chan-
nel (IC) is considered in which a noiseless unidirectional link
connects one encoder to the other. Having a constant capacity,
the additional link provides partial cooperation between the
encoders. It is shown that the available cooperation can dra-
matically increase the sum-capacity of the channel. This fact
is proved based on comparison of proposed lower and upper
bounds on the sum-capacity. Partitioning the data into three
independent messages, namely private, common, and cooperative
ones, the transmission strategy used to obtain the lower bound
enjoys a simple type of Han-Kobayashi scheme together with a
cooperative communication scheme. A Genie-aided upper bound
is developed which incorporates the capacity of the cooperative
link. Other upper bounds are based on the sum-capacity of the
Cognitive Radio Channel and cut-set bounds. For the strong
interference regime, the achievablity scheme is simplified to
employ common and/or cooperative messages but not the private
one. Through a careful analysis it is shown that the gap between
these bounds is at most one and two bits per real dimension for
strong and weak interference regimes, respectively. Moreover, the
Generalized Degrees-of-Freedom of the channel is characterized.

I. INTRODUCTION
Interference is one of the major limiting factors in achieving

the total throughput of a network consisting of multiple
non-cooperative transmitters intending to convey independent
messages to their corresponding receivers through a common
bandwidth. The way interference is usually dealt with is either
by treating it as noise or preventing it by associating different
orthogonal dimensions, e.g. time or frequency division, to
different users. Since interference has structure, it is possible
for a receiver to decode some part of the interference and
remove it from the received signal. This is, in fact, the coding
scheme proposed by Han-Kobayashi (HK) for the two-user
Gaussian IC [1]. The two-user Gaussian IC provides a simple
example showing that a single strategy against interference
is not optimal. In fact, one needs to adjust the strategy
according to the channel parameters [2]�[4]. However, a single
suboptimal strategy can be proposed to achieve up to a single
bit per user of the capacity region of the two-user Gaussian
IC [5].

If the senders can cooperate, interference management can
be done more effectively through cooperation. Cooperative

Financial support provided by Nortel and the corresponding matching
funds by the Natural Sciences and Engineering Research Council of Canada
(NSERC), and Ontario Centres of Excellence (OCE) are gratefully acknowl-
edged.

The authors are affiliated with the Coding and Signal Transmission
Laboratory, Electrical and Computer Engineering Department, University of
Waterloo, Waterloo, ON, N2L 3G1, Canada, Tel: 519-884-8552, Fax: 519-
888-4338, Emails: {hbagheri, abolfazl, khandani}@cst.uwaterloo.ca.

links can be either orthogonal (using out-of-band signalling)
or non-orthogonal (using in-band signalling) to the shared
medium [6]. In this work, unidirectional orthogonal cooper-
ation is considered.

Prior Works. Transmitter coordination over orthogonal links
is considered in different scenarios using two-transmitter
configurations as indicated in [6]�[13]. The capacity region
of the Multiple Access Channel (MAC) with bidirectional
cooperation is derived in [7], where cooperation is referred to
as conference. Several achievable rate regions are proposed for
the IC with bidirectional transmitter and receiver cooperation
[8]. The transmit cooperation employs Dirty Paper Coding
(DPC), whereas the receive cooperation uses Wyner-Ziv cod-
ing [14]. In the transmit cooperation, the entire message of
each transmitter is decoded by the other cooperating transmit-
ter, which apparently limits the performance of the scheme to
the capacity of the cooperative link. The capacity regions of
the compound MAC with bidirectional transmitter cooperation
and the IC with Unidirectional Cooperation (ICUC) under
certain strong interference conditions are obtained in [9].
These rate regions are shown to be closely related to the
capacity region of the Compound MAC with Common Infor-
mation (CMACCI). Furthermore, the inner and outer bounds
for the ICUC considered in [9] are given in [10] for different
interference regimes. The above papers considered the ICUC
in which the message sent by one of the encoders is completely
known to the other encoder. This setup is referred to as
cognitive radio in the literature and is also considered in [11]�
[13]. More recently, the capacity region of the compound MAC
with both encoder and decoder unidirectional cooperation,
and physically degraded channels is derived in [6]. In a
related problem, the sum-capacity of the ICUC channel with a
different transmission model is characterized upto 9 bits [15].

Contribution. For a Symmetric ICUC (SICUC) setup shown
in Fig. 1 when aP > 1, a simple HK scheme in conjunction
with cooperative communication is employed in the weak
interference regime, i.e., a ? 1. In particular, each user
partitions its data into three independent messages, namely
private, common and cooperative, and constructs independent
codebooks for each of them. Each receiver, first jointly de-
codes the common and cooperative messages of both users
and then decodes its own private message. Following [5],
the power of the private message of each user is set such
that it is received at the level of the Gaussian noise at the
other receiver. This is to guarantee that the interference caused
by the private message has a small effect on the other user.
To prove that the scheme has negligible gap from the sum-
capacity for all transmit powers and channel gains, the transmit




2
power should be properly allocated among the messages used
by each user. Optimizing the achievable sum-rate requires
finding an optimum Power Allocation (PA) over different
codebooks employed by each user, which is mathematically
involved. For the weak interference regime, a simple power
allocation strategy that works for both users and operates over
the three codebooks is then proposed. Noting that the private
codebook contributes in a small rate for the strong interference
regime, i.e., a > 1, the scheme is subsequently modified to
achieve a smaller gap and complexity by utilizing one or
both of the common and cooperative codebooks but not the
private codebook. The achievable sum-rates are compared to
appropriate upper bounds to prove that the gap from the sum-
capacity of the channel is one and two bits per real dimension,
for the strong and weak interference regimes, respectively. The
upper bounds are based on the cut-set bound and the sum-
capacity of the Cognitive Radio Channel (CgRC), in which
the message sent by one of the encoders is known to the other
(cooperating) encoder. A new upper bound is also proposed to
incorporate the effect of the imperfect cooperative link, i.e., the
case that only part of the message sent by one of the encoders
is known to the other (cooperating) encoder [16]. To obtain
some asymptotic results, the Generalized Degrees of Freedom
(GDOF) of the channel is characterized. The GDOF represents
the number of dimensions available for communication in
the presence of the interference when Signal-to-Noise Ratio
SNR ? ?. When SICUC is noise-limited (aP ? 1), it is
shown that treating interference as noise, and not using the
cooperative link, gives a sum-rate within a single bit to the
sum-capacity of the channel for all channel parameters.

The rest of this paper is organized as follows: Section II
presents the system model. Sections III and IV, respectively,
focus on the weak and strong interference regimes. For
each regime, the corresponding achievable sum-rate, power
allocation, upper bounds, and the gap analysis are provided.
Section V characterizes the GDOF of the channel. Section
VI considers the noise-limited regime. Finally, section VII
concludes the paper.

Notation. The sequence xn denotes x1, � � � , xn. x�, 1?x.
Prob(�) indicates the probability of an event, and E[X ] rep-
resents the expectation over the random variable X . All
logarithms are to the base 2 and all rates are expressed in
bits per real dimension. Finally, C(P ) , 12 log(1 + P ).

II. SYSTEM MODEL

In this work, a two-user symmetric Gaussian interference
channel with partial unidirectional cooperation, as depicted in
Fig. 1, is considered. The model consists of two transmitter-
receiver pairs, in which each transmitter wishes to convey
its own data to its corresponding receiver. There exists a
noiseless cooperative link with capacity C12 from encoder 1
to encoder 2. It is assumed that all nodes are equipped with a
single antenna. The input-output relationship for this channel
in standard form is expressed as [17]:

y1 = x1 +
?
a x2 + z1,

y2 =
?
a x1 + x2 + z2,

(1)

1

1

1

2

1

2

?
a

?
a

C12

z1

z2

Fig. 1. The symmetric interference channel with unidirectional transmitter
cooperation.

where constant a ? 0 represents the gain of the interference
links. For i ? {1, 2}, zi ? N (0, 1). The average power
constraint of each transmitter is P , i.e., E[|x2i |] ? P . The full
Channel State Information (CSI) is assumed to be available at
the transmitters as well as receivers. Following [5], SNR=P
and Interference-to-Noise Ratio INR= aP are defined as the
characterizing parameters of SICUC.

For a given block length n, encoder i sends its own (random)
message index mi from the index set Mi = {1, 2, ...,Mi =
2nRi} with rate Ri [bits/channel use]. Each pair (m1,m2)
occurs with the same probability 1/M1M2. A one-step con-
ference is made up of a communicating function k which
maps the message index m1 into qn with finite alphabet size
|Q| = 2nC12 . The encoding function f1 maps the message
index m1 into a codeword xn1 chosen from the codebook C1.
The encoding function f2 maps the message index m2 and
what was obtained from the conference with encoder 1 into a
codeword xn2 selected from the codebook C2. Therefore:

qn = k(m1),
xn1 = f1(m1),
xn2 = f2(m2, qn).

(2)

The codewords in each codebook must satisfy the average
power constraint 1

n

?n
t=1 |xi,t|2 ? P for i ? {1, 2}. Each

decoder uses a decoding function gi(yni ) to decode its desired
message index mi based on its received sequence. Let m�i
be the output of the decoder. The average probability of
error for each decoder is: Pei = E[Prob(m�i 6= mi)]. A rate
pair (R1, R2) is said to be achievable when there exists
an (M1,M2, n, Pe1 , Pe2 )-code for the ICUC consisting of
two encoding functions {f1, f2} and two decoding functions
{g1, g2} such that for sufficiently large n:

R1 ? 1n log(M1),
R2 ? 1n log(M2),
Pe ? ?.

In the above, Pe=max(Pe1 , Pe2) and ??0 is a constant that
can be chosen arbitrarily small. The capacity region of the
ICUC is the closure of the set of achievable rate pairs. In this
work, the characterization of the sum-capacity of the channel
is considered. In addition, the interference-limited regime is
mainly investigated, i.e., INR? 1 since otherwise the system
is noise limited and is not of much interest [5], [16]. In fact,



3
it will be shown later that treating interference as noise, and
not using the cooperative link, will give a sum-rate within a
single bit to the sum-capacity of the channel for all channel
parameters. Finally, for rate derivations in this paper, Gaussian
codebooks are employed.

III. WEAK INTERFERENCE REGIME (a ? 1)
A. Achievable Sum-Rate

A simple type of the Han-Kobayashi scheme is used in [5]
for the symmetric IC, in which the codebook of transmitter i
is composed of private and common codebooks denoted by Cui
and Cwi , respectively. The transmitted signal is xi =

?
Puui+?

Pwwi, and it is assumed that E[|ui|2] = E[|wi|2] = 1. The
power associated with the codebooks are Pu and Pw, such that
Pu+Pw = P . First, the common messages are decoded at both
decoders and their effect is subtracted from the received signal.
Then, the private messages are decoded at their corresponding
receiver while treating the private signal of the other user as
noise. One of the main steps towards getting a small gap is
that the power of the interfering private message is set to be
at the noise level, i.e., aPu = 1 [5].

In this work, because the existence of the cooperative link,
encoder 2 can relay the transmitter 1�s information to help
the other receiver [18]. Therefore, it is natural to have three
codebooks at encoder i, namely private codebook Cui with
codewords ui, common codebook Cwi with codewords wi , and
cooperative codebook Cvi with codewords v. The cooperative
message is decoded error free at transmitter 2 and relayed to
both receivers. The input to the channel can be written as:

Xi=
?
Puui +

?
Pwiwi +

?
Pviv. (3)

The average power constraint for transmitter i is Pu + Pwi +
Pvi = P . The following power allocation is used:

Pu =
1
a
,

Pvi = ?i(P ? 1a ),
Pwi = ?�i(P ? 1a ),

(4)

where 0 ? ?i ? 1 is the corresponding power allocation
parameter for transmitter i. The received power of v at decoder
1 is: PV = (

?
Pv1 +

?
aPv2)

2
.

Because of the symmetries in the problem (in terms of
the channel gains), and the fact that the sum-rate is the
objective of this paper, we set ?1 = ?2 = ?. It is also
assumed that v is decoded at both decoders. Therefore, the
achievable rate pairs associated with the HK scheme are in
fact the intersection of the capacity regions of two MACs: each
composed of four virtual users, i.e., {u1, w1, w2, v} for MAC1,
and {u2, w1, w2, v} for MAC2. Among all rate assignments
and decoding orders for the MACs, the following scheme
is used. It will be shown later that the scheme achieves
within two bits of the associated upper bound. The strategy
is along the same line as the approach of [5] for the IC. The
common codewords w1, w2, and the cooperative codeword v
are jointly decoded at both receivers, treating the interfering
private signals as noise. Finally, each receiver decodes its

private codeword1. Therefore, the capacity region of four-user
MACs are projected onto three-dimensional subspaces. As a
result, a compound MAC with three virtual users (w1, w2, v)
is obtained. The capacity region of MACi i ? {1, 2} is:

Rv? min{C( PV
Pu + 2

), C12}, (5)

Rwi? C(
Pw

Pu + 2
), (6)

Rwj? C(
aPw
Pu + 2

), (7)

Rwi +Rwj? C(
Pw(1 + a)

Pu + 2
), (8)

Rwi +Rv? C(
Pw + PV
Pu + 2

), (9)

Rwj +Rv? C(
aPw + PV
Pu + 2

), (10)

Rwi +Rwj +Rv? C(
Pw + aPw + PV

Pu + 2
), (11)

where j ? {1, 2}, and j 6= i. Also, Pw1 = Pw2 , Pw, Pv1 =
Pv2 ,Pv , and

PV = (1 +
?
a)2Pv. (12)

The factor 2 in the denominators comes from the fact that the
power of the interfering private signal has been set to unity.
It is also assumed that: Rw1 =Rw2 ,Rw. Since the sum-rate
is 2(Ru+Rw)+Rv, in the above, 2Rw + Rv is of particular
interest. There exist three ways to construct 2Rw + Rv from
inequalities (5-11):

2Rw + Rv? C(Pw(1 + a) + PV
Pu + 2

) , RB1 , (13)

2Rw + Rv? 2R�w +min{C( PV
Pu + 2

), C12} , RB2 , (14)

2Rw + Rv? C(aPw + PV
Pu + 2

) + R�w , RB3 , (15)

where

R�w , min{C( aPw
Pu + 2

),
1

2
C(

Pw(1 + a)

Pu + 2
)}, (16)

and (13-15) come from inequalities (11), (5-7), and (6, 7, 9,
10), respectively. The achievable sum-rate is:

Rsum= max
0???1

{
2C(

Pu
2
) + min

i?{1,2,3}
{RBi}

}
. (17)

Optimizing the achievable sum-rate requires finding optimum
power allocation parameter ?, which is mathematically in-
volved. In the following, a simple power allocation strategy
is provided, and is later shown to be within two bits of the
related upper bound.

1It is remarked that encoder 2 could perform the Gelfand-Pinsker binning
[19] in order to precode its own message against the known interference.
This, in general, could increase the rate at receiver 2, especially in the weak
interference regime. In this paper, however we show that one can achieve
a sum-rate within two bits of the sum-capacity of SICUC without binning.
Employing the binning technique will be considered in future works.



4
B. Power Allocation Policies and the Associated Sum-Rates
1) Universal PA: As will be shown later, the following

power allocation guarantees a small gap form the sum-capacity
for all a ? 1 and hence called universal. We set Pw = PV ,
which together with Pu= 1a give:

Pw =
P ? 1

a

1 + 1
(1+

?
a)2

. (18)

The corresponding sum-rate is R+2C(Pu2 ), where

R, min
i?{1,��� ,5}

{Ri}, (19)

and

R1= C(
(2 + a)Pw
Pu + 2

),

R2= 2C(
aPw
Pu + 2

) + C12,

R3= C(
(1 + a)Pw
Pu + 2

) + C12,

R4= C(
(1 + a)Pw
Pu + 2

) + C(
aPw
Pu + 2

),

R5=
3

2
C(

(1 + a)Pw
Pu + 2

) (20)
are obtained from inequalities (13-15), Eq. (18). Note that the
following redundant inequalities are eliminated:

R6= 2C(
aPw
Pu + 2

) + C(
Pw

Pu + 2
),

R7= C(
(1 + a)Pw
Pu + 2

) + C(
Pw

Pu + 2
).

It is remarked that this allocation achieves the optimal GDOF
of the channel as will be shown later and therefore can be
among the achievable schemes that have a small gap from the
sum-capacity for all channel parameters2. This allocation also
simplifies the gap analysis.

2) Full Cooperation PA: It is also interesting to consider
the case that all the remaining P ? 1

a
power is devoted to

cooperation or equivalently Pw =0, i.e., ?=1 in (4). In this
case, the achievable sum-rate becomes:

RFC = min{C12, C( (1 +
?
a)2(aP ? 1)
2a+ 1

)}+ 2C(Pu
2
). (21)

It will be shown that this power allocation provides a smaller
gap compared to the universal PA in some regions. However, it
does not achieve the GDOF universally [16], and hence cannot
provide a small gap for all channel parameters.

C. Upper Bounds
Two upper bounds are used for this regime. The first one

is an enlarged version of the sum-capacity of the Cognitive
Radio Channel (CgRC) with weak interference [12]. The sum-
capacity of the CgRC for a ? 1, denoted by Cwsum, is [12]:
Cwsum =

1

2
max

0???1

[
log(1 + aP + 2

?
?�aP + P ) + log(

1 + ?P

1 + ?aP
)

]
.

2In fact, if an achievable scheme fails to achieve the GDOF of the channel
it cannot have a small gap from the sum-capacity for all channel parameters.

It is noted that the first and the second terms in the above upper
bound are monotonically decreasing and increasing functions
of ?, respectively. The above upper bound is enlarged by
setting ?� = 1 and ? = 1 in the first and second terms,
respectively. Therefore, the following upper bound serves as
the first upper bound for the region:

R
(1)
ub =

1

2
log

(
1 + (1 +

?
a)2P

)
+

1

2
log(

1 + P

1 + aP
). (22)

For the IC (C12 = 0), Etkin, et.al [5] provide a new upper
bound by selecting an appropriate genie. In the following, the
effect of the cooperative link is incorporated in the genie-
aided upper bound. The genie signal si, i = 1, 2 is provided
to receiver i:

s1 =
?
ax1 + z2,

s2 =
?
ax2 + z1.

(23)

The upper bound on the sum-capacity of the genie-aided
channel is:

nRsum? I(m1; yn1 , sn1 , qn) + I(m2; yn2 , sn2 , qn) + n?n
(a)
= I(m1; qn) + I(m1; yn1 , sn1 |qn) + I(m2; qn)
+ I(m2; yn2 , sn2 |qn) + n?n

(b)
= h(qn) + I(m1; yn1 , sn1 |qn)+
I(m2; yn2 , sn2 |qn) + n?n
?

n?
t=1

h(qt) + I(m1; s
n
1 |qn) + I(m1; yn1 |sn1 , qn)

+I(m2; s
n
2 |qn) + I(m2; yn2 |sn2 , qn) + n?n

= nC12 + h(s
n
1 |qn)? h(sn1 |m1, qn)+

I(m1; yn1 |sn1 , qn) + h(sn2 |qn)? h(sn2 |m2, qn)+
I(m2; yn2 |sn2 , qn) + n?n
(c)
= nC12 + h(s

n
1 |qn)? h(sn1 |xn1 ) +

h(yn1 |sn1 , qn)? h(yn1 |sn1 , qn,m1, xn1 (m1)) +
h(sn2 |qn)? h(sn2 |xn2 ) + h(yn2 |sn2 , qn)
?h(yn2 |sn2 , qn,m2, xn2 (m2, qn)) + n?n
(d)? nC12 + h(sn1 |qn)? h(zn2 ) + h(yn1 |sn1 )
?h(yn1 |sn1 , qn,m1, xn1 (m1)) + h(sn2 |qn)
?h(zn1 ) + h(yn2 |sn2 )
?h(yn2 |sn2 , qn,m2, xn2 (m2, qn)) + n?n
(e)
= nC12 ? h(zn1 )? h(zn2 ) + h(yn1 |sn1 )+
h(yn2 |sn2 ) + n?n, (24)

where (a), (b), and (c) respectively, follow from the chain rule
of mutual information [18], Eq. (2), and Markovity of m1 ?
xn1 ? sn1 and (qn,m2)? xn2 ? sn2 . (d) comes from Eq. (23)
and the fact that removing the condition qn does not decrease
the entropy. (e) is valid because yn2 and sn2 are independent
conditionally on (xn2 , qn). The same fact is applied to yn1 and
sn1 conditionally on (xn1 , qn). Following the same reasoning
as [5] for h(yni |sni ), i ? {1, 2}, we get the following upper
bound:

R
(2)
ub = C12 + log

(P + (aP + 1)2
aP + 1

)
. (25)



5
We remark that the above upper bound is the same as the upper
bound derived in [5], except for the additional term C12.

D. Gap Analysis
Theorem 1 For a ? 1, the achievable sum-rate associated
with the universal power allocation policy of (18) is within
two bits of the sum-capacity of the channel.

Proof: See Appendix A.
In the next theorem, the maximum gap is shown to be smaller
than 2 bits using full cooperation for some cases.

Theorem 2 The achievable sum-rate corresponding to the full
cooperation power allocation policy (Eq. (21)) is within 1 and
1.5 bits of the sum-capacity of the channel for the following
cases, respectively.

1) CASE I: C12?C( b(aP?1)2a+1 ) and a3P 2?a+1

2) CASE II: C12?C( b(aP?1)2a+1 ),

where
b , (1 +

?
a)2, (26)

Proof: For each case, the gap ?FC is calculated:
1) CASE I: The achievable sum-rate is:

Rsum=C12 + 2C(
1

2a
).

The gap from the second upper bound, i.e., Eq. (25) is:

?FC,I = 1 + log
(
1 +

a3P 2 ? a? 1
(aP + 1)(2a+ 1)

) ? 1.
2) CASE II: In this case, the achievable sum-rate

Rsum=
1

2

[
log

(2a+ 1 + b(aP ? 1)
a

)
+log(

2a+ 1

a
)
]?1

is compared to the upper bound of Eq. (22):

?FC,II= 1 +
1

2
log(

a+ aP

(2a+ 1)(1 + aP )
)+

+C(
2
?
a

2a+ 1 + b(aP ? 1))

? 1 + C( 2
?
a

a+ 1
)

? 3
2
.

IV. STRONG INTERFERENCE REGIME (a>1)
A. Achievable Sum-Rate

It is remarked that for a?1, there is little benefit using the
private codebooks because: log(1 + Pu2 ) = log(1 +

1
2a ) ? 1.

Therefore, we can modify the achievablity scheme to have
only w1, w2, and v. Here, we propose simple PA policies
for the following cases and prove that the gap between the
corresponding achievable rates and sum-capacity is less than
1 bit for each scenario. For a ? 1, three cases may happen:

1) P ?1?a
2) 1?a?P

3) 1?P ?a
For the first case, user 2 can at most get C(P ) ? 1.

Since a > 1 we can simply use all the available power for
cooperation i.e., Pw=Pu=0.

The common codebooks play an important role for the last
two cases in the IC without the cooperative link. As IC is
a special case of ICUC with C12 = 0, we give most of the
available power to common codewords in these cases.

1) P ?1?a: In this case, we let Pw=Pu=0 and PV =bP .
The achievable sum-rate becomes:

Rsum = min{C12, C(bP )}. (27)

2) 1?a?P : In this case, let Pw = P and Pu = PV = 0.
It is straightforward to show that C

(
(1 + a)Pw

) ? 2C(Pw).
Therefore, the achievable sum-rate becomes:

Rsum = C
(
(1 + a)P

)
. (28)

3) 1?P ?a: In this scenario, it is suggested that receiver
1 jointly decodes w2, v first and then w1. Similarly, receiver 2
jointly decodes w1, v first and then w2. The following power
allocation is used:

Pw = P ? 1,
PV = b.

(29)

The rate constraints are:

Rv? min{C12, C( PV
Pw + 1

)},

Rw? min{C(Pw), C( aPw
Pw + 1

)},

Rv +Rw? C(PV + aPw
Pw + 1

).

Since a ? P , the power allocation leads to:

Rw = min{C(P ? 1), C(a(P ? 1)
P

)} = C(P ? 1).
Therefore, the achievable sum-rate becomes:

Rsum = min
{
min

{
C12, C(

b
P
)
}
+

+ C(P ? 1), C(aP+1+2
?
a

P
)
}
+ C(P ? 1).

(30)

B. Upper Bounds
Using the cut-set analysis [18], transmitter 1 can send at

most up to C12 + C(P ) bits per channel use. It is clear
that transmitter 2 can provide at most C(P ) bits per channel
use to its corresponding receiver. Another upper bound is the
sum-capacity of the CgRC for the specific strong interference
regime reported in [9]. It is easy to verify that the SICUC
with a? 1 satisfies the strong interference conditions of [9].
Therefore, the sum-rate is upper bounded by:

R
(3)
ub = min {C12 + 2C(P ), C(bP )} . (31)

C. Gap Analysis

1) P ?1?a: In this case, if C12?C(bP ), the achievable
sum-rate of Eq. (27) is compared to the first term of the upper
bound of Eq. (31), otherwise it is compared to the second term
of Eq. (31). Therefore the gap ??2C(P )?1.



6
2) 1 ?a? P : In this case, the difference (?) between the
second term in the upper bound of Eq. (31) and the achievable
sum-rate of Eq. (28) is calculated as follows:

?=
1

2
log

(
1 +

2
?
aP

1 + aP + P

)

? 1
2
log

(
1 +

aP + P

1 + aP + P

)

? 1
2
.

3) 1 ? P ? a: Because of Eq. (30) two cases can occur:
� C12 > C

(
b
P

)
: For this condition, the achievable sum-rate

of Eq. (30) is:

Rsum=
1

2
min

{
log(P 2 + bP ), log(P + aP + 1 + 2

?
a)
}

=
1

2
log(P + aP + 1 + 2

?
a).

Then the gap with respect to Eq. (31) becomes:

?=
1

2
log(1 +

2
?
a(P ? 1)

2
?
a+ 1 + aP + P

)

? 1
2
log(1 +

2
?
aP

1 + aP + P
)

? 1
2
log(1 +

aP + P

1 + aP + P
)

? 1
2
.

� C12 ? C
(
b
P

)
: In this case, the gap between the following

achievable sum-rate:

Rsum= min

{
C12 + log(P ),

1

2
log(1 + P + aP + 2

?
a)

}

and the upper bound of Eq. (31) is:
? ? max

{
log(1 + 1

P
), 12 log(1 +

2
?
a(P?1)

1+P+aP+2
?
a
)
}

? 1.

D. Sum-Capacity for Sufficiently Large C12
In [16], we simply showed that if

C12 ? C
(
bP
)
, (32)

by setting Pv1 = Pv2 = P , we can achieve the second term
of Eq. (31) and hence, the sum-capacity of the channel.

V. GDOF ANALYSIS
In this section, the sum-capacity behavior is considered in

the high SNR regime by characterizing the GDOF of the
channel.

It is known that the interference can reduce the available
degrees of freedom for data communication [5]. To understand
this effect, we define the GDOF as below:

d(?, ?) = lim
SNR??

Rsum(SNR, ?, ?)
C(SNR) , (33)

where
? ,

log(INR)
log(SNR) , (34)

and ? ? 0 is the multiplexing gain of the cooperative link,
i.e.,

C12 , ?C(P ). (35)
Here, the GDOF of the proposed scheme with rate constraints
(13-15) is derived.

Using the power allocation (4) and Eq. (34), a can be written
as a = P??1. As mentioned earlier, optimizing the achiev-
able sum-rate of Eq. (17) requires finding optimum power
allocation parameter ?, which is mathematically involved. To
characterize the GDOF, the sum-rate from Eq. (17) for each
region Bi is calculated assuming a suboptimal fixed ?, for
P ??. Then the minimum of the sum-rates is considered for
GDOF analysis. It is clear that this assumption on ? provides
an achievable GDOF that might be smaller than the GDOF
obtained using the optimal ?. However, by deriving the GDOF
associated with the considered upper bounds, it is easy to see
that the assumption does not entail any GDOF loss. In fact
the GDOF associated with the proposed scheme is the optimal
GDOF for the SICUC. Theorem 3 provides the GDOF of the
SICUC:
Theorem 3 The optimal GDOF for the SICUC setup is:

d(?, ?) =

??????
?????

2? 2?+min{?, ?}, ? < 12
min

{
2? ?, 2?+min{?, ?}}, 12 ? ? < 23

2? ?, 23 ? ? < 1
?, 1 ? ? < 2
min

{
2 + ?, ?

}
. 2 ? ?

(36)
Proof: The derivation involves straightforward computa-

tion, and hence is omitted.
It is remarked that if ? = 0, the same GDOF for the symmetric
IC derived in [5] will be obtained.

Figures 2 and 3 show the GDOF as a function of ? and
?. Fig. 2 gives the GDOF for ? ? 12 . Compared to the
GDOF of the IC, one can realize that increasing the amount
of cooperation (corresponding to the value of ?) is beneficial
in terms of the GDOF for the ranges ? < 23 and ? > 2. For
instance, it increases the GDOF from 2?2? to 2?? for ? ? 12
and ? ? ? compared to the IC case. Fig. 3 demonstrates the
GDOF for 12 ? ?. Here, increasing the amount of cooperation
is not useful for ? ? 2. For the purpose of comparison, the
GDOF of the two known extreme cases of ? = 0 i.e., the IC
[5] and ? =?, i.e., the CgRC [9] are plotted as well.

VI. SIGNALLING FOR aP ? 1
The interference-limited regime, i.e., aP ? 1 has so far been

considered in this paper. In this section, the sum-rate analysis
is done for the case that noise is the major performance-
limiting factor in the communication system shown in Fig.
1.

Theorem 4 If aP ? 1, setting Pu = P , i.e., treating the
interference as noise, achieves within 1 bit of the sum-capacity
of the channel.

Proof: For the IC, i.e., C12 = 0, [5] proves the theorem.
The achievable sum-rate becomes:

Rnlsum = log(1 +
P

1 + aP
). (37)



7
0 0.5 1 1.5 2 2.5 3
0.8

1

1.2

1.4

1.6

1.8

2

2.2

2.4

2.6

 ?

d(?
,?)

 

 

?=0 (IC)
?=.15
?=.4

Fig. 2. The effect of partial cooperation on the GDOF of the SICUC for
? < 1

2
.

0 0.5 1 1.5 2 2.5 3 3.5 4
0.5

1

1.5

2

2.5

3

3.5

4

?

d(?
,?)

 

 

?=0 (IC)
?=1.5
?=1.75
?=? (CgRC)

Fig. 3. The effect of partial cooperation on the GDOF of the SICUC for
1

2
? ?. ? = 0 is also considered for the purpose of comparison.

To extend the result of [5], to C12 > 0 case, this rate is
compared to the upper bounds obtained from CgRC.

For a ? 1, the difference ?nla?1 between the achievable
sum-rate of Eq. (37) and the upper bound (22) becomes:

?nla?1=
1

2

[
log

(
1 + (1 +

?
a)2P

)
+ log(

1 + P

1 + aP
)?

?2 log (1 + aP + P
1 + aP

)]

=
1

2

[
log

(1 + (1 +?a)2P
1 + aP + P

)
+ log(

(1 + P )(1 + aP )

1 + aP + P
)

]

=
1

2

[
log

(
1 +

2
?
aP

1 + aP + P

)
+ log

(
1 +

aP 2

1 + aP + P

)]

(a)
? 1

2

[
log

(
1 +

aP + P

1 + aP + P

)
+ log

(
1 +

P

1 + aP + P

)]

? 1,

where (a) follows from 2?a ? (1 + a) and aP ? 1.
Similarly, by comparing the achievable sum-rate of Eq. (37)

and the second term in the upper bound of Eq. (31), it can
be shown that the gap is not greater than 1 bit for a > 1 as

follows:

?nla>1 =
1

2
log

(
1 + (1 +

?
a)2P

)? log (1 + P
1 + aP

)

=
1

2

[
log

(1 + (1 +?a)2P
1 + aP + P

)
+ log

( (1 + aP )2
1 + aP + P

)]

=
1

2

[
log

(
1+

2
?
aP

1+aP+P

)
+ log

(
1+

(aP )2+aP?P
1+aP+P

)]
(a)? 1

2

[
log

(
1 +

aP + P

1 + aP + P

)
+ log

(
1 +

1 + aP ? P
1 + aP + P

)]

? 1
2
[1 + 1],

? 1,
where in (a), 2?a and (aP )2 are replaced by the larger
quantities (1 + a) and 1, respectively.

VII. CONCLUSION
We achieved within two bits of the sum-capacity of the

symmetric interference channel with unidirectional coopera-
tion by applying simple HK scheme in conjunction with co-
operative communication. In particular, we proposed a power
allocation strategy to divide power between private, common
and cooperative codewords for each transmitter in the weak
interference regime. For the strong interference regime we
simplified the achievablity scheme to use one or both of
common and cooperative codebooks for different scenarios.
The simplified schemes ensure the maximum gap of 1 bit from
the sum-capacity of the channel.

APPENDIX A
PROOF OF THEOREM 1

The proof consists of the following steps:
1) R1 ?Ri ? 12 for i ? {3, 4, 5},

2) ?1 , R(1)ub ?R?1 ? 1.2,

3) ?2 , R(2)ub ?R?2 ? 2,

where R1, � � � , R5 are defined in Eq. (20), and

R?i , Ri + 2C(
Pu
2
) for i ? {1, 2}.

To prove the first step, it is sufficient to show

?? , C(
(2 + a)Pw
Pu + 2

)? C( (1 + a)Pw
Pu + 2

) ? 1
2
,

which is true since:

?? =
1

2
log(1 +

Pw
Pw(1 + a) + Pu + 2

) ? 1
2
,

where b is defined in Eq. (26). R?1 and R?2 can be written as:

R?1=
1

2
log

( (2a+ 1)[ab(1 + (2 + a)P ) + a? 2?a]
4a2(1 + b)

)
,

R?2= C12 + log
( (2a+ 1) + b(1 + a+ a2P )

a(1 + b)

)
.



8
We define:

X, abP

A, 1 + b

B, aA

C, (2 + a)(2a+ 1)

D, (2a+ 1)(ab+ a? 2?a).
Hence:

?1= 1 +
1

2

[
log(

AX +B

CX +D
) + log(

aP + a

aP + 1
)
]

(I)? 1 + 1
2
log(

AX +B

CX +D
)

(II)? 1 + 1
2
log(

Ab +B

Cb+D
)

(III)? 1.2
Since a ? 1 (I) is true. Because AD ? BC ? 0, AX+B

CX+D

is a monotonically decreasing function of X for ?D
C
? X .

Noting that aP ?1, it is straightforward to show ?D
C
?b?X ,

therefore (II) is also true. As Ab+B
Cb+D is only a function of a,

(III) can be simply verified. In addition:

?2 = 1 + log
( a(b+ 1)[P + (aP + 1)2]
(aP + 1)

[
b(a+ 1 + a2P ) + 2a+ 1

]),
and it is easy to show that ?2 ? 2. Therefore, the gap is at
most 2 bits which occurs when R = R2, where R is defined
in Eq. (19).

REFERENCES
[1] T. S. Han and K. Kobayashi, �A new achievable rate region for the

interference channel�, IEEE. Trans. Info. Theory, vol. 27, pp. 49�60,
Jan. 1981.

[2] A. S. Motahari and A. K. Khandani, �Capacity bounds for the Gaussian
interference channel�, IEEE. Trans. Info. Theory, vol. 55, no. 2, pp.
620�643, Feb. 2009.

[3] X. Shang, G. Kramer, and B. Chen, �A new outer bound and the noisy-
interference sumrate capacity for Gaussian interference channels�, IEEE.
Trans. Info. Theory, vol. 55, no. 2, pp. 689�699, Feb. 2009.

[4] V. S. Annapureddy and V. V. Veeravalli, �Gaussian interference
networks: Sum capacity in the low-interference regime and new outer
bounds on the capacity region�, IEEE. Trans. Info. Theory, vol. 55, no.
7, pp. 3032�3050, Jul. 2009.

[5] R. Etkin, D. Tse, and H. Wang, �Gaussian interference channel capacity
to within one bit�, IEEE. Trans. Info. Theory, vol. 54, no. 12, pp. 5534�
5562, Dec. 2008.

[6] O. Simeone, D. Gunduz, H. V. Poor, A. Goldsmith, and S. Shamai
(Shitz), �Compound multiple access channels with partial cooperation�,
Submitted to IEEE. Trans. Info. Theory, Jul. 2008.

[7] F. M. J. Willems, �The discrete memoryless multiple channel with
partially cooperating encoders�, IEEE. Trans. Info. Theory, vol. 29,
no. 3, pp. 441�445, May 1983.

[8] C. T. K. Ng, N. Jindal, U. Mitra, and A. Goldsmith, �Capacity gain
from two-transmitter and two-receiver cooperation�, IEEE. Trans. Info.
Theory, vol. 53, no. 10, pp. 3822�3827, Oct. 2007.

[9] I. Maric, R. D. Yates, and G. Kramer, �Capacity of interference channels
with partial transmitter cooperation�, IEEE. Trans. Info. Theory, vol. 53,
no. 10, pp. 3536�3548, Oct. 2007.

[10] I. Maric, A. Goldsmith, G. Kramer, and S. Shamai (Shitz), �On the
capacity of interference channels with one cooperating transmitter�,
European Trans. Telecomm, 2008.

[11] N. Devroye, P. Mitran, and V. Tarokh, �Achievable rates in cognitive
radio channels�, IEEE. Trans. Info. Theory, vol. 52, no. 5, pp. 1813�
1827, May. 2006.

[12] W. Wu, S. Vishwanath, and A. Arapostathis, �Capacity of a class
of interfenrece channels: Interference channels with degraded degraded
message sets�, IEEE. Trans. Info. Theory, vol. 53, no. 11, pp. 4391�
4399, November. 2007.

[13] A. Jovicic and P. Viswanath, �Cognitive radio: An information-theoretic
perspective�, Submitted to IEEE. Trans. Info. Theory, 2006.

[14] G. Kramer, M. Gastpar, and P. Gupta, �Cooperative strategies and
capacity theorems for relay networks�, IEEE. Trans. Info. Theory, vol.
51, no. 9, pp. 3037�3063, Sep. 2005.

[15] V. Prabhakaran and P. Viswanath, �Interference channels with source
cooperation�, availabe at: http://arxiv.org/abs/0905.3109v1, May. 2009.

[16] H. Bagheri, A. S. Motahari, and A. K. Khandani, �Generalized degrees
of freedom of the symmetric Gaussian interference channel with partial
unidirectional transmitter cooperation�, in Conf. Inf. Sciences and
Systems (CISS), Mar. 2009, pp. 307�312.

[17] I. Sason, �On achievable rate regions for the Gaussian interference
channel�, IEEE. Trans. Info. Theory, vol. 50, no. 6, pp. 1345�1356,
Jun. 2004.

[18] T. M. Cover and J. A. Thomas, New York: Wiley, second edition, 2006.
[19] S. I. Gelfand and M. S. Pinsker, �Coding for channel with random

parameters�, Probl. Peredachi Info., vol. 9, pp. 19, 1980.


	Introduction
	System Model
	Weak Interference Regime (a1)
	Achievable Sum-Rate
	Power Allocation Policies and the Associated Sum-Rates
	Universal PA
	Full Cooperation PA

	Upper Bounds
	Gap Analysis

	Strong Interference Regime (a>1)
	Achievable Sum-Rate
	P1a
	1aP
	1Pa

	Upper Bounds
	Gap Analysis
	P1a
	1aP
	1Pa

	Sum-Capacity for Sufficiently Large C12

	GDOF Analysis
	Signalling for aP1
	Conclusion
	Appendix A: Proof of Theorem 1
	References

