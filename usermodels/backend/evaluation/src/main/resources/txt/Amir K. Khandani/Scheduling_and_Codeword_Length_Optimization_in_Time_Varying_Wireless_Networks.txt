
ar
X

iv
:c

s/0
60

60
71

v2
  [

cs
.IT

]  
28

 M
ar 

20
07

1

Scheduling and Codeword Length Optimization
in Time Varying Wireless Networks
Mehdi Ansari Sadrabadi, Alireza Bayesteh and Amir K. Khandani

Coding & Signal Transmission Laboratory(www.cst.uwaterloo.ca)
Dept. of Elec. and Comp. Eng., University of Waterloo

Waterloo, ON, Canada, N2L 3G1
Tel: 519-884-8552, Fax: 519-888-4338

e-mail: {mehdi, alireza, khandani}@cst.uwaterloo.ca

Abstract

In this paper, a downlink scenario in which a single-antenna base station communicates with K single antenna
users, over a time-correlated fading channel, is considered. It is assumed that channel state information is perfectly
known at each receiver, while the statistical characteristics of the fading process and the fading gain at the beginning
of each frame are known to the transmitter. By evaluating the random coding error exponent of the time-correlated
fading channel, it is shown that there is an optimal codeword length which maximizes the throughput. The throughput
of the conventional scheduling that transmits to the user with the maximum signal to noise ratio is examined using
both fixed length codewords and variable length codewords. Although optimizing the codeword length improves
the performance, it is shown that using the conventional scheduling, a gap of ?(

?
log log logK) exists between

the achievable throughput and the maximum possible throughput of the system. A simple scheduling that considers
both the signal to noise ratio and the channel time variation is proposed. It is shown that by using this scheduling,
the gap between the achievable throughput and the maximum throughput of the system approaches zero.

Index Terms

Downlink scheduling, multiuser diversity, Rayleigh fading, time varying channels.

I. INTRODUCTION
In wireless networks, diversity is a means to combat the time varying nature of the communication link.

Conventional diversity techniques over point-to-point links, such as spatial diversity and frequency diversity
are widely used and offer performance improvements. In multiuser wireless systems, there exists another
form of diversity, called multiuser diversity [1]. In a broadcast channel where users have independent
fading and feed back their signal to noise ratio (SNR) to the base station (BS), system throughput is
maximized by transmitting to the user with the strongest SNR [1], [2].

Multiuser diversity was introduced first by Knopp and Humblet [3]. It is shown that the optimal
transmission strategy in the uplink of multiuser systems using power control is to only let the user with
the largest SNR transmit. A similar result is shown to be valid for the downlink [4]. Multiuser diversity
underlies much of the recent works for downlink scheduling [5]�[8] as in Qualcomm�s high data rate
(HDR) system [9], [10]. In [8], [11], the opportunistic scheduling is based on the highest data rate which
can be reliably transmitted to each user. Distributed scheduling is proposed in an uplink scenario, where
full channel state information (CSI) is not required at the transmitter [12], [13]. Multiuser diversity has
also been studied in the context of multiple antenna systems [1], [14] and ad-hoc networks [15].

In wireless networks, the rate of channel variations is characterized by maximum Doppler frequency
which is proportional to the velocity. Utilizing multiuser diversity in such environments needs to be
revisited since the throughput depends not only on the received SNR, but also on how fast the channel
varies over time.

In this paper, we consider a broadcast channel in which a BS transmits data to a large number of users in
a time-correlated flat fading environment. It is assumed that CSI is perfectly known to the receivers, while
BS only knows the statistical characteristics of the fading process for all the users (which is assumed to be




2
constant during a long period). Moreover, each user feeds back its channel gain to the BS at the beginning
of each frame. Based on this information, BS selects a single user for transmission in each frame, in order
to maximize the throughput. For the case of Additive White Gaussian Noise (AWGN) or block fading, it is
well known that increasing the codeword length results in improving the achievable throughput. However,
in a time varying channel, it is not possible to obtain arbitrary small error probabilities by increasing the
codeword length. In fact, increasing the codeword length also results in increasing the fading fluctuations
over the frame, and consequently, the throughput will decrease. Therefore, it is of interest to find the
optimum codeword length which maximizes the throughput.

In this paper, a downlink scenario in which a single-antenna base station communicates with K single
antenna users, over a time-correlated fading channel, is considered. We analyze different user selection
strategies; i) the BS transmits data to the user with the strongest SNR using fixed length codewords
(conventional multiuser scheduling), ii) the BS transmits data to the user with the strongest SNR using
variable length codewords, and iii) the BS transmits data to the user that achieves the maximum throughput
using variable length codewords. We show that in all cases the achievable throughput scales as log logK.
Moreover, in cases (i) and (ii), the gap between the achievable throughput and the maximum throughput
scales as

?
log log logK , while in case (iii), this gap approaches zero.

The rest of the paper is organized as follows. In Section II, the model of time-correlated fading channel is
described. In Section III, different user selection strategies are discussed and the corresponding throughput
of the system is derived for each strategy, for K ??. Finally, in Section IV, we conclude the paper.

Throughout this paper, E{.} and var{.} represents the expectation and variance, respectively, �log� is
used for the natural logarithm, and rate is expressed in nats. For given functions f(N) and g(N), f(N) =
O(g(N)) is equivalent to limN??

??? f(N)g(N) ??? < ?, f(N) = o(g(N)) is equivalent to limN?? ??? f(N)g(N) ??? =
0, f(N) = ?(g(N)) is equivalent to limN?? f(N)g(N) = ?, and f(N) = ?(g(N)) is equivalent to
limN??

f(N)
g(N)

= c, where 0 < c <?.

II. SYSTEM MODEL
The channel of any given user is modeled as a time-correlated fading process. It is assumed that the

channel gain is constant over each channel use (symbol) and varies from symbol to symbol, following
a Markovian random process. Assume that the fading gain of kth user is hk = [h1,k, . . . , hNk,k]T where
hi,k, 1 ? i ? Nk are complex Gaussian random variables with zero mean and unit variance and Nk is the
codeword length of the kth user. The received signal for the kth user is given by

rk = Skhk + nk, (1)
where Sk = diag(s1,k, s2,k, . . . , sNk,k) is the transmitted codeword with the power constraint1 E{|si,k|2} ?
P , and nk is AWGN with zero mean and covariance matrix I. Assume that h0,k is the fading gain at
the time instant before Sk is transmitted. The sequence ui,k = |hi,k|, 0 ? i ? Nk, is assumed to be a
stationary ergodic chain with the following probability density function [16]:

fu0,k(u) =

{
2ue?u

2
u ? 0

0 otherwise , (2)

f(u1,k, u2,k, � � � , uNk,k|u0,k) =
Nk?
i=1

qk(ui,k|ui?1,k), (3)

where,

qk(u|v) =
{

2u
1??2k

exp
(
?u2+?2kv2

1??2k

)
I0(2?kuv1??2k ) u ? 0

0 otherwise

1Obviously, for maximizing the throughput, the power constraint translates to E{|si,k|2} = P .



3
in which 0 < ?k < 1 describes the channel correlation coefficient of the kth user. It is assumed that
?k, 1 ? k ? K, are i.i.d. random variables with uniform distribution which remain fixed during the
entire transmission, and I0(.) denotes the modified Bessel function of order zero. It is assumed that CSI
is perfectly known at each receiver, while the statistical characteristics of the fading process and u0,k,
1 ? k ? K are known to the transmitter.

III. THROUGHPUT ANALYSIS
In this section, we derive the achievable throughput of the system in the asymptotic case of K ??.

We define the kth user�s throughput per channel use, denoted by Tk, as

Tk , Rk(1? pe(k)), (4)
where Rk is the transmitted rate per channel use and pe(k) is the frame error probability for this user.
Using the concept of random coding error exponent [17], pe(k) can be upper-bounded as

pe(k) ? inf
0???1

e?N(Ek(?)??Rk). (5)

For simplicity of analysis, we use this upper-bound in evaluating the throughput. This bound is tight for
rates close to the capacity as used in [18]�[20].

Assuming si,k, 1 ? i ? Nk, are Gaussian and i.i.d., it is shown that the random coding error exponent
for the kth user, Ek(?), is given by [20],

Ek(?) = ? 1
Nk

logEuk

{
Nk?
i=1

(
1

1 + P
1+?

u2i,k

)?}
. (6)

where uk = [u1,k, . . . , uNk,k].
In the following, we assume that u0,k ? 1. Since in strategies introduced in this work, a user is selected

if the corresponding initial fading gain is maximum or above a certain threshold, this assumption is valid
when the number of users is large.

Theorem 1 For the channel model described in the previous section, and assuming u0,k is known, we
have

Ek(?) =
1

Nk

Nk?
i=1

? log

(
1 +

Pu20,k?
2i
k

(1 + ?)

)
+O

(
1?
u0,k

)
? O

(
e?u

2
0,k

)
. (7)

Proof: See Appendix A.



4
Minimizing (5) is equivalent to maximizing Ek(?)? ?Rk. Noting (7), we have

Ek(?)? ?Rk = 1
Nk

Nk?
i=1

? log

(
1 +

Pu20,k?
2i
k

(1 + ?)

)
? ?Rk +O

(
1?
u0,k

)
?O

(
e?u

2
0,k

)

=
1

Nk

Nk?
i=1

? log

(
Pu20,k?

2i
k

(1 + ?)

)
? ?Rk + 1

Nk

Nk?
i=1

? log

(
1 +

(1 + ?)

Pu20,k?
2i
k

)

+ O

(
1?
u0,k

)
? O

(
e?u

2
0,k

)

=
?

Nk

Nk?
i=1

(
log
(
Pu20,k

)
+ 2i log(?k)? log(1 + ?)

)? ?Rk
+

1

Nk

Nk?
i=1

O

(
1

u20,k

)
+O

(
1?
u0,k

)
?O

(
e?u

2
0,k

)
= ?[log(Pu20,k) + (Nk + 1) log(?k)? log(?+ 1)? Rk]
+ O

(
1?
u0,k

)
? O

(
e?u

2
0,k

)
(8)

It is easy to show that ?optk which maximizes (8) for large values of u0,k is

log(1 + ?optk ) +
?optk

1+?optk
= ?k, ?k < log(2) +

1
2

?optk = 1, ?k ? log(2) + 12
(9)

where
?k = log(Pu

2
0,k) + (Nk + 1) log(?k)?Rk. (10)

Using (4), (5), (7) and (8), we have
Tk = Rk

[
1? e?Nk(Ek(?optk )??optk Rk)

]
= Rk

[
1? e??optk Nk(log(Pu20,k)+(Nk+1) log(?k)?log(?optk +1)?Rk)

]
. (11)

It is easy to show that Tk is a concave function of variables Rk and Nk, and the values of Rk and Nk
which maximize the throughput (Roptk and Noptk ) satisfy the following equations2:

Roptk = log(Pu
2
0,k) + (2N

opt
k + 1) log(?k)? log(?optk + 1), (12)

Noptk =

?
log
(
1 + ?optk N

opt
k R

opt
k

)
?optk log(?

?1
k )

. (13)

It follows that Noptk ? ? and Roptk ? ? as u0,k ? ?. Using (12) and (13), (11) can be re-written as
follows:

Tk =

(
log

(
Pu20,k

?optk + 1

)
+ (2Noptk + 1) log(?k)

)(
1? 1

1 + ?optk N
opt
k R

opt
k

)
(14)

Substituting (12) in (10), we have
?k = N

opt
k log(?

?1
k ) + log(?

opt
k + 1). (15)

2Note that we have relaxed the condition of Nk being integer. However, since the optimizing Nk tends to infinity as K ? ?, this
assumption does not affect the result.



5
From (9) and (15), it is concluded that

?optk =

{
Noptk log(?

?1
k )

1?Noptk log(??1k )
Noptk log(?

?1
k ) <

1
2

1 Noptk log(?
?1
k ) ? 12

(16)

Noting (12) and (16), for ?k = 1, we have ?optk = 0 and Roptk = log(Pu20,k) which corresponds to the
capacity of a quasi-static fading channel (for large values of channel gain u20,k).

In the following, we obtain the asymptotic throughput of the kth user. Since there are two regions
for ?optk as shown in (16), we calculate the closed form formula of the throughputs of these two cases
separately.

� (?optk = 1): The corresponding asymptotic throughput is obtained by substituting (12) and (13) in
(11) as follows:

Tk = log

(
Pu20,k
2

)
? 2

?
log(??1k ) log log

(
Pu20,k
2

)
�(

1 +O

(
log log log(u0,k)

log log(u0,k)

))
. (17)

From (13), the optimum codeword length scales as follows:

Noptk ?
?

log log(Pu20,k)

log??1
. (18)

Note that if ?k is fixed and ?k 6= 1, then Noptk log(??1k ) ? 12 for large values of u0,k and (17) is
appliable.

� (?optk < 1): We limit the calculation of the throughput to the assumption of Noptk log(??1k ) ? o(1) to
be able to derive the following closed form formula by using (14) and (16):

Tk = log(Pu
2
0,k)? 2 3

?
log(??1k ) log log

(
Pu20,k

)�(
1 +O

(
log log log(u0,k)

log log(u0,k)

))
? o(1). (19)

The corresponding optimum codeword length scales as follows:

Noptk ? 3
?

log log(Pu20,k)

(log??1)2
. (20)

As we will see later, this special case is sufficient to accomplish the calculations required in III-C.
From the above equations, it is concluded that the throughput not only depends on the initial fading gain,
u0,k, but also on the fading correlation coefficient. Moreover, the throughput is an increasing function of
the channel correlation coefficient.

In the following, we introduce three scheduling strategies in order to maximize the throughput; i)
Traditional scheduling in which the user with the largest channel gain is selected (SNR-based scheduling)
and the codeword length is assumed to be fixed. ii) SNR-based scheduling with optimized codeword
length regarding the channel condition of the selected user, and iii) Scheduling which exploits both the
channel gain and the channel correlation coefficient of the users. The asymptotic throughput of the system
is derived under each strategy for K ??.



6
A. Strategy I: SNR-based scheduling with fixed codeword length
The BS transmits to the user with the maximum initial fading gain. The codeword length of all users

is fixed, i.e., N1 = N2 = � � � = NK = N . The codeword length N is selected such that the throughput of
the system is maximized. The BS adapts the data rate to maximize the throughput of the selected user.

Theorem 2 The asymptotic throughput of the system under Strategy I scales as

T 1 ? log
(
P logK

2

)
? 2

?
E{log(??1)} log log logK, (21)

as K ??.
For simplicity of notation, we define ?k , u20,k. Let ? = max1?k?K ?k and ? be the corresponding

correlation coefficient of the selected user. Setting the derivative of (11) with respect to Rk to zero, we
find the rate of the selected user and the corresponding throughput in terms of ? and ? as follows3 :

R = log

(
P?

1 + ?opt

)
+ (N + 1) log(?)? log(1 + ?

optNR)

?optN
, (22)

T1(?, ?) =
[
log

(
P?

1 + ?opt

)
+ (N + 1) log(?)? log(1 + ?

optNR)

?optN

]
�[

1? 1
1 + ?optNR

]
, (23)

where noting (9), ?opt is determined as follows:
log(1 + ?opt) + ?

opt

1+?opt
= ?, ? < log(2) + 1

2

?opt = 1, ? ? log(2) + 1
2

(24)

Using (10) and (22), we have

? = log(1 + ?opt) +
log(1 + ?optNR)

?optN
. (25)

Let us define R? and event A as follows:
R? , log(P?) + (N + 1) log(?)

(22),(25)
= R + ?, (26)

A ? {R? > 1
2
log logK}, (27)

In the following, we derive upper-bounds for the throughput of the system in terms of R? and A which
we use later in Lemma 1 and Lemma 2.

T 1 = E{T1(?, ?)}
(22),(23)
? E{R}
= E{R|A}Pr{A}+ E{R|AC}Pr{AC}

(26)
= (E{R?|A} ? E{?|A})Pr{A}+ E{R|AC}Pr{AC}
? E{R?} ? E{?|A}Pr{A} (28)
? E{R?} (29)

3We drop user index of parameters R and ?opt for the selected user.



7
where (28) is derived by replacing R with R?, noting R ? R?, and Pr{A} can be computed as follows:
Pr{A} = Pr{log(P?) + (N + 1) log? > 1

2
log logK}

= 1? Pr{log(P?) + (N + 1) log? ? 1
2
log logK}

= 1?
? ?
0

Pr
{
log? <

1

N + 1

(
1

2
log logK ? log(Px)

)????x
}
f?(x)dx

= 1?
? ?
0

F?

(
e

1
N+1(

1
2
log logK?log(Px))

)
f?(x)dx, (30)

where fy(.) and Fy(.) are probability density function and cumulative density function of random variable
y, respectively. Noting that ? has a uniform distribution, we have

Pr{A} = 1? e log logK2(N+1)
? ?
0

e?
log(Px)
N+1 f?(x)dx

? 1? e? log logK+2 log(P )2(N+1) , (31)
where the second line follows from the fact that ? ? logK, with probability one [21].

According to (24), there are two regions for ?opt of the selected user. To obtain the throughput of the
system, we upper-bound the throughput in these two regions in Lemma 1 and Lemma 2, respectively.
Then, we derive a lower-bound for the throughput of the system in Lemma 4.

Lemma 1 Assuming ? < log(2) + 1
2
, the throughput of Strategy I is upper-bounded as follows:

T 11 . log(P logK)? (log(log logK ? 2 log(2)))E{log(??1)}Pr{A}. (32)
Proof: Using (24) and (25) and noting ? < log(2) + 1

2
, we obtain

(?opt)?1 + (?opt)?2 =
N

log(1 + ?optNR)
. (33)

Noting ?opt < 1, it follows from (33) that
N > log(1 + ?optNR). (34)

Assuming R is large enough, from (33), we have
?optNR

log(1 + ?optNR)
> 2R? ?optN > 2. (35)

Using (34) and (35), we can write

N
(34)
> E

{
log(1 + ?optNR)|A}Pr{A}

(35)
> E{(log(1 + 2R))|A}Pr(A)
a
> (log(log logK ? 2 log(2)))Pr{A} (36)

where (a) results from the fact that conditioned on A, we have R = R? ? ? > 1
2
log logK ? 1

2
? log(2).

Noting that ? ? logK + O(log logK) with probability one [21], we can write the throughput of the
system as follows:

T 11
(26),(29)
? E{log(P?) + (N + 1) log(?)}
= E{log(P?)} ? (N + 1)E{log(??1)}

(36)
. log(P logK)? (log(log logK ? 2 log(2)))E{log??1}Pr{A}. (37)



8
?

Lemma 2 Assuming ? ? log(2) + 1
2
, the throughput of Strategy I is upper-bounded as follows:

T 12 . log
(
P logK

2

)
? 2

?
E{log??1}

?
log log logK. (38)

Proof: Noting ? ? log(2) + 1
2
, from (24), we have ?opt = 1. Hence, using (26) and (28) and noting

? ? logK, we can write
T 12 ? log (P logK)? (N + 1)E{log(??1)} ? E{?|A}Pr{A}

(25)
. log (P logK)? (N + 1)E{log(??1)} ? E{log(2) + log(1 +NR)

N
|A}Pr{A}

a

. log (P logK)? (N + 1)E{log(??1)}

?
[
log(1

2
log logK ? log(1+N log(P logK))

N
? log(2))

N
+

logN

N
+ log(2)

]
Pr{A},

(39)
where (a) follows from the fact that conditioned on A, we have

R = R? ? ?
>

1

2
log logK ? log(1 +NR)

N
? log(2)

>
1

2
log logK ? log(1 +N log(P logK))

N
? log(2), (40)

The last line results from the fact that R < log(P logK) which follows from (22). Substituting (31) in
(39), and setting the derivative of T 12 to zero with respect to N , we obtain

Nopt ?
?

log log logK

E{log??1} [1 + o(1)]. (41)

Substituting (31) and (41) in (39), the result of the lemma follows.
Lemma 3 Assume that N is set as in (41). Then, under condition A, we have ?opt = 1 as K ??.
Proof: If ?opt < 1, then using (24), we have ? < 1

2
+ log(2). Noting (34) and (35), we can write

Nopt
(34)
> log(1 + ?optNoptR)

(35)
> log(1 + 2R)

(26)
= log(1 + 2(R? ? ?))
> log(2R? ? 2 log(2))

(27)? log log logK, (42)
which contradicts (41).

?

Lemma 4 The throughput of Strategy I is lower-bounded as follows:

T 1 & log
(
P logK

2

)
? 2

?
E{log??1} log log logK. (43)



9
Proof: Choosing N = Nopt and subsequently replacing ?opt = 1 (Lemma 3), we compute R in (22),
under condition A, as follows:

R = R? ? ?
>

1

2
log logK ? log(2)? log(1 +N

optR)

Nopt

>
1

2
log logK ? log(2)? log(1 +N

opt log logK)

Nopt

? 1
2
log logK ?

?
E{log??1} log log logK) (44)

Using (23) and (26), we can lower-bound T 1 as
T 1 ? T 1|APr{A}

= E

{[
R? ? log(1 + ?opt)? log(1 + ?

optNoptR)

?optNopt

] [
1? 1

1 + ?optNoptR

]????A
}
�

Pr{A}, (45)
where T 1|A denotes the throughput of the system conditioned on A. Since E{R?} = E{R?|A}Pr{A}+
E{R?|AC}Pr{AC}, and E{R?|A} > E{R?|AC}, it follows that E{R?|A} ? E{R?}. Having this fact,
noting Lemma (3) and using (31), (44) and (45), we can write

T 1 ?
(

E{R?} ? log(2)? E
{
log(1 +Nopt log

(
P logK

2

)
)

Nopt

?????A
})

�
(
1? 1

1 +Nopt(1
2
log logK ??E{log??1} log log logK))

)(
1? e?

log logK

2(Nopt+1)

)

? log
(
P logK

2

)
? 2

?
E{log??1} log log logK. (46)

?
The Proof of Theorem 2: Lemma (1) and Lemma (2) provide upper-bounds on two complementary

cases where ?opt of the selected user is either less than 1 or equal to 1 in (32) and (38), respectively.
Lemma (4) lower-bounds the throughput of the system as in (43). Comparing (32), (38) and (43), we
conclude the result of the theorem.

?
Remark 1- To prove Theorem 2, we utilize the distribution function of ? to calculate Pr(A). The value
of Pr(A) is used in (37), (39) and (46). For Pr(A) = 1 ? o

(
1

log logK

)
, (37), (39) and (46) are valid.

Therefore, the assumption of uniform distribution for the correlation coefficients can be relaxed if Pr(A) =
1? o

(
1

log logK

)
.

B. Strategy II: SNR-based scheduling with adaptive codeword length
In this scheme, the BS transmits to the user with the maximum initial fading gain. The rate and codeword

length are selected to maximize the corresponding throughput.

Theorem 3 Assuming K ? ?, the asymptotic throughput of the system under Strategy II scales as
follows:

T 2 ? log
(
P logK

2

)
? 2E{

?
log(??1)}

?
log log

(
P logK

2

)
. (47)



10

Proof: The throughput of the system can be written as

T 2 = T 2|BPr{B}+ T 2|BCPr{BC}, (48)
where B represents the event that ?opt = 1, T 2|B denotes the throughput conditioned on B, and T 2|BC is
the throughput of the system conditioned on BC , the complement of B. Using (17), we can write

T 2|B = E
{
log

(
P?

2

)
? 2

?
log(??1) log log

(
P?

2

)(
1 +O

(
log log log(?)

log log(?)

))?????B
}
,

(49)
where ? = max1?k?K ?k, and ? is the channel correlation coefficient of the selected user. Noting that
? ? logK +O(log logK) with probability one, and ? and ? are independent, we have

T 2|B ? log
(
P logK

2

)
? 2E

{?
log(??1)

???B}
?

log log

(
P logK

2

)
�(

1 +O

(
log log log logK

log log logK

))
. (50)

Using (16) and (18), we can write

B ? Nopt log(??1) ? 1
2

?=
?

log(??1)
?

log log logK ? 1
2
. (51)

Uniform distribution for ? results in exponential distribution for X , log(??1), i.e., fX(x) = e?xu(x).
Let us define ? , 1

4 log log logK
. Pr{B} can be derived as follows:

Pr{B} = Pr{log(??1) ? ?}
= e?? (52)

Using (52), we have

E

{?
log(??1)

???B} =
?
B
?
xe?xdx

Pr{B}
=

??
?

?
xe?xdx

Pr{B}

? E{
?

log(??1)} ? ???e??
e??

? E{
?

log(??1)}(1 +O(?)). (53)
Similarly, we can write

E

{?
log(??1)

???BC} =
? ?
0

?
xe?xdx

Pr{BC}
? ?

?
?e??

1? e??
? O(??). (54)



11

Using (14), T 2|BC can be written as

T 2|BC = E
{(

log

(
P?

1 + ?opt

)
? (2Nopt + 1) log(??1)

)(
1? 1

1 + ?optNoptRopt

)????BC
}

a

& E

{(
log

(
P?

1 + ?opt

)
? 2 ?

opt

1 + ?opt
? log(??1)

)(
1? 1

1 + ?optNoptRopt

)????BC
}

b

& E

{(
log

(
P?

1 + ?opt

)
? 2 ?

opt

1 + ?opt
? log(??1)

)(
1? 1

1 +Ropt

)????BC
}

& log

(
P logK

2

)
? E{log(??1)|BC} ? 2

(54)
& log

(
P logK

2

)
? 2?O(??), (55)

where (a) follows from (16) which implies Nopt log(??1) = ?opt
1+?opt

conditioned on BC and (b) results
from the following inequality:

?optNoptRopt
(16)
=

Ropt(Nopt)2 log(??1)
1?Nopt log(??1)

? Ropt(Nopt)2 log(??1)
(13)
= Ropt

log(1 + ?optNoptRopt)

?opt
a? Ropt log(1 +NoptRopt)
& Ropt (56)

where (a) follows from the fact that log(1+?optNoptRopt)
?opt

is a decreasing function of ?opt. Moreover, using
(14) and noting that ? ? logK with probability one, we have

T 2|BC ? E {log (P?)| BC
}

. log(P logK) (57)
Combining (55) and (57), we have

log

(
P logK

2

)
? 2?O(??) . T 2|BC . log

(
P logK

2

)
+ log(2) (58)

Substituting (50) and (58) in (48) and noting (52) and (53), after some manipulations, we have

T 2 ?
(
log

(
P logK

2

)
? 2E{

?
log(??1)|B}

?
log log

(
P logK

2

))
Pr{B}

+

((
P logK

2

)
+ ?(1)

)
Pr{BC}

? log
(
P logK

2

)
? 2E{

?
log(??1)}

?
log log

(
P logK

2

)

+ 2E{
?

log(??1)}
?

log log

(
P logK

2

)
?
?
?e?? + ?(1)O(

?
?)

? log
(
P logK

2

)
? 2E{

?
log(??1)}

?
log log

(
P logK

2

)
+O(

?
?) (59)



12

which completes the proof of Theorem 3.
?

Remark 1- To prove Theorem 3, we used the following properties:

E

{?
log(??1)

???B} ? E{?log(??1)}(1 +O(?))
E

{?
log(??1)

???BC} ? O(??)
Pr{BC} ? O(?), (60)

where ? ? 1
log log logK

. The theorem is valid for any distribution function of ? that satisfies the above
properties.
Remark 2- Since E{?x} ? ?E{x}, for x > 0, it is concluded that the achievable rate of Strategy II is
higher than that of Strategy I. More precisely,

T 2 ? T 1 ? 2
(?

E{log(??1)} ? E{
?

log(??1)}
)?

log log logK. (61)
For the case of uniform distribution for ?, we have

T 2 ? T 1 ? 0.228
?

log log logK. (62)
Remark 3- Although limK?? T 1T max = limK??

T 2
T max = 1, where T max ? log (P logK) is the maximum

achievable throughput for a quasi-static fading channel [21], there exists a gap of ?(?log log logK)
between the achievable throughput of Strategies I and II, and the maximum throughput. As we show later,
this gap is due to the fact that the channel correlation coefficients of the users are not considered in the
scheduling. In fact, this gap approaches zero by exploiting the channel correlation, which is discussed in
Strategy III.

C. Strategy III: Scheduling based on both SNR and channel correlation coefficient with adaptive codeword
length

To maximize the throughput of the system, the user which maximizes the expression in (14) should be
serviced. Here, for simplicity of analysis, we propose a sub-optimum scheduling that considers the effect
of both SNR and channel correlation in the user selection. In this strategy, each user is required to feed
back its initial fading gain only if it is greater than a pre-determined threshold

?
?, where ? is a function

of the number of users. Among these users, the BS selects the one with the maximum channel correlation
coefficient. The data rate and codeword length are selected to maximize the corresponding throughput.
The following theorem gives the system throughput under this strategy.

Theorem 4 Using Strategy III, with ? satisfying
logK ? o(logK) . ? . logK ? log logK ? ?(1), (63)

the throughput of the system scales as
T 3 & log (P logK)? o(1) (64)

Proof: Define S , {k|?k ? ?} and ?max , maxk?S ?k. Let ? be the squared initial fading gain of the
user corresponding to ?max. We define the event G as follows:

G ? Nopt log(??1max) ? o(1), (65)
where Nopt is the corresponding codeword length as computed from (13). Using (19) and (65), we can
write

T 3 ? Pr{G}E{T3(?, ?max)|G} (66)



13

where following (19),
E{T3(?, ?max)|G} = E

{
log (P?)? 2 3

?
log(??1max)

3
?

log log (P?)

[
1 +O

(
log log log(?)

log log(?)

)]
? o(1)

????G
}

(67)

Noting that {T3(?, ?max)|G} in (67) is an increasing function of ?, we have
E{T3(?, ?max)|G} ? log (P?)? 2E{ 3

?
log(??1max)|G}

3
?

log log (P?)

[
1 +O

(
log log log(?)

log log(?)

)]
? o(1)

a? log (P?)? 2E{ 3
?

log(??1max)}
3
?

log log (P?)

[
1 +O

(
log log log(?)

log log(?)

)]
? o(1)

b? log (P?)? 2 3
?

E{log(??1max)}
3
?

log log (P?)

[
1 +O

(
log log log(?)

log log(?)

)]
? o(1), (68)

where (a) follows from the fact that E{ 3
?

log(??1max)|G} ? E{ 3
?

log(??1max)} and (b) results from the
convexity of cube root. For large values of K, E{log(??1max)} can be approximated as follows (See
Appendix B):

E{log(??1max)} ?
1

Ke??

(
1 +O

(
1

Ke??

))
+ e?Ke

??
(?? logK). (69)

Noting (69), for values of ? satisfying (63), we have
E{log(??1max)} log log (?) ? o(1). (70)

Using (68) and (70), we can write
E{T3(?, ?max)|G} ? log (P logK)? o(1). (71)

To compute Pr{G} defined in (65), we use Chebychev inequality.

Pr
{
|Z ? E{Z}| <

?
3
?

logKvar{Z}
}
> 1? 1

3
?
logK

, (72)

where Z = Nopt log(??1max). Noting (20), (63) and (69), we have

E{Nopt log(??1max)}
(20)
? E{ 3

?
log(??1max) log log (P?max)}

= E{ 3
?

log(??1max)}E{ 3
?

log log (P?max)}
a

. 3
?

E{log(??1max)} 3
?

log log (P logK)

(70)
= o(1) (73)



14

where (a) follows from the fact the ?max ? log(K) with probability one. Also, noting (20), (63) and (69),
we have

var{Nopt log(??1max)} = var{ 3
?

log(??1max) log log (P?)}
? E{(log(??1max) log log (P?))

2
3}

? (E{log(??1max)})
2
3 (E{log log (P?)}) 23

(63),a
. O

(
1

(logK)
2
3

)
O((log log logK)

2
3 )

= O

((
log log logK

logK

) 2
3

)
(74)

where (a) follows from the fact that ? ? logK + O(log logK) with probability one. Substituting (73)
and (74) in (72), we have

Pr

{
|Nopt log(??1max)? o(1)| < O

((
log log logK?

logK

) 1
3

)}
> 1? 1

3
?
logK

(75)

Noting (66), (71), and (75), the result of the theorem follows.
Remark 1- The uniform distribution of the correlation coefficients is not a necessary condition for Theorem
4. In fact, Theorem 4 is valid if Pr{G} ? 1? o

(
1

log logK

)
. Pr{G} can be written as

Pr{Nopt log(??1max) < g(K)} = Pr{ 3
?

log(??1max) log log (P logK) < g(K)}
= Pr{?max > e

?g(K)3

log log(P logK) }
= 1? (F?(e

?g(K)3

log log(P logK) ))K (76)

where g(K) satisfies g(K) ? o(1). Noting (76), there must exist a function g(K) such that F?(e
?g(K)3

log log(P logK) ) ?
1? ? ( log log logK

K

)
to satisfy Pr{G} ? 1? o

(
1

log logK

)
. Hence, there exists a larger class of distributions

that satisfy the requirements for this theorem.

IV. CONCLUSION
A multiuser downlink communication over a time-correlated fading channel has been considered. We

have proposed three scheduling schemes in order to maximize the throughput of the system. Assuming a
large number of users in the system, we show that using SNR-based scheduling, a gap of ?(

?
log log logK)

exists between the achievable throughput and the maximum throughput of the system. We propose a simple
scheduling, considering both the SNR and channel correlation of the users. We show that the gap between
the throughput of the proposed scheme and the maximum throughput of the system approaches zero as
the number of users tends to infinity.

APPENDIX A
For simplicity, we drop the user index. Noting (6), we have E0(?) = ? 1N log IN , where

IN =

?
uN

...

?
u1

N?
i=1

(
1

1 + P
1+?

u2i

)?
p(u|u0)dui. (77)

Using (3), we have

IN =

?
uN

...

?
u1

N?
i=1

2ui
1? ?2 exp

{
?u

2
i + ?

2u2i?1
1? ?2

}
I0
(
2?uiui?1
1? ?2

)(
1

1 + P
1+?

u2i

)?
dui. (78)



15

Substituting vi = ui
u0
?

(1??2)/2 , 0 ? i ? N , we have

IN =

?
vN

...

?
v1

N?
i=1

u20vie
? v

2
i+?

2v2i?1
2/u20 I0(?u20vivi?1)f(vi)dvi

=

?
vN

...

?
v1

N?
i=1

u20vie
? (vi??vi?1)

2

2/u2
0 e??u

2
0vivi?1I0(?u20vivi?1)f(vi)dvi, (79)

where,

f(vi) =

?
? 1

1 +
Pu20(1??2)
2(1+?)

v2i

?
?

?

. (80)

For large values of u0, we evaluate the following integral.

I = u20ve
? (v?�)2

2/u2
0 e?u

2
0v�I0(u20v�)?(v)

=

? ?
0

g(v)
1?

2?/u20
e
? (v?�)2

2/u2
0 dv, (81)

where g(v) ,
?
2?vu0I0(u20v�)e?u20v�?(v) and ?(v) is differentiable and satisfies 0 ? ?(v) ? 1 and

?(v) ? O( 1
u?0
). Noting that [22]

I0(z)e?z
?
2?z = 1 +O

(
1

z

)
, z ? 1 (82)

it is easy to show that g(n)(�) is bounded for � ? 0 and n ? 1. Using Taylor series of g(?) about �, we
have

I =

? ?
0

(
g(�) +

??
n=1

g(n)(�)

n!
(v ? �)n

)
1?

2?/u20
e
? (v?�)2

2/u20 dv

= g(�)(1?Q(�u0)) +
? ?
0

??
n=1

g(n)(�)

n!
(v ? �)n 1?

2?/u20
e
? (v?�)2

2/u2
0 dv

= g(�)(1?Q(�u0)) +
? �+ 1?

u0

[�? 1?
u0

]+

??
n=1

g(n)(�)

n!
(v ? �)n 1?

2?/u20
e
? (v?�)2

2/u2
0 dv + ?

= g(�)(1?Q(�u0)) +O
(
g?(�)?
u0

)
+ ?. (83)



16

where ? can be bounded as follows:

?
a?
? [�? 1?

u0
]+

0

g(v)?
2?/u20

e
? (v?�)2

2/u2
0 dv +

? ?
�+ 1?

u0

g(v)?
2?/u20

e
? (v?�)2

2/u2
0 dv

b?
?
2?u0

? [�? 1?
u0

]+

0

v?
2?/u20

e
? (v?�)2

2/u20 dv +
?
2?u0

? ?
�+ 1?

u0

v?
2?/u20

e
? (v?�)2

2/u20 dv

? 2
?
2?u0

? ?
�+ 1?

u0

v?
2?/u20

e
? (v?�)2

2/u2
0 dv

= 2
?
2?u0

(
Q(
?
u0)

(
�+

1?
u0

)
+

? ?
?
u0

Q(z)dz

)

c? 2
?
2?u0

((
�+

1?
u0

)
e?

u0
2 +

? ?
?
u0

e?
z2

2 dz

)

d? 2
?
2?e?

u0
2 u0

((
�+

1?
u0

)
+
?
2?

)

? O
(
u0e

?u0
2

)
(84)

where (a) results from the fact that g(�) ? 0, (b) is valid because I0(�z)e?�z ? 1 for � ? 0 and z ? 0,
and (c) and (d) follow from the fact that Q(z) , 1?

2?

??
z

e?t
2/2dt ? e?z2/2. Moreover, using (82), we can

write

g(�) = ?(�)
?
2?�u0I0(u20�2)e?u

2
0�

2

= ?(�)

(
1 +O

(
1

u20

))
. (85)

Also, using (82) and noting ?(v) ? O( 1
u?0
), we have

g(v) = ?(v)

?
v

�

(
1 +O

(
1

u20

))

? O(g?(v)) = O
(

?(v)

2
?
v�

+ ??(v)
?

v

�

)
? O(g?(�)) = O(?(�)). (86)

Using (83), (84), (85) and (86), we have

I = ?(�)

(
1 +O

(
1?
u0

))
+O

(
u0e

?u0
2

)
a
= ?(�)

(
1 +O

(
1?
u0

))
, (87)



17

where (a) follows from the fact that ?(�) = O
(

1
u?0

)
. Applying (87) in (79), we have

IN =

?
vN?1

...

?
v1

f(?vN?1)
(
1 +O

(
1?
u0

))
(1?Q(?vN?1u0))�

N?1?
i=1

u20vie
? (vi??vi?1)

2

2/u2
0

??u20vivi?1I0(?u20vivi?1)f(vi)dvi

=

?
vN?2

...

?
v1

f(?2vN?2)f(?vN?2)
(
1 +O

(
1?
u0

))2 (
1?Q(?2vN?2u0)

)�
(1?Q(?vN?2u0))

N?2?
i=1

u20vie
? (vi??vi?1)

2

2/u2
0

??u20vivi?1I0(?u20vivi?1)f(vi)dvi

= � � � =
N?
i=1

f(?iv0)

(
1 +O

(
1?
u0

))(
1?Q(?iv0u0)

) (88)
Substituting v0 = 1?

(1??2)/2 , we have

IN =

N?
i=1

f

( ?
2?i?

(1? ?2)

)(
1 +O

(
1?
u0

))(
1?Q

( ?
2?iu0?
(1? ?2)

))

=

N?
i=1

f

( ?
2?i?

(1? ?2)

)(
1 +O

(
1?
u0

))(
1? O

(
e?u

2
0

))
(89)

Using (80) and (89) and noting E0(?) = ? 1N log IN , we conclude Theorem 1.

V. APPENDIX B
E{log(??1max)} can be derived as follows

E{log(??1max)} =
K?
n=1

E{log(??1max)
?? |S| = n}Pr{|S| = n}. (90)

Since ?k, k = 1, � � � , K, are i.i.d. random variables with uniform distribution, we can write
F?max(? ||S| = n) = ?n

? E{log(??1max)
?? |S| = n} = ? 1

0

log(??1)n?n?1d?

=
1

n
, (91)

where FX(.) denotes the cumulative density function of the random variable X . Indeed, |S| is a binomial
random variable with parameters K and e??. (Since ?k = u20,k and u0,k has a Rayleigh distribution, we
have Pr(?k ? ?) = e??). Hence,

Pr{|S| = n} =
(
K

n

)
e?n?(1? e??)K?n. (92)

Substituting (91) and (92) in (90), we have

E{log(??1max)} =
K?
n=1

(
K

n

)
1

n
e?n?(1? e??)K?n. (93)



18

Let us define ?(K) ,
?K

n=1

(
K
n

)
1
n
e?n?(1? e??)K?n.

?(K) =
K?1?
n=0

(
K

n + 1

)
e?(n+1)?

n+ 1
(1? e??)K?n?1

= (1? e??)
K?2?
n=0

(
K ? 1
n + 1

)
e?(n+1)?

n + 1
(1? e??)K?n?2 +

e??
K?1?
n=0

(
K ? 1
n

)
1

n+ 1
e?n?(1? e??)K?n?1

= (1? e??)?(K ? 1) + 1
K
? (1? e

??)K

K
(94)

Solving the iteration considering ?(1) = e??, we derive ?(K) which is equal to E{log(??1max)}.

E{log(??1max)} =
K?
n=1

1

n
(1? e??)K?n ? (1? e??)K

K?
n=1

1

n
. (95)

For large values of K, we can approximate (95) as

E{log(??1max)} ? (1? e??)K
(? K

1

(1? e??)?xdx
x

?
? K
1

dx

x

)
a
?

1

?K log(1? e??)
(
1 +O

(
1

?K log(1? e??)
))

+(1? e??)K(?? logK)
?

1

Ke??

(
1 +O

(
1

Ke??

))
+ e?Ke

??
(?? logK). (96)

where (a) results from the following approximations [22]:? ?
??

e?t

t
dt ? e

?

?

(
1 +O

(
1

?

))
? i? ? ? 1 (97)? ?

??

e?t

t
dt ? log(?)? i? 0 < ? ? 1 (98)

REFERENCES
[1] P. Viswanath, D.N.C. Tse, R. Laroia, �Opportunistic beamforming using dumb antennas,� IEEE Trans. Inform. Theory, vol. 48, pp. 1277�

1294, June 2002.
[2] D. N. C. Tse and S. Hanly, �Multiaccess fading channels: Part i: Polymatroid structure, optimal resource allocation and throughput

capacities,,� IEEE Trans. Information Theory, vol. 44, p. 27962815, Nov. 1998.
[3] R. Knopp and P. A. Humblet, �Information capacity and power control in single-cell multiuser communications,� IEEE ICC�95, vol. 1,

pp. 331 � 335, June 1995.
[4] D. Tse, �Optimal power allocation over parallel gaussian channels,� in Proc. Int. Symp. on Inform. Theory, p. 27, June 1997.
[5] R. Agrawal, A. Bedekar, R. La, and V. Subramanian, �A class and channel condition based weighted proportionally fair scheduler,� in

Teletraffic Engineering in the Internet Era, Proc. Int. Teletraffic Congr., vol. ITC-17, p. 553565, Sept. 2001.
[6] X. Liu, E. K. P. Chong, and N. B. Shroff, �Opportunistic transmission scheduling with resource-sharing constraints in wireless networks,�

IEEE JSAC, vol. 19, pp. 2053�2064, Oct. 2001.
[7] Y. Liu and E. Knightly, �Opportunistic fair scheduling over multiple wireless channels,� in Proc. of 2003 IEEE INFOCOM, p. 11061115,

April 2003.
[8] S. Borst and P. Whiting, �Dynamic rate control algorithms for HDR throughput optimization,� in Proc. IEEE INFOCOM 2001, vol. 2,

pp. 976�985, April 2001.
[9] P. Bender, P. Black, M. Grob, R. Padovani, N. Sindhushayana, and A. Viterbi, �CDMA/HDR: A bandwidth efficient high-speed wireless

data service for nomadic users,� IEEE Communications Magazine, pp. 70�77, July 2000.
[10] CDMA 2000: High Rate Packet Data Air Interface Specification. Std. TIA/EIA IS-856, Nov. 2000.



19

[11] A. Jalali, R. Padovani, and R. Pankaj , �Data throughput of CDMA/HDR: A high efficiency, high data rate personal wireless system,�
in Proc. IEEE Vehicular Tech. Conference, vol. 3, pp. 1854�1858, May 2000.

[12] X. Qin and R. Berry , �Exploiting multiuser diversity for medium access control in wireless networks,� in Proc. IEEE, INFOCOM,
pp. 1084�1094, 2003.

[13] S. Shamai and E. Telatar, �Some information theoretic aspects of decentralized power control in multiple access fading channels,� in
Proc. Inform. Theory and Networking Workshop, 1999.

[14] N. Sharma and L. H. Ozarow , �A study of opportunism for multiple-antenna systems,� IEEE Trans. Inform. Theory, vol. 51, pp. 1804
� 1814, May 2005.

[15] M. Grossglauser and D. Tse, �Mobility increases the capacity of ad-hoc wireless networks,� in Proc. INFOCOM 2001, vol. 3, pp. 1360�
1369, 2001.

[16] A. N. Trofimov, �Convolutional codes for channels with fading,� in Proc. Inform Transmission, vol. 27, pp. 155�165, Oct. 1991.
[17] R. G. Galager, Information theory and Reliable communication. J. Wiley, New York, 1968.
[18] R. A. Berry and R. G. Gallager, �Communication over fading channels with delay constraints,� IEEE Trans. Inform. Theory, vol. 48,

pp. 1135 � 1149, May 2002.
[19] E. Telatar and R. G. Gallager , �Combining queueing theory with information theory for multiaccess,� IEEE J. Select. Areas Commun.,

vol. 13, pp. 963�969, May 1995.
[20] G. Kaplan and S. Shamai , �Achievable performance over the correlated rician channel,� IEEE Trans. Commun., vol. 42, pp. 2967 �

2978, Nov. 1994.
[21] M. Sharif and B. Hassibi, �On the capacity of mimo broadcast channels with partial side information,� IEEE Trans. Inform. Theory,

vol. 51, pp. 506 � 522, Feb. 2005.
[22] Y. L. Luke, The special functions and their approximations. New York, Academic Press, 1969.


	Introduction
	System model
	Throughput Analysis
	Strategy I: SNR-based scheduling with fixed codeword length
	Strategy II: SNR-based scheduling with adaptive codeword length
	Strategy III: Scheduling based on both SNR and channel correlation coefficient with adaptive codeword length 

	Conclusion
	Appendix B
	References

