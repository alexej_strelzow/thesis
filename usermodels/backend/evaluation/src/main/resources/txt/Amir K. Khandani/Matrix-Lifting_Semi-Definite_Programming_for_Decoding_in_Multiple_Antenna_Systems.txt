
ar
X

iv
:0

70
9.

16
74

v1
  [

cs
.IT

]  
11

 Se
p 2

00
7

1

Matrix-Lifting Semi-Definite Programming
for Decoding in Multiple Antenna Systems

Amin Mobasher and Amir K. Khandani

Abstract

This paper presents a computationally efficient decoder for multiple antenna systems. The
proposed algorithm can be used for any constellation (QAM or PSK) and any labeling method.
The decoder is based on matrix-lifting Semi-Definite Programming (SDP). The strength of
the proposed method lies in a new relaxation algorithm applied to the method of [1]. This
results in a reduction of the number of variables from (NK + 1)2 to (2N + K)2, where N is
the number of antennas and K is the number of constellation points in each real dimension.
Since the computational complexity of solving SDP is a polynomial function of the number of
variables, we have a significant complexity reduction. Moreover, the proposed method offers
a better performance as compared to the best quasi-maximum likelihood decoding methods
reported in the literature.

I. INTRODUCTION

The problem of Maximum Likelihood (ML) decoding in Multi-Input Multi-Output
(MIMO) wireless systems is known to be NP-hard. A variety of sub-optimum polynomial
time algorithms based on Semi-Definite Programming (SDP) are suggested for MIMO
decoding [1]�[9]. The first quasi ML decoding methods based on SDP were introduced
for PSK signalling [2]�[5], offering a near ML performance and a polynomial time worst

The authors are with Coding & Signal Transmission Laboratory (www.cst.uwaterloo.ca), Department of Electrical
and Computer Engineering, University of Waterloo, Waterloo, Ontario, Canada, N2L 3G1, e-mail: {amin, khan-
dani}@cst.uwaterloo.ca

This work is partly presented in the 10th Canadian Workshop on Information Theory (CWIT�07), Edmonton, AB,
June 2007.




2
case complexity. Subsequently, SDP methods were used for decoding of MIMO systems
based on QAM constellation [1], [6].

The method presented in [6] is for MIMO systems using 16-QAM, where the
structure of constellation is captured by a polynomial constraint. Then, by introducing
some slack variables, the constraints are expressed in terms of quadratic polynomials.
This method can be generalized for larger constellations at the cost of defining more
slack variables, increasing the complexity, and significantly decreasing the performance.
The method proposed in [7] is a further relaxation of [6], only utilizing upper and lower
bounds on the symbol energy in the relaxation step. There is a very slight degradation in
performance compared to [6]; however, its computational complexity is independent of
the constellation size for any uniform QAM (order of complexity is cubic). The method
in [8] is a further tightening of [7] by appending some inequality conditions that are
implicit in the alphabet constraint. Its computational complexity is still less than [6].

In [1], an efficient approximate ML decoder for MIMO systems is developed based
on vector lifting SDP. The transmitted vector is expanded as a linear combination (with
zero-one coefficients) of all the possible constellation points in each dimension. Using
this formulation, the distance minimization in Euclidean space is expressed in terms
of a binary quadratic minimization problem. The minimization of this problem is over
the set of all binary rank-one matrices with column sums equal to one. Although the
algorithm in [1] is a sub-optimal decoding method, it is shown that by adding several
extra constraints, it can approach the ML performance. However, implementing the extra
constraints increases the computational complexity.

In this paper, we introduce a new algorithm based on matrix-lifting SDP [10], [11]
for any constellation (QAM or PSK) and any labeling method. This algorithm is inspired
by the method in [1] with an efficient implementation resulting in a better performance
and lower computational complexity. In SDP optimization problems, the computational
complexity is a polynomial function of the number of variables. Using the proposed
method, the number of variables in [1] is decreased from (NK + 1)2 to (2N + K)2,
where N is the number of antennas and K is the number of constellation points in each
real dimension. In addition to this large reduction in the complexity, simulation results



3
show that the proposed algorithm also outperforms all other known convex quasi-ML
decoding methods, e.g. [6]�[8].

Following notations are used in the sequel. The space of N �K (resp. N �N) real
matrices is denoted by MN�K (resp. MN ), and the space of N �N symmetric matrices
is denoted by SN . For a N �K matrix X ?MN�K , the (i, j)th element is represented
by xij , where 1 ? i ? N, 1 ? j ? K, i.e. X = [xij ]. We use trace(A) to denote
the trace of a square matrix A. The space of symmetric matrices is considered with the
trace inner product ?A,B? = trace(AB). For A,B ? SN , A ? 0 (resp. A ? 0) denotes
positive semi-definiteness (resp. positive definiteness), and A ? B denotes A?B ? 0.
For two matrices A,B ?MN , A ? B, (A > B) means aij ? bij , (aij > bij) for all i, j.
The Kronecker product of two matrices A and B is denoted by A?B. For X ?MN�K ,
vec(X) denotes the vector in RNK (real NK-dimensional space) that is formed from the
columns of the matrix X. For X ? MN , diag(X) is a vector of the diagonal elements
of X. We use eN ? RN (resp. 0N ? RN ) to denote the N �1 vector of all ones (resp. all
zeros), EN�K ? MN�K to denote the matrix of all ones, and IN to denote the N �N
Identity matrix. For X ?MN�K , the notation X(1 : i, 1 : j), i < K and j < N denotes
the sub-matrix of X containing the first i rows and the first j columns.

The rest of the paper is organized as follows. The problem formulation is introduced
in Section II. Section III is the review of the vector-lifting semi-definite programming
presented in [1]. In Section IV, we propose our new algorithm based on matrix-lifting
semi-definite programming. We use the geometry of the relaxation to find a projected
relaxation which has a better performance. In Section V, we present an optimization
method, based on matrix nearness to find the integer solution of the original decoding
problem from the relaxed optimization problem. Finally, Section VI conclude the paper
with some simulation results.

II. PROBLEM FORMULATION

A MIMO system with M� transmit antennas and N� receive antennas can be modeled
by

y = Hx + n, (1)



4
where M = 2M� , N = 2N� , y is the M � 1 received vector, H is M � N real channel
matrix, n is N�1 additive white Gaussian noise vector, and x is N�1 data vector whose
components are selected from the set {s1, � � � , sK}, see [1]. Noting xi ? {s1, � � � , sK},
for i = 1, � � � , N , we have

xi = ui,1s1 + ui,2s2 + � � �+ ui,KsK , (2)
where

ui,j ? {0, 1} and

K?
j=1

ui,j = 1, ? i = 1, � � � , N. (3)

Let

U =

?
??????

u1,1 � � � u1,K

u2,1 � � � u2,K
.

.

.

.
.
.

.

.

.

uN,1 � � � uN,K

?
?????? and s =

?
???

s1
.

.

.

sK

?
??? .

Therefore, the transmitted vector is x = Us where UeK = eN .
At the receiver, the ML decoding is given by

x� = arg min
xi?{s1,��� ,sK}

?y�?Hx?2, (4)
where x� is the most likely input vector and y� is the received vector. Noting x = Us,
this problem is equivalent to

min
UeK=eN

?y� ?HUs?2 ?

min
UeK=eN

sTUT HTHUs? 2y�THUs. (5)
Therefore, the decoding problem can be formulated as

min sTUTHTHUs? 2y�THUs

s.t. UeK = eN

ui,j ? {0, 1}. (6)
Let Q = HTH, S = ssT , C = ?sy�TH, and let EN�K denote the set of all binary

matrices in MN�K with row sums equal to one, i.e.

EN�K ={U?MN�K : UeK = eN , uij ? {0, 1}} . (7)



5
Therefore, the minimization problem (6) is

min trace
(
SUTQU + 2CU

)
s.t. U ? EN�K (8)

III. VECTOR-LIFTING SEMI-DEFINITE PROGRAMMING

In order to solve the optimization problem (8), the authors in [1] proposed a
quadratic vector optimization solution by defining u = vec(UT ),U ? EN�K . By using
this notation, the objective function is replaced by uT (Q?S)u + 2vec(C)Tu. Then, the
quadratic form is linearized using the vector

[
1

u

]
, i.e.

Zu =

[
1

u

] [
1 uT

]

=

[
1 uT

u uuT

]
=

[
1 uT

u X

]
, (9)

where X = uuT and it is relaxed to X ? uuT , or equivalently, by the Schur complement,

to the lifted constraint
[

1 uT

u X

]
? 0. Note that this matrix is selected from the set

F := conv
{
Zu : u = vec(U

T ), U ? EN�K
}

, (10)

where conv(.) denotes the convex hull of a set. Therefore, the decoding problem using
vector lifting semi-definite programming can be represented by

trace

[
0 vec(C)T

vec(C) Q? S

][
1 uT

u X

]

s.t.

[
1 uT

u X

]
? F , (11)

which can be solved by SDP technique.
Note that in (11), the optimization parameter is a matrix in SNK+1, which has

(NK + 1)2 variables. In the following, we reduce the number of optimization variables
by exploiting the matrix structure of U.



6
IV. MATRIX-LIFTING SEMI-DEFINITE PROGRAMMING

To keep the matrix U in its original form in (8), the idea is to use the constraint
X = UTU instead of X = uuT . As a result, the relaxation X ? uuT is transformed to

X ? UT U, or equivalently, by the Schur complement,
[

IN U

UT X

]
? 0. This is known

as matrix-lifting semi-definite programming. Define the new variable V = US. Since the
matrix S is symmetric, the objective function in (8) can be represented as the Quadratic
Matrix Program [11]

trace

([
UT VT

] [ 0 1
2
Q

1
2
Q 0

][
U

V

]
+ 2CU

)

= trace

([
0 1

2
Q

1
2
Q 0

][
U

V

] [
UT VT

]
+ 2CU

)

= trace (LQWU) , (12)

where

L =

?
???

0 C 0

CT 0 1
2
Q

0 1
2
Q 0

?
??? (13)

and

WU =

?
???

I UT VT

U UUT UVT

V VUT VVT

?
??? . (14)

To linearize WU, we consider the matrix[
U

V

] [
UT VT

]
=

[
X Y

Y Z

]
, (15)

where X,Y,Z ? SN . This equality can be relaxed to[
UUT UVT

VUT VVT

]
?

[
X Y

Y Z

]
? 0. (16)



7
It can be shown that this relaxation is convex in the Lo�wner partial order and it is
equivalent to the linear constraint [10]

W ,

?
???

I UT VT

U X Y

V Y Z

?
??? ? 0. (17)

On the other hand, the feasible set in (8) is the set of binary matrices in MN�K with row
sum equal to one, the set EN�K in (7). By relaxing the rank-one constraint for the matrix
variable in (12), we have a tractable SDP problem. The feasible set for the objective
function in (12) is approximated by

FM = conv {WU | U ?MN�K : UeK = eN ,

uij ? {0, 1}, ?i, j;V = US} (18)
Therefore, the decoding problem can be represented by

min trace (LW)

s.t. W ? FM. (19)
Note that the size of matrix W is (2N+K)�(2N+K), compared to (NK+1)�(NK+1)
in [1]. In SDP optimization problems, the computational complexity is a polynomial
function of the number of variables (elements of W). By the new implementation of
(19), the number of variables in [1] is decreased from (NK +1)2 to (2N +K)2, resulting
in a large reduction in the complexity.

Although the rank constraint in (15) is relaxed, we can still consider some additional
linear constraints to further improve the quality of the solution. These constraints are
valid for the non-convex rank-constrained decoding problem. However, we force the SDP
problem to satisfy these constraints. Consider the auxiliary matrix V and the symmetric
matrices X,Y and Z in matrix W. Since U ? EN�K and

?N
j=1 u

2
ij = 1, it is clear that

diag(X) = eN . Also, Y represents USUT and Z represents US2UT . It is easy to show
that

diag(Y) = Udiag(S) and diag(Z) = Udiag(S2). (20)



8
Moreover, S = ssT (rank-one matrix) and S2 = (?K1=i s2i )S. Therefore, instead of
diag(Z) = Udiag(S2), we have a stronger result for Z, i.e. Z = (

?K
1=i s

2
i )Y. Therefore,

we have

min trace

?
???L

?
???

I UT VT

U X Y

V Y Z

?
???
?
???

s.t. UeK = eN ; U ? 0

V = US

diag(X) = eN

diag(Y) = Udiag(S)

Z = (
K?

1=i

s2i )Y?
???

I UT VT

U X Y

V Y Z

?
??? ? 0

U,V ?MN�K ,X,Y,Z ? SN (21)

The equation in (20) determines the diagonal elements of Y. This property is hidden
in the special structure of U, i.e. U ? EN�K . By using this property, we can even add
more constraints. The equation Y = USUT implies that Yij = Skl for some k and l.
Therefore, the value of Yij is between the minimum and the maximum elements of S.
In addition, it can be easily shown that in communication applications, S, Y, and Z are
diagonal dominant matrices (since sTeK = 0). This property can be also used to add more
constraints to improve the quality of the solution. Our studies show that the improvements
due to including the above constraints are marginal. Therefore, in the sequel, we focus on
the form given in (21) with the following consideration. The objective function in (8) is
trace

(
SUTQU + 2CU

)
which is equivalent to trace

(
QUSUT + 2UC

)
. Exchanging

the role of Q and S results in two different formulations. Here, the auxiliary variable V is
defined as QU. Similarly, the auxiliary variables X,Y, and Z represents UT U,UTQU,
and UTQ2U, respectively. Therefore, it is easy to show that the equivalent minimization



9
problem is

min trace

?
???
?
???

0 C 0

CT 0 1
2
S

0 1
2
S 0

?
???
?
???

I U V

UT X Y

VT Y Z

?
???
?
???

s.t. UeK = eN ; U ? 0

V = QU

diag(X) = UTeN ; Xij = 0 i 6= j

YeK = U
TQeN ; trace(YEK) = trace(QEN)

ZeK = U
TQ2eN ; trace(ZEK) = trace(Q

2EN)?
???

I U V

UT X Y

VT Y Z

?
??? ? 0

U,V ?MN�K ,X,Y,Z ? S
K , (22)

where the size of the variable matrix is (2K + N). Note that both (21) and (22) are
equivalent, however, depending on the structure of the system (values of N and K), we
can use the one which offers a smaller number of variables. In the following, we focus
on (21), which is a better choice for N ? K.

A. Geometry of the Relaxation
In this section, we eliminate the constraints defining UeK = eN by providing a

tractable representation of the linear manifold spanned by this constraint. This method
is called gradient projection or reduced gradient method [12]. The following lemma is
on the representation of matrices having sum of the elements in each row equal to one.
This lemma is used in our reduced gradient method.

Lemma 1: [1] Let G =
[

IK?1 ?eK?1

]
?M(K?1)�K and F = 1K

(
EN�K ? EN�(K?1)G

)
?

MN�K . A matrix U ? MN�K with the property that the summation of its elements in
each row is equal to one, i.e. UeK = eN , can be written as

U = F + U�G, (23)



10

where U� = U(1 : N, 1 : (K? 1)).
Corollary 1: ?U ? EN�K , ?U� ?MN�(K?1), u�ij ? {0, 1} s.t. U = F+U�G, where

U� = U(1 : N, 1 : (K? 1)). Note that the summation of each row of U� is 0 or 1.
Consider the minimization problem (8). By substituting (23), the objective function

is

trace
(
SUT QU + 2CU

)
= trace

(
S(F + U�G)

T
Q(F + U�G) + 2C(F + U�G)

)
= trace

(
GSGT U�TQU� + GSFTQU� + QFSGTU�T

+GCU� + CTGTU�T + 2CF + SFTQF
)

= trace
(
L�WU� + 2CF + SF

TQF
)

, (24)

where

L� =

?
???

0 GSFTQ + GC 0

QFSGT + CTGT 0 1
2
Q

0 1
2
Q 0

?
??? ,

WU� =

?
???

I U�T V�T

U� U�U�T U�V�T

V� V�U�T V�V�T

?
??? ,

V� = U�GSGT. (25)

Therefore, (6) can be written as

min trace
(
L�WU�

)
s.t. U� = U(1 : N, 1 : (K? 1)); U ? EN�K

V� = U�
(
GSGT

) (26)
Using a similar procedure, we can show that (26) is equivalent to the following reduced
matrix-lifting semi-definite programming problem:



11

min trace

?
???L�

?
???

I U�T V�T

U� X� Y�

V� Y� Z�

?
???
?
???

s.t. U�eK?1 ? eN ; U� ? 0

V� = U�
(
GSGT

)
diag(X�) = U�eK?1

diag(Y�) = U�diag
(
GSGT

)
Z� =

(
K?1?
1=i

(si ? sK)
2

)
Y�

?
???

I U�T V�T

U� X� Y�

V� Y� Z�

?
??? ? 0

U�, V� ?MN�(K?1), X�, Y�, Z� ? SN (27)

Note that this method can also be applied to the equivalent formulation in (22).

B. Solving the SDP Problem

The relaxed decoding problems can be solved using Interior-Point Methods (IPMs),
which are the most common methods for solving SDP problems of moderate sizes with
polynomial computational complexities [13]. There are a large number of IPM-based
solvers to handle SDP problems, e.g., DSDP [14], SeDuMi [15], SDPA [16], etc. In our
numerical experiments, we use SDPA solver.

In the matrix-lifting SDP optimization problem (21), the rank-constrained matrix
Wu is relaxed to the positive semi-definite matrix W. Utilizing the rank-constrained
property of the variable parameter, the relaxed problem (21) can be solved using a non-
linear method, known as the augmented Lagrangian algorithm. This approach can be
used for large problem sizes and the complexity can be significantly reduced, while the
performance degradation is negligible [17].



12

V. INTEGER SOLUTION - MATRIX NEARNESS PROBLEM

Solving the relaxed decoding problems results in the solution U�. In general, this
matrix is not in EN�K . The condition UeK = eN is satisfied. However, the elements
are between 0 and 1. This matrix has to be converted to a 0-1 matrix by finding a
matrix in EN�K which is nearest to this matrix. Matrix approximation problems typically
measure the distance between matrices with a norm. The Frobenius and spectral norms
are common choices as they are analytically tractable.

To find the nearest solution in EN�K to U�, the solution of the relaxed problem, we
solve

min
U? EN�K

?U? U�?2
F
, (28)

where ?A?2
F

is the Frobenius norm of the matrix A which is defined as ?A?2
F

=

trace(AAT ), and

?U? U�?2
F

= trace
(
(U? U�)(U? U�)T

)
= N ? 2trace(U�UT) + trace(U�U�T). (29)

The last equality is due to the fact that for any U ? EN�K , we have diag(UUT ) = eN , see
(21). Therefore, after removing the constants, finding the integer solution is the solution
of the following problem:

max
U? EN�K

trace(U�UT) (30)

Consider the maximization problem

max trace(U�UT)

s.t. UeK = eN

0 ? U ? 1, (31)

where ? in the last constraint is element-wise. This problem is a linear programming
problem with linear constraints and the optimum solution is a corner point meaning that
the constraints are satisfied with equality at the optimum point. In other words, at the
optimum point, U ? EN�K . Therefore, to find the solution for (30), we can simply solve



13

the linear problem (31), which is strongly polynomial time. To improve this result, the
randomization algorithms, introduced in [1], can be further applied.

VI. SIMULATION RESULTS

We simulate the proposed matrix lifting method (27) for system with 4 transmit and
4 receive antennas employing 16-QAM. Fig. 1 shows the performance of the proposed
method vs. the performance of the vector lifting method in [1] and the previous known
methods in [6]�[8]. As it can be seen, the proposed method outperforms all other convex
sub-optimal methods.

0 5 10 15
10?3

10?2

10?1

100

Eb/N0

SE
R

4�4 Multiple Antenna Sysmtems with 16QAM Modulation

 

 

Method proposed in [7]
Method proposed in [6]
Method proposed in [8]
Method proposed in [1]
Matrix Lifting SDP Method

Fig. 1. Performance of the proposed matrix lifting SDP method in a MIMO system with 4 transmit and 4 receive
antennas employing 16-QAM

The worst case complexity of the proposed method solved by IPMs is a polynomial
function of the number of antennas (similar to the analysis in [1]). In the optimization
problem of (21), where N ? K, the dimension of the matrix variable W is m = O(K)
and the number of constraints is p = O(K2). Similar to [1], it can be easily seen that
a solution to (27) can be found in at most O(K5.5) arithmetic operations (utilizing the
sparsity of the rank-one constraint matrices), where the computational complexity of
[1], [6], [8], [7] are O(N5.5K5.5), O(N6.5K6.5), O(K2N4.5 + K3N3.5), and O(N3.5)



14

respectively1. Note that for the equivalent optimization problem (22), where K ? N ,
the computational complexity is at most O(N5.5). It must be emphasized that depending
on values of N and K, we can implement the optimization problem (21) or (22) which
results in less computational complexity.

Note that many of the constraints have very simple structures. This property can be
used to develop an interior-point optimization algorithm fully exploiting the constraint
structures of the problem, thereby getting complexity order better than that of using a
general purpose solver such as SeDuMi or SDPA. Moreover, we can further reduce the
complexity of the proposed method by implementing the augmented Lagrangian method
[17].

ACKNOWLEDGEMENT

The authors would like to thank R. Sotirov, H. Wolkowicz and Y. Ding for many
helpful discussions and comments. The authors would also like to thank T. Davidson,
M. Kisialiou, W. Ma and N. Sidiropoulos for their comments on an earlier draft of this
work.

REFERENCES

[1] A. Mobasher, M. Taherzadeh, R. Sotirov, and A. K. Khandani, �A near maximum likelihood decoding algorithm
for mimo systems based on semi-definite programming,� to appear in IEEE Trans. on Info. Theory, Nov. 2007.

[2] B. Steingrimsson, T. Luo, and K. M. Wong, �Soft quasi-maximum-likelihood detection for multiple-antenna
wireless channels,� IEEE Transactions on Signal Processing, vol. 51, no. 11, pp. 2710� 2719, Nov. 2003.

[3] B. Steingrimsson, Z. Q. Luo, and K. M. Wong;, �Quasi-ML detectors with soft output and low complexity for PSK
modulated MIMO channels,� in 4th IEEE Workshop on Signal Processing Advances in Wireless Communications
(SPAWC 2003), 15-18 June 2003, pp. 427 � 431.

[4] Z. Q. Luo, X. Luo, and M. Kisialiou, �An Efficient Quasi-Maximum Likelihood Decoder for PSK Signals,� in
ICASSP �03, 2003.

[5] W.-K. Ma, P. C. Ching, and Z. Ding, �Semidefinite relaxation based multiuser detection for m-ary psk multiuser
systems,� IEEE Trans. on Signal Processing, 2004.

[6] A. Wiesel, Y. C. Eldar, and S. Shamai, �Semidefinite relaxation for detection of 16-QAM signaling in mimo
channels,� IEEE Signal Processing Letters, vol. 12, no. 9, Sept. 2005.

1Due to space limit, we refer the reader to [1] for a comparison on the execution time of different methods.



15

[7] N. D. Sidiropoulos and Z.-Q. Luo, �A semidefinite relaxation approach to mimo detection for high-order qam
constellations,� IEEE SIGNAL PROCESSING LETTERS, vol. 13, no. 9, Sept. 2006.

[8] Y. Yang, C. Zhao, P. Zhou, and W. Xu, �Mimo detection of 16-qam signaling based on semidefinite relaxation,�
to appear in IEEE SIGNAL PROCESSING LETTERS, 2007.

[9] A. Mobasher, M. Taherzadeh, R. Sotirov, and A. K. Khandani, �A Near Maximum Likelihood Decoding
Algorithm for MIMO Systems Based on Graph Partitioning,� in IEEE International Symposium on Information
Theory, Sept. 4-9 2005.

[10] Y. Ding and H. Wolkowicz, �A Matrix-lifting Semidefinite Relaxation for the Quadratic Assignment Problem,�
Department of Combinatorics & Optimization, University of Waterloo, Tech. Rep. CORR 06-22, 2006.

[11] A. Beck, �Quadratic matrix programming,� SIAM Journal on Optimization, vol. 17, no. 4, pp. 1224�1238, 2007.
[12] S. Hadley, F. Rendl, and H. Wolkowicz, �A new lower bound via projection for the quadratic assignment problem,�

Math. Oper. Res., vol. 17, no. 3, pp. 727�739, 1992.
[13] F. Alizadeh, J.-P. A. Haeberly, and M. Overton, �Primal-dual interior-point methods for semidefinite programming:

Stability, convergence, and numerical results,� SIAM Journal on Optimization, vol. 8, no. 3, pp. 746�768, 1998.
[14] S. J. Benson and Y. Ye, �DSDP5 User Guide The Dual-Scaling Algorithm for Semidefinite Programming,�

Mathematics and Computer Science Division, Argonne National Laboratory, Argonne, IL, Tech. Rep. ANL/MCS-
TM-255, 2004, available via the WWW site at http://www.mcs.anl.gov/?benson.

[15] J. Sturm, �Using sedumi 1.02, a matlab toolbox for optimization over symmetric cones,� Optimization Methods
and Software, vol. 11-12, pp. 625�653, 1999.

[16] M. Kojima, K. Fujisawa, K. Nakata, and M. Yamashita, �SDPA(SemiDefinite Programming Algorithm) Users
Mannual Version 6.00,� Dept. of Mathematical and Computing Sciences, Tokyo Institute of Technology, 2-
12-1 Oh-Okayama, Meguro-ku, Tokyo, 152-0033, Japan, Tech. Rep., 2002, available via the WWW site at
http://sdpa.is.titech.ac.jp/SDPA/.

[17] A. Mobasher and A. K. Khandani, �Matrix-Lifting SDP for Decoding in Multiple Antenna Systems,� Department
of E&CE, University of Waterloo, Tech. Rep. UW-E&CE 2007-06, 2007, available via the WWW site at
http://www.cst.uwaterloo.ca/?amin.





	Introduction
	Problem Formulation
	Vector-Lifting Semi-Definite Programming
	Matrix-Lifting Semi-Definite Programming
	Geometry of the Relaxation
	Solving the SDP Problem

	Integer Solution - Matrix Nearness Problem
	Simulation Results
	References

