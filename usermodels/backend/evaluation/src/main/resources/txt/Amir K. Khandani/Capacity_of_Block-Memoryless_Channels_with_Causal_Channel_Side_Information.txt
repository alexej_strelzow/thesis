
ar
X

iv
:0

80
6.

10
62

v1
  [

cs
.IT

]  
5 J

un
 20

08

1

Capacity of Block-Memoryless Channels

with Causal Channel Side Information
Hamid Farmanbar and Amir K. Khandani

Coding and Signal Transmission Laboratory
Department of Electrical and Computer Engineering

University of Waterloo
Waterloo, Ontario, N2L 3G1

Email: {hamid,khandani}@cst.uwaterloo.ca

Abstract

The capacity of a time-varying block-memoryless channel in which the transmitter and

the receiver have access to (possibly different) noisy causal channel side information (CSI) is
obtained. It is shown that the capacity formula obtained in this correspondence reduces to the

capacity formula reported in [1] for the special case where the transmitter CSI is a deterministic
function of the receiver CSI.

Index Terms

Channel capacity, block-memoryless channel, time-varying channel, causal side informa-

tion.

I. INTRODUCTION

Motivated by the result reported in [1], this correspondence studies the capacity
of a stationary and ergodic time-varying block-memoryless (BM) channel where the
transmitter and the receiver have access to noisy causal channel side information (CSI).




2
The CSI at the transmitter (CSIT) and the CSI at the receiver (CSIR) can be different.
The time variations of the channel are modeled as a set of channel states where the

channel is at some state at each time instant. A time-varying (state-dependent) BM
channel is memoryless between blocks, however, within each block the state and the

channel conditioned on the state can have memory. For example, such a channel model

applies to systems based on frequency hopping with slow mobility. This results in a

quasi-static (block) fading channel model where the channel fading is static within a
block and changes independently between blocks as the frequency hops to a different

carrier. A formal definition of a state-dependent BM channel is given in Section II.

The capacity of time-varying BM channels with CSI at transmitter and receiver

has been studied in [1] and the capacity is obtained for the case that the CSIT is a
deterministic function of the CSIR. As an example, the scenario where the CSIT is a

deterministic function of the CSIR occurs when the receiver quantizes its observation of

the channel state and transmits it via a noiseless channel to the transmitter. However,

when the feedback channel is noisy, the CSIT will no longer be a deterministic function

of the CSIR. In this correspondence, we obtain the capacity for such a general case.

The key idea comes from the capacity results due to Shannon for state-dependent

discrete memoryless channels with causal side information at the transmitter [2]. In
the model considered by Shannon, the state of the channel is perfectly known at the

transmitter and unknown at the receiver. Shannon�s work was extended by Salehi [3] to
the case that (possibly different) noisy versions of the CSI are available at the transmitter
and at the receiver. It was later shown by Caire and Shamai [4] that the capacity with
noisy CSI can be obtained from Shannon�s original work by considering a new state-

dependent channel with CSIT alphabet as the new state alphabet. It is worth mentioning

that in our problem, since CSIT symbols are not available up to the end of the current

block, applying Shannon�s results [2] to super symbols corresponding to blocks would
not yield the capacity.

We will use the following notations throughout the correspondence. Random vari-



3
ables are denoted by upper case letters (X) and their values are denoted by lower case
letters (x). The sequence of random variables Xm, . . . , Xn is denoted by Xnm; and xnm
denotes a particular realization of Xnm. The sequences Xn1 and xn1 are denoted by Xn and

xn, respectively. Sets are denoted by calligraphic letters (X ); |X | denotes the cardinality
of X , and X n = X � � � � � X? ?? ?

n

is the n-th Cartesian power of X .

II. CHANNEL MODEL

The channel model considered in this correspondence is the same as the one intro-

duced in [1] where a state-dependent block-memoryless channel is defined by a finite
channel input alphabet X , a finite channel output alphabet Y , a finite state alphabet S, and

transition probabilities p(yn0|xn0 , sn0) where n0 is the channel block length. We denote

the CSIT and the CSIR by U ? U and V ? V , respectively. The CSIT and the CSIR are

dependent on the state according to the joint distribution p(sn0, un0, vn0).
It is convenient to express the transition probabilities of the channel in terms of the

CSIT and the CSIR as

p(yn0, vn0|xn0 , un0) =
?
sn0

p(yn0, vn0|xn0 , un0, sn0)p(sn0|xn0 , un0)

=
?
sn0

p(yn0|xn0 , un0, sn0, vn0)p(vn0|xn0 , un0, sn0)p(sn0|xn0 , un0)

=
?
sn0

p(yn0|xn0 , sn0)p(vn0|un0, sn0)p(sn0 |un0)

=
?
sn0

p(yn0|xn0 , sn0)p(sn0 , un0, vn0)/p(un0), (1)

where p(un0) =
?

sn0 ,vn0 p(s
n0 , un0, vn0).

For n = Jn0 uses of the channel, we have

p(yn, vn|xn, un) =
J?1?
j=0

p
(
y

(j+1)n0
jn0+1

, v
(j+1)n0
jn0+1

|x
(j+1)n0
jn0+1

, u
(j+1)n0
jn0+1

)
, (2)

and

p(sn, un, vn) =

J?1?
j=0

p
(
s
(j+1)n0
jn0+1

, u
(j+1)n0
jn0+1

, v
(j+1)n0
jn0+1

)
. (3)



4
We define a (2nR, n) block code of length n for the state-dependent BM channel to be

2nR sequences of n encoding functions fi : W � Un ? X for i = 1, . . . , n such that

xi = fi(w, u
i
1), where w ? W = {1, . . . , 2nR}. Note that the channel input at time i

depends on the CSIT up to time i. In other words, we consider causal knowledge setting.

At the receiver, a decoding function g : Yn�Vn ?W is used to decode the transmitted

message as w� = g(yn1 , vn1 ). The rate of the block code is R = 1n log |W|, and P
(n)
e is

defined as the probability that a message W , uniformly distributed over W , is received

in error, i.e.,

P (n)e = Pr{W� 6= W}. (4)

III. CAPACITY OF BLOCK-MEMORYLESS CHANNELS WITH CSI

The capacity of a time-varying BM channel for the case that the CSIT, Un0 , is a

deterministic function of the CSIR, V n0 , is given by [1]

C = max
p(xn0 |un0)

1

n0
I(Xn0;Y n0|V n0)

=
?
un0

p(un0) max
p(xn0 |un0)

1

n0
I(Xn0;Y n0 |un0, V n0) (5)

where the maximum is taken over all distributions satisfying the causal side information

constraint, i.e.,

p(xn0 |un0) =

n0?
i=1

p(xi|x
i?1, ui). (6)

The capacity is achieved by a scheme that adapts itself to channel variations so that for

every realization of the CSIT, the encoder uses a code which is capacity-achieving for

that specific realization. The final coding scheme will be simply a multiplexed version

of the coding schemes for all possible CSIT realizations.

The scenario in which the CSIT is a function of the CSIR describes a situation where

the CSIT is, for example, a quantized version of the CSIR due to rate restrictions on the

capacity of the feedback link between the receiver and the transmitter. However, when the

feedback channel introduces noise, the CSIT will no longer be a deterministic function



5
of the CSIR. In this case, the decoder will no longer know the transmission strategy

and this complicates capacity analysis. In the following, we show that the Shannon�s

approach for state-dependent discrete memoryless channels with causal side information

at the transmitter, with some modifications, can be used to obtain the capacity in this

more general case. It should be notes that applying Shannon�s scheme to our channel

with super symbols of size n0 does not yield the capacity since CSIT is available only

up to the current symbol, not up to the end of the current channel block (super symbol).
We will show that to achieve the capacity, it is sufficient to consider encoding schemes

that use the CSIT up to the current symbol and within the current super symbol. In other

words, there is no loss in capacity by disregarding the past CSIT symbols that are not

within the current super symbol.

Theorem 1: The capacity of a time-varying BM channel with the CSIT and the

CSIR denoted by Un0 and V n0 , respectively, is equal to

C = max
p(tn0 )

1

n0
I(T n0;Y n0 |V n0), (7)

where the equivalent channel from T n0 to (Y n0, V n0) is defined by1

p(yn0, vn0|tn0) =
?
un0

p(un0)p
(
yn0, vn0|xi = ti(u

i)|n0i=1, u
n0
)
. (8)

Proof:
Achievability: Consider the following encoding scheme. A message w ? {1, . . . , 2nR}

is encoded to
(
tn01 (w), t

2n0
n0+1

(w), . . . , tn(J?1)n0+1(w)
)

, where tjn0+i ? X |U|
i is a function

from U i to X 2, j = 0, 1, . . . , J ? 1, i = 1, 2, . . . , n0. Then, for any CSIT sequence un1 ,

the channel input sequence xn1 is given by xjn0+i = tjn0+i
(
ujn0+ijn0+1

)
, j = 0, 1, . . . , J ? 1,

i = 1, 2, . . . , n0. The new channel from T n0 to (Y n0, V n0) defined by (8) is not state
1Theorem 1 may equaivalently be stated as follows. The capacity is given by (7) in which the maximization is

restricted to distributions satisfying p(tn0 , un0 , vn0 , xn0 , yn0) = p(tn0)p(un0)p(xn0 |tn0 , un0)p(yn0 , vn0 |xn0 , un0)

and xi = ti(ui), i = 1, . . . , n0. I.e., T n0 is independent of Un0 and p(xn0 |tn0 , un0) takes values zero and one only.
2There is a one-to-one correspondence between the elements of U i and the elements of

?
1, 2, . . . , |U|i

�
. A function

from U i to X can be represented by a |U|i-tuple composed of elements of X . Each component of the |U|i-tuple

represents the value of the function for a specific element of U i.



6
dependent and for which the rate 1
n0
I(T n0 ;Y n0 , V n0) is achievable for a fixed p(tn0).

However, we have

I(T n0;Y n0 , V n0) = I(T n0 ;V n0) + I(T n0 ;Y n0|V n0)

= I(T n0 ;Y n0|V n0), (9)

since T n0 is independent of V n0 . Hence, the rate C given in (7) is achievable.
Converse: For any (2nR, n) code for the state-dependent BM channel with arbitrary

small probability of error, we have

nR = H(W ) (10)
= I(W ;Y n, V n) + H(W |Y n, V n) (11)
? I(W ;Y n, V n) + n?n (12)

=
J?1?
j=0

I
(
W ;Y

(j+1)n0
jn0+1

, V
(j+1)n0
jn0+1

|Y jn01 , V
jn0
1

)
+ n?n (13)

?
J?1?
j=0

I
(
W,Y jn01 , V

jn0
1 ;Y

(j+1)n0
jn0+1

, V
(j+1)n0
jn0+1

)
+ n?n (14)

?
J?1?
j=0

I
(
W,U jn01 ;Y

(j+1)n0
jn0+1

, V
(j+1)n0
jn0+1

)
+ n?n (15)

=
J?1?
j=0

I
(
W,U jn01 ;Y

(j+1)n0
jn0+1

|V
(j+1)n0
jn0+1

)
+ n?n (16)

=
J?1?
j=0

I
(
Tj ;Y

(j+1)n0
jn0+1

|V
(j+1)n0
jn0+1

)
+ n?n (17)

? nC + n?n, (18)

where ?n = 1n +P
(n)
e R? 0 for large n; (12) follows from Fano�s inequality; (15) follows

from the data processing inequality for the Markov chain (W,Y jn01 , V
jn0
1 ) ? (W,U

jn0
1 ) ?

(Y
(j+1)n0
jn0+1

, V
(j+1)n0
jn0+1

); (16) follows since (W,U jn01 ) is independent of V (j+1)n0jn0+1 ; Tj =
(W,U jn01 ); and (18) follows by comparing I

(
Tj;Y

(j+1)n0
jn0+1

|V
(j+1)n0
jn0+1

)
with (7) and noting



7
that Tj is independent of U (j+1)n0jn0+1 and Xjn0+i = fjn0+i
(
Tj , U

jn0+i
jn0+1

)
, for j = 0, . . . , J?1,

i = 1, . . . , n0.

In the sequel, we show that the capacity formula (7), reduces to (5) when Un0 is
a deterministic function of V n0 , i.e., Un0 = k(V n0). Any distribution p(tn0) induces a

distribution p(xn0 |un0) according to

p(xn0 |un0) =
?

tn0 :tn0 (un0 )=xn0

p(tn0), ?xn0 ? X n0, ?un0 ? Un0 , (19)

where tn0(un0) = xn0 implies xi = ti(ui), i = 1, . . . , n0. On the other hand, for any

distribution p(xn0 |un0), there is a corresponding distribution p(tn0) which can be obtained

by solving (19). Given a realization of the CSIT, un0 , we have the Markov chain T n0 ?
Xn0|un0 ? (Y n0 , V n0)|un0. Therefore, by averaging over all realizations, we have

I(T n0;Y n0, V n0|Un0) ? I(Xn0;Y n0 , V n0|Un0). (20)

However,

I(T n0;Y n0V n0 |Un0) = I(T n0;V n0 |Un0) + I(T n0;Y n0|V n0 , Un0)

= I(T n0;Y n0|V n0), (21)

since T n0 is independent of (Un0 , V n0), and Un0 = k(V n0). Furthermore,

I(Xn0;Y n0 , V n0 |Un0) = I(Xn0 ;V n0|Un0) + I(Xn0 ;Y n0|V n0 , Un0)

= I(Xn0 ;Y n0|V n0), (22)

Since V n0 ? Un0 ? Xn0 form a Markov chain. Hence,

max
p(tn0 )

I(T n0;Y n0 |V n0) ? max
p(xn0 |un0 )

I(Xn0;Y n0|V n0). (23)



8
On the other hand,

I(T n0;Y n0 |V n0) = H(Y n0|V n0)?H(Y n0|T n0, V n0) (24)
= H(Y n0|V n0)?H(Y n0|T n0, V n0 , Un0) (25)
= H(Y n0|V n0)?H(Y n0|T n0, V n0 , Un0 , Xn0) (26)
? H(Y n0|V n0)?H(Y n0|V n0 , Un0 , Xn0) (27)
= H(Y n0|V n0)?H(Y n0|Xn0, V n0) (28)
= I(Xn0;Y n0 |V n0), (29)

where (25) and (28) follow since Un0 = k(V n0); (26) follows since Xn0 is a function of
T n0 and Un0 ; and (27) follows since conditioning reduces entropy. Hence,

max
p(tn0 )

I(T n0;Y n0 |V n0) ? max
p(xn0 |un0 )

I(Xn0;Y n0|V n0). (30)

Comparing (23) and (30), we conclude the result.

IV. CONCLUSION

In this work, we obtained the capacity of time-varying block-memoryless channels

where (possibly different) noisy causal CSI is available at the transmitter and at the
receiver. We showed that for the case that the CSIT is a deterministic function of the

CSIR, the obtained result reduces to the capacity expression reported in [1].

REFERENCES

[1] A. J. Goldsmith and M. Medard, �Capacity of time-varying channels with causal channel side information,� IEEE
Trans. Inform. Theory, vol. 53, no. 3, pp. 881-899, Mar. 2007.

[2] C. E. Shannon, �Channels with side information at the transmitter,� IBM Journal of Research and Development,
vol. 2, pp. 289-293, Oct. 1958.

[3] M. Salehi, �Capacity and coding for memories with real-time noisy defect information at the encoder and decoder,�
Proc. Inst. Elec. Eng.-Pt. I, vol. 139, no. 2, pp. 113-117, Apr. 1992.

[4] G. Caire and S. Shamai,�On the capacity of some channels with channel state information,� IEEE Trans. Inform.
Theory, vol. 45, no. 6, pp. 2007-2019, Sep. 1999.


	Introduction
	Channel Model
	Capacity of Block-Memoryless Channels with CSI
	Conclusion
	References

