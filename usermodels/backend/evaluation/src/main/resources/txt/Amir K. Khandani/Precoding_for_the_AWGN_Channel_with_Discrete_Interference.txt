
ar
X

iv
:0

70
7.

04
79

v1
  [

cs
.IT

]  
3 J

ul 
20

07

Precoding for the AWGN Channel with Discrete
Interference

Hamid Farmanbar and Amir K. Khandani
Department of Electrical and Computer Engineering

University of Waterloo
Waterloo, Ontario, Canada N2L 3G1

Email: {hamid,khandani}@cst.uwaterloo.ca

Abstract� For a state-dependent DMC with input alphabet X
and state alphabet S where the i.i.d. state sequence is known
causally at the transmitter, it is shown that by using at most
|X ||S| ? |S| + 1 out of |X ||S| input symbols of the Shannon�s
associated channel, the capacity is achievable. As an example of
state-dependent channels with side information at the transmitter,
M -ary signal transmission over AWGN channel with additive
Q-ary interference where the sequence of i.i.d. interference
symbols is known causally at the transmitter is considered.
For the special case where the Gaussian noise power is zero,
a sufficient condition, which is independent of interference, is
given for the capacity to be log2 M bits per channel use. The
problem of maximization of the transmission rate under the
constraint that the channel input given any current interference
symbol is uniformly distributed over the channel input alphabet
is investigated. For this setting, the general structure of a
communication system with optimal precoding is proposed.

I. INTRODUCTION
Information transmission over channels with known interfer-

ence at the transmitter has received a great deal of attention. A
remarkable result on such channels was obtained by Costa who
showed that the capacity of the additive white Gaussian noise
(AWGN) channel with additive Gaussian i.i.d. interference,
where the sequence of interference symbols is known non-
causally at the transmitter, is the same as the capacity of
AWGN channel [1]. Therefore, the interference does not incur
any loss in the capacity. This result was extended to arbitrary
interference (random or deterministic) by Erez et al. [2]. The
result obtained by Costa does not hold for the case that the
sequence of interference symbols is known causally at the
transmitter.

Channels with known interference at the transmitter are spe-
cial case of channels with side information at the transmitter
which were considered by Shannon [3] in causal knowledge
setting and by Gel�fand and Pinsker [4] in non-causal knowl-
edge setting.

Shannon considered a discrete memoryless channel (DMC)
whose transition matrix depends on the channel state. A state-
dependent discrete memoryless channel (SD-DMC) is defined
by a finite input alphabet X = {x1, . . . , x|X |}, a finite output
alphabet Y , and transition probabilities p(y|x, s), where the
state s takes on values in a finite alphabet S = {1, . . . , |S|}.

1This work was supported by Nortel, the Natural Sciences and Engineering
Research Council of Canada (NSERC), and the Ontario Centres of Excellence
(OCE).

Shannon [3] showed that the capacity of an SD-DMC where
the i.i.d. state sequence is known causally at the encoder is
equal to the capacity of an associated regular (without state)
DMC with an extended input alphabet T and the same output
alphabet Y . The input alphabet of the associated channel is
the set of all functions from the state alphabet to the input
alphabet of the state-dependent channel. There are a total of
|X ||S| of such functions, where |.| denotes the cardinality of
a set. Any of the functions can be represented by a |S|-tuple
(xi1 , xi2 , . . . , xi|S|) composed of elements of X , implying that
the value of the function at state s is xis , s = 1, 2, . . . , |S|.

The capacity is given by [3]
C = max

p(t)
I(T ; Y ), (1)

where the maximization is taken over the probability mass
function (pmf) of the random variable T .

In the capacity formula (1), we can alternatively replace T
with (X1, . . . , X|S|), where Xs is the random variable that
represents the input to the state-dependent channel when the
state is s, s = 1, . . . , |S|.

This paper is organized as follows. In section II, we
derive an upper bound on the cardinality of the Shannon�s
associated channel input alphabet to achieve the capacity. In
section III, we introduce our channel model. In section IV,
we investigate the capacity of the channel in the absence of
noise. In section V, we consider maximizing the transmission
rate under the constraint that the channel input given any
current interference symbol is uniformly distributed over the
channel input alphabet. We present the general structure of a
communication system for the channel with causally-known
discrete interference in section VI. We conclude this paper in
section VII.

II. A BOUND ON THE CARDINALITY OF THE SHANNON�S
ASSOCIATED CHANNEL INPUT ALPHABET

We can obtain the pmf of the channel output Y as

pY (y) =
?

s?S

pS(s)pY |S(y|s)

=
?

s?S

pS(s)
?

x?X

pX|S(x|s)pY |X,S(y|x, s)

=
?

s?S

pS(s)
?

x?X

pXs(x)pY |X,S(y|x, s). (2)




The capacity of the associated channel, which is the same
as the capacity of the original state-dependent channel, is the
maximum of I(T ; Y ) = I(X1X2 � � �X|S|; Y ) over the joint
pmf values pi1i2���i|S| = Pr{X1 = xi1 , . . . , X|S| = xi|S|}, i.e.,

C = max
pi1i2���i|S|

I(X1X2 � � �X|S|; Y ). (3)

The mutual information between T and Y is the difference
between the entropies H(Y ) and H(Y |T ). It can be seen from
(2) that pY (y), and hence H(Y ), are uniquely determined
by the marginal pmfs {pXs(xi)}

|X |
i=1, s = 1, . . . , |S|. The

conditional entropy H(Y |T ) is given by

H(Y |T ) = H(Y |X1X2 � � �X|S|)

=

|X |?

i1=1

� � �

|X |?

i|S|=1

hi1���i|S|pi1���i|S| , (4)

where hi1���i|S| = H(Y |X1 = xi1 , . . . , X|S| = xi|S|).
There are |X ||S| variables involved in the maximization

problem (3). Each variable represents the probability of an
input symbol of the associated channel. The following theorem
regards the number of nonzero variables required to achieve
the maximum in (3).

Theorem 1: The capacity of the associated channel is
achieved by using at most |X ||S|? |S|+1 out of |X ||S| input
symbols with nonzero probabilities.

Proof: Denote by {p�(s)i }|X |i=1 = {p�Xs(xi)}|X |i=1 the pmf
of Xs, s = 1, 2, . . . , |S|, induced by a capacity-achieving
joint pmf {p�i1���i|S|}|X |i1,...,i|S|=1. We limit the search for a
capacity-achieving joint pmf to those joint pmfs that yield the
same marginal pmfs as {p�i1���i|S|}

|X |
i1,...,i|S|=1

. By limiting the
search to this smaller set, the maximum of I(X1 � � �X|S|; Y )
remains unchanged since the capacity-achieving joint pmf
{p�i1���i|S|}

|X |
i1,...,i|S|=1

is in the smaller set. But all joint pmfs
in the smaller set yield the same H(Y ) since they induce the
same marginal pmfs on X1, . . . , X|S|. Therefore, the maxi-
mization problem in (3) reduces to the linear minimization
problem

min
pi1���i|S|

|X |?

i1=1

� � �

|X |?

i|S|=1

hi1���i|S|pi1���i|S|

s. t.
|X |?

i2=1

� � �

|X |?

i|S|=1

pi1���i|S| = p�
(1)
i1

, i1 = 1, . . . , |X |,

.

.

.

.

.

.

|X |?

i1=1

� � �

|X |?

i|S|?1=1

pi1���i|S| = p�
(|S|)
i|S|

, i|S| = 1, . . . , |X |,

pi1���i|S| ? 0, i1, . . . , i|S| = 1, 2, . . . , |X |. (5)
There are |X ||S| equality constraints in (5) out of which
|X ||S| ? |S| + 1 are linearly independent. From the theory
of linear programming, the minimum of (5), and hence the

maximum of I(X1 � � �X|S|; Y ), is achieved by a feasible
solution with at most |X ||S| ? |S|+ 1 nonzero variables.
Theorem 1 states that at most |X ||S| ? |S| + 1 out of |X ||S|
input symbols of the associated channel are needed to be used
with positive probability to achieve the capacity. However, in
general one does not know which of the inputs must be used
to achieve the capacity. If we knew the marginal pmfs for
X1, . . . , X|S| induced by a capacity-achieving joint pmf, we
could obtain the capacity-achieving joint pmf itself by solving
the linear program (5).

III. THE CHANNEL MODEL
We consider data transmission over the channel

Y = X + S + N, (6)
where X is the channel input, which takes on values in a fixed
real constellation

X = {x1, x2, . . . , xM} , (7)
Y is the channel output, N is additive white Gaussian noise
with power PN , and the interference S is a discrete random
variable that takes on values in

S = {s1, s2, . . . , sQ} (8)
with probabilities r1, r2, . . . , rQ, respectively. The sequence of
i.i.d. interference symbols is known causally at the encoder.
The above channel can be considered as a special case of
state-dependent channels considered by Shannon with one
exception, that the channel output alphabet is continuous.
In our case, the likelihood function fY |X,S(y|x, s) is used
instead of the transition probabilities. We denote the input to
the associated channel by T , which can also be represented
as (X1, X2, . . . , XQ), where Xj is the random variable that
represents the channel input when the current interference
symbol is sj , j = 1, . . . , Q.

The likelihood function for the associated channel is given
by

fY |T (y|t) =

Q?

j=1

rjfY |X,S(y|xij , sj)

=

Q?

j=1

rjfN (y ? xij ? sj), (9)

where fN denotes the pdf of the noise N , and t is
the input symbol of the associated channel represented by
(xi1 , xi2 , . . . , xiQ).

According to theorem 1, the capacity of our channel is
obtained by using at most MQ ? Q + 1 out of MQ input
symbols of the associated channel.

IV. THE NOISE-FREE CHANNEL
We consider a special case where the noise power is zero

in (6). In the absence of noise, the channel output Y takes on
at most MQ different values since different X and S pairs
may yield the same sum. If Y takes on exactly MQ different



values, then it is easy to see that the capacity is log2 M bits
1: The decoder just needs to partition the set of all possible
channel output values into M subsets of size Q corresponding
to M possible inputs, and decide that which subset the current
received symbol belongs to.

In general, where the cardinality of the channel output
symbols can be less than MQ, we will show that under some
condition on the channel input alphabet there exists a coding
scheme that achieves the rate log2 M in one use of the channel.
We do this by considering a one-shot coding scheme which
uses only M (out of MQ) inputs of the associated channel.

In a one-shot coding scheme, a message is encoded to
a single input of the associated channel. Any input of the
associated channel can be represented by a Q-tuple composed
of elements of X . Given that the current interference symbol
is sj , the jth element of the Q-tuple is sent through the
channel. Therefore, one single message can result in (up to)
Q symbols at the output. For convenience, we consider the
output symbols corresponding to a single message as a multi-
set2 of size (exactly) Q. If the M multi-sets at the output
corresponding to M different messages are mutually disjoint,
reliable transmission through the channel is possible.

Unfortunately, we cannot always find M inputs of the
associated channel such that the corresponding multi-sets are
mutually disjoint. For example, consider a channel with the
input alphabet X = {0, 1, 2, 4} and the interference alphabet
S = {0, 1, 3}. It is easy to check that for this channel we
cannot find four triples composed of elements of X such that
the corresponding multi-sets are mutually disjoint. In fact, by
entropy calculations we can show that the capacity of the
channel in this example is less than 2 bits.

However, if we put some constraint on the channel input
alphabet, the rate log2 M is achievable.

Theorem 2: Suppose that the elements of the channel input
alphabet X form an arithmetic progression. Then the capacity
of the noise-free channel

Y = X + S, (10)

where the sequence of interference symbols is known causally
at the encoder equals log2 M bits.

Proof: Let Y(q) be the set of all possible outputs of the
noise-free channel when the interference symbol is sq , i.e.,

Y(q) = {x1 + sq, x2 + sq, . . . , xM + sq} , q = 1, . . . , Q.
(11)

The union of Y(q)s is the set of all possible outputs of the
noise-free channel.

Without loss of generality we can assume that s1 <
s2 < � � � < sQ. The elements of Y(q) form an arithmetic
progression, q = 1, . . . , Q. Furthermore, these Q arithmetic
progressions are shifted versions of each other.

1This is true even if the interference sequence is unknown to the encoder.
2A multi-set differs from a set in that each member may have a multiplicity

greater than one. For example, {1, 3, 3, 7} is a multi-set of size four where
3 has multiplicity two.

...

...

.                .                .                            ....

.
.
.

.
.
.

.                *                *                            *

...*                *                            *                .

.                .                .                            .Y (1):

Y (j):

Y (q+1):

Y (q):

xM + sq+1

xM + s1x3 + s1x2 + s1x1 + s1

x1 + sj xM + sjx3 + sjx2 + sj

xk + sq+1x2 + sq+1x1 + sq+1

x1 + sq x2 + sq x3 + sq xM + sq

Fig. 1. The elements of Y(1), . . . ,Y(q+1) shown as shifted version of each
other. The elements of Y(q+1) up to xk + sq+1 appear in Y(j).

We prove by induction on Q that there exist M mutually-
disjoint multi-sets of size Q composed of the elements of
Y(1),Y(2), . . . ,Y(Q) (one element from each). If we can
find such M multi-sets of size Q, then we can obtain the
corresponding M Q-tuples of elements of X by subtracting
the corresponding interference terms from the elements of the
multi-sets. These M Q-tuples can serve as the inputs of the
associated channel to be used for sending any of M distinct
messages through the channel without error in one use of the
channel, hence achieving the rate log2 M bits per channel use.

For Q = 1, the statement of the theorem is true since we can
take {x1 +s1}, {x2 +s1}, . . . , {xM +s1} as mutually-disjoint
sets of size one.

Assume that there exist M mutually-disjoint multi-sets of
size Q = q. For Q = q+1, we will have the new set of channel
outputs Y(q+1) = {x1 +sq+1, x2 +sq+1, . . . , xM +sq+1}. We
consider two possible cases:

Case 1: None of the elements of Y(q+1) appear in any of
the multi-sets of size Q = q.

In this case, we include the elements of Y(q+1) in the M
multi-sets arbitrarily (one element is included in each multi-
set). It is obvious that the resulting multi-sets of size Q = q+1
are mutually disjoint.

Case 2: Some of the elements of Y(q+1) appear in some of
the multi-sets of size Q = q.

Suppose that the largest element of Y(q+1) which appears
in any of the sets Y(1), . . ., Y(q) (or equivalently, in any of
the multi-sets of size Q = q) is xk + sq+1 for some 1 ?
k ? M ? 1. Then since Y(q+1) is shifted version of each
Y(1), . . . ,Y(q) and sq+1 > sq > � � � > s1, exactly one of the
sets Y(1), . . . ,Y(q), say Y(j) for some 1 ? j ? q, contains
all elements of Y(q+1) up to xk + sq+1. See fig. 1. Since any
of the disjoint multi-sets of size Q contain just one element
of Y(j), the elements of Y(q+1) up to xk + sq+1 appear in
different multi-sets of size Q = q. We can form the disjoint
multi-sets of size q + 1 by including these common elements
in the corresponding multi-sets and including the elements of
{xk+1 + sq+1, . . . , xM + sq+1} in the remaining multi-sets
arbitrarily.

The condition on the channel input alphabet in the statement
of theorem 2 is a sufficient condition for the channel capacity
to be log2 M . However, it is not a necessary condition. For
example, the statement of theorem 2 without that condition is
true for the case of Q = 2. Because in the second iteration,



we do not need the arithmetic progression condition to form
M mutually-disjoint multi-sets of size two.

The proof of theorem 2 is actually a constructive algorithm
for finding M (out of MQ) inputs of the associated channel
to be used with probability 1

M
to achieve the rate log2 M bits.

It is interesting to see that the set containing the qth elements
of the M Q-tuples obtained by the constructive algorithm is
X , q = 1, . . . , Q. This is due to the fact that each multi-set
contains one element from each Y(1), . . . ,Y(Q). Therefore,
a uniform distribution on the M Q-tuples induces uniform
distribution on X1, . . . , XQ.

V. UNIFORM TRANSMISSION
In the sequel, we study the maximization of the rate

I(X1 � � �XQ; Y ) over joint pmfs {pi1���iQ}Mi1,...,iQ=1 that in-
duce uniform marginal distributions on X1, . . .,XQ, i.e.,

p
(1)
i = p

(2)
i = � � � = p

(Q)
i =

1

M
, i = 1, 2, . . . , M, (12)

for which we show how to obtain the optimal input proba-
bility assignment. We call a transmission scheme that induces
uniform distribution on X1, . . . , XQ as uniform transmission.
The uniform distribution for X1, . . . , XQ implies uniform
distribution for X , the input to the state-dependent channel
defined in (6).

In the previous section, we established that the capacity
achieving pmf for the asymptotic case of noise-free channel
induces uniform distributions on X1, . . . , XQ (provided that
we can find M Q-tuples such that the corresponding multi-
sets are mutually disjoint).

Considering the constraints in (12), the maximization of
I(X1 � � �XQ; Y ) is reduced to the linear minimization prob-
lem

min
pi1���iQ

M?

i1=1

� � �

M?

iQ=1

hi1���iQpi1���iQ

s. t.
M?

i2=1

� � �

M?

iQ=1

pi1���iQ =
1

M
, i1 = 1, . . . , M,

.

.

.

.

.

.

M?

i1=1

� � �

M?

iQ?1=1

pi1���iQ =
1

M
, iQ = 1, . . . , M,

pi1���iQ ? 0, i1, . . . , iQ = 1, 2, . . . , M. (13)
The same argument used in the last part of the proof of
theorem 1 can be used to show that the maximum is achieved
by using at most MQ?Q+1 inputs of the associated channel
with positive probabilities. This is restated in the following
corollary.

Corollary 1: The maximum of I(X1 � � �XQ; Y ) over joint
pmfs {pi1���iQ}Mi1,...,iQ=1 that induce uniform marginal distri-
butions on X1, X2, . . . , XQ is achieved by a joint pmf with
at most MQ?Q + 1 nonzero elements.

This result is independent of the coefficients {hi1���iQ}.
However, which probability assignment with at most MQ ?

?5 0 5 10
0

0.1

0.2

0.3

0.4

0.5

0.6

0.7

0.8

0.9

1

SNR (dB)

M
ut

ua
l I

nf
or

m
at

io
n

Maximum achievable rate for uniform transmission

 

 

Uniform 2?PAM (no interference)
p12=p21=1/2
p11=p22=1/2

Fig. 2. Maximum mutual information vs. SNR for the channel with X =
S = {?1, +1} and r1 = r2 = 12 .

Q + 1 nonzero elements is optimal depends on the coeffi-
cients {hi1���iQ}. The coefficient hi1���iQ is determined by the
interference levels s1, . . . , sQ, the probability of interference
levels r1, . . . , rQ, the noise power PN , and the signal points
x1, x2, . . . , xM . The optimal probability assignment is ob-
tained by solving the linear programming problem (13) using
the simplex method [6].
A. Two-Level Interference

If the number of interference levels is two, i.e., Q = 2, we
can make a stronger statement than corollary 1.

Theorem 3: The maximum of I(X1X2; Y ) over
{pi1i2}

M
i1,i2=1

with uniform marginal pmfs for X1 and
X2 is achieved by using exactly M out of M2 inputs of the
associated channel with probability 1

M
.

Proof: The equality constraints of (13) can be written in
matrix form as

Ap = 1, (14)
where A is a zero-one MQ�MQ matrix, p is M times the
vector containing all pi1���iQs in lexicographical order, and 1
is the all-one MQ� 1 vector.

For Q = 2, it is easy to check that A is the vertex-edge
incidence matrix of KM,M , the complete bipartite graph with
M vertices at each part. Therefore, A is a totally unimodular
matrix3 [5]. Hence, the extreme points of the feasible region
F = {p : Ap = 1,p ? 0} are integer vectors. Since the
optimal value of a linear optimization problem is attained at
one of the extreme points of its feasible region, the minimum
in (13) is achieved at an all-integer vector p?. Considering
that p? satisfies (14), it can only be a zero-one vector with
exactly M ones.

Fig. 2 depicts the maximum mutual information (for the uni-
form transmission scenario) vs. SNR for the channel with X =
S = {?1, +1} and equiprobable interference symbols. The
mutual information vs. SNR curve for the interference-free
AWGN channel with equiprobable input alphabet {?1, +1}

3A totally unimodular matrix is a matrix for which every square submatrix
has determinant 0,1, or ?1.



is plotted for comparison purposes. As it can be seen, for
low SNRs, the input probability assignment p11 = p22 =
1
2 is optimal, whereas at high SNRs, the input probability
assignment p12 = p21 = 12 is optimal. The maximum
achievable rate for uniform transmission is the upper envelope
of the two curves corresponding to different input probability
assignments. Also, it can be observed that the achievable rate
approaches log2 2 = 1 bit per channel use as SNR increases
complying with the fact that we established in section IV for
the noise-free channel.

It turns out from the proof of theorem 3 that the optimum
solution of the linear optimization problem, p?, is a zero-
one vector. So, if we add the integrality constraint to the
set of constraints in (14), we still obtain the same optimal
solution. The resulting integer linear optimization problem is
called the assignment problem [5], which can be solved using
low-complexity algorithms such as the Hungarian method [6].
B. Integrality Constraint for the Q-Level Interference

The fact that for the case Q = 2, there exists an optimal p
which is a zero-one vector with exactly M ones simplifies the
encoding operation. Because any encoding scheme just needs
to work on a subset of size M of the associated channel input
alphabet with equal probabilities 1

M
.

For Q 6= 2, A is not a totally unimodular matrix. Therefore,
not all extreme points of the feasible region defined by Ap =
1,p ? 0, are integer vectors. However, at the expense of
possible loss in rate, we may add the integrality constraint
in this case. The resulting optimization problem is called
the multi-dimensional assignment problem [7]. The optimal
solution of (13) with the integrality constraint, will be a
vector with exactly M nonzero elements with the value 1

M
.

Therefore, any encoding scheme just needs to use M symbols
of the associated channel with equal probabilities, simplifying
the encoding operation.

VI. OPTIMAL PRECODING
The general structure of a communication system for the

channel defined in (6) is shown in fig. 3. Any encoding and
decoding scheme for the associated channel can be translated
to an encoding and decoding scheme for the original channel
defined in (6). A message w is encoded into a block of
length n composed of input symbols of the associated channel
t ? (xi1 , xi2 , . . . , xiQ). There are MQ input symbols. How-
ever, we showed that the maximum rate with uniformity and
integrality constraints can be achieved by using just M input
symbols of the associated channel with equal probabilities.
The optimal M input symbols of the associated channel are
obtained by solving the linear programming problem (13)
with the integrality constraint. Those M input symbols of the
associated channel define the optimal precoding operation: For
any t that belongs to the set of M optimal input symbols,
the precoder sends the qth component of t if the current
interference symbol is sq , q = 1, . . . , Q. Based on the received
sequence, the receiver decodes w� as the transmitted message.

Encoder Precoder
T X

S N

Y
Decoder

w
w�

Fig. 3. General structure of the communication system for channels with
causally-known discrete interference.

VII. CONCLUSION
In this paper, we proved that the capacity of an SD-DMC

with finite input alphabet X and finite state alphabet S and
with causally known i.i.d. state sequence at the encoder can be
achieved by using at most |X ||S|? |S|+1 out of |X ||S| input
symbols of the associated channel. As an example of state-
dependent channels with side information at the encoder, we
investigated M -ary signal transmission over AWGN channel
with additive Q-level interference, where the sequence of
interference symbols is known causally at the transmitter.

For the noise-free channel, provided that the signal points
are equally spaced, we proposed a one-shot coding scheme that
uses M input symbols of the associated channel to achieves
the capacity log2 M bits.

We considered the transmission schemes with uniform pmfs
for X1, . . . , XQ. For this so called uniform transmission, the
optimal input probability assignment with at most MQ?Q+
1 nonzero elements can be obtained by solving the linear
optimization problem (13). The optimal solution to (13) with
the integrality constraint has exactly M nonzero elements. For
the case Q = 2, we showed that the integrality constraint does
not reduce the maximum achievable rate. The loss in rate (if
there is any) by imposing the integrality constraint for the
general case is a problem to be explored.

ACKNOWLEDGMENT
The authors would like to thank Gerhard Kramer and Syed

Ali Jafar for pointing out that the proof of theorem 1, which
was originally given for the channel model considered in this
paper, works for any SD-DMC with causal side information
at the encoder.

REFERENCES
[1] M. H. M. Costa, �Writing on dirty paper,� IEEE Trans. Inform. Theory,

vol. IT-29, no. 3, pp. 439-441, May 1983.
[2] U. Erez, S. Shamai, and R. Zamir, �Capacity and lattice strategies for

canceling known interference,� IEEE Trans. Inform. Theory, vol. 51, no.
11, pp. 3820-3833, Nov. 2005.

[3] C. E. Shannon, �Channels with side information at the transmitter,� IBM
Journal of Research and Development, vol. 2, pp. 289-293, Oct. 1958.

[4] S. Gel�fand and M. Pinsker, �Coding for channel with random param-
eters,� Problems of Control and Information Theory, vol. 9, no. 1, pp.
19-31, Jan. 1980.

[5] G. Nemhauser and L. Wolsey, Integer and combinatorial optimization,
John Wiley & Sons, 1988.

[6] B. Kreko�, Linear Programming, Translated by J. H. L. Ahrens and C. M.
Safe. Sir Isaac Pitman & Sons Ltd., 1968.

[7] W. P. Pierskalla, �The multidimensional assignment problem,� Operations
Research 16, 1968, p. 422-431.


	Introduction
	A bound on the Cardinality of the Shannon's Associated Channel input alphabet
	The Channel Model
	The Noise-Free Channel
	Uniform Transmission
	Two-Level Interference
	Integrality Constraint for the Q-Level Interference

	Optimal Precoding
	Conclusion
	References

