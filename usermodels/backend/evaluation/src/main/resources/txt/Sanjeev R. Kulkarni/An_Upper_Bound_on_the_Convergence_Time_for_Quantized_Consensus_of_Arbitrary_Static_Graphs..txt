
1
An Upper Bound on the Convergence Time for
Quantized Consensus of Arbitrary Static Graphs

Shang Shang?, Paul Cuff?, Pan Hui� and Sanjeev Kulkarni?
?Department of Electrical Engineering, Princeton University

Princeton NJ, 08540, U.S.A.
�Department of Computer Science and Engineering, The Hong Kong University of Science and Technology,

Hong Kong
?{sshang, cuff, kulkarni}@princeton.edu, �panhui@cse.ust.hk

Abstract�We analyze a class of distributed quantized con-
sensus algorithms for arbitrary static networks. In the initial
setting, each node in the network has an integer value. Nodes
exchange their current estimate of the mean value in the network,
and then update their estimation by communicating with their
neighbors in a limited capacity channel in an asynchronous clock
setting. Eventually, all nodes reach consensus with quantized
precision. We analyze the expected convergence time for the
general quantized consensus algorithm proposed by Kashyap et
al [1]. We use the theory of electric networks, random walks,
and couplings of Markov chains to derive an O(N3 logN) upper
bound for the expected convergence time on an arbitrary graph
of size N , improving on the state of art bound of O(N5) for
quantized consensus algorithms. Our result is not dependent on
graph topology. Example of complete graphs is given to show
how to extend the analysis to graphs of given topology. This is
consistent with the analysis in [2].

Index Terms�Distributed quantized consensus, gossip, conver-
gence time

I. INTRODUCTION

Over the past decade, the problem of quantized consen-
sus has received significant attention [1], [3]�[9]. It models
averaging in a network with a limited capacity channel [1].
Distributed algorithms are attractive due to their flexibility,
simple deployment and the lack of central control. This prob-
lem is of interest in the context of coordination of autonomous
agents, estimation, distributed data fusion on sensor networks,
peer-to-peer systems, etc. [2], [3]. It is especially relevant to
remote and extreme environments where communication and
computation are limited, for example, in a decision-making
sensor network [10].

This work is motivated by a class of quantized consen-
sus algorithms in [1]: nodes randomly and asynchronously
update local estimate and exchange information. Unlike the
distributed algorithm in [11], where the sum of values in the
network is not preserved, Kashyap et al. proposed an algorithm
guaranteeing convergence with limited communication, more
specifically, only involving quantization levels [1]. This is a
desired property in a large-scale network where memory is
limited, communication between nodes is expensive and no

This work was presented in part at IEEE INFOCOM 2013.

central control is available to the network. Also, this dis-
tributed algorithm is designed in a privacy-preserving manner:
during the process, the local estimation on the average value
is exchanged without revealing the initial observation from
nodes. Analysis of convergence time on the complete graph
and line graph is given in the original paper in [1], and an
O(N5) bound was claimed in [12] by creating a random walk
model.

In this paper, unlike the natural random walk model claimed
in [12], we construct a biased lazy random walk model for
this random communication process to analyze the multi-level
quantized consensus problem with the use of Lyapunov func-
tions [1]. By novelly utilizing the relation between commuting
time of a random walk and electric networks [13], we derive
an upper bound on the hitting time of a biased random walk.
Several coupled Markov processes are then constructed to
help the analysis. Thus we improve the state of art bound
in [12] from O(N5) to O(N3 logN). In [2], proving through
different methods, the authors introduced a function ?(G,?)
depending on the graph structure and voting margin to provide
an upper bound on the convergence time of binary consensus
algorithm, but did not provide a universal upper bound on an
arbitrary graph. Unlike the convergence time bound in [2],
which depends on the network topologies and the location of
eigenvalues of some contact rate matrices, our result provides
a universal upper bound on the convergence time of quantized
consensus. Notably, a deterministic protocol was proposed in
[14], which achieves quantized consensus in O(N2). However,
it cannot be extended beyond fixed graphs as the algorithms
discussed in this paper, as analyzed in [12].

The contribution of this paper is as follows:
� A polynomial upper bound of O(N3 logN) for the quan-

tized consensus algorithm. It is, to the best knowledge
of the authors, the tightest bound in literature for the
quantized consensus algorithm proposed in [1], [4]. We
use the degree of nodes on the shortest path on the graph
to improve the bound on the hitting time of the biased
random walk.

� The analysis for arbitrary graphs is extended to a tighter
bound for certain network topologies by computing the
effective resistance between a pair of nodes on the graph.
This is attractive because we can then apply results from
algebraic graph theory [15], [16] to compute the effective

ar
X

iv
:1

40
9.

68
28

v1
  [

cs
.SY

]  
24

 Se
p 2

01
4



2
resistance easily on the given graph structure.
The remainder of this paper is organized as follows. Section
2 describes the algorithm proposed in [1], and formulates
the convergence speed problem. In Section 3, we derive our
polynomial bound for this class of algorithms. We provide our
conclusions in Section 4.

II. PROBLEM STATEMENT
A network is represented by a connected graph G = (V, E),

where V = {1, 2, ..., N} is the set of nodes and E is the set
of edges. (i, j) ? E if nodes i, j can communicate with each
other. Ni is the set of neighbors of node i.

Consider a network of N nodes, labeled 1 through N .
As proposed in [1], [3], [4], each node has a clock which
ticks according to a rate 1 exponential distribution. By the
superposition property for the exponential distribution, this
set up is equivalent to a single global clock with a rate
N exponential distribution ticking at times {Zk}k?0. The
communication and update of states only occur at {Zk}k?0.
When the clock of node i ticks, i randomly chooses a neighbor
j from the set Ni. We say edge (i, j) is activated. In the rest of
the analysis, for consistency with previous literatures as [12]
[1], we discretize time instant t according to {Zk}k?0, i.e., in
terms of the total number of clock ticks.

In the rest of this section, we will describe the distributed
quantized consensus algorithm [1]. We are interested in the
performance of this class of algorithms on arbitrary graphs.

A. Quantized Consensus

Without loss of generality, let us assume that all nodes hold
integer values and the quantization is 1. Let Q(i)(t) denote the
integer value of node i at time t, with Q(i)(0) denoting the
initial values. Define

Qsum =

N?
i=1

Q(i)(0). (1)

Let Qsum be written as qN + r, where 0 ? r < N . Then
the mean of the initial value in the network 1NQsum ? [q, q+
1). Thus either q or q + 1 is an acceptable integer value for
quantized average consensus (if the quantization level is 1).

Definition 1 (Convergence on Quantized Consensus). A quan-
tized consensus reaches convergence at time t, if for any node
i on the graph, Q(i)(t) ? {q, q + 1}.

There are a few properties that are desired for the quantized
consensus algorithm:
� Sum conservation:

N?
i=1

Q(i)(t) =

N?
i=1

Q(i)(t+ 1). (2)

� Variation non-increasing: if two nodes i, j exchange
information,

|Q(i)(t+ 1)?Q(j)(t+ 1)| ? |Q(i)(t)?Q(j)(t)|. (3)
When two nodes i and j exchange information, without loss

of generality, suppose that Q(i)(t) ? Q(j)(t). They follow the
simple update rules below:

1) If Q(j)(t)?Q(i)(t) ? 2, a non-trivial meeting occurs:
Q(i)(t+ 1) = Q(i)(t) + 1, Q(j)(t+ 1) = Q(j)(t)? 1.

2) If Q(j)(t)?Q(i)(t) ? 1, a trivial meeting occurs:

Q(i)(t+ 1) = Q(j)(t), Q(j)(t+ 1) = Q(i)(t).

We can view this random process as a finite state Markov
chain. Because the variation decreases whenever there is a non-
trivial exchange, convergence will be reached in finite time
almost surely.

Remark: In this section, the update rules allow the node
values to change by at most 1. This is relevant to load-
balancing systems where only one value can be exchanged
in the channel at a time due to the communication limit [1].
Adjustments can be made for this class of quantized consensus
algorithms, e.g. when two nodes exchange information, both
nodes can update their value to the mean of the two. The
analysis on the convergence time remains similar.

III. CONVERGENCE TIME ANALYSIS

The main result of this work is the following theorem:

Theorem 1. For a connected network of N nodes, an upper
bound for the expected convergence time of the quantized
consensus algorithm is O(N3 log(N)).

We use the analogy of electric networks and random walks
to derive the upper bound. Before deriving the bound on
the convergence time, we first provide some definitions and
notation that we will use and prove some useful lemmas in
Section III-A and Section III-B.

A. Definition and Notation

Definition 2 (Hitting Time). For a graph G and a specific
random walk X , and i, j ? V , let H(i, j) denote the expected
number of steps a random walk beginning at i must take
before reaching j, i.e., H(i, j) = E [min{t : Xt = j}|X0 = i]
. Define the �hitting time� of G by H(G) = maxi,j H(i, j).
Definition 3 (Meeting Time). Consider two random
walkers X,Y placed on G, and i, j ? V . At each tick of
the clock, they move according to some joint probability
distribution. Let M(i, j) denote the expected time for the
two walkers starting from i and j respectively to meet
at the same node or to cross each other through the
same edge (if they move at the same time), i.e.M(i, j) =
E [min{t : Xt = Yt or Xt = Yt?1, Yt = Xt?1}|X0 = i, Y0 = j].
Define the �meeting time� of G by M(G) = maxi,jM(i, j).

Define a simple random walk on G, with transition matrix
PS = (Pij) as follows:
� PSii := 0 for ?i ? V ,
� PSij :=

1
|Ni| for (i, j) ? E .

Ni is the set of neighbors of node i and |Ni| is the degree of
node i.

Define a natural random walk with transition matrix PN =
(Pij) as follows:
� PNii = 1? 1N for ?i ? V ,



3
� PNij =
1

N |Ni| for (i, j) ? E .
Define a biased random walk with transition matrix PB =

(Pij) as follows:
� PBii := 1? 1N ?

?
k?Ni

1
N |Nk| for ?i ? V ,

� PBij :=
1
N

(
1
|Ni| +

1
|Nj |

)
for (i, j) ? E .

B. Hitting Time and Meeting Time on Weighted Graph

In this class of algorithms, we label the initial obser-
vations(states or values) by the nodes as ?1, ?2, ..., ?N . A
random walk is a Markov process with random variables
A1, A2, ..., At, ... such that the next state only depends on
the current state. In the system setting, when the node i�s
clock ticks, i randomly choose one of its neighbor node j
from the set Ni to exchange information. We notice that
before any two observations ?m, ?n meet each other, they
take random walks on the graph G. Their marginal transition
matrices are both PB . It may be tempting to think that they are
taking the natural random walks as stated in [12]. Upon closer
inspection, we find that there are two sources stimulating the
random walk from i to j, for all (i, j) ? E : one is active,
initiated by node i�s clock, which leads to P 1ij = P

N
ij ; the

other one is passive, initiated by i�s neighbor j, which leads
to P 2ij = P

N
ji . Thus Pij = P

1
ij +P

2
ij = P

B
ij ; i.e., the transition

matrix is actually PB instead of PN . Because of the system
settings, two random walks ?m, ?n can only move at the same
time if they are adjacent. Denote this joint random process as
X . Suppose ?m is at node x, and ?n is at node y.

For x /? Ny , and i ? Nx, we have
PX joint(?m moves from x to i, ?n does not move)

= PBxi ? PX joint(?m moves from x to i, ?n moves)
= PBxi . (4)

Similar for PX joint(?n moves from y to j, ?m does not move),
where j ? Ny . Also,

PX joint(?m does not move, ?n does not move)

= 1?
?
i?Nx

PBxi ?
?
j?Ny

PByj . (5)

For x ? Ny and i 6= y we have,
PX joint(?m moves from x to i, ?n does not move)

= PBxi ? PX joint(?m moves from x to i, ?n moves)
= PBxi . (6)

PX joint(?m moves to y, ?n moves to x) = PBxy. (7)

PX joint(?m does not move, ?n does not move)

= 1?
?
i?Nx

PBxi ?
?
j?Ny

PByj + P
B
xy. (8)

Lemma 1. The biased random walk X is a reversible Markov
process.

i j

k1

2

1

Fig. 1: A simple example of effective resistance.

Proof. Let pi be the stationary distribution of the biased
random walk X . It is easy to verify that

pii =
1

N
(9)

for all i ? V . Thus by the symmetry of PB ,
piiP

B
ij = pijP

B
ji .

Lemma 2. In an arbitrary connected graph G with N nodes,
the hitting time of the biased random walk X satisfies

HPB (G) < 3N3.
Proof. The biased random walk X defined above is a random
walk on a weighted graph with weight

wij :=
1

N

(
1

|Ni| +
1

|Nj |
)

for (i, j) ? E . (10)

wii := 1?
?
j?Ni

wij . (11)

wi =
?
j?V

wij = 1, w =
?
i

wi = N. (12)

It is well-known that there is an analogy between a weighted
graph and an electric network [13]. Let rij denote the re-
sistance between to adjacent nodes, i.e. an edge (i, j) ? E ,
and let r?xy denote the effective resistance between any two
nodes x, y. For example, in Fig. 1, rij = 2, rik = 1,
r?ij = 1 and r

?
ik = 0.75. In an electric circuit, we always

have r?ij ? rij because of Ohm�s Law. For a random walk
on a weighted graph, a wire linking i and j has conductance
wij , i.e., resistance rij = 1/wij . And the commuting time
between x and y, HPB (x, y) +HPB (y, x), has the following
relationship with the effective resistance r?xy:

HPB (x, y) +HPB (y, x) = wr?xy, (13)
where r?xy is the effective resistance in the electric network
between node x and node y (Chapter 3 Corollary 11 in [13]).
Since the degree of any node is at most N ? 1, for (i, j) ? E ,

wij =
1

N

(
1

|Ni| +
1

|Nj |
)
>

1

N

1

min(|Ni|, |Nj |) (14)

rij < N �min(|Ni|, |Nj |) (15)
Consequently, r?ij ? rij < N �min(|Ni|, |Nj |).



4
For all x, y ? V , let Q = (q1 = x, q2, q3, ..., ql?1, ql = y)
be the shortest path on the graph connecting x and y . Now
we claim that

?l
k=1 |Nqk | < 3N (from the proof of Theorem

2 in [17]).
Since any node not lying on the shortest path can only be

adjacent to at most three vertices on Q, we have
l?

k=1

|Nqk | ? 2l + 3(N ? l) < 3N. (16)

The first term 2l in Equation (16) is due to the fact that
nodes on the shortest path connect to one another, resulting in
total degree about 2l (2(l ? 1) to be precise). The second
term 3(N ? l) is due to the fact that any node, say u,
not lying on the shortest path can only be adjacent to at
most three vertices on the shortest path, say qi1, qi2, qi3 on
Q. Suppose u is also adjacent to qi4, which is also on
Q. Without loss of generality, let i1 < i2 < i3 < i4.
Then the path q1, ..., qi1, u, qi4, ..., ql is shorter than the path
Q = (q1, ..., qi1, ..., qi2, ..., qi3, ..., qi4, ..., ql), contradict with
the fact u is not on shortest path. Thus the (N ? l) points not
on the shortest path contributing at most 3(N ? l) degrees for
the total degrees of nodes on the shortest path.

The effective resistance between any two nodes x and y is
less than or equal to the sum of the effective resistance r?qkqk+1
on the shortest path. This is due to the triangle inequality for
effective resistance on undirected graphs (Theorem B in [18]).
By (15) and (16), we have

r?xy ?
l?1?
k=1

r?qkqk+1 ?
l?1?
k=1

N �min(|Nqk |, |Nqk+1|)

? N �
l?

k=1

|Nqk | < 3N2 (17)

By (13), we have

HPB (x, y) < HPB (x, y) +HPB (y, x)
= wr?xy
< N � 3N2
= 3N3. (18)

This completes the proof.

Note that this is an upper bound for arbitrary connected
graphs. A tighter bound can be derived for certain network
topologies.

Lemma 3. HPB (x, y) + HPB (y, z) + HPB (z, x) =
HPB (x, z) +HPB (z, y) +HPB (y, x).
Proof. This is direct result from Lemma 2 in Chap 3 of
Aldous-Fill�s book [13] since X is reversible.
Definition 4 (Hidden Vertex). A vertex t in a graph is said
to be hidden if for every other point in the graph, H(t, v) ?
H(v, t). A hidden vertex is shown to exist for all reversible
Markov chains in Lemma 3 in [19].

Lemma 4. The meeting time of any two random walders on
the network G following the random processes X in Section
III-B is less than 4HPB (G).

Proof. In order to prove the lemma, we construct a coupled
Markov chain, X ? to assist the analysis. X ? has the same joint
distribution as X except (7) and (8).

PX ?joint(?m, ?n meet at x or y) = 2PBxy (19)

PX ?joint(?m does not move, ?n does not move)

= 1?
?
i?Nx

PBxi ?
?
j?Ny

PByj . (20)

The proof is based on the following sequence of claims:
1) The meeting time of two random walkers following X ?

is less than 2HPB (G).
2) The meeting time of random process X and X ? satisfies
MX (G) ? 2MX ?(G).

3) There holds that MX (G) < 4HPB (G).
The formal proof is as follows:
For convenience, we adopt the following notation: Let

H(x�, y) denote the weighted average of H(u, y) over all
neighbors u of x; Let M(x�, y) denote the weighted average
of M(u, y) over all neighbors u of x; Let ?(x�, y) denote
the weighted average of ?(u, y) over all neighbors u of x.
Weightings are according to the edge weights.

Similar as in [19], define a potential function

?(x, y) := HPB (x, y) +HPB (y, t)?HPB (t, y), (21)
where t is a hidden vertex on the graph. By Corollary 3,
?(x, y) is symmetric, i.e. ?(x, y) = ?(y, x). By the definition
of meeting time, M is also symmetric, i.e. M(x, y) =
M(y, x). Next we use ? to bound the meeting time.

By the definition of hitting time, for x 6= y we have
HPB (x, y) = 1 + PBxxHPB (x, y) +

?
i?Nx

PBxiHPB (i, y)

= 1 + wxxHPB (x, y) +
?
i?Nx

wxiHPB (i, y),

(22)

i.e.,

HPB (x, y) =
1?

i?Nx wxi
+

?
i?Nx wxiHPB (i, y)?

i?Nx wxi

=
1?

i?Nx wxi
+H(x�, y). (23)

So for x 6= y, by Equation (21) and (23),

?(x, y) =
1?

i?Nx wxi
+ ?(x�, y). (24)

Similarly, by the definition of meeting time, we have,

MX ?(x, y) = 1 +
??1? ?

i?Nx
PBxi ?

?
j?Ny

PByj

??MX ?(x, y)
+

?
i?Nx

PBxiMX ?(i, y)

+
?
j?Ny

PByjMX ?(x, j). (25)

Note that (25) also holds for x ? Ny . We now have



5
???
i?Nx

PBxi +
?
j?Ny

PByj

??MX ?(x, y)
= 1 +

?
i?Nx

PBxiMX ?(i, y) +
?
j?Ny

PByjMX ?(x, j).(26)

(26) shows that at least one of the two inequalities below
holds:

MX ?(x, y) >
?

i?Nx P
B
xiMX ?(i, y)?

i?Nx P
B
xi

=MX ?(x�, y) (27)

MX ?(x, y) >
?

j?Ny P
B
yjMX ?(x, j)?

j?Ny P
B
yj

=MX ?(x, y�) (28)

Without loss of generality, suppose that (28) holds (otherwise,
we can prove the other way around). From (26), we have

?
i?Nx

PBxiMX ?(x, y) = 1 +
?
i?Nx

PBxiMX ?(i, y)

+
?
j?Ny

PByjMX ?(x, j)?
?
j?Ny

PByjMX ?(x, y). (29)

i.e.,

MX ?(x, y) = 1?
i?Nx

PBxi
+MX ?(x�, y)

+

?
j?Ny

PByj (MX ?(x, y�)?MX ?(x, y))?
i?Nx

PBxi

<
1?

i?Nx
wxi

+MX ?(x�, y). (30)

Now we claim thatMX ?(x, y) ? ?(x, y). Suppose it is not the
case. Let ? = maxx,y{MX ?(x, y)? ?(x, y)}. Among all the
pairs x, y realizing ?, choose any pair. It is clear that x 6= y,
since MX ?(x, x) = 0 ? ?(x, x). By (24) and (30),

MX ?(x, y) = ?(x, y) + ? = 1?
i?Nx wxi

+ ?(x�, y) + ?

? 1?
i?Nx wxi

+MX ?(x�, y)
> MX ?(x, y). (31)

This is a contradiction. From the definition of the poten-
tial function in (21), we have ?(x, y) < 2HPB (G). Thus
MX ?(G) ? ?(x, y) < 2HPB (G).

Now we are ready to complete the proof of Lemma 4. We
couple the Markov chains X and X ? so that they are equal until
the two random walkers become neighbors. Note that half of
the time in expectation almost surely when the walkers in X ?
meet, they do not meet in X , but stay in the same position.
We claim that MX (G) ? 2MX ?(G).

In the random process X ?, when two random walkers m, n
meet, instead of finishing the process, we let them exchange

positions and continue the random walks according to PX ?joint.
The expected length of each exchange is less than or equal to
MX ?(G). At each cross, the random process X finishes with
a probability of 1/2, independently. Thus for any x, y ? V we
have

MX (x, y) ?
??
i=1

(
1

2

)i
iMX ?(G) = 2MX ?(G). (32)

This completes the proof.

Let CTG(v) denote the expected time for a random walker
starting from node v to meet all other random walkers who
are also taking random walks on the same graph but starting
from different nodes. Define CT (G) = maxv?V CTG(v).
Lemma 5. Let MX (G) be the meeting time of the biased
random walk X defined in Section III-A. Then

CT (G) = O(MX (G) logN). (33)
Proof. Since there are no more than N consecutive meetings
with random walkers that it never meets, we can easily get a
union bound for CT (G), which is NMX (G).

In order to obtain a tighter bound for CT (G), we divide
the random walk into lnN periods of length kMX (G) each,
where k is a constant. Let a be the �special� random walker
trying to meet all other random walkers. For any period i and
any other random walker v, by the Markov inequality, we have

Pr(a does not meet v during period i) ? MX (G)
kMX (G) =

1

k
(34)

so

Pr(a does not meet v during any period) ?
(

1

k

)lnN
= N? ln k

(35)
If we take the union bound,

Pr(a doesn�t meet some walker during any period) ? N �N? ln k.
(36)

Conditioning on whether or not the walker a has met all
other walkers after all kMX (G) lnN steps, and using the
previous NMX (G) upper bound, we have

CT (G) ? kMX (G) lnN +N �N? ln k �NMX (G)
= kMX (G) lnN +N2?ln kMX (G) (37)

When k is sufficiently large, say k ? e6, the second term
is small, so

CT (G) < (k + 1)MX (G) lnN. (38)
This completes the proof.

C. An Upper Bound on Quantized Consensus

Recall that a non-trivial exchange in quantized consensus
happens when the difference in values at the nodes is greater
than 1.

Let Q(t) denote a vector of values all nodes holding at time
t. Set Q� = Qsum/N , where Qsum is defined in (1).



6
We construct a Lyapunov function LQ� [1], [11], [12] as:

LQ�(Q(t)) =

N?
i=1

(
Qi(t)? Q�

)2
. (39)

Let m = miniQi(0) and M = maxiQi(0). It is easy to
see that LQ�(Q(0)) ? (M?m)

2N
4 . Equality holds when half of

the values are M and others are m.

Lemma 6. In a non-trivial meeting,

LQ�(Q(t)) ? LQ�(Q(t+ 1)) + 2.

Proof. A non-trivial meeting follows the first update rule of
quantized consensus algorithm in Section II-A.

Suppose Qi(t) = x1 and Qj(t) = x2 have a non-trivial
meeting at time t, and the rest of the values stay unchanged.
Without loss of generality, let x1 ? x2 ? 2. We have

LQ�(Q(t))? LQ�(Q(t+ 1))
= x21 + x

2
2 ? (x1 + 1)2 ? (x2 ? 1)2

= 2(x2 ? x1)? 2 ? 2. (40)

Proof of Theorem 1. Corollary 6 shows that the Lyapunov
function is decreasing. The convergence of quantized con-
sensus must be reached after at most ? = (M?m)

2N
8 non-

trivial meetings. When every random walker has met each
of the other random walkers ? times, all the non-trivial
meetings must have finished. The rest of the proof follows
from Corollary 5, except we divide the random walks into
ln(?N/2) periods of length kMX (G) instead of lnN . By
Corollary 2, 4 and 5, this process finishes in O(N3 logN)
time.

Corollary 1. Given a network G of N nodes, an upper bound
for the expected convergence time of the quantized consensus
algorithm is O(MX (G) logN).
Proof. This is direct result from the proof of Corollary 5 and
Theorem 1.

For example, for a fully connected network C (i.e.complete
graph) with N nodes, at time t, the probability of any two
random walker i, j to meet is PB(C) = 2N(N?1) . It is easy
to get that MPB (C) = N(N?1)2 = O(N2). Thus by Corollary
1, the upper bound for the expected convergence time of the
quantized consensus algorithms is O(N2 logN). Note that this
result agrees with the analysis of convergence time of complete
graph in Section IV.A in [2], where the authors derived an
upper bound of O(N logN), regarding to local clock (See
Section 2 for definition of local clock and global clock). Since
every second, there are number of N clock ticks on average,
this is hence equivalent to a O(N2 logN) bound regarding
to the number of clock ticks in our case. More examples and
simulations can be found in [20].

IV. CONCLUSIONS

In this paper, we use the theory of electric networks,
random walks, and couplings of Markov chains to derive a
polynomial bound on convergence time with respect to the size
of the network, for a class of distributed quantized consensus
algorithms [1], [4]. We improve the state of art bound of
O(N5) for quantized consensus algorithms to O(N3 logN).
Our analysis can be extended to a tighter bound for certain
network topologies using the effective resistance analogy. Our
results provide insights to the performance of the quantized
consensus algorithms.

REFERENCES
[1] Akshay Kashyap, Tamer Bas�ar, and Ramakrishnan Srikant, �Quantized

consensus,� Automatica, vol. 43, no. 7, pp. 1192�1203, 2007.
[2] Moez Draief and Milan Vojnovic, �Convergence speed of binary interval

consensus,� SIAM Journal on Control and Optimization, vol. 50, no. 3,
pp. 1087�1109, 2012.

[3] Stephen Boyd, Arpita Ghosh, Balaji Prabhakar, and Devavrat Shah,
�Randomized gossip algorithms,� Information Theory, IEEE Transac-
tions on, vol. 52, no. 6, pp. 2508�2530, 2006.

[4] Florence Be�ne�zit, Patrick Thiran, and Martin Vetterli, �Interval con-
sensus: from quantized gossip to voting,� in Acoustics, Speech and
Signal Processing, 2009. ICASSP 2009. IEEE International Conference
on. IEEE, 2009, pp. 3661�3664.

[5] Javad Lavaei and Richard M Murray, �Quantized consensus by means
of gossip algorithm,� Automatic Control, IEEE Transactions on, vol. 57,
no. 1, pp. 19�32, 2012.

[6] Ruggero Carli, Fabio Fagnani, Paolo Frasca, and Sandro Zampieri,
�Gossip consensus algorithms via quantized communication,� Automat-
ica, vol. 46, no. 1, pp. 70�80, 2010.

[7] Paolo Frasca, Ruggero Carli, Fabio Fagnani, and Sandro Zampieri, �Av-
erage consensus by gossip algorithms with quantized communication,�
in Decision and Control, 2008. CDC 2008. 47th IEEE Conference on.
IEEE, 2008, pp. 4831�4836.

[8] Kai Cai and Hideaki Ishii, �Convergence time analysis of quantized
gossip algorithms on digraphs,� in Decision and Control (CDC), 2010
49th IEEE Conference on. IEEE, 2010, pp. 7669�7674.

[9] Kai Cai and Hideaki Ishii, �Average consensus on general strongly
connected digraphs,� Automatica, vol. 48, no. 11, pp. 2750�2761, 2012.

[10] Xiaojiang Du, Ming Zhang, Kendall E Nygard, and Sghaier Guizani,
�Self-healing sensor networks with distributed decision making,� Inter-
national Journal of Sensor Networks, vol. 2, no. 5, pp. 289�298, 2007.

[11] Angelia Nedic, Alex Olshevsky, Asuman Ozdaglar, and John N Tsit-
siklis, �On distributed averaging algorithms and quantization effects,�
Automatic Control, IEEE Transactions on, vol. 54, no. 11, pp. 2506�
2517, 2009.

[12] Minghui Zhu and Sonia Mart?�nez, �On the convergence time of
asynchronous distributed quantized averaging algorithms,� Automatic
Control, IEEE Transactions on, vol. 56, no. 2, pp. 386�390, 2011.

[13] David Aldous and Jim Fill, �Reversible markov chains and random
walks on graphs,� 2002.

[14] Julien M Hendrickx, Alex Olshevsky, and John N Tsitsiklis, �Distributed
anonymous discrete function computation,� Automatic Control, IEEE
Transactions on, vol. 56, no. 10, pp. 2276�2289, 2011.

[15] Lowell W Beineke and Robin J Wilson, Topics in algebraic graph
theory, vol. 102, Cambridge University Press, 2004.

[16] Chris Godsil and Gordon Royle, Algebraic Graph Theory, Springer,
2001.

[17] Satoshi Ikeda, Izumi Kubo, and Masafumi Yamashita, �The hitting
and cover times of random walks on finite graphs using local degree
information,� Theoretical Computer Science, vol. 410, no. 1, pp. 94�
100, 2009.

[18] Douglas J Klein and M Randic�, �Resistance distance,� Journal of
Mathematical Chemistry, vol. 12, no. 1, pp. 81�95, 1993.

[19] Don Coppersmith, Prasad Tetali, and Peter Winkler, �Collisions among
random walks on a graph,� SIAM Journal on Discrete Mathematics, vol.
6, no. 3, pp. 363�374, 1993.

[20] Shang Shang, Paul W Cuff, Pan Hui, and Sanjeev R Kulkarni, �An
upper bound on the convergence time for quantized consensus,� arXiv
preprint arXiv:1208.0788, 2012.


	I Introduction
	II Problem Statement
	II-A Quantized Consensus

	III Convergence Time Analysis
	III-A Definition and Notation
	III-B Hitting Time and Meeting Time on Weighted Graph
	III-C An Upper Bound on Quantized Consensus

	IV conclusions
	References

