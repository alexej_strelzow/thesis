
Wisdom of the Crowd: Incorporating Social
Influence in Recommendation Models

Shang Shang?, Pan Hui�, Sanjeev R. Kulkarni?, and Paul W. Cuff?
?Department of Electrical Engineering, Princeton University, Princeton NJ, 08540, U.S.A.

�Deutsche Telekom Laboratories, Ernst-Reuter-Platz 7, 10587 Berlin, Germany
?{sshang, kulkarni, cuff}@princeton.edu, �pan.hui@telekom.de

Abstract�Recommendation systems have received consider-
able attention recently. However, most research has been focused
on improving the performance of collaborative filtering (CF)
techniques. Social networks, indispensably, provide us extra
information on people�s preferences, and should be considered
and deployed to improve the quality of recommendations.

In this paper, we propose two recommendation models, for
individuals and for groups respectively, based on social contagion
and social influence network theory. In the recommendation model
for individuals, we improve the result of collaborative filtering
prediction with social contagion outcome, which simulates the
result of information cascade in the decision-making process.
In the recommendation model for groups, we apply social
influence network theory to take interpersonal influence into
account to form a settled pattern of disagreement, and then
aggregate opinions of group members. By introducing the concept
of susceptibility and interpersonal influence, the settled rating
results are flexible, and inclined to members whose ratings are
�essential�.

Index Terms�recommendation model, social influence, collab-
orative filtering

I. INTRODUCTION

With the rise of e-commerce, recommendation systems have
been studied intensively in the context of collaborative filtering
(CF) techniques [1]. Early generations of recommendation
system have already been commercialized and have achieved
great success. Recommendation systems serve as an important
component of online retail and Video on Demand (VoD) such
as Amazon and Netflix [5]. Recommenders give customized
recommendations to online users on books, movies, and com-
modities according to their previous preference data. In order
to improve the quality of recommendations, work has been
done mostly based on improving collaborative filtering tech-
niques. Most recommendation systems based on collaborative
filtering assume that users are independent, which ignores the
role of social influence in people�s buying decisions. Problems
such as �data sparsity�, �cold start�, �shilling attack� still
challenge the design of recommendation systems [1][3]. Social
networks and social influence, on the other side, can provide
us extra information on users� preferences, but have received
less attention. This may be partially due to the unavailability

This research was supported in part by the Center for Science of In-
formation (CSoI), an NSF Science and Technology Center, under grant
agreement CCF-0939370, by the U.S. Army Research Office under grant
number W911NF-07-1-0185, and by a research grant from Deutsche Telekom
AG.

of large-scale datasets to analyze in the past. Recently, the
emergence of Online Social Networks (OSN) provides us an
opportunity to reconsider the structure and effects of social
networks so as to improve recommendation results [3][7][18].
He et al. proposed a social network-based recommendation
system (SNRS) in [3], which is a probabilistic model for
personalized recommendations. SNRS is based on homophily
among friends but did not take social influence into consid-
eration and did not provide a solution for recommendation to
groups. Traditionally, recommendation systems are designed
to recommend items to individual users. However, there are
some buying-decisions/activities done by a group of users such
as going to a restaurant or watching a movie where group
recommendations are desired. Surprisingly, recommendation
to groups is a nontrivial extension of recommendation to
individuals. Unlike individual�s buying decisions which are
a personal choice (the user may be influenced by others�
opinions but there will be no compromise in the final decision),
preference of a group reflects not only each individual�s sub-
jective taste but also the knowledge of other group members�
opinions. Most of the current group recommendation systems
such as POLYLENS [15] and G.A.I.N. [16] focus on different
ways to aggregate preference. However, the problem of how
to make recommendations for a group with the consideration
of interpersonal influence among group members rather than
viewing each individual�s preference separately is often ig-
nored.

In this paper, we propose two mathematical models of
recommendation systems, for individuals and for groups re-
spectively, based on social contagion and social influence
network theory. In the past few years, study on social con-
tagion finds applications in viral marketing, which uses pre-
existing social networks to promote new products or brands
to market, without the knowledge of users� preference records
[20]. Given the information of users� social networks and
preference data, we use a modified linear threshold contagion
model to simulate the social influence by word of mouth,
and add this effect to collaborative filtering results in order
to provide more effective recommendations to individuals. In
the model of recommendations to groups, in order to consider
people�s �compromised disagreement� among group members,
we introduce social influence under sufficient interpersonal
communication to give recommendations that reflect more than
personal taste.

ar
X

iv
:1

20
8.

07
82

v2
  [

cs
.IR

]  
17

 M
ay

 20
13



In a typical setting, there is a list of m users U =
{u1, u2, ..., um} and a list of n items I = {i1, i2, ..., in}. Each
user uj has a list of items Iuj , which the user has rated or from
which the user�s preferences can be inferred. The ratings can
either be explicit, for example, on a 1-5 scale as in Netflix, or
implicit such as purchases or clicks. These data form a m�n
rating matrix. G = (U , E) is a social network, represented by
an undirected graph, where U is a set of nodes and E is a set
of edges. For all u, v ? U , (u, v) ? E if u, v are �friends�, in
which case v ? N(u) and u ? N(v), where N(u) is the set
of neighbors of u. We want to make recommendations for a
target user or a group of users given the above information.

The rest of this paper is organized as follows. We discuss
related work in Section II. We propose our social influential
recommendation models for individuals and for groups in
Section III and Section IV, followed by conclusions and future
work in Section V.

II. RELATED WORK

A few recent papers used information of users� social
networks as a component in their recommendation models.
The work in [4] used friendship matrix to modify the prob-
abilistic matrix factorization proposed in [5]. In [6], Debnath
et al. constructed a social network graph with items as nodes,
which represents human judgement of similarity between
items aggregated over a large population of users to estimate
feature weights for the content-based recommendations. The
work in [7] studied how collaboration should be done in
a recommendation system based on a network of �personal
agents� . The most closely related work is found in [18] and
[3]. The work in [18] proposed to reduce computational cost
by limiting similar users to target user�s immediate friends.
The work in [3] considered the effects of homophily among
friends in a recommendation system and built a probabilistic
model to describe this homophily effect. In [3], the author
assumed that item attributes, user attributes and ratings of
immediate friends are independent of each other and that for
each pair of immediate friends, their ratings on the same item
are identical but with an error term following the distribution
of the histogram of previous rating differences. There are,
to the best knowledge of the authors�, no previous work
considering social influence, which plays an important role in
people�s buying decisions. We use a social contagion model to
add the effects of information cascade to the target user in the
buying decision, in order to improve the result of collaborative
filtering prediction.

In addition to recommendation for individuals, there are
some circumstances that recommendation for a group of
users are desired. There are mainly three categories of rec-
ommendation systems for groups [14]: (1) merging sets of
recommendations, e.g. taking intersection among all members�
top-k preference; (2) aggregation of individuals� ratings for
particular items; (3) construction of group preference models.
POLYLENS [15], a system to recommend movies to a group
of users, merged the results of individual recommendations.
While Yu et al. [17] introduced a group preference model

which merged individual profiles, using a minimization of total
Dalal�s distance, and took this virtual profile as a target user
for recommendations. In this paper, we utilize social influence
network theory [8] as a tool to model the opinion formation
of a group of users under interpersonal influences within the
group.

III. RECOMMENDATIONS FOR INDIVIDUALS
Traditional collaborative filtering systems suppose that users

are all independent. However, in reality, social influence plays
a crucial role in people�s buying decisions. For example,
a user, Jack, is browsing on Amazon to choose a leisure
book to read, with nothing particular in mind. In the list of
new collections, Jack vaguely remembers that his friend Lisa
mentioned book A in the list. However, it is a romance book,
which is not his type. His eyes then are caught by two other
books B and C. B, a thriller, was strongly recommended by
his friends Henry and James, and C, though, he has never
heard of it, also looks interesting. Finally, Jack puts C in wish
list and decides to buy book B first. If we look closely at
this scenario, and think about how we make our decisions
everyday, we can see that what drives us to buying decisions
are not only our own preferences but also some informative
comments made by friends. In addition, studies show that
two persons connected via a social relationship tend to have
similar tastes, which is known as �homophily principle� [3].
Social influence and social networks thus can help us predict a
target user�s preferences and decisions. In this section, we will
first briefly introduce a prevalent memory-based collaborative
filtering algorithm as a baseline algorithm, and then describe
our social influential recommendation model for individual
users in detail.

A. Collaborative Filtering

Collaborative filtering (CF) is one of the most successful
approaches to build a recommendation system. It uses the
known preferences of users to make recommendations or
predictions to the target user [1].

1) Similarity Computation: There are a variety of simi-
larity measures. A generally adopted one is called Pearson
Correlation which measures the extent to which two variables
linearly relate with each other [2]. For user-based algorithm,
the Pearson Correlation between user u and v is

wu,v =

?
i?I(ru,i ? r�u)(rv,i ? r�v)??

i?I(ru,i ? r�u)2
??

i?I(rv,i ? r�v)2
, (1)

where i ? I is the item rated by both users u and v, ru,i is
the rating of user u on item i, and r�u is the average rating of
user u in co-rating set I .

2) Prediction Computation: Prediction Computation is the
most important step in a collaborative filtering system [1]. We
can use the weighted sum to predict the rating P cfu,i for target
user u on a certain item i as proposed in [2]:

P cfu,i = r�u +

?
v?U (rv,i ? r�v) � wu,v?

v?U |wu,v|
. (2)



Recommenders based on collaborative filtering then refer to
this prediction to provide top-k recommendations to the user or
simply display the personalized predicted rating of each item.
Unfortunately, they ignore the dependence and influence in
social networks in users preferences. Social Contagion Model
provides us an alternative solution to deal with this problem
by taking these effects into consideration.

B. Social Contagion Model

The flow of information through a social network can be
thought of as unfolding with the dynamics of an epidemic
[11]. This information from social relationships has potential
influence in people�s final buying decision. We use a linear
threshold social contagion model [10][11] to make recommen-
dations with respect to this natural social contagion and user�s
subjective taste. We will start with a simple case that users use
�like� and �dislike� to describe their personal preferences and
explain the model in the perspective of game theory. We will
then discuss the multiple-scale rating case in Section III-B2.

1) A Simple Binary Rating Case: In a binary rating system,
we assume that users have three possible states namely like,
dislike and inactive. We use �1�, �-1� and �inactive-0� to label
them respectively. A node v is influenced by each neighbor
w according to a weight/influence factor, bv,w ? [0, 1] such
that

?
w?N(v) bv,w = 1. Intuitively, bv,w is related to trust

and interpersonal communication frequency. If we lack such
knowledge, we can randomly partition the unit influence
among neighbors of a user, which in fact simulates the
randomness of information cascade. The process proceeds as
follows: each node v chooses a threshold ?v,i for item i from
the interval [0, 1], representing the weighted fraction of v�s
neighbors that must become active (either like or dislike in
order for v to become active to state like or dislike on item
i). Since ?v,i indicates the latent tendency of nodes to adopt
the opinion, we naturally associate it with v�s CF prediction
on item i. E.g. we can set it as in Equation 4. Two other
classes of approaches are setting all thresholds uniformly at
random from the interval [0,1] or at a known value like 1/2.
Given a threshold, and an initial set of active nodes Ai (users
with non-empty ratings on a certain item i), this progressive
diffusion process proceeds deterministically.

Time operates in discrete steps t = 1, 2, 3, . . . . At a given
time t, any inactive node v becomes active if its fraction of
active neighbors exceeds its threshold:??????

?
w?N(v)

bvw � Sw,i

?????? ? ?v,i, (3)
where Sw,i is the state of node w on item i, and ?v,i is the
influential threshold.

?v,i =

{
P cfv,i, if P

cf
v,i �

(?
w?N(v) bvw � Sw,i

)
< 0

min{P cfv,i, 1? P cfv,i}, otherwise
. (4)

The intuition in Equations 3 and 4 is that if receiving too
many controversial opinions from neighbors, the node will

fail to be activated to either state. Equation 4 states that
similar opinions to users� expectations are easier and opposite
opinions are harder to be taken.

The state of newly activated node v under social influence
is decided by,

Sv,i = sign

?? ?
w?N(v)

bvw � Sw,i
?? (5)

This in turn may cause other nodes to become active
in subsequent time steps, leading to potentially cascading
adoption behaviors of �like� or �dislike�. The process runs
until no more activations are possible.

For the target user u,

P siu,i =

{
Su,i if u is activated
inactive-0 otherwise . (6)

We note that if assuming the time interval during which a
user decides to buy the item recommended is short, then we
only need to consider the influence of the user�s immediate
friends under a unit time step or two steps, which means the
system only requires local social network information.

We can view this as a networked cooperation game [12].
Each node in the social network has three possible opinions
on item i: like, dislike, and inactive. Because of the homophily
effect of social networks, if two nodes are connected in the
social network, there is an incentive for them to have their
opinions match. We represent the payoff matrix as in Table I.

TABLE I: Payoff for the three-strategy coordination game

w
like(1) dislike(-1) inactive(0)

v
1 avw, awv ?avw,?awv ?f(P cfv,i, 1), 0

-1 ?avw,?awv avw, awv ?f(P cfv,i,?1), 0
0 0,?f(P cfw,i, 1) 0,?f(P cfw,i,?1) 0, 0

Each node are playing many copies of this game with
all its neighbors at the same time. avw > 0 represents the
payoff/penalty node v receives if it coordinates/discoordinates
with node w, and f(P cfv,i, �) > 0 represents the penalty for
node v playing active strategy if its neighbor is inactive. The
total payoff is the sum of all payoffs of individual games. Then
the payoffs for node v are:

� Payoff1 =
?

w?N(v)

(
avwSw,i ? f(P cfv,i, 1)1{Sw,i=0}

)
� Payoff?1 =

?
w?N(v)

(
?avwSw,i ? f(P cfv,i,?1)1{Sw,i=0}

)
� Payoff0 = 0

If node v chooses the strategy which provides maximum
payoff and f(P cfv,i, �) is reversely proportional to the number
of inactive nodes, the solution matches our social contagion
model.



(a) Threshold = 0.1. (b) Threshold = 0.5. (c) Threshold uniformly drawn from 0.05 to 0.8.

(d) Threshold = 0.1. (e) Threshold = 0.5. (f) Threshold uniformly drawn from 0.05 to 0.8.

Fig. 1: Simulations for binary rating social contagion process in a small-world social network of 1000 nodes.

2) General Case: For a 1?to?R scale rating system,
we pair up like�s and dislike�s in different levels. De-
note R = {1, 2, � � � , R} as the set of ratings and S =
{�1,�2, � � � ,�bR2 c} as the set of active state of different
levels of like�s and dislike�s. �active-0� ? S if R is odd. Define
the mapping function fr : R ? S

fr(r) = r ? r�, (7)
where

r� =

???
dR2 e if R is odd
R
2 if R is even and r >

R
2

R
2 + 1 if R is even and r ? R2

.

E.g., for a 1-5 scale rating system, we assign �-2�, �-1�,
�active-0�, �1�, �2� respectively on ratings 1 through 5. At
each time step, let

S = arg max
s?S

????????
?

w?N(v)
|Sw,i|=s

bvw � sign (Sw,i)

???????? , (8)

Sv,i =

???????
inactive-0, if |?w?N(v)

|Sw,i|=S
bvw � sign (Sw,i) | < ?v,i

S � sign
(?

w?N(v)
|Sw,i|=S

bvw � Sw,i
)
, otherwise

.

(9)
?v,i in Equation 9 is the influential threshold.

Here we assume that the mean-state �active-0� does not
provide valuable information thus it cannot influence other
nodes. We can easily change this assumption by making a
minor modification in Equation 8 and 9. The social influential
prediction on a target user u is

P siu,i =

{
f?1(Su,i) if u is activated
inactive-0 otherwise . (10)

3) Simulation Results: We simulate the social contagion
process of binary rating model as shown in Fig.1. We construct
a social network generated by Watts and Strogatz model [13]
of 1000 nodes, and assign different thresholds for the nodes
and analyze the convergent rate and speed. Initially, active
nodes are in State like with probability 0.7 and dislike with
probability 0.3. We assign influential factors by a uniform
random partition of a unit influence among neighbors of a
node. Results are the average of 500 independent simulations.
When the threshold is low, as in Fig.1a and Fig.1d, even a very
small portion of initial active nodes can activate almost all the
inactive nodes in the network, into both majority state like
and minority state dislike. With the increasing number of the
initial active nodes, the newly activated nodes dominatingly
choose the majority state and the number of iterations to
convergence decreases. This shows that in a susceptible social
network, social contagion is likely to progressively influence
all the nodes. When we increase threshold to 0.5, as shown in
Fig.1b and Fig.1e, social contagion process converges faster,



but small ratio of initial active nodes cannot influence the
network. Fig.1c and Fig.1f show a more realistic setting of
the threshold: each node�s threshold is drawn randomly from a
uniform distribution from 0.05 to 0.8, representing the various
susceptibility of different people. When 20% of nodes are
initially activated, more than half of the unactivated nodes will
become active to the majority states eventually, this rate stays
stable when the ratio of initial active nodes increases, while
the convergence speed increases. Our simulations show that
in a general setting, the result of social contagion process is
users� local estimation of majority opinions with the penalty
from the disagreement with their own estimation. This also
follows our coordination game explanation. For more general
case, we expect that the ratings of newly activated nodes to be
clustered according to their social network structure, generally
following the majority opinion.

C. Discussions on Recommendation to Individuals

Define susceptibility factor ?u ? [0, 1], which is the
attribute of a user. The more susceptible the user is, the higher
the value of ?u should be. The recommendation prediction
Pu,i to a target user u on a certain item i is calculated by

Pu,i =

{
P cfu,i if P

si
u,i = inactive-0

(1? ?u)P cfu,i + ?uP siu,i otherwise
(11)

Then we can recommend the top-k predicted items to the
target user. In particular, if we set ?u = 1 for ?u, Equation
11 becomes

Pu,i =

{
P cfu,i if P

si
u,i = inactive-0

P siu,i otherwise
(12)

If we ignore the inactive ratings, the recommendation model
above becomes a recommender based on ratings by users�
immediate and distant friends. It can be a supplement of
the existing recommendation systems and provide users extra
information of the opinions from someone they would trust.

IV. RECOMMENDATIONS FOR GROUPS

To show how our group recommendation model differs from
aggregating each user�s predictions or merging preference
profiles, let us begin with a brief example: Jessica and Mike
want to see a movie with Eric, a friend visiting them. When
choosing the movie to watch, the couple would like to follow
their guest�s opinion. Eric, accommodating as he is, also tries
to take Jessica and Mike�s taste into consideration. After some
discussion, they finally agree on a movie that Eric has wanted
to see for a long time, and also is interesting to both Jessica
and Mike. We will now introduce a social influence model to
describe Jessica, Mike and Eric�s opinion evolution in their
discussion.

A. Social Influence Model

Social influence theory [8][9] describes how a network
of interpersonal influence enters into the process of opinion

formation, which postulates a recursive definition for the
interpersonal influence in a group of N users:

P
(t)
i = AWP

(t?1)
i + (I ?A)P (1)i , (13)

for t = 2, 3, . . .. P (1)i is an N � 1 vector of users� initial
opinions on an item i, it can be either existing individual rating
ru,i or preference prediction Pu,i, or a mixture of both. P

(t)
i

is an N � 1 vector of users� opinions at time t. W = [wuv]
is an N � N matrix of interpersonal influences with 0 ?
wuv ? 1,

?N
j wuv = 1, and A = diag(?11, ?22, ..., ?NN )

is an N � N diagonal matrix of users� susceptibilities to
interpersonal influence on item i with 0 ? ?uu ? 1. This
susceptibility factor is a user�s attribute and by default it is
associated with ?u in Equation 11, but can also be set by
users. When A = 0, meaning no group members are willing
to change their own opinions in response to other group
members� tastes. In that case, P (t)i = P

(1)
i . This is normally

what a traditional group recommender assumes. However,
although individuals in a group may not necessarily reach
consensus, they tend to coordinate their decisions through the
effect of social influence [20]. This is the initial motivation that
we apply social influence network theory to recommendation
model for groups.

When we consider the process in an equilibrium state
(assuming convergence), Equation 13 becomes

P
(?)
i = AWP

(?)
i + (I ?A)P (1)i . (14)

If I ?AW is nonsingular, then
P

(?)
i = V P

(1)
i , (15)

where
V = (I ?AW )?1(I ?A). (16)

V is a matrix describing the total interpersonal influence
that affect a user�s evolution on a certain item. P (?)i is an
N � 1 vector describing users� final preference prediction on
a certain item i under social influence. This can be viewed as
a compromised disagreement after sufficient consideration on
N ? 1 other group members� opinions. Then we can follow
the normal regime, such as average rating, to aggregate users�
opinions to form a recommendation list for the group.

For example, in the scenario at the beginning of this section,
Jessica, Mike and Eric may set A = diag(0.9, 0.9, 0.5) and
W = [0.1 0.1 0.8; 0.1 0.1 0.8; 0.25 0.25 0.5]. For movie A,
suppose their individual predicted ratings are P (1)A = (2, 3, 5).
In above setting, by Equation 15 and 16, we have P (?)A =
(4.52, 4.62, 4.86). Now the average rating of A for the group
is 4.66 instead of 3.33 if averaging directly from P (1)A . We can
see that the result is inclined to Eric as it initially assumed.

B. Discussions on Possible Applications for Group Recom-
mendation

The recommendation model proposed in Section IV-A can
be applied and added to various recommendation systems with



different settings of interpersonal influence factor W and sus-
ceptibility factor A. For instance, for restaurant recommenda-
tion, it is likely that group members are willing to go to a good
restaurant even though some of them have already dined there
before. Thus original opinions P (1)i in Section IV-A can be a
combination of existing ratings and predicted ratings, and with
a higher susceptibility for the latter. On the other hand, for trip
recommendation and movie recommendation, group members
are likely to explore something unknown to everybody, thus
P

(1)
i are set as original predictions of individual preference.

In both cases, the recommendation model provides a way to
take interpersonal influence within a group into account before
preference aggregation procedure.

C. Social Contagion Model vs. Social Influence Model

Recommendation to individuals provides items that a target
user is most likely to be interested in. Social contagion model
in Section III is a probabilistic framework that simulates
how an opinion on a certain item spreads through the social
network. However, the buying decision made by the user is
a personal choice: the user may listen to others� opinions but
there will be no compromise in the final decision. In addition,
the active nodes at the beginning are users who have already
made their explicit or implicit statement of preferences and
will not change with time. Since the time for the target user to
decide whether to buy the recommended item is usually short,
it is reasonable to model the social influence to individual
users as a progressive process. On the other hand, a single
decision for a group of people is not only based on fairness,
as in most of the current group recommenders, but also based
on group members� susceptibility and influence power. We
assume group members will or are willing to modify their
choices according to other group members� opinions, and it
is usually the case in social activities. Social influence model
shows us the compromised disagreement formed iteratively by
interpersonal influence.

V. CONCLUSIONS AND FUTURE WORK

In this paper, we propose two social influential recommen-
dation models, for individuals and for groups respectively.
The individual recommendation is based on social contagion,
which takes the effect of homophily among friends and
effect of word of mouth into consideration while making
recommendations. In the recommendation model for groups,
we provide preference specifications that not only reflect
subjective personal taste but also opinion evolution with the
knowledge of other members� preferences.

For future work, we plan to validate our models through
a real online social network dataset from Yelp.com, which
provides users� ratings of restaurants, spas, etc. Yelp also
provides social network feature which is ideal to compare
our model with the performance of collaborative filtering
recommendation and some existing recommendation systems
based on social networks such as SNRS [3]. We also want
to use machine learning techniques to cluster different types
(e.g. subjective, susceptible, hybrid, etc. ) of users, and explore

the well-performing settings for each type on threshold ?u,i,
susceptibility A and interpersonal influence weight W . Fur-
thermore, we want to apply our group recommendation model
in different application areas such as movie recommendation,
restaurant application, etc.

REFERENCES
[1] Xiaoyuan Su and Taghi M. Khoshgoftaar, �A Survey of Collaborative

Filtering Techniques�, Advances in Artificial Intelligence, vol. 2009,
Article ID 421425, 19 pages, 2009. doi:10.1155/2009/421425.

[2] P. Resnick, N. Iacovou, M. Suchak, P. Bergstrom, and J. Riedl, �Grou-
plens: an open architecture for collaborative filtering of netnews�, in
Proceedings of the ACM Conference on Computer Supported Cooperative
Work, pp. 175-186, New York, NY, USA, 1994.

[3] Jianming He and Wesley W. Chu, �A Social Network Based Recom-
mender System�, Annals of Information Systems: Special Issue on Data
Mining for Social Network Data (AIS-DMSND), 2010.

[4] Jorge Aranda, Inmar Givoni, Jeremy Handcock and Danny Tarlow,
�An Online Social Network-Based Recommendation System�, Technical
report.

[5] Ruslan Salakhutdinov and Andriy Mnih, �Probablistic Matrix Factoriza-
tion Applied to the Netflix Rating Prediction Problem�, NIPS, 2007.

[6] Souvik Debnath, Niloy Ganguly and Pabitr Mitra, �Feature Weighting in
Content Based Recommendation System Using Social Network Analy-
sis�, WWW, Beijing, 2008.

[7] Jordi Palau , Miquel Montaner , Beatriz Lpez , Josep Llus De La
Rosa, �Collaboration Analysis in Recommender Systems Using Social
Networks�, Cooperative Information Agents VIII: 8th International Work-
shop, CIA 2004. Volume 3191 of Lectures Notes in Computer Science.

[8] Noah E. Friedkin and Eugene C. Johnsen, �Social Influence Networks
and Opinion Change�, Advances in Group Processes, vol. 16, pp.1-29,
1999

[9] Pan Hui and Sonja Buchegger, �Groupthink and Peer Pressure: Social
Influence in Online Social Network Groups�, In Proceeding International
Conference on Advances in Social Networks Analysis and Mining
(ASONAM), Athens, Greece, July, 2009.

[10] David Kempe, Jon Kleinberg and Eva Tardos, �Maximizing the Spread
of Influence through a Social Network�, In Proc. 9th ACM SIGKDD Intl.
Conf. on Knowledge Discovery and Data Mining, 2003.

[11] Jon Kleinberg, �Cascading Behavior in Networks: Algorithmic and Eco-
nomic Issues�, In Algorithmic Game Theory (N. Nisan, T. Roughgarden,
E. Tardos, V. Vazirani, eds.), Cambridge University Press, 2007.

[12] David Easley and Jon Kleinberg, �Networks, Crowds, and Markets:
Reasoning About a Highly Connected World�, Cambridge University
Press, pages 497-517, 2010.

[13] Duncan J. Watts and Steven Strogatz, �Collective dynamics of �small-
world� networks�, Nature 393 (6684): 40910, 1998.

[14] Anthony Jameson and Barry Smyth, �Recommendation to Groups�, The
Adaptive Web (2007), pp. 596-627. doi:10.1007/978-3-540-72079-9-20.

[15] Mark OConnor, Dan Cosley, Joseph A. Konstan, and John
Riedl,�PolyLens: A Recommender System for Groups of Users�, Eu-
ropeon Conference on Computer Supported Co-Operative Work, Bonn,
Germany. pages199-218, 2001.

[16] S. Pizzutilo, B. De Carolis, G. Cozzolongo, and F. Ambruoso, �Group
Modeling in a Public Space: Methods, Techniques, Experiences�, Pro-
ceedings of the 5th WSEAS International Conference on Applied Infor-
matics and Communications, pages 175-180, 2005.

[17] Yu, Zhiwen and Zhou, Xingshe and Hao, Yanbin and Gu, Jianhua, �TV
Program Recommendation for Multiple Viewers Based on user Profile
Merging�, User Modeling and User-Adapted Interaction, volume16(1),
pages 63�82, 2006.

[18] Rong Zheng, Foster Provost and Anindya Ghose, �Social Network
Collaborative Filtering: Preliminary Results�, In Proceedings of the Sixth
Workshop on eBusiness(WEB2007), Dec. 2007

[19] Elchanan Mossel and Sebastien Roch, �On the submodularity of influ-
ence in social networks�, In Proc. 39th ACM Symposium on Theory of
Computing, 2007.

[20] D. Cosley, D. Huttenlocher, J. Kleinberg, X. Lan, and S. Suri, �Se-
quential Influence Models in Social Networks�, In Proc. 4th International
AAAI Conference on Weblogs and Social Media, 2010


	I Introduction
	II Related Work
	III Recommendations for individuals
	III-A Collaborative Filtering
	III-A1 Similarity Computation
	III-A2 Prediction Computation

	III-B Social Contagion Model
	III-B1 A Simple Binary Rating Case
	III-B2 General Case
	III-B3 Simulation Results

	III-C Discussions on Recommendation to Individuals

	IV Recommendations for Groups
	IV-A Social Influence Model
	IV-B Discussions on Possible Applications for Group Recommendation
	IV-C Social Contagion Model vs. Social Influence Model

	V Conclusions and future work
	References

