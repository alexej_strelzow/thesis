
ar
X

iv
:c

s/0
60

10
89

v1
  [

cs
.L

G]
  2

0 J
an

 20
06

1

Distributed Kernel Regression: An Algorithm for
Training Collaboratively

J. B. Predd, Member, IEEE, S. R. Kulkarni, Fellow, IEEE, and H. V. Poor, Fellow, IEEE

Abstract� This paper addresses the problem of distributed learn-
ing under communication constraints, motivated by distributed signal
processing in wireless sensor networks and data mining with dis-
tributed databases. After formalizing a general model for distributed
learning, an algorithm for collaboratively training regularized ker-
nel least-squares regression estimators is derived. Noting that the
algorithm can be viewed as an application of successive orthogonal
projection algorithms, its convergence properties are investigated and
the statistical behavior of the estimator is discussed in a simplified
theoretical setting.

I. INTRODUCTION
In this paper, we address the problem of distributed learning un-

der communication constraints, motivated primarily by distributed
signal processing in wireless sensor networks (WSNs) and data
mining with distributed databases. WSNs are a fortiori designed to
make inferences from the environments they are sensing; however
they are typically characterized by constraints on energy and
bandwidth, which limit the sensors� ability to share data with each
other or with a centralized fusion center. In data mining with
distributed databases, multiple agents (e.g., corporations) have
access to possibly overlapping databases, and wish to collabo-
rate to make optimal inferences; privacy or security concerns,
however, may preclude them from fully sharing information.
Nonparametric methods studied within machine learning have
demonstrated widespread empirical success in many centralized
(i.e., communication unconstrained) signal processing applica-
tions. Thus, in both the aforementioned applications, a natural
question arises: can the power of machine learning methods be
tapped for nonparametric inference in distributed learning under
communication constraints?

In this paper, we address this question by formalizing a general
model for distributed learning, and then deriving a distributed
algorithm for collaborative training in regularized kernel least-
squares regression. The algorithm can be viewed as an instan-
tiation of successive orthogonal projection algorithms, and thus,
insight into the statistical behavior of these algorithms can be
gleaned from standard analyses in mathematical programming.

A. Related Work
Distributed learning has been addressed in a variety of other

works. Reference [9] considered a PAC-like model for learning
with many individually trained hypotheses in a distribution-
specific learning framework. Reference [13] considered the clas-
sical model for decentralized detection [17] in a nonparamet-
ric setting. Reference [15] studied the existence of consistent

This research was supported in part by the Army Research Office under Grant
DAAD19-00-1-0466, in part by Draper Laboratory under Grant IR&D 6002, in
part by the National Science Foundation under Grants CCR-0020524 and CCR-
0312413, and in part by the U. S. Army Pantheon Project.

The authors are with the Department of Electrical Engineering, Princeton
University, Princeton, NJ 08540 USA (email: jpredd@princeton.edu, kulka-
rni@princeton.edu, poor@princeton.edu)

estimators in several models for distributed learning. From a
data mining perspective, [6] and [12] derived algorithms for
distributed boosting. Most similar to the research presented here,
[7] presented a general framework for distributed linear regression
motivated by WSNs.

Ongoing research in the machine learning community seeks
to design statistically sound learning algorithms that scale to
large data sets (e.g., [3] and references therein). One approach
is to decompose the database into smaller �chunks�, and sub-
sequently parallelize the learning process by assigning distinct
processors/agents to each of the chunks. In principle, algorithms
for parallelizing learning may be useful for distributed learning,
and vice-versa. To our knowledge, there has not been an attempt
to parallelize reproducing kernel methods using the approach
outlined below.

A related area of research lies in the study of ensemble meth-
ods in machine learning; examples of these techniques include
bagging, boosting, and mixtures of experts (e.g., [5] and others).
Typically, the focus of these works is on the statistical and
algorithmic advantages of learning with an ensemble and not on
the problem of learning under communication constraints. To our
knowledge, the methods derived here have not been derived in
this related context, though future work in distributed learning
may benefit from the many insights gleaned from this important
area.

Those familiar with the online learning framework may find
our collaborative training algorithm reminiscent of the equations
for additive gradient updates [11]. Though both algorithms may
be interpreted in the context of successive orthogonal projection
algorithms, it does not appear possible to specialize the current
model for distributed learning in a way that recovers the online
learning framework (or vice versa).

The research presented here generalizes the model and algo-
rithm discussed in [14], which focused exclusively on the WSN
application. Distinctions between the current and former work are
discussed in more detail below.

B. Organization
The remainder of this paper is organized as follows. In Section

II, we review preliminary background information necessary for
the remainder of the work. In Section III, we describe a general
model for distributed learning and propose a distributed algo-
rithm for collaboratively training regularized kernel least-squares
regression estimators. Subsequently, we analyze the algorithm�s
convergence properties and use these properties to gain insight
into the statistical behavior of the estimator in a simplified setting.
We conclude with a discussion of the method in Section IV.

II. PRELIMINARIES
In this section, we briefly review the supervised learning model

for nonparametric least-squares regression, reproducing kernel




2
methods, and alternating projection algorithms. Since a thorough
introduction to these models and methods is beyond the scope of
this paper, we refer the reader to standard references on the topics;
see, for example, [4], [8], [16] and references therein.

A. Nonparametric Least-squares Regression
Let X and Y be X and Y-valued random variables, respectively.

X is known as the feature, input, or observation space; Y is known
as the label, output, or target space. For now, we allow X to be
arbitrary, but take Y = IR. In the least-squares estimation problem,
we seek a decision rule mapping inputs to outputs that minimizes
the expected squared error. In particular, we seek a function g :
X ? Y that minimizes

E{|g(X)? Y |2}.

It is well-known that ?(x) = E{Y |X = x} is the loss minimizing
rule. However, without prior knowledge of the joint distribution
of (X,Y ), this regression function cannot be computed. In the
supervised learning model, one is instead provided a database S =
{(xi, yi)}

n
i=1 of training examples with (xi, yi) ? X � Y ?i ?

{1, . . . , n}; the learning task is to use S to estimate ?(x).

B. Regularized Kernel Methods
Regularized kernel methods [16] offer one approach to nonpara-

metric regression. In particular, let HK denote the reproducing
kernel Hilbert space (RKHS) induced by a positive semi-definite
kernel K(�, �) : X�X ? IR; let ?�?HK denote the norm associated
with HK . In practice, the kernel K is a design parameter, chosen
as a similarity measure between inputs to reflect prior application-
specific domain knowledge. The regularized kernel least-squares
estimate is defined as the solution f? ? HK of the following
optimization problem:

min
f?HK

[ n?
i=1

(f(xi)? yi)
2 + ??f?2HK

]
. (1)

The statistical behavior of this estimator is well-understood
under various assumptions on the stochastic process that generates
the examples {(xi, yi)}ni=1 [16], [19]. In this paper, we focus
primarily on algorithmic aspects of computing a solution to (1) (or
an approximation thereof) in distributed environments. To this end,
consider the following �Representer Theorem� proved originally
in [10].

Theorem 1 ([10]): Let f? ? HK be the minimizer of (1). Then,
there exists c? ? IRn such that

f?(�) =

n?
i=1

c?,iK(�, xi).

From a computational perspective, the result is significant because
it states that while the objective function (1) is defined over a
potentially infinite dimensional Hilbert space, its minimizer must
lie in a finite dimensional subspace.

Finally, note that (1) can be naturally interpreted as an orthog-
onal projection. In particular, by introducing an auxiliary vector
z ? IRn, (1) can be rewritten as the following optimization
program:

min ?z? y?22 + ??f?
2
HK

(2)
s.t. zi = f(xi) ?i ? {1, ..., n} (3)

z ? IRn

f ? HK .

Through the constraints in (3), (1) and (2) are equivalent in the
following sense: if f? is the minimizer of (1) and (z?, f ??) is
the solution of (2), then f ?? = f?. Therefore, through (2), we
can interpret the regularized kernel least-squares estimator as a
projection of the vector (y, 0) ? IRn �HK onto the set{
(z, f) ? IRn�HK : zi = f(xi) ?i ? {1, ..., n}

}
? IRn�HK .

This simple observation will recur in the sequel.

C. Alternating Projections Algorithms
Let X be a Hilbert space with a norm denoted by ? � ?. Let

C1, . . . , Cm be closed convex subsets of X whose intersection
C = ?mi=1Ci is nonempty. Let PC(x�) denote the orthogonal
projection of x� ? X onto C, i.e.,

PC(x�) , argmin
x?C

?x? x�?.

Define PCi(x�) analogously.
Successive orthogonal projection (SOP) algorithms [4] provide

a natural way to compute PC(�) given {PCi(�)}mi=1. For example,
the (unrelaxed) SOP algorithm is defined as follows:

x0 := x� xn := PC(n mod m)+1(xn?1). (4)
In words, the algorithm successively and iteratively projects

onto each of the subsets. In the case where Ci is a linear subspace
for all i ? {1, . . . ,m}, this algorithm was first studied by von
Neumann [18]. Often examined in the context of the convex
feasibility problem, SOP has been generalized in various ways
[4], to address more general convex sets and non-orthogonal (e.g.,
Bregman) projections; accordingly, the algorithm often takes on
other names (e.g., the von Neumann-Halperin algorithm, Breg-
man�s algorithm). Much of the behavior of this algorithm can
be understood through Theorem 2; the proof of this fundamental
result can be found in [2].

Theorem 2: Let {Ci}mi=1 be a set of closed, convex subsets of
X whose intersection C = ?mi=1Ci is nonempty. Let xn be defined
as in (4). Then, for every x ? C and every n ? 1,

?xn ? x? ? ?xn?1 ? x?.

Moreoever, limn?? xn ? ?mi=1Ci. If Ci are affine for all i ?
{1, ...,m}, then limn?? ?xn ? PC(x�)? = 0.

III. DISTRIBUTED KERNEL REGRESSION
A. The Model

In contrast to the model for supervised learning reviewed in
Section II, suppose that each member of a collection of m
learning agents has limited access to the training database S =
{(xi, yi)}

n
i=1. In particular, assume that learning agent i has access

only to the training examples in subset Si ? S. For convenience,
we shall henceforth refer to {Si}mi=1 as an ensemble.

A bipartite graph is a convenient way to represent an ensemble
in this model for distributed regression. As depicted in Figure 1,
nodes on the top-level of the graph represent learning agents;
nodes on the bottom-level represent training examples. An edge
between a learning agent i and a training sample j signifies that
agent i has access to example j, i.e., (xj , yj) ? Si. For now,
we make no additional assumptions on the structural relationship
between the agents� locally accessible training sets; for example,



3
we do not require the ensemble {Si}mi=1 to partition S, nor do we
require the corresponding bipartite graph to be connected in any
way.

To be concrete, consider a few examples that illustrate special-
cases of the general model depicted in Figure 1. The standard
centralized model for supervised learning can be represented by
the graph in Figure 2, where each of the m learning agents has
access to all exemplars in the training database. Figure 3 illustrates
an ensemble where a publicly available database is available to
all the learning agents, each of which retains a private training
set. In some applications, X may be endowed with a topology.
For example, in wireless sensor networks, X = IR2 may model
locations in a city; learning agents (i.e., sensors) may exist as
points within X , and query those examples that are �nearby� with
respect to the underlying topology; such an ensemble is depicted
in Figure 4.

As mentioned earlier, the current model is a generalization of
the the work discussed in [14]. Whereas [14] focuses exclusively
on the WSN application by assuming a topology on X and
by modeling one agent per training observation, the present
formulation allows a more general structure with multiple agents
per training datum and an arbitrary input space.

Fig. 1. A Bipartite Graph Representation of an Ensemble in this Model for
Distributed Regression

Fig. 2. A �Centralized� Ensemble

Fig. 3. An Ensemble with a Public Database

Fig. 4. A Sensor Network: An Ensemble with Topology Dependent Structure

Presumably, each of the m agents wishes to use nonparametric
methods to estimate the regression function. One simple approach

is for agent i to compute f?i using only the exemplars in its
local training database Si. However, doing so ignores the structure
of distributed regression and fails to exploit an opportunity to
collaborate using the (partially) shared training database.

We henceforth assume that after locally computing f?i ? HK ,
agent i may share f?i(xj) ? IR with any agent k such that
(xj , yj) ? Sk. In other words, neighboring agents (with respect to
the bi-partite graph) communicate point estimates for the training
data they share. Using such limited communication, can the agents
collaborate to jointly improve the accuracy of their estimates?

In the next section, we derive a collaborative training algo-
rithm in this model for distributed nonparametric regression. The
algorithm is derived as an application of SOP algorithms applied
to a relaxation of the classical regularized kernel least-squares
estimator. Subsequently, we analyze its convergence properties
and investigate its statistical properties in a simplified theoretical
setting.

B. A Collaborative Training Algorithm

For technical convenience, let us introduce sets {S�i}mi=1, such
that S�i ? {1, . . . , n}. Let j ? S�i if and only if (xj , yj) ? Si. In
other words S�i contains the indices of the training examples in
Si as enumerated in S. Analogously, let S� = {1, . . . , n}.

To begin, let us rewrite (1) in a way that reveals the structure
of distributed regression. To do so, first let us introduce a function
fi ? HK for each agent i ? {1, . . . ,m}, and consider the
following constrained optimization program:

min ?z? y?22 +
?m

i=1 ?i?fi?
2
HK

(5)
s.t. zj = fi(xj) ?j ? S�, i ? {1, ...,m} (6)

fi ? HK i ? {1, . . . ,m}

Here, the optimization variables are z ? IRn and {fi}ni=1 ?
HK ; S = {(xi, yi)}ni=1 and {?i}mi=1 ? IR are the program
data. The coupling constraints in (6) dictate that for any feasible
solution to (5), every agent�s associated function is equivalent
when evaluated at {xi}ni=1. As a result, one can think about (5)
as an equivalent form of (1) in the following sense.

Lemma 1: Let (z, f?1 , ..., f?m) ? IRn�HmK denote the solution
of (5) and let f? ? HK denote the solution of (1). Assume that
?i > 0 ?i ? {1, ...,m}. Then, f?1 = � � � = f?m . If

?m
i=1 ?i = ?,

then f? = f?1 .
This form of the regularized least-squares regression problem

suggests a natural relaxation that allows us to incorporate the
structure of the distributed regression model into the estimator. In
particular, we relax the coupling constraints to require that agents
agree only on training examples they share:

min ?z? y?22 +
?m

i=1 ?i?fi?
2
HK

(7)
s.t. zj = fi(xj) ?j ? S�i, i ? {1, ...,m} (8)

fi ? HK i ? {1, . . . ,m}

Thus, for any feasible solution to (7), fi(xj) = fk(xj) if
(xj , yj) ? Si ? Sk. Looked at in this way, (5) models the
�centralized ensemble� depicted in Figure 2, while (7) captures
the more general structure in Figure 1.

Note that just as (1) can be interpreted as a projection via (2),
(7) can be interpreted as a (weighted) projection of the vector



4
(y, 0, . . . , 0) ? IRn �HmK onto the set C = ?mi=1Ci, with

Ci =
{
(z, f1, . . . , fm) : fi(xj) = zj ?j ? S�i, z ? IR

n, (9)
{fi}

m
i=1 ? HK

}
? IRn �HmK .

The significance of this observation lies in the fact that the relaxed
form of the regularized kernel least-squares estimator has been
expressed as a projection onto the intersection of a collection of
m convex sets; in particular, note that each set Ci is a subspace.
Thus, by Lemma 1, the SOP algorithm can be used to solve the
relaxed problem (7). Moreover, computing PCi(�) requires agent
i to gather examples only within its locally accessible database.
More precisely, note that for any v = (z, f1, . . . , fm) ? IRn�HmK ,
PCi(v) = (z

?, f?1 , . . . , f
?
m) where

f?j = fj ?j 6= i

f?i = arg min
f?HK

?

j?S�i

(f(xj)? zj)
2 + ?i?f ? fi?

2
HK

z?j = zj ?j s.t. j /? S�i
z?j = f

?
i (xj) ?j s.t. j ? S�i

To emphasize, computing PCi(v) leaves zj unchanged for all
j /? S�i and leaves fj unchanged for all j 6= i. The function
associated with agent i, f?i can be computed using fi and Si
after the training data labels {yj}j?S�i have been updated with
the corresponding �message variables� {zj}j?S�i . Tying these
observations together, we are left with an algorithm for collab-
orative regression estimation which solves a relaxed form of the
regularized least-squares estimator (7).

The algorithm is summarized in psuedo-code in Table 1 and
depicted pictorially in Figure 5. In words, the algorithm iterates
over each agent in turn, allowing them to compute a local
kernel estimate and to update the labels in the training database
accordingly. Multiple passes (in fact, T cycles) over the agents
are made.

Fig. 5. A Collaborative Training Algorithm

C. Convergence
Note that the asymptotic behavior of the collaborative training

algorithm is implied by the analysis of the SOP algorithm. In
particular, we have the following.

Theorem 3: Let (z, f?1 , . . . , f?m) ? IRn�HmK be the solution
to (7) and let {fi,T}mi=1 ? HK be as defined in the algorithm
described in Table I. Then,

lim
T??

fi,T = f?i

for all i ? {1, . . . ,m}.
This theorem follows from Theorem 2 and the fact that con-

vergence in norm implies point-wise convergence in RKHSs.

Given the structure of RKHS and the general analysis in [2], the
algorithm is expected to converge linearly for many kernels. We
forego a discussion of this important, but technical point for the
sake of space.

Observe that Theorem 3 characterizes the output of collab-
orative training algorithm relative to (7). This characterization
is useful insofar as it sheds light on the relationship between
the algorithm�s output and (1), the centralized regularized least-
squares estimator. The following straightforward generalization of
Theorem 1 is a step toward further understanding this important
relationship.

Theorem 4: Let (z, f?1 , . . . , f?m) ? IRn�HmK be the solution
to (7) . Then, for every agent i ? {1, . . . ,m}, there exists c?i ?
IR|Si| such that

f?i(�) =
?

j?S�i

c?ijK(�, xj). (10)

The proof of this theorem follows from the original Representer
Theorem (applied to the update equation for fi,t) and the fact that
HK is closed.

The significance of Theorem 4 lies in the fact that the size
of any agent�s locally accessible database fundamentally limits
the accuracy of that agent�s estimate. In particular, an agent
having access to only a few exemplars in an otherwise large
training database will still be limited to estimates that lie in
the span of functions determined by its local data; thus, local
connectivity influences the agent�s bias. Intuitively, however, the
message-passing through the training database may optimize the
estimator within that limited span if the ensemble is �connected�
in some meaningful way. To bear out this intuition in a simplified
theoretical setting, we consider a simple notion of connectedness
in the next section.

D. A Simplified Setting
For a given ensemble, kernel pair ({Si}mi=1,K), let us construct

an auxiliary graph as follows: let there be a node for every learning
agent and let there be an edge between node (i.e., agent) i and
node k if the following condition holds:

span({K(�, xj)}j?S�i) = span({K(�, xj)}j?S�k) (11)
= span({K(�, xj)}j?S�i?S�k)

In other words, an edge connects two nodes if the training exam-
ples they share determine the space of functions their estimates
lie in as dictated by Theorem 4.

Definition 1: Let us call the ensemble, kernel pair
({Si}

m
i=1,K) connected if and only if the auxiliary graph

so constructed is connected.
This definition leads to the following theorem, which can be
viewed as a straightforward generalization of Lemma 1.

Theorem 5: Let ({Si}mi=1,K) be connected and suppose the
ensemble employs the collaborative training algorithm using
{?i}

m
i=1. Finally, let f? denote the solution to (1) for ? =?m

i=1 ?i. Then,

f? = lim
T??

fi,T (12)
for all i ? {1, . . . ,m}.
Theorem 5 follows from Theorem 3 after noting that connect-
edness implies that the solution to (7) (z, f?1 , . . . , f?m) satisfies
f?1 = � � � = f?m . To illustrate the significance of Theorem 5



5
Init: Agents agree on a positive semi-definite kernel K(�, �) : X � X ? IR.
Training database S = {(xi, zi)}ni=1 is initialized

so that zi = yi ?i ? {1, . . . , n}.

Train: for t = 1, . . . , T
for i = 1, . . . , m

Agent i:
Retrieves database Si ? S
Computes fi,t := arg minf?HK

[ ?
j?S�i

(f(xj )? zj)2 + ?i?f ? fi,t?1?2HK

]

Updates database: zj ? fi,t(xj) ?(xj , zj) ? Si
end

end

TABLE I
AN ALGORITHM FOR TRAINING COLLABORATIVELY

and to tie it to the foregoing discussion, consider the following
example.

Example 1: Suppose X = IRd and that K(x,x?) = xTx? is
the linear kernel; in this case, HK is the set of linear functions
on X . If {Si}mi=1 is an ensemble with public database of d
linearly independent examples (depicted in Figure 3 and discussed
in Section III), then ({Si}mi=1,K) is connected. Therefore, by
Theorem 5, the collaborative training algorithm would allow agent
i to find the best linear fit to the entire data set S (for the
particular choice of regularization parameter ?), despite the fact
that only ni+d? m

i=1 ni+d
percent of the data is locally accessible.

More generally, if a pth order polynomial kernel is used, then
an analogous observation holds when dp examples are shared.

In this simple example, the potential utility of the collaborative
training algorithm is revealed. Consider the extreme case when
each agent has access to only a single example in addition
to the public database. As the number of agents m ? ?,
the collaborative training algorithm would allow every agent a
consistent estimate of the optimal linear least-squares estimate as
long as

?m
i=1 ?i ? 0; this is true despite the fact that each agent

retains local access to only d+ 1 examples for all m.

IV. DISCUSSION

As described in Table 1, the inner loop of the collaborative
training algorithm iterates over agents in the ensemble serially.
Note that the ordering is non-essential and parallelism may be
introduced. In fact, two agents can train simultaneously as long
as they do not share exemplars in their locally accessible training
database. In practical settings, multiple-access algorithms that
are frequently studied in the communications literature (e.g.,
ALOHA) may be adapted to negotiate an ordering in a distributed
fashion. Since the SOP algorithm and Theorem 2 have been
generalized to a very general class of (perhaps random) control
orderings [2], Theorem 3 can be extended in many cases. Experi-
ments that validate the collaborative training algorithm in a WSN
setting can be found in [14].

In this paper, we have focused exclusively on regularized kernel
least-squares regression. However using Bregman�s algorithm [4],
the method and many of the theorems may be extended to
more general loss functions and regularizers including Bregman
divergences.

Those familiar with LDPC codes or Bayes networks may find
the current model and algorithm reminiscent of message-passing
algorithms such a belief-propagation which are frequently studied
in those fields; variational interpretations of kernel methods in
the context of Gaussian processes further suggests a relationship

between these works. Formalizing such a connection would likely
require one to interpret our �relaxation� in the context of depen-
dency structures in Gaussian processes, and to connect alternating
projection algorithms with the generalized distributive law [1].

REFERENCES
[1] S. M. Aji and R. J. McEliece, �The generalized distributive law,� IEEE

Transactions on Information Theory, vol. 46, no. 2, pp. 325�343, March
2000.

[2] H. H. Bauschke and J. M. Borwein, �On projection algorithms for solving
convex feasibility problems,� SIAM Review, vol. 38, no. 3, pp. 367�426,
September 1996.

[3] A. Bordes, S. Ertekin, J. Weston, and L. Bottou, �Fast kernel classifiers with
online and active learning,� Journal of Machine Learning Research, vol. 6,
pp. 1579�1619, 2005.

[4] Y. Censor and S. A. Zenios, Parallel Optimization: Theory, Algorithms, and
Applications. New York: Oxford, 1997.

[5] Y. Freund and R. E. Schapire, �A decision-theoretic generalization of on-line
learning and an application to boosting,� Computer and System Sciences,
vol. 55, pp. 119�139, 1997.

[6] S. Gambs, B. Ke�gl, and E. A�imeur, �Privacy-preserving boosting,� submitted
to Data Mining and Knowledge Discovery, 2005.

[7] C. Guestrin, P. Bodi, R. Thibau, M. Paskin, and S. Madde, �Distributed
regression: an efficient framework for modeling sensor network data,� in
IPSN�04: Proceedings of the Third International Symposium on Information
Processing in Sensor Networks. New York: ACM Press, 2004.

[8] L. Gyo�rfi, M. Kohler, A. Krzyzak, and H. Walk, A Distribution-Free Theory
of Nonparametric Regression. New York: Springer, 2002.

[9] M. Kearns and H. S. Seung, �Learning from a population of hypotheses,�
Machine Learning, vol. 18, pp. 255�276, 1995.

[10] G. Kimeldorf and G. Wahba, �Some results on Tchebycheffian spline
functions,� Journal of Mathematical Analysis and Applications, vol. 33, pp.
82�95, 1971.

[11] J. Kivinen and M. K. Warmuth, �Additive versus exponentiated gradient
updates for linear prediction.� Information and Computation, vol. 132, no. 1,
pp. 1�64, 1997.

[12] A. Lazarevic and Z. Obradovic, �The distributed boosting algorithm,� in KDD
�01: Proceedings of the Seventh ACM SIGKDD International Conference on
Knowledge Discovery and Data Mining, San Francisco, CA. ACM Press,
2001.

[13] X. Nguyen, M. J. Wainwright, and M. I. Jordan, �Decentralized detection
and classification using kernel methods,� in Proceedings of the Twenty-first
International Conference on Machine Learning, Banff, Canada, 2004.

[14] J. B. Predd, S. R. Kulkarni, and H. V. Poor, �Regression in sensor networks:
Training distributively with alternating projections,� in Proceedings of the
SPIE Conference on Advanced Signal Processing Algorithms, Architectures,
and Implementations XV (invited), San Diego, CA, July 31 � August 4 2005.

[15] ��, �Consistency in models for distributed learning under communication
constraints,� IEEE Transactions on Information Theory, vol. 52, no. 1, pp.
52�63, Jan. 2006.

[16] B. Scho�lkopf and A. Smola, Learning with Kernels, 1st ed. Cambridge,
MA: MIT Press, 2002.

[17] P. K. Varshney, Distributed Detection and Data Fusion. New York: Springer,
1996.

[18] J. von Neumann, Function Operators II. Princeton, NJ: Princeton University,
1950.

[19] G. Wahba, Spline Models for Observational Data. Philadelphia: SIAM,
1990.


	Introduction
	Related Work
	Organization

	Preliminaries
	Nonparametric Least-squares Regression
	Regularized Kernel Methods
	Alternating Projections Algorithms

	Distributed Kernel Regression
	The Model
	A Collaborative Training Algorithm
	Convergence
	A Simplified Setting

	Discussion
	References

