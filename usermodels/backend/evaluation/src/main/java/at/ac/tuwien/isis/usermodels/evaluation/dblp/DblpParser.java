package at.ac.tuwien.isis.usermodels.evaluation.dblp;

import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentDTO;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserService;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserServiceImpl;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Created by Alexej Strelzow on 15.02.2016.
 */
public class DblpParser {

    private static final String ROOT_DIR = "D:\\Studium\\Master\\Master Thesis New\\Data\\";
    private static final String MASTER_FILE = ROOT_DIR + "dblp.xml";

    // where articles from chosen authors land
    private static final String DOWNLOAD_DIR = ROOT_DIR + "Downloads\\";
    // where chosen articles from chosen authors land
    private static final String CHOSEN_PAPERS_DIR = ROOT_DIR + "Chosen Papers\\";

    private static final String DBLP_XML_FOLDER = ROOT_DIR + "dblps\\";
    private static final String FILTERED_ARTICLES = DBLP_XML_FOLDER + "articles.xml";
    private static final String CHOSEN_ARTICLES = DBLP_XML_FOLDER + "articles_authors_chosen_2.xml"; // change me if necessary

    private static final String DBLP_XML_STATS = ROOT_DIR + "dblp_statistics_";

    private static final String DBLP_TEST_XML_FILE = ROOT_DIR + "dblps\\dblp1.xml";

    // author name -> list of keys of articles/etc.
    private static final Map<String, List<Work>> authorsWork = new HashMap<>();
    // author name -> size(keys)
    private static Map<String, Integer> authorsWorkCount = new HashMap<>();

    private static final List<String> FILTER_ELEMENTS = new ArrayList();
    private static final List<String> FILTER_AUTHOR = new ArrayList<>();
    private static final List<String> FILTER_EE = new ArrayList<>();

    private static final int ARTICLE_AUTHOR_THRESHOLD = 100; // from statistics.csv
    private static final int FILTERED_WORK_THRESHOLD = 25; // first filter has already been applied (author + ee)
    private static final int CHOSEN_AUTHORS_THRESHOLD = 10;

    static {
        FILTER_ELEMENTS.add("article");
        FILTER_ELEMENTS.add("author");
        FILTER_ELEMENTS.add("title");
        FILTER_ELEMENTS.add("url");
        FILTER_ELEMENTS.add("ee");

        FILTER_EE.add(".pdf");
        FILTER_EE.add(".PDF");
        FILTER_EE.add("http://arxiv.org"); // http://arxiv.org/abs/1401.3863
        FILTER_EE.add("http://ieeexplore.ieee.org"); // http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6810769
        FILTER_EE.add("http://www.isca-speech.org"); // http://www.isca-speech.org/archive/wocci_2012/wc12_104.html
        FILTER_EE.add("http://www.aaai.org"); // http://www.aaai.org/ojs/index.php/aimagazine/article/view/1826

        // minor domains
        /*
        FILTER_EE.add("http://www.sciencedirect.com"); // http://www.sciencedirect.com/science/article/pii/S0167739X14001496
        FILTER_EE.add("http://ccsenet.org");
        FILTER_EE.add("http://www.mdpi.com");
        FILTER_EE.add("https://www.jstage.jst.go.jp");
        FILTER_EE.add("http://www.mii.lt/");
        */

        // CAN'T DOWNLOAD PDFs
        /*
        FILTER_EE.add("http://dx.doi.org"); //http://dx.doi.org/10.1007/BF03036466
        FILTER_EE.add("http://doi.acm.org");
        FILTER_EE.add("http://doi.ieeecomputersociety.org");
        FILTER_EE.add("http://dl.acm.org");
        FILTER_EE.add("http://aisel.aisnet.org");
        FILTER_EE.add("http://link.springer");
        */
    }

    enum dblp {
        article,
        inproceedings,
        proceedings, // no author, but editor element
        book,
        incollection,
        phdthesis,
        mastersthesis,
        www
    }

    private static class Work {
        String key;
        String title;
        String type;
        String url;
        String ee;

        Work(String key, String type, String title,  String url, String ee) {
            this.key = key;
            this.type = type;
            this.title = title;
            this.url = url;
            this.ee = ee;
        }

        @Override
        public String toString() {
            return this.title;
        }
    }

    public static Long[] count(dblp type) throws IOException {
        List<String> keyList = new LinkedList<>();
        Long[] result = new Long[2];
        long count = 0;
        BufferedReader br = new BufferedReader(new FileReader(MASTER_FILE)); //DBLP_TEST_XML_FILE
        boolean inside = false;
        boolean isAuthor = false;

        for(String line; (line = br.readLine()) != null; ) {
            if (line.trim().isEmpty()) continue;

            if (line.indexOf("<" + type.name() + " ") != -1) {
                inside = true;
                if (line.indexOf("key") == -1) {
                    System.out.println(line);
                } else {
                    int index = line.indexOf("key");
                    keyList.add(line.substring(index+5, line.indexOf("\"", index+10)));
                    count++;
                }
            } else if (line.indexOf("</" + type.name() + ">") != -1) {
                inside = false;
            }

            if (inside && line.indexOf("<author>") != -1) {
                String content = line.substring("<author>".length(), line.length()-"</author>".length());
                if (content.contains("Amir") && content.contains("Keyvan") && content.contains("Khandani")) {
                    System.out.print(content);
                    isAuthor = true;
                }
            } else if (isAuthor && line.indexOf("<title>") != -1) {
                String content = line.substring("<title>".length(), line.length()-"</title>".length());
                System.out.print(" -> " + content + "\r\n");
                isAuthor = false;
                if ("Layered Interference Alignment: Achieving the Total DoF of MIMO X Channels.".equals(content)) {
                    System.out.print("");
                }
            }
        }
        result[0] = (long)keyList.size();
        result[1] = count;
        return result;
    }

    // // -Xmx6144M -d64
    public static void main(String[] args) {
        try {
            Long[] result = count(dblp.article);
            System.out.println("Articles in ROOT-XML FILE: " + result[0] + "/" + result[1]);
        } catch (IOException e) { e.printStackTrace(); }

        /*
        try {
            BufferedReader br = new BufferedReader(new FileReader(CHOSEN_ARTICLES)); //DBLP_TEST_XML_FILE

            long counter = 0;
            for (String line; (line = br.readLine()) != null; ) {
                if (line.startsWith("<author")) counter++;
            }
            System.out.println(counter);

        } catch (IOException e) {
        }
        */

        // 1) identify most popular domains (-> ee)
        //filterEEsFromArticles();

        // 2) identify most popular authors
        //createCountStats();

        // 3) filter articles by taking ees (1) and authors (2) into account
        //filterArticles();

        // 4) filter filtered articles (3) to get authors with certain work count
        //filterFilteredArticles();

        // 5) download filtered filtered (= chosen) articles as good as possible
        //downloadPdfsFromChosenAuthors();

        // 6) Q.Mgmt.: generate some meta-data (is author mentioned in paper? title, ee, #pages)
        //generateMetadata();

        // 7) copy only files from download dir to chosen papers-dir where author is mentioned in paper
        //copyRelevantFiles();
    }

    /**
     * Find most relevant domains to which dblp content refers.
     * Findings for domains with papers > 10k:
     *  2.270.860 -> http://dx.doi.org/
     *    346.315 -> http://doi.ieeecomputersociety.org/
     *    274.414 -> http://doi.acm.org/
     *     96.820 -> http://arxiv.org/
     *     36.010 -> http://dl.acm.org/
     *     27.458 -> http://ieeexplore.ieee.org/
     *     21.002 -> http://www.isca-speech.org/
     *     16.936 -> http://aisel.aisnet.org/
     *     16.807 -> http://www.aaai.org/
     *     11.023 -> http://search.ieice.org/
     */
    public static void filterEEsFromArticles() {
        try {
            //Set<String> sortedEEs = new TreeSet<>();
            //Set<String> sortedRootEEs = new TreeSet<>();
            Map<String, String> rootExampleMap = new LinkedHashMap<>();
            Map<String, Integer> rootFrequencyMap = new LinkedHashMap<>();

            BufferedReader br = new BufferedReader(new FileReader(MASTER_FILE)); //DBLP_TEST_XML_FILE

            for(String line; (line = br.readLine()) != null; ) {
                if (line.startsWith("<ee>")) {
                    String link = line.substring(line.indexOf(">") + 1, line.lastIndexOf("<"));

                    if (link.endsWith(".pdf") || link.endsWith(".PDF")) {
                        continue;
                    }

                    if (link.indexOf("/", 10) == -1) {
                        continue;
                    }

                    String root = link.substring(0, link.indexOf("/", 10));

                    if (!rootExampleMap.containsKey(root)) {
                        rootExampleMap.put(root, link);
                        rootFrequencyMap.put(root, 1);

                    } else {
                        rootFrequencyMap.put(root, (rootFrequencyMap.get(root)+1));
                    }
                }
            }

            rootFrequencyMap = sortByValue(rootFrequencyMap);
            final ArrayList<String> keys = new ArrayList<>(rootFrequencyMap.keySet());
            Collections.reverse(keys);

            for (String root : keys) {
                if (rootFrequencyMap.get(root) > 10000) {
                    System.out.println(rootFrequencyMap.get(root) + " -> " + rootExampleMap.get(root));
                }
            /*
            2270860 -> http://dx.doi.org/10.1007/BF03036466
             346315 -> http://doi.ieeecomputersociety.org/10.1109/TKDE.2005.21
             274414 -> http://doi.acm.org/10.1145/2794299
              96820 -> http://arxiv.org/abs/1401.3863
              36010 -> http://dl.acm.org/citation.cfm?id=202940
              27458 -> http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6810769
              21002 -> http://www.isca-speech.org/archive/wocci_2012/wc12_104.html
              16936 -> http://aisel.aisnet.org/jais/vol12/iss5/1
              16807 -> http://www.aaai.org/ojs/index.php/aimagazine/article/view/1826
              11023 -> http://search.ieice.org/bin/summary.php?id=e92-d_1_97&amp;category=D&amp;year=2009&amp;lang=E&amp;abst=
             */
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Count work of authors from dblp1.xml to dblp10.xml inside {@link DBLP_XML_FOLDER} and
     * create statistics inside {@link DBLP_XML_STATS}. Those statistics will be used to determine
     * suitable authors. It contains the name of the author + the number of articles (s)he has worked on.
     * In dblp articles or other resources may contains author-elements as children.
     */
    public static void createCountStats() {
        File dir = new File(DBLP_XML_FOLDER);
        if (!dir.isDirectory()) {
            return;
        }

        dblp type = dblp.article;
        /*
        for (dblp type : dblp.values()) {
            countWork(doc, type);
        }
        */

        for (File f : dir.listFiles()) {
            if (f.getName().startsWith("generator")) {
                Document doc = parseResource(f);
                countWork(doc, type);
            }
        }

        File f = new File(DBLP_XML_STATS + type.name() + ".csv");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        toCSV(sortDesc(), f, type);
    }

    /**
     * Filter articles from {@link MASTER_FILE} based on authors and ee-links and create own file {@link FILTERED_ARTICLES}
     * with own lean structure.
     * <code>
     *     <article mdate="2014-07-21" key="journals/access/LiuYY14">
     *         <author>Chang Liu</author>
     *         <title>Source Code Revision History Visualization Tools: Do They Work and What Would it Take to Put Them to Work?</title>
     *         <ee>http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6810769</ee>
     *         <url>db/journals/access/access2.html#LiuYY14</url>
     *      </article>
     * </code>
     */
    public static void filterArticles() {
        fillAuthorFilter();

        boolean isArticle = false;
        boolean authorOk = false, eeOk = false;

        try(PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(FILTERED_ARTICLES)), true)) {
            StringBuilder sb = new StringBuilder();
            //BufferedReader br = new BufferedReader(new InputStreamReader(DblpParser.class.getClassLoader().getResourceAsStream("test.xml")));
            BufferedReader br = new BufferedReader(new FileReader(MASTER_FILE)); //DBLP_TEST_XML_FILE

            for(String line; (line = br.readLine()) != null; ) {
                if (line.trim().isEmpty()) continue;

                if (line.startsWith("<" + dblp.article.name())) {
                    isArticle = true;
                    sb.append(line + "\r\n");

                } else if (line.endsWith("</" + dblp.article.name() + ">")) {
                    isArticle = false;
                    if (authorOk && eeOk) {
                        sb.append(line);
                        pw.println(sb.toString());
                    }
                    sb.setLength(0);
                    authorOk = eeOk = false;

                } else {
                    if (isArticle) {
                        String element = line.substring(line.indexOf("<") + 1, line.indexOf(">"));

                        if (FILTER_ELEMENTS.contains(element)) {
                            if ("author".equals(element)) {
                                String content = line.substring(line.indexOf(">")+1, line.lastIndexOf("<"));
                                for (String filer : FILTER_AUTHOR) {
                                    if (content.equals(filer)) {
                                        sb.append(line + "\r\n");
                                        authorOk = true;
                                        continue;
                                    }
                                }
                            } else if ("ee".equals(element)) {
                                String content = line.substring(line.indexOf(">")+1, line.lastIndexOf("<"));
                                for (String filer : FILTER_EE) {
                                    if (content.contains(filer)) {
                                        sb.append(line + "\r\n");
                                        eeOk = true;
                                        break;
                                    }
                                }
                            } else {
                                sb.append(line + "\r\n");
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * To narrow the authors down some more we filter the {@link FILTERED_ARTICLES} and only allow those
     * authors, who wrote more than {@link FILTERED_WORK_THRESHOLD} articles. From them we choose
     * randomly ten and store them in {@link CHOSEN_ARTICLES}.
     */
    public static void filterFilteredArticles() {
        fillAuthorFilter();

        dblp type = dblp.article;
        Document doc = parseResource(new File(FILTERED_ARTICLES));
        for (String author : FILTER_AUTHOR) {
            getWorkFromAuthor(doc, type, author);
        }

        final Map<String, List<Work>> chosenAuthors = new LinkedHashMap<>();

        for (String author : FILTER_AUTHOR) {
            final List<Work> workList = authorsWork.get(author);
            if (workList != null && workList.size() >= FILTERED_WORK_THRESHOLD) {
                System.out.println(author);
                chosenAuthors.put(author, workList);
                //for (Work work : workList) {
                //    System.out.println("\t" + work);
                //}
            }
        }

        List<Integer> randomIndexes = new ArrayList<>(CHOSEN_AUTHORS_THRESHOLD);
        while (randomIndexes.size() != CHOSEN_AUTHORS_THRESHOLD) {
            final int randomIndex = ThreadLocalRandom.current().nextInt(0, chosenAuthors.size() - 1 + 1);
            if (randomIndexes.contains(randomIndex)) continue;
            randomIndexes.add(randomIndex);
        }

        StringBuilder sb = new StringBuilder();
        for (Integer r : randomIndexes) {
            List<String> l = new ArrayList<>(chosenAuthors.keySet());
            String author = l.get(r);
            sb.append("<author name=\"" + author + "\">").append("\r\n");
            for (Work work : chosenAuthors.get(author)) {
                sb.append("\t<article key=\"" + work.key + "\">").append("\r\n");
                sb.append("\t\t<title>").append(work.title).append("</title>").append("\r\n");
                sb.append("\t\t<ee>").append(work.ee).append("</ee>").append("\r\n");
                sb.append("\t</article>").append("\r\n");
            }
            sb.append("<author>").append("\r\n");
        }

        try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(CHOSEN_ARTICLES)), true)) {
            pw.println(sb.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Download articles into {@link DOWNLOAD_DIR} via ee-link from following sources if possible (see {@link FILTER_EE}):
     * <ul>
     *     <li>ee-urls that end with *.pdf or *.PDF</li>
     *     <li>http://arxiv.org</li>
     *     <li>http://ieeexplore.ieee.org</li>
     *     <li>http://www.isca-speech.org</li>
     *     <li>http://www.aaai.org</li>
     * </ul>
     */
    public static void downloadPdfsFromChosenAuthors() {
        //List<String> authorsToLookInto = new ArrayList<>();

        final Document doc = parseResource(new File(CHOSEN_ARTICLES));

        for (Element author : doc.getElementsByTag("author")) {
            String authorName = author.attr("name");

            if (authorName.trim().isEmpty() /*|| !authorsToLookInto.contains(authorName.trim())*/) continue;

            final File file = new File(DOWNLOAD_DIR + authorName);
            if (!file.exists()) {
                file.mkdirs();
            }

            for (Element article : author.children()) {
                try {
                    String title = article.getElementsByTag("title").get(0).ownText();
                    String ee = article.getElementsByTag("ee").get(0).ownText();

                    title = title.replaceAll("[^a-zA-Z0-9.-]", "_"); // maybe title has illegal characters
                    String dlFilePath = DOWNLOAD_DIR + authorName + "\\" + title + ".pdf";
                    String dlLink = null;

                    if (ee.endsWith(".pdf") || ee.endsWith(".PDF")) {
                        dlLink = ee;

                    } else { // more complicated, have to find the link on our own
                        /*
                        http://arxiv.org            -> http://arxiv.org/abs/1401.3863
                        http://ieeexplore.ieee.org  -> http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6810769
                        http://www.isca-speech.org  -> http://www.isca-speech.org/archive/wocci_2012/wc12_104.html
                        http://www.aaai.org         -> http://www.aaai.org/ojs/index.php/aimagazine/article/view/1826
                         */

                        if (ee.startsWith("http://arxiv.org")) {
                            HttpClient client = HttpClientBuilder.create().build();
                            HttpGet request = new HttpGet(ee);
                            HttpResponse response = client.execute(request);
                            final String html = EntityUtils.toString(response.getEntity());
                            Document htmlDoc = Jsoup.parse(html);
                            for (Element e : htmlDoc.select("a[href]")) {
                                if (e.attr("href").startsWith("/pdf/")) {
                                    dlLink = "http://arxiv.org" + e.attr("href") + ".pdf";
                                }
                            }

                            /* shortcut that does not work always
                            final String id = ee.substring(ee.indexOf("/abs/") + 5, ee.length());
                            // http://arxiv.org : http://arxiv.org/ftp/arxiv/papers/1401/1401.3863.pdf
                            if (id.indexOf(".") != -1) {
                                final String folder = id.substring(0, id.indexOf("."));
                                dlLink = "http://arxiv.org/ftp/arxiv/papers/" + folder + "/" + id + ".pdf";
                            }
                            */

                        } else if (ee.startsWith("http://ieeexplore.ieee.org")) {
                            HttpClient client = HttpClientBuilder.create().build();
                            HttpGet request = new HttpGet(ee);
                            HttpResponse response = client.execute(request);
                            final String html = EntityUtils.toString(response.getEntity());
                            Document htmlDoc = Jsoup.parse(html);
                            final Elements select = htmlDoc.select("#full-text-pdf");
                            if (!select.isEmpty()) {
                                // http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6810769
                                dlLink = "http://ieeexplore.ieee.org" + select.get(0).attr("href");
                            }
                        } else if (ee.startsWith("http://www.isca-speech.org")) {
                            // http://www.isca-speech.org/archive/wocci_2012/wc12_104.html
                            final String baseUrl = ee.substring(0, ee.lastIndexOf("/")+1);
                            final String id = ee.substring(ee.lastIndexOf("/")+1, ee.lastIndexOf("."));
                            // http://www.isca-speech.org/archive/wocci_2012/papers/wc12_104.pdf
                            dlLink = baseUrl + "papers/"+ id + ".pdf";

                        } else if (ee.startsWith("http://www.aaai.org")) {
                            HttpClient client = HttpClientBuilder.create().build();
                            HttpGet request = new HttpGet(ee);
                            HttpResponse response = client.execute(request);
                            final String html = EntityUtils.toString(response.getEntity());
                            Document htmlDoc = Jsoup.parse(html);
                            final Elements select = htmlDoc.select(".file");
                            if (!select.isEmpty()) {
                                // http://www.aaai.org/ojs/index.php/aimagazine/article/view/1826
                                dlLink = select.get(0).attr("href").replace("/view/", "/download/");
                            }
                        }

                        if (!new File(dlFilePath).exists() && dlLink != null) {
                            URL url = new URL(dlLink);
                            InputStream in = null;
                            try {
                                in = url.openStream();
                            } catch (FileNotFoundException e) {
                                System.err.println(dlLink + " to " + ee + " not found!");
                            }
                            Files.copy(in, Paths.get(dlFilePath), StandardCopyOption.REPLACE_EXISTING);
                            in.close();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Iterate over {@link CHOSEN_ARTICLES} and look at those entries where the author is one of our chosen ones.
     * Create two kind of files if necessary:
     * <ul>
     *     <li>_meta_ok.csv</li>: article from {@link CHOSEN_ARTICLES} is inside our {@link DOWNLOAD_DIR}
     *     Save following info:
     *     <ul>
     *         <li>OK</li>: shall we chose this article for our experiments? Only if the dblp-author is mentioned inside the paper as one.
     *         <li>TITLE</li>: title of the article
     *         <li>EE</li>: ee link of the article
     *         <li>IN DOCUMENT</li>: true if dblp-author is mentioned as author inside the paper
     *         <li>PAGES</li>: number of pages
     *     </ul>
     *     <li>_meta_nok.csv</li>: article from {@link CHOSEN_ARTICLES} is NOT in our {@link DOWNLOAD_DIR}
     *     Save following info:
     *     <ul>
     *         <li>TITLE</li>: title of the article
     *         <li>EE</li>: ee link of the article
     *     </ul>
     * </ul>
     *
     *
     */
    public static void generateMetadata() {
        // OK, TITLE, EE, IN DOCUMENT, PAGES
        // gather authors
        Map<String, List<Work>> authorsMap = new HashMap<>();
        PdfParserService pdfExtractor = new PdfParserServiceImpl();

        for (File f : new File(DOWNLOAD_DIR).listFiles()) {
            if (f.isDirectory()) {
                authorsMap.put(f.getName(), new ArrayList<>());
            }
        }

        final Document doc = parseResource(new File(CHOSEN_ARTICLES));
        for (String author : authorsMap.keySet()) {
            StringBuilder sbOk = new StringBuilder("OK;TITLE;EE;IN DOCUMENT;PAGES\r\n");
            StringBuilder sbNOK = new StringBuilder("TITLE;EE");
            boolean writeOks = false;
            boolean writeNoks = false;

            for (Element authorElement : doc.getElementsByTag("author")) {
                String authorName = authorElement.attr("name");
                if (authorName.trim().isEmpty()) continue;

                if (author.equals(authorName.trim())) {
                    for (Element article : authorElement.children()) {
                        String title;
                        String ee;
                        try {
                            title = article.getElementsByTag("title").get(0).ownText();
                            ee = article.getElementsByTag("ee").get(0).ownText();
                        } catch (IndexOutOfBoundsException e) {
                            continue;
                        }

                        String titleOnHD = title.replaceAll("[^a-zA-Z0-9.-]", "_"); // maybe title has illegal characters
                        titleOnHD = titleOnHD + ".pdf";
                        File pdf = new File(DOWNLOAD_DIR + authorName + "\\" + titleOnHD);
                        if (pdf.exists()) {
                            try {
                                DocumentDTO documentDTO = pdfExtractor.extract(pdf);
                                String text = documentDTO.getText();
                                String pageCount = String.valueOf(documentDTO.getPageCount());

                                // check only last name and lowercase it, because there are many ways authors appear in the paper
                                String nameToCheck = author.substring(author.lastIndexOf(" "), author.length()).trim().toLowerCase();
                                boolean isAuthorInDoc = containsAuthor(text, nameToCheck);

                                sbOk.append(isAuthorInDoc + ";" + title + ";" + ee + ";" + isAuthorInDoc + ";" + pageCount + "\r\n");
                                writeOks = true;
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            sbNOK.append(title + ";" + ee + "\r\n");
                            writeNoks = true;
                        }
                    }
                }
            }

            if (writeOks) {
                try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(DOWNLOAD_DIR + author + "_meta_ok.csv")), true)) {
                    pw.println(sbOk.toString());
                    pw.flush();
                    pw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (writeNoks) {
                try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(DOWNLOAD_DIR + author + "_meta_nok.csv")), true)) {
                    pw.println(sbNOK.toString());
                    pw.flush();
                    pw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean containsAuthor(String text, String author) {
        Matcher m = Pattern.compile(author).matcher(text.substring(0, 1000).toLowerCase());
        return m.find();
    }

    /**
     * Read all *_meta_ok.csv files from {@link DOWNLOAD_DIR} and copy only those files into the {@link CHOSEN_PAPERS_DIR},
     * where the author is mentioned in the paper.
     */
    public static void copyRelevantFiles() {
        try {
            for (File f : new File(DOWNLOAD_DIR).listFiles()) {
                if (f.getName().endsWith("_meta_ok.csv")) {
                    for (String line : Files.readAllLines(f.toPath(), Charset.forName("ISO-8859-1"))) {
                        final String[] split = line.split(";");
                        if ("true".equals(split[0])) { // OK;TITLE;EE;IN DOCUMENT;PAGES
                            String author = f.getName().substring(0, f.getName().indexOf("_meta"));
                            String title = split[1].replaceAll("[^a-zA-Z0-9.-]", "_") + ".pdf";
                            File pdf = new File(DOWNLOAD_DIR + author + "\\" + title);
                            File targetDir = new File(CHOSEN_PAPERS_DIR + author);
                            if (!targetDir.exists()) {
                                targetDir.mkdirs();
                            }
                            File targetFile = new File(targetDir, title);
                            if (!targetFile.exists()) {
                                Files.copy(pdf.toPath(), targetFile.toPath());
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void fillAuthorFilter() {
        String articleStatPath = DBLP_XML_STATS + dblp.article.name() + ".csv";
        try {
            for (String line : Files.readAllLines(Paths.get(articleStatPath), Charset.forName("ISO-8859-1"))) {
                if (line.contains(";")) {
                    if (line.contains("#")) continue;

                    final String[] split = line.split(";");
                    if (Integer.parseInt(split[1]) >= ARTICLE_AUTHOR_THRESHOLD) {
                        FILTER_AUTHOR.add(split[0]);
                    } else {
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Document parseResource(File file) {
        try {
            return Jsoup.parse(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void countWork(Document doc, dblp type) {
        for (Element e : doc.getElementsByTag(type.name())) {
            for (Element author : e.getElementsByTag("author")) {
                Integer count = authorsWorkCount.get(author.ownText());
                if (count == null) {
                    authorsWorkCount.put(author.ownText(), 1);
                } else {
                    authorsWorkCount.put(author.ownText(), ++count);
                }
            }
        }
    }

    public static void getWorkFromAuthor(Document doc, dblp type, String targetAuthor) {
        for (Element e : doc.getElementsByTag(type.name())) {
            for (Element author : e.getElementsByTag("author")) {
                if (targetAuthor != null && targetAuthor.equals(author.ownText())) {
                    String key = e.attr("key");
                    String title = e.getElementsByTag("title").get(0).ownText();
                    String url = e.getElementsByTag("url") != null ? e.getElementsByTag("url").get(0).ownText() : "";
                    String ee = null;
                    try {
                        ee = e.getElementsByTag("ee") != null ? e.getElementsByTag("ee").get(0).ownText() : "";
                    } catch(Exception exc) {
                        exc.printStackTrace();
                        System.out.println(key + " " + title + " " + targetAuthor);
                    }

                    if (authorsWork.containsKey(author.ownText())) {
                        final List<Work> list = authorsWork.get(author.ownText());
                        Work work = new Work(key, type.name(), title, url, ee);
                        list.add(work);
                    } else {
                        List<Work> list = new ArrayList<>();
                        Work work = new Work(key, type.name(), title, url, ee);
                        list.add(work);
                        authorsWork.put(author.ownText(), list);
                    }
                }
            }
        }
    }

    private static List<String> sortDesc() {
        authorsWorkCount = sortByValue(authorsWorkCount);
        List<String> keyList = new ArrayList<>(authorsWorkCount.keySet());
        Collections.reverse(keyList);
        return keyList;
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        Map<K,V> result = new LinkedHashMap<>();
        Stream<Map.Entry<K, V>> st = map.entrySet().stream();

        st.sorted(Comparator.comparing(e -> e.getValue()))
                .forEachOrdered(e -> result.put(e.getKey(), e.getValue()));

        return result;
    }

    private static void toCSV(List<String> keyList, File file, dblp type) {
        PrintWriter pw;

        try {
            pw = new PrintWriter(new FileOutputStream(file), true);
        }catch (IOException e) {
            e.printStackTrace();
            return;
        }

        pw.println("AUTHORS;" + type.name().toUpperCase());

        for (String author : keyList) {
            pw.println(author + ";" + authorsWorkCount.get(author));
        }

        pw.close();
    }

    public static List<Work> getWorkFromAuthor(String authorName) {
        File dir = new File(DBLP_XML_FOLDER);
        if (!dir.isDirectory()) {
            return null;
        }

        dblp type = dblp.article;

        for (File f : dir.listFiles()) {
            Document doc = parseResource(f);
            getWorkFromAuthor(doc, type, authorName);
        }

        return authorsWork.get(authorName);
    }

}
