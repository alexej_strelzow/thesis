package at.ac.tuwien.isis.usermodels.evaluation.generator;

import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import org.junit.Assert;

import java.io.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

/**
 * Created by Alexej Strelzow on 19.07.2016.
 */
public class LatexTableGenerator {

    public static String getUserModelsAsLatexTable() throws Exception {
        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        if (!modelsDir.isDirectory()) {
            Assert.fail();
        }

        StringBuilder latexBuilder = new StringBuilder();
        latexBuilder.append("\\begin{tabular}{l|c|c|c|c|c|c}").append("\r\n");
        latexBuilder.append("\\multicolumn{1}{p{1.8cm}|}{\\centering Author}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{1.8cm}|}{\\centering Total \\\\ initial \\\\ tokens}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{1.8cm}|}{\\centering Tokens \\\\ after \\\\ preprocess.}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{1.8cm}|}{\\centering Total \\\\ removed \\\\ tokens}").append("\r\n").append("  & ");

        latexBuilder.append("\\multicolumn{1}{p{1.6cm}|}{\\centering Total \\\\ tokens \\\\ in $M_u$}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{1.6cm}|}{\\centering Total \\\\ types \\\\ in $M_u$}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{1.6cm}}{\\centering Average \\\\ word \\\\ rank}").append("  \\\\").append("\r\n");
        latexBuilder.append("  ").append("\\hline").append("\r\n");

        Locale useLocal = Locale.ENGLISH;
        DecimalFormat dfZero = new DecimalFormat();
        dfZero.setMaximumFractionDigits(0);
        dfZero.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(useLocal));
        dfZero.setDecimalSeparatorAlwaysShown(false);

        long totalInitialTokens = 0;
        long totalRemovedTokens = 0;
        long totalRemovedTokensAfterPreprocessing = 0;
        long sumModelTokens = 0;
        long sumModelTypes = 0;
        long sumAvgRank = 0;

        Map<String, List<String>> authorsEntries = new LinkedHashMap<>();

        for (File userDir : modelsDir.listFiles()) {
            if (userDir.isDirectory()) {
                for (File serializedModel : userDir.listFiles()) {
                    ObjectInputStream is = new ObjectInputStream(new FileInputStream(serializedModel));
                    UserModelDTO model = (UserModelDTO) is.readObject();

                    String author = model.getName();
                    double initalTokens = model.getInitalTokenSize();
                    double removedTokensAfterPreprocessing = model.getAvgRemovedTokensAfterPreprocessing()*model.getContainingDocs().size();
                    double removedtokens = model.getInitalTokenSize() - model.getModel().size();
                    double modelTokens = model.getModel().size();
                    double modelTypes = model.getModel().uniqueSet().size();
                    double avgRank = model.getAvgRank();

                    String line = author + ";" + initalTokens + ";" + removedTokensAfterPreprocessing + ";" + removedtokens + ";" + modelTokens + ";" + modelTypes + ";" + avgRank;

                    if (!authorsEntries.containsKey(author)) {
                        final LinkedList<String> strings = new LinkedList<>();
                        strings.add(line);
                        authorsEntries.put(author, strings);
                    } else {
                        final List<String> strings = authorsEntries.get(author);
                        strings.add(line);
                    }

                    totalInitialTokens += initalTokens;
                    totalRemovedTokens += removedtokens;
                    totalRemovedTokensAfterPreprocessing += removedTokensAfterPreprocessing;
                    sumModelTokens += modelTokens;
                    sumModelTypes += modelTypes;
                    sumAvgRank += avgRank;

                    String name = userDir.getName();
                    name = name.substring(name.lastIndexOf(" "), name.length());

                    latexBuilder
                            .append("  ").append(name).append(" & ")
                            .append(dfZero.format(initalTokens)).append(" & ")
                            .append(dfZero.format(removedTokensAfterPreprocessing)).append(" & ")
                            .append(dfZero.format(removedtokens)).append(" & ")
                            .append(dfZero.format(modelTokens)).append(" & ")
                            .append(dfZero.format(modelTypes)).append(" & ")
                            .append(dfZero.format(avgRank)).append(" \\\\")
                            .append("\r\n");
                }
            }
        }

        latexBuilder.append("  ").append("\\hline").append("\r\n").append("  ").append("\\hline").append("\r\n");

        long avgInitialTokens = totalInitialTokens / 10;
        long avgRemovedTokensAfterPreprocessing = totalRemovedTokensAfterPreprocessing / 10;
        long avgRemovedTokens = totalRemovedTokens / 10;
        long avgModelTokens = sumModelTokens / 10;
        long avgModelTypes = sumModelTypes / 10;
        long avgAvgRank = sumAvgRank / 10;

        latexBuilder
                .append("  ").append("Average").append(" & ")
                .append(dfZero.format(avgInitialTokens)).append(" & ")
                .append(dfZero.format(avgRemovedTokensAfterPreprocessing)).append(" & ")
                .append(dfZero.format(avgRemovedTokens)).append(" & ")
                .append(dfZero.format(avgModelTokens)).append(" & ")
                .append(dfZero.format(avgModelTypes)).append(" & ")
                .append(dfZero.format(avgAvgRank)).append(" \\\\")
                .append("\r\n");

        List<String> allEntries = new LinkedList<>();
        for (List<String> strings : authorsEntries.values()) {
            allEntries.addAll(strings);
        }

        final List<Double> standardDeviation = computeStandardDeviationForUserModel(allEntries,
                avgInitialTokens,
                avgRemovedTokensAfterPreprocessing,
                avgRemovedTokens,
                avgModelTokens,
                avgModelTypes,
                avgAvgRank);

        latexBuilder // "$\\sigma$ = $\\sqrt {\\mu}$"
                .append("  ").append("$\\sigma$").append(" & ")
                .append(dfZero.format(standardDeviation.get(0))).append(" & ")
                .append(dfZero.format(standardDeviation.get(1))).append(" & ")
                .append(dfZero.format(standardDeviation.get(2))).append(" & ")
                .append(dfZero.format(standardDeviation.get(3))).append(" & ")
                .append(dfZero.format(standardDeviation.get(4))).append(" & ")
                .append(dfZero.format(standardDeviation.get(5))).append(" \\\\")
                .append("  ").append("\\hline").append("\r\n");

        latexBuilder.append("\\end{tabular}");

        return latexBuilder.toString();
    }

    public static String getExperimentsAsLatexTable(String file) throws Exception {
        File csvFile = new File(file);
        if (!csvFile.exists()) {
            Assert.fail();
        }

        StringBuilder latexBuilder = new StringBuilder();
        latexBuilder.append("\\begin{tabular}{l|c|c|c|c|c}").append("\r\n");
        latexBuilder.append("\\multicolumn{1}{p{2cm}|}{\\centering Author/ \\\\ Standard \\\\ deviation}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{2cm}|}{\\centering Average \\\\ tokens \\\\ in $M_d$}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{2cm}|}{\\centering Average \\\\ types \\\\ in $M_d$}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{2cm}|}{\\centering Average \\\\ unknown \\\\ words}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{2cm}|}{\\centering Average \\\\ known \\\\ words ($M_u$)}").append("\r\n").append("  & ");
        latexBuilder.append("\\multicolumn{1}{p{2cm}}{\\centering Average \\\\ known \\\\ words ($M_c$)}").append("  \\\\").append("\r\n");
        latexBuilder.append("  ").append("\\hline").append("\r\n");

        Locale useLocal = Locale.ENGLISH;
        DecimalFormat dfZero = new DecimalFormat();
        dfZero.setMaximumFractionDigits(0);
        dfZero.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(useLocal));
        dfZero.setDecimalSeparatorAlwaysShown(false);

        // total
        long sumTokensDocumentModel = 0;
        long sumTypesDocumentModel = 0;
        long sumUnknownWords = 0;
        long sumKnownWordsUserModel = 0;
        long sumKnownWordsGeneralModel = 0; // fwl

        // author
        long sumTokensDocumentModelForAuthor = 0;
        long sumTypesDocumentModelForAuthor = 0;
        long sumUnknownWordsForAuthor = 0;
        long sumKnownWordsUserModelForAuthor = 0;
        long sumKnownWordsGeneralModelForAuthor = 0; // fwl

        Map<String, List<String>> authorsEntries = new LinkedHashMap<>();

        String lastAuthor = null;
        final String SEP = ",";
        int entryCounter = 0;
        boolean start = false;

        // parse CSV protocol file and generate table
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        String line; // header
        while ((line = br.readLine()) != null) {
            if (line.startsWith("Name; [Md] model terms;")) { // needed for exp1, because of lines 1-30
                start = true;
                continue;
            }

            if (start && line.contains(";") || line.trim().isEmpty()) { // line.trim().isEmpty() = last line
                String author = null;
                long tokensDocumentModel = 0;
                long typesDocumentModel = 0;
                long unknownWords = 0;
                long knownWordsUserModel = 0;
                long knownWordsGeneralModel = 0;

                if (!line.trim().isEmpty()) {
                    final String[] fields = line.split(";");
                    author = fields[0];
                    tokensDocumentModel = Long.parseLong(fields[1].replace(SEP, ""));
                    typesDocumentModel = Long.parseLong(fields[2].replace(SEP, ""));
                    unknownWords = Long.parseLong(fields[3].replace(SEP, ""));
                    knownWordsUserModel = Long.parseLong(fields[4].replace(SEP, ""));
                    knownWordsGeneralModel = Long.parseLong(fields[5].replace(SEP, ""));

                    if (!authorsEntries.containsKey(author)) {
                        final LinkedList<String> strings = new LinkedList<>();
                        strings.add(line);
                        authorsEntries.put(author, strings);
                    } else {
                        final List<String> strings = authorsEntries.get(author);
                        strings.add(line);
                    }
                }

                if (author != null && (lastAuthor == null || author.equals(lastAuthor))) {
                    entryCounter++;
                    sumTokensDocumentModelForAuthor += tokensDocumentModel;
                    sumTypesDocumentModelForAuthor += typesDocumentModel;
                    sumUnknownWordsForAuthor += unknownWords;
                    sumKnownWordsUserModelForAuthor += knownWordsUserModel;
                    sumKnownWordsGeneralModelForAuthor += knownWordsGeneralModel; // fwl

                } else {
                    sumTokensDocumentModel += sumTokensDocumentModelForAuthor;
                    sumTypesDocumentModel += sumTypesDocumentModelForAuthor;
                    sumUnknownWords += sumUnknownWordsForAuthor;
                    sumKnownWordsUserModel += sumKnownWordsUserModelForAuthor;
                    sumKnownWordsGeneralModel += sumKnownWordsGeneralModelForAuthor; // fwl

                    final long tokensDocumentModelForAuthorAvg = sumTokensDocumentModelForAuthor / entryCounter;
                    final long typesDocumentModelForAuthorAvg = sumTypesDocumentModelForAuthor / entryCounter;
                    final long unknownWordsForAuthorAvg = sumUnknownWordsForAuthor / entryCounter;
                    final long knownWordsUserModelForAuthorAvg = sumKnownWordsUserModelForAuthor / entryCounter;
                    final long knownWordsGeneralModelForAuthorAvg = sumKnownWordsGeneralModelForAuthor / entryCounter;

                    String name = lastAuthor;
                    name = name.substring(name.lastIndexOf(" "), name.length());

                    latexBuilder
                            .append("  ").append(name).append(" & ")
                            .append(dfZero.format(tokensDocumentModelForAuthorAvg)).append(" & ")
                            .append(dfZero.format(typesDocumentModelForAuthorAvg)).append(" & ")
                            .append(dfZero.format(unknownWordsForAuthorAvg)).append(" & ")
                            .append(dfZero.format(knownWordsUserModelForAuthorAvg)).append(" & ")
                            .append(dfZero.format(knownWordsGeneralModelForAuthorAvg)).append(" \\\\")
                            .append("\r\n");

                    final List<Double> standardDeviation = computeStandardDeviation(authorsEntries.get(lastAuthor),
                            tokensDocumentModelForAuthorAvg,
                            typesDocumentModelForAuthorAvg,
                            unknownWordsForAuthorAvg,
                            knownWordsUserModelForAuthorAvg,
                            knownWordsGeneralModelForAuthorAvg);

                    latexBuilder // "$\\sigma$ = $\\sqrt {\\mu}$"
                            .append("  ").append("$\\sigma$").append(" & ")
                            .append(dfZero.format(standardDeviation.get(0))).append(" & ")
                            .append(dfZero.format(standardDeviation.get(1))).append(" & ")
                            .append(dfZero.format(standardDeviation.get(2))).append(" & ")
                            .append(dfZero.format(standardDeviation.get(3))).append(" & ")
                            .append(dfZero.format(standardDeviation.get(4))).append(" \\\\")
                            .append("  ").append("\\hline").append("\r\n");

                    sumTokensDocumentModelForAuthor = 0;
                    sumTypesDocumentModelForAuthor = 0;
                    sumUnknownWordsForAuthor = 0;
                    sumKnownWordsUserModelForAuthor = 0;
                    sumKnownWordsGeneralModelForAuthor = 0; // fwl
                    entryCounter = 0;

                    if (!line.trim().isEmpty()) {
                        // start of first entry
                        entryCounter++;
                        sumTokensDocumentModelForAuthor += tokensDocumentModel;
                        sumTypesDocumentModelForAuthor += typesDocumentModel;
                        sumUnknownWordsForAuthor += unknownWords;
                        sumKnownWordsUserModelForAuthor += knownWordsUserModel;
                        sumKnownWordsGeneralModelForAuthor += knownWordsGeneralModel; // fwl
                    }
                }

                lastAuthor = author;
            }
        }

        long tokensDocumentModelAvg = sumTokensDocumentModel / 180;
        long typesDocumentModelAvg = sumTypesDocumentModel / 180;
        long unknownWordsAvg = sumUnknownWords / 180;
        long knownWordsUserModelAvg = sumKnownWordsUserModel / 180;
        long knownWordsGeneralModelAvg = sumKnownWordsGeneralModel / 180;

        latexBuilder.append("  ").append("\\hline").append("\r\n").append("  ").append("\\hline").append("\r\n");
        latexBuilder
                .append("  ").append("Average").append(" & ")
                .append(dfZero.format(tokensDocumentModelAvg)).append(" & ")
                .append(dfZero.format(typesDocumentModelAvg)).append(" & ")
                .append(dfZero.format(unknownWordsAvg)).append(" & ")
                .append(dfZero.format(knownWordsUserModelAvg)).append(" & ")
                .append(dfZero.format(knownWordsGeneralModelAvg)).append(" \\\\")
                .append("\r\n");

        List<String> allEntries = new LinkedList<>();
        for (List<String> strings : authorsEntries.values()) {
            allEntries.addAll(strings);
        }

        final List<Double> standardDeviation = computeStandardDeviation(allEntries,
                tokensDocumentModelAvg,
                typesDocumentModelAvg,
                unknownWordsAvg,
                knownWordsUserModelAvg,
                knownWordsGeneralModelAvg);

        latexBuilder
                .append("  ").append("$\\sigma$").append(" & ")
                .append(dfZero.format(standardDeviation.get(0))).append(" & ")
                .append(dfZero.format(standardDeviation.get(1))).append(" & ")
                .append(dfZero.format(standardDeviation.get(2))).append(" & ")
                .append(dfZero.format(standardDeviation.get(3))).append(" & ")
                .append(dfZero.format(standardDeviation.get(4))).append(" \\\\")
                .append("  ").append("\\hline").append("\r\n");

        latexBuilder.append("\\end{tabular}");

        return latexBuilder.toString();
    }

    /**
     * http://www.standardabweichung.org/berechnung.html
     * @param lines
     * @param averages
     * @return
     */
    private static List<Double> computeStandardDeviation(List<String> lines, long... averages) {
        final String SEP = ",";

        final long tokensDocumentModelAvg = averages[0];
        final long typesDocumentModelAvg = averages[1];
        final long unknownWordsAvg = averages[2];
        final long knownWordsUserModelAvg = averages[3];
        final long knownWordsGeneralModelAvg = averages[4];

        List<Double> varianzes = new LinkedList<>();
        varianzes.add(0D);
        varianzes.add(0D);
        varianzes.add(0D);
        varianzes.add(0D);
        varianzes.add(0D);

        List<Double> standardDeviation = new LinkedList<>();

        double tmp = 0D;

        for (String line : lines) {
            final String[] fields = line.split(";");
            long tokensDocumentModel = Long.parseLong(fields[1].replace(SEP, ""));
            long typesDocumentModel = Long.parseLong(fields[2].replace(SEP, ""));
            long unknownWords = Long.parseLong(fields[3].replace(SEP, ""));
            long knownWordsUserModel = Long.parseLong(fields[4].replace(SEP, ""));
            long knownWordsGeneralModel = Long.parseLong(fields[5].replace(SEP, ""));

            tmp = varianzes.get(0);
            tmp += Math.pow(tokensDocumentModel - tokensDocumentModelAvg, 2);
            varianzes.set(0, tmp);

            tmp = varianzes.get(1);
            tmp += Math.pow(typesDocumentModel - typesDocumentModelAvg, 2);
            varianzes.set(1, tmp);

            tmp = varianzes.get(2);
            tmp += Math.pow(unknownWords - unknownWordsAvg, 2);
            varianzes.set(2, tmp);

            tmp = varianzes.get(3);
            tmp += Math.pow(knownWordsUserModel - knownWordsUserModelAvg, 2);
            varianzes.set(3, tmp);

            tmp = varianzes.get(4);
            tmp += Math.pow(knownWordsGeneralModel - knownWordsGeneralModelAvg, 2);
            varianzes.set(4, tmp);
        }

        for (int i = 0; i < varianzes.size(); i++) {
            final Double val = varianzes.get(i)/lines.size();
            varianzes.set(i, val);

            standardDeviation.add(Math.sqrt(varianzes.get(i)));
            // check
            //System.out.println(varianzes.get(i));
        }

        //for (Double s : standardDeviation) {
        //    System.out.println(s);
        //}

        return standardDeviation;
    }

    /**
     * http://www.standardabweichung.org/berechnung.html
     * @param lines
     * @param averages
     * @return
     */
    private static List<Double> computeStandardDeviationForUserModel(List<String> lines, long... averages) {

        final long avgInitialTokens = averages[0];
        final long avgRemovedTokensAfterPreprocessing = averages[1];
        final long avgRemovedTokens = averages[2];
        final long avgModelTokens = averages[3];
        final long avgModelTypes = averages[4];
        final long avgAvgRank = averages[5];

        List<Double> varianzes = new LinkedList<>();
        varianzes.add(0D);
        varianzes.add(0D);
        varianzes.add(0D);
        varianzes.add(0D);
        varianzes.add(0D);
        varianzes.add(0D);

        List<Double> standardDeviation = new LinkedList<>();

        double tmp = 0D;

        for (String line : lines) {
            final String[] fields = line.split(";");
            long initialTokens = Long.parseLong(fields[1].substring(0, fields[1].indexOf(".")));
            long removedTokensAfterPreprocessing = Long.parseLong(fields[2].substring(0, fields[2].indexOf(".")));
            long removedTokens = Long.parseLong(fields[3].substring(0, fields[3].indexOf(".")));
            long modelTokens = Long.parseLong(fields[4].substring(0, fields[4].indexOf(".")));
            long modelTypes = Long.parseLong(fields[5].substring(0, fields[5].indexOf(".")));
            long avgRank = Long.parseLong(fields[6].substring(0, fields[6].indexOf(".")));

            tmp = varianzes.get(0);
            tmp += Math.pow(initialTokens - avgInitialTokens, 2);
            varianzes.set(0, tmp);

            tmp = varianzes.get(1);
            tmp += Math.pow(removedTokensAfterPreprocessing - avgRemovedTokensAfterPreprocessing, 2);
            varianzes.set(1, tmp);

            tmp = varianzes.get(2);
            tmp += Math.pow(removedTokens - avgRemovedTokens, 2);
            varianzes.set(2, tmp);

            tmp = varianzes.get(3);
            tmp += Math.pow(modelTokens - avgModelTokens, 2);
            varianzes.set(3, tmp);

            tmp = varianzes.get(4);
            tmp += Math.pow(modelTypes - avgModelTypes, 2);
            varianzes.set(4, tmp);

            tmp = varianzes.get(5);
            tmp += Math.pow(avgRank - avgAvgRank, 2);
            varianzes.set(5, tmp);
        }

        for (int i = 0; i < varianzes.size(); i++) {
            final Double val = varianzes.get(i)/lines.size();
            varianzes.set(i, val);

            standardDeviation.add(Math.sqrt(varianzes.get(i)));
            // check
            //System.out.println(varianzes.get(i));
        }

        //for (Double s : standardDeviation) {
        //    System.out.println(s);
        //}

        return standardDeviation;
    }

    public static void main(String[] args) {
        final boolean computeUserModels = true;
        final boolean computeExperiments = false;
        try {

            if (computeUserModels) {
                System.out.println(getUserModelsAsLatexTable()); // TODO: standard deviation
            }

            if (computeExperiments) {
                String finalPathExp1 = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\experiments\\exp1_2016_07_19_07_20.csv";
                String finalPathExp2 = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\backend\\evaluation\\src\\main\\resources\\experiments\\exp2_2016_07_19_05_06.csv";

                System.out.println(getExperimentsAsLatexTable(finalPathExp2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
