package at.ac.tuwien.isis.usermodels.evaluation.generator;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import org.junit.Assert;

import java.io.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by Alexej Strelzow on 29.04.2016.
 */
public class StatisticsUtils {

    public static void createTestStatistics() throws IOException, ClassNotFoundException {
        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        if (!modelsDir.isDirectory()) {
            Assert.fail();
        }

        for (File userDir : modelsDir.listFiles()) {
            if (userDir.isDirectory()) {
                for (File serializedModel : userDir.listFiles()) {
                    ObjectInputStream is = new ObjectInputStream(new FileInputStream(serializedModel));
                    UserModelDTO model = (UserModelDTO) is.readObject();

                    System.out.println("Name: " + userDir.getName());
                    System.out.println("Containing docs: " + model.getContainingDocs().size());
                    System.out.println("Avg document length: " + model.getAvgDocLength());
                    System.out.println("Avg removed tokens: " + model.getAvgRemovedTokens());
                    System.out.println("Model token size: " + model.getModel().size());
                    System.out.println("Unique token size: " + model.getModel().uniqueSet().size());

                }
            }
        }
    }

    public static String getStatisticsHeader() {
        //String USER_MODEL = "[Mu] #docs; [Mu] avg doc length; [Mu] avg removed tokens; [Mu] model terms; [Mu] model types;";
        String DOC_MODEL = "[Md] model terms; [Md] model types;";
        String RESULT = "[Res] unknown words; [Res] known Mu; [Res] known Mc";

        return "Name; " + /*USER_MODEL +*/ DOC_MODEL + RESULT;
    }

    public static String createStatistics(String name, UserModelDTO userModel, DocumentModelDTO documentModel, ComparisonResultDTO resultDTO) {
        StringBuilder sb = new StringBuilder(name + ";");

        Locale useLocal = Locale.ENGLISH;
        DecimalFormat dfZero = new DecimalFormat();
        dfZero.setMaximumFractionDigits(0);
        dfZero.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(useLocal));
        dfZero.setDecimalSeparatorAlwaysShown(false);

        // userModel
        /*
        sb.append(userModel.getContainingDocs().size()).append(";");
        sb.append(userModel.getAvgDocLength()).append(";");
        sb.append(userModel.getAvgRemovedTokens()).append(";");
        sb.append(userModel.getModel().size()).append(";");
        sb.append(userModel.getModel().uniqueSet().size()).append(";");
        */

        // documentModel
        sb.append(dfZero.format(documentModel.getModel().size())).append(";");
        sb.append(dfZero.format(documentModel.getModel().uniqueSet().size())).append(";");

        // resultDTO
        sb.append(dfZero.format(resultDTO.getUnknownWords().size())).append(";");
        sb.append(dfZero.format(resultDTO.getKnownWordsUserModel().size())).append(";");
        sb.append(dfZero.format(resultDTO.getKnownWordsCollection().size())).append(";");

        return sb.toString();
    }

    public static void writeModelStatistics() throws IOException, ClassNotFoundException {
        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        if (!modelsDir.isDirectory()) {
            Assert.fail();
        }

        StringBuilder sb = new StringBuilder("Name;Containing docs;Avg doc. length;Avg removed tokens;Model tokens;Model types;Ratio;Avg rank\r\n");

        Locale useLocal = Locale.ENGLISH;
        DecimalFormat dfZero = new DecimalFormat();
        dfZero.setMaximumFractionDigits(0);
        dfZero.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(useLocal));
        dfZero.setDecimalSeparatorAlwaysShown(false);

        DecimalFormat dfTwo = new DecimalFormat();
        dfTwo.setMaximumFractionDigits(2);
        dfTwo.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(useLocal));
        dfTwo.setDecimalSeparatorAlwaysShown(true);

        for (File userDir : modelsDir.listFiles()) {
            if (userDir.isDirectory()) {
                for (File serializedModel : userDir.listFiles()) {
                    ObjectInputStream is = new ObjectInputStream(new FileInputStream(serializedModel));
                    UserModelDTO model = (UserModelDTO) is.readObject();
               /*A*/sb.append(userDir.getName()).append(";");
               /*B*/sb.append(model.getContainingDocs().size()).append(";");
               /*C*/sb.append(dfZero.format(model.getAvgDocLength())).append(";");
               /*D*/sb.append(dfZero.format(model.getAvgRemovedTokens())).append(";");
               /*E*/sb.append(dfZero.format(model.getModel().size())).append(";");
               /*F*/sb.append(dfZero.format(model.getModel().uniqueSet().size())).append(";");
               /*G*/sb.append(dfTwo.format(model.getModel().size()/model.getModel().uniqueSet().size())).append(";");
               /*H*/sb.append(dfZero.format(model.getAvgRank())).append(";");
                }
                sb.append("\r\n");
            }
        }

        File outputFile = new File(TestConfiguration.EXPERIMENTS + "/model_statistics.csv");
        if (!outputFile.exists()) {
            outputFile.createNewFile();
        }

        PrintWriter pw = new PrintWriter(new FileOutputStream(outputFile), true);
        pw.print(sb.toString());
        pw.flush();
        pw.close();

        System.out.println("Report see: " + TestConfiguration.EXPERIMENTS + "/model_statistics.csv");
    }

    public static void main(String[] args) {
        try {

            StatisticsUtils.createTestStatistics();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
