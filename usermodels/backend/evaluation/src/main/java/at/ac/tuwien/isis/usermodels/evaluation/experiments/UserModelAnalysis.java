package at.ac.tuwien.isis.usermodels.evaluation.experiments;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlServiceImpl;
import edu.stanford.nlp.ling.TaggedWord;
import org.apache.commons.collections4.Bag;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Alexej Strelzow on 13.08.2016.
 */
public class UserModelAnalysis {

    @Test
    public void analyzeMeyer() throws IOException, ClassNotFoundException {
        String path = TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS + "\\Bertrand Meyer\\Bertrand Meyer.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(path));
        UserModelDTO userModel = (UserModelDTO) is.readObject();
        is.close();

        final Set<String> containingDocs = userModel.getContainingDocs();
        Assert.assertNotNull(containingDocs);

        printCountStatistics(userModel);

        final Bag<TaggedWordWrapper> model = userModel.getModel();
        double avgRank = computeAvgRank(model);
        System.out.println("Avg Rank: " + avgRank);

        /*
            that#WDT   1118
            this#DT   1466
            that#IN   1476
            with#IN   1862
         */

        /*
        System.out.println("Removing words...");
        remove(model, "that#WDT", "this#DT", "that#IN", "with#IN");
        avgRank = computeAvgRank(model);
        System.out.println("Avg Rank: " + avgRank);
        */

        /*
        System.out.println("Removing words...");
        remove(model, false, 1);
        avgRank = computeAvgRank(model);
        System.out.println("Avg Rank: " + avgRank);

        System.out.println("Removing words...");
        remove(model, false, 2);
        avgRank = computeAvgRank(model);
        System.out.println("Avg Rank: " + avgRank);

        System.out.println("Removing words...");
        remove(model, false, 3);
        avgRank = computeAvgRank(model);
        System.out.println("Avg Rank: " + avgRank);
        */
    }

    private static void remove(Bag<TaggedWordWrapper> model, boolean verbose, String... wordsWithPos) {
        List<String> removeMe = Arrays.asList(wordsWithPos);
        Set<TaggedWordWrapper> removeList = new HashSet<>();
        for (TaggedWordWrapper wrapper : model.uniqueSet()) {
            final String toString = wrapper.toString();
            if (removeMe.contains(toString)) {
                removeList.add(wrapper);
            }
        }

        for (TaggedWordWrapper remove : removeList) {
            model.remove(remove);
            if (verbose) {
                System.out.println("\t" + remove.toString());
            }
        }
    }

    private static void remove(Bag<TaggedWordWrapper> model, boolean verbose, Integer... frequencies) {
        List<Integer> removeMe = Arrays.asList(frequencies);
        Set<TaggedWordWrapper> removeList = new HashSet<>();
        for (TaggedWordWrapper wrapper : model.uniqueSet()) {
            final int frequency = model.getCount(wrapper);
            if (removeMe.contains(frequency)) {
                removeList.add(wrapper);
            }
        }

        for (TaggedWordWrapper remove : removeList) {
            model.remove(remove);
            if (verbose) {
                System.out.println("\t" + remove.toString());
            }
        }
    }

    private static void printCountStatistics(UserModelDTO userModel) {
        Map<String, Integer> countMap = new HashMap<>();
        Map<Integer, Integer> wordCountCountMap = new HashMap<>();

        final Bag<TaggedWordWrapper> model = userModel.getModel();
        for (TaggedWordWrapper wrapper : model) {
            final String word = wrapper.toString();
            final int count = model.getCount(wrapper);
            if (!countMap.containsKey(word)) {
                countMap.put(word, count);
            }
            if (wordCountCountMap.containsKey(count)) {
                Integer counter = wordCountCountMap.get(count);
                counter++;
                wordCountCountMap.put(count, counter);
            } else {
                wordCountCountMap.put(count, 1);
            }
        }

        countMap = sortByValue(countMap);
        wordCountCountMap = sortByValue(wordCountCountMap);

        for (String word : countMap.keySet()) {
            System.out.println(word + "   " + countMap.get(word));
        }

        System.out.println("Counts of counts of words:");

        for (Integer count : wordCountCountMap.keySet()) {
            System.out.println(count + "   " + wordCountCountMap.get(count));
        }

        System.out.println("Counts of counts of types:");

        for (Integer count : wordCountCountMap.keySet()) {
            System.out.println(count + "   " + (wordCountCountMap.get(count)/count));
        }
    }

    private static double computeAvgRank(final Bag<TaggedWordWrapper> model) {
        FwlServiceImpl fwlService = new FwlServiceImpl();
        double avgRank = 0;
        long counter = 0;

        for (TaggedWordWrapper wrapper : model.uniqueSet()) {
            final TaggedWord taggedWord = wrapper.get();
            final String valueToLower = taggedWord.value().toLowerCase();

            if (fwlService.isInFwlList(valueToLower, taggedWord.tag())) {
                avgRank += fwlService.getRank(valueToLower, taggedWord.tag());
                counter++;
            } else {
                if (fwlService.isInFwlList(valueToLower)) {
                    avgRank += fwlService.getLowestRank(valueToLower);
                    counter++;
                }
            }
        }

        avgRank = avgRank/counter;
        System.out.println("Counter: " + counter);
        return avgRank;
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map ) {
        Map<K, V> result = new LinkedHashMap<>();
        Stream<Map.Entry<K, V>> st = map.entrySet().stream();

        st.sorted( Map.Entry.comparingByValue() )
                .forEachOrdered( e -> result.put(e.getKey(), e.getValue()) );

        return result;
    }

}
