package at.ac.tuwien.isis.usermodels.evaluation.generator;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetServiceImpl;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.config.ModelGenerationConfiguration;
import at.ac.tuwien.isis.usermodels.service.langmodel.*;
import at.ac.tuwien.isis.usermodels.service.ner.NerServiceImpl;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserService;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserServiceImpl;
import at.ac.tuwien.isis.usermodels.service.protocol.FileProtocolServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.TokenizerServiceImpl;
import org.apache.commons.collections4.Bag;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
public class ModelGenerator {
    // -Xmx6144M -d64
    private PdfParserService pdfExtractor;

    public ModelGenerator() {
        pdfExtractor = new PdfParserServiceImpl();
    }

    /**
     * Extract text from pdf, which belongs to the users.
     * This lays the foundation for the user model generation.
     * @throws IOException
     */
    public void extractTextFromUserPDF(String... authorNames) throws IOException {
        List<String> filter = new ArrayList<>();
        if (authorNames != null) {
            filter = Arrays.asList(authorNames);
        }

        File rootDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT);
        if (!rootDir.isDirectory()) {
            Assert.fail();
        }

        File papersDir = new File(TestConfiguration.PAPERS);
        if (!papersDir.isDirectory()) {
            Assert.fail();
        }

        for (File userDir : papersDir.listFiles()) {
            if (userDir.isDirectory() && (filter.isEmpty() || (!filter.isEmpty() && filter.contains(userDir.getName())))) {

                File extractedUserDir = new File(rootDir, userDir.getName());
                if (!extractedUserDir.exists()) {
                    extractedUserDir.mkdir();
                }

                for (File paper : userDir.listFiles()) {
                    File newPaper = new File(extractedUserDir, paper.getName().replace(".pdf", ".txt"));
                    newPaper.setWritable(true);
                    newPaper.delete();

                    PrintWriter pw = new PrintWriter(new FileOutputStream(newPaper), true);
                    pw.print(pdfExtractor.extract(paper).getText());
                    pw.flush();
                    pw.close();
                }
            }
        }    }

    /**
     * Extract text from PDF's from the medical domain.
     * @throws IOException
     */
    public void extractTextFromMedicalPDF() throws IOException {
        File rootDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT_MED);
        if (!rootDir.isDirectory()) {
            Assert.fail();
        }

        File papersDir = new File(TestConfiguration.MEDICAL_PAPERS);
        if (!papersDir.isDirectory()) {
            Assert.fail();
        }

        for (File paper : papersDir.listFiles()) {
            File newPaper = new File(rootDir, paper.getName().replace(".pdf", ".txt"));
            newPaper.setWritable(true);
            newPaper.delete();

            PrintWriter pw = new PrintWriter(new FileOutputStream(newPaper), true);
            pw.print(pdfExtractor.extract(paper).getText());
            pw.flush();
            pw.close();
        }
    }


    /**
     * Extract text from papers from the medical domain.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void createMedicalDocumentModels() throws IOException, ClassNotFoundException {
        final String medicalPapers = TestConfiguration.MEDICAL_PAPERS;
        File paperDir = new File(medicalPapers);
        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.DOCUMENT_MODELS + "/medicine/");

        if (!paperDir.exists() || !paperDir.isDirectory()) {
            Assert.fail();
            return;
        }

        if (!modelsDir.exists()) {
            modelsDir.mkdirs();
        }

        for (File paper : paperDir.listFiles()) {
            DocumentModelServiceImpl documentModelService = new DocumentModelServiceImpl();

            setupDocumentModelService(documentModelService);

            final DocumentModelDTO documentModel = documentModelService.create(paper);
            final Map<String, Bag<String>> removedWords = documentModelService.getProtocolService().getProtocol().getRemovedWords();
            FileProtocolServiceImpl.printRemovedWords(removedWords, Configuration.EXPERIMENTS, paper.getName());

            create(modelsDir, paper.getName() + ".ser", null, documentModel);
        }
    }

    /**
     * Extract text from papers from the medical domain.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void createMedicalDocumentModelsFromText() throws IOException, ClassNotFoundException {
        File txtDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT_MED);

        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.DOCUMENT_MODELS + "/medicine/");

        if (!txtDir.exists()) {
            return;
        }

        for (File paper : txtDir.listFiles()) {
            DocumentModelServiceImpl documentModelService = new DocumentModelServiceImpl();

            setupDocumentModelService(documentModelService);

            final List<String> strings = Files.readAllLines(paper.toPath(), Charset.forName("ISO-8859-1"));
            final String content = StringUtils.join(strings.toArray(), "\r\n");

            final DocumentModelDTO documentModel = documentModelService.create(paper.getName(), content);
            final Map<String, Bag<String>> removedWords = documentModelService.getProtocolService().getProtocol().getRemovedWords();
            FileProtocolServiceImpl.printRemovedWords(removedWords, Configuration.EXPERIMENTS, paper.getName());

            create(modelsDir, paper.getName() + ".ser", null, documentModel);
        }
    }

    public void createUserModels(String... authorNames) throws IOException {
        final int PAPERS_IN_MODEL = 25;

        List<String> filter = new ArrayList<>();
        if (authorNames != null) {
            filter = Arrays.asList(authorNames);
        }

        File extractedTextDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT);
        if (!extractedTextDir.isDirectory()) {
            Assert.fail();
        }

        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        if (!modelsDir.isDirectory()) {
            Assert.fail();
        }

        for (File userDir : extractedTextDir.listFiles()) {
            if (userDir.isDirectory() && (filter.isEmpty() || (!filter.isEmpty() && filter.contains(userDir.getName())))) {
                File userModelDir = new File(modelsDir, userDir.getName());
                if (!userModelDir.exists()) {
                    userModelDir.mkdir();
                }

                Map<String, String> articles = new HashMap<>();
                UserModelServiceImpl service = new UserModelServiceImpl();

                setupUserModelService(service);

                for (File paper : getRandomFilesFromUserDir(userDir, PAPERS_IN_MODEL)) {
                    final List<String> strings = Files.readAllLines(paper.toPath(), Charset.forName("ISO-8859-1"));
                    articles.put(paper.getName(), StringUtils.join(strings.toArray(), "\r\n"));
                }

                final File outputFile = new File(userModelDir, userDir.getName() + ".ser");
                if (outputFile.exists()) {
                    outputFile.setWritable(true);
                    outputFile.delete();
                }

                final UserModelDTO userModel = service.createFromArticles(userDir.getName(), articles);
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(outputFile));
                oos.writeObject(userModel);
            }
        }
    }

    private void setupUserModelService(UserModelServiceImpl userModelService) {
        userModelService.setModelGenerationConfiguration(new ModelGenerationConfiguration());
        final BabelNetServiceImpl babelNetService = new BabelNetServiceImpl();
        babelNetService.setBabelNetConfiguration(new BabelNetConfiguration());
        userModelService.setBabelNet(babelNetService);
        userModelService.setConfiguration(new Configuration());
        userModelService.setNerService(new NerServiceImpl());
        userModelService.setTokenizer(new TokenizerServiceImpl());
        userModelService.setFwlService(new FwlServiceImpl());
        userModelService.setPreprocessor(new PreprocessorServiceImpl());
        userModelService.setProtocolService(new FileProtocolServiceImpl());
    }

    private void setupDocumentModelService(DocumentModelServiceImpl documentModelService) {
        documentModelService.setModelGenerationConfiguration(new ModelGenerationConfiguration());
        final BabelNetServiceImpl babelNetService = new BabelNetServiceImpl();
        babelNetService.setBabelNetConfiguration(new BabelNetConfiguration());
        documentModelService.setBabelNet(babelNetService);
        documentModelService.setConfiguration(new Configuration());
        documentModelService.setNerService(new NerServiceImpl());
        documentModelService.setTokenizer(new TokenizerServiceImpl());
        documentModelService.setFwlService(new FwlServiceImpl());
        documentModelService.setPreprocessor(new PreprocessorServiceImpl());
        documentModelService.setProtocolService(new FileProtocolServiceImpl());
    }

    private List<File> getRandomFilesFromUserDir(File userDir, int amount) {
        final List<File> fileList = Arrays.asList(userDir.listFiles());
        if (amount == -1) {
            return fileList;
        }

        final List<File> chosenFiles = new LinkedList<>();

        List<Integer> randomIndexes = new ArrayList<>(amount);
        while (randomIndexes.size() != amount) {
            final int randomIndex = ThreadLocalRandom.current().nextInt(0, fileList.size() - 1 + 1);
            if (randomIndexes.contains(randomIndex)) continue;
            randomIndexes.add(randomIndex);
        }

        for (Integer r : randomIndexes) {
            chosenFiles.add(fileList.get(r));
        }

        Assert.assertTrue(chosenFiles.size() == amount);
        return chosenFiles;
    }

    private void create(File modelsDir, String modelName, UserModelDTO userModel, DocumentModelDTO documentModel) throws IOException, ClassNotFoundException {
        FileOutputStream fout = new FileOutputStream(new File(modelsDir, modelName));
        ObjectOutputStream oos = new ObjectOutputStream(fout);

        if (userModel != null) {
            oos.writeObject(userModel);

            ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(modelsDir, modelName)));
            UserModelDTO model = (UserModelDTO) is.readObject();

            Assert.assertEquals(userModel.getModel().size(), model.getModel().size());
            Assert.assertEquals(userModel.getModel().uniqueSet().size(), model.getModel().uniqueSet().size());
            Assert.assertEquals(userModel.getUnseenProbability(), model.getUnseenProbability());

        } else {
            oos.writeObject(documentModel);

            ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(modelsDir, modelName)));
            DocumentModelDTO model = (DocumentModelDTO) is.readObject();

            Assert.assertEquals(documentModel.getModel().size(), model.getModel().size());
            Assert.assertEquals(documentModel.getModel().uniqueSet().size(), model.getModel().uniqueSet().size());
        }
    }

    public static void main(String[] args) {
        ModelGenerator modelGenerator = new ModelGenerator();
        boolean extractTextFromMedicalPDF = false;
        boolean medicalDocumentModels = false;

        boolean extractTextFromUserPDF = false;
        boolean userModels = true;

        try {
            if (extractTextFromMedicalPDF) {
                modelGenerator.extractTextFromMedicalPDF();
            }

            if (medicalDocumentModels) {
                modelGenerator.createMedicalDocumentModelsFromText();
                //modelGenerator.createMedicalDocumentModels();
            }

            if (extractTextFromUserPDF) {
                // "Chin-Chen Chang", "Sanjeev R. Kulkarni", "Lei Chen"
                modelGenerator.extractTextFromUserPDF();
            }

            if (userModels) {
                modelGenerator.createUserModels();//"Stefan Szeider"
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
