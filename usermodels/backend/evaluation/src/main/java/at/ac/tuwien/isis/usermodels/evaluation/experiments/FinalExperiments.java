package at.ac.tuwien.isis.usermodels.evaluation.experiments;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import at.ac.tuwien.isis.usermodels.evaluation.generator.StatisticsUtils;
import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetServiceImpl;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.config.ModelGenerationConfiguration;
import at.ac.tuwien.isis.usermodels.service.experiment.ExperimentServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.ModelComparatorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.ner.NerServiceImpl;
import at.ac.tuwien.isis.usermodels.service.protocol.DummyProtocolServiceImpl;
import at.ac.tuwien.isis.usermodels.service.protocol.FileProtocolServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.TokenizerServiceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
public class FinalExperiments extends AbstractFinalExperiments {

    // -Xmx6144M -d64
    public void performEquiDomainExperiments() throws Exception {
        final int PAPERS_PER_USER = 2;

        String fileName = "exp1_" + new SimpleDateFormat("yyyy_MM_dd_hh_mm").format(new Date()) + ".csv";
        PrintWriter printWriter = new PrintWriter(new File(TestConfiguration.EXPERIMENTS + "/" + fileName));

        Map<String, Map<String, String>> testSet = prepareFiles(PAPERS_PER_USER);
        for (String user : testSet.keySet()) {
            printWriter.println(user);
            for (String title : testSet.get(user).keySet()) {
                printWriter.println(";" + title);
            }
        }
        printWriter.flush();

        printWriter.println(StatisticsUtils.getStatisticsHeader());
        printWriter.flush();

        File modelRoot = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        for (File userDir : modelRoot.listFiles()) {
            for (File model : userDir.listFiles()) {
                ObjectInputStream is = new ObjectInputStream(new FileInputStream(model));
                UserModelDTO userModel = (UserModelDTO) is.readObject();
                is.close();

                final Map<String, String> articles = getFiles(testSet, userDir.getName());
                for (String name : articles.keySet()) {
                    final String content = articles.get(name);

                    final DocumentModelServiceImpl documentModelService = new DocumentModelServiceImpl();
                    setupDocumentModelService(documentModelService);

                    final ExperimentServiceImpl experimentService = new ExperimentServiceImpl();
                    setupExperimentService(experimentService);

                    final DocumentModelDTO documentModel = documentModelService.create(name, content);
                    final ComparisonResultDTO resultDTO = experimentService.run(userModel, documentModel, new FileProtocolServiceImpl());
                    String csvString = StatisticsUtils.createStatistics(userDir.getName(), userModel, documentModel, resultDTO);
                    printWriter.println(csvString);
                    printWriter.println("");
                    printWriter.flush();
                }
            }
        }

        printWriter.close();

        System.out.println("Results see: " + TestConfiguration.EXPERIMENTS + "/final_" + fileName);
    }

    public void performCrossDomainExperiments() throws Exception {
        String fileName = "exp2_" + new SimpleDateFormat("yyyy_MM_dd_hh_mm").format(new Date()) + ".csv";
        PrintWriter printWriter = new PrintWriter(new File(TestConfiguration.EXPERIMENTS + "/" + fileName));

        printWriter.println(StatisticsUtils.getStatisticsHeader());
        printWriter.flush();

        final List<DocumentModelDTO> medicalFiles = getMedicalFiles();

        File modelRoot = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        for (File userDir : modelRoot.listFiles()) {
            for (File model : userDir.listFiles()) {
                ObjectInputStream is = new ObjectInputStream(new FileInputStream(model));
                UserModelDTO userModel = (UserModelDTO) is.readObject();
                is.close();

                for (DocumentModelDTO documentModel : medicalFiles) {
                    ExperimentServiceImpl experimentService = new ExperimentServiceImpl();

                    setupExperimentService(experimentService);
                    System.out.println("Comparing " + userDir.getName() + " with " + documentModel.getTitle());

                    final ComparisonResultDTO resultDTO = experimentService.run(userModel, documentModel, new FileProtocolServiceImpl());
                    String csvString = StatisticsUtils.createStatistics(userDir.getName(), userModel, documentModel, resultDTO);
                    printWriter.println(csvString);
                    printWriter.println("");
                    printWriter.flush();
                }
            }
        }

        printWriter.close();

        System.out.println("Results see: " + TestConfiguration.EXPERIMENTS + "\\" + "final_" + fileName);
    }

    private void setupDocumentModelService(DocumentModelServiceImpl documentModelService) {
        documentModelService.setModelGenerationConfiguration(new ModelGenerationConfiguration());
        final BabelNetServiceImpl babelNetService = new BabelNetServiceImpl();
        babelNetService.setBabelNetConfiguration(new BabelNetConfiguration());
        documentModelService.setBabelNet(babelNetService);
        documentModelService.setConfiguration(new Configuration());
        documentModelService.setNerService(new NerServiceImpl());
        documentModelService.setTokenizer(new TokenizerServiceImpl());
        documentModelService.setFwlService(new FwlServiceImpl());
        documentModelService.setPreprocessor(new PreprocessorServiceImpl());
        documentModelService.setProtocolService(new FileProtocolServiceImpl());
    }

    private void setupExperimentService(ExperimentServiceImpl experimentService) {
        experimentService.setConfiguration(new Configuration());

        ModelComparatorServiceImpl comparatorService = new ModelComparatorServiceImpl();
        comparatorService.setConfiguration(new Configuration());
        comparatorService.setFwlService(new FwlServiceImpl());
        final BabelNetServiceImpl babelNetService = new BabelNetServiceImpl();
        babelNetService.setBabelNetConfiguration(new BabelNetConfiguration());
        comparatorService.setBabelNetService(babelNetService);

        experimentService.setModelComparatorService(comparatorService);
    }

    public static void main(String[] args) {
        FinalExperiments finalExperiments = new FinalExperiments();
        try {

            finalExperiments.performEquiDomainExperiments();

            finalExperiments.performCrossDomainExperiments();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
