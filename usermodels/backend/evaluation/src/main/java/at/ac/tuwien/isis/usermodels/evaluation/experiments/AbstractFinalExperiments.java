package at.ac.tuwien.isis.usermodels.evaluation.experiments;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alexej Strelzow on 22.04.2016.
 */
public abstract class AbstractFinalExperiments {

    // -Xmx6144M -d64

    protected static final String USER_MODELS = TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS;
    protected static final String DOCUMENT_MODELS = TestConfiguration.RESOURCE_ROOT + TestConfiguration.DOCUMENT_MODELS;

    protected static DocumentModelDTO getCancer() throws IOException, ClassNotFoundException {
        String path = DOCUMENT_MODELS + "/medicine/cancer.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (DocumentModelDTO) is.readObject();
    }

    protected Map<String, String> getArticlesForUser(String user) throws IOException {
        File extractedTextDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT);

        for (File userDir : extractedTextDir.listFiles()) {
            if (userDir.isDirectory() && user.equals(userDir.getName())) {
                Map<String, String> articles = new HashMap<>();

                for (File paper : userDir.listFiles()) {
                    final List<String> strings = Files.readAllLines(paper.toPath(), Charset.forName("ISO-8859-1"));
                    articles.put(paper.getName(), StringUtils.join(strings.toArray(), "\r\n"));
                }

                return articles;
            }
        }

        return null;
    }

    protected Map<String, String> getRandomArticleFromUser(String user) throws IOException {
        File extractedTextDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT);
        for (File userDir : extractedTextDir.listFiles()) {
            if (userDir.isDirectory() && user.equals(userDir.getName())) {
                final File[] files = userDir.listFiles();

                final int randomIndex = ThreadLocalRandom.current().nextInt(0, files.length);
                final File file = files[randomIndex];

                Map<String, String> articles = new HashMap<>();
                final List<String> strings = Files.readAllLines(file.toPath(), Charset.forName("ISO-8859-1"));
                articles.put(file.getName(), StringUtils.join(strings.toArray(), "\r\n"));

                return articles;
            }
        }

        return null;
    }

    protected List<DocumentModelDTO> getMedicalFiles() throws IOException, ClassNotFoundException {
        List<DocumentModelDTO> modelList = new LinkedList<>();

        File modelsDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.DOCUMENT_MODELS + "/medicine/");
        for (File model : modelsDir.listFiles()) {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(model));
            DocumentModelDTO modelDTO = (DocumentModelDTO) is.readObject();
            modelList.add(modelDTO);
        }

        return modelList;
    }

    protected Map<String, Map<String, String>> prepareFiles(int papersPerUser) throws IOException {
        Map<String, Map<String, String>> map = new HashMap<>();

        File modelRoot = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        for (File userDir : modelRoot.listFiles()) {
            final Map<String, String> articles = getFiles(userDir.getName(), papersPerUser);
            map.put(userDir.getName(), articles);
        }

        return map;
    }

    protected Map<String, String> getFiles(Map<String, Map<String, String>> map, String excludeAuthor) throws IOException {
        final Map<String, String> articles = new HashMap<>();

        for (String author : map.keySet()) {
            if (!author.equals(excludeAuthor)) {
                articles.putAll(map.get(author));
            }
        }

        return articles;
    }

    protected Map<String, String> getFiles(String author, int amount) throws IOException {
        final Map<String, String> articles = new HashMap<>();

        File userDir = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT + "/" + author);
        articles.putAll(getRandomFilesFromUserDir(userDir, amount));

        return articles;
    }

    protected Map<String, String> getRandomFilesFromUserDir(File userDir, int amount) throws IOException {
        final Map<String, String> articles = new HashMap<>();
        final List<File> fileList = Arrays.asList(userDir.listFiles());

        List<Integer> randomIndexes = new ArrayList<>(amount);
        while (randomIndexes.size() != amount) {
            final int randomIndex = ThreadLocalRandom.current().nextInt(0, fileList.size() - 1 + 1);
            if (randomIndexes.contains(randomIndex)) continue;
            randomIndexes.add(randomIndex);
        }

        for (Integer r : randomIndexes) {
            final File file = fileList.get(r);
            final List<String> strings = Files.readAllLines(file.toPath(), Charset.forName("ISO-8859-1"));
            articles.put(file.getName(), StringUtils.join(strings.toArray(), "\r\n"));
        }

        Assert.assertTrue(articles.size() == amount);

        return articles;
    }

}
