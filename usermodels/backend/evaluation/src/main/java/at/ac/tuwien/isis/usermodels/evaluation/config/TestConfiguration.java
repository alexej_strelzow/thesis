package at.ac.tuwien.isis.usermodels.evaluation.config;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
public class TestConfiguration {
    private static final String PROJECT_ROOT = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\";
    public static final String RESOURCE_ROOT = PROJECT_ROOT + "backend\\evaluation\\src\\main\\resources\\";

    public static final String PAPERS = PROJECT_ROOT + "papers";
    public static final String MEDICAL_PAPERS = PAPERS + "\\_Medical Domain";

    public static final String PDF = "pdf";
    public static final String TXT = "txt";
    public static final String TXT_MED = "txt_med";
    public static final String USER_MODELS = "user_models";
    public static final String DOCUMENT_MODELS = "doc_models";
    public static final String EXPERIMENTS = RESOURCE_ROOT + "experiments";
}
