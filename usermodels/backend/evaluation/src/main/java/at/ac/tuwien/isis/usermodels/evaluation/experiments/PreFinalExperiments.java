package at.ac.tuwien.isis.usermodels.evaluation.experiments;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.evaluation.config.TestConfiguration;
import at.ac.tuwien.isis.usermodels.service.experiment.ExperimentService;
import at.ac.tuwien.isis.usermodels.service.experiment.ExperimentServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.UserModelServiceImpl;
import at.ac.tuwien.isis.usermodels.service.protocol.FileProtocolServiceImpl;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alexej Strelzow on 22.04.2016.
 */
public class PreFinalExperiments extends AbstractFinalExperiments {

    @Test
    public void test2AllAlpha() throws Exception {
        //Map<String, Bag<TaggedWordWrapper>> result = new LinkedHashMap<>();
        Map<String, String> result = new LinkedHashMap<>();

        final Map<String, String> randomArticleFromUser = getRandomArticleFromUser("Jim Gray");
        final String randomArticleName = randomArticleFromUser.keySet().iterator().next();
        final String randomArticleContent = randomArticleFromUser.keySet().iterator().next();

        DocumentModelService document = new DocumentModelServiceImpl();
        final DocumentModelDTO documentModel = document.create(randomArticleName, randomArticleContent);

        File modelRoot = new File(TestConfiguration.RESOURCE_ROOT + TestConfiguration.USER_MODELS);
        for (File dir : modelRoot.listFiles()) {
            if (dir.isDirectory()) {
                for (File model : dir.listFiles()) {
                    ObjectInputStream is = new ObjectInputStream(new FileInputStream(model));
                    UserModelDTO userModel = (UserModelDTO) is.readObject();
                    final ComparisonResultDTO resultDTO = new ExperimentServiceImpl().run(userModel, documentModel, new FileProtocolServiceImpl());
                    //result.put(model.getName(), resultDTO.getUnknownWords());
                    result.put(model.getName(), String.valueOf(resultDTO.getUnknownWords().size()));
                }
            }
        }

        System.out.println("Random article from Jim Gray: " + randomArticleName);

        for (String name : result.keySet()) {
            //System.out.println(name + " -> " + result.get(name).size());
            System.out.println(name + " -> " + result.get(name));

        }
    }

    @Test
    public void testPrepareFiles() throws IOException {
        final Map<String, Map<String, String>> testSet = prepareFiles(25);
        final Map<String, String> all = new HashMap<>();

        int counter = 0;
        for (String author : testSet.keySet()) {
            final Map<String, String> stringStringMap = testSet.get(author);
            Assert.assertTrue(stringStringMap.size() == 25);
            counter += stringStringMap.size();

            all.putAll(stringStringMap);
        }

        Assert.assertTrue(counter == 250);
        Assert.assertTrue(all.size() == 250);

        final Map<String, String> filesSzeider = getFiles(testSet, "Stefan Szeider");
        Assert.assertTrue(filesSzeider.size() == (counter - 25));

        final Map<String, String> filesGray = getFiles(testSet, "Jim Gray");
        Assert.assertTrue(filesGray.size() == (counter - 25));

        Assert.assertNotEquals(filesSzeider, filesGray);
        Assert.assertNotEquals(filesSzeider.hashCode(), filesGray.hashCode());

        String contentSzeider = "";
        for (String content : filesSzeider.values()) {
            contentSzeider += content;
        }

        String contentGray = "";
        for (String content : filesGray.values()) {
            contentGray += content;
        }

        Assert.assertNotEquals(contentSzeider, contentGray);

    }

    @Test
    public void testSzeider_1() throws Exception {
        final Map<String, String> articles = getArticlesForUser("Stefan Szeider");
        final int randomIndex = ThreadLocalRandom.current().nextInt(0, articles.size());

        Assert.assertTrue(randomIndex >= 0 && randomIndex < articles.size());

        String filteredArticleName = new ArrayList<>(articles.keySet()).get(randomIndex);
        String filteredArticleText = new ArrayList<>(articles.values()).get(randomIndex);

        int oldSize = articles.size();
        articles.remove(filteredArticleName);

        Assert.assertEquals(oldSize - 1, articles.size());

        UserModelServiceImpl service = new UserModelServiceImpl();
        final UserModelDTO userModelMinusOne = service.createFromArticles("szeider", articles);

        Assert.assertNotNull(userModelMinusOne);

        DocumentModelService document = new DocumentModelServiceImpl();
        final DocumentModelDTO documentModel = document.create(filteredArticleName, filteredArticleText);

        Assert.assertNotNull(documentModel);

        ExperimentService experimentService = new ExperimentServiceImpl();
        final ComparisonResultDTO resultDTO = experimentService.run(userModelMinusOne, documentModel, new FileProtocolServiceImpl());
        System.out.println("testSzeider_1: " + resultDTO.getUnknownWords().size()); // -> 7
    }

    @Test
    public void testSzeider_2() throws Exception {
        final Map<String, String> randomArticleFromUser = getRandomArticleFromUser("Jim Gray");
        final String randomArticleName = randomArticleFromUser.keySet().iterator().next();
        final String randomArticleContent = randomArticleFromUser.keySet().iterator().next();

        DocumentModelService document = new DocumentModelServiceImpl();
        final DocumentModelDTO documentModel = document.create(randomArticleName, randomArticleContent);

        final ComparisonResultDTO resultDTO =  new ExperimentServiceImpl().run(getSzeider(), documentModel, new FileProtocolServiceImpl());
        System.out.println("testSzeider_2: " + resultDTO.getUnknownWords().size()); // -> 0
    }

    @Test
    public void testSzeider_3() throws Exception {
        final ComparisonResultDTO resultDTO =  new ExperimentServiceImpl().run(getSzeider(), getCancer(), new FileProtocolServiceImpl());
        System.out.println("testSzeider_3: " + resultDTO.getUnknownWords().size()); // -> 141
    }

    @Test
    public void testSzeiderVsKahndi() throws Exception {
        Bag<TaggedWordWrapper> khandaniDiff =  new ExperimentServiceImpl().run(getKhandani(), getCancer(), new FileProtocolServiceImpl()).getUnknownWords();
        Bag<TaggedWordWrapper> szeiderDiff =  new ExperimentServiceImpl().run(getSzeider(), getCancer(), new FileProtocolServiceImpl()).getUnknownWords();

        final Set<TaggedWordWrapper> szeiderUniqueSet = szeiderDiff.uniqueSet();
        final Set<TaggedWordWrapper> khandaniUniqueSet = khandaniDiff.uniqueSet();

        if (szeiderUniqueSet.size() == khandaniUniqueSet.size()) {
            System.out.println("Szeider == Khandani: " + szeiderUniqueSet.size());

        } else if (szeiderUniqueSet.size() > khandaniUniqueSet.size()) {
            System.out.println("Szeider > Khandani: " + szeiderUniqueSet.size() + " vs. " + khandaniUniqueSet.size());

        } else {
            System.out.println("Szeider < Khandani: " + szeiderUniqueSet.size() + " vs. " + khandaniUniqueSet.size());
        }

        Bag<TaggedWordWrapper> intersection = new HashBag<>(khandaniDiff);
        intersection.retainAll(szeiderDiff);

        Bag<TaggedWordWrapper> khandaniDiffCopy = new HashBag<>(khandaniDiff);
        Bag<TaggedWordWrapper> szeiderDiffCopy = new HashBag<>(szeiderDiff);

        szeiderDiffCopy.removeAll(khandaniDiff);
        khandaniDiffCopy.removeAll(szeiderDiff);


        System.out.println("Intersection " + intersection.size() + ":");
        for (TaggedWordWrapper wrapper : intersection) {
            System.out.println("\t" + wrapper.get().word());
        }

        System.out.println("\r\nKhandani " + khandaniDiffCopy.size() + ":");
        for (TaggedWordWrapper wrapper : khandaniDiffCopy) {
            System.out.println("\t" + wrapper.get().word());
        }

        System.out.println("\r\nSzeider " + szeiderDiffCopy.size() + ":");
        for (TaggedWordWrapper wrapper : szeiderDiffCopy) {
            System.out.println("\t" + wrapper.get().word());
        }

    }

    private static UserModelDTO getKhandani() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Amir K. Khandani/Amir K. Khandani.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getMontanari() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Andrea Montanari/Andrea Montanari.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getMeyer() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Bertrand Meyer/Bertrand Meyer.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getChang() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Chin-Chen Chang/Chin-Chen Chang.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getGray() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Jim Gray/Jim Gray.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getChen() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Lei Chen/Lei Chen.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getNasipuri() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Mita Nasipuri/Mita Nasipuri.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getWenger() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Philippe Wenger/Philippe Wenger.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getKulkarni() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Sanjeev R. Kulkarni/Sanjeev R. Kulkarni.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

    private static UserModelDTO getSzeider() throws IOException, ClassNotFoundException {
        String path = USER_MODELS + "/Stefan Szeider/Stefan Szeider.ser";
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(path)));
        return (UserModelDTO) is.readObject();
    }

}
