package at.ac.tuwien.isis.usermodels.dto;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * Created by Alexej Strelzow on 07.05.2016.
 */
public class Utils {

    public static byte[] stringToBytes(String string) {
        return string.getBytes(Charset.forName("UTF-8"));
    }

    public static String stringFromBytes(byte[] bytes) throws UnsupportedEncodingException {
        return new String(bytes, "UTF-8");
    }

}
