package at.ac.tuwien.isis.usermodels.dto;

import lombok.Value;

@Value
public class ErrorResponse {
    private String code;
    private String message;
}
