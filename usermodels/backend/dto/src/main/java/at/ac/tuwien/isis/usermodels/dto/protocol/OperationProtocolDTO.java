package at.ac.tuwien.isis.usermodels.dto.protocol;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Data;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
@Data
public class OperationProtocolDTO extends AbstractDTO {

    private String operation;
    private String operationMetaData;
    private long startMs;
    private long endMs;
    private String textBefore;
    private String textAfter;

    public OperationProtocolDTO(String operation, String operationMetaData, long startMs, long endMs, String textBefore, String textAfter) {
        this.operation = operation;
        this.operationMetaData = operationMetaData;
        this.startMs = startMs;
        this.endMs = endMs;
        this.textBefore = textBefore;
        this.textAfter = textAfter;
    }

    @Override
    public String toString() {
        return "\tOperationProtocolDTO {" +
                "\r\n\t\toperation: " + operation +
                "\r\n\t\tduration: " + (endMs - startMs) + "ms" +
                "\r\n\t}";
    }
}
