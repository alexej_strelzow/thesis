package at.ac.tuwien.isis.usermodels.dto;

import edu.stanford.nlp.ling.CoreLabel;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Created by Alexej Strelzow on 11.03.2016.
 */
public class CoreLabelWrapper implements IWrapper {

    private CoreLabel coreLabel;
    private double probability;
    private double smoothedProbability;


    public CoreLabelWrapper(CoreLabel coreLabel) {
        this.coreLabel = coreLabel;
    }

    @Override
    public CoreLabel get() {
        return coreLabel;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public void setSmoothedProbability(double smoothedProbability) {
        this.smoothedProbability = smoothedProbability;
    }

    @Override
    public double getProbability() {
        return probability;
    }

    @Override
    public double getSmoothedProbability() {
        return smoothedProbability;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CoreLabelWrapper))
            return false;
        if (obj == this)
            return true;

        CoreLabelWrapper objWrapper = (CoreLabelWrapper)obj;

        // if deriving: appendSuper(super.equals(obj)).
        return new EqualsBuilder()
                .append(this.coreLabel.value(), objWrapper.coreLabel.value())
                .append(this.coreLabel.category(), objWrapper.coreLabel.category())
                .append(this.coreLabel.tag(), objWrapper.coreLabel.tag())
                .isEquals();

        /*
        final boolean sameValue = StringUtils.equals(this.coreLabel.value(), objWrapper.coreLabel.value());
        final boolean sameCategory = StringUtils.equals(this.coreLabel.category(), objWrapper.coreLabel.category());
        final boolean sameTag = StringUtils.equals(this.coreLabel.tag(), objWrapper.coreLabel.tag());

        return sameValue && sameCategory && sameTag;
        */
    }

    @Override
    public int hashCode() {
        return coreLabel.value().hashCode();
    }
}
