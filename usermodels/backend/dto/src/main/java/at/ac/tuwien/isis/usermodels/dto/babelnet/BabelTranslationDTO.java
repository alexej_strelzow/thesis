package at.ac.tuwien.isis.usermodels.dto.babelnet;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Getter;

import java.util.List;

/**
 * Created by Alexej Strelzow on 16.10.2014.
 */
public class BabelTranslationDTO extends AbstractDTO {

    @Getter
    private String language;

    @Getter
    private List<String> translations;

    public BabelTranslationDTO(String language, List<String> translations) {
        this.language = language;
        this.translations = translations;
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();
        toStringBuilder.append(
                "{ " +
                    "language =  " + language + " ;  " +
                    "translations = " + translations.toString() +
                "}");
        return toStringBuilder.toString();
    }

}