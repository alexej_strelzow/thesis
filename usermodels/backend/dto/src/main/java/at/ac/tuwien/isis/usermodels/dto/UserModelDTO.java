package at.ac.tuwien.isis.usermodels.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.Bag;

import java.util.Map;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public class UserModelDTO extends AbstractModelDTO {

    @Getter
    private String name;

    @Getter
    @Setter
    private double initalTokenSize;

    @Getter
    @Setter
    private double tokenSizeAfterPreprocessing;

    @Getter
    @Setter
    private double avgRemovedTokens;

    @Getter
    @Setter
    private double avgRemovedTokensAfterPreprocessing;

    @Getter
    @Setter
    private Set<String> containingDocs;

    @Getter
    @Setter
    private double avgDocLength;

    @Getter
    @Setter
    private Map<String, Double> wordToSmoothedProbability;

    @Getter
    @Setter
    private Map<Integer, Integer> frequencyOfFrequency;

    @Getter
    @Setter
    private long countSum;

    @Getter
    @Setter
    private double unseenProbability; // result of smoothing

    @Getter
    @Setter
    private double avgSmoothedProbability;

    @Getter
    @Setter
    private double avgRank;

    // from persistence
    public UserModelDTO(Long id, String name, Bag<TaggedWordWrapper> model) {
        this.id = id;
        this.name = name;
        this.model = model;
    }

    // not persistent yet
    public UserModelDTO(String name, Bag<TaggedWordWrapper> model) {
        this.name = name;
        this.model = model;
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();

        if (this.id != null) {
            toStringBuilder.append("From Table with ID: " + this.id);

        } else {
            toStringBuilder.append("From File with name: " + this.name);
        }

        return toStringBuilder.toString();
    }

}
