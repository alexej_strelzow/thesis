package at.ac.tuwien.isis.usermodels.dto.protocol;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Data;

import java.util.Map;

/**
 * Created by Alexej Strelzow on 23.04.2016.
 */
@Data
public class DocumentDTO extends AbstractDTO {

    private String sourceType;
    private String text;
    private String textWithPageMarkup;
    private Map<Integer, String> pages; // # -> content
    private String title;
    private int pageCount;

    @Override
    public String toString() {
        return this.title;
    }
}
