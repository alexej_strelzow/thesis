package at.ac.tuwien.isis.usermodels.dto.babelnet;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Getter;

/**
 * Created by Alexej Strelzow on 29.10.2014.
 */
public class BabelRelatedSynsetDTO extends AbstractDTO {

    @Getter
    private String babelId;

    @Getter
    private String word;

    @Getter
    private String pointer;

    @Getter
    private String pointerName;

    public BabelRelatedSynsetDTO(String babelId, String word, String pointer, String pointerName) {
        this.babelId = babelId;
        this.word = word;
        this.pointer = pointer;
        this.pointerName = pointerName;
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();
        toStringBuilder.append(
            "{ " +
                "babelId =  " + babelId + " ;  " +
                "word = " + word + " ; " +
                "pointer = " + pointer + " ; " +
            "}");
        return toStringBuilder.toString();
    }
}
