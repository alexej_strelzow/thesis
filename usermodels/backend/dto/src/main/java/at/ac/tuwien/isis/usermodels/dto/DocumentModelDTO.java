package at.ac.tuwien.isis.usermodels.dto;

import lombok.Getter;
import org.apache.commons.collections4.Bag;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public class DocumentModelDTO extends AbstractModelDTO {

    @Getter
    private String title;

    @Getter
    private Long rawDocId;

    // from persistence
    public DocumentModelDTO(String title, Long rawDocId, Long id, Bag<TaggedWordWrapper> model) {
        this.title = title;
        this.rawDocId = rawDocId;
        this.id = id;
        this.model = model;
    }

    // not persistent yet
    public DocumentModelDTO(Bag<TaggedWordWrapper> model) {
        this.model = model;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("DocumentModelDTO {\r\n");
        sb.append("\trawDocId: " + rawDocId + ",\r\n");
        sb.append("\tid: " + id + ",\r\n");
        sb.append("\tmodel size: " + model.size() + "\r\n");
        sb.append("}");
        return sb.toString();
    }
}
