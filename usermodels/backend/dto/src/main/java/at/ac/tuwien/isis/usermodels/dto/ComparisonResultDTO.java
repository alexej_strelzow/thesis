package at.ac.tuwien.isis.usermodels.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.Bag;

/**
 * Created by Alexej Strelzow on 29.04.2016.
 */
public class ComparisonResultDTO extends AbstractDTO {

    @Getter
    @Setter
    private Long experimentId;

    @Getter
    @Setter
    private Long userModelId;

    @Getter
    @Setter
    private Long documentModelId;

    @Getter
    private Bag<TaggedWordWrapper> unknownWords;

    @Getter
    @Setter
    private Bag<TaggedWordWrapper> knownWordsUserModel;

    @Getter
    @Setter
    private Bag<TaggedWordWrapper> knownWordsCollection;


    public ComparisonResultDTO(Bag<TaggedWordWrapper> unknownWords) {
        this.unknownWords = unknownWords;
    }

    @Override
    public String toString() {
        return "ComparisonResultDTO {";
    }
}
