package at.ac.tuwien.isis.usermodels.dto;

/**
 * Created by Alexej Strelzow on 23.04.2016.
 */
public enum SourceType {

    TXT,

    PDF,

    UNKNOWN

}
