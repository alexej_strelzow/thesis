package at.ac.tuwien.isis.usermodels.dto.babelnet;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Getter;

/**
 * Created by Alexej Strelzow on 17.10.2014.
 */
public class BabelImageDTO extends AbstractDTO {

    @Getter
    private String imageURL;

    @Getter
    private String name;

    @Getter
    private String language;

    public BabelImageDTO(String imageURL, String name, String language) {
        this.imageURL = imageURL;
        this.name = name;
        this.language = language;
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();
        toStringBuilder.append(
                "{ " +
                    "imageURL =  " + imageURL + " ;  " +
                    "name = " + name + " ; " +
                    "language = " + language +
                "}");
        return toStringBuilder.toString();
    }
}
