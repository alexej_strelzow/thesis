package at.ac.tuwien.isis.usermodels.dto;

import org.apache.commons.collections4.Bag;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public class AbstractModelDTO extends AbstractDTO {

    protected Long id;

    // content
    protected Bag<TaggedWordWrapper> model;

    public Bag<TaggedWordWrapper> getModel() {
        return model;
    }

    public Long getId() { return id; }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " with id: " + id;
    }
}
