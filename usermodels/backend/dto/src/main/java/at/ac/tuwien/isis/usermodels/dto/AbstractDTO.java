package at.ac.tuwien.isis.usermodels.dto;

import java.io.Serializable;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public abstract class AbstractDTO implements Serializable {

    public abstract String toString();
}
