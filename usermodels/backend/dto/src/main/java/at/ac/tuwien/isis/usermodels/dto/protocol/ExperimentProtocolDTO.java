package at.ac.tuwien.isis.usermodels.dto.protocol;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.Bag;

import java.util.Map;

/**
 * Created by Alexej Strelzow on 10.04.2016.
 */
public class ExperimentProtocolDTO extends AbstractDTO {

    @Getter
    @Setter
    private Map<String, DocumentProtocolDTO> documentProtocols;

    @Getter
    @Setter
    private Map<String, Bag<String>> removedWords;

    @Getter
    @Setter
    private Map<String, Bag<String>> unknownWords;

    @Override
    public String toString() {
        return "";
    }
}
