package at.ac.tuwien.isis.usermodels.dto.babelnet;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexej Strelzow on 14.05.2016.
 */
public class BabelNetWordDTO extends AbstractDTO {

    @Getter
    private String word;

    /** see edu.mit.jwi.item.POS enum */
    @Getter
    private String pos;

    /** see it.uniroma1.lcl.jlt.util.Language enum */
    @Getter
    @Setter
    private String srcLanguage;

    /** see it.uniroma1.lcl.jlt.util.Language enum */
    @Getter
    @Setter
    private List<String> destLanguages;

    /** see it.uniroma1.lcl.babelnet.Source enum */
    @Getter
    @Setter
    private List<String> source;

    public BabelNetWordDTO(String word, String pos) {
        this.word = word;
        this.pos = pos;
        // default values
        this.srcLanguage = "EN";
        destLanguages = new ArrayList<>();
        destLanguages.add(this.srcLanguage);
        source = new ArrayList<>();
    }

    @Override
    public String toString() {
        return word + "#" + pos;
    }
}
