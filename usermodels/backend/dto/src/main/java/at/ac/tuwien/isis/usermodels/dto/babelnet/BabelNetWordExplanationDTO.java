package at.ac.tuwien.isis.usermodels.dto.babelnet;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Data;

import java.util.List;

/**
 * Created by Alexej Strelzow on 14.05.2016.
 */
@Data
public class BabelNetWordExplanationDTO extends AbstractDTO {

    private String synsetId;

    private String language;

    private String word;

    private String pos;

    private List<BabelImageDTO> imageURLs;

    private List<BabelTranslationDTO> translationDTOs;

    private List<String> wikiURLs;

    private List<BabelGlossDTO> glossDTOs;

    private List<BabelRelatedTermsDTO> relatedTermsDTOs;

    private List<BabelRelatedSynsetDTO> relatedSynsetDTOs;

}
