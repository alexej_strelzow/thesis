package at.ac.tuwien.isis.usermodels.dto;

import edu.stanford.nlp.ling.TaggedWord;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Alexej Strelzow on 11.03.2016.
 */
public class TaggedWordWrapper implements IWrapper, Comparable<TaggedWordWrapper> {

    private TaggedWord taggedWord;

    @Getter
    @Setter
    private double probability;

    @Getter
    @Setter
    private double smoothedProbability;

    @Getter
    private Set<Integer> pages;

    public TaggedWordWrapper(TaggedWord taggedWord) {
        this.taggedWord = taggedWord;
        this.pages = new TreeSet<>();
    }

    @Override
    public TaggedWord get() {
        return taggedWord;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TaggedWordWrapper))
            return false;
        if (obj == this)
            return true;

        TaggedWordWrapper objWrapper = (TaggedWordWrapper)obj;

        // if deriving: appendSuper(super.equals(obj)).
        return new EqualsBuilder()
                .append(this.taggedWord.value(), objWrapper.taggedWord.value())
                .append(this.taggedWord.tag(), objWrapper.taggedWord.tag())
                .isEquals();
    }

    public void addPage(int page) {
        pages.add(page);
    }

    @Override
    public int hashCode() {
        return taggedWord.value().hashCode();
    }

    @Override
    public int compareTo(TaggedWordWrapper o) {
        return toString().compareTo(o.toString());
    }

    @Override
    public String toString() {
        return taggedWord.value() + "#" + taggedWord.tag();
    }
}
