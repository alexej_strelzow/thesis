package at.ac.tuwien.isis.usermodels.dto.babelnet;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Getter;

/**
 * Created by Alexej Strelzow on 16.10.2014.
 */
public class BabelGlossDTO extends AbstractDTO {

    @Getter
    private String gloss;

    /** see BabelSenseSource enum */
    @Getter
    private String babelSenseSource;

    /** see Language enum */
    @Getter
    private String srcLanguage;

    @Getter
    private String senseId;

    public BabelGlossDTO(String gloss, String babelSenseSource, String srcLanguage, String senseId) {
        this.gloss = gloss;
        this.babelSenseSource = babelSenseSource;
        this.srcLanguage = srcLanguage;
        this.senseId = senseId;
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();
        toStringBuilder.append(
                "{ " +
                    "gloss =  " + gloss + " ;  " +
                    "source = " + babelSenseSource + " ; " +
                    "srcLanguage = " + srcLanguage + " ; " +
                    "senseId = " + senseId +
                "}");
        return toStringBuilder.toString();
    }

}
