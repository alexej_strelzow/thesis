package at.ac.tuwien.isis.usermodels.dto.protocol;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;

import java.util.*;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
public class DocumentProtocolDTO extends AbstractDTO implements Iterable<OperationProtocolDTO> {

    private List<OperationProtocolDTO> entries;
    private Map<String, String> diffs;
    private Map<String, Integer> posCount;

    public DocumentProtocolDTO() {
        entries = new LinkedList<>();
        diffs = new HashMap<>();
        posCount = new HashMap<>();
    }

    public void addOperationProtocol(OperationProtocolDTO entryDTO) {
        entries.add(entryDTO);
    }

    public void addDiff(String name, String diff) {
        diffs.put(name, diff);
    }

    public String getDiff(String name) {
        return diffs.get(name);
    }

    public Collection<String> getDiffs() {
        return Collections.unmodifiableCollection(diffs.values());
    }

    public void addWord(String word, String pos) {
        if (posCount.containsKey(pos)) {
            Integer counter = posCount.get(pos);
            posCount.put(pos, ++counter);
        } else {
            posCount.put(pos, 0);
        }
    }

    public Map<String, Integer> getPosCount() {
        return Collections.unmodifiableMap(posCount);
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder("DocumentProtocolDTO {\r\n");
        toStringBuilder.append("\tduration: " + (entries.get(entries.size()-1).getEndMs() - entries.get(0).getStartMs()) + "ms\r\n");
        toStringBuilder.append("\tentries: " + entries.size()).append("\r\n");
        for (OperationProtocolDTO operation : entries) {
            toStringBuilder.append(operation.toString()).append("\r\n");
        }
        toStringBuilder.append("}\r\n");

        return toStringBuilder.toString();
    }

    @Override
    public Iterator<OperationProtocolDTO> iterator() {
        return entries.iterator();
    }

}
