package at.ac.tuwien.isis.usermodels.dto;

import edu.stanford.nlp.ling.Label;

import java.io.Serializable;

/**
 * Created by Alexej Strelzow on 11.03.2016.
 */
public interface IWrapper extends Serializable {

    Label get();

    double getProbability();

    double getSmoothedProbability();

}
