package at.ac.tuwien.isis.usermodels.dto.babelnet;

import at.ac.tuwien.isis.usermodels.dto.AbstractDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alexej Strelzow on 16.10.2014.
 */
public class BabelRelatedTermsDTO extends AbstractDTO {

    @Getter
    private String senseKey;

    @Getter
    private String word;

    /** see POS enum */
    @Getter
    private String pos;

    /** see Language enum */
    @Getter
    @Setter
    private String srcLanguage;

    /** see BabelSenseSource */
    @Getter
    @Setter
    private String babelSenseSource;

    public BabelRelatedTermsDTO(String senseKey, String word, String pos) {
        this.senseKey = senseKey;
        this.word = word;
        this.pos = pos;
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();
        toStringBuilder.append(
                "{ " +
                    "senseKey =  " + senseKey + " ;  " +
                    "word = " + word + " ; " +
                    "pos = " + pos + " ; " +
                    "srcLanguage = " + srcLanguage + " ; " +
                    "source = " + babelSenseSource +
                "}");
        return toStringBuilder.toString();
    }

}
