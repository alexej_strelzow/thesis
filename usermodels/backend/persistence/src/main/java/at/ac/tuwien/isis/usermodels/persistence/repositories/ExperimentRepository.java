package at.ac.tuwien.isis.usermodels.persistence.repositories;

import at.ac.tuwien.isis.usermodels.persistence.tables.Experiment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExperimentRepository extends JpaRepository<Experiment, Long> {
    
}
