package at.ac.tuwien.isis.usermodels.persistence.tables;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "document_model")
@ToString
public class DocumentModel {

    public DocumentModel() {
        super();
    }

    public DocumentModel(RawDocument document, String options, byte[] content) {
        this.document = document;
        this.options = options;
        this.content = content;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @NotNull
    @OneToOne(cascade=CascadeType.ALL) // persist RawDocument automatically
    @JoinColumn(name="id")
    @JsonIgnore
    @Getter
    private RawDocument document;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "documents")
    //@Column(name = "user_models")
    @Getter
    private Set<UserModel> userModels;

    @Getter
    private String options;

    @Lob
    @NotNull
    @Getter
    private byte[] content;

    @NotNull
    @Column(name = "created_at")
    @Getter
    private Date createdAt;

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }
}
