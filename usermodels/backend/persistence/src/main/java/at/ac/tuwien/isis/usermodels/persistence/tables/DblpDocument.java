package at.ac.tuwien.isis.usermodels.persistence.tables;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "dblp_document")
@ToString
@Data
public class DblpDocument extends RawDocument {

    public DblpDocument() {
        super();
    }

    public DblpDocument(String title, String contentType, byte[] content) {
        super(title, contentType, content);
    }

    public DblpDocument(/*Author author, */String dblpId, String title, String doi, String ee, String contentType, byte[] content) {
        this(title, contentType, content);
        //this.author = author;
        this.dblpId = dblpId;
        this.doi = doi;
        this.ee = ee;
    }

    //@NotNull
    //@ManyToOne
    //private Author author;

    @Column(name = "dblp_id")
    private String dblpId;

    private String doi;

    private String ee;

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }

}
