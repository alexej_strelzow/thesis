package at.ac.tuwien.isis.usermodels.persistence.tables;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "document")
@ToString
public class RawDocument {

    public RawDocument() {
        super();
    }

    public RawDocument(String title, String contentType, byte[] content) {
        this.title = title;
        this.contentType = contentType;
        this.content = content;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    protected Long id;

    //@NotNull
    //@ManyToOne
    //private Author author;

    @NotNull
    @Getter
    protected String title;

    /**
     * PDF, txt, etc.
     */
    @NotNull
    @Column(name = "content_type")
    @Getter
    protected String contentType;

    @Lob
    @NotNull
    @Getter
    protected byte[] content;

    @NotNull
    @Column(name = "created_at")
    @Getter
    protected Date createdAt;

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }

}
