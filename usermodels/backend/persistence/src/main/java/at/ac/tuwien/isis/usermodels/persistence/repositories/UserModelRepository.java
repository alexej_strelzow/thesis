package at.ac.tuwien.isis.usermodels.persistence.repositories;

import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserModelRepository extends JpaRepository<UserModel, Long> {

    @Query("select um from UserModel um where um.name = :name")
    UserModel getByName(@Param("name") String name);

}
