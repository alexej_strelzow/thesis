package at.ac.tuwien.isis.usermodels.persistence.tables;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "experiment")
@ToString
public class Experiment {

    public Experiment() {
        super();
    }

    public Experiment(UserModel userModel, DocumentModel documentModel, byte[] result) {
        this.userModel = userModel;
        this.documentModel = documentModel;
        this.result = result;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
    @JoinColumn(name="user_model_id")
    @Getter
    private UserModel userModel;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
    @JoinColumn(name="document_model_id")
    @Getter
    private DocumentModel documentModel;

    @Lob
    @NotNull
    @Getter
    private byte[] result;

    @NotNull
    @Column(name = "created_at")
    @Getter
    private Date createdAt;

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }
}
