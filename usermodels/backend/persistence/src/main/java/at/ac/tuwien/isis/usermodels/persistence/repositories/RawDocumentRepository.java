package at.ac.tuwien.isis.usermodels.persistence.repositories;

import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RawDocumentRepository extends JpaRepository<RawDocument, Long> {

    @Query("select rd from RawDocument rd where rd.title in :titles")
    Set<RawDocument> getDocumentsForTitles(@Param("titles") Set<String> titles);

}
