package at.ac.tuwien.isis.usermodels.persistence.repositories;

import at.ac.tuwien.isis.usermodels.persistence.tables.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    //Optional<User> findOneByUsername(String username);

    //Page<User> findFollowings(@Param("user") User user, Pageable pageable);

    //Page<User> findFollowers(@Param("user") User user, Pageable pageable);
}
