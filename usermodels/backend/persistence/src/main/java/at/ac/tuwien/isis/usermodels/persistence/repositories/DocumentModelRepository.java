package at.ac.tuwien.isis.usermodels.persistence.repositories;

import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface DocumentModelRepository extends JpaRepository<DocumentModel, Long> {

    @Query("select dm from DocumentModel dm where dm.document.title in :titles")
    Set<DocumentModel> getDocumentsForTitles(@Param("titles") Set<String> titles);

    @Query("select dm from DocumentModel dm where dm.document.title = :title")
    DocumentModel getByName(@Param("title") String title);

}
