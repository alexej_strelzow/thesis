package at.ac.tuwien.isis.usermodels.persistence.tables;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "user_model", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@ToString
public class UserModel {

    public UserModel() {
        super();
    }

    public UserModel(String name, Set<RawDocument> documents, byte[] content) {
        this.name = name;
        this.documents = documents;
        this.content = content;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @NotNull
    @Getter
    private String name;

    @Getter
    @Setter
    @Column(name = "initial_token_size")
    private Double initalTokenSize;

    @Getter
    @Setter
    @Column(name = "avg_rank")
    private Double avgRank;

    @Getter
    @Setter
    @Column(name = "avg_doc_length")
    private Double avgDocLength;

    @Getter
    @Setter
    @Column(name = "avg_removed_tokens")
    private Double avgRemovedTokens;

    @Getter
    @Setter
    @Column(name = "token_size_after_preprocessing")
    private Double tokenSizeAfterPreprocessing;

    @Getter
    @Setter
    @Column(name = "avg_removed_tokens_after_preprocessing")
    private Double avgRemovedTokensAfterPreprocessing;

    @NotNull
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE) //FetchType.LAZY makes problems, either EAGER or we write query
    @JoinTable(name = "model_mapping", joinColumns = {
            @JoinColumn(name = "user_model_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "raw_document_id",
                    nullable = false, updatable = false) })
    @Getter
    private Set<RawDocument> documents;

    @Lob
    @NotNull
    @Getter
    private byte[] content;

    @NotNull
    @Column(name = "created_at")
    @Getter
    private Date createdAt;

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }

}
