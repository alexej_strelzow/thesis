package at.ac.tuwien.isis.usermodels.persistence.tables;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "author")
@ToString
public class Author {

    public Author(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @NotNull
    @Column(name = "first_name")
    @Getter
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    @Getter
    private String lastName;

    //@OneToMany(mappedBy = "author", fetch = FetchType.LAZY, orphanRemoval = true)
    //@JsonIgnore
    //@Getter
    //private List<RawDocument> documents;

    @NotNull
    @Column(name = "created_at")
    @Getter
    private Date createdAt;

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }
}
