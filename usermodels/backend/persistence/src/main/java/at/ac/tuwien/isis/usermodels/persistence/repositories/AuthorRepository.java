package at.ac.tuwien.isis.usermodels.persistence.repositories;

import at.ac.tuwien.isis.usermodels.persistence.tables.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

    //Optional<Author> findOneByLastName(String lastName);

    //Page<Author> findFollowings(@Param("author") Author user, Pageable pageable);

    //Page<Author> findFollowers(@Param("author") Author user, Pageable pageable);
}
