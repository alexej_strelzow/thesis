package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.CoreLabelWrapper;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.stemmer.PorterStemmer;
import opennlp.tools.stemmer.Stemmer;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.junit.Assert;
import org.junit.Ignore;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.englishStemmer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexej Strelzow on 11.03.2016.
 */
@Ignore
public class GeneralNlpTest extends AbstractTest {

    public static Bag<String> stemSnowball(Bag<CoreLabelWrapper> model) {
        Bag<String> result = new HashBag<>();
        SnowballStemmer stemmer = new englishStemmer();

        for (CoreLabelWrapper label : model) {
            stemmer.setCurrent(label.get().value());
            stemmer.stem();
            String stemmedTerm = stemmer.getCurrent();

            result.add(stemmedTerm);
        }

        return result;
    }

    public static Bag<String> stemPorter(Bag<CoreLabelWrapper> model) {
        Bag<String> result = new HashBag<>();
        Stemmer stemmer = new PorterStemmer();

        for (CoreLabelWrapper label : model) {
            String stemmedTerm = String.valueOf(stemmer.stem(label.get().value()));
            result.add(stemmedTerm);
        }

        return result;
    }

    public static void addTags(Bag<CoreLabelWrapper> model) {
        String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
        MaxentTagger tagger = new MaxentTagger(taggerPath);

        for (CoreLabelWrapper label : model) {
            List<Word> words = new ArrayList<>();
            words.add(new Word(label.get().value()));

            TaggedWord tw = tagger.apply(words).get(0);
            label.get().setTag(tw.tag());
        }
    }

    public static List<TaggedWord> tag(String sentence) {
        String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
        MaxentTagger tagger = new MaxentTagger(taggerPath);

        List<Word> words = new ArrayList<>();
        for (String word : sentence.split(" ")) {
            words.add(new Word(word));
        }
        final List<TaggedWord> taggedWords = tagger.apply(words);

        return taggedWords;
    }

    public void testSentenceDetectorTool() {
        //InputStream stream = new ByteArrayInputStream(strelzowPaper.getBytes(StandardCharsets.UTF_8));

        SentenceModel model = null;
        try {
            model = new SentenceModel(getClass().getClassLoader().getResourceAsStream("en-sent.bin"));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }

        // http://opennlp.sourceforge.net/models-1.5/
        SentenceDetectorME sdetector = new SentenceDetectorME(model);

        String[] sents = sdetector.sentDetect(strelzowPaper);

        String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
        MaxentTagger tagger = new MaxentTagger(taggerPath);

        for (String sentence : sents) {
            System.out.println(sentence);

            List<Word> words = new ArrayList<>();
            for (String word : sentence.split(" ")) {
                words.add(new Word(word));
            }

            for (TaggedWord tw : tagger.apply(words)) {
                System.out.println(tw);
            }
        }

       /*
       ObjectStream<String> paraStream = new ParagraphStream(new PlainTextByLineStream(stream,
               SystemInputStreamFactory.encoding()));

       String para;
       try {
           while ((para = paraStream.read()) != null) {

               String[] sents = sdetector.sentDetect(para);
               for (String sentence : sents) {
                   System.out.println(sentence);
               }

               System.out.println();
           }
       } catch (IOException e) {
           e.printStackTrace();
       }
       */

       /*
       PerformanceMonitor perfMon = new PerformanceMonitor(System.err, "sent");
       perfMon.start();

       try {
           ObjectStream<String> paraStream = new ParagraphStream(new PlainTextByLineStream(new SystemInputStreamFactory(),
                   SystemInputStreamFactory.encoding()));

           String para;
           while ((para = paraStream.read()) != null) {

               String[] sents = sdetector.sentDetect(para);
               for (String sentence : sents) {
                   System.out.println(sentence);
               }

               perfMon.incrementCounter(sents.length);

               System.out.println();
           }
       }
       catch (IOException e) {
           CmdLineUtil.handleStdinIoError(e);
       }

       perfMon.stopAndPrintFinalResult();
       */
    }

}
