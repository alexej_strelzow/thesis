package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.CoreLabelWrapper;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorService;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.TokenizerService;
import at.ac.tuwien.isis.usermodels.service.tokenization.TokenizerServiceImpl;
import it.uniroma1.lcl.jlt.util.EnglishLemmatizer;
import org.apache.commons.collections4.Bag;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 27.02.2016.
 */
public class PreprocessingServiceTest extends AbstractTest {

    private PreprocessorService preprocessor;
    private TokenizerService tokenizer;

    @Before
    public void init() {
        preprocessor = new PreprocessorServiceImpl();
        tokenizer = new TokenizerServiceImpl();
    }

    @Test
    public void test_handleLineBreaks_should_removeSpacing() {
        String content = strelzowPaper;

        String result = preprocessor.removeSpacing(content);

        Assert.assertTrue(!result.contains("\r\n"));
        Assert.assertTrue(!result.contains("\n"));
    }

    @Test
    public void test_handleLineBreaks_should_removeReferences_1() {
        String content = strelzowPaper;

        String result = preprocessor.removeReferences(content);

        Assert.assertTrue(!result.contains("REFERENCES"));
    }

    @Test
    public void test_handleLineBreaks_should_removeReferences_2() {
        String content = "Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content  10. REFERENCES\n strelzowPaper strelzowPaper";

        String result = preprocessor.removeReferences(content);

        Assert.assertTrue(!result.contains("REFERENCES"));
        Assert.assertTrue(!result.contains("strelzowPaper"));
    }

    @Test
    public void test_handleLineBreaks_should_removeReferences_3() {
        String content = "Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content Content content\r\n 10.REFERENCES\n strelzowPaper strelzowPaper";

        String result = preprocessor.removeReferences(content);

        Assert.assertTrue(!result.contains("REFERENCES"));
    }

    @Test
    public void test_handleLineBreaks_should_removeReferences_4() {
        String content = complexPaper;

        String result = preprocessor.removeReferences(content);

        Assert.assertTrue(!result.contains("REFERENCES"));
    }

    @Test
    public void test_handleLineBreaks_should_lowerCaseAfterFullStop() {
        String content = "Hello. My name is Brian. This is USA! Yeah! Ready? Yes!";

        String result = preprocessor.lowerCaseAfterFullStop(content);

        Assert.assertTrue(result.contains("Hello"));
        Assert.assertTrue(!result.contains("My"));
        Assert.assertTrue(result.contains("my"));
        Assert.assertTrue(!result.contains("This"));
        Assert.assertTrue(result.contains("this"));
        Assert.assertTrue(result.contains("USA"));

        Assert.assertTrue(!result.contains("Yeah!"));
        Assert.assertTrue(result.contains("yeah!"));
        Assert.assertTrue(!result.contains("Ready?"));
        Assert.assertTrue(result.contains("ready?"));
        Assert.assertTrue(!result.contains("Yes!"));
        Assert.assertTrue(result.contains("yes!"));

        System.out.println(result);
    }

    @Test
    public void test_handleLineBreaks_should_removeShortWords() {
        String content = "I am Brian. This is I USA! The end.";

        String result = preprocessor.removeShortWords(content);

        //Assert.assertTrue(!result.contains("I")); // TODO
        Assert.assertTrue(!result.contains(" am "));
        Assert.assertTrue(!result.contains(" is "));
        //Assert.assertTrue(!result.contains(" I ")); // TODO
        Assert.assertTrue(!result.contains(" The "));

        System.out.println(result);
    }

    @Test
         public void test_articleToTokens_should_returnCoreLabelWrapper() {
        String content = "The dog! The dog! The dog!";

        final Bag<CoreLabelWrapper> coreLabels = tokenizer.articleToTokens(content);

        final int size = coreLabels.size();
        final int sizeSet = coreLabels.uniqueSet().size();

        Assert.assertTrue(size > sizeSet);
    }

    @Test
    public void test_PoS_should_returnTags() {
        String content = "My name is Alexej and I want coffee!";

        final Bag<TaggedWordWrapper> taggedWords = tokenizer.articleToTaggedTokens(content);

        for (TaggedWordWrapper label : taggedWords) {
            System.out.println(label.get().value() + ": " + label.get().tag());
        }

    }

    @Test
    public void test_removeNonUnicode_should_removeAll() {
        String text = "I hope I never � and always see the � 0815 text.";

        Assert.assertTrue(text.contains("�"));

        final String ppText = preprocessor.removeNonUnicode(text);

        Assert.assertTrue(!ppText.contains("�"));
    }

    @Test
    public void test_articleToTokens_uniqueSetMustWork() {
        String text ="and and not the and";

        Bag<CoreLabelWrapper> lables = tokenizer.articleToTokens(text);

        System.out.println("#: " + lables.size());
        Assert.assertTrue(lables.size() == 5);

        for (CoreLabelWrapper label : lables) {
            System.out.println(label.get().value() + " ");
        }

        System.out.println("#unique: " + lables.uniqueSet().size());
        Assert.assertTrue(lables.uniqueSet().size() == 3);
    }

    @Test
    public void testTokenizationWithHTML() {
        String text = "This is, obviously, 'n easy text, see <a href=\"www.orf.at\">some link</a>.";

        Bag<CoreLabelWrapper> lables = tokenizer.articleToTokens(text);
        int oldSize = lables.size();

        System.out.println("#: " + oldSize);
        Assert.assertTrue(oldSize == 15);

        for (CoreLabelWrapper label : lables) {
            System.out.println(label.get().value());
        }

        final String newText = preprocessor.removeMarkup(text);
        System.out.println(newText);

        lables = tokenizer.articleToTokens(newText);
        int newSize = lables.size();

        System.out.println("#: " + lables.size());

        for (CoreLabelWrapper label : lables) {
            System.out.println(label.get().value());
        }

        Assert.assertTrue(newSize == (oldSize-2));
    }

    @Test
    public void test_removePunctuation_withoutWhitelist() {
        String text = "This is, obviously, 'n easy text.";

        final String newText = preprocessor.removePunctuation(text, null);

        Assert.assertTrue(!newText.contains("."));
        Assert.assertTrue(!newText.contains(","));
        Assert.assertTrue(!newText.contains("'"));
    }

    @Test
    public void test_removePunctuation_withWhitelist() {
        String text = "That's, obviously, 'n easy text.";

        final String newText = preprocessor.removePunctuation(text, "'");
        System.out.println("After punctuation handling: " + newText);

        // blacklist
        Assert.assertTrue(!newText.contains("."));
        Assert.assertTrue(!newText.contains(","));

        // whitelist
        Assert.assertTrue(newText.contains("'"));
    }

    @Test
    public void test_handleSyllables_shouldGlueWords() throws IOException {
        String text = "risk strati- fication asdf";

        text = preprocessor.handleSyllables(text);

        Assert.assertTrue(!text.contains("-"));
        Assert.assertTrue(text.contains("stratification"));
    }

    @Test
    public void test_preprocessing_cancerPaperShouldBeOk() throws IOException {
        StringBuilder textBuilder = new StringBuilder();
        File f = new File(TXT_PATH + "/medicine/cancer_paper.txt");
        for (String line : Files.readAllLines(f.toPath(), Charset.forName("ISO-8859-1"))) {
            textBuilder.append(line).append("\r\n");
        }
        String text = textBuilder.toString();

        text = preprocessor.removeReferences(text); // depends on line breaks
        text = preprocessor.lowerCaseAfterFullStop(text); // depends on punctuation
        text = preprocessor.removeSpacing(text);
        text = preprocessor.removeDigits(text);
        text = preprocessor.removeMarkup(text);
        text = preprocessor.removePunctuation(text, "-"); // syllables
        text = preprocessor.removeShortWords(text);
        text = preprocessor.handleSyllables(text); //"risk strati-fication asdf"
        text = preprocessor.removeNonUnicode(text);

        Assert.assertTrue(!text.contains("-"));
    }

    //@Test
    public void test_lemmatizer_shouldBeCorrect() throws IOException {
        EnglishLemmatizer lemmatizer = new EnglishLemmatizer();
        FwlService fwlService = new FwlServiceImpl();
        try {
            final Field wordToLemma = FwlServiceImpl.class.getDeclaredField("wordToLemma");
            wordToLemma.setAccessible(true);
            final HashMap obj = (HashMap) wordToLemma.get(fwlService);
            for (Object key : obj.keySet()) {
                String word = String.valueOf(key);
                String lemma = String.valueOf(obj.get(word));

                System.out.println("Word: " + word + ", Lemma: " + lemma);

                final Set<String> lemmas = lemmatizer.getLemmas(word);
                System.out.println(lemmas);

                if (lemmas.size() > 1) {
                    Assert.assertTrue(lemmas.contains(lemma));
                } else {
                    Assert.assertEquals(lemma, lemmas.iterator().next());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
