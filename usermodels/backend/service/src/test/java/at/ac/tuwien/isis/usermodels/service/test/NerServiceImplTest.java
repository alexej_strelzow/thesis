package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.service.ner.NerService;
import at.ac.tuwien.isis.usermodels.service.ner.NerServiceImpl;
import edu.stanford.nlp.ling.CoreLabel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

/**
 * Created by Alexej Strelzow on 17.05.2016.
 */
public class NerServiceImplTest {

    private NerService nerService;

    @Before
    public void init() {
        nerService = new NerServiceImpl();
    }

    @Test
    public void testBorderCases() {
        String originalText = "I'd like to visit the San Francisco.";
        String ppText = "like visit Francisco";

        final Set<CoreLabel> originalTextNer = nerService.identifyNER(originalText);
        Assert.assertTrue(originalTextNer.size() == 2);

        final Set<CoreLabel> ppTextNer = nerService.identifyNER(ppText);
        Assert.assertTrue(ppTextNer.size() == 1);
    }

}
