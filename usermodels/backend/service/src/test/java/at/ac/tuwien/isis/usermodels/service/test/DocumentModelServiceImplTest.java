package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.service.test.config.TestConfiguration;
import org.apache.commons.collections4.Bag;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 27.03.2016.
 */
public class DocumentModelServiceImplTest extends AbstractTest {

    @Test
    public void createModel_should_work() throws IOException {
        final DocumentModelDTO documentModelDTO = getModel();
        final Map<Integer, List<TaggedWordWrapper>> byPageModel = getByPageModel(documentModelDTO.getModel());

        for (Integer page : byPageModel.keySet()) {
            final int size = byPageModel.get(page).size();
            Assert.assertTrue(size > 1);
            System.out.println("page: " + page + " size: " + size);
        }
    }

    @Test
    public void createModel_should_have_same_count() throws IOException {
        final DocumentModelDTO documentModelDTO = getModel();
        final Bag<TaggedWordWrapper> model = documentModelDTO.getModel();
        final Map<Integer, List<TaggedWordWrapper>> byPageModel = getByPageModel(model);

        int sumF = 0; // count unique words
        for (TaggedWordWrapper wrapper : model.uniqueSet()) {
            sumF += model.getCount(wrapper);
        }

        int sumW = 0; // count words on each page
        for (Integer page : byPageModel.keySet()) {
            sumW += byPageModel.get(page).size();
        }

        System.out.println("Sum (f): " + sumF + " Sum: " +sumW);

        // sum of both must be the same
        Assert.assertEquals(sumF, sumW);
    }

    private Map<Integer, List<TaggedWordWrapper>> getByPageModel(Bag<TaggedWordWrapper> model) {
        Map<Integer, List<TaggedWordWrapper>> byPage = new LinkedHashMap<>();

        for (TaggedWordWrapper wrapper : model) {
            for (Integer page : wrapper.getPages()) {
                if (byPage.get(page) == null) {
                    List<TaggedWordWrapper> list = new LinkedList<>();
                    list.add(wrapper);
                    byPage.put(page, list);
                } else {
                    final List<TaggedWordWrapper> list = byPage.get(page);
                    list.add(wrapper);
                }
            }
        }

        return byPage;
    }

    private DocumentModelDTO getModel() throws IOException{
        String location = TestConfiguration.RESOURCE_ROOT + TestConfiguration.PDF + "/medicine/cancer_paper.pdf";
        return documentModelService.create(new File(location));
    }

}
