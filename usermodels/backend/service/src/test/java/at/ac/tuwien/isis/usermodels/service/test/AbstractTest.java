package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetServiceImpl;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.config.ModelGenerationConfiguration;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.ModelComparatorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.UserModelServiceImpl;
import at.ac.tuwien.isis.usermodels.service.ner.NerServiceImpl;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserService;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserServiceImpl;
import at.ac.tuwien.isis.usermodels.service.protocol.DummyProtocolServiceImpl;
import at.ac.tuwien.isis.usermodels.service.test.config.TestConfiguration;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.TokenizerServiceImpl;
import it.uniroma1.lcl.babelnet.BabelNet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public abstract class AbstractTest {
    protected static final String PDF_PATH = TestConfiguration.RESOURCE_ROOT + TestConfiguration.PDF;
    protected static final String TXT_PATH = TestConfiguration.RESOURCE_ROOT + TestConfiguration.TXT;

    protected static String simplePath = PDF_PATH + "/test_simple_1.pdf";
    protected static String strelzowPath = PDF_PATH + "/strelzow/www2015paper_strelzow.pdf";
    protected static String complexPath = PDF_PATH + "/complex_paper.pdf";

    protected static String simplePaper;
    protected static String strelzowPaper;
    protected static String complexPaper;

    protected static PdfParserService pdfExtractor;

    protected UserModelServiceImpl userModelService;
    protected DocumentModelServiceImpl documentModelService;
    protected ModelComparatorServiceImpl comparatorService;

    @Before
    public void init() {
        userModelService = new UserModelServiceImpl();
        userModelService.setModelGenerationConfiguration(new ModelGenerationConfiguration());
        final BabelNetServiceImpl babelNetService = new BabelNetServiceImpl();
        babelNetService.setBabelNetConfiguration(new BabelNetConfiguration());
        userModelService.setBabelNet(babelNetService);
        userModelService.setConfiguration(new Configuration());
        userModelService.setNerService(new NerServiceImpl());
        userModelService.setTokenizer(new TokenizerServiceImpl());
        userModelService.setFwlService(new FwlServiceImpl());
        userModelService.setPreprocessor(new PreprocessorServiceImpl());
        userModelService.setProtocolService(new DummyProtocolServiceImpl());

        documentModelService = new DocumentModelServiceImpl();
        documentModelService.setModelGenerationConfiguration(new ModelGenerationConfiguration());
        documentModelService.setBabelNet(babelNetService);
        documentModelService.setConfiguration(new Configuration());
        documentModelService.setNerService(new NerServiceImpl());
        documentModelService.setTokenizer(new TokenizerServiceImpl());
        documentModelService.setFwlService(new FwlServiceImpl());
        documentModelService.setPreprocessor(new PreprocessorServiceImpl());
        documentModelService.setProtocolService(new DummyProtocolServiceImpl());

        comparatorService = new ModelComparatorServiceImpl();
        comparatorService.setConfiguration(new Configuration());
        comparatorService.setFwlService(new FwlServiceImpl());
        comparatorService.setProtocolService(new DummyProtocolServiceImpl());
        comparatorService.setBabelNetService(babelNetService);
    }

    @BeforeClass
    public static void initClass() {
        pdfExtractor = new PdfParserServiceImpl();

        try {
            simplePaper = pdfExtractor.extract(new File(simplePath)).getText();
            strelzowPaper = pdfExtractor.extract(new File(strelzowPath)).getText();
            complexPaper = pdfExtractor.extract(new File(complexPath)).getText();

        } catch (IOException e) {
            Assert.fail(e.getLocalizedMessage());
        }
    }

}
