package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.babelnet.*;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import edu.mit.jwi.item.POS;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Alexej Strelzow on 14.05.2016.
 */
public class BabelNetServiceImplTest extends AbstractBabelNetTest {

    @Test
    public void testWord_should_returnDTO() {
        String explainWord = "dog";

        BabelNetWordDTO wordDTO = new BabelNetWordDTO(explainWord, POS.NOUN.name());
        final BabelNetWordExplanationDTO explanationDTO = babelNetService.explain(wordDTO);

        Assert.assertNotNull(explanationDTO);

        final String word = explanationDTO.getWord();
        Assert.assertNotNull(word);
        Assert.assertEquals(explainWord, word);

        if (babelNetConfiguration.isExplainIncludeGloss()) {
            final List<BabelGlossDTO> glossDTOs = explanationDTO.getGlossDTOs();
            Assert.assertNotNull(glossDTOs);
        }

        if (babelNetConfiguration.isExplainIncludeImage()) {
            final List<BabelImageDTO> imageURLs = explanationDTO.getImageURLs();
            Assert.assertNotNull(imageURLs);
        }

        if (babelNetConfiguration.isExplainIncludeRelatedSynset()) {
            final List<BabelRelatedSynsetDTO> relatedSynsetDTOs = explanationDTO.getRelatedSynsetDTOs();
            Assert.assertNotNull(relatedSynsetDTOs);
        }

        if (babelNetConfiguration.isExplainIncludeTranslation()) {
            final List<BabelTranslationDTO> translationDTOs = explanationDTO.getTranslationDTOs();
            Assert.assertNotNull(translationDTOs);
        }

        if (babelNetConfiguration.isExplainIncludeWikiUrl()) {
            final List<String> wikiURLs = explanationDTO.getWikiURLs();
            Assert.assertNotNull(wikiURLs);
        }

        if (babelNetConfiguration.isExplainIncludeRelatedTerm()) {
            final List<BabelRelatedTermsDTO> relatedTerms = explanationDTO.getRelatedTermsDTOs();
            Assert.assertNotNull(relatedTerms);
        }
    }

    @Test
    public void testWord_should_returnDTO2() {
        String explainWord = "amnesia";

        BabelNetWordDTO wordDTO = new BabelNetWordDTO(explainWord, POS.NOUN.name());
        final BabelNetWordExplanationDTO explanationDTO = babelNetService.explain(wordDTO);

        Assert.assertNotNull(explanationDTO);
    }

}
