package at.ac.tuwien.isis.usermodels.service.test.config;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
public class TestConfiguration {
    private static final String PROJECT_ROOT = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\";
    public static final String RESOURCE_ROOT = PROJECT_ROOT + "backend\\service\\src\\test\\resources\\";
    public static final String PDF = "pdf";
    public static final String TXT = "txt";
}
