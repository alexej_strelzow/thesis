package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Alexej Strelzow on 14.03.2016.
 */
public class SmoothingTest extends AbstractTest {

    @Test
    public void testSmoothing_should_sumTo1() throws IOException {
        final UserModelDTO pappasDTO = userModelService.create(PDF_PATH + "/pappas");
        if (pappasDTO == null) {
            Assert.fail();
        }

        double smooothedSum = 0;
        double sum = 0;
        for (TaggedWordWrapper wrapper : pappasDTO.getModel()) {
            sum += wrapper.getProbability();
            smooothedSum += wrapper.getSmoothedProbability();
        }

        final double unseenProbability = pappasDTO.getUnseenProbability();

        Assert.assertTrue(sum > 0.99 && sum < 1.1);
        Assert.assertTrue(unseenProbability > 0);
        Assert.assertTrue(smooothedSum > 0.99 && smooothedSum < 1.1);
        double sumUnseenAndSmoothed = unseenProbability + smooothedSum;
        Assert.assertTrue(sumUnseenAndSmoothed > 0.99 && sumUnseenAndSmoothed < 1.1);

    }

}
