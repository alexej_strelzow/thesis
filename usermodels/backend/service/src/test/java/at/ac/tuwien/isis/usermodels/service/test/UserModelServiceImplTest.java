package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

/**
 * Created by Alexej Strelzow on 27.03.2016.
 */
public class UserModelServiceImplTest extends AbstractTest {

    @Test
    public void testSimpleBagSerialization_should_serialize() {
        Bag<String> testBag1 = new HashBag<>();
        testBag1.add("a");
        testBag1.add("b");
        testBag1.add("a");

        byte[] byteContent = null;
        try {
            byteContent = serialize(testBag1);
        } catch (IOException e) {
            Assert.fail();
        }

        UserModel userModel = new UserModel("test", null, byteContent);

        Bag<String> testBag2 = new HashBag<>();

        try {
            testBag2 = (HashBag<String>) deserialize(userModel.getContent());
        } catch (IOException | ClassNotFoundException e) {
            Assert.fail();
        }

        for (String str : testBag2) {
            Assert.assertTrue(testBag1.contains(str));
            Assert.assertTrue(testBag2.getCount(str) == testBag1.getCount(str));
        }

        //make experiments and visualize output
        //define input params that can be tuned
        //visualize difference between runs
    }

    @Test
    public void testUserModelSerialization_should_serialize() throws IOException {
        final UserModelDTO pappasDTO = userModelService.create(PDF_PATH + "/pappas");
        final Bag<TaggedWordWrapper> originalModel = pappasDTO.getModel();

        byte[] userModelBytes = null;
        try {
            userModelBytes = ModelUtils.convert(originalModel);
        } catch (IOException e) {
            Assert.fail();
        }

        UserModelDTO deserializedUserModel = null;

        try {
            deserializedUserModel = new UserModelDTO("pappas", ModelUtils.convert(userModelBytes));
        } catch (IOException | ClassNotFoundException e) {
            Assert.fail();
        }

        final Bag<TaggedWordWrapper> deserializedModel = deserializedUserModel.getModel();

        for (TaggedWordWrapper wrapper : originalModel) {
            Assert.assertTrue(deserializedModel.contains(wrapper));
        }
    }

    public static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream  in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }

}
