package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlServiceImpl;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorServiceImpl;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.WordTokenFactory;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

/**
 * Created by Alexej Strelzow on 14.03.2016.
 */
public class PoSTaggingTest extends AbstractTest {

    @Test
    public void tesPoSTagging_should_tagEverything() throws IOException {
        final UserModelDTO pappasDTO = userModelService.create(PDF_PATH + "/pappas");
        if (pappasDTO == null) {
            Assert.fail();
        }

        for (TaggedWordWrapper wrapper : pappasDTO.getModel()) {
            Assert.assertNotNull(wrapper.get().tag());
        }
    }

    @Test
    public void tesPoSTagging_should_findWordsInFwl() throws IOException {
        FwlService fwlService = new FwlServiceImpl();
        final UserModelDTO pappasDTO = userModelService.create(PDF_PATH + "/pappas");
        for (TaggedWordWrapper wrapper : pappasDTO.getModel()) {
            final TaggedWord taggedWord = wrapper.get();
            String word = taggedWord.value().toLowerCase();
            String pos = taggedWord.tag();

            if (!fwlService.isInFwlList(word, pos)) {
                System.out.print(String.format("-%20s -%5s", word, pos));
            }
        }
    }

    @Test
    public void testPosModels() {
        String path = Configuration.POS_MODELS + "\\english-bidirectional-distsim.tagger";
        MaxentTagger tagger = new MaxentTagger(path);

        Bag<TaggedWordWrapper> bag = new HashBag<>();
        final PTBTokenizer<Word> tokenizer = new PTBTokenizer(new StringReader("This is just a text"), new WordTokenFactory(), "untokenizable=noneDelete,ptb3Escaping=true");

        final List<Word> tokens = tokenizer.tokenize();
        for (TaggedWord word : tagger.apply(tokens)) {
            bag.add(new TaggedWordWrapper(word));
        }
    }

    @Test
    public void testCaseSensitivePosTagging_should_makeDifference() {
        String content = "I am Bob. Apple, indeed a delicious fruit!";

        String path = Configuration.POS_MODELS + "\\english-left3words-distsim.tagger";
        MaxentTagger tagger = new MaxentTagger(path);

        Bag<TaggedWordWrapper> bagNonPP = new HashBag<>();
        PTBTokenizer<Word> tokenizer = new PTBTokenizer(new StringReader(content), new WordTokenFactory(), null);
        List<Word> tokens = tokenizer.tokenize();
        for (TaggedWord word : tagger.apply(tokens)) {
            bagNonPP.add(new TaggedWordWrapper(word));
        }

        TaggedWord originalApple = null;
        for (TaggedWordWrapper original : bagNonPP) {
            final TaggedWord taggedWord = original.get();
            if ("Apple".equals(taggedWord.value())) {
                originalApple = taggedWord;
            }
        }

        Assert.assertEquals("NNP", originalApple.tag());

        final PreprocessorServiceImpl preprocessorService = new PreprocessorServiceImpl();
        final String ppContent = preprocessorService.lowerCaseAfterFullStop(content);

        Assert.assertNotEquals(content, ppContent);

        Bag<TaggedWordWrapper> bagPP = new HashBag<>();
        tokenizer = new PTBTokenizer(new StringReader(ppContent), new WordTokenFactory(), null);
        tokens = tokenizer.tokenize();
        for (TaggedWord word : tagger.apply(tokens)) {
            bagPP.add(new TaggedWordWrapper(word));
        }

        TaggedWord newApple = null;
        for (TaggedWordWrapper preprocessed : bagPP) {
            final TaggedWord taggedWord = preprocessed.get();
            if ("apple".equals(taggedWord.value())) {
                newApple = taggedWord;
            }
        }

        Assert.assertEquals("NN", newApple.tag());
    }

    @Test
    public void testCaseSensitivePosTagging2_should_makeDifference() {
        String content = "I am Bob. Running, indeed is healthy!";

        String path = Configuration.POS_MODELS + "\\english-left3words-distsim.tagger";
        MaxentTagger tagger = new MaxentTagger(path);

        Bag<TaggedWordWrapper> bagNonPP = new HashBag<>();
        PTBTokenizer<Word> tokenizer = new PTBTokenizer(new StringReader(content), new WordTokenFactory(), null);
        List<Word> tokens = tokenizer.tokenize();
        for (TaggedWord word : tagger.apply(tokens)) {
            bagNonPP.add(new TaggedWordWrapper(word));
        }

        TaggedWord originalRunning = null;
        for (TaggedWordWrapper original : bagNonPP) {
            final TaggedWord taggedWord = original.get();
            if ("Running".equals(taggedWord.value())) {
                originalRunning = taggedWord;
            }
        }

        Assert.assertEquals("NN", originalRunning.tag());

        final PreprocessorServiceImpl preprocessorService = new PreprocessorServiceImpl();
        final String ppContent = preprocessorService.lowerCaseAfterFullStop(content);

        Assert.assertNotEquals(content, ppContent);

        Bag<TaggedWordWrapper> bagPP = new HashBag<>();
        tokenizer = new PTBTokenizer(new StringReader(ppContent), new WordTokenFactory(), null);
        tokens = tokenizer.tokenize();
        for (TaggedWord word : tagger.apply(tokens)) {
            bagPP.add(new TaggedWordWrapper(word));
        }

        TaggedWord newRunning = null;
        for (TaggedWordWrapper preprocessed : bagPP) {
            final TaggedWord taggedWord = preprocessed.get();
            if ("running".equals(taggedWord.value())) {
                newRunning = taggedWord;
            }
        }

        Assert.assertEquals("VBG", newRunning.tag());
    }
}
