package at.ac.tuwien.isis.usermodels.service.test.suite;

import at.ac.tuwien.isis.usermodels.service.test.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        PdfExtractorServiceTest.class,
        GeneralNlpTest.class,
        PoSTaggingTest.class,
        PreprocessingServiceTest.class,
        SmoothingTest.class,
        UserModelServiceImplTest.class,
        ModelComparatorServiceImplTest.class
})
public class TestSuite {
}
