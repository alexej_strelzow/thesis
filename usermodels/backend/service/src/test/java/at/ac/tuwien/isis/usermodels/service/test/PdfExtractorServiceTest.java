package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentDTO;
import at.ac.tuwien.isis.usermodels.service.parser.PdfUtils;
import at.ac.tuwien.isis.usermodels.service.test.config.TestConfiguration;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 24.01.2016.
 */
public class PdfExtractorServiceTest extends AbstractTest {

    @Test
    public void testExtraction_paper_should_work() throws IOException {
        String location = TestConfiguration.RESOURCE_ROOT + TestConfiguration.PDF + "/medicine/cancer_paper.pdf";
        final DocumentDTO documentDTO = pdfExtractor.extract(new File(location));
        final String text = documentDTO.getText();

        Assert.assertNotNull(text);

        final Map<Integer, String> pages = documentDTO.getPages();

        Assert.assertNotNull(pages);

        long length = 0;
        long textLength = text.length();

        for (Integer page : pages.keySet()) {
            final String content = pages.get(page);

            Assert.assertNotNull(content);

            length += content.length();
        }

        System.out.println(length);
        System.out.println(textLength);
    }

    @Test
    public void testPageTransformations_should_work() throws IOException {
        String location = TestConfiguration.RESOURCE_ROOT + TestConfiguration.PDF + "/medicine/cancer_paper.pdf";
        final DocumentDTO documentDTO = pdfExtractor.extract(new File(location));
        final String text = documentDTO.getText();
        Assert.assertNotNull(text);

        final Map<Integer, String> pages = documentDTO.getPages();
        Assert.assertNotNull(pages);

        final String textWithPageMarkup = documentDTO.getTextWithPageMarkup();
        Assert.assertNotNull(textWithPageMarkup);

        final Map<Integer, String> pagesFromString = PdfUtils.getPages(textWithPageMarkup);

        for (Integer page : pages.keySet()) {
            final String originalContent = pages.get(page);
            Assert.assertNotNull(originalContent);

            final String convertedContent = pagesFromString.get(page);
            Assert.assertNotNull(convertedContent);

            Assert.assertEquals(originalContent, convertedContent);
        }
    }

}
