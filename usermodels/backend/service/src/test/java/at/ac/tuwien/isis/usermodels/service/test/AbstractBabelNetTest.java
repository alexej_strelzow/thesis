package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetServiceImpl;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import it.uniroma1.lcl.babelnet.BabelNet;
import org.junit.BeforeClass;

/**
 * Created by Alexej Strelzow on 14.05.2016.
 */
public class AbstractBabelNetTest {

    protected static BabelNetServiceImpl babelNetService;
    protected static BabelNetConfiguration babelNetConfiguration;

    @BeforeClass
    public static void init() {
        BabelNet.getInstance();
        babelNetService = new BabelNetServiceImpl();

        babelNetConfiguration = new BabelNetConfiguration();
        babelNetService.setBabelNetConfiguration(babelNetConfiguration);
    }

}
