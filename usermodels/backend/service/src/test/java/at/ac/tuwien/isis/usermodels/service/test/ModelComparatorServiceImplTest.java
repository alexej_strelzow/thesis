package at.ac.tuwien.isis.usermodels.service.test;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 14.04.2016.
 */
public class ModelComparatorServiceImplTest extends AbstractTest {

    @Test
    public void testComparisonSameModel_should_beBlank() {
        Map<String, String> articles = new HashMap<>();

        articles.put("www2015paper_strelzow", strelzowPaper);

        try {
            final UserModelDTO strelzowUserModel = userModelService.createFromArticles("strelzow", articles);
            final DocumentModelDTO strelzowDocumentModel = documentModelService.create(strelzowPath);

            final ComparisonResultDTO resultDTO = comparatorService.compare(strelzowUserModel, strelzowDocumentModel);
            ModelUtils.visualizeDiff(strelzowUserModel.getModel(), strelzowDocumentModel.getModel(), resultDTO.getUnknownWords());

            Assert.assertTrue(resultDTO.getUnknownWords().size() == 0);
        } catch (IOException e) {
            Assert.fail();
        }
    }

    /**
     *    Size of tokens/types in Mu: 2046/641
     *    Size of tokens/types in Md: 623/327
     *    Size of diff: 57
     *
     *    NOT in Collection (FWL):   6
     *    In Collection (FWL):       16
     *    In Collection (FWL + POS): 35
     */
    @Test
    public void testComparisonSameModel_should_NotbeBlank() {
        Map<String, String> articles = new HashMap<>();

        articles.put("complexPaper", complexPaper);

        try {
            final UserModelDTO strelzowUserModel = userModelService.createFromArticles("strelzow", articles);
            final DocumentModelDTO complexDocumentModel = documentModelService.create(strelzowPath);

            final ComparisonResultDTO resultDTO = comparatorService.compare(strelzowUserModel, complexDocumentModel);
            ModelUtils.visualizeDiff(strelzowUserModel.getModel(), complexDocumentModel.getModel(), resultDTO.getUnknownWords());

            Assert.assertTrue(resultDTO.getUnknownWords().size() > 0);
        } catch (IOException e) {
            Assert.fail();
        }
    }

}
/*
Size of tokens/types in Mu: 2046/641
Size of tokens/types in Md: 623/327
Size of diff: 57

NOT in Collection (FWL):   6
In Collection (FWL):       16
In Collection (FWL + POS): 35

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc
-------------------------------------------------------------------------------------------------------
URLs                  |  NNS      |  -1       |  false   |  false         |  false
gamification          |  NN       |  -1       |  false   |  false         |  false
hyponyms              |  NNS      |  -1       |  false   |  false         |  false
synset                |  NN       |  -1       |  false   |  false         |  false
synsets               |  NNS      |  -1       |  false   |  false         |  false
uRLs                  |  NNS      |  -1       |  false   |  false         |  false

Apache                |  NNP      |  19131    |  false   |  false         |  true
Corpus                |  NNP      |  15110    |  false   |  false         |  true
Enrich                |  NNP      |  14457    |  false   |  false         |  true
Google                |  NNP      |  20790    |  false   |  false         |  true
Hibernate             |  NNP      |  45887    |  false   |  false         |  true
Informatics           |  NNP      |  55512    |  false   |  false         |  true
Interface             |  NNP      |  8437     |  false   |  false         |  true
Maven                 |  NNP      |  39790    |  false   |  false         |  true
Multilingual          |  NNP      |  36053    |  false   |  false         |  true
QUIZ                  |  NNP      |  12970    |  false   |  false         |  true
Quiz                  |  NNP      |  12970    |  false   |  false         |  true
Toolkit               |  NNP      |  43310    |  false   |  false         |  true
iNVADER               |  NNP      |  31437    |  false   |  false         |  true
kanji                 |  FW       |  48424    |  false   |  false         |  true
outsource             |  VB       |  38344    |  false   |  false         |  true
specify               |  VB       |  11170    |  false   |  false         |  true

Experimentation       |  NN       |  12143    |  false   |  true          |  true
Keywords              |  NNS      |  17233    |  false   |  true          |  true
categorical           |  JJ       |  17559    |  false   |  true          |  true
convenient            |  JJ       |  6843     |  false   |  true          |  true
corresponding         |  JJ       |  8278     |  false   |  true          |  true
dictionaries          |  NNS      |  30402    |  false   |  true          |  true
download              |  NN       |  22203    |  false   |  true          |  true
enforced              |  JJ       |  25538    |  false   |  true          |  true
enrich                |  VBP      |  14457    |  false   |  true          |  true
entities              |  NNS      |  7921     |  false   |  true          |  true
entity                |  NN       |  7478     |  false   |  true          |  true
extract               |  NN       |  10465    |  false   |  true          |  true
feasibility           |  NN       |  15537    |  false   |  true          |  true
filter                |  NN       |  7090     |  false   |  true          |  true
formulate             |  VBP      |  14711    |  false   |  true          |  true
gloss                 |  VBP      |  41269    |  false   |  true          |  true
harness               |  NN       |  14369    |  false   |  true          |  true
homepage              |  NN       |  51996    |  false   |  true          |  true
identifier            |  NN       |  44439    |  false   |  true          |  true
invader               |  NN       |  31437    |  false   |  true          |  true
lemma                 |  NN       |  48032    |  false   |  true          |  true
lexical               |  JJ       |  24184    |  false   |  true          |  true
metadata              |  NN       |  29942    |  false   |  true          |  true
multilingual          |  JJ       |  36053    |  false   |  true          |  true
ontology              |  NN       |  32906    |  false   |  true          |  true
outline               |  VBP      |  16187    |  false   |  true          |  true
prototype             |  NN       |  9654     |  false   |  true          |  true
query                 |  NN       |  18048    |  false   |  true          |  true
quiz                  |  NN       |  12970    |  false   |  true          |  true
register              |  NN       |  7662     |  false   |  true          |  true
semantic              |  JJ       |  12644    |  false   |  true          |  true
structured            |  JJ       |  10447    |  false   |  true          |  true
unused                |  JJ       |  14256    |  false   |  true          |  true
validation            |  NN       |  13282    |  false   |  true          |  true
visualization         |  NN       |  19858    |  false   |  true          |  true


Unique words (size: 51, lower case) without POS:
apache
categorical
convenient
corpus
corresponding
dictionaries
download
enforced
enrich
entities
entity
experimentation
extract
feasibility
filter
formulate
gamification
gloss
google
harness
hibernate
homepage
hyponyms
identifier
informatics
interface
invader
kanji
keywords
lemma
lexical
maven
metadata
multilingual
ontology
outline
outsource
prototype
query
quiz
register
semantic
specify
structured
synset
synsets
toolkit
unused
urls
validation
visualization
 */