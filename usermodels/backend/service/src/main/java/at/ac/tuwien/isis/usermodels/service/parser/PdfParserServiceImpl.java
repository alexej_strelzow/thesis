package at.ac.tuwien.isis.usermodels.service.parser;

import at.ac.tuwien.isis.usermodels.dto.SourceType;
import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentDTO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
@Service
public class PdfParserServiceImpl implements PdfParserService {

    @Override
    public DocumentDTO extract(InputStream is, SourceType type) throws IOException {
        DocumentDTO documentDTO;
        boolean usePdfBox = true;

        switch (type) {
            case PDF:
                documentDTO = usePdfBox ? extractPdfWithPdfBox(is) : extractPdfWithTika(is);
                break;
            case TXT:
                documentDTO = extractText(is);
                break;
            case UNKNOWN:
                documentDTO = extractText(is);
                break;
            default:
                documentDTO = usePdfBox ? extractPdfWithPdfBox(is) : extractPdfWithTika(is);
                break;
        }

        return documentDTO;
    }

    private DocumentDTO extractPdfWithPdfBox(InputStream is) throws IOException {
        DocumentDTO documentDTO = new DocumentDTO();

        PDDocument document = PDDocument.load(is);
        Map<Integer, String> pageToContent = new LinkedHashMap<>();
        final int pages = document.getNumberOfPages();

        for (int i = 0 ; i <  pages; i++) {
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setStartPage(i);
            stripper.setEndPage(i + 1);
            final String text = stripper.getText(document);
            pageToContent.put(i, text);
        }

        documentDTO.setSourceType(SourceType.PDF.name());
        documentDTO.setTitle(document.getDocumentInformation().getTitle());
        documentDTO.setPageCount(pages);

        documentDTO.setText(new PDFTextStripper().getText(document));
        documentDTO.setPages(pageToContent);
        documentDTO.setTextWithPageMarkup(PdfUtils.getTextWithPageMarkup(documentDTO.getPages()));

        return documentDTO;
    }

    private DocumentDTO extractPdfWithTika(InputStream is) throws IOException {
        DocumentDTO documentDTO = new DocumentDTO();

        // http://stackoverflow.com/questions/6839787/reading-a-particular-page-from-a-pdf-document-using-pdfbox
        // http://novyden.blogspot.co.at/2011/06/extracting-text-from-pdf-files-with.html

        Metadata metadata = new Metadata();
        BodyContentHandler handler = new PdfPageHandler(-1);
        Parser parser = new PDFParser();
        PDFParserConfig config = new PDFParserConfig();
        config.setSortByPosition(true);
        ((PDFParser)parser).setPDFParserConfig(config);

        ParseContext parseContext = new ParseContext();

        try {
            parser.parse(is, handler, metadata, parseContext);
        } catch (SAXException | TikaException e) {
            throw new IOException(e);
        } finally {
            if (is != null) {
                is.close();
            }
        }

        documentDTO.setSourceType(SourceType.PDF.name());
        documentDTO.setTitle(metadata.get(TikaCoreProperties.TITLE));
        documentDTO.setPageCount(Integer.valueOf(metadata.get("xmpTPg:NPages")));

        documentDTO.setText(handler.toString());
        documentDTO.setTextWithPageMarkup(PdfUtils.getTextWithPageMarkup(((PdfPageHandler) handler).getPageContent()));
        documentDTO.setPages(((PdfPageHandler) handler).getPageContent());

        return documentDTO;
    }

    private DocumentDTO extractText(InputStream is) throws IOException {
        DocumentDTO documentDTO = new DocumentDTO();

        BodyContentHandler handler = new BodyContentHandler(-1);
        Metadata metadata = new Metadata();
        AutoDetectParser parser = new AutoDetectParser();

        ParseContext parseContext = new ParseContext();

        try {
            parser.parse(is, handler, metadata, parseContext);
        } catch (SAXException | TikaException e) {
            throw new IOException(e);
        } finally {
            if (is != null) {
                is.close();
            }
        }

        documentDTO.setSourceType(SourceType.TXT.name());
        documentDTO.setText(handler.toString());
        documentDTO.setTitle(metadata.get(TikaCoreProperties.TITLE));

        return documentDTO;
    }


    @Override
    public DocumentDTO extract(String url) throws IOException {
        return extract(new FileInputStream(new File(url)), SourceType.UNKNOWN);
    }

    @Override
    public DocumentDTO extract(File file) throws IOException {
        boolean isPdf = file.getName().endsWith("pdf");
        boolean istxt = file.getName().endsWith("txt");
        SourceType type = isPdf ? SourceType.PDF : istxt ? SourceType.TXT : SourceType.UNKNOWN;

        return extract(new FileInputStream(file), type);
    }

    public static void main(String arg[]) {
        PdfParserServiceImpl pdfExtractor = new PdfParserServiceImpl();
        DocumentDTO documentDTO;
        try {
            documentDTO = pdfExtractor.extract("D:\\Studium\\Master\\Master Thesis New\\Literature Review\\12lmodel.pdf");
            System.out.println(documentDTO.getText());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
