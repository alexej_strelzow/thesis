package at.ac.tuwien.isis.usermodels.service;

public class NotPermittedException extends RuntimeException {

    public NotPermittedException(String message) {
        super(message);
    }

}
