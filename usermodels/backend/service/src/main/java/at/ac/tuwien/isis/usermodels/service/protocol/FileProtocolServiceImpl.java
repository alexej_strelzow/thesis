package at.ac.tuwien.isis.usermodels.service.protocol;

import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentProtocolDTO;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
@Log
@Service(value = "file")
public class FileProtocolServiceImpl extends AbstractProtocolServiceImpl implements FileProtocolService {

    @Override
    public void printProtocol(String directory) {
        printProtocol(directory, null);
    }

    @Override
    public void printProtocol(String directory, String additionalContent) {
        final File dir = new File(directory);
        if (dir.exists() && dir.isDirectory()) {
            String fileName = new SimpleDateFormat("yyyy_MM_dd_hh_mm'.txt'").format(new Date());
            final File file = new File(dir, fileName);
            try {
                final PrintWriter printWriter = new PrintWriter(new FileOutputStream(file));
                printWriter.print(getContent());
                if (additionalContent != null) {
                    printWriter.print("\r\n" + additionalContent);
                }
                printWriter.flush();

                this.log.info("Protocol printed: " + file.getAbsolutePath());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void printRemovedWords(Map<String, Bag<String>> removedWords, String directory, String name) {
        StringBuilder protocolBuilder = new StringBuilder("Removed words from: " + name);

        for (String category : removedWords.keySet()) { //see RemoveType
            final Bag<String> words = removedWords.get(category);
            final Set<String> uniqueWords = new TreeSet<>(words.uniqueSet());
            protocolBuilder.append(category + " (" + words.size() + "/" + uniqueWords.size() + ")").append("\r\n");

            for (String word : uniqueWords) {
                protocolBuilder.append("\t" + word + " " + words.getCount(word)).append("\r\n");
            }

            protocolBuilder.append("\r\n");
        }

        final File dir = new File(directory);
        if (dir.exists() && dir.isDirectory()) {
            String fileName = new SimpleDateFormat("yyyy_MM_dd_hh_mm'_removedWords.txt'").format(new Date());
            final File file = new File(dir, fileName);
            try {
                final PrintWriter printWriter = new PrintWriter(new FileOutputStream(file));
                printWriter.print(protocolBuilder.toString());
                printWriter.flush();

                System.out.println("Protocol printed: " + file.getAbsolutePath());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private String getContent() {
        StringBuilder protocolBuilder = new StringBuilder();

        final Map<String, DocumentProtocolDTO> protocols = getProtocol(/*Operation.removeShortWords*/).getDocumentProtocols();
        for (String articleName : protocols.keySet()) {
            DocumentProtocolDTO docProtocol = protocols.get(articleName);
            /*
            final String removedWordsDiff = docProtocol.getDiff(Operation.removeShortWords.name());
            protocolBuilder.append(articleName).append("\r\n");
            protocolBuilder.append(docProtocol.toString()).append("\r\n");

            final Bag<String> diffBag = ModelUtils.textToBag(removedWordsDiff);
            protocolBuilder.append("Operation: removeShortWords (detailed)").append("\r\n");
            protocolBuilder.append("\tsize: " + diffBag.size()).append("\r\n");
            protocolBuilder.append("\tuniqueSet() size: " + diffBag.uniqueSet().size()).append("\r\n");
            for (String word : new TreeSet<>(diffBag.uniqueSet())) {
                protocolBuilder.append("\t\t" + String.format("%-20s %-5s", word, diffBag.getCount(word))).append("\r\n");
            }
            */

            // POS-Tags
            final Map<String, Integer> posCount = docProtocol.getPosCount();
            if (posCount != null) {
                protocolBuilder.append(articleName).append("\r\n");
                for (String pos : posCount.keySet()) {
                    final Integer counter = posCount.get(pos);
                    protocolBuilder.append("\tcount(" + pos + "): " + counter).append("\r\n");
                }
            }
        }

        protocolBuilder.append("\r\n");

        final Map<String, Bag<String>> removedWords = getProtocol().getRemovedWords();
        protocolBuilder.append("Removed words by operation:").append("\r\n");
        for (String key : removedWords.keySet()) {
            final Bag<String> words = removedWords.get(key);
            protocolBuilder.append("\t" + key).append("\r\n");
            protocolBuilder.append("\t\tTokens: " + words.size()).append("\r\n");
            protocolBuilder.append("\t\tTypes: " + words.uniqueSet().size()).append("\r\n");
            for (String word : new TreeSet<>(words.uniqueSet())) {
                protocolBuilder.append("\t\t" + String.format("%-20s %-5s", word, words.getCount(word))).append("\r\n");
            }
        }

        protocolBuilder.append("\r\n");

        final Map<String, Bag<String>> unknownWords = getProtocol().getUnknownWords();
        protocolBuilder.append("Unknown words by operation:").append("\r\n");
        for (String key : unknownWords.keySet()) {
            final Bag<String> words = unknownWords.get(key);
            protocolBuilder.append("\t" + key).append("\r\n");
            protocolBuilder.append("\t\tTokens: " + words.size()).append("\r\n");
            protocolBuilder.append("\t\tTypes: " + words.uniqueSet().size()).append("\r\n");
            for (String word : new TreeSet<>(words.uniqueSet())) {
                protocolBuilder.append("\t\t" + String.format("%-20s %-5s", word, words.getCount(word))).append("\r\n");
            }
        }

        return protocolBuilder.toString();
    }
}
