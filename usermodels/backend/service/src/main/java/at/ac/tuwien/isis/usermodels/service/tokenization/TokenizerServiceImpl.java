package at.ac.tuwien.isis.usermodels.service.tokenization;

import at.ac.tuwien.isis.usermodels.dto.CoreLabelWrapper;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.WordTokenFactory;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
@Service
public class TokenizerServiceImpl implements TokenizerService {
    private final String DEFAULT_TAGGER_PATH = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";

    private final String taggerPath = Configuration.POS_TAGGER != null ? Configuration.POS_TAGGER : DEFAULT_TAGGER_PATH;
    private MaxentTagger tagger;

    public TokenizerServiceImpl() {
        tagger = new MaxentTagger(taggerPath);
    }

    @Override
    public DocumentPreprocessor articleToSentences(String text) {
        return new DocumentPreprocessor(new InputStreamReader(IOUtils.toInputStream(text)));
    }

    /**
     * @param text
     * @return
     */
    @Override
    public Bag<CoreLabelWrapper> articleToTokens(String text) {
        InputStreamReader isr = new InputStreamReader(IOUtils.toInputStream(text));
        Bag<CoreLabelWrapper> bag = new HashBag<>();

        PTBTokenizer<CoreLabel> tokenizer = new PTBTokenizer<>(isr, new CoreLabelTokenFactory(), "");
        while (tokenizer.hasNext()) {
            bag.add(new CoreLabelWrapper(tokenizer.next()));
        }

        return bag;
    }

    // TODO: make basic impl of IWrapper, so that we always have same interface -> interchangeable between CoreLabel + TaggedWord
    // or integrate tag from TaggedWord in CoreLabel!!! -> there might be a pipeline somewhere
    // see it.uniroma1.lcl.jlt.pipeline.stanford.StanfordPOSTagger
    // interesting? http://www.programcreek.com/java-api-examples/index.php?api=edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation
    @Override
    public Bag<TaggedWordWrapper> articleToTaggedTokens(String text) {
        Bag<TaggedWordWrapper> bag = new HashBag<>();
        final PTBTokenizer<Word> tokenizer = new PTBTokenizer(new StringReader(text), new WordTokenFactory(), getOptions());

        final List<Word> tokens = tokenizer.tokenize();
        for (TaggedWord word : tagger.apply(tokens)) {
            bag.add(new TaggedWordWrapper(word));
        }

        return bag;
    }

    @Override
    public int getNumberOfTokens(String text) {
        return new PTBTokenizer(new StringReader(text), new WordTokenFactory(), getOptions()).tokenize().size();
    }

    @Override
    public String getOptions() {
        return Configuration.TOKENIZER_OPTIONS;
    }
}
