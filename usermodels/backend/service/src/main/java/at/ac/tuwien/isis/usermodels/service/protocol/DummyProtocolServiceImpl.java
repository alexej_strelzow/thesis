package at.ac.tuwien.isis.usermodels.service.protocol;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentProtocolDTO;
import at.ac.tuwien.isis.usermodels.dto.protocol.ExperimentProtocolDTO;
import at.ac.tuwien.isis.usermodels.service.tokenization.Operation;
import org.apache.commons.collections4.Bag;
import org.springframework.stereotype.Service;

/**
 * Created by Alexej Strelzow on 10.04.2016.
 */
@Service(value = "dummy")
public class DummyProtocolServiceImpl implements ProtocolService {


    @Override
    public void protocolDocumentStart(String docId) {

    }

    @Override
    public void protocolUnitStart(Operation operation, String before) {

    }

    @Override
    public void protocolUnitEnd(Operation operation, String after) {

    }

    @Override
    public void protocolDocumentEnd(String docId) {

    }

    @Override
    public void protocolRemovedWord(RemoveType type, String word, String pos) {

    }

    @Override
    public void protocolUnknownWord(Reason reason, String word, String pos) {

    }

    @Override
    public DocumentProtocolDTO getProtocol(String docId, Operation... diffForOperations) {
        return null;
    }

    @Override
    public ExperimentProtocolDTO getProtocol(Operation... diffForOperations) {
        return null;
    }

    @Override
    public void analyzeTokens(String article, Bag<TaggedWordWrapper> bag) {

    }

    @Override
    public void printProtocol() {

    }
}
