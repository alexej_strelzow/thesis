package at.ac.tuwien.isis.usermodels.service.utils;

import at.ac.tuwien.isis.usermodels.dto.babelnet.*;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import com.google.common.collect.Multimap;
import edu.mit.jwi.item.IPointer;
import it.uniroma1.lcl.babelnet.BabelGloss;
import it.uniroma1.lcl.babelnet.BabelImage;
import it.uniroma1.lcl.babelnet.BabelSense;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.jlt.util.Language;
import it.uniroma1.lcl.jlt.util.ScoredItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 17.10.2014.
 */
public class BabelToDtoConverter {

    public static List<BabelGlossDTO> convertGlossesToDto(List<BabelGloss> glosses) {
        List<BabelGlossDTO> glossDTOs = new ArrayList<>(glosses.size());

        for (BabelGloss gloss : glosses) {
            glossDTOs.add(convertGlossToDto(gloss));
        }

        return glossDTOs;
    }

    public static BabelGlossDTO convertGlossToDto(BabelGloss gloss) {
        return new BabelGlossDTO(gloss.getGloss(), gloss.getSource().name(), gloss.getLanguage().name(), gloss.getSourceSense());
    }

    public static List<BabelTranslationDTO> convertTranslationsToDto(Multimap<Language, ScoredItem<String>> translations) {
        List<BabelTranslationDTO> translationDTOs = new ArrayList<>(translations.size());

        for (Language language : translations.keySet()) {
            translationDTOs.add(convertTranslationsToDto(language, translations.get(language)));
        }

        return translationDTOs;
    }

    public static BabelTranslationDTO convertTranslationsToDto(Language language, Collection<ScoredItem<String>> translations) {
        List<String> translationStrings = new ArrayList<>();
        for (ScoredItem<String> item : translations) {
            translationStrings.add(item.getItem());
        }
        return new BabelTranslationDTO(language.name(), translationStrings);
    }

    public static List<BabelImageDTO> convertImagesToDto(List<BabelImage> images, boolean validImagesOnly) {
        List<BabelImageDTO> babelImageDTOs = new ArrayList<>(images.size());

        if (validImagesOnly) {
            for (BabelImage image : images) {
                final String validatedURL = image.getValidatedURL();
                if (validatedURL != null) {
                    babelImageDTOs.add(convertImage(image, validatedURL));
                } else {
                    // TODO log
                    System.out.println("NOT VALID: " + image.getURL());
                }
            }

        } else {
            for (BabelImage image : images) {
                babelImageDTOs.add(convertImage(image, image.getURL()));
            }
        }

        return babelImageDTOs;
    }

    private static BabelImageDTO convertImage(BabelImage image, String url) {
        return  new BabelImageDTO(url, image.getName(), image.getLanguage().name());
    }

    public static List<BabelRelatedTermsDTO> convertSensesToDto(List<BabelSense> senses) {
        List<BabelRelatedTermsDTO> babelRelatedTermsDTOs = new ArrayList<>(senses.size());

        for (BabelSense sense : senses) {
            babelRelatedTermsDTOs.add(convertSensesToDto(sense));
        }

        return babelRelatedTermsDTOs;
    }

    public static BabelRelatedTermsDTO convertSensesToDto(BabelSense sense) {
        BabelRelatedTermsDTO babelRelatedTermsDTO = new BabelRelatedTermsDTO(sense.getSensekey(), sense.getLemma(), sense.getPOS().name());

        babelRelatedTermsDTO.setSrcLanguage(sense.getLanguage().name());
        babelRelatedTermsDTO.setBabelSenseSource(sense.getSource().name());

        return babelRelatedTermsDTO;
    }

    public static List<BabelRelatedSynsetDTO> convertRelatedSynsetsToDto(Map<IPointer, List<BabelSynset>> relatedMap) {
        List<BabelRelatedSynsetDTO> babelRelatedSynsetDTOs = new ArrayList<>();
        String pointerSymbol;

        for (IPointer pointer : relatedMap.keySet()) {
            pointerSymbol = pointer.getSymbol();

            for (BabelSynset synset : relatedMap.get(pointer)) {
                String word = BabelNetUtils.getDisplayWord(synset.getMainSense());
                BabelRelatedSynsetDTO babelRelatedSynsetDTO = new BabelRelatedSynsetDTO(synset.getId(), word, pointerSymbol, pointer.getName());

                babelRelatedSynsetDTOs.add(babelRelatedSynsetDTO);
            }
        }

        return babelRelatedSynsetDTOs;
    }

}
