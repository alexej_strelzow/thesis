package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import org.apache.commons.collections4.Bag;

import java.util.Map;

/**
 * Created by Alexej Strelzow on 23.04.2016.
 */
public interface ModelService {

    Map<String, Bag<TaggedWordWrapper>> preprocess(Map<String, String> articles);

    Bag<TaggedWordWrapper> tokenize(String text);

    Bag<TaggedWordWrapper> postprocess(Bag<TaggedWordWrapper> mergedBows);

}
