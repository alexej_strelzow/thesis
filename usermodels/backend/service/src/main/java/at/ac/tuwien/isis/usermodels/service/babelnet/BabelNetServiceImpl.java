package at.ac.tuwien.isis.usermodels.service.babelnet;

import at.ac.tuwien.isis.usermodels.dto.babelnet.*;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.exception.ServiceException;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import at.ac.tuwien.isis.usermodels.service.utils.BabelNetUtils;
import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.babelnet.BabelSense;
import it.uniroma1.lcl.babelnet.BabelSenseSource;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.jlt.util.Language;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexej Strelzow on 06.05.2016.
 */
@Service
@Log
public class BabelNetServiceImpl implements BabelNetService {

    @Autowired
    @Setter
    @Getter
    private BabelNetConfiguration babelNetConfiguration;

    @Autowired
    @Setter
    @Getter
    private FwlService fwlService;

    private BabelNet bn;

    public BabelNetServiceImpl() {
        bn = BabelNet.getInstance();
    }

    @Override
    public BabelNetConfiguration getConfiguration() {
        return babelNetConfiguration;
    }

    @Override
    public List<BabelSense> getSenses(Language language, String word) throws ServiceException {
        try {
            return bn.getSenses(language, word);
        } catch (IOException e) {
            throw new ServiceException(e.getLocalizedMessage(), e.getCause());
        }
    }

    @Override
    public List<BabelSense> getSenses(Language language, String word, POS pos) throws ServiceException {
        try {
            return bn.getSenses(language, word, pos);
        } catch (IOException e) {
            throw new ServiceException(e.getLocalizedMessage(), e.getCause());
        }
    }

    @Override
    public List<BabelSynset> getSynsets(Language language, String word) throws ServiceException {
        try {
            return bn.getSynsets(language, word);
        } catch (IOException e) {
            throw new ServiceException(e.getLocalizedMessage(), e.getCause());
        }
    }

    @Override
    public List<BabelSynset> getSynsets(Language language, String word, POS pos) throws ServiceException {
        try {
            return bn.getSynsets(language, word, pos);
        } catch (IOException e) {
            throw new ServiceException(e.getLocalizedMessage(), e.getCause());
        }
    }

    @Override
    public BabelNetWordExplanationDTO explain(BabelNetWordDTO dto) throws ServiceException {
        BabelNetWordExplanationDTO response = new BabelNetWordExplanationDTO();

        try {
            Language language = Language.valueOf(dto.getSrcLanguage());
            POS pos = dto.getPos() != null ? POS.valueOf(dto.getPos()) : null;
            final List<String> babelSenseSourceStr = dto.getSource();
            final List<String> destLangStr = dto.getDestLanguages();

            for (String destLang : babelNetConfiguration.getExplainDefaultDestLanguages()) {
                if (!destLangStr.contains(destLang)) {
                    destLangStr.add(destLang);
                }
            }

            List<Language> destLangEnum = !destLangStr.isEmpty() ? BabelNetUtils.convertLanguages(destLangStr) : null;
            List<BabelSenseSource> babelSenseSource = !babelSenseSourceStr.isEmpty() ? BabelNetUtils.convertSources(babelSenseSourceStr) : null;
            String word = dto.getWord();

            List<BabelSynset> synsets;
            if (babelSenseSource != null) {
                synsets = new ArrayList<>();
                for (BabelSenseSource senseSource : babelSenseSource) {
                    synsets.addAll(bn.getSynsets(language, word, pos, senseSource));
                }
            } else {
                synsets = bn.getSynsets(language, word, pos);
                if (synsets.size() == 0) {
                    synsets = bn.getSynsets(language, word);
                }
            }

            BabelSynset filteredSynset;

            if (synsets.size() == 0) {
                final String lemma = fwlService.getLemma(word);
                synsets = bn.getSynsets(language, lemma, pos);
                filteredSynset = BabelNetUtils.filterByMainSense(synsets, lemma);

            } else {
                filteredSynset = BabelNetUtils.filterByMainSense(synsets, word);
            }


            if (filteredSynset == null) {
                if (synsets.size() == 0) {
                    synsets = bn.getSynsets(language, word);
                }
                if (synsets.size() != 0) {
                    filteredSynset = synsets.get(0);
                    pos = filteredSynset.getPOS();

                } else {
                    throw new ServiceException("No main senses for word: " + word + " found");
                }
            }

            response.setSynsetId(filteredSynset.getId());
            response.setLanguage(language.getName());
            response.setWord(word);
            response.setPos(pos.name());

            if (babelNetConfiguration.isExplainIncludeGloss()) {
                List<BabelGlossDTO> glossDTOs = BabelNetUtils.getGlosses(filteredSynset, destLangEnum, babelSenseSource);
                response.setGlossDTOs(glossDTOs);
            }

            if (babelNetConfiguration.isExplainIncludeImage()) {
                List<BabelImageDTO> babelImageDTOs = BabelNetUtils.getImages(filteredSynset, destLangEnum, babelNetConfiguration.isValidImagesOnly());
                response.setImageURLs(babelImageDTOs);
            }

            if (babelNetConfiguration.isExplainIncludeWikiUrl()) {
                List<String> wikiURLs = BabelNetUtils.getWikiURLs(filteredSynset, destLangEnum);
                response.setWikiURLs(wikiURLs);
            }

            if (babelNetConfiguration.isExplainIncludeTranslation()) {
                List<BabelTranslationDTO> translationDTOs = BabelNetUtils.getTranslations(word, language, destLangEnum);
                response.setTranslationDTOs(translationDTOs);
            }

            if (babelNetConfiguration.isExplainIncludeRelatedSynset()) {
                List<BabelRelatedSynsetDTO> relatedSynsetDTOs = BabelNetUtils.getRelatedSynsets(filteredSynset);
                response.setRelatedSynsetDTOs(relatedSynsetDTOs);
            }

            if (babelNetConfiguration.isExplainIncludeRelatedTerm()) {
                List<BabelRelatedTermsDTO> relatedTermsDTOs = BabelNetUtils.getRelatedTerms(filteredSynset, destLangEnum, babelSenseSource);
                response.setRelatedTermsDTOs(relatedTermsDTOs);
            }

        } catch (IOException e) {
            throw new ServiceException(e.getLocalizedMessage(), e.getCause());
        }

        return response;
    }
}
