package at.ac.tuwien.isis.usermodels.service.tokenization;

import at.ac.tuwien.isis.usermodels.dto.CoreLabelWrapper;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import edu.stanford.nlp.process.DocumentPreprocessor;
import org.apache.commons.collections4.Bag;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
public interface TokenizerService {

    DocumentPreprocessor articleToSentences(String text);

    Bag<TaggedWordWrapper> articleToTaggedTokens(String text);

    Bag<CoreLabelWrapper> articleToTokens(String text);

    String getOptions();

    int getNumberOfTokens(String text);
}
