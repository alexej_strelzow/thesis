package at.ac.tuwien.isis.usermodels.service.tokenization;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
public interface PreprocessorService {

    /**
     * Removes !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ if not explicitly white-listed
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String removePunctuation(String text, String whitelist);

    /**
     * Jsoup.clean removes markup (no whitelist)
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String removeMarkup(String text);

    /**
     * Beginning of sentence gets lower-cased. Pattern: ((\.)|(\?)|(!))\s\p{Upper}
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String lowerCaseAfterFullStop(String text);

    /**
     * Removes non-unicode characters. Pattern: [^\p{L}\p{Nd}]+
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String removeNonUnicode(String text);

    /**
     * Removes digits. Pattern: \d+
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String removeDigits(String text);

    /**
     * Removes reference chapter
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String removeReferences(String text);

    /**
     * Removes words with less than 4 letters. Pattern: \s\w{1,3}\s
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String removeShortWords(String text);

    /**
     * Reduces multiple spaces (also new line) to one.
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String removeSpacing(String text);

    /**
     * TODO
     * @param text Text to pre-process
     * @return pre-processed text
     */
    String handleSyllables(String text);
}
