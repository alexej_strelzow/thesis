package at.ac.tuwien.isis.usermodels.service.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */

@Log
@Service
public class Configuration {
    private static final String PROJECT_ROOT = "C:\\Users\\Besitzer\\Dev\\bitbucket\\thesis\\usermodels\\";
    @Getter
    @Setter
    private String projectRoot = PROJECT_ROOT;

    private static final String RESOURCE_ROOT = PROJECT_ROOT + "backend\\service\\src\\main\\resources\\";
    @Getter
    @Setter
    private String resourceRoot = RESOURCE_ROOT;

    public static final String POS_MODELS = RESOURCE_ROOT + "pos-models";
    @Getter
    @Setter
    private String posModels = POS_MODELS;

    public static final String EXPERIMENTS = RESOURCE_ROOT + "experiments";
    @Getter
    @Setter
    private String experiments = EXPERIMENTS;

    /**
     * FWL
     */
    public static final String FWL_CSV_FILE = "fwl/fwl_100k_en_2.csv";
    @Getter
    @Setter
    private String fwlCsvFile = FWL_CSV_FILE;

    public static final String FWL_CONVERSION_FILE = "fwl/conversion.csv";
    @Getter
    @Setter
    private String fwlConversionFile = FWL_CONVERSION_FILE;

    public static final boolean IS_WORD_DRIVEN = true; // false for lemma driven
    @Getter
    @Setter
    private boolean isWordDriven = IS_WORD_DRIVEN;

    /**
     * STOP-WORDS
     */
    public static final String STOP_WORDS_FILE = "stopwords/smart-english.txt";
    @Getter
    @Setter
    private String stopWordsFile = STOP_WORDS_FILE;

    /**
     * BASEWORDS
     */
    public static final String BASEWORD_DIR = "basewords/";
    @Getter
    @Setter
    private String basewordDir = BASEWORD_DIR;

    /**
     * POS - TAGGING MODELS
     */
    private static final String POS_TAGGER_MODEL_DIR = RESOURCE_ROOT + "pos-models\\";
    public static final String POS_TAGGER = null; //POS_TAGGER_MODEL_DIR + "english-caseless-left3words-distsim.tagger";
    // "english-bidirectional-distsim.tagger";
    // "english-caseless-left3words-distsim.tagger";
    // "english-left3words-distsim.tagger";

    @Getter
    @Setter
    private String posTagger = POS_TAGGER;

    /**
     * NER - NAMED ENTITY RECOGNITION MODELS
     */
    private static String NER_MODEL_DIR = RESOURCE_ROOT + "ner-models\\";
    public static final String NER_MODEL = NER_MODEL_DIR + "english.muc.7class.distsim.crf.ser.gz";
    @Getter
    @Setter
    private String nerModel = NER_MODEL;

    /**
     * TOKENIZER
     */
    public static final String TOKENIZER_OPTIONS = "untokenizable=noneDelete,ptb3Escaping=true";

    /**
     * MODEL COMPARISON, see ModelComparatorServiceImpl
     * E.g. if words rank > avg rank we can ask the lemma
     */
    public static final boolean USE_LEMMA = true;
    /**
     * E.g. get Antonyms
     */
    public static final boolean USE_BABELNET = true;

    /**
     * DEBUGGING
     */
    public static final boolean DEBUG_ANALYZE_TOKENS = false;

}
