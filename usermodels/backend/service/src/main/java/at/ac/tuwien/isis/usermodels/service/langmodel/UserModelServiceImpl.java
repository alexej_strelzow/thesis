package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.dto.SourceType;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.dto.Utils;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.service.protocol.ProtocolService;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import com.google.common.primitives.Ints;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.stats.SimpleGoodTuring;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
@Log
@Service//(value = "user")
@Transactional
public class UserModelServiceImpl extends AbstractModelServiceImpl implements UserModelService {

    @Autowired
    private UserModelRepository userModelRepository;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    // helper
    private Map<String, Double> wordToSmoothedProbability;
    private Map<Integer, Integer> frequencyOfFrequency;
    private long sumCount;
    private double pUnseen; // result of smoothing
    private double avgSmoothedP;
    private double avgRank;

    public UserModelServiceImpl() {
        wordToSmoothedProbability = new HashMap<>();
        frequencyOfFrequency = new TreeMap<>();
    }

    @Override
    public UserModelDTO create(File directory) throws IOException {
        Map<String, String> articles;

        try {
            articles = ModelUtils.extractPdfs(directory);
            this.log.info("extracted " + articles.size() + " articles!");
        } catch(IOException e) {
            this.log.severe(e.getLocalizedMessage());
            throw new IOException(e);
        }

        return createFromArticles(directory.getName(), articles);
    }

    @Override
    public UserModelDTO create(String directory) throws IOException {
        final File dir = new File(directory);
        if (dir.exists() && dir.isDirectory()) {
            return create(dir);
        }

        final URL users = UserModelServiceImpl.class.getClassLoader().getResource(directory);
        if (users != null && users.getFile() != null) {
            File usersDir = new File(users.getFile());
            if (usersDir.isDirectory()) {
                return create(usersDir);
            }
            this.log.severe(usersDir.getParent() + " is not a directory!");
        }

        return null;
    }

    @Override
    public UserModelDTO create(String name, byte[] pdfFile) throws IOException {
        throw new UnsupportedOperationException("TODO: in later release");
    }

    @Override
    public UserModelDTO createFromArticles(String name, Map<String, String> articles) throws IOException {
        this.log.info("pre-processing...");
        Map<String, Bag<TaggedWordWrapper>> bows = preprocess(articles);

        this.log.info("merging bows...");
        Bag<TaggedWordWrapper> mergedBows = merge(bows);

        this.log.info("post-processing...");
        final Bag<TaggedWordWrapper> model = postprocess(mergedBows);

        computeCounts(model);
        doSmoothing(model);
        createProbabilityMap(model);

        UserModelDTO dto;

        if (userModelRepository != null && rawDocumentRepository != null) {
            final Set<String> articleNames = articles.keySet();
            final Set<RawDocument> documents = rawDocumentRepository.getDocumentsForTitles(articleNames);
            if (documents.size() != articleNames.size()) { // if we can't find the documents by name we have to persist them
                final Set<String> docNames = new HashSet<>();
                documents.forEach(element -> docNames.add(element.getTitle()));
                final Set<String> allTitles = new HashSet<>(articleNames);
                allTitles.removeAll(docNames);

                for (String title : allTitles) {
                    RawDocument rawDocument = new RawDocument(title, SourceType.PDF.name(), Utils.stringToBytes(articles.get(title)));
                    documents.add(rawDocumentRepository.save(rawDocument));
                }
            }

            UserModel userModel = new UserModel(name, documents, ModelUtils.convert(model));
            userModel.setAvgRank(avgRank);
            userModel.setAvgDocLength(avgDocLength);
            userModel.setAvgRemovedTokens(avgRemovedTokens);
            userModel = userModelRepository.save(userModel);

            dto = new UserModelDTO(userModel.getId(), name, model);
        } else {
            dto = new UserModelDTO(name, model);
        }

        dto.setContainingDocs(new HashSet(articles.keySet()));
        dto.setAvgDocLength(avgDocLength);
        dto.setAvgRank(avgRank);
        dto.setInitalTokenSize(initalTokenSize);
        dto.setTokenSizeAfterPreprocessing(tokenSizeAfterPreprocessing);
        dto.setAvgRemovedTokens(avgRemovedTokens);
        dto.setAvgRemovedTokensAfterPreprocessing(avgRemovedTokensAfterPreprocessing);
        // smoothing related, don't have to persist, we calculate again if needed
        dto.setCountSum(sumCount);
        dto.setUnseenProbability(pUnseen);
        dto.setAvgSmoothedProbability(avgSmoothedP);
        dto.setWordToSmoothedProbability(wordToSmoothedProbability);
        dto.setFrequencyOfFrequency(frequencyOfFrequency);

        this.log.info("user model created!");

        return dto;
    }

    protected Bag<TaggedWordWrapper> merge(Map<String, Bag<TaggedWordWrapper>> bows) {
        Bag<TaggedWordWrapper> mergedBows = new HashBag<>();

        for (Bag<TaggedWordWrapper> bow : bows.values()) {
            mergedBows.addAll(bow);
        }

        return mergedBows;
    }

    @Override
    public UserModelDTO get(long id) {
        final UserModel userModel = userModelRepository.findOne(id);
        return ModelUtils.convertUserModel(userModel);
    }

    @Override
    public UserModelDTO get(String name) {
        final UserModel userModel = userModelRepository.getByName(name);
        return ModelUtils.convertUserModel(userModel);
    }

    @Override
    public List<UserModelDTO> getAll() {
        List<UserModelDTO> list = new LinkedList<>();
        for (UserModel userModel : userModelRepository.findAll()) {
            list.add(ModelUtils.convertUserModel(userModel));
        }

        return list;
    }

    @Override
    public void setProtocolService(ProtocolService protocolService) {
        this.protocolService = protocolService;
    }

    @Override
    public void delete(long id) {
        final UserModel userModel = userModelRepository.findOne(id);
        userModelRepository.delete(userModel);
    }

    protected void computeCounts(final Bag<TaggedWordWrapper> model) {
        this.log.info("computing counts...");

        for (TaggedWordWrapper wrapper : model) {
            final int frequency = model.getCount(wrapper);
            if (frequencyOfFrequency.containsKey(frequency)) {
                Integer value = frequencyOfFrequency.get(frequency);
                frequencyOfFrequency.put(frequency, ++value);
            } else {
                frequencyOfFrequency.put(frequency, 1);
            }

            sumCount += model.getCount(wrapper);
        }

        this.log.info("counts computed, total tokens: " + sumCount);
    }

    protected void doSmoothing(final Bag<TaggedWordWrapper> model) {
        this.log.info("performing smoothing...");

        int[] r = new int[frequencyOfFrequency.size()];
        int[] n = new int[frequencyOfFrequency.size()];
        int index = 0;
        for (Integer key : frequencyOfFrequency.keySet()) {
            r[index] = key.intValue();
            n[index] = frequencyOfFrequency.get(key).intValue();
            index++;
        }

        SimpleGoodTuring sgt = new SimpleGoodTuring(r, n);
        final double[] probabilities = sgt.getProbabilities();

        final List<Integer> rList = Ints.asList(r);
        for (TaggedWordWrapper wrapper : model) {
            final Integer rank = rList.indexOf(model.getCount(wrapper));
            wrapper.setSmoothedProbability(probabilities[rank]);

            // compute un-smoothed probability
            wrapper.setProbability(model.getCount(wrapper)/(double)sumCount);
        }

        pUnseen = probabilities[0];

        this.log.info("smoothing performed, P(unseen): " + pUnseen);
    }

    protected void createProbabilityMap(final Bag<TaggedWordWrapper> model) {
        this.log.info("computing probabilities...");

        long counter = 0;
        for (TaggedWordWrapper wrapper : model.uniqueSet()) {
            final double smoothedProbability = wrapper.getSmoothedProbability();
            final TaggedWord taggedWord = wrapper.get();
            final String valueToLower = taggedWord.value().toLowerCase();

            wordToSmoothedProbability.put(wrapper.toString(), smoothedProbability);

            avgSmoothedP += smoothedProbability;
            if (fwlService.isInFwlList(valueToLower, taggedWord.tag())) {
                avgRank += fwlService.getRank(valueToLower, taggedWord.tag());
                counter++;
            } else {
                if (fwlService.isInFwlList(valueToLower)) {
                    avgRank += fwlService.getLowestRank(valueToLower);
                    counter++;
                }
            }
        }

        avgSmoothedP = avgSmoothedP/model.uniqueSet().size();
        avgRank = avgRank/counter;

        this.log.info("probabilities computed:");
        this.log.info("\tavgSmoothedP: " + avgSmoothedP); // avgSmoothedP: 2.444323369722474E-5
        this.log.info("\tavgRank: " + avgRank); // avgRank: 8428.960276338516
    }

}