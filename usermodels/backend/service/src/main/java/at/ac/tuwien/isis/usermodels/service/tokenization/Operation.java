package at.ac.tuwien.isis.usermodels.service.tokenization;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
public enum Operation {
    removeReferences("Removes reference chapter"),
    removePunctuation("Removes !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~ if not explicitly white-listed"),
    removeMarkup("Jsoup.clean removes markup (no whitelist)"),
    lowerCaseAfterFullStop("Beginning of sentence gets lower-cased. Pattern: ((\\.)|(\\?)|(!))\\s\\p{Upper}"),
    removeNonUnicode("Removes non-unicode characters. Pattern: [^\\p{L}\\p{Nd}]+"),
    removeDigits("Removes digits. Pattern: \\d+"),
    removeShortWords("Removes words with less than 4 letters. Pattern: \\s\\w{1,3}\\s"),
    removeSpacing("Reduces multiple spaces (also new line) to one."),
    handleSyllables("Handles words, which contain the '-' character.");

    private String desc;
    Operation(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return this.desc;
    }
}
