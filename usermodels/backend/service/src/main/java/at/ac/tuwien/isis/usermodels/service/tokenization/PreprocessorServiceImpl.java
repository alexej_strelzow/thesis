package at.ac.tuwien.isis.usermodels.service.tokenization;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
@Service
public class PreprocessorServiceImpl implements PreprocessorService {

    private Map<String, String> operationMetaData;

    public PreprocessorServiceImpl() {
        operationMetaData = new HashMap<>();
        for (Operation operation : Operation.values()) {
            operationMetaData.put(operation.name(), operation.getDesc());
        }
    }

    @Override
    public String removeReferences(String text) {
        // assumption: references are last chapter
        // e.g 7. REFERENCES or just REFERENCES
        Matcher m = Pattern.compile("\\p{Digit}*\\.*\\s*REFERENCES\\s*\n").matcher(text);

        while (m.find()) {
            if ((m.start() > ((text.length()/3) *2))) {
                return text.substring(0, m.start()); // has to be in the last third of the document
            }
        }

        m = Pattern.compile("\\p{Digit}*\\.*\\s*References\\s*\n").matcher(text);

        while (m.find()) {
            if ((m.start() > ((text.length()/3) *2))) {
                return text.substring(0, m.start()); // has to be in the last third of the document
            }
        }

        return text;
    }

    @Override
    public String removeSpacing(String text) {
        return text.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\\s+", " ");
    }

    /**
     * See https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
     * Punctuation: One of !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
     *
     * @param text
     * @return
     */
    @Override
    public String removePunctuation(String text, String whitelist) {
        //String pattern = "(?![@',&])\\p{Punct}"; // find any punctuation except @',&

        String pattern = whitelist != null ? "(?![" + whitelist + "])\\p{Punct}" : "\\p{Punct}";

        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m = r.matcher(text);

        return m.replaceAll("");
    }

    @Override
    public String lowerCaseAfterFullStop(String text) {
        StringBuilder sb = new StringBuilder(text);
        //"((\\.{1,2})|(\\.{4,})|(\\?+)|(!+))\\s\\p{Upper}"
        Matcher m = Pattern.compile("((\\.)|(\\?)|(!))\\s\\p{Upper}").matcher(text);

        while (m.find()) {
            sb.setCharAt(m.end() - 1, Character.toLowerCase(sb.charAt(m.end() -1)));
        }

        return sb.toString();
    }

    @Override
    public String removeShortWords(String text) {
        return text.replaceAll("\\s\\w{1,3}\\s", " ");//.replaceAll("\\s\\w{1,3}\\s*", "");

        /*
        StringBuilder sb = new StringBuilder(text);
        Matcher m = Pattern.compile("\\s\\w{1,3}\\s").matcher(text);

        while (m.find()) {
            sb.subSequence(m.start(), m.end());
            if (m.end()-m.start() < 5) {
                sb.subSequence(m.start(), m.end());
                sb.replace(m.start(), m.end(), " ");
            }
            sb.setCharAt(m.end() - 1, Character.toLowerCase(sb.charAt(m.end() -1)));
        }

        return sb.toString();
        */
    }

    @Override
    public String removeMarkup(String text) {
        return Jsoup.clean(text, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
    }

    @Override
    public String removeNonUnicode(String text) {
        // http://www.regular-expressions.info/unicode.html
        // p{L}letter
        // \p{Nd} or \p{Decimal_Digit_Number}: a digit zero through nine in any script except ideographic scripts.
        return text.replaceAll("[^\\p{L}\\p{Nd}]+", " ");
    }

    @Override
    public String removeDigits(String text) {
        return text.replaceAll("\\d+", " ");
    }

    @Override
    public String handleSyllables(String text) {
        Matcher m = Pattern.compile("\\s\\w+\\-\\s\\w+(\\s|\\p{Punct})").matcher(text);
        Map<String, String> replaceMap = new HashMap<>();

        while (m.find()) {
            String oldWord = text.substring(m.start()+1, m.end()-1);
            String newWord = oldWord.replaceAll("\\-\\s", "");
            replaceMap.put(oldWord, newWord);
        }

        String copy = text;
        for (String oldWord : replaceMap.keySet()) {
            final String newWord = replaceMap.get(oldWord);
            copy = copy.replaceAll(oldWord, newWord);
        }

        return copy;
    }

    //@Override
    public String removeDigits2(String text) {
        /*
        Matcher m = Pattern.compile("\\p{Blank}*\\d+\\p{Blank}+").matcher(text);

        while (m.find()) {
            System.out.println(text.substring(m.start(), m.end()));
            System.out.print("");
        }
        */

        // preserve 1st, 2nd etc.
        return text.replaceAll("\\p{Blank}*\\d+\\p{Blank}+", " ");
    }

}
