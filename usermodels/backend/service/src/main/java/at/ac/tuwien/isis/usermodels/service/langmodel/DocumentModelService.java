package at.ac.tuwien.isis.usermodels.service.langmodel;


import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
public interface DocumentModelService {

    DocumentModelDTO create(File doc) throws IOException;

    DocumentModelDTO create(String path) throws IOException;

    DocumentModelDTO create(String name, String content) throws IOException;

    DocumentModelDTO get(long id);

    String getRaw(long id);

    DocumentModelDTO get(String name);

    List<DocumentModelDTO> getAll();

    void delete(long id);

}
