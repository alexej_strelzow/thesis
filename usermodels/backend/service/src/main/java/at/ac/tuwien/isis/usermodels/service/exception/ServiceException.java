package at.ac.tuwien.isis.usermodels.service.exception;

/**
 * Created by Alexej Strelzow on 17.10.2014.
 */
public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
