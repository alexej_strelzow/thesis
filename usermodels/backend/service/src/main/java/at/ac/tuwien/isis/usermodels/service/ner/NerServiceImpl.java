package at.ac.tuwien.isis.usermodels.service.ner;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.sequences.SeqClassifierFlags;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Alexej Strelzow on 17.05.2016.
 */
@Service
@Log
public class NerServiceImpl implements NerService {

    final String MODEL_PATH = Configuration.NER_MODEL;
    private CRFClassifier<CoreLabel> classifier;

    public NerServiceImpl() {
        classifier = CRFClassifier.getClassifierNoExceptions(MODEL_PATH);
    }

    public Set<CoreLabel> identifyNER(String text) {
        Set<CoreLabel> ners = new LinkedHashSet<>();

        for (List<CoreLabel> coreLabels : classifier.classify(text)) {
            for (CoreLabel coreLabel : coreLabels) {
                //String word = coreLabel.word();
                String category = coreLabel.get(CoreAnnotations.AnswerAnnotation.class);
                if(!SeqClassifierFlags.DEFAULT_BACKGROUND_SYMBOL.equals(category)) {
                    ners.add(coreLabel);
                }
            }
        }
        return ners;
    }

    public HashMap<String,LinkedHashSet<String>> identifyNERWithCategory(String text) {
        HashMap<String, LinkedHashSet<String>> map = new LinkedHashMap<>();
        List<List<CoreLabel>> classify = classifier.classify(text);
        for (List<CoreLabel> coreLabels : classify) {
            for (CoreLabel coreLabel : coreLabels) {
                String word = coreLabel.word();
                String category = coreLabel.get(CoreAnnotations.AnswerAnnotation.class);
                if(!SeqClassifierFlags.DEFAULT_BACKGROUND_SYMBOL.equals(category)) {
                    if(map.containsKey(category)) {
                        map.get(category).add(word);
                    }
                    else {
                        LinkedHashSet<String> temp = new LinkedHashSet<>();
                        temp.add(word);
                        map.put(category, temp);
                    }
                    this.log.info(word+": "+category);
                }
            }
        }
        return map;
    }

    public static void main(String args[]) {
        String content = "The automotive company created by Henry Ford in 1903 is referred to as Ford or Ford Motor Company.";
        final NerServiceImpl nerService = new NerServiceImpl();
        for (CoreLabel ne : nerService.identifyNER(content)) {
            System.out.println(ne);
        }
    }

}