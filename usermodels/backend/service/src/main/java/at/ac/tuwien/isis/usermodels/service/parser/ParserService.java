package at.ac.tuwien.isis.usermodels.service.parser;

import at.ac.tuwien.isis.usermodels.dto.SourceType;
import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentDTO;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Alexej Strelzow on 23.04.2016.
 */
public interface ParserService {

    DocumentDTO extract(InputStream is, SourceType type) throws IOException;

    DocumentDTO extract(String url) throws IOException;

    DocumentDTO extract(File file) throws IOException;

}
