package at.ac.tuwien.isis.usermodels.service.protocol;

/**
 * Created by Alexej Strelzow on 17.04.2016.
 */
public interface FileProtocolService extends ProtocolService {

    void printProtocol(String directory);

    void printProtocol(String directory, String additionalContent);

}
