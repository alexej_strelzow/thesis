package at.ac.tuwien.isis.usermodels.service.langmodel;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
public interface FwlService {

    boolean isInFwlList(String word);

    long getLowestRank(String word);

    String getLemma(String word);

    boolean isInFwlList(String word, String pos);

    long getRank(String word, String pos);

    double getProbability(String word, String pos);

    String getLemma(String word, String pos);
}
