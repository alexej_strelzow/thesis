package at.ac.tuwien.isis.usermodels.service.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by Alexej Strelzow on 18.07.2016.
 */
@Service
@Scope
public class ModelGenerationConfiguration {

    /**
     * POST-PROCESSING, see AbstractModelServiceImpl
     */
    public static final boolean REMOVE_WORDS_FREQ_EQ_1 = false;         // hopefully not a big issue when having lots of docs
    public static final boolean REMOVE_WORDS_LENGTH_GT_1_LT_4 = true;   // small words are usually not that interesting
    public static final boolean REMOVE_NAMED_ENTITIES = true;           // detected by Stanford CRFClassifier
    public static final boolean REMOVE_WORDS_NOT_IN_BABELNET = true;    // filter out garbage, BabelNet is huge
    public static final boolean REMOVE_NNP_NOT_IN_FWL = true;           // remove proper nouns not in FWL, due to POS mis-classification
    public static final boolean REMOVE_WORDS_NOT_IN_FWL = false;        // defeats the purpose

    @Getter
    @Setter
    private boolean removeWordsFreqEq1 = REMOVE_WORDS_FREQ_EQ_1;

    @Getter
    @Setter
    private boolean removeWordsLengthGt1Lt4 = REMOVE_WORDS_LENGTH_GT_1_LT_4;

    @Getter
    @Setter
    private boolean removeNamedEntities = REMOVE_NAMED_ENTITIES;

    @Getter
    @Setter
    private boolean removeWordsNotInBabelnet = REMOVE_WORDS_NOT_IN_BABELNET;

    @Getter
    @Setter
    private boolean removeNnpNotInFwl = REMOVE_NNP_NOT_IN_FWL;

    @Getter
    @Setter
    private boolean removeWordsNotInFwl = REMOVE_WORDS_NOT_IN_FWL;
}
