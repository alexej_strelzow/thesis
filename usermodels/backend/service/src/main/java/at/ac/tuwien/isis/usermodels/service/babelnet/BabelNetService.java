package at.ac.tuwien.isis.usermodels.service.babelnet;

import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelNetWordDTO;
import at.ac.tuwien.isis.usermodels.dto.babelnet.BabelNetWordExplanationDTO;
import at.ac.tuwien.isis.usermodels.service.config.BabelNetConfiguration;
import at.ac.tuwien.isis.usermodels.service.exception.ServiceException;
import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.BabelSense;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.jlt.util.Language;

import java.io.IOException;
import java.util.List;

/**
 * Created by Alexej Strelzow on 06.05.2016.
 */
public interface BabelNetService {

    BabelNetConfiguration getConfiguration();

    List<BabelSense> getSenses(Language language, String word) throws ServiceException;

    List<BabelSense> getSenses(Language language, String word, POS pos) throws ServiceException;

    List<BabelSynset> getSynsets(Language language, String word) throws ServiceException;

    List<BabelSynset> getSynsets(Language language, String word, POS pos) throws ServiceException;

    BabelNetWordExplanationDTO explain(BabelNetWordDTO dto) throws ServiceException;

}
