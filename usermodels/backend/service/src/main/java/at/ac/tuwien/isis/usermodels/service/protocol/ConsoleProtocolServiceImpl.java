package at.ac.tuwien.isis.usermodels.service.protocol;

import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentProtocolDTO;
import at.ac.tuwien.isis.usermodels.service.tokenization.Operation;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by Alexej Strelzow on 10.04.2016.
 */
@Log
@Service(value = "console")
public class ConsoleProtocolServiceImpl extends AbstractProtocolServiceImpl {

    @Override
    public void printProtocol() {
        StringBuilder protocolBuilder = new StringBuilder();

        final Map<String, DocumentProtocolDTO> protocols = getProtocol(Operation.removeShortWords).getDocumentProtocols();
        for (String articleName : protocols.keySet()) {
            DocumentProtocolDTO docProtocol = protocols.get(articleName);
            final String removedWordsDiff = docProtocol.getDiff(Operation.removeShortWords.name());
            protocolBuilder.append(articleName).append("\r\n");
            protocolBuilder.append(docProtocol.toString()).append("\r\n");

            final Bag<String> diffBag = ModelUtils.textToBag(removedWordsDiff);
            protocolBuilder.append("Operation: removeShortWords (detailed)").append("\r\n");
            protocolBuilder.append("\tsize: " + diffBag.size()).append("\r\n");
            protocolBuilder.append("\tuniqueSet() size: " + diffBag.uniqueSet().size()).append("\r\n");
        }

        final Map<String, Bag<String>> removedWords = getProtocol().getRemovedWords();
        protocolBuilder.append("Removed words by operation:").append("\r\n");
        for (String key : removedWords.keySet()) {
            final Bag<String> words = removedWords.get(key);
            protocolBuilder.append("\t" + key).append("\r\n");
            protocolBuilder.append("\t\tTokens: " + words.size()).append("\r\n");
            protocolBuilder.append("\t\tTypes: " + words.uniqueSet().size()).append("\r\n");
        }

        this.log.info(protocolBuilder.toString());
    }
}
