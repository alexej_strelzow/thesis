package at.ac.tuwien.isis.usermodels.service.parser;

import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 18.08.2016.
 * inspired by: http://grokbase.com/t/tika/user/157f2tgs1h/per-page-document-content
 */
public class PdfPageHandler extends BodyContentHandler {

    private Map<Integer, String> pageContent = new LinkedHashMap<>();
    private String pageTag = "div";
    private String pageClass = "page";
    protected int pageNumber = 0;

    public PdfPageHandler(int writeLimit) {
        super(writeLimit);
    }

    @Override
    public void startElement(String uri, String localName, String name, Attributes atts) throws SAXException {
        super.startElement(uri, localName, name, atts);

        if (pageTag.equals(name) && pageClass.equals(atts.getValue("class"))) {
            startPage();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);

        if (length > 0) {
            String content = pageContent.get(pageNumber);
            if (content == null) {
                content = "";
            }
            pageContent.put(pageNumber, content + new String(ch));
        }
    }

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException {
        super.endElement(uri, localName, name);

        if (pageTag.equals(name)) {
            endPage();
        }
    }

    private void startPage() {
        pageNumber++;
    }

    private void endPage() {
        // noop
        //System.out.println("Page: " + pageNumber);
        //System.out.println(pageContent.get(pageNumber));
    }

    public Map<Integer, String> getPageContent() {
        return pageContent;
    }
}
