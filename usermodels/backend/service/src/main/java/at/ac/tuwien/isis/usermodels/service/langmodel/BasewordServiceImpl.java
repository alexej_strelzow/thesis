package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 17.06.2016.
 */
@Service
public class BasewordServiceImpl implements BasewordService {

    private Map<String, String> headwords;
    private Map<String, String> words;
    private Map<String, String> nnps;
    private String currentHeadword;


    public BasewordServiceImpl() {
        headwords = new HashMap<>();
        words = new HashMap<>();
        nnps = new HashMap<>();

        init();
    }

    private void init() {
        File dir = new File(Configuration.BASEWORD_DIR);

        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                // TODO: handle of nnps 15 + 16

                try {
                    BufferedReader br = new BufferedReader(new FileReader(f));
                    String line;

                    br.readLine();
                    while ((line = br.readLine()) != null) {
                        if (line.startsWith("\t")) {
                            words.put(getWordFromLine(line), currentHeadword);
                        } else {
                            currentHeadword = getWordFromLine(line);
                            headwords.put(currentHeadword, null);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getHeadword(String word) {
        if (headwords.containsKey(word)) {
            return word;
        }
        return words.get(word);
    }

    private String getWordFromLine(String line) {
        return line.split(" ")[0].trim();
    }

}
