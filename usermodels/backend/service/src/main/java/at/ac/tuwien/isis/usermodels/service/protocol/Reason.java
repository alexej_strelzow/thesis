package at.ac.tuwien.isis.usermodels.service.protocol;

/**
 * Created by Alexej Strelzow on 10.04.2016.
 */
public enum Reason {

    //NOT_IN_USER_MODEL,

    //NOT_IN_COLLECTION,

    NOT_IN_COLLECTION_NO_NE,

    WORD_RANK_GT_AVG_RANK,

    LEMMA_RANK_GT_AVG_RANK,

    ANTONYM_RANK_GT_AVG_RANK

}
