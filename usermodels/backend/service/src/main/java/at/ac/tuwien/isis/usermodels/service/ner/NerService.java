package at.ac.tuwien.isis.usermodels.service.ner;

import edu.stanford.nlp.ling.CoreLabel;

import java.util.Set;

/**
 * Created by Alexej Strelzow on 17.05.2016.
 */
public interface NerService {

    Set<CoreLabel> identifyNER(String text);

}
