package at.ac.tuwien.isis.usermodels.service.experiment;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.service.protocol.FileProtocolService;

import java.io.IOException;
import java.util.List;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public interface ExperimentService {

    ComparisonResultDTO run(String userModelDir, String docModelPath, FileProtocolService protocolService) throws IOException;

    ComparisonResultDTO run(long userModelId, long documentModelId, FileProtocolService protocolService) throws IOException;

    ComparisonResultDTO run(UserModelDTO userModelDTO, DocumentModelDTO documentModelDTO, FileProtocolService protocolService) throws IOException;

    ComparisonResultDTO get(long id);

    List<ComparisonResultDTO> getAll();
}
