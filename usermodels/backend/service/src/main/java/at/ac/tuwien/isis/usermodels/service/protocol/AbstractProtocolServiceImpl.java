package at.ac.tuwien.isis.usermodels.service.protocol;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentProtocolDTO;
import at.ac.tuwien.isis.usermodels.dto.protocol.ExperimentProtocolDTO;
import at.ac.tuwien.isis.usermodels.dto.protocol.OperationProtocolDTO;
import at.ac.tuwien.isis.usermodels.service.tokenization.Operation;
import at.ac.tuwien.isis.usermodels.service.utils.diff_match_patch;
import edu.stanford.nlp.ling.TaggedWord;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
@Log
public abstract class AbstractProtocolServiceImpl implements ProtocolService {

    private ExperimentProtocolDTO protocol;
    private boolean protocolInitialized;
    // docId -> docProtocol
    private Map<String, DocumentProtocolDTO> protocols;

    private diff_match_patch diff;
    private String currentDocId;
    private DocumentProtocolDTO docProtocol;
    private long startMs;
    private Operation currentOperation;
    private String currentBeforeText;
    private Map<String, Map<Operation, LinkedList<diff_match_patch.Diff>>> diffMap;
    private Map<String, Bag<String>> removedFromMergedBag;
    private Map<String, Bag<String>> unknownWordBag;

    public AbstractProtocolServiceImpl() {
        protocol = new ExperimentProtocolDTO();
        protocols = new LinkedHashMap<>();

        diff = new diff_match_patch();
        diffMap = new HashMap<>();

        removedFromMergedBag = new HashMap<>();
        for (RemoveType type : RemoveType.values()) {
            removedFromMergedBag.put(type.name(), new HashBag<>());
        }

        unknownWordBag = new HashMap<>();
        for (Reason reason : Reason.values()) {
            unknownWordBag.put(reason.name(), new HashBag<>());
        }
    }

    @Override
    public void protocolDocumentStart(String docId) {
        docProtocol = new DocumentProtocolDTO();
        currentDocId = docId;
        diffMap.put(currentDocId, new HashMap<>());
    }

    @Override
    public void protocolUnitStart(Operation operation, String before) {
        this.currentOperation = operation;
        this.currentBeforeText = before;
        this.startMs = System.currentTimeMillis();
    }

    @Override
    public void protocolUnitEnd(Operation operation, String after) {
        if (currentOperation != null && currentOperation.equals(operation)) {
            protocol(operation, startMs, System.currentTimeMillis(), currentBeforeText, after);
            this.currentOperation = null;
            this.currentBeforeText = null;
            this.startMs = 0;
        }
    }

    @Override
    public void protocolDocumentEnd(String docId) {
        if (currentDocId.equals(docId)) {
            protocols.put(docId, docProtocol);
        }
    }

    @Override
    public DocumentProtocolDTO getProtocol(String docId, Operation... diffForOperations) {
        final DocumentProtocolDTO protocol = protocols.get(docId);
        if (protocol == null) {
            return null;
        }

        for (Operation diffForOperation : diffForOperations) {
            protocol.addDiff(diffForOperation.name(), getDiff(docId, diffForOperation));
        }
        return protocol;
    }

    @Override
    public ExperimentProtocolDTO getProtocol(Operation... diffForOperations) {
        if (protocolInitialized) {
            return protocol;
        }

        if (diffForOperations != null) {
            for (String docId : protocols.keySet()) {
                DocumentProtocolDTO protocol = protocols.get(docId);
                for (Operation diffForOperation : diffForOperations) {
                    protocol.addDiff(diffForOperation.name(), getDiff(docId, diffForOperation));
                }
            }
        }

        protocol.setDocumentProtocols(Collections.unmodifiableMap(protocols));
        protocol.setRemovedWords(Collections.unmodifiableMap(removedFromMergedBag));
        protocol.setUnknownWords(Collections.unmodifiableMap(unknownWordBag));

        protocolInitialized = true;

        return protocol;
    }

    @Override
    public void protocolRemovedWord(RemoveType type, String word, String pos) {
        removedFromMergedBag.get(type.name()).add(word + "#" + pos);
    }

    @Override
    public void protocolUnknownWord(Reason reason, String word, String pos) {
        unknownWordBag.get(reason.name()).add(word + "#" + pos);
    }

    @Override
    public void analyzeTokens(String article, Bag<TaggedWordWrapper> bag) {
        final DocumentProtocolDTO documentProtocolDTO = protocols.get(article);
        if (documentProtocolDTO != null) {
            for (TaggedWordWrapper tw : bag) {
                final TaggedWord taggedWord = tw.get();
                documentProtocolDTO.addWord(taggedWord.value(), taggedWord.tag());
            }
        }
    }

    private void protocol(Operation operation, long startMs, long endMs, String textBefore, String textAfter) {
        OperationProtocolDTO entry = new OperationProtocolDTO(operation.name(), operation.getDesc(), startMs, endMs, textBefore, textAfter);
        docProtocol.addOperationProtocol(entry);
        final Map<Operation, LinkedList<diff_match_patch.Diff>> operationLinkedListMap = diffMap.get(currentDocId);
        operationLinkedListMap.put(operation, diff.diff_main(textBefore, textAfter));
    }

    private String getDiff(String docId, Operation operation) {
        StringBuilder diffBuilder = new StringBuilder();

        if (Operation.removeShortWords.equals(operation)) {
            // special case - need diff of diff
            for (diff_match_patch.Diff d : diffMap.get(docId).get(operation)) {
                if (d.operation == diff_match_patch.Operation.DELETE) {
                    String beforeText = d.text;
                    Matcher m = Pattern.compile("\\s\\w{1,3}\\s").matcher(beforeText);
                    while (m.find()) {
                        String deletedWord = beforeText.subSequence(m.start(), m.end()).toString().trim();
                        diffBuilder.append(deletedWord).append(" ");
                    }
                }
            }
        } else {
            for (diff_match_patch.Diff d : diffMap.get(docId).get(operation)) {
                if (d.operation == diff_match_patch.Operation.DELETE) {
                    diffBuilder.append(d.text);

                } else if (d.operation == diff_match_patch.Operation.INSERT) {
                    if (d.text.trim().length() > 1) {
                        System.err.println("We should never get here -> otherwise we got more content from one pp-step -> next one");
                    }

                } else {
                    // equals -  not interested in
                }
            }
        }
        return diffBuilder.toString();
    }

    @Override
    public void printProtocol() {
        // noop
    }
}