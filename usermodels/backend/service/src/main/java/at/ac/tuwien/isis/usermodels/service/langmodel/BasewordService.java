package at.ac.tuwien.isis.usermodels.service.langmodel;

/**
 * Created by Alexej Strelzow on 17.06.2016.
 */
public interface BasewordService {

    String getHeadword(String word);
}
