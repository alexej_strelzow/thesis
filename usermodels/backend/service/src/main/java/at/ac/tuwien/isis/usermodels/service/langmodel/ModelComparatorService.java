package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.service.protocol.ProtocolService;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public interface ModelComparatorService {

    void setProtocolService(ProtocolService protocolService);

    ComparisonResultDTO compare(UserModelDTO userModel, DocumentModelDTO documentModel);

}
