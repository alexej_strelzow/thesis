package at.ac.tuwien.isis.usermodels.service.protocol;

import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentProtocolDTO;
import at.ac.tuwien.isis.usermodels.dto.protocol.ExperimentProtocolDTO;
import at.ac.tuwien.isis.usermodels.service.tokenization.Operation;
import org.apache.commons.collections4.Bag;

/**
 * Created by Alexej Strelzow on 09.04.2016.
 */
public interface ProtocolService {

    void protocolDocumentStart(String docId);

    void protocolUnitStart(Operation operation, String before);

    void protocolUnitEnd(Operation operation, String after);

    void protocolDocumentEnd(String docId);

    void protocolRemovedWord(RemoveType type, String word, String pos);

    void protocolUnknownWord(Reason reason, String word, String pos);

    DocumentProtocolDTO getProtocol(String docId, Operation... diffForOperations);

    ExperimentProtocolDTO getProtocol(Operation... diffForOperations);

    void printProtocol();

    void analyzeTokens(String article, Bag<TaggedWordWrapper> bag);
}
