package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
@Service
public class FwlServiceImpl implements FwlService {

    // "word#pos" -> Word (word + pos) -> not unique
    private Map<WordId, Word> wordMap;
    // duplicates with higher rank unique -> (word + lemma + pos)
    private List<Word> duplicateWordsWithHigherRank;

    // "word#pos" -> Word (word + pos) -> not unique
    private Map<LemmaId, Word> lemmaMap;
    // duplicates with higher rank unique -> (word + lemma + pos)
    private List<Word> duplicateLemmasWithHigherRank;


    // CLAWS7 -> PENN
    private Map<String, String> claws7ToPenn;

    private Map<String, Integer> wordToRank;
    private Map<String, String> wordToLemma;
    //private Map<String, Long> wordToFrequency;
    private long frequencySum;

    public FwlServiceImpl() {
        claws7ToPenn = new HashMap<>();
        frequencySum = 0;

        wordToRank = new HashMap<>();
        wordToLemma = new HashMap<>();
        //wordToFrequency = new HashMap<>();

        if (Configuration.IS_WORD_DRIVEN) {
            wordMap = new HashMap<>();
            duplicateWordsWithHigherRank = new LinkedList<>();
        } else {
            lemmaMap = new HashMap<>();
            duplicateLemmasWithHigherRank = new LinkedList<>();
        }

        readConversionTable();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(Configuration.FWL_CSV_FILE);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;

        try {
            br.readLine();
            while ((line = br.readLine()) != null) {
                // rank, word, lemma, pos
                final String[] split = line.split(";");
                int rank = Integer.valueOf(split[0]);
                String word = split[1].trim();
                String lemma = split[2].trim();
                String pos = split[3].trim();
                Long frequency = Long.valueOf(split[4].trim());

                if (!wordToRank.containsKey(word)) {
                    wordToRank.put(word, rank);
                    wordToLemma.put(word, lemma);
                    //wordToFrequency.put(word, frequency);
                } else {
                    if (wordToRank.get(word) > rank) {
                        wordToRank.put(word, rank);
                        wordToLemma.put(word, lemma);
                    }
                }

                if (Configuration.IS_WORD_DRIVEN) {
                    final WordId wordId = createWordId(word, getPennForClaws7(pos.toUpperCase()));
                    final Word wordObj = createWord(wordId, lemma, rank, frequency);

                    //1830;provided;provide;vvn;24283 | 98325;provided;provided;vvn;17 (TAKE WITH LOWER RANK!)
                    if (wordMap.containsKey(wordId)) {
                        final Word wordInMap = wordMap.get(wordId);
                        if (wordInMap.getRank() > wordObj.getRank()) {
                            duplicateWordsWithHigherRank.add(wordInMap);
                            wordMap.put(wordId, wordObj);
                        } else {
                            duplicateWordsWithHigherRank.add(wordObj);
                        }

                    } else {
                        wordMap.put(wordId, wordObj);
                    }

                } else {
                    final LemmaId lemmaId = createLemmaId(lemma, getPennForClaws7(pos.toUpperCase()));
                    final Word lemmaObj = createWord(lemmaId, word, rank, frequency);

                    if (lemmaMap.containsKey(lemmaId)) {
                        final Word wordInMap = wordMap.get(lemmaId);
                        if (wordInMap.getRank() > lemmaObj.getRank()) {
                            duplicateWordsWithHigherRank.add(wordInMap);
                            lemmaMap.put(lemmaId, lemmaObj);
                        } else {
                            duplicateWordsWithHigherRank.add(lemmaObj);
                        }

                    } else {
                        lemmaMap.put(lemmaId, lemmaObj);
                    }
                }

                frequencySum += frequency;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // set probabilities
        if (Configuration.IS_WORD_DRIVEN) {
            for (Word word : wordMap.values()) {
                word.setProbability(word.getFrequency() / frequencySum);
            }
        } else {
            for (Word word : lemmaMap.values()) {
                word.setProbability(word.getFrequency() / frequencySum);
            }
        }
    }

    private void readConversionTable() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(Configuration.FWL_CONVERSION_FILE);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = br.readLine()) != null) {
                // http://ucrel.lancs.ac.uk/claws7tags.html
                // http://www.clips.ua.ac.be/pages/mbsp-tags
                if (line.startsWith("#") || line.isEmpty()) {
                    continue;
                }

                final String[] split = line.split(";");
                String claws7 = split[0].trim();
                //String google = split[1].trim();
                String penn = split[2].trim();

                claws7ToPenn.put(claws7.toUpperCase(), penn.toUpperCase());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getPennForClaws7(String claws7Pos) {
        if (claws7ToPenn.containsKey(claws7Pos)) {
            return claws7ToPenn.get(claws7Pos);
        } else {
            switch (claws7Pos) {
                case "PP":
                    return "PRP";
                case "VV":
                    return "MD";
                default:
                    System.err.println("Could not convert: " + claws7Pos);
                    return "?";
            }
        }
    }

    private WordId createWordId(String word, String pos) {
        return new WordId(word, pos);
    }

    private LemmaId createLemmaId(String lemma, String pos) {
        return new LemmaId(lemma, pos);
    }

    private Word createWord(WordId wordId, String lemma, long rank, long frequency) {
        return new Word(wordId, lemma, rank, frequency);
    }

    private Word createWord(LemmaId lemmaId, String word, long rank, long frequency) {
        return new Word(lemmaId, word, rank, frequency);
    }

    @Override
    public boolean isInFwlList(String word) {
        return wordToRank.containsKey(word);
    }

    @Override
    public long getLowestRank(String word) {
        if (wordToRank.containsKey(word)) {
            return wordToRank.get(word);
        } else {
            return -1;
        }
    }

    @Override
    public String getLemma(String word, String pos) {
        final WordId id = createWordId(word, pos);
        if (wordMap.containsKey(id)) {
            return wordMap.get(id).getLemma();
        } else {
            return null;
        }
    }

    @Override
    public String getLemma(String word) {
        if (wordToLemma.containsKey(word)) {
            return wordToLemma.get(word);
        }
        return null;
    }

    public boolean isInFwlList(String word, String pos) {
        return wordMap.containsKey(createWordId(word, pos));
    }

    public long getRank(String word, String pos) {
        final WordId id = createWordId(word, pos);
        if (wordMap.containsKey(id)) {
            return wordMap.get(id).getRank();
        } else {
            return -1;
        }
    }

    public double getProbability(String word, String pos) {
        final WordId id = createWordId(word, pos);
        if (wordMap.containsKey(id)) {
            return wordMap.get(id).getProbability();
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        final FwlServiceImpl fwlService = new FwlServiceImpl();
        System.out.println("Sum of all frequencies: " + fwlService.frequencySum);
    }

    @Data
    private class LemmaId {
        private String lemma;
        private String pos;

        public LemmaId(String lemma, String pos) {
            this.lemma = lemma;
            this.pos = pos;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof LemmaId))
                return false;
            if (obj == this)
                return true;

            LemmaId l = (LemmaId)obj;

            return new EqualsBuilder()
                    .append(this.lemma, l.lemma)
                    .append(this.pos, l.pos)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public String toString() {
            return (lemma + "#" + pos);
        }
    }

    @Data
    private class WordId {
        private String word;
        private String pos;

        public WordId(String word, String pos) {
            this.word = word;
            this.pos = pos;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof WordId))
                return false;
            if (obj == this)
                return true;

            WordId w = (WordId)obj;

            return new EqualsBuilder()
                    .append(this.word, w.word)
                    .append(this.pos, w.pos)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public String toString() {
            return (word + "#" + pos);
        }
    }

    @Data
    private class Word {
        private WordId wordId;
        private LemmaId lemmaId;

        private String lemma;
        private String word;
        private long rank;
        private long frequency;
        private double probability;

        public Word(WordId wordId, String lemma, long rank, long frequency) {
            this.wordId = wordId;
            this.lemma = lemma;
            this.rank = rank;
            this.frequency = frequency;
        }

        public Word(LemmaId lemmaId, String word, long rank, long frequency) {
            this.lemmaId = lemmaId;
            this.word = word;
            this.rank = rank;
            this.frequency = frequency;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Word))
                return false;
            if (obj == this)
                return true;

            Word w = (Word)obj;

            if (isWordDriven()) {
                return new EqualsBuilder()
                        .append(this.wordId.getWord(), w.wordId.getWord())
                        .append(this.wordId.getPos(), w.wordId.getPos())
                        .isEquals();
            } else {
                return new EqualsBuilder()
                        .append(this.lemmaId.getLemma(), w.lemmaId.getLemma())
                        .append(this.lemmaId.getPos(), w.wordId.getPos())
                        .isEquals();
            }
        }

        public boolean isWordDriven() {
            return wordId != null;
        }

        public boolean isLemmaDriven() {
            return lemmaId != null;
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public String toString() {
            return wordId.toString();
        }
    }

}
