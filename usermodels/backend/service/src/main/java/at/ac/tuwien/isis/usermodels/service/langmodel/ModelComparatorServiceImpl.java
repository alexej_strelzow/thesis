package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetService;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.exception.ServiceException;
import at.ac.tuwien.isis.usermodels.service.protocol.ProtocolService;
import at.ac.tuwien.isis.usermodels.service.protocol.Reason;
import at.ac.tuwien.isis.usermodels.service.utils.BabelNetUtils;
import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.BabelPointer;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.babelnet.BabelSynsetType;
import it.uniroma1.lcl.jlt.util.EnglishLemmatizer;
import it.uniroma1.lcl.jlt.util.Language;
import lombok.Setter;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
@Log
@Service
public class ModelComparatorServiceImpl implements ModelComparatorService {

    @Autowired
    @Setter
    private Configuration configuration;

    @Autowired
    @Setter
    private FwlService fwlService;

    @Autowired @Qualifier("dummy")
    @Setter
    protected ProtocolService protocolService;

    @Autowired
    @Setter
    private BabelNetService babelNetService;

    private EnglishLemmatizer lemmatizer;

    public ModelComparatorServiceImpl() {
        try {
            this.lemmatizer = new EnglishLemmatizer();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
        }
    }

    @Override
    public void setProtocolService(ProtocolService protocolService) {
        this.protocolService = protocolService;
    }

    @Override
    public ComparisonResultDTO compare(UserModelDTO userModel, DocumentModelDTO documentModel) {
        if (Configuration.USE_LEMMA) {
            return compareWithLemma(userModel, documentModel);
        } else {
            return compareWithoutLemma(userModel, documentModel);
        }
    }

    public ComparisonResultDTO compareWithLemma(UserModelDTO userModel, DocumentModelDTO documentModel) {
        Bag<TaggedWordWrapper> unknownWords = new HashBag<>();
        Bag<TaggedWordWrapper> knownWordsUserModel = new HashBag<>();

        final double avgRank = userModel.getAvgRank();

        for (TaggedWordWrapper docWrapper : documentModel.getModel().uniqueSet()) {
            final String word = docWrapper.get().value().toLowerCase();
            final String pos = docWrapper.get().tag();

            final boolean isInMu = userModel.getModel().contains(docWrapper);

            if (isInMu) {
                knownWordsUserModel.add(docWrapper);
                continue;
            }

            // gather meta data
            final boolean isInFwlListWithPoS = fwlService.isInFwlList(word, pos);
            final boolean isInFwlList = fwlService.isInFwlList(word);
            final boolean isWordsLowestRankGtAvgRank = fwlService.getLowestRank(word) > avgRank;

            String lemma;
            boolean isLemmasLowestRankGtAvgRank;
            boolean isLemmaInFwlList;
            lemma = getLemma(word);
            isLemmaInFwlList = fwlService.isInFwlList(lemma);
            isLemmasLowestRankGtAvgRank = fwlService.getLowestRank(lemma) > avgRank;

            final boolean useBabelNet = Configuration.USE_BABELNET;

            if (!isInFwlListWithPoS && isInFwlList && isWordsLowestRankGtAvgRank && isLemmasLowestRankGtAvgRank) {
                unknownWords.add(docWrapper);
                protocolService.protocolUnknownWord(Reason.LEMMA_RANK_GT_AVG_RANK, lemma, "");
                continue;
            }

            if (!isInFwlListWithPoS && !isInFwlList) {
                if (isLemmaInFwlList) {
                    if (isLemmasLowestRankGtAvgRank) {
                        unknownWords.add(docWrapper);
                        protocolService.protocolUnknownWord(Reason.LEMMA_RANK_GT_AVG_RANK, lemma, "");
                    }
                } else {
                    unknownWords.add(docWrapper);
                    protocolService.protocolUnknownWord(Reason.NOT_IN_COLLECTION_NO_NE, word, pos);
                }
                continue;
            }

            final long wordRank = fwlService.getRank(word, pos);
            final boolean wordRankWithPoSGtAvgRank = wordRank > avgRank;
            final boolean wordLowestRankGtAvgRank = fwlService.getLowestRank(word) > avgRank;
            lemma = fwlService.getLemma(word.toLowerCase(), pos);
            isLemmasLowestRankGtAvgRank = fwlService.getLowestRank(lemma) > avgRank;

            if (isInFwlListWithPoS && wordRankWithPoSGtAvgRank && wordLowestRankGtAvgRank && isLemmasLowestRankGtAvgRank) {
                if (useBabelNet) {
                    final POS posForTag = BabelNetUtils.getPosForTag(pos);
                    if (POS.ADJECTIVE.equals(posForTag) || POS.VERB.equals(posForTag)) {
                        final String antonymWord = getAntonym(Language.EN, word, posForTag);
                        if (antonymWord != null && fwlService.getLowestRank(antonymWord) > avgRank) {
                            unknownWords.add(docWrapper);
                            protocolService.protocolUnknownWord(Reason.ANTONYM_RANK_GT_AVG_RANK, antonymWord, pos);
                        } else {
                            /*if (antonymWord != null && fwlService.isInFwlList(antonymWord) && fwlService.getLowestRank(antonymWord) < avgRank) {
                                System.out.println();
                            }*/
                            unknownWords.add(docWrapper);
                            protocolService.protocolUnknownWord(Reason.LEMMA_RANK_GT_AVG_RANK, lemma, pos);
                        }
                    } else {
                        unknownWords.add(docWrapper);
                        protocolService.protocolUnknownWord(Reason.LEMMA_RANK_GT_AVG_RANK, lemma, "");
                    }
                } else {
                    unknownWords.add(docWrapper);
                    protocolService.protocolUnknownWord(Reason.LEMMA_RANK_GT_AVG_RANK, lemma, "");
                }
            }
        }

        final Set<TaggedWordWrapper> allWords = new HashSet<>(documentModel.getModel().uniqueSet());
        allWords.removeAll(new HashBag<>(unknownWords));
        allWords.removeAll(new HashBag<>(knownWordsUserModel));

        ComparisonResultDTO result = new ComparisonResultDTO(unknownWords);
        result.setKnownWordsUserModel(knownWordsUserModel);
        result.setKnownWordsCollection(new HashBag<>(allWords));

        return result;
    }

    public ComparisonResultDTO compareWithoutLemma(UserModelDTO userModel, DocumentModelDTO documentModel) {
        Bag<TaggedWordWrapper> unknownWords = new HashBag<>();
        Bag<TaggedWordWrapper> knownWordsUserModel = new HashBag<>();

        final double avgRank = userModel.getAvgRank();

        for (TaggedWordWrapper docWrapper : documentModel.getModel().uniqueSet()) {
            final String word = docWrapper.get().value().toLowerCase();
            final String pos = docWrapper.get().tag();

            final boolean isInMu = userModel.getModel().contains(docWrapper);

            if (isInMu) {
                knownWordsUserModel.add(docWrapper);
                continue;
            }

            // gather meta data
            final boolean isInFwlListWithPoS = fwlService.isInFwlList(word, pos);
            final boolean isInFwlList = fwlService.isInFwlList(word);
            final boolean isWordsLowestRankGtAvgRank = fwlService.getLowestRank(word) > avgRank;
            final boolean useBabelNet = Configuration.USE_BABELNET;

            if (!isInFwlListWithPoS && !isInFwlList) {
                unknownWords.add(docWrapper);
                protocolService.protocolUnknownWord(Reason.NOT_IN_COLLECTION_NO_NE, word, pos);
                continue;
            }

            if (!isInFwlListWithPoS && isInFwlList && isWordsLowestRankGtAvgRank) {
                unknownWords.add(docWrapper);
                protocolService.protocolUnknownWord(Reason.WORD_RANK_GT_AVG_RANK, word, "");
                continue;
            }

            final long wordRank = fwlService.getRank(word, pos);
            final boolean wordRankWithPoSGtAvgRank = wordRank > avgRank;
            final boolean wordLowestRankGtAvgRank = fwlService.getLowestRank(word) > avgRank;

            if (isInFwlListWithPoS && wordRankWithPoSGtAvgRank && wordLowestRankGtAvgRank) {
                if (useBabelNet) {
                    final POS posForTag = BabelNetUtils.getPosForTag(pos);
                    if (POS.ADJECTIVE.equals(posForTag) || POS.VERB.equals(posForTag)) {
                        final String antonymWord = getAntonym(Language.EN, word, posForTag);
                        if (antonymWord != null && fwlService.getLowestRank(antonymWord) > avgRank) {
                            unknownWords.add(docWrapper);
                            protocolService.protocolUnknownWord(Reason.ANTONYM_RANK_GT_AVG_RANK, antonymWord, pos);
                        } else {
                            unknownWords.add(docWrapper);
                            protocolService.protocolUnknownWord(Reason.WORD_RANK_GT_AVG_RANK, word, pos);
                        }
                    } else {
                        unknownWords.add(docWrapper);
                        protocolService.protocolUnknownWord(Reason.WORD_RANK_GT_AVG_RANK, word, "");
                    }
                } else {
                    unknownWords.add(docWrapper);
                    protocolService.protocolUnknownWord(Reason.WORD_RANK_GT_AVG_RANK, word, "");
                }
            }
        }

        final Set<TaggedWordWrapper> allWords = new HashSet<>(documentModel.getModel().uniqueSet());
        allWords.removeAll(new HashBag<>(unknownWords));
        allWords.removeAll(new HashBag<>(knownWordsUserModel));

        ComparisonResultDTO result = new ComparisonResultDTO(unknownWords);
        result.setKnownWordsUserModel(knownWordsUserModel);
        result.setKnownWordsCollection(new HashBag<>(allWords));

        return result;
    }

    private String getLemma(String word) {
        String lemma = fwlService.getLemma(word);
        if (lemma != null) {
            return lemma;
        } else {
            // TODO: maybe with POS
            final Set<String> lemmas = lemmatizer.getLemmas(word);
            if (lemmas.isEmpty()) {
                lemma = null;
            } else {
                lemma = lemmas.iterator().next();
            }

            return lemma;
        }
    }

    private String getAntonym(Language en, String word, POS posForTag) {
        try {
            final List<BabelSynset> synsets = babelNetService.getSynsets(Language.EN, word, posForTag);
            if (!synsets.isEmpty()) {
                final Map<IPointer, List<BabelSynset>> relatedMap = synsets.get(0).getRelatedMap();
                final List<BabelSynset> babelSynsets = relatedMap.get(BabelPointer.ANTONYM);
                if (babelSynsets != null && !babelSynsets.isEmpty()) {
                    final BabelSynset senses = babelSynsets.get(0); // TODO: but seems to be not much of an issue
                    final String mainSense = senses.getMainSense(); // perfect#a#1
                    return mainSense.substring(0, mainSense.indexOf("#"));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
