package at.ac.tuwien.isis.usermodels.service.utils;

import at.ac.tuwien.isis.usermodels.dto.babelnet.*;
import com.google.common.collect.Multimap;
import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.*;
import it.uniroma1.lcl.jlt.util.Language;
import it.uniroma1.lcl.jlt.util.ScoredItem;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by Alexej Strelzow on 12.04.2016.
 */
public class BabelNetUtils {

    /**
     * Source https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html.
     *
     *
     * @param pennTag
     * @return
     */
    public static POS getPosForTag(String pennTag) {

        /*
            12. 	NN 	    Noun, singular or mass
            13. 	NNS 	Noun, plural
            14. 	NNP 	Proper noun, singular
            15. 	NNPS 	Proper noun, plural
         */
        if ("N".equals(String.valueOf(pennTag.charAt(0)))) {
            return POS.NOUN;
        }

        /*
            7. 	JJ 	    Adjective
            8. 	JJR 	Adjective, comparative
            9. 	JJS 	Adjective, superlative
         */
        if ("J".equals(String.valueOf(pennTag.charAt(0)))) {
            return POS.ADJECTIVE;
        }

        /*
            27. 	VB 	    Verb, base form
            28. 	VBD 	Verb, past tense
            29. 	VBG 	Verb, gerund or present participle
            30. 	VBN 	Verb, past participle
            31. 	VBP 	Verb, non-3rd person singular present
            32. 	VBZ 	Verb, 3rd person singular present
         */
        if ("V".equals(String.valueOf(pennTag.charAt(0)))) {
            return POS.VERB;
        }

        /*
            20. 	RB 	    Adverb
            21. 	RBR 	Adverb, comparative
            22. 	RBS 	Adverb, superlative
         */
        if ("RB".equals(String.valueOf(pennTag.charAt(0) + "" + pennTag.charAt(1)))) {
            return POS.ADJECTIVE;
        }

        return null;
    }

    public static BabelSynset filterByMainSense(List<BabelSynset> synsets, String word) {
        for (BabelSynset synset : synsets) {
            if (synset.getMainSense() != null) {
                if (getWord(synset.getMainSense()).equals(word)) {
                    return synset;
                }
            }
        }

        return null;
    }

    public static List<BabelSynset> filterByLemma(List<BabelSynset> synsets, String lemma) {
        Set<BabelSynset> result = new HashSet<>();

        for (BabelSynset synset : synsets) {
            for (BabelSense sense : synset.getSenses(Language.EN, lemma)) {
                result.add(sense.getSynset());
            }
        }

        return new ArrayList<>(result);
    }

    public static String getWord(String mainSense) {
        String preparedWord = mainSense.replaceAll("_", " ");
        if (preparedWord.contains("#")) {
            return preparedWord.substring(0, preparedWord.indexOf("#"));

        } else if (preparedWord.contains(":")) {
            return preparedWord.substring(preparedWord.lastIndexOf(":")+1, preparedWord.length());
        }
        return null;
    }

    public static String getDisplayWord(String mainSense) {
        final String word = getWord(mainSense);
        String displayWord = word;

        if (word.contains("_")) {
            displayWord = word.replaceAll("_", " ");
        }

        return displayWord;
    }

    // TODO: provide filtering mechanism
    public static  List<BabelRelatedSynsetDTO> getRelatedSynsets(BabelSynset synset) throws IOException {
        Map<IPointer, List<BabelSynset>> relatedSynsets = new HashMap<>();
        final Map<IPointer, List<BabelSynset>> relatedMap = synset.getRelatedMap();

        for (IPointer pointer : relatedMap.keySet()) {
            if (!pointer.getSymbol().equals("r")) {
                relatedSynsets.put(pointer, relatedMap.get(pointer));
            }

            /*
            for (BabelSynset s : relatedMap.get(pointer)) {
                if (!pointer.getSymbol().equals("r")) {
                    relatedSynsets.add(s);
                }
            }
            */
        }

        return BabelToDtoConverter.convertRelatedSynsetsToDto(relatedSynsets);

        //return relatedSynsets;
    }

    public static List<BabelGloss> filterBabelGlosses(List<BabelGloss> glosses, Language language) {
        List<BabelGloss> results = new ArrayList<>();

        for (BabelGloss gloss : glosses) {
            if (gloss.getLanguage().equals(language)) {
                results.add(gloss);
            }
        }

        return results;
    }

    public static List<BabelGloss> filterBabelGlosses(List<BabelGloss> glosses, BabelSenseSource babelSenseSource) {
        List<BabelGloss> results = new ArrayList<>();

        for (BabelGloss gloss : glosses) {
            if (gloss.getSource().equals(babelSenseSource)) {
                results.add(gloss);
            }
        }

        return results;
    }

    public static List<BabelGloss> filterBabelGlosses(List<BabelGloss> glosses, Language language, BabelSenseSource babelSenseSource) {
        List<BabelGloss> results = new ArrayList<>();

        for (BabelGloss gloss : glosses) {
            if (gloss.getLanguage().equals(language) && gloss.getSource().equals(babelSenseSource)) {
                results.add(gloss);
            }
        }

        return results;
    }

    public static List<BabelGlossDTO> getGlossesByLanguage(BabelSynset synset, List<Language> destLangEnum) throws IOException {
        final List<BabelGlossDTO> glossDTOs = new ArrayList<>();

        for (Language lang : destLangEnum) {
            final List<BabelGloss> tmpGlosses = synset.getGlosses(lang);
            if (tmpGlosses != null && !tmpGlosses.isEmpty()) {
                glossDTOs.addAll(BabelToDtoConverter.convertGlossesToDto(tmpGlosses));
            }
        }

        return glossDTOs;
    }

    public static List<BabelGlossDTO> getGlossesBySource(BabelSynset synset, List<BabelSenseSource> babelSenseSource) throws IOException{
        final List<BabelGlossDTO> glossDTOs = new ArrayList<>();

        for (BabelGloss babelGloss : synset.getGlosses()) {
            if (babelSenseSource.contains(babelGloss.getSource())) {
                glossDTOs.add(BabelToDtoConverter.convertGlossToDto(babelGloss));
            }
        }

        return glossDTOs;
    }

    public static List<BabelGlossDTO> getGlosses(BabelSynset synset, List<Language> destLangEnum, List<BabelSenseSource> babelSenseSource) throws IOException{
        final List<BabelGlossDTO> glossDTOs = new ArrayList<>();

        if (destLangEnum != null && babelSenseSource != null) {
            for (Language lang : destLangEnum) {
                final List<BabelGloss> tmpGlosses = synset.getGlosses(lang);
                if (tmpGlosses != null && !tmpGlosses.isEmpty()) {
                    for (BabelGloss babelGloss : tmpGlosses) {
                        if (babelSenseSource.contains(babelGloss.getSource())) {
                            glossDTOs.add(BabelToDtoConverter.convertGlossToDto(babelGloss));
                        }
                    }
                }
            }

        } else if (destLangEnum != null && babelSenseSource == null) {
            return getGlossesByLanguage(synset, destLangEnum);

        } else if (destLangEnum == null && babelSenseSource != null) {
            return getGlossesBySource(synset, babelSenseSource);

        } else {
            return BabelToDtoConverter.convertGlossesToDto(synset.getGlosses());
        }

        return glossDTOs;
    }

    public static BufferedImage getImageFromURL(String validURL) {

        try {
            return ImageIO.read(new URL(validURL));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<BabelImage> filterImagesByLanguage(List<BabelImage> images, List<Language> languages) {
        List<BabelImage> filteredImages = new ArrayList<>();

        for (BabelImage image : images) {
            if (languages.contains(image.getLanguage())) {
                filteredImages.add(image);
            }
        }

        return filteredImages;
    }

    public static List<BabelImageDTO> getImages(BabelSynset synset, List<Language> destLangEnum, boolean validImagesOnly) {
        final List<BabelImageDTO> babelImageDTOs;

        if (destLangEnum != null) {
            babelImageDTOs = BabelToDtoConverter.convertImagesToDto(filterImagesByLanguage(synset.getImages(), destLangEnum), validImagesOnly);
        } else {
            babelImageDTOs = BabelToDtoConverter.convertImagesToDto(synset.getImages(),validImagesOnly);
        }

        return babelImageDTOs;
    }

    public static List<String> getWikiURLs(BabelSynset filteredSynset, List<Language> destLangEnum) {
        List<String> wikiURLs = new ArrayList<>();

        if (destLangEnum != null) {
            for (Language lang : destLangEnum) {
                for (BabelCategory babelCategory : filteredSynset.getCategories()) {
                    if (lang.equals(babelCategory.getLanguage())) {
                        wikiURLs.add(babelCategory.getWikipediaURI());
                    }
                }
            }
        } else {
            for (BabelCategory babelCategory : filteredSynset.getCategories()) {
                wikiURLs.add(babelCategory.getWikipediaURI());
            }
        }

        return wikiURLs;
    }

    public static List<BabelTranslationDTO> getTranslations(String word, Language language, List<Language> destLangEnum) throws IOException {
        final List<BabelTranslationDTO> translationDTOs = new ArrayList<>();

        if (destLangEnum != null) {
            final Multimap<Language, ScoredItem<String>> tmpTranslations = BabelNet.getInstance().getTranslations(language, word);
            if (tmpTranslations != null && !tmpTranslations.isEmpty()) {
                for (Language lang : destLangEnum) {
                    for (Language tmpLang : tmpTranslations.keySet()) {
                        if (lang.equals(tmpLang)) {
                            translationDTOs.add(BabelToDtoConverter.convertTranslationsToDto(lang, tmpTranslations.get(lang)));
                        }
                    }
                }
            }
        } else {
            translationDTOs.addAll(BabelToDtoConverter.convertTranslationsToDto(BabelNet.getInstance().getTranslations(language, word)));
        }

        return translationDTOs;
    }

    public static List<Language> convertLanguages(List<String> languageStrings) {
        List<Language> languageList = new ArrayList<>(languageStrings.size());

        for (String lang : languageStrings) {
            languageList.add(Language.valueOf(lang));
        }

        return languageList;
    }

    public static List<BabelSenseSource> convertSources(List<String> sources) {
        List<BabelSenseSource> sourceList = new ArrayList<>(sources.size());

        for (String source : sources) {
            final BabelSenseSource senseSource = BabelSenseSource.valueOf(source);
            if (senseSource != null) {
                sourceList.add(senseSource);
            }
        }

        return sourceList;
    }

    public static List<BabelSense> filterBySenseKey(List<BabelSense> senses, String senseKey) {
        List<BabelSense> results = new ArrayList<>();

        for (BabelSense sense : senses) {
            if (sense.getSensekey().equals(senseKey)) {
                results.add(sense);
            }
        }

        return results;
    }

    public static List<BabelSense> removeWikiRedirects(List<BabelSense> senses) {
        List<BabelSense> results = new ArrayList<>();

        for (BabelSense sense : senses) {
            if (!BabelSenseSource.WIKIRED.equals(sense.getSource())) {
                results.add(sense);
            }
        }

        return results;
    }

    public static List<BabelRelatedTermsDTO> getRelatedTermsByLanguage(BabelSynset synset, List<Language> destLangEnum) {
        final List<BabelRelatedTermsDTO> babelRelatedTermsDTOs = new ArrayList<>();

        for (Language lang : destLangEnum) {
            for (BabelSense sense : synset.getSenses(lang)) {
                babelRelatedTermsDTOs.add(BabelToDtoConverter.convertSensesToDto(sense));
            }
        }

        return babelRelatedTermsDTOs;
    }

    public static List<BabelRelatedTermsDTO> getRelatedTermsBySource(BabelSynset synset, List<BabelSenseSource> babelSenseSource) {
        final List<BabelRelatedTermsDTO> babelRelatedTermsDTOs = new ArrayList<>();

        for (BabelSense sense : synset.getSenses()) {
            if (babelSenseSource.contains(sense.getSource())) {
                babelRelatedTermsDTOs.add(BabelToDtoConverter.convertSensesToDto(sense));
            }
        }

        return babelRelatedTermsDTOs;
    }

    public static List<BabelRelatedTermsDTO> getRelatedTerms(BabelSynset synset, List<Language> destLangEnum, List<BabelSenseSource> babelSenseSource) {
        final List<BabelRelatedTermsDTO> babelRelatedTermsDTOs = new ArrayList<>();

        if (destLangEnum != null && babelSenseSource != null) {
            for (Language lang : destLangEnum) {
                for (BabelSense sense : synset.getSenses(lang)) {
                    if (babelSenseSource.contains(sense.getSource())) {
                        babelRelatedTermsDTOs.add(BabelToDtoConverter.convertSensesToDto(sense));
                    }
                }
            }

        } else if (destLangEnum != null && babelSenseSource == null) {
            return getRelatedTermsByLanguage(synset, destLangEnum);

        } else if (destLangEnum == null && babelSenseSource != null) {
            return getRelatedTermsBySource(synset, babelSenseSource);

        } else {
            return BabelToDtoConverter.convertSensesToDto(synset.getSenses());
        }

        return babelRelatedTermsDTOs;
    }

    public static List<BabelRelatedTermsDTO> getRelatedTerms(List<BabelSynset> synsets, List<Language> destLangEnum, List<BabelSenseSource> babelSenseSource) {
        final List<BabelRelatedTermsDTO> babelRelatedTermsDTOs = new ArrayList<>();

        for (BabelSynset synset : synsets) {
            babelRelatedTermsDTOs.addAll(getRelatedTerms(synset, destLangEnum, babelSenseSource));
        }

        return babelRelatedTermsDTOs;
    }

}
