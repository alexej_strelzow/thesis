package at.ac.tuwien.isis.usermodels.service.utils;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.Experiment;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlService;
import at.ac.tuwien.isis.usermodels.service.langmodel.FwlServiceImpl;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserService;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserServiceImpl;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.*;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
public class ModelUtils {

    public static String visualizeDiff(Bag<TaggedWordWrapper> userModel,
                                      Bag<TaggedWordWrapper> documentModel,
                                      Bag<TaggedWordWrapper> diff) {

        StringBuilder diffBuilder = new StringBuilder();

        FwlService fwlService = new FwlServiceImpl();
        diffBuilder.append("\r\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\r\n");
        diffBuilder.append("%                    Diff                  %\r\n");
        diffBuilder.append("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\r\n\r\n");
        diffBuilder.append("Size of tokens/types in Mu: " + userModel.size() + "/" + userModel.uniqueSet().size()).append("\r\n");
        diffBuilder.append("Size of tokens/types in Md: " + documentModel.size() + "/" + documentModel.uniqueSet().size()).append("\r\n");
        diffBuilder.append("Size of diff: " + diff.uniqueSet().size()).append("\r\n").append("\r\n");

        StringBuilder inFwlWithTagBuilder = new StringBuilder();
        StringBuilder inFwlBuilder = new StringBuilder();
        StringBuilder notInFwlBuilder = new StringBuilder();
        Set<String> uniqueWords = new TreeSet<>();

        int inFwlWithTagCounter = 0;
        int inFwlCounter = 0;

        for (TaggedWordWrapper docWrapper : new TreeSet<>(diff.uniqueSet())) {
            final TaggedWord word = docWrapper.get();
            final boolean isInUserModel = userModel.contains(docWrapper);
            final boolean inFwlListWithTag = fwlService.isInFwlList(word.word().toLowerCase(), word.tag());
            final boolean inFwlList = fwlService.isInFwlList(word.word().toLowerCase());
            final String rankWithTag = "" + fwlService.getRank(word.word().toLowerCase(), word.tag());
            final String rank = "" + fwlService.getLowestRank(word.word().toLowerCase());

            if (inFwlListWithTag) {
                inFwlWithTagCounter++;
                inFwlWithTagBuilder.append(String.format("%-20s  |  %-7s  |  %-7s  |  %-7s |  %-12s  |  %-7s", word.word(), word.tag(), rankWithTag, isInUserModel, inFwlListWithTag, inFwlList)).append("\r\n");
            } else {
                if (inFwlList) {
                    inFwlCounter++;
                    inFwlBuilder.append(String.format("%-20s  |  %-7s  |  %-7s  |  %-7s |  %-12s  |  %-7s", word.word(), word.tag(), rank, isInUserModel, inFwlListWithTag, inFwlList)).append("\r\n");
                } else {
                    notInFwlBuilder.append(String.format("%-20s  |  %-7s  |  %-7s  |  %-7s |  %-12s  |  %-7s", word.word(), word.tag(), rankWithTag, isInUserModel, inFwlListWithTag, inFwlList)).append("\r\n");
                }
            }

            uniqueWords.add(word.value().toLowerCase());
        }

        diffBuilder.append("NOT in Collection (FWL):   " + (diff.uniqueSet().size() - inFwlWithTagCounter - inFwlCounter)).append("\r\n");
        diffBuilder.append("In Collection (FWL):       " + inFwlCounter).append("\r\n");
        diffBuilder.append("In Collection (FWL + POS): " + inFwlWithTagCounter).append("\r\n").append("\r\n");
        diffBuilder.append(String.format("%-20s  |  %-7s  |  %-7s  |  %-7s  |  %-12s  |  %-7s", "Word", "POS", "Rank", "in Mu", "in Mc (POS)", "in Mc"));
        diffBuilder.append("\r\n-------------------------------------------------------------------------------------------------------").append("\r\n");
        diffBuilder.append(notInFwlBuilder.toString()).append("\r\n");
        diffBuilder.append(inFwlBuilder.toString()).append("\r\n");
        diffBuilder.append(inFwlWithTagBuilder.toString()).append("\r\n");

        StringBuilder uniqueWordBuilder = new StringBuilder("\r\nUnique words (size: " + uniqueWords.size() + ", lower case) without POS:\r\n");

        for (String uniqueWord : uniqueWords) {
            uniqueWordBuilder.append(uniqueWord).append("\r\n");
        }

        diffBuilder.append(uniqueWordBuilder.toString()).append("\r\n");


        System.out.print(diffBuilder.toString());

        return diffBuilder.toString();
    }

    public static Map<String, String> extractPdfs(File dir) throws IOException {
        Map<String, String> articles = new HashMap<>();
        PdfParserService pdfExtractor = new PdfParserServiceImpl();

        for (File article: dir.listFiles(new PdfFiler())) {
            articles.put(article.getName(), pdfExtractor.extract(article).getText());
        }

        return articles;
    }

    public static Map<String, String> extractPdfs(File usersDir, String userDir) throws IOException {
        Map<String, String> articles = new HashMap<>();
        PdfParserService pdfExtractor = new PdfParserServiceImpl();

        final File[] files = usersDir.listFiles(new NameFiler(userDir));
        if (files.length != 1) {
            System.err.println("No such directory " + userDir + " in user-root directory " + usersDir.getName());
            return null;
        }

        for (File article : files[0].listFiles(new PdfFiler())) {
            articles.put(article.getName(), pdfExtractor.extract(article).getText());
        }

        return articles;
    }

    public static byte[] convert(final Bag<TaggedWordWrapper> content) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(content);
        return out.toByteArray();
    }

    public static Bag<TaggedWordWrapper> convert(final byte[] content) throws IOException, ClassNotFoundException {
        ByteArrayInputStream  in = new ByteArrayInputStream(content);
        ObjectInputStream is = new ObjectInputStream(in);
        return (Bag<TaggedWordWrapper>) is.readObject();
    }

    public static ComparisonResultDTO convertExperimentResult(final byte[] content) throws IOException, ClassNotFoundException {
        ByteArrayInputStream  in = new ByteArrayInputStream(content);
        ObjectInputStream is = new ObjectInputStream(in);
        return (ComparisonResultDTO) is.readObject();
    }

    public static byte[] serializeComparisonResultDTO(ComparisonResultDTO obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static ComparisonResultDTO deserializeComparisonResultDTO(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream  in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return (ComparisonResultDTO) is.readObject();
    }

    public static DocumentModelDTO convertDocumentModel(DocumentModel model) {
        DocumentModelDTO dto;
        try {
            dto = new DocumentModelDTO(model.getDocument().getTitle(), model.getDocument().getId(), model.getId(), convert(model.getContent()));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return dto;
    }

    public static ComparisonResultDTO convertExperiment(Experiment experiment) {
        ComparisonResultDTO comparisonResultDTO = null;
        try {
            comparisonResultDTO = ModelUtils.convertExperimentResult(experiment.getResult());
            comparisonResultDTO.setExperimentId(experiment.getId());
            comparisonResultDTO.setUserModelId(experiment.getUserModel().getId());
            comparisonResultDTO.setDocumentModelId(experiment.getDocumentModel().getId());
        }catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return comparisonResultDTO;
    }

    /*
    public UserModel convert(UserModelDTO dto) {
        UserModel model;
        try {
            model = new UserModel("test", null, serializeUserModelDTO(dto));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return model;
    }

    public UserModelDTO convert(UserModel model) {
        UserModelDTO dto;
        try {
            dto = new UserModelDTO(deserializeContent(model.getContent()));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return dto;
    }

    public static DocumentModel convert(RawDocument rawDocument, DocumentModelDTO dto) {
        DocumentModel model;
        try {
            model = new DocumentModel(rawDocument, null, serializeDocumentModelDTO(dto));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return model;
    }

    public static DocumentModelDTO convert(DocumentModel model) {
        DocumentModelDTO dto;
        try {
            dto = new DocumentModelDTO(deserializeContent(model.getContent()));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return dto;
    }

    public static byte[] serializeUserModelDTO(UserModelDTO obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static UserModelDTO deserializeUserModelDTO(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream  in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return (UserModelDTO) is.readObject();
    }

    public static byte[] serializeDocumentModelDTO(DocumentModelDTO obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static DocumentModelDTO deserializeDocumentModelDTO(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream  in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return (DocumentModelDTO) is.readObject();
    }

    public static Bag<TaggedWordWrapper> deserializeContent(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream  in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return (Bag<TaggedWordWrapper>) is.readObject();
    }
    */

    /**
     * http://nlp.stanford.edu/software/tokenizer.shtml
     * TODO: checkout options -> untokenizable
     * @param text
     * @return
     */
    public static Bag<String> textToBag(String text) {
        InputStreamReader isr = new InputStreamReader(IOUtils.toInputStream(text));
        Bag<String> bag = new HashBag<>();

        PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(isr, new CoreLabelTokenFactory(), "");
        while (ptbt.hasNext()) {
            bag.add(ptbt.next().value());
        }

        return bag;
    }

    public static UserModelDTO convertUserModel(UserModel userModel) {
        Bag<TaggedWordWrapper> bag = null;
        try {
            bag = convert(userModel.getContent());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        UserModelDTO userModelDTO = new UserModelDTO(userModel.getId(), userModel.getName(), bag);
        userModelDTO.setInitalTokenSize(userModel.getInitalTokenSize());
        userModelDTO.setTokenSizeAfterPreprocessing(userModel.getTokenSizeAfterPreprocessing());
        Set<String> docNames = new HashSet<>();
        for (RawDocument document : userModel.getDocuments()) {
            docNames.add(document.getTitle());
        }
        userModelDTO.setContainingDocs(docNames);

        userModelDTO.setAvgDocLength(userModel.getAvgDocLength());
        userModelDTO.setAvgRank(userModel.getAvgRank());
        userModelDTO.setAvgRemovedTokens(userModel.getAvgRemovedTokens());
        userModelDTO.setAvgRemovedTokensAfterPreprocessing(userModel.getAvgRemovedTokensAfterPreprocessing());

        return userModelDTO;
    }

    private static class NameFiler implements FilenameFilter {
        private String name;

        private NameFiler(String name) {
            this.name = name;
        }

        @Override
        public boolean accept(File dir, String name) {
            return this.name.equals(name);
        }
    }

    private static class PdfFiler implements FilenameFilter {
        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(".pdf");
        }
    }

}
