package at.ac.tuwien.isis.usermodels.service;

import at.ac.tuwien.isis.usermodels.persistence.tables.User;

public interface SecurityContextService {
    User currentUser();
}
