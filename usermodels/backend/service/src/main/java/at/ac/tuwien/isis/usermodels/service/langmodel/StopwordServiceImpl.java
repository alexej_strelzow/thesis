package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
@Service
public class StopwordServiceImpl implements StopwordService {

    private Map<String, String> stopWords;

    public StopwordServiceImpl() {
        stopWords = new HashMap<>();
        final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(Configuration.STOP_WORDS_FILE);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;

        try {
            while ((line = br.readLine()) != null) {
                if (!line.startsWith("#")) {
                    stopWords.put(line.trim(), null);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isStopWord(String word) {
        return stopWords.containsKey(word);
    }

    public static void main(String[] args) {
        final StopwordService fwlService = new StopwordServiceImpl();
    }
}
