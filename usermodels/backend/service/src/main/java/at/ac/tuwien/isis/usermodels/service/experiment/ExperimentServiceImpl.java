package at.ac.tuwien.isis.usermodels.service.experiment;

import at.ac.tuwien.isis.usermodels.dto.ComparisonResultDTO;
import at.ac.tuwien.isis.usermodels.dto.DocumentModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.ExperimentRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.UserModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.Experiment;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.langmodel.DocumentModelService;
import at.ac.tuwien.isis.usermodels.service.langmodel.ModelComparatorServiceImpl;
import at.ac.tuwien.isis.usermodels.service.langmodel.UserModelService;
import at.ac.tuwien.isis.usermodels.service.protocol.FileProtocolService;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import lombok.Setter;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
@Log
@Service
@Transactional
public class ExperimentServiceImpl implements ExperimentService {

    @Autowired
    @Setter
    private Configuration configuration;

    @Autowired
    @Setter
    private ModelComparatorServiceImpl modelComparatorService;

    @Autowired
    @Setter
    private UserModelService userModelService;

    @Autowired
    @Setter
    private DocumentModelService documentModelService;

    @Autowired
    private UserModelRepository userModelRepository;

    @Autowired
    private DocumentModelRepository documentModelRepository;

    @Autowired
    private ExperimentRepository experimentRepository;

    @Override
    public ComparisonResultDTO run(String userModelDir, String docModelPath, FileProtocolService protocolService) throws IOException {
        if (protocolService != null) {
            userModelService.setProtocolService(protocolService);
            modelComparatorService.setProtocolService(protocolService);
        }

        final UserModelDTO userModelDTO = userModelService.create(userModelDir);

        if (userModelDTO != null) {
            final Bag<TaggedWordWrapper> userModel = userModelDTO.getModel();
            this.log.info("User-Model size: " + userModel.size());

            final DocumentModelDTO documentModelDTO = documentModelService.create(
                    new File(DocumentModelService.class.getClassLoader().getResource(docModelPath).getFile()));

            if (documentModelDTO != null) {
                final Bag<TaggedWordWrapper> documentModel = documentModelDTO.getModel();
                this.log.info("RawDocument-Model size: " + documentModel.size());

                // find unkown words
                final ComparisonResultDTO resultDTO = modelComparatorService.compare(userModelDTO, documentModelDTO);
                final String diffString = ModelUtils.visualizeDiff(userModelDTO.getModel(), documentModelDTO.getModel(), resultDTO.getUnknownWords());

                if (protocolService != null) {
                    protocolService.printProtocol(configuration.getExperiments(), diffString);
                }

                if (experimentRepository != null) {
                    final UserModel um = userModelRepository.findOne(userModelDTO.getId());
                    final DocumentModel dm = documentModelRepository.findOne(documentModelDTO.getId());
                    Experiment experiment = new Experiment(um, dm, ModelUtils.serializeComparisonResultDTO(resultDTO));
                    experiment = experimentRepository.save(experiment);
                    resultDTO.setExperimentId(experiment.getId());
                }

                return resultDTO;
            }

        } else {
            this.log.severe("Could not create model");
        }

        return null;
    }

    @Override
    public ComparisonResultDTO run(long userModelId, long documentModelId, FileProtocolService protocolService) throws IOException {
        final UserModelDTO userModelDTO = userModelService.get(userModelId);
        final DocumentModelDTO documentModelDTO = documentModelService.get(documentModelId);
        return run(userModelDTO, documentModelDTO, protocolService);
    }

    @Override
    public ComparisonResultDTO run(UserModelDTO userModelDTO, DocumentModelDTO documentModelDTO, FileProtocolService protocolService) throws IOException {
        if (userModelDTO != null && documentModelDTO != null) {
            if (protocolService != null) {
                modelComparatorService.setProtocolService(protocolService);
            }

            ComparisonResultDTO resultDTO = modelComparatorService.compare(userModelDTO, documentModelDTO);
            final String diffString = ModelUtils.visualizeDiff(userModelDTO.getModel(), documentModelDTO.getModel(), resultDTO.getUnknownWords());

            if (protocolService != null) {
                protocolService.printProtocol(configuration.getExperiments(), diffString);
            }

            if (experimentRepository != null) {
                final UserModel um = userModelRepository.findOne(userModelDTO.getId());
                final DocumentModel dm = documentModelRepository.findOne(documentModelDTO.getId());
                Experiment experiment = new Experiment(um, dm, ModelUtils.serializeComparisonResultDTO(resultDTO));
                experiment = experimentRepository.save(experiment);
                resultDTO.setExperimentId(experiment.getId());
            }

            return resultDTO;
        }
        return null;
    }

    @Override
    public ComparisonResultDTO get(long id) {
        final Experiment experiment = experimentRepository.findOne(id);

        return ModelUtils.convertExperiment(experiment);
    }

    @Override
    public List<ComparisonResultDTO> getAll() {
        List<ComparisonResultDTO> all = new ArrayList<>();

        //new Sort(new Sort.Order(Sort.Direction.ASC, "id"))
        for (Experiment experiment : experimentRepository.findAll()) {
            all.add(ModelUtils.convertExperiment(experiment));
        }

        return all;
    }
}
