package at.ac.tuwien.isis.usermodels.service.langmodel;


import at.ac.tuwien.isis.usermodels.dto.UserModelDTO;
import at.ac.tuwien.isis.usermodels.service.protocol.ProtocolService;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
public interface UserModelService {

    /**
     * Generate user model from directory
     *
     * @param dir
     * @return
     * @throws IOException
     */
    UserModelDTO create(File dir) throws IOException;

    /**
     * Generate user model from directory
     *
     * @param dirName
     * @return
     * @throws IOException
     */
    UserModelDTO create(String dirName) throws IOException;

    /**
     * Generate user model from pdf files, planned in later stage
     *
     * @param name
     * @param pdfFile
     * @return
     * @throws IOException
     */
    UserModelDTO create(String name, byte[] pdfFile) throws IOException;

    UserModelDTO createFromArticles(String name, Map<String, String> articles) throws IOException;

    UserModelDTO get(long id);

    UserModelDTO get(String name);

    List<UserModelDTO> getAll();

    void setProtocolService(ProtocolService protocolService);

    void delete(long id);

}
