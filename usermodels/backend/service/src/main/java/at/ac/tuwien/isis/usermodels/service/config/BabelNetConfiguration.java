package at.ac.tuwien.isis.usermodels.service.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by Alexej Strelzow on 18.07.2016.
 */
@Service
@Scope
public class BabelNetConfiguration {

    /**
     * BabelNet
     */
    public static final boolean VALID_IMAGES_ONLY = false;
    public static final boolean EXPLAIN_INCLUDE_GLOSS = true;
    public static final boolean EXPLAIN_INCLUDE_IMAGE = true;
    // in production we may set them to false
    public static final boolean EXPLAIN_INCLUDE_WIKI_URL = true;
    public static final boolean EXPLAIN_INCLUDE_TRANSLATION = true;
    public static final boolean EXPLAIN_INCLUDE_RELATED_SYNSET = true;
    public static final boolean EXPLAIN_INCLUDE_RELATED_TERM = false;

    public static final String EXPLAIN_DEFAULT_SRC_LANGUAGE = "EN";
    public static final String[] EXPLAIN_DEFAULT_DEST_LANGUAGES = new String[] {"EN", "DE"};

    @Getter
    @Setter
    private boolean validImagesOnly = VALID_IMAGES_ONLY;

    @Getter
    @Setter
    private boolean explainIncludeGloss = EXPLAIN_INCLUDE_GLOSS;

    @Getter
    @Setter
    private boolean explainIncludeImage = EXPLAIN_INCLUDE_IMAGE;

    @Getter
    @Setter
    private boolean explainIncludeWikiUrl = EXPLAIN_INCLUDE_WIKI_URL;

    @Getter
    @Setter
    private boolean explainIncludeTranslation = EXPLAIN_INCLUDE_TRANSLATION;

    @Getter
    @Setter
    private boolean explainIncludeRelatedSynset = EXPLAIN_INCLUDE_RELATED_SYNSET;

    @Getter
    @Setter
    private boolean explainIncludeRelatedTerm = EXPLAIN_INCLUDE_RELATED_TERM;

    @Getter
    @Setter
    private String explainDefaultSrcLanguage = EXPLAIN_DEFAULT_SRC_LANGUAGE;

    @Getter
    @Setter
    private String[] explainDefaultDestLanguages = EXPLAIN_DEFAULT_DEST_LANGUAGES;
}
