package at.ac.tuwien.isis.usermodels.service.langmodel;

/**
 * Created by Alexej Strelzow on 24.02.2016.
 */
public interface StopwordService {

    boolean isStopWord(String word);
}
