package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.dto.AbstractModelDTO;
import at.ac.tuwien.isis.usermodels.dto.TaggedWordWrapper;
import at.ac.tuwien.isis.usermodels.service.babelnet.BabelNetService;
import at.ac.tuwien.isis.usermodels.service.config.Configuration;
import at.ac.tuwien.isis.usermodels.service.config.ModelGenerationConfiguration;
import at.ac.tuwien.isis.usermodels.service.exception.ServiceException;
import at.ac.tuwien.isis.usermodels.service.ner.NerService;
import at.ac.tuwien.isis.usermodels.service.protocol.ProtocolService;
import at.ac.tuwien.isis.usermodels.service.protocol.RemoveType;
import at.ac.tuwien.isis.usermodels.service.tokenization.Operation;
import at.ac.tuwien.isis.usermodels.service.tokenization.PreprocessorService;
import at.ac.tuwien.isis.usermodels.service.tokenization.TokenizerService;
import at.ac.tuwien.isis.usermodels.service.utils.BabelNetUtils;
import edu.mit.jwi.item.POS;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.TaggedWord;
import it.uniroma1.lcl.jlt.util.Language;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
@Log
public abstract class AbstractModelServiceImpl implements ModelService {

    @Autowired
    @Setter
    private BabelNetService babelNet;

    @Autowired
    @Getter
    @Setter
    private Configuration configuration;

    @Autowired
    @Getter
    @Setter
    private ModelGenerationConfiguration modelGenerationConfiguration;

    @Autowired
    @Setter
    protected FwlService fwlService;

    @Autowired
    @Setter
    protected NerService nerService;

    @Autowired
    @Setter
    protected PreprocessorService preprocessor;

    @Autowired
    @Setter
    protected TokenizerService tokenizer;

    @Getter
    @Setter
    @Autowired @Qualifier("dummy")
    protected ProtocolService protocolService;

    protected AbstractModelDTO model;

    protected double avgDocLength;

    protected int initalTokenSize;
    protected int tokenSizeAfterPreprocessing;
    private int documentSize;
    protected double avgRemovedTokens;
    protected double avgRemovedTokensAfterPreprocessing;

    private Map<String, String> namedEntities;

    public AbstractModelServiceImpl() {
        // noop
    }

    @Override
    public Map<String, Bag<TaggedWordWrapper>> preprocess(Map<String, String> articles) {
        Map<String, Bag<TaggedWordWrapper>> bows = new HashMap<>();
        double docLengthSum = 0;
        documentSize = articles.size();

        for (String article : articles.keySet()) {
            String text = articles.get(article);
            docLengthSum += text.length();
            initalTokenSize += tokenizer.getNumberOfTokens(text);

            protocolService.protocolDocumentStart(article);

            // 0. remove reference chapter
            protocolService.protocolUnitStart(Operation.removeReferences, text);
            text = preprocessor.removeReferences(text); // depends on line breaks
            protocolService.protocolUnitEnd(Operation.removeReferences, text);

            // 1. lowercase after .?! (might destroy more than it brings us)
            protocolService.protocolUnitStart(Operation.lowerCaseAfterFullStop, text);
            text = preprocessor.lowerCaseAfterFullStop(text); // depends on punctuation
            protocolService.protocolUnitEnd(Operation.lowerCaseAfterFullStop, text);

            // 2. remove all kinds of spaces \r\n \t many spaces
            protocolService.protocolUnitStart(Operation.removeSpacing, text);
            text = preprocessor.removeSpacing(text);
            protocolService.protocolUnitEnd(Operation.removeSpacing, text);

            // 3. remove digits
            protocolService.protocolUnitStart(Operation.removeDigits, text);
            text = preprocessor.removeDigits(text);
            protocolService.protocolUnitEnd(Operation.removeDigits, text);

            // 4. remove markups
            protocolService.protocolUnitStart(Operation.removeMarkup, text);
            text = preprocessor.removeMarkup(text);
            protocolService.protocolUnitEnd(Operation.removeMarkup, text);

            // 5. remove punctuation (not good for PoS-tagging)
            //protocolService.protocolUnitStart(Operation.removePunctuation, text);
            //text = preprocessor.removePunctuation(text, "-"); // syllables
            //protocolService.protocolUnitEnd(Operation.removePunctuation, text);

            // 6. remove short words (not good for PoS-tagging)
            //protocolService.protocolUnitStart(Operation.removeShortWords, text);
            //text = preprocessor.removeShortWords(text);
            //protocolService.protocolUnitEnd(Operation.removeShortWords, text);

            // 7. handle syllables
            protocolService.protocolUnitStart(Operation.handleSyllables, text);
            text = preprocessor.handleSyllables(text);
            protocolService.protocolUnitEnd(Operation.handleSyllables, text);

            // 8. remove non unicode characters (probably done in post-processing)
            // we have it here, because it also removes the '-' character
            //protocolService.protocolUnitStart(Operation.removeNonUnicode, text);
            //text = preprocessor.removeNonUnicode(text);
            //protocolService.protocolUnitEnd(Operation.removeNonUnicode, text);

            protocolService.protocolDocumentEnd(article);

            tokenSizeAfterPreprocessing += tokenizer.getNumberOfTokens(text);

            namedEntities = new HashMap<>();
            for (CoreLabel coreLabel : nerService.identifyNER(text)) {
                final String word = coreLabel.value();
                String category = coreLabel.get(CoreAnnotations.AnswerAnnotation.class);
                if (word.length() > 3) {
                    namedEntities.put(word, category);
                }
            }

            Bag<TaggedWordWrapper> bag = tokenize(text);

            if (Configuration.DEBUG_ANALYZE_TOKENS) {
                protocolService.analyzeTokens(article, bag);
            }

            bows.put(article, bag);
        }

        avgDocLength = docLengthSum/documentSize;

        return bows;
    }

    @Override
    public Bag<TaggedWordWrapper> tokenize(String text) {
        return tokenizer.articleToTaggedTokens(text);
    }

    @Override
    public Bag<TaggedWordWrapper> postprocess(Bag<TaggedWordWrapper> mergedBows) {
        if (modelGenerationConfiguration.isRemoveWordsFreqEq1()) {
            Iterator<TaggedWordWrapper> iterator = mergedBows.iterator();
            while (iterator.hasNext()) {
                TaggedWordWrapper word = iterator.next();
                if (mergedBows.getCount(word) == 1) { // remove everything with count = 1
                    iterator.remove();
                    protocolService.protocolRemovedWord(RemoveType.FREQ_EQ_1, word.get().value(), word.get().tag());
                }
            }
        }

        if (modelGenerationConfiguration.isRemoveWordsLengthGt1Lt4()) {
            Iterator<TaggedWordWrapper> iterator = mergedBows.iterator();
            while (iterator.hasNext()) {
                final TaggedWord taggedWord = iterator.next().get();
                final String word = taggedWord.value();

                if (word.length() < 4) { // remove less than 4 characters
                    iterator.remove();
                    if (word.length() > 1) {
                        protocolService.protocolRemovedWord(RemoveType.LENGTH_GT_1_LT_4, word, taggedWord.tag());
                    }
                }
            }
        }

        // we don't go in there
        /*
        if (Configuration.REMOVE_WORDS_NOT_IN_FWL) {
            removeWordsNotInFwl(mergedBows);
        }
        */

        if (modelGenerationConfiguration.isRemoveNamedEntities()) {
            removeNamedEntities(mergedBows);
        }

        if (modelGenerationConfiguration.isRemoveWordsNotInBabelnet()) {
            try {
                removeWordsNotInBabelNet(mergedBows);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }

        if (modelGenerationConfiguration.isRemoveNnpNotInFwl()) {
            Map<String, TaggedWord> blackList = new HashMap<>();
            Iterator<TaggedWordWrapper> iterator = mergedBows.iterator();
            while (iterator.hasNext()) {
                final TaggedWord docWrapper = iterator.next().get();
                final String word = docWrapper.value();
                final String pos = docWrapper.tag();

                // filter out NNP that are not not in FWL, also NN(S) might be misclassified; but it will remove
                // almost all of NOT in COLLECTION (FWL) words! -> trade-off
                if (!fwlService.isInFwlList(word.toLowerCase()) &&
                        ("NNP".equals(pos) || "NNPS".equals(pos) /*|| "NN".equals(pos) || "NNS".equals(pos)*/)) {
                    iterator.remove();
                    blackList.put(word.toLowerCase(), docWrapper);
                    protocolService.protocolRemovedWord(RemoveType.BLACKLIST, word, pos);
                }
            }

            // find words that are miss-tagged, like Gleason#JJ, Gleason#NN and remove them
            iterator = mergedBows.iterator();
            while (iterator.hasNext()) {
                final TaggedWord docWrapper = iterator.next().get();
                final String word = docWrapper.value();
                final String pos = docWrapper.tag();

                if (blackList.containsKey(word.toLowerCase())) {
                    iterator.remove();
                    protocolService.protocolRemovedWord(RemoveType.BLACKLIST, word, pos);
                }
            }
           /*
            this.log.info("Blacklist");
            for (TaggedWordWrapper item : blackList.values()) {
                this.log.info("\t" + item.toString());
            }
            */
        }

        avgRemovedTokens = (initalTokenSize - mergedBows.size())/documentSize;
        avgRemovedTokensAfterPreprocessing = (tokenSizeAfterPreprocessing - mergedBows.size())/documentSize;

        return mergedBows;
    }

    /**
     * remove if NE and cannot be found in FWL:
     *   namedEntities.containsKey(word) : identified as NE
     *   && !fwlService.isInFwlList(word.toLowerCase()): weaken the constraint; we allow more words
     */
    private void removeNamedEntities(Bag<TaggedWordWrapper> mergedBows) {
        Iterator<TaggedWordWrapper> iterator = mergedBows.iterator();
        while (iterator.hasNext()) {
            final TaggedWord taggedWord = iterator.next().get();
            final String word = taggedWord.value();
            if (namedEntities.containsKey(word)) {
                //final String category = namedEntities.get(word); // can't rely on that
                if(!fwlService.isInFwlList(word.toLowerCase())) { // again, trade-off
                    iterator.remove();
                    protocolService.protocolRemovedWord(RemoveType.NAMED_ENTITY, word, taggedWord.tag());
                }
            }
        }
    }

    /**
     * Removes rubbish frmo mergedBows. If BabelNet does not know it, we just throw it away!
     *
     * @param mergedBows
     * @throws ServiceException
     */
    protected void removeWordsNotInBabelNet(Bag<TaggedWordWrapper> mergedBows) throws ServiceException {
        Iterator<TaggedWordWrapper> iterator = mergedBows.iterator();
        while(iterator.hasNext()) {
            final TaggedWord taggedWord = iterator.next().get();
            String word = taggedWord.value().toLowerCase();

            if (fwlService.isInFwlList(word)) {
                continue;
            }

            /*
            if ("NNP".equals(taggedWord.tag()) || "NNPS".equals(taggedWord.tag())) {
                System.out.println("");
            }
            */
            // TODO: think about removing words classified as Named_Entity from BabelNet

            final POS convertedPOS = BabelNetUtils.getPosForTag(taggedWord.tag());
            if (convertedPOS != null) {
                if (babelNet.getSenses(Language.EN, word, convertedPOS).isEmpty()) {
                    iterator.remove();
                    protocolService.protocolRemovedWord(RemoveType.BABELNET, taggedWord.value(), taggedWord.tag());
                }

            } else {
                if (babelNet.getSenses(Language.EN, word).isEmpty()) {
                    iterator.remove();
                    protocolService.protocolRemovedWord(RemoveType.BABELNET, taggedWord.value(), taggedWord.tag());
                }
            }
        }
    }

    /**
     * Defeats the purpose, don't!
     * @param mergedBows
     */
    protected void removeWordsNotInFwl(Bag<TaggedWordWrapper> mergedBows) {
        Iterator<TaggedWordWrapper> iterator = mergedBows.iterator();
        while(iterator.hasNext()) {
            final TaggedWord taggedWord = iterator.next().get();
            if(!fwlService.isInFwlList(taggedWord.value().toLowerCase(), taggedWord.tag())) {
                iterator.remove();
                protocolService.protocolRemovedWord(RemoveType.FWL, taggedWord.value(), taggedWord.tag());
            }
        }
    }
}