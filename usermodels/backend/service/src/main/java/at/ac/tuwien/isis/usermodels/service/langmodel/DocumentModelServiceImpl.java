package at.ac.tuwien.isis.usermodels.service.langmodel;

import at.ac.tuwien.isis.usermodels.dto.*;
import at.ac.tuwien.isis.usermodels.dto.protocol.DocumentDTO;
import at.ac.tuwien.isis.usermodels.persistence.repositories.DocumentModelRepository;
import at.ac.tuwien.isis.usermodels.persistence.repositories.RawDocumentRepository;
import at.ac.tuwien.isis.usermodels.persistence.tables.DocumentModel;
import at.ac.tuwien.isis.usermodels.persistence.tables.RawDocument;
import at.ac.tuwien.isis.usermodels.persistence.tables.User;
import at.ac.tuwien.isis.usermodels.persistence.tables.UserModel;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserService;
import at.ac.tuwien.isis.usermodels.service.parser.PdfParserServiceImpl;
import at.ac.tuwien.isis.usermodels.service.parser.PdfUtils;
import at.ac.tuwien.isis.usermodels.service.protocol.ProtocolService;
import at.ac.tuwien.isis.usermodels.service.utils.ModelUtils;
import lombok.extern.java.Log;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.*;

/**
 * Created by Alexej Strelzow on 28.03.2016.
 */
@Log
@Service
@Transactional
public class DocumentModelServiceImpl extends AbstractModelServiceImpl implements DocumentModelService {

    @Autowired
    private DocumentModelRepository documentRepository;

    @Autowired
    private RawDocumentRepository rawDocumentRepository;

    @Override
    public DocumentModelDTO create(File paper) throws IOException {
        PdfParserService pdfExtractor = new PdfParserServiceImpl();
        String text;

        try {
            final DocumentDTO documentDTO = pdfExtractor.extract(paper);
            //text = documentDTO.getText();
            text = documentDTO.getTextWithPageMarkup();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
            return null;
        }

        //return create(paper.getName(), text, SourceType.PDF);
        return createWithPageMarkup(paper.getName(), text, SourceType.PDF);
    }

    @Override
    public DocumentModelDTO create(String paper) throws IOException {
        final File file = new File(paper);
        if (file.exists()) {
            return create(file);
        }

        final URL url = DocumentModelServiceImpl.class.getClassLoader().getResource(paper);
        if (url != null && url.getFile() != null) {
            return create(url.getFile());
        }

        this.log.severe(paper + " does not exist!");

        return null;
    }

    @Override
    public Map<String, Bag<TaggedWordWrapper>> preprocess(Map<String, String> articles) {
        String article = articles.keySet().iterator().next();

        Bag<TaggedWordWrapper> documentBag = new HashBag<>();

        final String allPages = articles.get(article);
        final Map<Integer, String> pages = PdfUtils.getPages(allPages);

        for (Integer pageNumber : pages.keySet()) {
            Map<String, String> newArticles = new LinkedHashMap<>();
            newArticles.put(article, pages.get(pageNumber));
            final Map<String, Bag<TaggedWordWrapper>> pageBag = super.preprocess(newArticles);

            for (Bag<TaggedWordWrapper> bow : pageBag.values()) {
                for (TaggedWordWrapper word : bow) {
                    word.addPage(pageNumber);
                }
            }

            for (Bag<TaggedWordWrapper> bow : pageBag.values()) {
                documentBag.addAll(bow);
            }
        }

        Map<String, Bag<TaggedWordWrapper>> result = new HashMap<>();
        result.put(article, documentBag);

        return result;
    }

    @Override
    public DocumentModelDTO create(String name, String content) throws IOException {
        return create(name, content, SourceType.TXT, false);
    }

    @Override
    public DocumentModelDTO get(long id) {
        final DocumentModel documentModel = documentRepository.findOne(id);
        return ModelUtils.convertDocumentModel(documentModel);
    }

    @Override
    public String getRaw(long id) {
        final RawDocument rawDocument = rawDocumentRepository.findOne(id);
        if (rawDocument != null) {
            try {
                return new String(rawDocument.getContent(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                //
            }
        }
        return null;
    }

    @Override
    public DocumentModelDTO get(String name) {
        final DocumentModel documentModel = documentRepository.getByName(name);
        return ModelUtils.convertDocumentModel(documentModel);
    }

    @Override
    public List<DocumentModelDTO> getAll() {
        List<DocumentModelDTO> list = new LinkedList<>();
        for (DocumentModel documentModel : documentRepository.findAll()) {
            list.add(ModelUtils.convertDocumentModel(documentModel));
        }

        return list;
    }

    @Override
    public void delete(long id) {
        final DocumentModel documentModel = documentRepository.findOne(id);
        documentRepository.delete(documentModel);
    }

    @Override
    public void setProtocolService(ProtocolService protocolService) {
        this.protocolService = protocolService;
    }

    protected DocumentModelDTO createWithPageMarkup(String name, String contentWithPageMarkup, SourceType contentType) throws IOException {
        return this.create(name, contentWithPageMarkup, contentType, true);
    }

    protected DocumentModelDTO create(String name, String content, SourceType contentType, boolean pageLevel) throws IOException {
        Map<String, String> articles = new HashMap<>();
        articles.put(name, content);
        Map<String, Bag<TaggedWordWrapper>> bows;

        if (pageLevel) {
            bows = preprocess(articles);
        } else {
            bows = super.preprocess(articles);
        }

        final Bag<TaggedWordWrapper> model = postprocess(bows.get(name));

        DocumentModelDTO dto;

        if (documentRepository != null) {
            // TODO: optimization with hash
            RawDocument rawDocument = new RawDocument(name, contentType.name(), Utils.stringToBytes(content));

            DocumentModel documentModel = new DocumentModel(rawDocument, tokenizer.getOptions(), ModelUtils.convert(model));
            documentModel = documentRepository.save(documentModel);
            dto = new DocumentModelDTO(rawDocument.getTitle(), documentModel.getDocument().getId(), documentModel.getId(), model);

        } else {
            dto = new DocumentModelDTO(model);
        }

        return dto;
    }
}