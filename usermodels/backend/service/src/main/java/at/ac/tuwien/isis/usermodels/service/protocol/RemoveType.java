package at.ac.tuwien.isis.usermodels.service.protocol;

/**
 * Created by Alexej Strelzow on 10.04.2016.
 */
public enum RemoveType {

    FWL,

    BABELNET,

    FREQ_EQ_1,

    LENGTH_GT_1_LT_4,

    BLACKLIST,

    NAMED_ENTITY

}
