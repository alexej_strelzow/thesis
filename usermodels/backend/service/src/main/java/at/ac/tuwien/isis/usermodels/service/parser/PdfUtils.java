package at.ac.tuwien.isis.usermodels.service.parser;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Alexej Strelzow on 18.08.2016.
 */
public class PdfUtils {

    public static String getTextWithPageMarkup(Map<Integer, String> pages) {
        StringBuilder sb = new StringBuilder();
        for (Integer pageNumber : pages.keySet()) {
            sb.append("<div id=\"" + pageNumber + "\">").append("\r\n");
            sb.append(pages.get(pageNumber)).append("\r\n");
            sb.append("</div>").append("\r\n");
        }
        return sb.toString();
    }

    public static Map<Integer, String> getPages(String allPages) {
        Map<Integer, String> map = new LinkedHashMap<>();
        int startIndex  = 0;
        int endIndex;
        int pageCount = 1; // not 0!
        int EMERGENCY_LIMIT = 3000;

        while (true || pageCount > EMERGENCY_LIMIT) {
            startIndex = allPages.indexOf("<div id", startIndex);
            if (startIndex == -1) {
                break;
            }
            startIndex = startIndex + ("<div id=\"" + pageCount + "\">\r\n").length();
            endIndex = allPages.indexOf("</div>\r\n", startIndex);
            String content = allPages.substring(startIndex, endIndex);
            map.put(pageCount, content.trim());

            pageCount++;
            startIndex = endIndex;
        }
        return map;
    }

}
