
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 122
		Types: 119
		abbreviate#          1    
		abstraction#NN       1    
		adjacency#NN         1    
		algorithm#           1    
		alias#               1    
		alternation#NN       1    
		amenable#JJ          1    
		anew#RB              1    
		annotation#NN        1    
		annotation#NNS       1    
		applicability#NN     1    
		arithmetic#NN        1    
		attribute#NN         1    
		axiomatic#JJ         1    
		binary#JJ            1    
		blower#NN            1    
		boogie#              1    
		caliper#NNS          1    
		chore#NN             1    
		classify#            1    
		comb#                2    
		comb#NN              2    
		comb#VBP             1    
		computational#JJ     1    
		conceptually#RB      1    
		conventionally#RB    1    
		correctness#NN       1    
		cursor#NN            1    
		decoupling#NN        1    
		decrement#NN         1    
		delete#              1    
		deletion#NN          1    
		disallow#            1    
		disjunct#            1    
		disjunct#JJ          1    
		divisor#             1    
		edit#                1    
		edit#NN              1    
		elegance#NN          1    
		elicitation#NN       1    
		embedding#NN         1    
		execute#             1    
		execute#VBP          1    
		expressiveness#NN    1    
		formalism#NN         1    
		formalization#NN     1    
		galilean#JJ          1    
		generalize#          1    
		generalized#JJ       1    
		google#              1    
		graphical#JJ         1    
		increment#NN         1    
		inessential#JJ       1    
		inference#NN         1    
		ingenuity#NN         1    
		initialization#      1    
		initialization#NN    1    
		initialize#          1    
		insertion#NN         1    
		insightful#JJ        1    
		integer#             2    
		integer#NN           1    
		introductory#JJ      1    
		invariance#NN        1    
		invariant#JJ         1    
		inversion#NNS        1    
		knapsack#NN          1    
		leftmost#            1    
		leftmost#JJ          1    
		mint#NN              1    
		mutation#NN          1    
		node#NN              1    
		nutshell#NN          1    
		outbound#            1    
		outbound#JJ          1    
		overwrite#           1    
		partitioning#NN      1    
		perm#NN              1    
		positional#JJ        1    
		precondition#NN      1    
		predicate#           1    
		predicate#NN         1    
		programmer#NN        1    
		programmer#NNS       1    
		quantification#NN    1    
		quantifier#          1    
		quantifier#NN        1    
		recurrence#NN        1    
		reuse#VBP            1    
		rightmost#JJ         1    
		scalar#JJ            1    
		semantically#RB      1    
		semantics#           1    
		sequential#JJ        1    
		sequentially#RB      1    
		shorthand#NN         1    
		simplification#NN    1    
		specification#NN     1    
		specification#NNS    1    
		substitution#NN      1    
		subtraction#NN       1    
		suffice#             1    
		swap#                1    
		swap#NN              1    
		swap#NNS             1    
		syntactic#           1    
		syntax#NN            1    
		taxonomy#NN          1    
		temp#NN              1    
		termination#NN       1    
		unconstrained#JJ     1    
		uncoupling#NN        1    
		uncover#             1    
		unsorted#JJ          1    
		validation#NN        1    
		variant#NN           1    
		verifier#NN          1    
		weaken#              1    
		weakened#JJ          1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 4
		Types: 4
		conjunctive#JJ       1    
		deductive#JJ         1    
		inexpressible#JJ     1    
		necessitate#VBP      1    
	NOT_IN_USER_MODEL
		Tokens: 436
		Types: 436
		ARRAY#NN             1    
		Algorithms#NNP       1    
		Allow#VB             1    
		Arithmetic#NN        1    
		Array#NNP            1    
		Automatic#NNP        1    
		Beyond#IN            1    
		Binary#JJ            1    
		Body#NN              1    
		Body#NNP             1    
		Boogie#NNP           1    
		Boolean#JJ           1    
		Bubble#NN            1    
		Bubble#NNP           1    
		Challenge#NNP        1    
		Classification#NN    1    
		Comb#NN              1    
		Comb#NNP             1    
		Common#NNP           1    
		Computational#JJ     1    
		Contents#NNP         1    
		Counter#NNP          1    
		Disallow#VB          1    
		Divisor#NNP          1    
		Euclid#NN            1    
		Every#DT             1    
		Exit#NN              1    
		Exponentiation#NN    1    
		False#NNP            1    
		Finding#NNP          1    
		Formula#NNP          1    
		Galilean#JJ          1    
		Generalize#NN        1    
		Google#NNP           1    
		Grand#NNP            1    
		Greatest#NNP         1    
		INTEGER#NN           1    
		INTEGER#NNP          1    
		Initialization#NNP   1    
		Initiation#NN        1    
		Insertion#NN         1    
		Integer#NNP          1    
		Internet#NNP         1    
		Introduction#NN      1    
		Invariant#JJ         1    
		LIST#NN              1    
		Lessons#NNS          1    
		Levenshtein#NN       1    
		List#VBP             1    
		Long#JJ              1    
		Loop#NNP             1    
		NODE#NN              1    
		Notice#NNP           1    
		Other#JJ             1    
		Program#NN           1    
		Programming#NN       1    
		Quick#NNP            1    
		Quick#RB             1    
		REAL#NN              1    
		REAL#NNP             1    
		Related#JJ           1    
		Result#NN            1    
		Reversal#NN          1    
		Selection#NN         1    
		Software#NNP         1    
		Static#JJ            1    
		TREE#NN              1    
		That#WDT             1    
		True#JJ              1    
		Unbounded#JJ         1    
		Variable#JJ          1    
		Verifying#NNP        1    
		Void#NN              1    
		Void#NNP             1    
		Void#VBD             1    
		abbreviate#VB        1    
		abstract#JJ          1    
		abstraction#NN       1    
		accept#VBP           1    
		accessible#JJ        1    
		accommodate#VB       1    
		acyclic#JJ           1    
		adjacency#NN         1    
		aging#NN             1    
		alias#FW             1    
		alternation#NN       1    
		amenability#NN       1    
		amenable#JJ          1    
		amid#IN              1    
		anew#RB              1    
		annotation#NN        1    
		annotations#NNS      1    
		anything#NN          1    
		anyway#RB            1    
		appearance#NN        1    
		applicability#NN     1    
		areas#NNS            1    
		assertion#NN         1    
		assertions#NNS       1    
		assessment#NN        1    
		associate#JJ         1    
		atomic#JJ            1    
		attempt#NN           1    
		attraction#NN        1    
		attribute#NN         1    
		automated#JJ         1    
		automatic#JJ         1    
		axiomatic#JJ         1    
		balanced#JJ          1    
		basen#NN             1    
		basics#NNS           1    
		better#RB            1    
		blower#NN            1    
		body#NN              1    
		boundaries#NNS       1    
		branch#NN            1    
		bubble#NN            1    
		calipers#NNS         1    
		capture#NN           1    
		capture#VBP          1    
		carry#VB             1    
		catalog#NN           1    
		child#NN             1    
		children#NNS         1    
		chore#NN             1    
		clarity#NN           1    
		classic#JJ           1    
		classify#VB          1    
		clause#NN            1    
		clauses#NNS          1    
		closing#NN           1    
		comb#NN              1    
		comb#VB              1    
		comb#VBP             1    
		combine#VBP          1    
		comment#NN           1    
		commutativity#NN     1    
		compatible#JJ        1    
		conceptual#JJ        1    
		conceptually#RB      1    
		concern#NN           1    
		confidence#NN        1    
		conjunct#NN          1    
		connection#NN        1    
		consistency#NN       1    
		contracts#NNS        1    
		contrast#VB          1    
		convention#NN        1    
		conventionally#RB    1    
		correctness#NN       1    
		count#NN             1    
		counter#JJ           1    
		counter#NN           1    
		counter#RB           1    
		counter#VBP          1    
		course#NN            1    
		creation#NN          1    
		cursor#NN            1    
		deal#VB              1    
		declaration#NN       1    
		decoupling#NN        1    
		decrement#NN         1    
		delete#VB            1    
		deletion#NN          1    
		delicate#JJ          1    
		determine#VBN        1    
		diff#NN              1    
		disjunct#JJ          1    
		disjunct#NN          1    
		disjunctive#JJ       1    
		diverse#JJ           1    
		ease#VB              1    
		edit#NN              1    
		edit#VB              1    
		elegance#NN          1    
		elicitation#NN       1    
		else#RB              1    
		embedding#NN         1    
		empty#VB             1    
		ends#NNS             1    
		engine#NN            1    
		engineering#NN       1    
		ever#RB              1    
		evolution#NN         1    
		execute#VB           1    
		execute#VBP          1    
		execution#NN         1    
		executions#NNS       1    
		exhibit#VBP          1    
		existence#NN         1    
		exit#NN              1    
		exit#VB              1    
		exit#VBP             1    
		explore#VB           1    
		expressible#JJ       1    
		expressiveness#NN    1    
		false#JJ             1    
		feature#VBP          1    
		fields#NNS           1    
		files#NNS            1    
		findings#NNS         1    
		flag#NN              1    
		flavor#NN            1    
		flexibility#NN       1    
		formal#JJ            1    
		formalism#NN         1    
		formalization#NN     1    
		fractions#NNS        1    
		fresh#JJ             1    
		future#JJ            1    
		generalized#JJ       1    
		generic#JJ           1    
		ghost#NN             1    
		give#VBP             1    
		graphical#JJ         1    
		greatest#JJS         1    
		guess#VB             1    
		handle#VB            1    
		highlight#NN         1    
		hope#NN              1    
		human#JJ             1    
		humans#NNS           1    
		hypotheses#NNS       1    
		hypothesis#NN        1    
		identification#NN    1    
		illustrate#VB        1    
		image#NN             1    
		increasingly#RB      1    
		increment#NN         1    
		inductive#JJ         1    
		industry#NN          1    
		inessential#JJ       1    
		inference#NN         1    
		informal#JJ          1    
		ingenuity#NN         1    
		initialization#NN    1    
		initialize#VB        1    
		initiation#NN        1    
		insightful#JJ        1    
		insights#NNS         1    
		instruction#NN       1    
		instructions#NNS     1    
		interpretation#NN    1    
		introduction#NN      1    
		introductory#JJ      1    
		invariance#NN        1    
		inversions#NNS       1    
		inverted#JJ          1    
		involve#VB           1    
		involved#JJ          1    
		item#NN              1    
		iterate#VB           1    
		iterate#VBP          1    
		jourdain#NN          1    
		knapsack#NN          1    
		language#NN          1    
		languages#NNS        1    
		latest#JJS           1    
		leaves#NNS           1    
		left#RB              1    
		leftmost#JJ          1    
		leftmost#JJS         1    
		lend#VBP             1    
		lessons#NNS          1    
		library#NN           1    
		life#NN              1    
		light#NN             1    
		link#VBP             1    
		list#NN              1    
		list#VBP             1    
		location#NN          1    
		logic#NN             1    
		logical#JJ           1    
		look#VB              1    
		match#VBP            1    
		mathematics#NNS      1    
		maximal#JJ           1    
		mechanical#JJ        1    
		mechanism#NN         1    
		mint#NN              1    
		model#VBP            1    
		movement#NN          1    
		moves#NNS            1    
		mutation#NN          1    
		name#VB              1    
		nice#JJ              1    
		normally#RB          1    
		nutshell#NN          1    
		object#NN            1    
		object#VBP           1    
		obviate#VBP          1    
		ourselves#PRP        1    
		outbound#JJ          1    
		outbound#NN          1    
		over#RB              1    
		overwrite#NN         1    
		partitioning#NN      1    
		people#NNS           1    
		perm#NN              1    
		person#NN            1    
		physics#NNS          1    
		pick#NN              1    
		pick#VB              1    
		pictures#NNS         1    
		play#NN              1    
		plays#NNS            1    
		position#VB          1    
		position#VBP         1    
		positional#JJ        1    
		possible#RB          1    
		postconditions#NNS   1    
		precise#JJ           1    
		precision#NN         1    
		precondition#NN      1    
		predicate#NN         1    
		predicate#VB         1    
		preserved#JJ         1    
		proceeds#NNS         1    
		programmer#NN        1    
		programmers#NNS      1    
		progress#NN          1    
		prose#NN             1    
		prover#NN            1    
		provers#NNS          1    
		quantification#NN    1    
		quantifier#NN        1    
		quick#JJ             1    
		read#VB              1    
		recording#NN         1    
		records#NNS          1    
		recurrence#NN        1    
		remainder#NN         1    
		report#VBP           1    
		representative#NN    1    
		responsibility#NN    1    
		reuse#VBP            1    
		reversal#NN          1    
		reversed#JJ          1    
		right#NN             1    
		right#RB             1    
		rightmost#JJ         1    
		rigorous#JJ          1    
		roles#NNS            1    
		rooted#JJ            1    
		routine#JJ           1    
		rules#NNS            1    
		safety#NN            1    
		satisfaction#NN      1    
		scalar#JJ            1    
		scan#VB              1    
		scans#NNS            1    
		science#NN           1    
		scope#NN             1    
		score#NN             1    
		score#VBP            1    
		scores#NNS           1    
		second#RB            1    
		seek#VBP             1    
		seem#VBP             1    
		semantically#RB      1    
		semantics#NNS        1    
		sequential#JJ        1    
		sequentially#RB      1    
		settle#VB            1    
		shorthand#NN         1    
		similarity#NN        1    
		simplification#NN    1    
		sink#NN              1    
		slice#NN             1    
		slice#VBP            1    
		society#NN           1    
		soon#RB              1    
		sort#NN              1    
		sorting#NN           1    
		sound#NN             1    
		speaking#NN          1    
		specification#NN     1    
		specifications#NNS   1    
		state#VBP            1    
		stays#NNS            1    
		store#VB             1    
		stores#NNS           1    
		string#NN            1    
		structural#JJ        1    
		style#NN             1    
		styles#NNS           1    
		subsequence#NN       1    
		substitute#JJ        1    
		substitute#NN        1    
		substitution#NN      1    
		substraction#NN      1    
		subtraction#NN       1    
		subtree#NN           1    
		succinctness#NN      1    
		suffice#VB           1    
		swap#NN              1    
		swap#VB              1    
		swaps#NNS            1    
		syntactic#NN         1    
		syntax#NN            1    
		systematic#JJ        1    
		taxonomy#NN          1    
		technology#NN        1    
		teeth#NNS            1    
		temp#NN              1    
		terminate#VBP        1    
		termination#NN       1    
		test#VB              1    
		text#NN              1    
		thanks#NNS           1    
		themselves#PRP       1    
		trajectory#NN        1    
		tree#NN              1    
		unconstrained#JJ     1    
		uncoupling#NN        1    
		uncover#VB           1    
		undefined#JJ         1    
		understanding#NN     1    
		unsorted#JJ          1    
		useless#JJ           1    
		validation#NN        1    
		value#VBP            1    
		variability#NN       1    
		variant#NN           1    
		verifier#NN          1    
		view#VBP             1    
		violation#NN         1    
		visit#NN             1    
		walk#NN              1    
		weakened#JJ          1    
		weakening#NN         1    
		wish#NN              1    
		wupper#NN            1    
		year#NN              1    
		yielding#NN          1    
		zero#VB              1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 15
		Types: 15
		Exponentiation#NN    1    
		Levenshtein#NN       1    
		amenability#NN       1    
		commutativity#NN     1    
		conjunct#NN          1    
		diff#NN              1    
		iterate#VB           1    
		iterate#VBP          1    
		postconditions#NNS   1    
		prover#NN            1    
		provers#NNS          1    
		subsequence#NN       1    
		substraction#NN      1    
		subtree#NN           1    
		succinctness#NN      1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 5285/1242
Size of diff: 141

NOT in Collection (FWL):   16
In Collection (FWL):       29
In Collection (FWL + POS): 96

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Exponentiation        |  NN       |  -1       |  false   |  false         |  false  
Levenshtein           |  NN       |  -1       |  false   |  false         |  false  
amenability           |  NN       |  -1       |  false   |  false         |  false  
commutativity         |  NN       |  -1       |  false   |  false         |  false  
conjunct              |  NN       |  -1       |  false   |  false         |  false  
diff                  |  NN       |  -1       |  false   |  false         |  false  
iterate               |  VB       |  -1       |  false   |  false         |  false  
iterate               |  VBP      |  -1       |  false   |  false         |  false  
postconditions        |  NNS      |  -1       |  false   |  false         |  false  
prover                |  NN       |  -1       |  false   |  false         |  false  
provers               |  NNS      |  -1       |  false   |  false         |  false  
quantifiers           |  NNS      |  -1       |  false   |  false         |  false  
subsequence           |  NN       |  -1       |  false   |  false         |  false  
substraction          |  NN       |  -1       |  false   |  false         |  false  
subtree               |  NN       |  -1       |  false   |  false         |  false  
succinctness          |  NN       |  -1       |  false   |  false         |  false  

Algorithms            |  NNP      |  15348    |  false   |  false         |  true   
Boogie                |  NNP      |  28534    |  false   |  false         |  true   
Comb                  |  NNP      |  16382    |  false   |  false         |  true   
Disallow              |  VB       |  55164    |  false   |  false         |  true   
Divisor               |  NNP      |  99481    |  false   |  false         |  true   
Generalize            |  NN       |  20420    |  false   |  false         |  true   
Google                |  NNP      |  20790    |  false   |  false         |  true   
INTEGER               |  NNP      |  35492    |  false   |  false         |  true   
Initialization        |  NNP      |  70243    |  false   |  false         |  true   
Integer               |  NNP      |  35492    |  false   |  false         |  true   
abbreviate            |  VB       |  73080    |  false   |  false         |  true   
alias                 |  FW       |  24276    |  false   |  false         |  true   
classify              |  VB       |  16032    |  false   |  false         |  true   
comb                  |  VB       |  16382    |  false   |  false         |  true   
delete                |  VB       |  20116    |  false   |  false         |  true   
disjunct              |  NN       |  80139    |  false   |  false         |  true   
edit                  |  VB       |  15322    |  false   |  false         |  true   
execute               |  VB       |  11038    |  false   |  false         |  true   
initialize            |  VB       |  90290    |  false   |  false         |  true   
leftmost              |  JJS      |  77767    |  false   |  false         |  true   
outbound              |  NN       |  39649    |  false   |  false         |  true   
overwrite             |  NN       |  72763    |  false   |  false         |  true   
predicate             |  VB       |  42560    |  false   |  false         |  true   
semantics             |  NNS      |  23863    |  false   |  false         |  true   
suffice               |  VB       |  14350    |  false   |  false         |  true   
swap                  |  VB       |  14669    |  false   |  false         |  true   
syntactic             |  NN       |  28137    |  false   |  false         |  true   
uncover               |  VB       |  12220    |  false   |  false         |  true   
weakening             |  NN       |  20873    |  false   |  false         |  true   

Arithmetic            |  NN       |  15107    |  false   |  true          |  true   
Binary                |  JJ       |  14370    |  false   |  true          |  true   
Comb                  |  NN       |  16382    |  false   |  true          |  true   
Computational         |  JJ       |  16589    |  false   |  true          |  true   
Galilean              |  JJ       |  47996    |  false   |  true          |  true   
INTEGER               |  NN       |  35492    |  false   |  true          |  true   
Insertion             |  NN       |  21119    |  false   |  true          |  true   
Invariant             |  JJ       |  36210    |  false   |  true          |  true   
NODE                  |  NN       |  16149    |  false   |  true          |  true   
abstraction           |  NN       |  13301    |  false   |  true          |  true   
adjacency             |  NN       |  86919    |  false   |  true          |  true   
alternation           |  NN       |  40697    |  false   |  true          |  true   
amenable              |  JJ       |  22128    |  false   |  true          |  true   
anew                  |  RB       |  14297    |  false   |  true          |  true   
annotation            |  NN       |  49991    |  false   |  true          |  true   
annotations           |  NNS      |  37954    |  false   |  true          |  true   
applicability         |  NN       |  22611    |  false   |  true          |  true   
attribute             |  NN       |  17241    |  false   |  true          |  true   
axiomatic             |  JJ       |  44722    |  false   |  true          |  true   
blower                |  NN       |  28416    |  false   |  true          |  true   
calipers              |  NNS      |  41779    |  false   |  true          |  true   
chore                 |  NN       |  16524    |  false   |  true          |  true   
comb                  |  NN       |  16382    |  false   |  true          |  true   
comb                  |  VBP      |  22194    |  false   |  true          |  true   
conceptually          |  RB       |  20265    |  false   |  true          |  true   
conventionally        |  RB       |  21850    |  false   |  true          |  true   
correctness           |  NN       |  15629    |  false   |  true          |  true   
cursor                |  NN       |  29658    |  false   |  true          |  true   
decoupling            |  NN       |  62066    |  false   |  true          |  true   
decrement             |  NN       |  58557    |  false   |  true          |  true   
deletion              |  NN       |  38563    |  false   |  true          |  true   
disjunct              |  JJ       |  80139    |  false   |  true          |  true   
disjunctive           |  JJ       |  52185    |  false   |  true          |  true   
edit                  |  NN       |  41317    |  false   |  true          |  true   
elegance              |  NN       |  12524    |  false   |  true          |  true   
elicitation           |  NN       |  50190    |  false   |  true          |  true   
embedding             |  NN       |  37457    |  false   |  true          |  true   
execute               |  VBP      |  11038    |  false   |  true          |  true   
expressible           |  JJ       |  89061    |  false   |  true          |  true   
expressiveness        |  NN       |  38361    |  false   |  true          |  true   
formalism             |  NN       |  34867    |  false   |  true          |  true   
formalization         |  NN       |  59359    |  false   |  true          |  true   
generalized           |  JJ       |  14429    |  false   |  true          |  true   
graphical             |  JJ       |  23598    |  false   |  true          |  true   
increment             |  NN       |  34591    |  false   |  true          |  true   
inductive             |  JJ       |  28829    |  false   |  true          |  true   
inessential           |  JJ       |  70540    |  false   |  true          |  true   
inference             |  NN       |  17088    |  false   |  true          |  true   
ingenuity             |  NN       |  16106    |  false   |  true          |  true   
initialization        |  NN       |  70243    |  false   |  true          |  true   
insightful            |  JJ       |  17301    |  false   |  true          |  true   
introductory          |  JJ       |  11515    |  false   |  true          |  true   
invariance            |  NN       |  43751    |  false   |  true          |  true   
inversions            |  NNS      |  48140    |  false   |  true          |  true   
knapsack              |  NN       |  27189    |  false   |  true          |  true   
leftmost              |  JJ       |  77767    |  false   |  true          |  true   
mint                  |  NN       |  12053    |  false   |  true          |  true   
mutation              |  NN       |  16950    |  false   |  true          |  true   
nutshell              |  NN       |  23776    |  false   |  true          |  true   
obviate               |  VBP      |  43250    |  false   |  true          |  true   
outbound              |  JJ       |  39649    |  false   |  true          |  true   
partitioning          |  NN       |  33151    |  false   |  true          |  true   
perm                  |  NN       |  45893    |  false   |  true          |  true   
positional            |  JJ       |  35614    |  false   |  true          |  true   
precondition          |  NN       |  25984    |  false   |  true          |  true   
predicate             |  NN       |  42560    |  false   |  true          |  true   
programmer            |  NN       |  18161    |  false   |  true          |  true   
programmers           |  NNS      |  16232    |  false   |  true          |  true   
quantification        |  NN       |  37935    |  false   |  true          |  true   
quantifier            |  NN       |  81197    |  false   |  true          |  true   
recurrence            |  NN       |  14843    |  false   |  true          |  true   
reuse                 |  VBP      |  25030    |  false   |  true          |  true   
rightmost             |  JJ       |  71225    |  false   |  true          |  true   
scalar                |  JJ       |  51211    |  false   |  true          |  true   
semantically          |  RB       |  45559    |  false   |  true          |  true   
sequential            |  JJ       |  15077    |  false   |  true          |  true   
sequentially          |  RB       |  29428    |  false   |  true          |  true   
shorthand             |  NN       |  21231    |  false   |  true          |  true   
simplification        |  NN       |  28564    |  false   |  true          |  true   
specification         |  NN       |  20589    |  false   |  true          |  true   
specifications        |  NNS      |  12374    |  false   |  true          |  true   
substitution          |  NN       |  16352    |  false   |  true          |  true   
subtraction           |  NN       |  34113    |  false   |  true          |  true   
swap                  |  NN       |  24146    |  false   |  true          |  true   
swaps                 |  NNS      |  28134    |  false   |  true          |  true   
syntax                |  NN       |  20014    |  false   |  true          |  true   
taxonomy              |  NN       |  18920    |  false   |  true          |  true   
temp                  |  NN       |  23577    |  false   |  true          |  true   
termination           |  NN       |  14122    |  false   |  true          |  true   
unconstrained         |  JJ       |  39114    |  false   |  true          |  true   
uncoupling            |  NN       |  96015    |  false   |  true          |  true   
unsorted              |  JJ       |  77741    |  false   |  true          |  true   
validation            |  NN       |  13282    |  false   |  true          |  true   
variant               |  NN       |  15310    |  false   |  true          |  true   
verifier              |  NN       |  90691    |  false   |  true          |  true   
weakened              |  JJ       |  23052    |  false   |  true          |  true   


Unique words (size: 126, lower case) without POS:
abbreviate
abstraction
adjacency
algorithms
alias
alternation
amenability
amenable
anew
annotation
annotations
applicability
arithmetic
attribute
axiomatic
binary
blower
boogie
calipers
chore
classify
comb
commutativity
computational
conceptually
conjunct
conventionally
correctness
cursor
decoupling
decrement
delete
deletion
diff
disallow
disjunct
disjunctive
divisor
edit
elegance
elicitation
embedding
execute
exponentiation
expressible
expressiveness
formalism
formalization
galilean
generalize
generalized
google
graphical
increment
inductive
inessential
inference
ingenuity
initialization
initialize
insertion
insightful
integer
introductory
invariance
invariant
inversions
iterate
knapsack
leftmost
levenshtein
mint
mutation
node
nutshell
obviate
outbound
overwrite
partitioning
perm
positional
postconditions
precondition
predicate
programmer
programmers
prover
provers
quantification
quantifier
quantifiers
recurrence
reuse
rightmost
scalar
semantically
semantics
sequential
sequentially
shorthand
simplification
specification
specifications
subsequence
substitution
substraction
subtraction
subtree
succinctness
suffice
swap
swaps
syntactic
syntax
taxonomy
temp
termination
unconstrained
uncoupling
uncover
unsorted
validation
variant
verifier
weakened
weakening

