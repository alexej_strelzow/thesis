
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 60
		Types: 55
		abbreviation#        1    
		adenoma#NN           1    
		adjuvant#JJ          1    
		assay#NNS            1    
		attribution#         1    
		biomarker#NN         1    
		carcinoma#NN         1    
		cobra#               1    
		colorectal#JJ        2    
		commons#             2    
		confounder#NNS       1    
		confounding#JJ       1    
		covariate#NN         1    
		cutoff#NN            1    
		duplicate#           1    
		duplicate#NN         1    
		epidemiology#        1    
		epigenetics#         2    
		formalin#NN          1    
		inception#NN         1    
		inhibit#             1    
		keyword#             1    
		locus#NNS            1    
		lymphocytic#JJ       1    
		lymphoid#JJ          1    
		malignancy#NN        1    
		melting#NN           1    
		methylation#NN       2    
		microsatellite#NN    1    
		mint#                1    
		mint#JJ              1    
		mutation#NN          1    
		mutation#NNS         1    
		neoplasm#NN          1    
		paraffin#NN          1    
		pathological#JJ      1    
		phenotype#NN         1    
		postoperative#JJ     1    
		prognosis#           1    
		prognosis#NN         1    
		prognostic#JJ        1    
		promoter#NN          1    
		rectal#              1    
		rectal#JJ            2    
		rectum#NN            1    
		recurrence#NN        1    
		silencing#NN         1    
		simultaneous#JJ      1    
		subgroup#            1    
		subgroup#NNS         1    
		subtype#NNS          1    
		suppressor#NN        1    
		transcription#NN     1    
		unrestricted#JJ      1    
		waiver#NN            1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 3
		Types: 3
		avascular#JJ         1    
		continual#JJ         1    
		distal#JJ            1    
	NOT_IN_USER_MODEL
		Tokens: 198
		Types: 198
		Abbreviations#NNPS   1    
		Accepted#NNP         1    
		Aging#NNP            1    
		Attribution#NNP      1    
		Authors#NNS          1    
		Background#NNP       1    
		COBRA#NNP            1    
		Cancer#NNP           1    
		Chemotherapy#NNP     1    
		China#NNP            1    
		Colon#NN             1    
		Colorectal#JJ        1    
		Common#JJ            1    
		Commons#NNP          1    
		Commons#NNPS         1    
		Consortium#NNP       1    
		Contradictory#JJ     1    
		Correspondence#NN    1    
		Council#NNP          1    
		Creative#JJ          1    
		Creative#NNP         1    
		Crohn#NN             1    
		Cross#NNP            1    
		Data#NNS             1    
		Dedication#NN        1    
		Disease#NNP          1    
		Distal#JJ            1    
		Duplicate#NN         1    
		Eligibility#NN       1    
		English#JJ           1    
		Epidemiology#NNP     1    
		Epigenetics#NNP      1    
		Epigenetics#NNS      1    
		GATA#NN              1    
		Gene#NN              1    
		German#NNP           1    
		HLTF#NN              1    
		KIRREL#NN            1    
		Keywords#NNPS        1    
		Knowledge#NN         1    
		License#NNP          1    
		Literature#NN        1    
		MEDLINE#NN           1    
		MINT#JJ              1    
		MINT#NNP             1    
		Methods#NNS          1    
		Methylation#NN       1    
		Open#NNP             1    
		Overall#NNP          1    
		Page#NNP             1    
		Potentially#RB       1    
		Prognosis#NNP        1    
		Proximal#JJ          1    
		Public#NNP           1    
		REVIEW#NNP           1    
		Rectal#JJ            1    
		Rectal#NNP           1    
		SOCS#NN              1    
		Sample#NNP           1    
		Scholarship#NNP      1    
		Search#VBP           1    
		Stage#NN             1    
		Stage#NNP            1    
		Study#NN             1    
		Study#NNP            1    
		Subgroup#NNP         1    
		Survival#NN          1    
		TIMP#NN              1    
		TSLC#NN              1    
		Ward#NNP             1    
		aData#NN             1    
		abstracts#NNS        1    
		accordance#NN        1    
		address#VB           1    
		adenoma#NN           1    
		adjustment#NN        1    
		adjuvant#JJ          1    
		altered#JJ           1    
		assays#NNS           1    
		assessment#NN        1    
		bOther#NN            1    
		beings#NNS           1    
		binding#NN           1    
		biomarker#NN         1    
		bisulfite#NN         1    
		cancer#NN            1    
		carcinoma#NN         1    
		categories#NNS       1    
		category#NN          1    
		cause#VBP            1    
		chemotherapy#NN      1    
		clinical#JJ          1    
		colleagues#NNS       1    
		colon#NN             1    
		colorectal#JJ        1    
		confounders#NNS      1    
		confounding#JJ       1    
		contribute#VBP       1    
		country#NN           1    
		covariate#NN         1    
		credit#NN            1    
		criteria#NNS         1    
		cutoff#NN            1    
		dOther#NN            1    
		databases#NNS        1    
		declare#VBP          1    
		diagnosis#NN         1    
		duplicate#VB         1    
		eligible#JJ          1    
		exclusion#NN         1    
		extents#NNS          1    
		fOther#NN            1    
		females#NNS          1    
		file#NN              1    
		files#NNS            1    
		findings#NNS         1    
		formalin#NN          1    
		frozen#JJ            1    
		gender#NN            1    
		grade#NN             1    
		gross#JJ             1    
		hOther#NN            1    
		http#NN              1    
		hypermethylation#NN  1    
		inception#NN         1    
		included#JJ          1    
		inconsistent#JJ      1    
		inhibit#VB           1    
		invasion#NN          1    
		island#NN            1    
		islands#NNS          1    
		late#JJ              1    
		license#NN           1    
		loci#NNS             1    
		lymphocytic#JJ       1    
		lymphoid#JJ          1    
		males#NNS            1    
		malignancy#NN        1    
		marker#NN            1    
		markers#NNS          1    
		melting#NN           1    
		meta#NN              1    
		methylation#NN       1    
		microsatellite#NN    1    
		mortality#NN         1    
		mutation#NN          1    
		mutations#NNS        1    
		neoplasm#NN          1    
		onset#NN             1    
		originally#RB        1    
		paraffin#NN          1    
		pathological#JJ      1    
		patient#NN           1    
		patients#NNS         1    
		phenotype#NN         1    
		populations#NNS      1    
		postoperative#JJ     1    
		potentially#RB       1    
		prevalence#NN        1    
		primary#JJ           1    
		prognosis#NN         1    
		prognostic#JJ        1    
		progression#NN       1    
		promoter#NN          1    
		races#NNS            1    
		radiation#NN         1    
		rectal#JJ            1    
		rectum#NN            1    
		recurrence#NN        1    
		removal#NN           1    
		reproducible#JJ      1    
		reproduction#NN      1    
		silence#NN           1    
		silencing#NN         1    
		simultaneous#JJ      1    
		site#NN              1    
		sponsor#NN           1    
		sporadic#JJ          1    
		subgroups#NNS        1    
		subtypes#NNS         1    
		suppressor#NN        1    
		surgery#NN           1    
		survival#NN          1    
		therapies#NNS        1    
		therapy#NN           1    
		tissue#NN            1    
		title#NN             1    
		titles#NNS           1    
		transcription#NN     1    
		tumor#NN             1    
		tumors#NNS           1    
		unadjusted#JJ        1    
		understand#VB        1    
		unrestricted#JJ      1    
		vascular#JJ          1    
		waiver#NN            1    
		ward#NN              1    
		women#NNS            1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 7
		Types: 7
		MEDLINE#NN           1    
		SOCS#NN              1    
		TIMP#NN              1    
		bisulfite#NN         1    
		fOther#NN            1    
		http#NN              1    
		hypermethylation#NN  1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 75157/5049
Size of tokens/types in Md: 1895/562
Size of diff: 70

NOT in Collection (FWL):   7
In Collection (FWL):       15
In Collection (FWL + POS): 48

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
MEDLINE               |  NN       |  -1       |  false   |  false         |  false  
SOCS                  |  NN       |  -1       |  false   |  false         |  false  
TIMP                  |  NN       |  -1       |  false   |  false         |  false  
bisulfite             |  NN       |  -1       |  false   |  false         |  false  
fOther                |  NN       |  -1       |  false   |  false         |  false  
http                  |  NN       |  -1       |  false   |  false         |  false  
hypermethylation      |  NN       |  -1       |  false   |  false         |  false  

Abbreviations         |  NNPS     |  39805    |  false   |  false         |  true   
Attribution           |  NNP      |  18093    |  false   |  false         |  true   
COBRA                 |  NNP      |  17960    |  false   |  false         |  true   
Commons               |  NNP      |  18807    |  false   |  false         |  true   
Commons               |  NNPS     |  18807    |  false   |  false         |  true   
Epidemiology          |  NNP      |  17370    |  false   |  false         |  true   
Epigenetics           |  NNP      |  95007    |  false   |  false         |  true   
Epigenetics           |  NNS      |  95007    |  false   |  false         |  true   
Keywords              |  NNPS     |  17233    |  false   |  false         |  true   
MINT                  |  NNP      |  12053    |  false   |  false         |  true   
Prognosis             |  NNP      |  15745    |  false   |  false         |  true   
Rectal                |  NNP      |  30846    |  false   |  false         |  true   
Subgroup              |  NNP      |  20833    |  false   |  false         |  true   
duplicate             |  VB       |  18782    |  false   |  false         |  true   
inhibit               |  VB       |  15598    |  false   |  false         |  true   

Colorectal            |  JJ       |  32240    |  false   |  true          |  true   
Duplicate             |  NN       |  39251    |  false   |  true          |  true   
MINT                  |  JJ       |  13876    |  false   |  true          |  true   
Methylation           |  NN       |  45829    |  false   |  true          |  true   
Proximal              |  JJ       |  24712    |  false   |  true          |  true   
Rectal                |  JJ       |  30846    |  false   |  true          |  true   
adenoma               |  NN       |  50883    |  false   |  true          |  true   
adjuvant              |  JJ       |  43649    |  false   |  true          |  true   
assays                |  NNS      |  39753    |  false   |  true          |  true   
biomarker             |  NN       |  68805    |  false   |  true          |  true   
carcinoma             |  NN       |  19001    |  false   |  true          |  true   
colorectal            |  JJ       |  32240    |  false   |  true          |  true   
confounders           |  NNS      |  53882    |  false   |  true          |  true   
confounding           |  JJ       |  25492    |  false   |  true          |  true   
covariate             |  NN       |  28738    |  false   |  true          |  true   
cutoff                |  NN       |  16052    |  false   |  true          |  true   
formalin              |  NN       |  71149    |  false   |  true          |  true   
inception             |  NN       |  13961    |  false   |  true          |  true   
loci                  |  NNS      |  36919    |  false   |  true          |  true   
lymphocytic           |  JJ       |  60578    |  false   |  true          |  true   
lymphoid              |  JJ       |  59145    |  false   |  true          |  true   
malignancy            |  NN       |  31719    |  false   |  true          |  true   
melting               |  NN       |  12645    |  false   |  true          |  true   
methylation           |  NN       |  45829    |  false   |  true          |  true   
microsatellite        |  NN       |  70775    |  false   |  true          |  true   
mutation              |  NN       |  16950    |  false   |  true          |  true   
mutations             |  NNS      |  17271    |  false   |  true          |  true   
neoplasm              |  NN       |  45335    |  false   |  true          |  true   
paraffin              |  NN       |  39577    |  false   |  true          |  true   
pathological          |  JJ       |  18388    |  false   |  true          |  true   
phenotype             |  NN       |  39297    |  false   |  true          |  true   
postoperative         |  JJ       |  15509    |  false   |  true          |  true   
prognosis             |  NN       |  15745    |  false   |  true          |  true   
prognostic            |  JJ       |  44788    |  false   |  true          |  true   
promoter              |  NN       |  17073    |  false   |  true          |  true   
rectal                |  JJ       |  30846    |  false   |  true          |  true   
rectum                |  NN       |  36657    |  false   |  true          |  true   
recurrence            |  NN       |  14843    |  false   |  true          |  true   
silencing             |  NN       |  39640    |  false   |  true          |  true   
simultaneous          |  JJ       |  11403    |  false   |  true          |  true   
sporadic              |  JJ       |  15795    |  false   |  true          |  true   
subgroups             |  NNS      |  17283    |  false   |  true          |  true   
subtypes              |  NNS      |  34447    |  false   |  true          |  true   
suppressor            |  NN       |  50846    |  false   |  true          |  true   
transcription         |  NN       |  19849    |  false   |  true          |  true   
unrestricted          |  JJ       |  18851    |  false   |  true          |  true   
vascular              |  JJ       |  15468    |  false   |  true          |  true   
waiver                |  NN       |  16378    |  false   |  true          |  true   


Unique words (size: 61, lower case) without POS:
abbreviations
adenoma
adjuvant
assays
attribution
biomarker
bisulfite
carcinoma
cobra
colorectal
commons
confounders
confounding
covariate
cutoff
duplicate
epidemiology
epigenetics
formalin
fother
http
hypermethylation
inception
inhibit
keywords
loci
lymphocytic
lymphoid
malignancy
medline
melting
methylation
microsatellite
mint
mutation
mutations
neoplasm
paraffin
pathological
phenotype
postoperative
prognosis
prognostic
promoter
proximal
rectal
rectum
recurrence
silencing
simultaneous
socs
sporadic
subgroup
subgroups
subtypes
suppressor
timp
transcription
unrestricted
vascular
waiver

