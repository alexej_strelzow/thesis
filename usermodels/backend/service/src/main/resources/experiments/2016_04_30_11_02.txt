
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 11
		Types: 11
		comp#                1    
		conjugate#NN         1    
		converse#            1    
		converse#NN          1    
		decoding#            1    
		encode#              1    
		gaussian#            1    
		inaccuracy#NN        1    
		instantaneous#JJ     1    
		recount#NN           1    
		utilize#VBP          1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	NOT_IN_USER_MODEL
		Tokens: 37
		Types: 37
		Adaptive#JJ          1    
		CHANNEL#NNP          1    
		Capacity#NN          1    
		Comp#NNP             1    
		Decoding#NNP         1    
		EVALUATION#NN        1    
		Elog#NN              1    
		FEEDBACK#NN          1    
		Feedback#NNP         1    
		Laboratory#NNP       1    
		Markovian#JJ         1    
		Periodic#JJ          1    
		Varying#JJ           1    
		ViMi#NN              1    
		WITH#IN              1    
		cONCLUSION#NN        1    
		codebooks#NNS        1    
		compound#NN          1    
		conjugate#NN         1    
		converse#NN          1    
		converse#VB          1    
		dash#VBP             1    
		encoding#NN          1    
		ergodic#NN           1    
		gAUSSIAN#NNP         1    
		imperfect#JJ         1    
		inaccuracy#NN        1    
		instant#JJ           1    
		instantaneous#JJ     1    
		late#JJ              1    
		liminf#NN            1    
		pERFORMANCE#NN       1    
		periodic#JJ          1    
		perspectives#NNS     1    
		recount#NN           1    
		subchannel#NN        1    
		utilize#VBP          1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 5
		Types: 5
		Elog#NN              1    
		codebooks#NNS        1    
		ergodic#NN           1    
		liminf#NN            1    
		subchannel#NN        1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 75157/5049
Size of tokens/types in Md: 940/303
Size of diff: 16

NOT in Collection (FWL):   5
In Collection (FWL):       5
In Collection (FWL + POS): 6

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Elog                  |  NN       |  -1       |  false   |  false         |  false  
codebooks             |  NNS      |  -1       |  false   |  false         |  false  
ergodic               |  NN       |  -1       |  false   |  false         |  false  
liminf                |  NN       |  -1       |  false   |  false         |  false  
subchannel            |  NN       |  -1       |  false   |  false         |  false  

Comp                  |  NNP      |  17385    |  false   |  false         |  true   
Decoding              |  NNP      |  30562    |  false   |  false         |  true   
converse              |  VB       |  23438    |  false   |  false         |  true   
encoding              |  NN       |  25864    |  false   |  false         |  true   
gAUSSIAN              |  NNP      |  43333    |  false   |  false         |  true   

conjugate             |  NN       |  62853    |  false   |  true          |  true   
converse              |  NN       |  35017    |  false   |  true          |  true   
inaccuracy            |  NN       |  41369    |  false   |  true          |  true   
instantaneous         |  JJ       |  20017    |  false   |  true          |  true   
recount               |  NN       |  13820    |  false   |  true          |  true   
utilize               |  VBP      |  11420    |  false   |  true          |  true   


Unique words (size: 15, lower case) without POS:
codebooks
comp
conjugate
converse
decoding
elog
encoding
ergodic
gaussian
inaccuracy
instantaneous
liminf
recount
subchannel
utilize

