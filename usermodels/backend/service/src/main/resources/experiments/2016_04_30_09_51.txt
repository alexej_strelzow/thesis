
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 38
		Types: 37
		activation#NN        1    
		anatomical#JJ        1    
		authentication#      1    
		authentication#NN    1    
		benchmark#NN         1    
		classify#            1    
		cosmetics#NNS        1    
		disguise#            1    
		disguise#NNS         1    
		disguised#JJ         1    
		emeritus#            1    
		fingerprint#NN       1    
		forgery#NN           1    
		frontal#JJ           1    
		geometrically#RB     1    
		ghostly#JJ           1    
		illumination#NN      1    
		inference#NNS        1    
		modality#NNS         1    
		moustache#NN         1    
		neural#JJ            1    
		perceptual#JJ        1    
		pertinent#JJ         1    
		pixel#               1    
		pixel#NN             1    
		pixel#NNS            1    
		radial#              1    
		radial#JJ            2    
		retina#NN            1    
		seamless#JJ          1    
		simultaneous#JJ      1    
		specificity#NN       1    
		synergism#NN         1    
		synergistic#JJ       1    
		undesired#JJ         1    
		unifying#JJ          1    
		weighting#NN         1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 2
		Types: 2
		skinless#JJ          1    
		supervised#JJ        1    
	NOT_IN_USER_MODEL
		Tokens: 166
		Types: 166
		ACADEMY#NN           1    
		ACADEMY#NNP          1    
		Authentication#NNP   1    
		Back#RB              1    
		Basis#NN             1    
		CONCLUSION#NN        1    
		CROP#NN              1    
		Class#NNP            1    
		Classification#NN    1    
		Common#JJ            1    
		DISCUSSIONS#NNS      1    
		Development#NNP      1    
		EXPERIMENTAL#NNP     1    
		Effect#NN            1    
		Eigen#NN             1    
		Eigenfaces#NNS       1    
		Emeritus#NNP         1    
		Every#DT             1    
		Face#NNP             1    
		Fellow#NNP           1    
		Fusion#NN            1    
		Geometrically#RB     1    
		Government#NNP       1    
		Human#NNP            1    
		INTRODUCTION#NNP     1    
		Image#NN             1    
		Images#NNPS          1    
		Images#NNS           1    
		Infrared#JJ          1    
		Ministry#NNP         1    
		Momentum#NN          1    
		Neural#JJ            1    
		Object#NNP           1    
		Online#NNP           1    
		PUBLISHER#NN         1    
		PUBLISHER#NNP        1    
		Perception#NNP       1    
		Picture#NNP          1    
		Pixel#NNP            1    
		Radial#JJ            1    
		Radial#NNP           1    
		Recognition#NN       1    
		Relevant#JJ          1    
		Research#NN          1    
		SYSTEM#NNP           1    
		Spectrum#NN          1    
		Technique#NN         1    
		Thermal#JJ           1    
		Those#DT             1    
		Tracking#NNP         1    
		Visible#JJ           1    
		Visual#JJ            1    
		West#NNP             1    
		ability#NN           1    
		acceptance#NN        1    
		account#VBP          1    
		activation#NN        1    
		active#JJ            1    
		affairs#NNS          1    
		affect#VB            1    
		affect#VBP           1    
		alarms#NNS           1    
		ambiguities#NNS      1    
		ambiguous#JJ         1    
		analyze#VB           1    
		anatomical#JJ        1    
		attempt#VBP          1    
		authentication#NN    1    
		automated#JJ         1    
		automatic#JJ         1    
		bankcard#NN          1    
		beard#VBD            1    
		benchmark#NN         1    
		camera#NN            1    
		card#NN              1    
		categories#NNS       1    
		category#NN          1    
		classify#VB          1    
		constitute#VBP       1    
		conventional#JJ      1    
		cooperation#NN       1    
		cosmetics#NNS        1    
		dark#JJ              1    
		darkness#NN          1    
		detect#VBP           1    
		disguise#VB          1    
		disguised#JJ         1    
		disguises#NNS        1    
		display#VB           1    
		eigenface#NN         1    
		eigenfaces#NNS       1    
		emit#VBP             1    
		everyday#JJ          1    
		eyes#NNS             1    
		face#NN              1    
		face#VBP             1    
		facial#JJ            1    
		feed#NN              1    
		fingerprint#NN       1    
		forgery#NN           1    
		frontal#JJ           1    
		fusion#NN            1    
		fuzzy#JJ             1    
		ghostly#JJ           1    
		hair#NN              1    
		head#NN              1    
		heat#NN              1    
		illuminating#JJ      1    
		illumination#NN      1    
		imagery#NN           1    
		individual#NN        1    
		inferences#NNS       1    
		infrared#JJ          1    
		intelligence#NN      1    
		intensity#NN         1    
		lighting#NN          1    
		modalities#NNS       1    
		moustache#NN         1    
		optical#JJ           1    
		outdoor#JJ           1    
		overcome#VBN         1    
		passive#NN           1    
		perceptual#JJ        1    
		pertinent#JJ         1    
		pixel#NN             1    
		pixels#NNS           1    
		pose#VB              1    
		preferred#JJ         1    
		present#VBP          1    
		prone#JJ             1    
		radial#JJ            1    
		radiation#NN         1    
		retina#NN            1    
		scene#NN             1    
		scenes#NNS           1    
		seamless#JJ          1    
		senses#NNS           1    
		shots#NNS            1    
		simultaneous#JJ      1    
		skin#NN              1    
		skinned#JJ           1    
		specificity#NN       1    
		sunglasses#NNS       1    
		surveillance#NN      1    
		synergism#NN         1    
		synergistic#JJ       1    
		test#VBP             1    
		texture#NN           1    
		thankful#IN          1    
		tissue#NN            1    
		touch#RB             1    
		trained#JJ           1    
		transport#NN         1    
		trends#NNS           1    
		twins#NNS            1    
		uncontrolled#JJ      1    
		undesired#JJ         1    
		unifying#JJ          1    
		unsupervised#JJ      1    
		upright#JJ           1    
		vein#NN              1    
		vessels#NNS          1    
		visual#JJ            1    
		vulnerable#JJ        1    
		warm#JJ              1    
		weighting#NN         1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 3
		Types: 3
		Eigenfaces#NNS       1    
		eigenface#NN         1    
		eigenfaces#NNS       1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 75157/5049
Size of tokens/types in Md: 1738/675
Size of diff: 43

NOT in Collection (FWL):   3
In Collection (FWL):       6
In Collection (FWL + POS): 34

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Eigenfaces            |  NNS      |  -1       |  false   |  false         |  false  
eigenface             |  NN       |  -1       |  false   |  false         |  false  
eigenfaces            |  NNS      |  -1       |  false   |  false         |  false  

Authentication        |  NNP      |  35660    |  false   |  false         |  true   
Emeritus              |  NNP      |  14292    |  false   |  false         |  true   
Pixel                 |  NNP      |  22125    |  false   |  false         |  true   
Radial                |  NNP      |  22679    |  false   |  false         |  true   
classify              |  VB       |  16032    |  false   |  false         |  true   
disguise              |  VB       |  15953    |  false   |  false         |  true   

Geometrically         |  RB       |  46854    |  false   |  true          |  true   
Neural                |  JJ       |  13448    |  false   |  true          |  true   
Radial                |  JJ       |  22679    |  false   |  true          |  true   
activation            |  NN       |  17265    |  false   |  true          |  true   
anatomical            |  JJ       |  18978    |  false   |  true          |  true   
authentication        |  NN       |  35660    |  false   |  true          |  true   
benchmark             |  NN       |  13336    |  false   |  true          |  true   
cosmetics             |  NNS      |  13504    |  false   |  true          |  true   
disguised             |  JJ       |  27597    |  false   |  true          |  true   
disguises             |  NNS      |  39397    |  false   |  true          |  true   
fingerprint           |  NN       |  17758    |  false   |  true          |  true   
forgery               |  NN       |  28571    |  false   |  true          |  true   
frontal               |  JJ       |  14551    |  false   |  true          |  true   
ghostly               |  JJ       |  15325    |  false   |  true          |  true   
illumination          |  NN       |  15837    |  false   |  true          |  true   
inferences            |  NNS      |  19632    |  false   |  true          |  true   
modalities            |  NNS      |  21878    |  false   |  true          |  true   
moustache             |  NN       |  23936    |  false   |  true          |  true   
perceptual            |  JJ       |  13710    |  false   |  true          |  true   
pertinent             |  JJ       |  13346    |  false   |  true          |  true   
pixel                 |  NN       |  22125    |  false   |  true          |  true   
pixels                |  NNS      |  18580    |  false   |  true          |  true   
radial                |  JJ       |  22679    |  false   |  true          |  true   
retina                |  NN       |  23304    |  false   |  true          |  true   
seamless              |  JJ       |  16735    |  false   |  true          |  true   
simultaneous          |  JJ       |  11403    |  false   |  true          |  true   
skinned               |  JJ       |  36561    |  false   |  true          |  true   
specificity           |  NN       |  14703    |  false   |  true          |  true   
synergism             |  NN       |  76957    |  false   |  true          |  true   
synergistic           |  JJ       |  35177    |  false   |  true          |  true   
undesired             |  JJ       |  47608    |  false   |  true          |  true   
unifying              |  JJ       |  19059    |  false   |  true          |  true   
unsupervised          |  JJ       |  30155    |  false   |  true          |  true   
weighting             |  NN       |  28638    |  false   |  true          |  true   


Unique words (size: 38, lower case) without POS:
activation
anatomical
authentication
benchmark
classify
cosmetics
disguise
disguised
disguises
eigenface
eigenfaces
emeritus
fingerprint
forgery
frontal
geometrically
ghostly
illumination
inferences
modalities
moustache
neural
perceptual
pertinent
pixel
pixels
radial
retina
seamless
simultaneous
skinned
specificity
synergism
synergistic
undesired
unifying
unsupervised
weighting

