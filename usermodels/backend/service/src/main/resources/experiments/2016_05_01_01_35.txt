
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 150
		Types: 133
		accordance#NN        1    
		advancement#         1    
		anorexia#NN          1    
		antigen#NN           2    
		appendix#            1    
		aspirate#            2    
		assay#NN             1    
		bacteremia#          1    
		biopsy#              1    
		biopsy#NN            1    
		biostatistics#       1    
		bracket#NNS          1    
		cardiopulmonary#JJ   1    
		cellularity#NN       1    
		chemotherapy#NN      1    
		chimeric#JJ          2    
		cisplatin#NN         1    
		clone#NN             1    
		completeness#NN      1    
		conjunction#NN       1    
		conquer#             1    
		consist#VBP          1    
		cyclophosphamide#NN  1    
		cytokine#NN          1    
		cytometry#           1    
		cytometry#NN         1    
		cytotoxic#JJ         1    
		deciliter#NN         1    
		deletion#NN          1    
		detectable#JJ        1    
		dexamethasone#NN     1    
		download#            1    
		doxorubicin#NN       1    
		durability#NN        1    
		durable#JJ           1    
		electrophoresis#NN   1    
		eligibility#NN       1    
		eosin#NN             1    
		epidemiology#        1    
		evasion#NN           1    
		fang#                1    
		feasibility#NN       2    
		ferritin#NN          2    
		fluorescence#NN      1    
		fracture#NNS         1    
		gamma#NN             1    
		gastrointestinal#JJ  1    
		graft#NN             1    
		hematology#          1    
		hematopoietic#JJ     1    
		hematoxylin#NN       1    
		hybridization#NN     1    
		immunoglobulin#NN    2    
		inflammatory#JJ      2    
		infusion#NN          1    
		inhibitor#NNS        1    
		inset#NN             1    
		interferon#NN        2    
		interleukin#NN       2    
		kappa#NN             1    
		lambda#              1    
		leukemia#NN          1    
		lineage#NN           1    
		logarithmic#JJ       1    
		lymphocyte#NN        1    
		lymphocyte#NNS       1    
		lymphocytic#JJ       1    
		marina#              1    
		marker#NN            1    
		marker#NNS           2    
		marrow#NN            2    
		marshall#            1    
		maturation#NN        1    
		measurable#JJ        1    
		mediated#JJ          1    
		messenger#NN         1    
		metric#JJ            1    
		microenvironment#NN  1    
		microgram#NN         1    
		millimeter#NN        1    
		monoclonal#JJ        1    
		motley#              1    
		myeloma#             1    
		myeloma#NN           2    
		nadir#NN             1    
		nausea#NN            1    
		neoplastic#JJ        2    
		neurotoxic#JJ        1    
		neutropenia#NN       1    
		neutrophil#NN        1    
		pathology#           1    
		penultimate#         1    
		peripheral#JJ        1    
		phenotype#NN         1    
		phenotype#NNS        1    
		plasma#NN            2    
		poly#NN              1    
		polymerase#NN        1    
		prognosis#NN         1    
		progression#NN       1    
		reagent#NN           1    
		receptor#NN          2    
		referral#NN          1    
		refractory#JJ        1    
		reprint#             1    
		reservoir#NN         1    
		residual#JJ          1    
		scatter#             1    
		sequencing#NN        1    
		serum#NN             2    
		spike#NN             1    
		spike#VBP            1    
		sponsor#VBP          1    
		staining#NN          1    
		staphylococcus#      1    
		sterile#JJ           1    
		strait#              1    
		stringent#JJ         1    
		subtract#            1    
		synergistic#JJ       1    
		synergy#NN           1    
		systemic#JJ          1    
		therapeutics#        1    
		thrombocytopenia#NN  1    
		transcriptase#NN     1    
		transduction#NN      1    
		transplantation#NN   2    
		undergo#VBP          1    
		urinary#JJ           1    
		urine#NN             1    
		vertebral#JJ         1    
		vitro#NN             1    
		vouch#VBP            1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 5
		Types: 5
		benign#JJ            1    
		extracellular#JJ     1    
		immanent#JJ          1    
		syllabic#JJ          1    
		unshaded#JJ          1    
	NOT_IN_USER_MODEL
		Tokens: 389
		Types: 389
		Activity#NN          1    
		Advancement#NNP      1    
		Antigen#NN           1    
		Aplasia#NN           1    
		Appendix#NNP         1    
		Aspirate#VB          1    
		Autologous#JJ        1    
		Biopsy#NNP           1    
		Biostatistics#NNP    1    
		Blood#NN             1    
		Bone#NN              1    
		Bone#VBP             1    
		Brief#JJ             1    
		Burden#NN            1    
		Cancer#NNP           1    
		Cell#NN              1    
		Cells#NNS            1    
		Center#NNP           1    
		Children#NNP         1    
		Chimeric#JJ          1    
		Civic#NNP            1    
		Clinical#JJ          1    
		Conquer#NNP          1    
		Copyright#NN         1    
		Copyright#NNP        1    
		Core#NN              1    
		Cytometry#NNP        1    
		Days#NNS             1    
		Disease#NN           1    
		Division#NNP         1    
		Donor#NN             1    
		Downloaded#NNP       1    
		Effects#NNS          1    
		Eligibility#NN       1    
		Epidemiology#NNP     1    
		Expression#NN        1    
		FISH#NN              1    
		Facility#NN          1    
		Fang#NNP             1    
		Feasibility#NN       1    
		Ferritin#NN          1    
		Flow#NN              1    
		Frequency#NN         1    
		Green#NNP            1    
		Health#NNP           1    
		Healthy#JJ           1    
		Hematology#NNP       1    
		High#NNP             1    
		Holly#NNP            1    
		Hospital#NNP         1    
		Immunoglobulin#NN    1    
		Inflammatory#JJ      1    
		Institutes#NNP       1    
		Interferon#NN        1    
		Interleukin#NN       1    
		Investigator#NNP     1    
		Lambda#NNP           1    
		Maintenance#NN       1    
		Marina#NNP           1    
		Markers#NNS          1    
		Marrow#NN            1    
		Marshall#NNP         1    
		Measures#NNS         1    
		Medical#NNP          1    
		Medicine#NN          1    
		Medicine#NNP         1    
		Motley#NNP           1    
		Multiple#JJ          1    
		Myeloma#NN           1    
		Myeloma#NNP          1    
		Neoplastic#JJ        1    
		PBMC#NN              1    
		Panel#NN             1    
		Pathology#NNP        1    
		Plasma#NN            1    
		Production#NN        1    
		Receptor#NN          1    
		Report#NNP           1    
		Response#NN          1    
		Scatter#NNP          1    
		Sell#VB              1    
		September#NNP        1    
		Serum#NN             1    
		Side#NN              1    
		Staphylococcus#FW    1    
		Stem#NN              1    
		Strait#NNP           1    
		Study#NN             1    
		Subtract#VB          1    
		Systemic#JJ          1    
		Therapeutics#NNS     1    
		Therapies#NNS        1    
		Toxic#JJ             1    
		Transduction#NN      1    
		Translational#JJ     1    
		Transplantation#NN   1    
		Twelve#CD            1    
		Vaccine#NN           1    
		Working#NNP          1    
		Young#NNP            1    
		absence#NN           1    
		accordance#NN        1    
		acute#JJ             1    
		adequate#JJ          1    
		administration#NN    1    
		adverse#JJ           1    
		advice#NN            1    
		afebrile#RB          1    
		alone#RB             1    
		anorexia#NN          1    
		anti#JJ              1    
		antigen#NN           1    
		aplasia#NN           1    
		aspirate#VB          1    
		assay#NN             1    
		assess#VB            1    
		assessment#NN        1    
		attributable#JJ      1    
		aureus#FW            1    
		autologous#JJ        1    
		bacteremia#FW        1    
		bars#NNS             1    
		baseline#NN          1    
		before#RB            1    
		biopsy#NN            1    
		blood#NN             1    
		body#NN              1    
		bone#NN              1    
		bortezomib#NN        1    
		brackets#NNS         1    
		cancer#NN            1    
		cancers#NNS          1    
		cardiopulmonary#JJ   1    
		carfilzomib#NN       1    
		cell#NN              1    
		cells#NNS            1    
		cellular#JJ          1    
		cellularity#NN       1    
		chemotherapy#NN      1    
		chimeric#JJ          1    
		chronic#JJ           1    
		cisplatin#NN         1    
		clarithromycin#NN    1    
		clinical#JJ          1    
		clone#NN             1    
		combined#JJ          1    
		compassionate#JJ     1    
		complete#VB          1    
		completeness#NN      1    
		concentration#NN     1    
		concentrations#NNS   1    
		confidence#NN        1    
		conjunction#NN       1    
		consist#VBP          1    
		count#NN             1    
		cultures#NNS         1    
		cycles#NNS           1    
		cyclophosphamide#NN  1    
		cyto#NN              1    
		cytokine#NN          1    
		cytometry#NN         1    
		cytotoxic#JJ         1    
		daily#RB             1    
		days#NNS             1    
		dead#JJ              1    
		debris#NN            1    
		deciliter#NN         1    
		decline#NN           1    
		deep#JJ              1    
		deletion#NN          1    
		dependent#JJ         1    
		despite#IN           1    
		detectable#JJ        1    
		dexamethasone#NN     1    
		diagnosis#NN         1    
		disease#NN           1    
		dominant#JJ          1    
		donor#NN             1    
		dose#NN              1    
		doxorubicin#NN       1    
		drug#NN              1    
		durability#NN        1    
		durable#JJ           1    
		effect#NN            1    
		effects#NNS          1    
		efficacy#NN          1    
		electrophoresis#NN   1    
		elotuzumab#NN        1    
		enterocolitis#NN     1    
		eosin#NN             1    
		etoposide#NN         1    
		evasion#NN           1    
		exclude#VBP          1    
		faint#JJ             1    
		favorable#JJ         1    
		feasibility#NN       1    
		ferritin#NN          1    
		fever#NN             1    
		fevers#NNS           1    
		filgrastim#NN        1    
		findings#NNS         1    
		flow#NN              1    
		fluorescence#NN      1    
		fractures#NNS        1    
		frequencies#NNS      1    
		funding#NN           1    
		gain#NN              1    
		gamma#NN             1    
		gastrointestinal#JJ  1    
		gate#NN              1    
		gene#NN              1    
		grade#NN             1    
		graft#NN             1    
		healthy#JJ           1    
		hematopoietic#JJ     1    
		hematoxylin#NN       1    
		hour#NN              1    
		hybridization#NN     1    
		hypogammaglobulinemia#NN 1    
		idiotype#NN          1    
		immunofixation#NN    1    
		immunoglobulin#NN    1    
		immunophenotype#NN   1    
		immunostaining#NN    1    
		immunotherapeutic#JJ 1    
		inflammatory#JJ      1    
		infusion#NN          1    
		inhibitors#NNS       1    
		inset#NN             1    
		institutional#JJ     1    
		interferon#NN        1    
		interleukin#NN       1    
		interphase#NN        1    
		intracellular#JJ     1    
		involvement#NN       1    
		june#NNP             1    
		kappa#NN             1    
		karyotype#NN         1    
		kerr#NN              1    
		lacey#NN             1    
		lenalidomide#NN      1    
		leukapheresis#NN     1    
		leukemia#NN          1    
		levine#NN            1    
		light#NN             1    
		likely#JJ            1    
		lineage#NN           1    
		logarithmic#JJ       1    
		lymphocyte#NN        1    
		lymphocytes#NNS      1    
		lymphocytic#JJ       1    
		mRNA#NN              1    
		majority#NN          1    
		malignant#JJ         1    
		manufacturing#NN     1    
		manuscript#NN        1    
		marker#NN            1    
		markers#NNS          1    
		marrow#NN            1    
		maturation#NN        1    
		means#NNS            1    
		measurable#JJ        1    
		measure#NN           1    
		measurements#NNS     1    
		measures#NNS         1    
		mediated#JJ          1    
		melphalan#NN         1    
		messenger#NN         1    
		meter#NN             1    
		metric#JJ            1    
		microenvironment#NN  1    
		microgram#NN         1    
		millimeter#NN        1    
		minor#JJ             1    
		molecular#JJ         1    
		monitor#NN           1    
		monoclonal#JJ        1    
		monocytes#NNS        1    
		mononuclear#JJ       1    
		monosomy#NN          1    
		month#NN             1    
		months#NNS           1    
		mucositis#NN         1    
		myeloma#NN           1    
		nadir#NN             1    
		nausea#NN            1    
		negative#JJ          1    
		neoplastic#JJ        1    
		neurotoxic#JJ        1    
		neutropenia#NN       1    
		neutrophil#NN        1    
		nine#CD              1    
		observations#NNS     1    
		ongoing#JJ           1    
		optional#JJ          1    
		panel#NN             1    
		patient#JJ           1    
		patient#NN           1    
		patients#NNS         1    
		penultimate#NN       1    
		peripheral#JJ        1    
		pgml#NN              1    
		phenotype#NN         1    
		phenotypes#NNS       1    
		pilot#NN             1    
		plasma#NN            1    
		plot#NN              1    
		poly#NN              1    
		polymerase#NN        1    
		pomalidomide#NN      1    
		population#NN        1    
		populations#NNS      1    
		post#NN              1    
		prior#JJ             1    
		probably#RB          1    
		production#NN        1    
		prognosis#NN         1    
		progression#NN       1    
		promising#JJ         1    
		proteasome#NN        1    
		protein#NN           1    
		provision#NN         1    
		publication#NN       1    
		qPCR#NN              1    
		quantitation#NN      1    
		quantitative#JJ      1    
		reaction#NN          1    
		reagent#NN           1    
		recep#NN             1    
		receptor#NN          1    
		referral#NN          1    
		refractory#JJ        1    
		report#VBP           1    
		reprint#VB           1    
		reservoir#NN         1    
		residual#JJ          1    
		rise#NN              1    
		rise#VB              1    
		sample#NN            1    
		schedule#NN          1    
		screening#NN         1    
		sequencing#NN        1    
		serum#NN             1    
		severe#JJ            1    
		shaded#JJ            1    
		situ#FW              1    
		solid#JJ             1    
		spectrum#NN          1    
		spike#NN             1    
		spike#VBP            1    
		sponsor#VBP          1    
		square#NN            1    
		staining#NN          1    
		stem#NN              1    
		stemcell#NN          1    
		sterile#JJ           1    
		stringent#JJ         1    
		suggest#VBP          1    
		surface#NN           1    
		sustained#JJ         1    
		symptoms#NNS         1    
		syndrome#NN          1    
		synergistic#JJ       1    
		synergy#NN           1    
		thank#VB             1    
		therapies#NNS        1    
		therapy#NN           1    
		thoughtful#JJ        1    
		thrombocytopenia#NN  1    
		toxic#JJ             1    
		transcriptase#NN     1    
		transient#JJ         1    
		transplantation#NN   1    
		treatment#NN         1    
		trial#NN             1    
		tumor#NN             1    
		tumors#NNS           1    
		undergo#VBP          1    
		unpublished#JJ       1    
		upenn#NN             1    
		urinary#JJ           1    
		urine#NN             1    
		vast#JJ              1    
		vertebral#JJ         1    
		vitro#NN             1    
		vivo#FW              1    
		vorinostat#NN        1    
		vouch#VBP            1    
		weekly#JJ            1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 35
		Types: 35
		Aplasia#NN           1    
		PBMC#NN              1    
		afebrile#RB          1    
		aplasia#NN           1    
		aureus#FW            1    
		bortezomib#NN        1    
		carfilzomib#NN       1    
		clarithromycin#NN    1    
		cyto#NN              1    
		elotuzumab#NN        1    
		enterocolitis#NN     1    
		etoposide#NN         1    
		filgrastim#NN        1    
		hypogammaglobulinemia#NN 1    
		idiotype#NN          1    
		immunofixation#NN    1    
		immunophenotype#NN   1    
		immunostaining#NN    1    
		immunotherapeutic#JJ 1    
		karyotype#NN         1    
		lenalidomide#NN      1    
		leukapheresis#NN     1    
		mRNA#NN              1    
		melphalan#NN         1    
		monocytes#NNS        1    
		monosomy#NN          1    
		mucositis#NN         1    
		pomalidomide#NN      1    
		proteasome#NN        1    
		qPCR#NN              1    
		quantitation#NN      1    
		recep#NN             1    
		situ#FW              1    
		stemcell#NN          1    
		vorinostat#NN        1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 36698/3230
Size of tokens/types in Md: 1900/655
Size of diff: 190

NOT in Collection (FWL):   35
In Collection (FWL):       26
In Collection (FWL + POS): 129

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Aplasia               |  NN       |  -1       |  false   |  false         |  false  
PBMC                  |  NN       |  -1       |  false   |  false         |  false  
afebrile              |  RB       |  -1       |  false   |  false         |  false  
aplasia               |  NN       |  -1       |  false   |  false         |  false  
aureus                |  FW       |  -1       |  false   |  false         |  false  
bortezomib            |  NN       |  -1       |  false   |  false         |  false  
carfilzomib           |  NN       |  -1       |  false   |  false         |  false  
clarithromycin        |  NN       |  -1       |  false   |  false         |  false  
cyto                  |  NN       |  -1       |  false   |  false         |  false  
elotuzumab            |  NN       |  -1       |  false   |  false         |  false  
enterocolitis         |  NN       |  -1       |  false   |  false         |  false  
etoposide             |  NN       |  -1       |  false   |  false         |  false  
filgrastim            |  NN       |  -1       |  false   |  false         |  false  
hypogammaglobulinemia  |  NN       |  -1       |  false   |  false         |  false  
idiotype              |  NN       |  -1       |  false   |  false         |  false  
immunofixation        |  NN       |  -1       |  false   |  false         |  false  
immunophenotype       |  NN       |  -1       |  false   |  false         |  false  
immunostaining        |  NN       |  -1       |  false   |  false         |  false  
immunotherapeutic     |  JJ       |  -1       |  false   |  false         |  false  
karyotype             |  NN       |  -1       |  false   |  false         |  false  
lenalidomide          |  NN       |  -1       |  false   |  false         |  false  
leukapheresis         |  NN       |  -1       |  false   |  false         |  false  
mRNA                  |  NN       |  -1       |  false   |  false         |  false  
melphalan             |  NN       |  -1       |  false   |  false         |  false  
monocytes             |  NNS      |  -1       |  false   |  false         |  false  
monosomy              |  NN       |  -1       |  false   |  false         |  false  
mucositis             |  NN       |  -1       |  false   |  false         |  false  
pomalidomide          |  NN       |  -1       |  false   |  false         |  false  
proteasome            |  NN       |  -1       |  false   |  false         |  false  
qPCR                  |  NN       |  -1       |  false   |  false         |  false  
quantitation          |  NN       |  -1       |  false   |  false         |  false  
recep                 |  NN       |  -1       |  false   |  false         |  false  
situ                  |  FW       |  -1       |  false   |  false         |  false  
stemcell              |  NN       |  -1       |  false   |  false         |  false  
vorinostat            |  NN       |  -1       |  false   |  false         |  false  

Advancement           |  NNP      |  9207     |  false   |  false         |  true   
Appendix              |  NNP      |  12045    |  false   |  false         |  true   
Aspirate              |  VB       |  78680    |  false   |  false         |  true   
Biopsy                |  NNP      |  15859    |  false   |  false         |  true   
Biostatistics         |  NNP      |  72768    |  false   |  false         |  true   
Conquer               |  NNP      |  13829    |  false   |  false         |  true   
Cytometry             |  NNP      |  77299    |  false   |  false         |  true   
Downloaded            |  NNP      |  25164    |  false   |  false         |  true   
Epidemiology          |  NNP      |  17370    |  false   |  false         |  true   
Fang                  |  NNP      |  20182    |  false   |  false         |  true   
Hematology            |  NNP      |  72693    |  false   |  false         |  true   
Lambda                |  NNP      |  22104    |  false   |  false         |  true   
Marina                |  NNP      |  18755    |  false   |  false         |  true   
Marshall              |  NNP      |  24280    |  false   |  false         |  true   
Motley                |  NNP      |  20131    |  false   |  false         |  true   
Myeloma               |  NNP      |  51539    |  false   |  false         |  true   
Pathology             |  NNP      |  14409    |  false   |  false         |  true   
Scatter               |  NNP      |  14338    |  false   |  false         |  true   
Staphylococcus        |  FW       |  37884    |  false   |  false         |  true   
Strait                |  NNP      |  12873    |  false   |  false         |  true   
Subtract              |  VB       |  22362    |  false   |  false         |  true   
Therapeutics          |  NNS      |  39969    |  false   |  false         |  true   
aspirate              |  VB       |  78680    |  false   |  false         |  true   
bacteremia            |  FW       |  50352    |  false   |  false         |  true   
penultimate           |  NN       |  36872    |  false   |  false         |  true   
reprint               |  VB       |  31545    |  false   |  false         |  true   

Antigen               |  NN       |  29313    |  false   |  true          |  true   
Chimeric              |  JJ       |  80231    |  false   |  true          |  true   
Eligibility           |  NN       |  10760    |  false   |  true          |  true   
Feasibility           |  NN       |  15537    |  false   |  true          |  true   
Ferritin              |  NN       |  62160    |  false   |  true          |  true   
Immunoglobulin        |  NN       |  47645    |  false   |  true          |  true   
Inflammatory          |  JJ       |  13819    |  false   |  true          |  true   
Interferon            |  NN       |  41190    |  false   |  true          |  true   
Interleukin           |  NN       |  76622    |  false   |  true          |  true   
Markers               |  NNS      |  9331     |  false   |  true          |  true   
Marrow                |  NN       |  13218    |  false   |  true          |  true   
Myeloma               |  NN       |  51539    |  false   |  true          |  true   
Neoplastic            |  JJ       |  59382    |  false   |  true          |  true   
Plasma                |  NN       |  10997    |  false   |  true          |  true   
Receptor              |  NN       |  20653    |  false   |  true          |  true   
Serum                 |  NN       |  12000    |  false   |  true          |  true   
Systemic              |  JJ       |  10033    |  false   |  true          |  true   
Transduction          |  NN       |  67927    |  false   |  true          |  true   
Transplantation       |  NN       |  21610    |  false   |  true          |  true   
accordance            |  NN       |  51249    |  false   |  true          |  true   
anorexia              |  NN       |  24340    |  false   |  true          |  true   
antigen               |  NN       |  29313    |  false   |  true          |  true   
assay                 |  NN       |  29487    |  false   |  true          |  true   
biopsy                |  NN       |  15859    |  false   |  true          |  true   
brackets              |  NNS      |  18576    |  false   |  true          |  true   
cardiopulmonary       |  JJ       |  42050    |  false   |  true          |  true   
cellularity           |  NN       |  88066    |  false   |  true          |  true   
chemotherapy          |  NN       |  10500    |  false   |  true          |  true   
chimeric              |  JJ       |  80231    |  false   |  true          |  true   
cisplatin             |  NN       |  65949    |  false   |  true          |  true   
clone                 |  NN       |  17435    |  false   |  true          |  true   
completeness          |  NN       |  28266    |  false   |  true          |  true   
conjunction           |  NN       |  22467    |  false   |  true          |  true   
consist               |  VBP      |  10642    |  false   |  true          |  true   
cyclophosphamide      |  NN       |  81125    |  false   |  true          |  true   
cytokine              |  NN       |  62817    |  false   |  true          |  true   
cytometry             |  NN       |  77299    |  false   |  true          |  true   
cytotoxic             |  JJ       |  65163    |  false   |  true          |  true   
deciliter             |  NN       |  62048    |  false   |  true          |  true   
deletion              |  NN       |  38563    |  false   |  true          |  true   
detectable            |  JJ       |  21833    |  false   |  true          |  true   
dexamethasone         |  NN       |  55320    |  false   |  true          |  true   
doxorubicin           |  NN       |  88135    |  false   |  true          |  true   
durability            |  NN       |  15256    |  false   |  true          |  true   
durable               |  JJ       |  10056    |  false   |  true          |  true   
electrophoresis       |  NN       |  49826    |  false   |  true          |  true   
eosin                 |  NN       |  68749    |  false   |  true          |  true   
evasion               |  NN       |  20442    |  false   |  true          |  true   
feasibility           |  NN       |  15537    |  false   |  true          |  true   
ferritin              |  NN       |  62160    |  false   |  true          |  true   
fluorescence          |  NN       |  29940    |  false   |  true          |  true   
fractures             |  NNS      |  11943    |  false   |  true          |  true   
gamma                 |  NN       |  13401    |  false   |  true          |  true   
gastrointestinal      |  JJ       |  19555    |  false   |  true          |  true   
graft                 |  NN       |  23174    |  false   |  true          |  true   
hematopoietic         |  JJ       |  86881    |  false   |  true          |  true   
hematoxylin           |  NN       |  70653    |  false   |  true          |  true   
hybridization         |  NN       |  31218    |  false   |  true          |  true   
immunoglobulin        |  NN       |  47645    |  false   |  true          |  true   
inflammatory          |  JJ       |  13819    |  false   |  true          |  true   
infusion              |  NN       |  15536    |  false   |  true          |  true   
inhibitors            |  NNS      |  21528    |  false   |  true          |  true   
inset                 |  NN       |  17966    |  false   |  true          |  true   
interferon            |  NN       |  41190    |  false   |  true          |  true   
interleukin           |  NN       |  76622    |  false   |  true          |  true   
intracellular         |  JJ       |  49387    |  false   |  true          |  true   
kappa                 |  NN       |  23367    |  false   |  true          |  true   
leukemia              |  NN       |  15121    |  false   |  true          |  true   
lineage               |  NN       |  13685    |  false   |  true          |  true   
logarithmic           |  JJ       |  48798    |  false   |  true          |  true   
lymphocyte            |  NN       |  54263    |  false   |  true          |  true   
lymphocytes           |  NNS      |  39755    |  false   |  true          |  true   
lymphocytic           |  JJ       |  60578    |  false   |  true          |  true   
malignant             |  JJ       |  15763    |  false   |  true          |  true   
marker                |  NN       |  9287     |  false   |  true          |  true   
markers               |  NNS      |  9331     |  false   |  true          |  true   
marrow                |  NN       |  13218    |  false   |  true          |  true   
maturation            |  NN       |  21877    |  false   |  true          |  true   
measurable            |  JJ       |  15430    |  false   |  true          |  true   
mediated              |  JJ       |  37088    |  false   |  true          |  true   
messenger             |  NN       |  10102    |  false   |  true          |  true   
metric                |  JJ       |  13081    |  false   |  true          |  true   
microenvironment      |  NN       |  95488    |  false   |  true          |  true   
microgram             |  NN       |  95487    |  false   |  true          |  true   
millimeter            |  NN       |  23735    |  false   |  true          |  true   
monoclonal            |  JJ       |  37357    |  false   |  true          |  true   
myeloma               |  NN       |  51539    |  false   |  true          |  true   
nadir                 |  NN       |  37194    |  false   |  true          |  true   
nausea                |  NN       |  12487    |  false   |  true          |  true   
neoplastic            |  JJ       |  59382    |  false   |  true          |  true   
neurotoxic            |  JJ       |  68780    |  false   |  true          |  true   
neutropenia           |  NN       |  88293    |  false   |  true          |  true   
neutrophil            |  NN       |  76674    |  false   |  true          |  true   
peripheral            |  JJ       |  10649    |  false   |  true          |  true   
phenotype             |  NN       |  39297    |  false   |  true          |  true   
phenotypes            |  NNS      |  45938    |  false   |  true          |  true   
plasma                |  NN       |  10997    |  false   |  true          |  true   
poly                  |  NN       |  22800    |  false   |  true          |  true   
polymerase            |  NN       |  38288    |  false   |  true          |  true   
prognosis             |  NN       |  15745    |  false   |  true          |  true   
progression           |  NN       |  10136    |  false   |  true          |  true   
quantitative          |  JJ       |  8880     |  false   |  true          |  true   
reagent               |  NN       |  72792    |  false   |  true          |  true   
receptor              |  NN       |  20653    |  false   |  true          |  true   
referral              |  NN       |  11327    |  false   |  true          |  true   
refractory            |  JJ       |  40873    |  false   |  true          |  true   
reservoir             |  NN       |  9629     |  false   |  true          |  true   
residual              |  JJ       |  9527     |  false   |  true          |  true   
sequencing            |  NN       |  17114    |  false   |  true          |  true   
serum                 |  NN       |  12000    |  false   |  true          |  true   
shaded                |  JJ       |  19697    |  false   |  true          |  true   
spike                 |  NN       |  10745    |  false   |  true          |  true   
spike                 |  VBP      |  20212    |  false   |  true          |  true   
sponsor               |  VBP      |  14438    |  false   |  true          |  true   
staining              |  NN       |  36076    |  false   |  true          |  true   
sterile               |  JJ       |  12963    |  false   |  true          |  true   
stringent             |  JJ       |  12510    |  false   |  true          |  true   
synergistic           |  JJ       |  35177    |  false   |  true          |  true   
synergy               |  NN       |  24117    |  false   |  true          |  true   
thrombocytopenia      |  NN       |  76659    |  false   |  true          |  true   
transcriptase         |  NN       |  65901    |  false   |  true          |  true   
transient             |  JJ       |  14702    |  false   |  true          |  true   
transplantation       |  NN       |  21610    |  false   |  true          |  true   
undergo               |  VBP      |  9649     |  false   |  true          |  true   
urinary               |  JJ       |  20950    |  false   |  true          |  true   
urine                 |  NN       |  9255     |  false   |  true          |  true   
vertebral             |  JJ       |  34269    |  false   |  true          |  true   
vitro                 |  NN       |  95775    |  false   |  true          |  true   
vouch                 |  VBP      |  31839    |  false   |  true          |  true   


Unique words (size: 168, lower case) without POS:
accordance
advancement
afebrile
anorexia
antigen
aplasia
appendix
aspirate
assay
aureus
bacteremia
biopsy
biostatistics
bortezomib
brackets
cardiopulmonary
carfilzomib
cellularity
chemotherapy
chimeric
cisplatin
clarithromycin
clone
completeness
conjunction
conquer
consist
cyclophosphamide
cyto
cytokine
cytometry
cytotoxic
deciliter
deletion
detectable
dexamethasone
downloaded
doxorubicin
durability
durable
electrophoresis
eligibility
elotuzumab
enterocolitis
eosin
epidemiology
etoposide
evasion
fang
feasibility
ferritin
filgrastim
fluorescence
fractures
gamma
gastrointestinal
graft
hematology
hematopoietic
hematoxylin
hybridization
hypogammaglobulinemia
idiotype
immunofixation
immunoglobulin
immunophenotype
immunostaining
immunotherapeutic
inflammatory
infusion
inhibitors
inset
interferon
interleukin
intracellular
kappa
karyotype
lambda
lenalidomide
leukapheresis
leukemia
lineage
logarithmic
lymphocyte
lymphocytes
lymphocytic
malignant
marina
marker
markers
marrow
marshall
maturation
measurable
mediated
melphalan
messenger
metric
microenvironment
microgram
millimeter
monoclonal
monocytes
monosomy
motley
mrna
mucositis
myeloma
nadir
nausea
neoplastic
neurotoxic
neutropenia
neutrophil
pathology
pbmc
penultimate
peripheral
phenotype
phenotypes
plasma
poly
polymerase
pomalidomide
prognosis
progression
proteasome
qpcr
quantitation
quantitative
reagent
recep
receptor
referral
refractory
reprint
reservoir
residual
scatter
sequencing
serum
shaded
situ
spike
sponsor
staining
staphylococcus
stemcell
sterile
strait
stringent
subtract
synergistic
synergy
systemic
therapeutics
thrombocytopenia
transcriptase
transduction
transient
transplantation
undergo
urinary
urine
vertebral
vitro
vorinostat
vouch

