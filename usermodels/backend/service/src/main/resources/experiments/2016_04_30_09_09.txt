
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 83
		Types: 76
		abstraction#NN       1    
		accumulative#JJ      1    
		acknowledgement#NN   1    
		approximation#NN     1    
		attribution#         1    
		bloom#NN             1    
		bonding#             1    
		bonding#NN           2    
		brute#               1    
		byte#NNS             1    
		categorize#          1    
		centrality#NN        1    
		centric#JJ           1    
		closeness#NN         1    
		commons#             1    
		compressed#JJ        1    
		computation#         2    
		conceptually#RB      1    
		correctness#NN       1    
		decompose#           1    
		diffuse#JJ           1    
		diffusion#NN         1    
		dong#                1    
		empower#             1    
		footprint#NN         1    
		fore#NN              1    
		formalize#           1    
		google#              1    
		greedy#              1    
		greedy#JJ            1    
		hash#VBP             1    
		heron#               1    
		impractical#JJ       1    
		indispensable#JJ     1    
		invalid#JJ           1    
		kernel#NN            1    
		keyword#NNS          1    
		locality#NN          1    
		logistic#JJ          1    
		logistics#           1    
		lookup#NN            1    
		node#                1    
		novelty#NN           1    
		parallelism#NN       1    
		partition#NN         1    
		percolation#NN       1    
		populate#            1    
		preprocessing#       2    
		prominence#NN        1    
		pruning#NN           1    
		query#               3    
		query#VBP            1    
		queue#NN             1    
		reachability#NN      1    
		recurrence#NN        1    
		reside#              1    
		reside#VBP           1    
		scalability#NN       1    
		semantic#JJ          1    
		semantics#           1    
		sequential#JJ        1    
		simplification#NN    1    
		speedup#NN           1    
		summarize#VBP        1    
		terminology#NN       1    
		toolkit#NN           1    
		topological#JJ       1    
		topologically#RB     1    
		twitter#NN           1    
		unaccepted#JJ        1    
		uncovered#JJ         1    
		undirected#JJ        1    
		upfront#JJ           1    
		validation#NN        1    
		vertex#              3    
		workload#NN          1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 3
		Types: 3
		autogenous#JJ        1    
		invalidate#VBP       1    
		unmanageable#JJ      1    
	NOT_IN_USER_MODEL
		Tokens: 402
		Types: 402
		ABSTRACT#NN          1    
		Acknowledgement#NN   1    
		Approximation#NN     1    
		Attribution#NNP      1    
		August#NNP           1    
		Award#NN             1    
		Aware#NNP            1    
		BUILDING#NNP         1    
		Bases#NNS            1    
		Blocks#NNS           1    
		Bonding#NN           1    
		Bonding#NNP          1    
		Building#NN          1    
		CLOSED#NNP           1    
		COVERAGE#NNP         1    
		CPUs#NNS             1    
		Category#NNP         1    
		China#NNP            1    
		Chinese#NNP          1    
		Cloud#NN             1    
		Cloud#NNP            1    
		Coast#NNP            1    
		Common#JJ            1    
		Commons#NNP          1    
		Communication#NNP    1    
		Community#NNP        1    
		Complexity#NN        1    
		Compressed#JJ        1    
		Compression#NN       1    
		Computation#NNP      1    
		Copyright#NNP        1    
		Correctness#NN       1    
		Crawl#NNP            1    
		Creative#NNP         1    
		DEFINITION#NN        1    
		DEFINITION#NNP       1    
		DOMINANCE#NN         1    
		DOMINANCE#NNP        1    
		Data#NN              1    
		Data#NNP             1    
		Degree#NN            1    
		Direct#NNP           1    
		Dong#NNP             1    
		Edges#NNP            1    
		Efficacy#NNP         1    
		Efficiency#NNP       1    
		Endowment#NNP        1    
		Engine#NNP           1    
		English#JJ           1    
		Evaluation#NN        1    
		Experimental#JJ      1    
		Exploration#NN       1    
		Exploration#NNP      1    
		Explore#VB           1    
		Faculty#NNP          1    
		Fast#JJ              1    
		Flow#NN              1    
		Foundation#NNP       1    
		Framework#NN         1    
		Fund#NNP             1    
		Gift#NN              1    
		Google#NNP           1    
		Grand#NNP            1    
		Grant#NNP            1    
		Grants#NNPS          1    
		Graph#NN             1    
		Greedy#JJ            1    
		Greedy#NNP           1    
		Heron#NNP            1    
		Influential#JJ       1    
		Institute#NNP        1    
		January#NNP          1    
		LandMark#NNP         1    
		Landmark#NNP         1    
		Language#NNP         1    
		Large#JJ             1    
		License#NNP          1    
		Line#NN              1    
		Line#NNP             1    
		Lines#NNS            1    
		MINIMUM#NNP          1    
		Management#NNP       1    
		Mark#NNP             1    
		Million#NNP          1    
		Models#NNS           1    
		Monitoring#NN        1    
		National#NNP         1    
		Nodes#NNP            1    
		OPTIMAL#NNP          1    
		Other#JJ             1    
		Over#IN              1    
		PROCESSING#NNP       1    
		Partition#NN         1    
		Plan#NN              1    
		Preprocessing#NN     1    
		Preprocessing#NNP    1    
		Probe#NNP            1    
		Procedure#NNP        1    
		Proceedings#NNP      1    
		Processing#NNP       1    
		Program#NNP          1    
		Project#NNP          1    
		Query#NNP            1    
		Ranking#NNP          1    
		Result#NN            1    
		Science#NNP          1    
		Selection#NN         1    
		Setup#NNP            1    
		Size#NN              1    
		State#NNP            1    
		Storage#NN           1    
		Subsequent#JJ        1    
		Technologies#NNP     1    
		Trin#NN              1    
		Vertex#NNP           1    
		Very#RB              1    
		WORK#VBP             1    
		abstraction#NN       1    
		access#VB            1    
		accumulative#JJ      1    
		adjusted#JJ          1    
		advance#NN           1    
		agent#NN             1    
		alter#VBP            1    
		ancestors#NNS        1    
		arccos#FW            1    
		assemble#VB          1    
		assess#VB            1    
		balance#NN           1    
		balanced#JJ          1    
		baseline#NN          1    
		batch#NN             1    
		believe#VBP          1    
		belong#VB            1    
		best#RBS             1    
		better#RB            1    
		billion#CD           1    
		bloom#NN             1    
		bonding#NN           1    
		broadcast#VB         1    
		brute#JJ             1    
		budget#NN            1    
		build#VB             1    
		burst#NN             1    
		bytes#NNS            1    
		cache#NN             1    
		candidates#NNS       1    
		careful#JJ           1    
		categorize#NN        1    
		centrality#NN        1    
		centric#JJ           1    
		check#VBP            1    
		child#NN             1    
		children#NNS         1    
		classic#JJ           1    
		clauses#NNS          1    
		closeness#NN         1    
		cold#JJ              1    
		colis#NN             1    
		color#NN             1    
		combine#VBP          1    
		communities#NNS      1    
		compression#NN       1    
		compromise#NN        1    
		computation#NNP      1    
		conceptually#RB      1    
		concern#NN           1    
		conduct#VB           1    
		connect#VB           1    
		connect#VBP          1    
		connection#NN        1    
		contact#NN           1    
		contexts#NNS         1    
		contradictory#JJ     1    
		contrary#JJ          1    
		cool#JJ              1    
		cope#VB              1    
		copy#VBP             1    
		copying#NN           1    
		copyright#NN         1    
		correlations#NNS     1    
		cost#VB              1    
		cost#VBD             1    
		costly#JJ            1    
		costs#NNS            1    
		count#NN             1    
		counter#NN           1    
		credit#NN            1    
		dash#NN              1    
		decompose#VB         1    
		delicate#JJ          1    
		dense#JJ             1    
		determination#NN     1    
		dgri#NN              1    
		diff#NN              1    
		diffuse#JJ           1    
		diffusion#NN         1    
		discover#VBP         1    
		discovery#NN         1    
		disk#NN              1    
		distance#VBP         1    
		dominate#VB          1    
		eVALUATION#NN        1    
		eXPERIMENTS#NNS      1    
		early#JJ             1    
		edmonds#NNS          1    
		effectiveness#NN     1    
		efficacy#NN          1    
		eliminate#VBP        1    
		else#RB              1    
		empower#VB           1    
		engine#NN            1    
		estimate#VB          1    
		examination#NN       1    
		examine#VB           1    
		exclude#VBP          1    
		execution#NN         1    
		experiment#NN        1    
		exploration#NN       1    
		explore#VB           1    
		explore#VBP          1    
		extracts#NNS         1    
		false#JJ             1    
		findings#NNS         1    
		fine#JJ              1    
		flexibility#NN       1    
		footprint#NN         1    
		force#NN             1    
		fore#NN              1    
		formalize#VB         1    
		friendly#JJ          1    
		future#JJ            1    
		generic#JJ           1    
		girvan#NN            1    
		give#VBP             1    
		guess#VBP            1    
		guide#VB             1    
		hash#VBP             1    
		heterogeneous#JJ     1    
		highlight#NN         1    
		highmem#NN           1    
		hints#NNS            1    
		holder#NN            1    
		identify#VB          1    
		impractical#JJ       1    
		indispensable#JJ     1    
		inevitable#JJ        1    
		influential#JJ       1    
		invalid#JJ           1    
		keep#VB              1    
		kernel#NN            1    
		keywords#NNS         1    
		label#VB             1    
		label#VBP            1    
		landmark#NN          1    
		landmarks#NNS        1    
		large#RB             1    
		license#NN           1    
		lightweight#JJ       1    
		list#NN              1    
		locality#NN          1    
		lock#NN              1    
		logistic#JJ          1    
		logistics#NNS        1    
		lookup#NN            1    
		machines#NNS         1    
		makespan#NN          1    
		manageable#JJ        1    
		mark#NN              1    
		marked#JJ            1    
		meanwhile#RB         1    
		measurement#NN       1    
		meet#VBP             1    
		meta#NN              1    
		methodology#NN       1    
		million#CD           1    
		monitor#VBP          1    
		native#JJ            1    
		neighbors#NNS        1    
		normally#RB          1    
		novelty#NN           1    
		online#JJ            1    
		opening#NN           1    
		pages#NNS            1    
		parallelism#NN       1    
		parallelization#NN   1    
		participate#VB       1    
		pass#NN              1    
		pass#VB              1    
		pass#VBP             1    
		passing#NN           1    
		passive#NN           1    
		people#NNS           1    
		percolation#NN       1    
		permission#NN        1    
		persistent#JJ        1    
		pick#VBP             1    
		plan#NN              1    
		planning#NN          1    
		platform#NN          1    
		plugin#NN            1    
		populate#VB          1    
		precedent#NN         1    
		precision#NN         1    
		predecessor#NN       1    
		prediction#NN        1    
		preferred#JJ         1    
		prepare#VBP          1    
		preserved#JJ         1    
		probe#NN             1    
		prominence#NN        1    
		prominent#JJ         1    
		promises#NNS         1    
		prototype#NN         1    
		pruning#NN           1    
		qUERY#NNP            1    
		query#VB             1    
		query#VBP            1    
		queue#NN             1    
		quickly#RB           1    
		rELATED#NNP          1    
		ranking#JJ           1    
		ranking#NN           1    
		reachability#NN      1    
		read#VBD             1    
		read#VBN             1    
		reais#NN             1    
		record#NN            1    
		recurrence#NN        1    
		recursions#NNS       1    
		report#NN            1    
		report#VBP           1    
		representative#NN    1    
		reside#VB            1    
		reside#VBP           1    
		return#VB            1    
		road#NN              1    
		rule#VB              1    
		savas#NNS            1    
		scalability#NN       1    
		scope#NN             1    
		search#VB            1    
		seed#NN              1    
		seeds#NNS            1    
		select#VBP           1    
		semantic#JJ          1    
		semantics#NNS        1    
		sequential#JJ        1    
		server#NN            1    
		shared#JJ            1    
		shelf#NN             1    
		simplification#NN    1    
		skip#VB              1    
		social#JJ            1    
		sometimes#RB         1    
		soon#RB              1    
		speed#VB             1    
		speedup#NN           1    
		stamp#NN             1    
		stop#NN              1    
		straight#JJ          1    
		suffer#VB            1    
		summarize#VBP        1    
		superstep#NN         1    
		support#VBP          1    
		synthetic#JJ         1    
		target#VBP           1    
		technical#JJ         1    
		tell#VB              1    
		temporary#JJ         1    
		terminology#NN       1    
		thing#NN             1    
		threads#NNS          1    
		tightly#RB           1    
		toolkit#NN           1    
		topological#JJ       1    
		topologically#RB     1    
		trange#NN            1    
		tree#NN              1    
		trend#NN             1    
		trick#NN             1    
		twitter#NN           1    
		uUuP#NN              1    
		unacceptable#JJ      1    
		unaccepted#JJ        1    
		uncovered#JJ         1    
		understanding#NN     1    
		undirected#JJ        1    
		upfront#JJ           1    
		validate#VBP         1    
		validation#NN        1    
		valuable#JJ          1    
		vertex#IN            1    
		vertex#NNP           1    
		visit#NN             1    
		vital#JJ             1    
		walk#NN              1    
		watch#VB             1    
		winner#NN            1    
		workload#NN          1    
		world#NN             1    
		xXyY#NN              1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 11
		Types: 11
		CPUs#NNS             1    
		arccos#FW            1    
		diff#NN              1    
		highmem#NN           1    
		makespan#NN          1    
		parallelization#NN   1    
		plugin#NN            1    
		reais#NN             1    
		recursions#NNS       1    
		superstep#NN         1    
		xXyY#NN              1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 4936/1201
Size of diff: 97

NOT in Collection (FWL):   11
In Collection (FWL):       27
In Collection (FWL + POS): 59

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
CPUs                  |  NNS      |  -1       |  false   |  false         |  false  
arccos                |  FW       |  -1       |  false   |  false         |  false  
diff                  |  NN       |  -1       |  false   |  false         |  false  
highmem               |  NN       |  -1       |  false   |  false         |  false  
makespan              |  NN       |  -1       |  false   |  false         |  false  
parallelization       |  NN       |  -1       |  false   |  false         |  false  
plugin                |  NN       |  -1       |  false   |  false         |  false  
reais                 |  NN       |  -1       |  false   |  false         |  false  
recursions            |  NNS      |  -1       |  false   |  false         |  false  
superstep             |  NN       |  -1       |  false   |  false         |  false  
xXyY                  |  NN       |  -1       |  false   |  false         |  false  

Attribution           |  NNP      |  18093    |  false   |  false         |  true   
Bonding               |  NNP      |  12499    |  false   |  false         |  true   
Commons               |  NNP      |  18807    |  false   |  false         |  true   
Computation           |  NNP      |  20460    |  false   |  false         |  true   
Dong                  |  NNP      |  65501    |  false   |  false         |  true   
Google                |  NNP      |  20790    |  false   |  false         |  true   
Greedy                |  NNP      |  12297    |  false   |  false         |  true   
Heron                 |  NNP      |  20462    |  false   |  false         |  true   
Nodes                 |  NNP      |  13423    |  false   |  false         |  true   
Preprocessing         |  NN       |  82734    |  false   |  false         |  true   
Preprocessing         |  NNP      |  82734    |  false   |  false         |  true   
Query                 |  NNP      |  18048    |  false   |  false         |  true   
Vertex                |  NNP      |  57064    |  false   |  false         |  true   
brute                 |  JJ       |  17982    |  false   |  false         |  true   
categorize            |  NN       |  21745    |  false   |  false         |  true   
computation           |  NNP      |  20460    |  false   |  false         |  true   
decompose             |  VB       |  34047    |  false   |  false         |  true   
empower               |  VB       |  14822    |  false   |  false         |  true   
formalize             |  VB       |  46237    |  false   |  false         |  true   
logistics             |  NNS      |  13221    |  false   |  false         |  true   
populate              |  VB       |  26246    |  false   |  false         |  true   
qUERY                 |  NNP      |  18048    |  false   |  false         |  true   
query                 |  VB       |  18048    |  false   |  false         |  true   
reside                |  VB       |  12767    |  false   |  false         |  true   
semantics             |  NNS      |  23863    |  false   |  false         |  true   
vertex                |  IN       |  57064    |  false   |  false         |  true   
vertex                |  NNP      |  57064    |  false   |  false         |  true   

Acknowledgement       |  NN       |  21165    |  false   |  true          |  true   
Approximation         |  NN       |  21090    |  false   |  true          |  true   
Bonding               |  NN       |  12499    |  false   |  true          |  true   
Compressed            |  JJ       |  17074    |  false   |  true          |  true   
Correctness           |  NN       |  15629    |  false   |  true          |  true   
Greedy                |  JJ       |  12297    |  false   |  true          |  true   
Partition             |  NN       |  14386    |  false   |  true          |  true   
abstraction           |  NN       |  13301    |  false   |  true          |  true   
accumulative          |  JJ       |  62043    |  false   |  true          |  true   
bloom                 |  NN       |  13734    |  false   |  true          |  true   
bonding               |  NN       |  12499    |  false   |  true          |  true   
bytes                 |  NNS      |  27085    |  false   |  true          |  true   
centrality            |  NN       |  17163    |  false   |  true          |  true   
centric               |  JJ       |  70163    |  false   |  true          |  true   
closeness             |  NN       |  14063    |  false   |  true          |  true   
conceptually          |  RB       |  20265    |  false   |  true          |  true   
diffuse               |  JJ       |  18837    |  false   |  true          |  true   
diffusion             |  NN       |  14432    |  false   |  true          |  true   
footprint             |  NN       |  16270    |  false   |  true          |  true   
fore                  |  NN       |  19051    |  false   |  true          |  true   
hash                  |  VBP      |  34291    |  false   |  true          |  true   
heterogeneous         |  JJ       |  17331    |  false   |  true          |  true   
impractical           |  JJ       |  18697    |  false   |  true          |  true   
indispensable         |  JJ       |  12248    |  false   |  true          |  true   
invalid               |  JJ       |  24660    |  false   |  true          |  true   
kernel                |  NN       |  23559    |  false   |  true          |  true   
keywords              |  NNS      |  17233    |  false   |  true          |  true   
locality              |  NN       |  24399    |  false   |  true          |  true   
logistic              |  JJ       |  16295    |  false   |  true          |  true   
lookup                |  NN       |  58588    |  false   |  true          |  true   
manageable            |  JJ       |  14464    |  false   |  true          |  true   
novelty               |  NN       |  11201    |  false   |  true          |  true   
parallelism           |  NN       |  35256    |  false   |  true          |  true   
percolation           |  NN       |  54878    |  false   |  true          |  true   
prominence            |  NN       |  11749    |  false   |  true          |  true   
pruning               |  NN       |  20475    |  false   |  true          |  true   
query                 |  VBP      |  37547    |  false   |  true          |  true   
queue                 |  NN       |  23132    |  false   |  true          |  true   
reachability          |  NN       |  84041    |  false   |  true          |  true   
recurrence            |  NN       |  14843    |  false   |  true          |  true   
reside                |  VBP      |  12767    |  false   |  true          |  true   
scalability           |  NN       |  55742    |  false   |  true          |  true   
semantic              |  JJ       |  12644    |  false   |  true          |  true   
sequential            |  JJ       |  15077    |  false   |  true          |  true   
simplification        |  NN       |  28564    |  false   |  true          |  true   
speedup               |  NN       |  66712    |  false   |  true          |  true   
summarize             |  VBP      |  16727    |  false   |  true          |  true   
terminology           |  NN       |  12433    |  false   |  true          |  true   
toolkit               |  NN       |  43310    |  false   |  true          |  true   
topological           |  JJ       |  46195    |  false   |  true          |  true   
topologically         |  RB       |  81013    |  false   |  true          |  true   
twitter               |  NN       |  11391    |  false   |  true          |  true   
unaccepted            |  JJ       |  96770    |  false   |  true          |  true   
uncovered             |  JJ       |  32203    |  false   |  true          |  true   
undirected            |  JJ       |  58846    |  false   |  true          |  true   
upfront               |  JJ       |  47559    |  false   |  true          |  true   
validate              |  VBP      |  16156    |  false   |  true          |  true   
validation            |  NN       |  13282    |  false   |  true          |  true   
workload              |  NN       |  16901    |  false   |  true          |  true   


Unique words (size: 86, lower case) without POS:
abstraction
accumulative
acknowledgement
approximation
arccos
attribution
bloom
bonding
brute
bytes
categorize
centrality
centric
closeness
commons
compressed
computation
conceptually
correctness
cpus
decompose
diff
diffuse
diffusion
dong
empower
footprint
fore
formalize
google
greedy
hash
heron
heterogeneous
highmem
impractical
indispensable
invalid
kernel
keywords
locality
logistic
logistics
lookup
makespan
manageable
nodes
novelty
parallelism
parallelization
partition
percolation
plugin
populate
preprocessing
prominence
pruning
query
queue
reachability
reais
recurrence
recursions
reside
scalability
semantic
semantics
sequential
simplification
speedup
summarize
superstep
terminology
toolkit
topological
topologically
twitter
unaccepted
uncovered
undirected
upfront
validate
validation
vertex
workload
xxyy

