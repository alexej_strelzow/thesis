
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 48
		Types: 43
		abstraction#NNS      1    
		arbiter#NN           1    
		asynchronously#RB    1    
		buffer#NN            2    
		concurrency#NN       1    
		concurrent#JJ        1    
		correctness#NN       1    
		deadlock#NN          1    
		deadlock#NNS         1    
		debug#               1    
		execute#             1    
		execute#VBP          1    
		handler#NN           1    
		handler#NNS          1    
		identifier#NN        1    
		integer#NN           1    
		interleave#          1    
		internship#NN        1    
		issuer#NN            1    
		issuer#NNS           1    
		keyword#NN           1    
		lifeline#NN          1    
		manifest#            1    
		orient#              1    
		precondition#NN      1    
		precondition#NNS     1    
		predominant#JJ       1    
		prerequisite#NN      1    
		queue#               1    
		queue#NN             1    
		recreate#            1    
		replay#              1    
		replay#NN            2    
		reproduce#           1    
		requester#NN         1    
		scheduler#NN         1    
		scoop#               1    
		scoop#NN             1    
		scoop#VBP            2    
		semantics#           1    
		sequentially#RB      1    
		terminate#           2    
		unlock#              2    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 3
		Types: 3
		intransitive#JJ      1    
		synchronous#JJ       1    
		unrepeatable#JJ      1    
	NOT_IN_USER_MODEL
		Tokens: 196
		Types: 196
		BUFFER#NN            1    
		Bacon#NNP            1    
		CONSUMER#NN          1    
		Chair#NNP            1    
		Check#VB             1    
		Conclusion#NNP       1    
		Concurrent#JJ        1    
		Condition#NN         1    
		Foundation#NNP       1    
		Heisenbug#NN         1    
		INTEGER#NN           1    
		INVESTOR#NN          1    
		Instant#JJ           1    
		Introduction#NN      1    
		Investor#NNP         1    
		Listing#NNP          1    
		Logical#JJ           1    
		MARKET#NN            1    
		National#NNP         1    
		Object#NN            1    
		Object#NNP           1    
		Oriented#NNP         1    
		PRODUCER#NN          1    
		Record#NNP           1    
		Record#VB            1    
		Recording#NNP        1    
		Related#JJ           1    
		Replay#NN            1    
		Replay#NNP           1    
		SCOOP#NN             1    
		SCOOP#VB             1    
		SCOOP#VBP            1    
		Simple#JJ            1    
		Software#NNP         1    
		Swiss#NNP            1    
		Terminate#VB         1    
		Three#CD             1    
		Whenever#NNP         1    
		While#IN             1    
		abstract#JJ          1    
		abstractions#NNS     1    
		approve#VB           1    
		approve#VBP          1    
		arbiter#NN           1    
		asynchronous#JJ      1    
		asynchronously#RB    1    
		award#NN             1    
		backstepping#NN      1    
		backup#NN            1    
		basel#NN             1    
		bases#NNS            1    
		behalf#NN            1    
		body#NN              1    
		buffer#NN            1    
		cause#VBP            1    
		client#NN            1    
		coarse#JJ            1    
		company#NN           1    
		complications#NNS    1    
		concurrency#NN       1    
		consumer#NN          1    
		controlled#JJ        1    
		correctness#NN       1    
		counter#NN           1    
		counter#RB           1    
		counters#NNS         1    
		creation#NN          1    
		deadlock#NN          1    
		deadlocks#NNS        1    
		debugging#NN         1    
		delays#NNS           1    
		detail#NN            1    
		distinction#NN       1    
		else#RB              1    
		empirical#JJ         1    
		empty#VB             1    
		enforce#VB           1    
		entities#NNS         1    
		entity#NN            1    
		execute#VB           1    
		execute#VBP          1    
		execution#NN         1    
		executions#NNS       1    
		exhibit#VBP          1    
		explosion#NN         1    
		fail#VBP             1    
		false#JJ             1    
		familiar#JJ          1    
		file#NN              1    
		forall#NN            1    
		formal#JJ            1    
		future#JJ            1    
		future#NN            1    
		grants#NNS           1    
		handler#NN           1    
		handlers#NNS         1    
		identifier#NN        1    
		influence#VB         1    
		instruction#NN       1    
		instructions#NNS     1    
		integrate#VBP        1    
		interact#VBP         1    
		interactions#NNS     1    
		interleaving#NN      1    
		internship#NN        1    
		investor#NN          1    
		investors#NNS        1    
		issuer#NN            1    
		issuers#NNS          1    
		item#NN              1    
		keep#VB              1    
		keep#VBP             1    
		keyword#NN           1    
		languages#NNS        1    
		later#JJ             1    
		libraries#NNS        1    
		library#NN           1    
		lifeline#NN          1    
		lifetime#NN          1    
		list#NN              1    
		lock#NN              1    
		lock#VB              1    
		locks#NNS            1    
		logical#JJ           1    
		manifest#VB          1    
		market#NN            1    
		markets#NNS          1    
		meantime#NN          1    
		mechanism#NN         1    
		modify#VBP           1    
		necessity#NN         1    
		object#NN            1    
		object#VB            1    
		object#VBP           1    
		occur#VB             1    
		outlook#NN           1    
		overhead#RB          1    
		precondition#NN      1    
		preconditions#NNS    1    
		predominant#JJ       1    
		prerequisite#NN      1    
		probe#NN             1    
		process#VB           1    
		processor#NN         1    
		processors#NNS       1    
		producer#NN          1    
		queue#NN             1    
		queue#VB             1    
		race#NN              1    
		races#NNS            1    
		read#VBD             1    
		read#VBP             1    
		record#NN            1    
		record#VB            1    
		record#VBP           1    
		recorder#NN          1    
		records#NNS          1    
		recreate#VB          1    
		recreate#VBP         1    
		repeatable#JJ        1    
		replay#NN            1    
		reproduce#VB         1    
		request#NN           1    
		request#VBP          1    
		requester#NN         1    
		responsibility#NN    1    
		right#RB             1    
		rise#VB              1    
		root#VBP             1    
		sCOOP#VBP            1    
		satisfiable#JJ       1    
		schedule#NN          1    
		scheduler#NN         1    
		semantics#NNS        1    
		sequentially#RB      1    
		shared#JJ            1    
		some#RB              1    
		sometimes#RB         1    
		soon#RB              1    
		stack#VBP            1    
		stores#NNS           1    
		supplier#NN          1    
		targets#NNS          1    
		terminate#VB         1    
		thank#VBP            1    
		threads#NNS          1    
		transaction#NN       1    
		transitive#JJ        1    
		trigger#NN           1    
		undef#NN             1    
		undefined#JJ         1    
		unlock#NN            1    
		unlock#VB            1    
		unlock#VBP           1    
		value#VBP            1    
		wait#NN              1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 4
		Types: 4
		Heisenbug#NN         1    
		backstepping#NN      1    
		forall#NN            1    
		satisfiable#JJ       1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 1832/541
Size of diff: 55

NOT in Collection (FWL):   4
In Collection (FWL):       15
In Collection (FWL + POS): 36

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Heisenbug             |  NN       |  -1       |  false   |  false         |  false  
backstepping          |  NN       |  -1       |  false   |  false         |  false  
forall                |  NN       |  -1       |  false   |  false         |  false  
satisfiable           |  JJ       |  -1       |  false   |  false         |  false  

Oriented              |  NNP      |  12642    |  false   |  false         |  true   
Replay                |  NNP      |  20147    |  false   |  false         |  true   
SCOOP                 |  VB       |  14238    |  false   |  false         |  true   
Terminate             |  VB       |  17120    |  false   |  false         |  true   
debugging             |  NN       |  47892    |  false   |  false         |  true   
execute               |  VB       |  11038    |  false   |  false         |  true   
interleaving          |  NN       |  86049    |  false   |  false         |  true   
manifest              |  VB       |  14377    |  false   |  false         |  true   
queue                 |  VB       |  23132    |  false   |  false         |  true   
recreate              |  VB       |  18866    |  false   |  false         |  true   
reproduce             |  VB       |  11740    |  false   |  false         |  true   
semantics             |  NNS      |  23863    |  false   |  false         |  true   
terminate             |  VB       |  17120    |  false   |  false         |  true   
unlock                |  NN       |  18148    |  false   |  false         |  true   
unlock                |  VB       |  18148    |  false   |  false         |  true   

BUFFER                |  NN       |  12971    |  false   |  true          |  true   
Concurrent            |  JJ       |  15330    |  false   |  true          |  true   
INTEGER               |  NN       |  35492    |  false   |  true          |  true   
Replay                |  NN       |  20147    |  false   |  true          |  true   
SCOOP                 |  NN       |  14238    |  false   |  true          |  true   
SCOOP                 |  VBP      |  16123    |  false   |  true          |  true   
abstractions          |  NNS      |  21947    |  false   |  true          |  true   
arbiter               |  NN       |  24845    |  false   |  true          |  true   
asynchronous          |  JJ       |  35833    |  false   |  true          |  true   
asynchronously        |  RB       |  93220    |  false   |  true          |  true   
buffer                |  NN       |  12971    |  false   |  true          |  true   
concurrency           |  NN       |  52780    |  false   |  true          |  true   
correctness           |  NN       |  15629    |  false   |  true          |  true   
deadlock              |  NN       |  26575    |  false   |  true          |  true   
deadlocks             |  NNS      |  85630    |  false   |  true          |  true   
execute               |  VBP      |  11038    |  false   |  true          |  true   
handler               |  NN       |  17909    |  false   |  true          |  true   
handlers              |  NNS      |  17797    |  false   |  true          |  true   
identifier            |  NN       |  44439    |  false   |  true          |  true   
internship            |  NN       |  16719    |  false   |  true          |  true   
issuer                |  NN       |  23583    |  false   |  true          |  true   
issuers               |  NNS      |  20836    |  false   |  true          |  true   
keyword               |  NN       |  27500    |  false   |  true          |  true   
lifeline              |  NN       |  20795    |  false   |  true          |  true   
precondition          |  NN       |  25984    |  false   |  true          |  true   
preconditions         |  NNS      |  27437    |  false   |  true          |  true   
predominant           |  JJ       |  15742    |  false   |  true          |  true   
prerequisite          |  NN       |  18987    |  false   |  true          |  true   
queue                 |  NN       |  23132    |  false   |  true          |  true   
repeatable            |  JJ       |  40766    |  false   |  true          |  true   
replay                |  NN       |  20147    |  false   |  true          |  true   
requester             |  NN       |  94966    |  false   |  true          |  true   
sCOOP                 |  VBP      |  16123    |  false   |  true          |  true   
scheduler             |  NN       |  49114    |  false   |  true          |  true   
sequentially          |  RB       |  29428    |  false   |  true          |  true   
transitive            |  JJ       |  55521    |  false   |  true          |  true   


Unique words (size: 45, lower case) without POS:
abstractions
arbiter
asynchronous
asynchronously
backstepping
buffer
concurrency
concurrent
correctness
deadlock
deadlocks
debugging
execute
forall
handler
handlers
heisenbug
identifier
integer
interleaving
internship
issuer
issuers
keyword
lifeline
manifest
oriented
precondition
preconditions
predominant
prerequisite
queue
recreate
repeatable
replay
reproduce
requester
satisfiable
scheduler
scoop
semantics
sequentially
terminate
transitive
unlock

