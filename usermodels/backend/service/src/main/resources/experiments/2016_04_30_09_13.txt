
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 115
		Types: 112
		abstraction#NNS      1    
		actionable#JJ        1    
		amenable#JJ          1    
		applicability#NN     1    
		asymmetry#NN         1    
		asymmetry#NNS        1    
		berry#               1    
		biochemistry#        1    
		bioinformatics#NNS   2    
		blueprint#NN         1    
		broaden#VBP          1    
		canonical#JJ         1    
		categorization#NN    1    
		cavity#NN            1    
		chromatic#JJ         1    
		cognition#NN         1    
		computational#JJ     1    
		contextual#JJ        1    
		cybernetics#NNS      1    
		deathtrap#NN         1    
		dendritic#JJ         1    
		descendant#NN        1    
		dispersion#NN        1    
		divergence#NN        1    
		edit#                1    
		empirically#RB       1    
		energetically#RB     1    
		erratic#JJ           1    
		erroneous#JJ         1    
		externality#NNS      1    
		formulate#           1    
		genomics#            1    
		genomics#NNS         2    
		graphical#JJ         1    
		hardness#NN          1    
		heterogeneity#NN     1    
		heuristics#NNS       1    
		histogram#NN         1    
		impossibility#NN     1    
		inattention#NN       1    
		indispensable#JJ     1    
		inertial#JJ          1    
		inseparable#JJ       1    
		interconnected#JJ    1    
		invoke#VBP           1    
		latency#NN           1    
		lithography#NN       1    
		longstanding#JJ      1    
		lossy#               1    
		macroeconomics#NNS   1    
		macroscopic#JJ       1    
		manifold#NNS         1    
		microeconomic#JJ     1    
		modularity#NN        1    
		monopolistic#JJ      1    
		nano#                1    
		naturalistic#JJ      1    
		neoclassical#JJ      1    
		networked#           1    
		neural#JJ            1    
		neuron#NN            1    
		neuron#NNS           1    
		neuroscience#        1    
		neuroscience#NN      1    
		observational#JJ     1    
		obviate#             1    
		omniscience#NN       1    
		organism#NN          1    
		parsimonious#JJ      1    
		pathology#NN         1    
		percolation#NN       1    
		phenotype#NNS        1    
		phylogeny#NN         1    
		physicist#NNS        1    
		pollination#NN       1    
		polymerase#          1    
		predictive#          1    
		predictive#JJ        1    
		prerequisite#NN      1    
		quantify#            1    
		quantitatively#RB    1    
		rationality#NN       2    
		receptor#NN          1    
		renormalization#NN   1    
		replica#NN           1    
		retrieval#NN         1    
		richness#NN          1    
		rigor#NN             1    
		salient#JJ           1    
		scalar#              1    
		scarcity#NN          1    
		sequencing#NN        1    
		simplification#NN    1    
		spectrometry#NN      1    
		strive#VBP           1    
		substantiate#        1    
		terrestrial#JJ       1    
		theta#               1    
		toil#VBP             1    
		tolerant#JJ          1    
		tradeoff#NNS         1    
		transceiver#NNS      1    
		transduction#NN      1    
		unawareness#NN       1    
		uncharted#JJ         1    
		unconnected#JJ       1    
		unimaginable#JJ      1    
		unimagined#JJ        1    
		utilization#NN       1    
		vagueness#NN         1    
		volatility#NN        1    
		wiener#              1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 7
		Types: 7
		autogenous#JJ        1    
		gnostic#JJ           1    
		indigestible#JJ      1    
		obfuscate#VBP        1    
		symmetrical#JJ       1    
		unfruitful#JJ        1    
		unidirectional#JJ    1    
	NOT_IN_USER_MODEL
		Tokens: 505
		Types: 505
		Arrow#NNP            1    
		Asymmetric#JJ        1    
		Berry#NNP            1    
		Beyond#IN            1    
		Biochemistry#NNP     1    
		Bioinformatics#NNS   1    
		Biology#NN           1    
		Biology#NNP          1    
		Black#JJ             1    
		Brain#NN             1    
		Brains#NNS           1    
		Classical#JJ         1    
		Clinical#JJ          1    
		Cognitive#JJ         1    
		Committee#NNP        1    
		Communication#NNP    1    
		Compression#NNP      1    
		Computational#JJ     1    
		Connections#NNP      1    
		Consciousness#NNP    1    
		Contents#NNP         1    
		Control#NNP          1    
		Control#VBP          1    
		Controller#NN        1    
		DMCs#NNS             1    
		Data#NNP             1    
		Delay#NN             1    
		Development#NN       1    
		Directions#NNP       1    
		Directions#NNPS      1    
		Directions#NNS       1    
		Duffy#NN             1    
		Economic#NNP         1    
		Economics#NNP        1    
		Estimates#NNS        1    
		Evolution#NNP        1    
		Evolutionary#NNP     1    
		External#NNP         1    
		Fields#NNP           1    
		Finance#NNP          1    
		Formal#JJ            1    
		Future#JJ            1    
		Future#NNP           1    
		Genome#NNP           1    
		Genomics#NNP         1    
		Genomics#NNS         1    
		Human#JJ             1    
		Humans#NNS           1    
		Important#JJ         1    
		Institute#NNP        1    
		Internet#NN          1    
		Intersection#NN      1    
		Intersection#NNP     1    
		Investment#NNP       1    
		LANs#NNS             1    
		LDCs#NNS             1    
		LRCs#NNS             1    
		Labs#NNPS            1    
		Learning#NNP         1    
		Levenshtein#NN       1    
		Like#IN              1    
		Limited#JJ           1    
		Limits#NNS           1    
		Locally#RB           1    
		Long#JJ              1    
		MIMO#NN              1    
		Machine#NNP          1    
		Many#JJ              1    
		Mathematical#JJ      1    
		Message#NN           1    
		Molecular#JJ         1    
		Nano#NNP             1    
		National#NNP         1    
		Networked#NNP        1    
		Neuroscience#NNP     1    
		Nevertheless#RB      1    
		Northeastern#NNP     1    
		Northwestern#NNP     1    
		Other#JJ             1    
		Pathology#NN         1    
		Perspective#NNP      1    
		PhDs#NNS             1    
		Physics#NN           1    
		Predictive#JJ        1    
		Predictive#NNP       1    
		Pricing#NN           1    
		Processing#NN        1    
		Processing#NNP       1    
		Rational#JJ          1    
		Rationality#NN       1    
		Reliable#JJ          1    
		Representation#NNP   1    
		Scalar#NNP           1    
		Science#NNP          1    
		Sensory#JJ           1    
		Sims#NNS             1    
		Society#NNP          1    
		Sometimes#RB         1    
		Statistical#JJ       1    
		Theoretical#NNP      1    
		Theta#NNP            1    
		Thousands#NNS        1    
		Tools#NNS            1    
		Traditionally#RB     1    
		Value#NNP            1    
		What#WDT             1    
		Where#WRB            1    
		While#IN             1    
		Wiener#NNP           1    
		abstract#JJ          1    
		abstractions#NNS     1    
		abundant#JJ          1    
		achievability#NN     1    
		acquire#NN           1    
		acquisition#NN       1    
		action#NN            1    
		actionable#JJ        1    
		adapt#VB             1    
		advance#NN           1    
		advent#NN            1    
		adverse#JJ           1    
		agent#NN             1    
		agnostic#JJ          1    
		albeit#IN            1    
		allow#VB             1    
		ambiguity#NN         1    
		ambitious#JJ         1    
		amenable#JJ          1    
		analyses#NNS         1    
		analytical#JJ        1    
		applicability#NN     1    
		architectures#NNS    1    
		areas#NNS            1    
		articulate#VB        1    
		asset#NN             1    
		assets#NNS           1    
		asymmetries#NNS      1    
		asymmetry#NN         1    
		atomic#JJ            1    
		attack#NN            1    
		attempt#NN           1    
		attract#VBP          1    
		averages#NNS         1    
		begin#VBP            1    
		behavioral#JJ        1    
		belief#NN            1    
		believe#VBP          1    
		better#RB            1    
		biases#NNS           1    
		bidirectional#JJ     1    
		billions#NNS         1    
		bioinformatics#NNS   1    
		biological#JJ        1    
		biology#NN           1    
		birthday#NN          1    
		blood#NN             1    
		blueprint#NN         1    
		body#NN              1    
		boundaries#NNS       1    
		brain#NN             1    
		branch#NN            1    
		broad#JJ             1    
		broaden#VBP          1    
		bulk#NN              1    
		burden#NN            1    
		canonical#JJ         1    
		capture#NN           1    
		capture#VBP          1    
		categorization#NN    1    
		cause#NN             1    
		cautiously#RB        1    
		cavity#NN            1    
		celebrated#JJ        1    
		centers#NNS          1    
		century#NN           1    
		chromatic#JJ         1    
		circuit#NN           1    
		cite#VBP             1    
		classic#JJ           1    
		cognition#NN         1    
		committee#NN         1    
		companies#NNS        1    
		comparative#JJ       1    
		compression#NN       1    
		conceptual#JJ        1    
		concern#NN           1    
		connection#NN        1    
		consciousness#NN     1    
		consumer#NN          1    
		consumers#NNS        1    
		contemporary#JJ      1    
		content#NN           1    
		contextual#JJ        1    
		continue#VB          1    
		convertors#NNS       1    
		coordination#NN      1    
		cope#VB              1    
		correlations#NNS     1    
		cybernetics#NNS      1    
		deathtrap#NN         1    
		demand#VB            1    
		demand#VBP           1    
		dendritic#JJ         1    
		depth#NN             1    
		descendant#NN        1    
		desire#NN            1    
		detailed#JJ          1    
		developmental#JJ     1    
		device#NN            1    
		digestible#JJ        1    
		diminish#VB          1    
		discipline#NN        1    
		discoveries#NNS      1    
		disease#NN           1    
		dispersion#NN        1    
		divergence#NN        1    
		drug#NN              1    
		economic#JJ          1    
		economics#NN         1    
		economics#NNS        1    
		economy#NN           1    
		edit#VB              1    
		electrical#JJ        1    
		electronic#JJ        1    
		elucidate#VBP        1    
		elusive#JJ           1    
		empirical#JJ         1    
		empirically#RB       1    
		enable#VB            1    
		endeavor#NN          1    
		energetically#RB     1    
		engineer#NN          1    
		engineering#NN       1    
		erratic#JJ           1    
		erroneous#JJ         1    
		escape#NN            1    
		evolution#NN         1    
		evolutionary#JJ      1    
		exciting#JJ          1    
		experts#NNS          1    
		exploration#NN       1    
		extent#NN            1    
		externalities#NNS    1    
		failures#NNS         1    
		famous#JJ            1    
		fields#NNS           1    
		finance#NN           1    
		fitness#NN           1    
		flavor#NN            1    
		fold#NN              1    
		fold#VB              1    
		formulate#VB         1    
		fragile#JJ           1    
		frequencies#NNS      1    
		fruitful#JJ          1    
		future#JJ            1    
		future#NN            1    
		gambling#NN          1    
		gene#NN              1    
		genes#NNS            1    
		genetic#JJ           1    
		genomics#NNS         1    
		glass#NN             1    
		graphical#JJ         1    
		guidance#NN          1    
		halfway#RB           1    
		hardness#NN          1    
		hazard#NN            1    
		heterogeneity#NN     1    
		heterogeneous#JJ     1    
		heuristics#NNS       1    
		hide#VB              1    
		histogram#NN         1    
		historical#JJ        1    
		historically#RB      1    
		hope#NN              1    
		human#JJ             1    
		humans#NNS           1    
		hypothesis#NN        1    
		ideals#NNS           1    
		identify#VB          1    
		imaginations#NNS     1    
		impact#VB            1    
		impossibility#NN     1    
		inattention#NN       1    
		increasingly#RB      1    
		indispensable#JJ     1    
		industry#NN          1    
		inertial#JJ          1    
		influence#NN         1    
		influential#JJ       1    
		informed#JJ          1    
		innovation#NN        1    
		innovations#NNS      1    
		innovative#JJ        1    
		inseparable#JJ       1    
		insights#NNS         1    
		inspiration#NN       1    
		institutions#NNS     1    
		intellectual#JJ      1    
		interaction#NN       1    
		interactions#NNS     1    
		interconnected#JJ    1    
		interface#NN         1    
		interfaces#NNS       1    
		internal#JJ          1    
		investment#NN        1    
		invoke#VBP           1    
		involve#VB           1    
		justified#JJ         1    
		lDCs#NNS             1    
		latency#NN           1    
		lengths#NNS          1    
		less#CC              1    
		lies#NNS             1    
		life#NN              1    
		light#NN             1    
		lithography#NN       1    
		logical#JJ           1    
		longstanding#JJ      1    
		lossy#NN             1    
		machine#NN           1    
		machines#NNS         1    
		macroeconomics#NNS   1    
		macroscopic#JJ       1    
		manifolds#NNS        1    
		markets#NNS          1    
		massive#JJ           1    
		mathematics#NNS      1    
		matter#VBP           1    
		meanwhile#RB         1    
		measurement#NN       1    
		menus#NNS            1    
		microeconomic#JJ     1    
		microeconomics#NNS   1    
		mining#NN            1    
		modularity#NN        1    
		molecular#JJ         1    
		monetary#JJ          1    
		monopolistic#JJ      1    
		moral#JJ             1    
		motor#NN             1    
		movement#NN          1    
		native#JJ            1    
		naturalistic#JJ      1    
		neoclassical#JJ      1    
		nervous#JJ           1    
		neural#JJ            1    
		neuron#NN            1    
		neurons#NNS          1    
		neuroscience#NN      1    
		nevertheless#RB      1    
		normative#JJ         1    
		observational#JJ     1    
		obviate#VB           1    
		occur#VB             1    
		omniscience#NN       1    
		opportunity#NN       1    
		organ#NN             1    
		organism#NN          1    
		origins#NNS          1    
		paradigm#NN          1    
		parsimonious#JJ      1    
		past#IN              1    
		people#NNS           1    
		percentage#NN        1    
		perception#NN        1    
		percolation#NN       1    
		perhaps#RB           1    
		persistent#JJ        1    
		pervasive#JJ         1    
		phenotypes#NNS       1    
		phylogeny#NN         1    
		physically#RB        1    
		physicists#NNS       1    
		physics#NNS          1    
		play#NN              1    
		play#VBP             1    
		players#NNS          1    
		pollination#NN       1    
		powerful#JJ          1    
		predict#VB           1    
		prediction#NN        1    
		prerequisite#NN      1    
		prevalent#JJ         1    
		price#NN             1    
		pricing#NN           1    
		principles#NNS       1    
		produce#VBP          1    
		production#NN        1    
		prominent#JJ         1    
		promote#VBP          1    
		protein#NN           1    
		quantify#VB          1    
		quantitatively#RB    1    
		question#VBP         1    
		radical#JJ           1    
		range#VBP            1    
		rationality#NN       1    
		receptor#NN          1    
		recovery#NN          1    
		reductions#NNS       1    
		reform#NN            1    
		regression#NN        1    
		regulatory#JJ        1    
		relevance#NN         1    
		renormalization#NN   1    
		repair#NN            1    
		replica#NN           1    
		report#NN            1    
		report#VB            1    
		representations#NNS  1    
		resemble#VB          1    
		resistance#NN        1    
		resolution#NN        1    
		retrieval#NN         1    
		revenue#NN           1    
		reward#NN            1    
		richness#NN          1    
		rigor#NN             1    
		rigorous#JJ          1    
		ripe#JJ              1    
		risk#NN              1    
		roles#NNS            1    
		round#NN             1    
		rules#NNS            1    
		salient#JJ           1    
		scarcity#NN          1    
		school#NN            1    
		science#NN           1    
		sciences#NNS         1    
		scope#NN             1    
		seem#VB              1    
		sensory#JJ           1    
		separations#NNS      1    
		sequencing#NN        1    
		sharpen#VBP          1    
		similarity#NN        1    
		simplification#NN    1    
		soon#RB              1    
		sort#NN              1    
		spectrometry#NN      1    
		spin#NN              1    
		standpoint#NN        1    
		status#NN            1    
		stay#NN              1    
		stores#NNS           1    
		string#NN            1    
		strings#NNS          1    
		strive#VBP           1    
		style#NN             1    
		substantiate#VB      1    
		success#NN           1    
		successful#JJ        1    
		surprising#JJ        1    
		suspect#NN           1    
		tackle#VBP           1    
		technological#JJ     1    
		technology#NN        1    
		telecommunications#NN 1    
		telecommunications#NNS 1    
		temporal#JJ          1    
		terrestrial#JJ       1    
		tests#NNS            1    
		theories#NNS         1    
		timely#JJ            1    
		today#NN             1    
		toil#VBP             1    
		tolerant#JJ          1    
		topics#NNS           1    
		toward#IN            1    
		traces#NNS           1    
		tractability#NN      1    
		tradeoffs#NNS        1    
		transceivers#NNS     1    
		transduction#NN      1    
		transitions#NNS      1    
		translate#VB         1    
		transportation#NN    1    
		trip#NN              1    
		troubling#JJ         1    
		ultimate#JJ          1    
		unaffected#JJ        1    
		unawareness#NN       1    
		uncharted#JJ         1    
		unconnected#JJ       1    
		understanding#NN     1    
		unemployment#NN      1    
		unimaginable#JJ      1    
		unimagined#JJ        1    
		uninformed#JJ        1    
		unreliable#JJ        1    
		unsolved#JJ          1    
		unwanted#JJ          1    
		urgent#JJ            1    
		utilization#NN       1    
		vagueness#NN         1    
		viable#JJ            1    
		video#NN             1    
		viewpoint#NN         1    
		volatility#NN        1    
		welfare#NN           1    
		wideband#NN          1    
		wisdom#NN            1    
		world#NN             1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 8
		Types: 8
		LDCs#NNS             1    
		Levenshtein#NN       1    
		PhDs#NNS             1    
		achievability#NN     1    
		lDCs#NNS             1    
		microeconomics#NNS   1    
		tractability#NN      1    
		wideband#NN          1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 3236/1256
Size of diff: 130

NOT in Collection (FWL):   9
In Collection (FWL):       16
In Collection (FWL + POS): 105

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
LDCs                  |  NNS      |  -1       |  false   |  false         |  false  
Levenshtein           |  NN       |  -1       |  false   |  false         |  false  
PhDs                  |  NNS      |  -1       |  false   |  false         |  false  
achievability         |  NN       |  -1       |  false   |  false         |  false  
lDCs                  |  NNS      |  -1       |  false   |  false         |  false  
microeconomics        |  NNS      |  -1       |  false   |  false         |  false  
polymerases           |  NNS      |  -1       |  false   |  false         |  false  
tractability          |  NN       |  -1       |  false   |  false         |  false  
wideband              |  NN       |  -1       |  false   |  false         |  false  

Berry                 |  NNP      |  18103    |  false   |  false         |  true   
Biochemistry          |  NNP      |  27152    |  false   |  false         |  true   
Genomics              |  NNP      |  30564    |  false   |  false         |  true   
Nano                  |  NNP      |  30727    |  false   |  false         |  true   
Networked             |  NNP      |  25699    |  false   |  false         |  true   
Neuroscience          |  NNP      |  25726    |  false   |  false         |  true   
Predictive            |  NNP      |  12343    |  false   |  false         |  true   
Scalar                |  NNP      |  51211    |  false   |  false         |  true   
Theta                 |  NNP      |  30683    |  false   |  false         |  true   
Wiener                |  NNP      |  55701    |  false   |  false         |  true   
edit                  |  VB       |  15322    |  false   |  false         |  true   
formulate             |  VB       |  14711    |  false   |  false         |  true   
lossy                 |  NN       |  70778    |  false   |  false         |  true   
obviate               |  VB       |  43250    |  false   |  false         |  true   
quantify              |  VB       |  16829    |  false   |  false         |  true   
substantiate          |  VB       |  27223    |  false   |  false         |  true   

Asymmetric            |  JJ       |  32021    |  false   |  true          |  true   
Bioinformatics        |  NNS      |  60896    |  false   |  true          |  true   
Computational         |  JJ       |  16589    |  false   |  true          |  true   
Genomics              |  NNS      |  30564    |  false   |  true          |  true   
Pathology             |  NN       |  14409    |  false   |  true          |  true   
Predictive            |  JJ       |  12343    |  false   |  true          |  true   
Rationality           |  NN       |  15087    |  false   |  true          |  true   
abstractions          |  NNS      |  21947    |  false   |  true          |  true   
actionable            |  JJ       |  39963    |  false   |  true          |  true   
agnostic              |  JJ       |  44959    |  false   |  true          |  true   
amenable              |  JJ       |  22128    |  false   |  true          |  true   
applicability         |  NN       |  22611    |  false   |  true          |  true   
asymmetries           |  NNS      |  47492    |  false   |  true          |  true   
asymmetry             |  NN       |  23096    |  false   |  true          |  true   
bidirectional         |  JJ       |  45682    |  false   |  true          |  true   
bioinformatics        |  NNS      |  60896    |  false   |  true          |  true   
blueprint             |  NN       |  12342    |  false   |  true          |  true   
broaden               |  VBP      |  13871    |  false   |  true          |  true   
canonical             |  JJ       |  18155    |  false   |  true          |  true   
categorization        |  NN       |  22335    |  false   |  true          |  true   
cavity                |  NN       |  12044    |  false   |  true          |  true   
chromatic             |  JJ       |  35310    |  false   |  true          |  true   
cognition             |  NN       |  14950    |  false   |  true          |  true   
contextual            |  JJ       |  13606    |  false   |  true          |  true   
cybernetics           |  NNS      |  52192    |  false   |  true          |  true   
deathtrap             |  NN       |  79650    |  false   |  true          |  true   
dendritic             |  JJ       |  67917    |  false   |  true          |  true   
descendant            |  NN       |  20145    |  false   |  true          |  true   
digestible            |  JJ       |  48914    |  false   |  true          |  true   
dispersion            |  NN       |  23012    |  false   |  true          |  true   
divergence            |  NN       |  21208    |  false   |  true          |  true   
elucidate             |  VBP      |  29759    |  false   |  true          |  true   
empirically           |  RB       |  15235    |  false   |  true          |  true   
energetically         |  RB       |  28774    |  false   |  true          |  true   
erratic               |  JJ       |  14405    |  false   |  true          |  true   
erroneous             |  JJ       |  17698    |  false   |  true          |  true   
externalities         |  NNS      |  31253    |  false   |  true          |  true   
fruitful              |  JJ       |  16004    |  false   |  true          |  true   
genomics              |  NNS      |  30564    |  false   |  true          |  true   
graphical             |  JJ       |  23598    |  false   |  true          |  true   
hardness              |  NN       |  26053    |  false   |  true          |  true   
heterogeneity         |  NN       |  20846    |  false   |  true          |  true   
heterogeneous         |  JJ       |  17331    |  false   |  true          |  true   
heuristics            |  NNS      |  46799    |  false   |  true          |  true   
histogram             |  NN       |  51052    |  false   |  true          |  true   
impossibility         |  NN       |  18450    |  false   |  true          |  true   
inattention           |  NN       |  27737    |  false   |  true          |  true   
indispensable         |  JJ       |  12248    |  false   |  true          |  true   
inertial              |  JJ       |  37880    |  false   |  true          |  true   
inseparable           |  JJ       |  17596    |  false   |  true          |  true   
interconnected        |  JJ       |  22919    |  false   |  true          |  true   
invoke                |  VBP      |  15010    |  false   |  true          |  true   
latency               |  NN       |  27110    |  false   |  true          |  true   
lithography           |  NN       |  30527    |  false   |  true          |  true   
longstanding          |  JJ       |  15038    |  false   |  true          |  true   
macroeconomics        |  NNS      |  60843    |  false   |  true          |  true   
macroscopic           |  JJ       |  36272    |  false   |  true          |  true   
manifolds             |  NNS      |  58542    |  false   |  true          |  true   
microeconomic         |  JJ       |  53291    |  false   |  true          |  true   
modularity            |  NN       |  56016    |  false   |  true          |  true   
monopolistic          |  JJ       |  36828    |  false   |  true          |  true   
naturalistic          |  JJ       |  20574    |  false   |  true          |  true   
neoclassical          |  JJ       |  23582    |  false   |  true          |  true   
neural                |  JJ       |  13448    |  false   |  true          |  true   
neuron                |  NN       |  34657    |  false   |  true          |  true   
neurons               |  NNS      |  14442    |  false   |  true          |  true   
neuroscience          |  NN       |  25726    |  false   |  true          |  true   
observational         |  JJ       |  16841    |  false   |  true          |  true   
omniscience           |  NN       |  50475    |  false   |  true          |  true   
organism              |  NN       |  11590    |  false   |  true          |  true   
parsimonious          |  JJ       |  41231    |  false   |  true          |  true   
percolation           |  NN       |  54878    |  false   |  true          |  true   
phenotypes            |  NNS      |  45938    |  false   |  true          |  true   
phylogeny             |  NN       |  48850    |  false   |  true          |  true   
physicists            |  NNS      |  11413    |  false   |  true          |  true   
pollination           |  NN       |  29813    |  false   |  true          |  true   
prerequisite          |  NN       |  18987    |  false   |  true          |  true   
quantitatively        |  RB       |  32646    |  false   |  true          |  true   
rationality           |  NN       |  15087    |  false   |  true          |  true   
receptor              |  NN       |  20653    |  false   |  true          |  true   
renormalization       |  NN       |  71808    |  false   |  true          |  true   
replica               |  NN       |  15570    |  false   |  true          |  true   
retrieval             |  NN       |  18990    |  false   |  true          |  true   
richness              |  NN       |  11903    |  false   |  true          |  true   
rigor                 |  NN       |  17807    |  false   |  true          |  true   
salient               |  JJ       |  12133    |  false   |  true          |  true   
scarcity              |  NN       |  13366    |  false   |  true          |  true   
sequencing            |  NN       |  17114    |  false   |  true          |  true   
simplification        |  NN       |  28564    |  false   |  true          |  true   
spectrometry          |  NN       |  47135    |  false   |  true          |  true   
strive                |  VBP      |  11183    |  false   |  true          |  true   
terrestrial           |  JJ       |  12272    |  false   |  true          |  true   
toil                  |  VBP      |  33013    |  false   |  true          |  true   
tolerant              |  JJ       |  12442    |  false   |  true          |  true   
tradeoffs             |  NNS      |  28667    |  false   |  true          |  true   
transceivers          |  NNS      |  60014    |  false   |  true          |  true   
transduction          |  NN       |  67927    |  false   |  true          |  true   
unawareness           |  NN       |  68655    |  false   |  true          |  true   
uncharted             |  JJ       |  24006    |  false   |  true          |  true   
unconnected           |  JJ       |  31735    |  false   |  true          |  true   
unimaginable          |  JJ       |  16656    |  false   |  true          |  true   
unimagined            |  JJ       |  51889    |  false   |  true          |  true   
utilization           |  NN       |  12496    |  false   |  true          |  true   
vagueness             |  NN       |  34041    |  false   |  true          |  true   
volatility            |  NN       |  16001    |  false   |  true          |  true   


Unique words (size: 123, lower case) without POS:
abstractions
achievability
actionable
agnostic
amenable
applicability
asymmetric
asymmetries
asymmetry
berry
bidirectional
biochemistry
bioinformatics
blueprint
broaden
canonical
categorization
cavity
chromatic
cognition
computational
contextual
cybernetics
deathtrap
dendritic
descendant
digestible
dispersion
divergence
edit
elucidate
empirically
energetically
erratic
erroneous
externalities
formulate
fruitful
genomics
graphical
hardness
heterogeneity
heterogeneous
heuristics
histogram
impossibility
inattention
indispensable
inertial
inseparable
interconnected
invoke
latency
ldcs
levenshtein
lithography
longstanding
lossy
macroeconomics
macroscopic
manifolds
microeconomic
microeconomics
modularity
monopolistic
nano
naturalistic
neoclassical
networked
neural
neuron
neurons
neuroscience
observational
obviate
omniscience
organism
parsimonious
pathology
percolation
phds
phenotypes
phylogeny
physicists
pollination
polymerases
predictive
prerequisite
quantify
quantitatively
rationality
receptor
renormalization
replica
retrieval
richness
rigor
salient
scalar
scarcity
sequencing
simplification
spectrometry
strive
substantiate
terrestrial
theta
toil
tolerant
tractability
tradeoffs
transceivers
transduction
unawareness
uncharted
unconnected
unimaginable
unimagined
utilization
vagueness
volatility
wideband
wiener

