
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 21
		Types: 21
		abstraction#NN       1    
		attenuation#NN       1    
		bootstrap#NN         1    
		bootstrapping#NN     1    
		centralized#         1    
		dimensionally#RB     1    
		graphical#           1    
		graphical#JJ         1    
		histogram#NN         1    
		inference#           1    
		inference#NNS        1    
		informational#JJ     1    
		initialize#VBP       1    
		iteration#           1    
		locality#NN          1    
		optimality#NN        1    
		pawn#NN              1    
		query#NN             1    
		reside#VBP           1    
		rook#                1    
		triangulation#NN     1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 1
		Types: 1
		unforgiving#JJ       1    
	NOT_IN_USER_MODEL
		Tokens: 83
		Types: 83
		APPROACH#NNP         1    
		COLLABORATIVE#JJ     1    
		COLLABORATIVE#NNP    1    
		Centralized#NNP      1    
		Chess#NNP            1    
		DISCUSSIONS#NNS      1    
		Data#NNS             1    
		Decision#NNP         1    
		Error#NNP            1    
		Errors#NNPS          1    
		Errors#NNS           1    
		GLOBAL#NNP           1    
		GRAPHICAL#NNP        1    
		Graphical#JJ         1    
		Histogram#NN         1    
		INFERENCE#NNP        1    
		Iterations#NNPS      1    
		King#NNP             1    
		MODEL#NNP            1    
		MODELS#NNP           1    
		Pawn#NN              1    
		Poor#NNP             1    
		Potentials#NNS       1    
		Repeat#NN            1    
		Rook#NNP             1    
		SENSOR#NNP           1    
		TRAINING#NN          1    
		TRAINING#NNP         1    
		Test#NNP             1    
		Tree#NNP             1    
		abstraction#NN       1    
		accessible#JJ        1    
		ally#NN              1    
		argmax#FW            1    
		attenuation#NN       1    
		blankets#NNS         1    
		bootstrap#NN         1    
		bootstrapping#NN     1    
		category#NN          1    
		confidential#JJ      1    
		conversion#NN        1    
		cooperate#VBP        1    
		deeply#RB            1    
		dimensionally#RB     1    
		eXPERIMENTS#NNS      1    
		efficacy#NN          1    
		enforce#VB           1    
		fROM#NN              1    
		face#NN              1    
		forgiving#JJ         1    
		fusion#NN            1    
		hands#NNS            1    
		inferences#NNS       1    
		informational#JJ     1    
		initialize#VBP       1    
		king#NN              1    
		lOCAL#NNP            1    
		locality#NN          1    
		manage#VB            1    
		model#VBP            1    
		modis#NN             1    
		optimality#NN        1    
		particles#NNS        1    
		pathe#NN             1    
		peaky#JJ             1    
		perform#VBP          1    
		prob#NN              1    
		prone#JJ             1    
		query#NN             1    
		reside#VBP           1    
		resort#NN            1    
		resort#VBP           1    
		search#VB            1    
		search#VBP           1    
		stft#FW              1    
		stft#NN              1    
		totally#RB           1    
		tracking#NN          1    
		train#VBP            1    
		trained#JJ           1    
		triangulation#NN     1    
		wish#NN              1    
		worthwhile#JJ        1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 3
		Types: 3
		argmax#FW            1    
		stft#FW              1    
		stft#NN              1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 75157/5049
Size of tokens/types in Md: 1574/550
Size of diff: 25

NOT in Collection (FWL):   3
In Collection (FWL):       5
In Collection (FWL + POS): 17

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
argmax                |  FW       |  -1       |  false   |  false         |  false  
stft                  |  FW       |  -1       |  false   |  false         |  false  
stft                  |  NN       |  -1       |  false   |  false         |  false  

Centralized           |  NNP      |  12926    |  false   |  false         |  true   
GRAPHICAL             |  NNP      |  23598    |  false   |  false         |  true   
INFERENCE             |  NNP      |  17088    |  false   |  false         |  true   
Iterations            |  NNPS     |  35132    |  false   |  false         |  true   
Rook                  |  NNP      |  41799    |  false   |  false         |  true   

Graphical             |  JJ       |  23598    |  false   |  true          |  true   
Histogram             |  NN       |  51052    |  false   |  true          |  true   
Pawn                  |  NN       |  22776    |  false   |  true          |  true   
abstraction           |  NN       |  13301    |  false   |  true          |  true   
attenuation           |  NN       |  35659    |  false   |  true          |  true   
bootstrap             |  NN       |  47780    |  false   |  true          |  true   
bootstrapping         |  NN       |  55752    |  false   |  true          |  true   
dimensionally         |  RB       |  88846    |  false   |  true          |  true   
forgiving             |  JJ       |  13676    |  false   |  true          |  true   
inferences            |  NNS      |  19632    |  false   |  true          |  true   
informational         |  JJ       |  18364    |  false   |  true          |  true   
initialize            |  VBP      |  90290    |  false   |  true          |  true   
locality              |  NN       |  24399    |  false   |  true          |  true   
optimality            |  NN       |  59179    |  false   |  true          |  true   
query                 |  NN       |  18048    |  false   |  true          |  true   
reside                |  VBP      |  12767    |  false   |  true          |  true   
triangulation         |  NN       |  30925    |  false   |  true          |  true   


Unique words (size: 23, lower case) without POS:
abstraction
argmax
attenuation
bootstrap
bootstrapping
centralized
dimensionally
forgiving
graphical
histogram
inference
inferences
informational
initialize
iterations
locality
optimality
pawn
query
reside
rook
stft
triangulation

