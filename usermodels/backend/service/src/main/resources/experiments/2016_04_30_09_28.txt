
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 58
		Types: 57
		algebra#NN           1    
		ascent#NN            1    
		bellow#VBP           1    
		brute#               1    
		calculus#NN          1    
		compose#VBP          1    
		compute#             1    
		contrived#JJ         1    
		converging#          1    
		correspond#          1    
		covariance#          1    
		defer#               1    
		denote#              1    
		disentangle#         1    
		empirically#RB       1    
		entail#              1    
		enunciation#NN       1    
		exemplify#           1    
		exposition#NN        1    
		ferromagnetic#JJ     1    
		gradient#NN          1    
		graphical#JJ         1    
		heuristics#NNS       1    
		impractical#JJ       1    
		incoherence#         1    
		incoherence#NN       2    
		indistinguishable#JJ 1    
		ingenious#JJ         1    
		intermediary#NNS     1    
		isomorphic#JJ        1    
		lasso#               1    
		leftmost#            1    
		limp#JJ              1    
		logistic#JJ          1    
		magnetization#NN     1    
		maxi#                1    
		merge#VBP            1    
		neural#              1    
		notation#            1    
		omit#                1    
		parameterization#NN  1    
		portuguese#          1    
		prototypical#JJ      1    
		provable#JJ          1    
		pseudo#              1    
		reconstruct#         1    
		recurrence#NN        1    
		regularization#NN    1    
		replica#NN           1    
		seminal#JJ           1    
		shorthand#NN         1    
		statistic#NN         1    
		suggestive#JJ        1    
		symposium#NN         1    
		undirected#JJ        1    
		uniqueness#NN        1    
		validate#            1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 2
		Types: 2
		unidentifiable#JJ    1    
		unreconstructed#JJ   1    
	NOT_IN_USER_MODEL
		Tokens: 271
		Types: 271
		Basic#JJ             1    
		Begin#NNP            1    
		Boundary#NNP         1    
		Bridge#NN            1    
		CAREER#NNP           1    
		Calculate#VB         1    
		Chain#NNP            1    
		Compute#VB           1    
		Condition#NN         1    
		Configurations#NNS   1    
		Core#NN              1    
		Correlation#NN       1    
		Covariance#NNP       1    
		Curves#NNS           1    
		Despite#IN           1    
		Dmin#NN              1    
		Doctoral#NNP         1    
		Dual#JJ              1    
		EXiM#NN              1    
		Elements#NNS         1    
		Expression#NN        1    
		Failure#NN           1    
		Further#JJ           1    
		Further#RB           1    
		Incoherence#NN       1    
		Incoherence#NNP      1    
		IndD#NN              1    
		Indeed#UH            1    
		Independence#NN      1    
		Introduction#NN      1    
		Just#RB              1    
		Label#NN             1    
		Lasso#NNP            1    
		Learning#NNP         1    
		Likelihoods#NNPS     1    
		Lyapunov#NN          1    
		Negative#JJ          1    
		Neural#NNP           1    
		Notation#NNP         1    
		Notice#NNP           1    
		October#NNP          1    
		Outline#NNP          1    
		PnGL#NN              1    
		Portuguese#NNP       1    
		Processing#NNP       1    
		Pseudo#NNP           1    
		Regression#NN        1    
		Repeat#NN            1    
		Score#NN             1    
		Select#NNP           1    
		Series#NN            1    
		Simple#NN            1    
		Success#NN           1    
		Test#NN              1    
		Their#PRP$           1    
		Thresholding#FW      1    
		Trees#NNP            1    
		about#RB             1    
		accomplish#VB        1    
		advanced#JJ          1    
		algebra#NN           1    
		allow#VB             1    
		ambiguity#NN         1    
		analytic#JJ          1    
		antipodal#JJ         1    
		apart#RB             1    
		arctanh#NN           1    
		areas#NNS            1    
		arguably#RB          1    
		ascent#NN            1    
		atanh#NN             1    
		award#NN             1    
		balance#NN           1    
		ball#NN              1    
		behave#VB            1    
		bellow#VBP           1    
		bias#NN              1    
		bigger#JJR           1    
		bridge#NN            1    
		brute#JJ             1    
		calculus#NN          1    
		care#NN              1    
		child#NN             1    
		choose#VBP           1    
		choosing#NN          1    
		combine#VB           1    
		compose#VBP          1    
		composition#NN       1    
		concentration#NN     1    
		concrete#JJ          1    
		connect#VBP          1    
		connection#NN        1    
		conservative#JJ      1    
		consistency#NN       1    
		const#NN             1    
		contribute#VB        1    
		contrived#JJ         1    
		convention#NN        1    
		converging#NN        1    
		convincing#JJ        1    
		copy#NN              1    
		corrections#NNS      1    
		correlations#NNS     1    
		correspond#VB        1    
		correspondent#NN     1    
		cosh#NN              1    
		cosh#VBP             1    
		counter#NN           1    
		days#NNS             1    
		defer#VB             1    
		denote#NN            1    
		department#NN        1    
		depth#NN             1    
		differ#VB            1    
		disentangle#VB       1    
		disti#NN             1    
		eheh#NN              1    
		empirical#JJ         1    
		empirically#RB       1    
		enough#NN            1    
		ensemble#NN          1    
		entail#VB            1    
		enunciation#NN       1    
		estimates#NNS        1    
		everything#NN        1    
		evolution#NN         1    
		exemplify#VB         1    
		exercise#NN          1    
		exhibit#VBP          1    
		expansions#NNS       1    
		explore#VB           1    
		exposition#NN        1    
		fail#VB              1    
		fail#VBP             1    
		fake#JJ              1    
		fascinating#JJ       1    
		fellowship#NN        1    
		ferromagnetic#JJ     1    
		fields#NNS           1    
		fools#NNS            1    
		force#NN             1    
		formal#JJ            1    
		formulae#NNS         1    
		fusion#NN            1    
		generations#NNS      1    
		glauber#NN           1    
		glue#NN              1    
		gradient#NN          1    
		grant#NN             1    
		graphical#JJ         1    
		grids#NNS            1    
		heuristics#NNS       1    
		identifiable#JJ      1    
		identify#VB          1    
		impractical#JJ       1    
		include#VB           1    
		incoherence#NN       1    
		indistinguishable#JJ 1    
		ingenious#JJ         1    
		instructive#JJ       1    
		interact#VBP         1    
		interaction#NN       1    
		intermediaries#NNS   1    
		intricate#JJ         1    
		irrelevant#JJ        1    
		isomorphic#JJ        1    
		keep#VBP             1    
		leaf#NN              1    
		learning#NN          1    
		left#RB              1    
		leftmost#JJS         1    
		limp#JJ              1    
		logistic#JJ          1    
		machine#NN           1    
		machines#NNS         1    
		magnetization#NN     1    
		marginals#NNS        1    
		match#VBP            1    
		maxi#NN              1    
		maxima#NNS           1    
		maybe#RB             1    
		mean#VBP             1    
		mechanics#NNS        1    
		merge#VBP            1    
		merits#NNS           1    
		minAB#NN             1    
		mina#NN              1    
		mind#NN              1    
		minima#NN            1    
		model#VB             1    
		neighbor#NN          1    
		neighborhood#NN      1    
		neighbors#NNS        1    
		obscure#JJ           1    
		omit#VB              1    
		pages#NNS            1    
		panel#NN             1    
		parameterization#NN  1    
		part#VBP             1    
		performances#NNS     1    
		perimeter#NN         1    
		picture#NN           1    
		predict#VB           1    
		proceeding#NN        1    
		processors#NNS       1    
		produce#VB           1    
		prototypical#JJ      1    
		provable#JJ          1    
		pseudolikelihood#NN  1    
		qualitative#JJ       1    
		question#VBP         1    
		quickly#RB           1    
		recall#NN            1    
		reconstruct#VB       1    
		reconstructed#JJ     1    
		reconstruction#NN    1    
		recurrence#NN        1    
		regression#NN        1    
		regularization#NN    1    
		relevance#NN         1    
		remains#NNS          1    
		replica#NN           1    
		report#NN            1    
		reports#NNS          1    
		right#NN             1    
		rooted#JJ            1    
		scale#VBP            1    
		score#VBP            1    
		search#VB            1    
		search#VBP           1    
		sech#NN              1    
		seminal#JJ           1    
		shorthand#NN         1    
		sinh#NN              1    
		sometimes#RB         1    
		somewhat#RB          1    
		speed#VB             1    
		spin#NN              1    
		spirit#NN            1    
		star#NN              1    
		state#VBP            1    
		statistic#NN         1    
		stress#VBP           1    
		strikes#NNS          1    
		structural#JJ        1    
		success#NN           1    
		successful#JJ        1    
		suggestive#JJ        1    
		symposium#NN         1    
		tables#NNS           1    
		tanh#FW              1    
		tanh#IN              1    
		tanh#NN              1    
		technical#JJ         1    
		temperature#NN       1    
		thanks#NNS           1    
		themselves#PRP       1    
		thick#JJ             1    
		think#VB             1    
		thresholding#NN      1    
		thresholds#NNS       1    
		tree#NN              1    
		understanding#NN     1    
		undirected#JJ        1    
		uniq#NN              1    
		uniqueness#NN        1    
		validate#NN          1    
		vanish#VBP           1    
		violation#NN         1    
		writing#NN           1    
		xixi#NN              1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 14
		Types: 14
		Thresholding#FW      1    
		antipodal#JJ         1    
		arctanh#NN           1    
		atanh#NN             1    
		const#NN             1    
		cosh#NN              1    
		cosh#VBP             1    
		pseudolikelihood#NN  1    
		sech#NN              1    
		sinh#NN              1    
		tanh#FW              1    
		tanh#IN              1    
		tanh#NN              1    
		thresholding#NN      1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 4946/1077
Size of diff: 74

NOT in Collection (FWL):   14
In Collection (FWL):       21
In Collection (FWL + POS): 39

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Thresholding          |  FW       |  -1       |  false   |  false         |  false  
antipodal             |  JJ       |  -1       |  false   |  false         |  false  
arctanh               |  NN       |  -1       |  false   |  false         |  false  
atanh                 |  NN       |  -1       |  false   |  false         |  false  
const                 |  NN       |  -1       |  false   |  false         |  false  
cosh                  |  NN       |  -1       |  false   |  false         |  false  
cosh                  |  VBP      |  -1       |  false   |  false         |  false  
pseudolikelihood      |  NN       |  -1       |  false   |  false         |  false  
sech                  |  NN       |  -1       |  false   |  false         |  false  
sinh                  |  NN       |  -1       |  false   |  false         |  false  
tanh                  |  FW       |  -1       |  false   |  false         |  false  
tanh                  |  IN       |  -1       |  false   |  false         |  false  
tanh                  |  NN       |  -1       |  false   |  false         |  false  
thresholding          |  NN       |  -1       |  false   |  false         |  false  

Compute               |  VB       |  20750    |  false   |  false         |  true   
Covariance            |  NNP      |  30487    |  false   |  false         |  true   
Incoherence           |  NNP      |  40428    |  false   |  false         |  true   
Lasso                 |  NNP      |  40121    |  false   |  false         |  true   
Neural                |  NNP      |  13448    |  false   |  false         |  true   
Notation              |  NNP      |  14619    |  false   |  false         |  true   
Portuguese            |  NNP      |  13798    |  false   |  false         |  true   
Pseudo                |  NNP      |  43916    |  false   |  false         |  true   
brute                 |  JJ       |  17982    |  false   |  false         |  true   
converging            |  NN       |  31649    |  false   |  false         |  true   
correspond            |  VB       |  12753    |  false   |  false         |  true   
defer                 |  VB       |  17709    |  false   |  false         |  true   
denote                |  NN       |  23027    |  false   |  false         |  true   
disentangle           |  VB       |  40964    |  false   |  false         |  true   
entail                |  VB       |  16153    |  false   |  false         |  true   
exemplify             |  VB       |  25262    |  false   |  false         |  true   
leftmost              |  JJS      |  77767    |  false   |  false         |  true   
maxi                  |  NN       |  66142    |  false   |  false         |  true   
omit                  |  VB       |  20295    |  false   |  false         |  true   
reconstruct           |  VB       |  15817    |  false   |  false         |  true   
validate              |  NN       |  16156    |  false   |  false         |  true   

Incoherence           |  NN       |  40428    |  false   |  true          |  true   
algebra               |  NN       |  17236    |  false   |  true          |  true   
ascent                |  NN       |  14396    |  false   |  true          |  true   
bellow                |  VBP      |  46370    |  false   |  true          |  true   
calculus              |  NN       |  17393    |  false   |  true          |  true   
compose               |  VBP      |  13924    |  false   |  true          |  true   
contrived             |  JJ       |  29105    |  false   |  true          |  true   
empirically           |  RB       |  15235    |  false   |  true          |  true   
enunciation           |  NN       |  43804    |  false   |  true          |  true   
exposition            |  NN       |  14594    |  false   |  true          |  true   
ferromagnetic         |  JJ       |  73521    |  false   |  true          |  true   
gradient              |  NN       |  22856    |  false   |  true          |  true   
graphical             |  JJ       |  23598    |  false   |  true          |  true   
heuristics            |  NNS      |  46799    |  false   |  true          |  true   
identifiable          |  JJ       |  14436    |  false   |  true          |  true   
impractical           |  JJ       |  18697    |  false   |  true          |  true   
incoherence           |  NN       |  40428    |  false   |  true          |  true   
indistinguishable     |  JJ       |  17375    |  false   |  true          |  true   
ingenious             |  JJ       |  16065    |  false   |  true          |  true   
intermediaries        |  NNS      |  26693    |  false   |  true          |  true   
isomorphic            |  JJ       |  72829    |  false   |  true          |  true   
limp                  |  JJ       |  16525    |  false   |  true          |  true   
logistic              |  JJ       |  16295    |  false   |  true          |  true   
magnetization         |  NN       |  53535    |  false   |  true          |  true   
merge                 |  VBP      |  12900    |  false   |  true          |  true   
parameterization      |  NN       |  98167    |  false   |  true          |  true   
prototypical          |  JJ       |  31308    |  false   |  true          |  true   
provable              |  JJ       |  53050    |  false   |  true          |  true   
reconstructed         |  JJ       |  33031    |  false   |  true          |  true   
recurrence            |  NN       |  14843    |  false   |  true          |  true   
regularization        |  NN       |  66305    |  false   |  true          |  true   
replica               |  NN       |  15570    |  false   |  true          |  true   
seminal               |  JJ       |  15240    |  false   |  true          |  true   
shorthand             |  NN       |  21231    |  false   |  true          |  true   
statistic             |  NN       |  12821    |  false   |  true          |  true   
suggestive            |  JJ       |  14649    |  false   |  true          |  true   
symposium             |  NN       |  12082    |  false   |  true          |  true   
undirected            |  JJ       |  58846    |  false   |  true          |  true   
uniqueness            |  NN       |  16204    |  false   |  true          |  true   


Unique words (size: 68, lower case) without POS:
algebra
antipodal
arctanh
ascent
atanh
bellow
brute
calculus
compose
compute
const
contrived
converging
correspond
cosh
covariance
defer
denote
disentangle
empirically
entail
enunciation
exemplify
exposition
ferromagnetic
gradient
graphical
heuristics
identifiable
impractical
incoherence
indistinguishable
ingenious
intermediaries
isomorphic
lasso
leftmost
limp
logistic
magnetization
maxi
merge
neural
notation
omit
parameterization
portuguese
prototypical
provable
pseudo
pseudolikelihood
reconstruct
reconstructed
recurrence
regularization
replica
sech
seminal
shorthand
sinh
statistic
suggestive
symposium
tanh
thresholding
undirected
uniqueness
validate

