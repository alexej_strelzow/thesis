
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 52
		Types: 43
		ambush#              1    
		anecdotal#JJ         1    
		archival#JJ          1    
		archive#             2    
		archive#NN           1    
		bio#                 1    
		bio#NNS              1    
		boarder#NN           1    
		byte#NNS             2    
		compress#            1    
		deception#NNS        1    
		decryption#NN        1    
		dispose#             1    
		encrypt#             1    
		encryption#          1    
		encryption#NN        1    
		ethernet#            1    
		ethernet#NN          1    
		firewall#NN          1    
		floppy#              1    
		floppy#JJ            1    
		gigabit#NN           1    
		gigabyte#NN          1    
		innumerable#JJ       1    
		kilogram#NNS         1    
		megabyte#NNS         1    
		motherboard#NN       2    
		plug#                1    
		saturate#            1    
		scanner#NNS          1    
		sneaker#             2    
		sneaker#NN           1    
		spoof#               1    
		stripe#              1    
		stripe#NN            1    
		subscriber#NN        2    
		subsystem#NN         1    
		tariff#NNS           1    
		terabyte#            4    
		terabyte#NN          2    
		terabyte#NNS         1    
		threaded#JJ          1    
		woe#NNS              1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	NOT_IN_USER_MODEL
		Tokens: 245
		Types: 245
		Advanced#NNP         1    
		Archiving#NNP        1    
		Aren#NN              1    
		BIOS#NNP             1    
		BIOS#NNS             1    
		Backup#NN            1    
		Brick#NNP            1    
		Bricks#NNP           1    
		Bytes#NNS            1    
		CIFS#NNS             1    
		Cabinet#NNP          1    
		Card#NNP             1    
		Client#NNP           1    
		Compression#NN       1    
		Context#NNP          1    
		Corporation#NNP      1    
		Cost#NN              1    
		Database#NNP         1    
		Defensive#JJ         1    
		Digital#NNP          1    
		Disk#NN              1    
		Division#NNP         1    
		Encryption#NNP       1    
		Ethernet#NN          1    
		Ethernet#NNP         1    
		Exchange#NNP         1    
		Floppy#NNP           1    
		Generation#NNP       1    
		Gray#NNP             1    
		Huge#JJ              1    
		IOps#NNS             1    
		Inexpensive#JJ       1    
		Internet#NNP         1    
		Iron#NNP             1    
		Item#NNP             1    
		Labor#NNP            1    
		Last#JJ              1    
		Media#NNP            1    
		Memory#NN            1    
		Motherboard#NN       1    
		Mountain#NNP         1    
		Portable#NNP         1    
		Power#NN             1    
		Price#NNP            1    
		Publisher#NNP        1    
		RAID#NN              1    
		Read#NNP             1    
		Read#VB              1    
		Remote#JJ            1    
		Rent#NNP             1    
		Robot#NNP            1    
		Security#NNP         1    
		Send#VB              1    
		Server#NN            1    
		Shipping#NNP         1    
		Sneaker#NNP          1    
		Software#NNP         1    
		Speed#NN             1    
		Still#RB             1    
		Storage#NN           1    
		Subscriber#NN        1    
		Supply#NN            1    
		Suppose#VBP          1    
		Survey#NNP           1    
		Tape#NN              1    
		Tape#NNP             1    
		Taxes#NNS            1    
		Terabyte#NN          1    
		Terabyte#NNP         1    
		Terabytes#NNP        1    
		Thanks#NNS           1    
		Today#NN             1    
		Total#NNP            1    
		Until#IN             1    
		Video#NNP            1    
		Virus#NN             1    
		Viruses#NNS          1    
		Write#NNP            1    
		ambush#VB            1    
		anecdotal#JJ         1    
		answer#VB            1    
		anyone#NN            1    
		archival#JJ          1    
		archive#NN           1    
		archiving#NN         1    
		arrive#VB            1    
		arrive#VBP           1    
		async#NN             1    
		auto#NN              1    
		backup#NN            1    
		bands#NNS            1    
		bill#NN              1    
		board#NN             1    
		boarder#NN           1    
		boot#NN              1    
		boxes#NNS            1    
		brick#NN             1    
		bricks#NNS           1    
		bytes#NNS            1    
		cabinet#NN           1    
		cables#NNS           1    
		card#NN              1    
		carriers#NNS         1    
		claim#VB             1    
		client#NN            1    
		clients#NNS          1    
		colleagues#NNS       1    
		company#NN           1    
		compress#VB          1    
		computers#NNS        1    
		confess#VBP          1    
		controller#NN        1    
		controllers#NNS      1    
		copper#NN            1    
		cost#VB              1    
		cpus#NN              1    
		dangerous#JJ         1    
		deceptions#NNS       1    
		decryption#NN        1    
		defend#VB            1    
		deliver#VB           1    
		deliver#VBP          1    
		desktop#NN           1    
		disk#NN              1    
		dispose#VB           1    
		dollars#NNS          1    
		door#NN              1    
		employer#NN          1    
		encrypt#VB           1    
		encryption#NN        1    
		extras#NNS           1    
		favored#JJ           1    
		file#NN              1    
		files#NNS            1    
		firewall#NN          1    
		floppy#JJ            1    
		foreign#JJ           1    
		gigabit#NN           1    
		gigabyte#NN          1    
		higher#RBR           1    
		home#NN              1    
		host#NN              1    
		hours#NNS            1    
		housing#NN           1    
		huge#JJ              1    
		impressive#JJ        1    
		innumerable#JJ       1    
		install#VB           1    
		integrated#JJ        1    
		internet#NN          1    
		kbps#NNS             1    
		kilograms#NNS        1    
		labor#NN             1    
		magnetic#JJ          1    
		master#NN            1    
		medium#JJ            1    
		megabits#NNS         1    
		megabytes#NNS        1    
		merit#NN             1    
		middle#NN            1    
		minutes#NNS          1    
		motherboard#NN       1    
		move#VB              1    
		nasty#JJ             1    
		organizations#NNS    1    
		outgoing#JJ          1    
		overnight#JJ         1    
		overnight#RB         1    
		passive#JJ           1    
		petabyte#NN          1    
		phone#NN             1    
		plug#VB              1    
		portable#JJ          1    
		pounds#NNS           1    
		preservation#NN      1    
		prevent#VB           1    
		prevent#VBP          1    
		problematic#JJ       1    
		protect#VB           1    
		publisher#NN         1    
		quote#VBP            1    
		raid#NN              1    
		read#NN              1    
		recommend#VB         1    
		respectable#JJ       1    
		restore#VB           1    
		robot#NN             1    
		robots#NNS           1    
		round#JJ             1    
		rumors#NNS           1    
		safe#JJ              1    
		saturate#VB          1    
		scan#VB              1    
		scanners#NNS         1    
		scare#VBP            1    
		seconds#NNS          1    
		secure#JJ            1    
		server#NN            1    
		seven#CD             1    
		ship#NN              1    
		shipping#NN          1    
		ships#NNS            1    
		slave#NN             1    
		slot#NN              1    
		smartcards#NNS       1    
		sneaker#NN           1    
		sneaker#NNP          1    
		speeds#NNS           1    
		spell#VBP            1    
		spoof#VB             1    
		stack#VBP            1    
		staff#NN             1    
		stripe#NN            1    
		striping#NN          1    
		subscriber#NN        1    
		subsystem#NN         1    
		susceptible#JJ       1    
		suspect#VBP          1    
		tape#NN              1    
		tape#VBP             1    
		tapes#NNS            1    
		tariffs#NNS          1    
		taxes#NNS            1    
		terabyte#NN          1    
		terabyte#RB          1    
		terabytes#NNS        1    
		terabytes#RB         1    
		terrible#JJ          1    
		thousand#CD          1    
		threaded#JJ          1    
		threats#NNS          1    
		till#IN              1    
		travel#VB            1    
		trillion#CD          1    
		trust#NN             1    
		trust#VB             1    
		trusts#NNS           1    
		ttcp#NN              1    
		virus#NN             1    
		viruses#NNS          1    
		volts#NNS            1    
		wait#VBP             1    
		woes#NNS             1    
		your#PRP$            1    
		yourself#PRP         1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 6
		Types: 6
		Aren#NN              1    
		async#NN             1    
		cpus#NN              1    
		petabyte#NN          1    
		smartcards#NNS       1    
		ttcp#NN              1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 75157/5049
Size of tokens/types in Md: 1196/598
Size of diff: 58

NOT in Collection (FWL):   6
In Collection (FWL):       20
In Collection (FWL + POS): 32

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Aren                  |  NN       |  -1       |  false   |  false         |  false  
async                 |  NN       |  -1       |  false   |  false         |  false  
cpus                  |  NN       |  -1       |  false   |  false         |  false  
petabyte              |  NN       |  -1       |  false   |  false         |  false  
smartcards            |  NNS      |  -1       |  false   |  false         |  false  
ttcp                  |  NN       |  -1       |  false   |  false         |  false  

Archiving             |  NNP      |  47976    |  false   |  false         |  true   
BIOS                  |  NNP      |  34266    |  false   |  false         |  true   
Encryption            |  NNP      |  21645    |  false   |  false         |  true   
Ethernet              |  NNP      |  43142    |  false   |  false         |  true   
Floppy                |  NNP      |  17333    |  false   |  false         |  true   
Sneaker               |  NNP      |  26614    |  false   |  false         |  true   
Terabyte              |  NNP      |  81725    |  false   |  false         |  true   
Terabytes             |  NNP      |  62025    |  false   |  false         |  true   
ambush                |  VB       |  16407    |  false   |  false         |  true   
archiving             |  NN       |  47976    |  false   |  false         |  true   
compress              |  VB       |  26407    |  false   |  false         |  true   
dispose               |  VB       |  16583    |  false   |  false         |  true   
encrypt               |  VB       |  38651    |  false   |  false         |  true   
plug                  |  VB       |  11303    |  false   |  false         |  true   
saturate              |  VB       |  39931    |  false   |  false         |  true   
sneaker               |  NNP      |  26614    |  false   |  false         |  true   
spoof                 |  VB       |  33688    |  false   |  false         |  true   
striping              |  NN       |  66109    |  false   |  false         |  true   
terabyte              |  RB       |  81725    |  false   |  false         |  true   
terabytes             |  RB       |  62025    |  false   |  false         |  true   

BIOS                  |  NNS      |  34266    |  false   |  true          |  true   
Bytes                 |  NNS      |  27085    |  false   |  true          |  true   
Ethernet              |  NN       |  43142    |  false   |  true          |  true   
Motherboard           |  NN       |  35320    |  false   |  true          |  true   
Subscriber            |  NN       |  22185    |  false   |  true          |  true   
Terabyte              |  NN       |  81725    |  false   |  true          |  true   
anecdotal             |  JJ       |  14062    |  false   |  true          |  true   
archival              |  JJ       |  16956    |  false   |  true          |  true   
archive               |  NN       |  13855    |  false   |  true          |  true   
boarder               |  NN       |  40437    |  false   |  true          |  true   
bytes                 |  NNS      |  27085    |  false   |  true          |  true   
deceptions            |  NNS      |  33393    |  false   |  true          |  true   
decryption            |  NN       |  64130    |  false   |  true          |  true   
encryption            |  NN       |  21645    |  false   |  true          |  true   
firewall              |  NN       |  27825    |  false   |  true          |  true   
floppy                |  JJ       |  17333    |  false   |  true          |  true   
gigabit               |  NN       |  69764    |  false   |  true          |  true   
gigabyte              |  NN       |  58031    |  false   |  true          |  true   
innumerable           |  JJ       |  18507    |  false   |  true          |  true   
kilograms             |  NNS      |  24685    |  false   |  true          |  true   
megabytes             |  NNS      |  32541    |  false   |  true          |  true   
motherboard           |  NN       |  35320    |  false   |  true          |  true   
scanners              |  NNS      |  19705    |  false   |  true          |  true   
sneaker               |  NN       |  26614    |  false   |  true          |  true   
stripe                |  NN       |  18712    |  false   |  true          |  true   
subscriber            |  NN       |  22185    |  false   |  true          |  true   
subsystem             |  NN       |  28087    |  false   |  true          |  true   
tariffs               |  NNS      |  13098    |  false   |  true          |  true   
terabyte              |  NN       |  81725    |  false   |  true          |  true   
terabytes             |  NNS      |  62025    |  false   |  true          |  true   
threaded              |  JJ       |  35557    |  false   |  true          |  true   
woes                  |  NNS      |  12664    |  false   |  true          |  true   


Unique words (size: 43, lower case) without POS:
ambush
anecdotal
archival
archive
archiving
aren
async
bios
boarder
bytes
compress
cpus
deceptions
decryption
dispose
encrypt
encryption
ethernet
firewall
floppy
gigabit
gigabyte
innumerable
kilograms
megabytes
motherboard
petabyte
plug
saturate
scanners
smartcards
sneaker
spoof
stripe
striping
subscriber
subsystem
tariffs
terabyte
terabytes
threaded
ttcp
woes

