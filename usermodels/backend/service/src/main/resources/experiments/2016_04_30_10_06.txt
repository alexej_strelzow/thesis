
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 27
		Types: 26
		align#               1    
		alignment#NN         1    
		attenuation#NN       1    
		distort#VBP          1    
		extractor#NN         1    
		fallacy#NNS          1    
		foreground#NN        1    
		frontal#             1    
		frontal#JJ           2    
		henceforth#RB        1    
		illumination#NN      1    
		impulsive#JJ         1    
		juncture#NN          1    
		liming#              1    
		localized#JJ         1    
		moustache#NN         1    
		neighbour#NNS        1    
		pixel#NN             1    
		pixel#NNS            1    
		prescribed#JJ        1    
		scanner#NNS          1    
		simultaneous#JJ      1    
		template#NN          1    
		tilt#NN              1    
		utilize#VBP          1    
		weighted#            1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 1
		Types: 1
		supervised#JJ        1    
	NOT_IN_USER_MODEL
		Tokens: 121
		Types: 121
		ANALYSIS#NN          1    
		Acquisition#NN       1    
		Acquisition#NNP      1    
		Alignment#NN         1    
		Axis#NNP             1    
		DISCUSSION#NN        1    
		Database#NNP         1    
		FUTURE#NN            1    
		Faces#NNS            1    
		Facial#NNP           1    
		Failures#NNS         1    
		Feature#NN           1    
		Feature#NNP          1    
		Frontal#JJ           1    
		Frontal#NNP          1    
		Generation#NN        1    
		Generation#NNP       1    
		Image#NN             1    
		Laplacian#NN         1    
		Liming#NNP           1    
		Median#JJ            1    
		Mesh#NN              1    
		Normally#RB          1    
		Nose#NNP             1    
		Original#JJ          1    
		Perform#NNP          1    
		Pose#VB              1    
		Registration#NN      1    
		Registration#NNP     1    
		SCOPE#NN             1    
		Smoothing#NN         1    
		Smoothing#NNP        1    
		Step#VB              1    
		Step#VBP             1    
		Success#NN           1    
		Surface#NN           1    
		VIVID#NNP            1    
		Viewpoint#NN         1    
		Vivid#NNP            1    
		Weighted#NNP         1    
		align#VBN            1    
		attenuation#NN       1    
		automatic#JJ         1    
		axes#NNS             1    
		cONCLUSION#NN        1    
		camera#NN            1    
		cause#VBP            1    
		chin#NN              1    
		compensate#VB        1    
		contrast#VBP         1    
		conventional#JJ      1    
		cubic#JJ             1    
		databases#NNS        1    
		deliver#VB           1    
		distort#VBP          1    
		eXPERIMENTAL#JJ      1    
		ears#NNS             1    
		extractor#NN         1    
		eyes#NNS             1    
		face#NN              1    
		face#VBP             1    
		facial#JJ            1    
		fallacies#NNS        1    
		fiducial#JJ          1    
		focal#JJ             1    
		foreground#NN        1    
		frontal#JJ           1    
		hair#NN              1    
		hairs#NNS            1    
		henceforth#RB        1    
		illumination#NN      1    
		impulsive#JJ         1    
		inspired#JJ          1    
		intensity#NN         1    
		juncture#NN          1    
		laser#NN             1    
		lighting#NN          1    
		localized#JJ         1    
		located#JJ           1    
		lter#NN              1    
		mesh#NN              1    
		mesh#VB              1    
		mesh#VBP             1    
		meshes#NNS           1    
		moustache#NN         1    
		mouth#NN             1    
		neck#NN              1    
		neighbours#NNS       1    
		neutral#JJ           1    
		nose#NN              1    
		noses#NNS            1    
		pixel#NN             1    
		pixels#NNS           1    
		pose#VB              1    
		prescribed#JJ        1    
		preservation#NN      1    
		prone#JJ             1    
		rELATED#NN           1    
		rays#NNS             1    
		register#VB          1    
		registration#NN      1    
		repeated#JJ          1    
		rotations#NNS        1    
		scanners#NNS         1    
		scars#NNS            1    
		scene#NN             1    
		secondly#RB          1    
		shadow#NN            1    
		simultaneous#JJ      1    
		spikes#NNS           1    
		stack#VBP            1    
		surface#VBP          1    
		template#NN          1    
		texture#NN           1    
		tilt#NN              1    
		tips#NNS             1    
		triangles#NNS        1    
		unsupervised#JJ      1    
		untreated#JJ         1    
		utilize#VBP          1    
		window#NN            1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 1
		Types: 1
		Laplacian#NN         1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 75157/5049
Size of tokens/types in Md: 1808/588
Size of diff: 29

NOT in Collection (FWL):   1
In Collection (FWL):       4
In Collection (FWL + POS): 24

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Laplacian             |  NN       |  -1       |  false   |  false         |  false  

Frontal               |  NNP      |  14551    |  false   |  false         |  true   
Liming                |  NNP      |  72362    |  false   |  false         |  true   
Weighted              |  NNP      |  19109    |  false   |  false         |  true   
align                 |  VBN      |  15414    |  false   |  false         |  true   

Alignment             |  NN       |  11142    |  false   |  true          |  true   
Frontal               |  JJ       |  14551    |  false   |  true          |  true   
attenuation           |  NN       |  35659    |  false   |  true          |  true   
distort               |  VBP      |  18831    |  false   |  true          |  true   
extractor             |  NN       |  55080    |  false   |  true          |  true   
fallacies             |  NNS      |  46499    |  false   |  true          |  true   
foreground            |  NN       |  12202    |  false   |  true          |  true   
frontal               |  JJ       |  14551    |  false   |  true          |  true   
henceforth            |  RB       |  25562    |  false   |  true          |  true   
illumination          |  NN       |  15837    |  false   |  true          |  true   
impulsive             |  JJ       |  18559    |  false   |  true          |  true   
juncture              |  NN       |  17675    |  false   |  true          |  true   
localized             |  JJ       |  17731    |  false   |  true          |  true   
moustache             |  NN       |  23936    |  false   |  true          |  true   
neighbours            |  NNS      |  37091    |  false   |  true          |  true   
pixel                 |  NN       |  22125    |  false   |  true          |  true   
pixels                |  NNS      |  18580    |  false   |  true          |  true   
prescribed            |  JJ       |  12984    |  false   |  true          |  true   
scanners              |  NNS      |  19705    |  false   |  true          |  true   
simultaneous          |  JJ       |  11403    |  false   |  true          |  true   
template              |  NN       |  14856    |  false   |  true          |  true   
tilt                  |  NN       |  16090    |  false   |  true          |  true   
unsupervised          |  JJ       |  30155    |  false   |  true          |  true   
utilize               |  VBP      |  11420    |  false   |  true          |  true   


Unique words (size: 27, lower case) without POS:
align
alignment
attenuation
distort
extractor
fallacies
foreground
frontal
henceforth
illumination
impulsive
juncture
laplacian
liming
localized
moustache
neighbours
pixel
pixels
prescribed
scanners
simultaneous
template
tilt
unsupervised
utilize
weighted

