
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	NAMED_ENTITY
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_4
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 63
		Types: 58
		aforementioned#JJ    1    
		algorithm#           2    
		alternating#         1    
		annihilate#VBN       1    
		asymptotic#JJ        1    
		attacker#            2    
		class-based#JJ       1    
		coincide#VBP         1    
		complement#          1    
		dataset#             1    
		dichotomy#           1    
		discriminate#VBN     1    
		dynamically#         1    
		formalize#VBN        1    
		formulation#         1    
		hexagonal#JJ         1    
		identically#         1    
		imbalanced#JJ        1    
		implicitly#          1    
		initialize#VBN       1    
		inject#              1    
		inject#VBD           1    
		inject#VBG           1    
		intersect#VBP        1    
		intrusion#           1    
		lasso#               1    
		logistic#            1    
		logistic#JJ          1    
		malicious#JJ         1    
		memorization#        1    
		mete#                1    
		multiplier#          1    
		notation#            1    
		numerically#         1    
		observable#JJ        2    
		penalize#VBZ         1    
		primal#JJ            1    
		real-time#JJ         1    
		realtime#            1    
		reformulate#VBN      1    
		regularization#      1    
		reside#              1    
		residuals#           1    
		separable#JJ         1    
		sequentially#        1    
		slack#               1    
		sparse#              1    
		sparse#JJ            2    
		sparsity#            1    
		stacking#            1    
		supervise#VBN        1    
		surmount#            1    
		toolbox#             1    
		trade-off#           1    
		unlabeled#JJ         1    
		unobservable#JJ      2    
		voltage#             1    
		wellknown#JJ         1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 1
		Types: 1
		multidimensional#JJ  1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 10
		Types: 10
		anderson#NN          1    
		duku#NN              1    
		hyperplane#NN        1    
		hyperplanes#NNS      1    
		i.i.d.#NN            1    
		iii-a#NN             1    
		k-nn#NN              1    
		penalization#NN      1    
		prec#FW              1    
		rudin#NN             1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 44902/5196
Size of tokens/types in Md: 4004/994
Size of diff: 74

NOT in Collection (FWL):   10
In Collection (FWL):       15
In Collection (FWL + POS): 49

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
III-A                 |  NN       |  -1       |  false   |  false         |  false  
anderson              |  NN       |  -1       |  false   |  false         |  false  
duKu                  |  NN       |  -1       |  false   |  false         |  false  
hyperplane            |  NN       |  -1       |  false   |  false         |  false  
hyperplanes           |  NNS      |  -1       |  false   |  false         |  false  
i.i.d.                |  NN       |  -1       |  false   |  false         |  false  
k-NN                  |  NN       |  -1       |  false   |  false         |  false  
penalization          |  NN       |  -1       |  false   |  false         |  false  
prec                  |  FW       |  -1       |  false   |  false         |  false  
rudin                 |  NN       |  -1       |  false   |  false         |  false  

Algorithms            |  NNP      |  15348    |  false   |  false         |  true   
Algorithms            |  NNPS     |  15348    |  false   |  false         |  true   
Alternating           |  NNP      |  16078    |  false   |  false         |  true   
FORMULATION           |  NNP      |  11542    |  false   |  false         |  true   
LASSO                 |  NNP      |  40121    |  false   |  false         |  true   
Logistic              |  NNP      |  16295    |  false   |  false         |  true   
Mete                  |  NNP      |  54716    |  false   |  false         |  true   
Multipliers           |  NNPS     |  58840    |  false   |  false         |  true   
Sparse                |  NNP      |  14364    |  false   |  false         |  true   
datasets              |  NN       |  54849    |  false   |  false         |  true   
inject                |  VB       |  15351    |  false   |  false         |  true   
realtime              |  NN       |  47600    |  false   |  false         |  true   
reside                |  VB       |  12767    |  false   |  false         |  true   
stacking              |  VBG      |  19916    |  false   |  false         |  true   
surmount              |  NN       |  40558    |  false   |  false         |  true   

Observable            |  JJ       |  18459    |  false   |  true          |  true   
Real-time             |  JJ       |  14709    |  false   |  true          |  true   
Sparse                |  JJ       |  14364    |  false   |  true          |  true   
Supervised            |  VBN      |  18124    |  false   |  true          |  true   
Unobservable          |  JJ       |  62770    |  false   |  true          |  true   
aforementioned        |  JJ       |  16177    |  false   |  true          |  true   
annihilated           |  VBN      |  40184    |  false   |  true          |  true   
asymptotic            |  JJ       |  47649    |  false   |  true          |  true   
attacker              |  NN       |  17499    |  false   |  true          |  true   
attackers             |  NNS      |  15752    |  false   |  true          |  true   
class-based           |  JJ       |  50828    |  false   |  true          |  true   
coincide              |  VBP      |  16276    |  false   |  true          |  true   
complement            |  NN       |  15676    |  false   |  true          |  true   
dichotomies           |  NNS      |  40694    |  false   |  true          |  true   
discriminated         |  VBN      |  19970    |  false   |  true          |  true   
dynamically           |  RB       |  34923    |  false   |  true          |  true   
formalized            |  VBN      |  40166    |  false   |  true          |  true   
hexagonal             |  JJ       |  34485    |  false   |  true          |  true   
identically           |  RB       |  35796    |  false   |  true          |  true   
imbalanced            |  JJ       |  48501    |  false   |  true          |  true   
implicitly            |  RB       |  12738    |  false   |  true          |  true   
initialized           |  VBN      |  83928    |  false   |  true          |  true   
injected              |  VBD      |  22463    |  false   |  true          |  true   
injecting             |  VBG      |  18234    |  false   |  true          |  true   
intersect             |  VBP      |  23090    |  false   |  true          |  true   
intrusion             |  NN       |  13843    |  false   |  true          |  true   
logistic              |  JJ       |  16295    |  false   |  true          |  true   
malicious             |  JJ       |  17806    |  false   |  true          |  true   
memorization          |  NN       |  30161    |  false   |  true          |  true   
notation              |  NN       |  14619    |  false   |  true          |  true   
numerically           |  RB       |  31031    |  false   |  true          |  true   
observable            |  JJ       |  18459    |  false   |  true          |  true   
one-dimensional       |  JJ       |  25336    |  false   |  true          |  true   
penalizes             |  VBZ      |  52218    |  false   |  true          |  true   
primal                |  JJ       |  16235    |  false   |  true          |  true   
reformulated          |  VBN      |  47941    |  false   |  true          |  true   
regularization        |  NN       |  66305    |  false   |  true          |  true   
residuals             |  NNS      |  16469    |  false   |  true          |  true   
separable             |  JJ       |  45991    |  false   |  true          |  true   
sequentially          |  RB       |  29428    |  false   |  true          |  true   
slack                 |  NN       |  13129    |  false   |  true          |  true   
sparse                |  JJ       |  14364    |  false   |  true          |  true   
sparsity              |  NN       |  91490    |  false   |  true          |  true   
toolbox               |  NN       |  24508    |  false   |  true          |  true   
trade-off             |  NN       |  20268    |  false   |  true          |  true   
unlabeled             |  JJ       |  53044    |  false   |  true          |  true   
unobservable          |  JJ       |  62770    |  false   |  true          |  true   
voltage               |  NN       |  13608    |  false   |  true          |  true   
wellknown             |  JJ       |  49556    |  false   |  true          |  true   


Unique words (size: 68, lower case) without POS:
aforementioned
algorithms
alternating
anderson
annihilated
asymptotic
attacker
attackers
class-based
coincide
complement
datasets
dichotomies
discriminated
duku
dynamically
formalized
formulation
hexagonal
hyperplane
hyperplanes
i.i.d.
identically
iii-a
imbalanced
implicitly
initialized
inject
injected
injecting
intersect
intrusion
k-nn
lasso
logistic
malicious
memorization
mete
multipliers
notation
numerically
observable
one-dimensional
penalization
penalizes
prec
primal
real-time
realtime
reformulated
regularization
reside
residuals
rudin
separable
sequentially
slack
sparse
sparsity
stacking
supervised
surmount
toolbox
trade-off
unlabeled
unobservable
voltage
wellknown

