
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 20
		Types: 18
		asynchronously#RB    1    
		chap#                1    
		conductance#NN       1    
		contradict#VBP       1    
		convergence#         1    
		convergence#NN       1    
		coupling#NNS         1    
		cuff#                1    
		cuff#NN              1    
		mint#NN              1    
		notation#            1    
		tempting#JJ          1    
		tick#                1    
		undirected#JJ        1    
		vertex#              3    
		walker#NN            1    
		walker#NNS           1    
		weighted#            1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 1
		Types: 1
		synchronous#JJ       1    
	NOT_IN_USER_MODEL
		Tokens: 92
		Types: 92
		ANALYSIS#NN          1    
		Arbitrary#NNP        1    
		Chap#NNP             1    
		Consensus#NNP        1    
		Convergence#NNP      1    
		Cuff#NNP             1    
		Equation#NN          1    
		Fill#VB              1    
		Hidden#NNP           1    
		Hitting#NNP          1    
		Notation#NNP         1    
		Over#IN              1    
		Science#NNP          1    
		Several#JJ           1    
		Static#NNP           1    
		TIME#NN              1    
		Variation#NN         1    
		Vertex#NNP           1    
		Weighted#NNP         1    
		analogy#NN           1    
		assist#VB            1    
		asynchronous#JJ      1    
		asynchronously#RB    1    
		cONVERGENCE#NN       1    
		circuit#NN           1    
		claim#VBP            1    
		clock#NN             1    
		conductance#NN       1    
		connect#VB           1    
		consensus#NN         1    
		consistency#NN       1    
		contact#NN           1    
		contradict#VBP       1    
		coordination#NN      1    
		couplings#NNS        1    
		cuff#NN              1    
		deployment#NN        1    
		describe#VB          1    
		electric#JJ          1    
		exchange#VB          1    
		exchange#VBP         1    
		expected#JJ          1    
		flexibility#NN       1    
		formal#JJ            1    
		fusion#NN            1    
		gossip#NN            1    
		guaranteeing#NN      1    
		insights#NNS         1    
		inspection#NN        1    
		label#VBP            1    
		lazy#JJ              1    
		literatures#NNS      1    
		location#NN          1    
		meet#VB              1    
		meet#VBP             1    
		meeting#NN           1    
		meetings#NNS         1    
		mint#NN              1    
		move#VBP             1    
		moves#NNS            1    
		neighbor#NN          1    
		neighbors#NNS        1    
		passive#JJ           1    
		peer#VB              1    
		peer#VBP             1    
		piiP#NN              1    
		precise#JJ           1    
		precision#NN         1    
		preserved#JJ         1    
		privacy#NN           1    
		remainder#NN         1    
		resistance#NN        1    
		reversible#JJ        1    
		rules#NNS            1    
		stay#VB              1    
		stay#VBP             1    
		suppose#VB           1    
		tempting#JJ          1    
		think#VBP            1    
		tick#VB              1    
		undirected#JJ        1    
		universal#JJ         1    
		update#VB            1    
		vertex#IN            1    
		vertex#NNP           1    
		view#VBP             1    
		voting#NN            1    
		walk#NN              1    
		walk#VBP             1    
		walker#NN            1    
		walkers#NNS          1    
		wrxy#NN              1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 1
		Types: 1
		piiP#NN              1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 1510/474
Size of diff: 22

NOT in Collection (FWL):   1
In Collection (FWL):       9
In Collection (FWL + POS): 12

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
piiP                  |  NN       |  -1       |  false   |  false         |  false  

Chap                  |  NNP      |  21834    |  false   |  false         |  true   
Convergence           |  NNP      |  13374    |  false   |  false         |  true   
Cuff                  |  NNP      |  11447    |  false   |  false         |  true   
Notation              |  NNP      |  14619    |  false   |  false         |  true   
Vertex                |  NNP      |  57064    |  false   |  false         |  true   
Weighted              |  NNP      |  19109    |  false   |  false         |  true   
tick                  |  VB       |  18327    |  false   |  false         |  true   
vertex                |  IN       |  57064    |  false   |  false         |  true   
vertex                |  NNP      |  57064    |  false   |  false         |  true   

asynchronous          |  JJ       |  35833    |  false   |  true          |  true   
asynchronously        |  RB       |  93220    |  false   |  true          |  true   
cONVERGENCE           |  NN       |  13374    |  false   |  true          |  true   
conductance           |  NN       |  58577    |  false   |  true          |  true   
contradict            |  VBP      |  16216    |  false   |  true          |  true   
couplings             |  NNS      |  36838    |  false   |  true          |  true   
cuff                  |  NN       |  11447    |  false   |  true          |  true   
mint                  |  NN       |  12053    |  false   |  true          |  true   
tempting              |  JJ       |  11214    |  false   |  true          |  true   
undirected            |  JJ       |  58846    |  false   |  true          |  true   
walker                |  NN       |  16996    |  false   |  true          |  true   
walkers               |  NNS      |  19976    |  false   |  true          |  true   


Unique words (size: 18, lower case) without POS:
asynchronous
asynchronously
chap
conductance
contradict
convergence
couplings
cuff
mint
notation
piip
tempting
tick
undirected
vertex
walker
walkers
weighted

