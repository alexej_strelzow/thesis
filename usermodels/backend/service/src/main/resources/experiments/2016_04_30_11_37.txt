
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 72
		Types: 70
		accessibility#NN     1    
		aggregate#NN         1    
		algorithm#           1    
		altruism#            1    
		altruistic#          1    
		annotation#NN        1    
		authoritative#JJ     1    
		banquet#NN           1    
		binomial#            1    
		brevity#NN           1    
		budgeting#NN         1    
		careless#JJ          1    
		carelessness#NN      1    
		chile#               1    
		conquer#             1    
		conquer#VBP          1    
		convolution#NN       1    
		defect#NN            1    
		delete#              1    
		discern#VBP          1    
		discerning#JJ        1    
		enhancement#NN       1    
		enlargement#NN       1    
		enroll#              1    
		enroll#VBP           1    
		entail#VBP           1    
		estimation#NN        1    
		extract#NN           1    
		follower#NN          1    
		forwarding#          1    
		greedy#              1    
		gust#NN              1    
		heuristic#           1    
		incur#VBP            1    
		induce#VBP           1    
		inefficient#JJ       1    
		insignificant#JJ     1    
		iphone#              1    
		judgement#NN         1    
		kebab#               1    
		knapsack#            1    
		knapsack#NN          1    
		latent#JJ            1    
		macro#NN             1    
		malfunction#NN       1    
		markup#NN            1    
		merge#VBP            1    
		micro#               2    
		normalize#           1    
		notation#            1    
		parameter#NN         1    
		pruning#             1    
		pruning#NN           1    
		quadratic#JJ         1    
		reconcile#VBP        1    
		recursive#JJ         1    
		redefine#VBP         1    
		resemblance#NN       1    
		reset#               2    
		sarcasm#NN           1    
		spam#NN              1    
		superiority#NN       1    
		terminology#NN       1    
		tong#                1    
		tweet#NN             1    
		tweet#VBP            1    
		username#NN          1    
		username#NNS         1    
		utilize#VBP          1    
		validate#            1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 2
		Types: 2
		decelerate#VBP       1    
		egoistic#JJ          1    
	NOT_IN_USER_MODEL
		Tokens: 279
		Types: 279
		Account#NNP          1    
		Algorithm#FW         1    
		Altruism#NNP         1    
		Altruistic#NNP       1    
		Anyone#NN            1    
		August#NNP           1    
		Bases#NNS            1    
		Binomial#NNP         1    
		Budget#NNP           1    
		Calculation#NN       1    
		Candidate#NNP        1    
		Carelessness#NN      1    
		Chain#NNP            1    
		Chile#NNP            1    
		Clear#NNP            1    
		Conquer#NNP          1    
		Convolution#NN       1    
		Copy#VB              1    
		Crowd#NN             1    
		DEFINITION#NN        1    
		Decision#NN          1    
		Delete#VB            1    
		Details#NNS          1    
		Divide#VB            1    
		ESTIMATION#NN        1    
		Effectiveness#NN     1    
		Effectiveness#NNP    1    
		Efficiency#NN        1    
		Endowment#NNP        1    
		Estimate#NNP         1    
		Expert#NNP           1    
		Extract#NN           1    
		Fast#JJ              1    
		Formation#NN         1    
		Framework#NNP        1    
		Fundamental#JJ       1    
		Genome#NNP           1    
		Grand#NNP            1    
		Greedy#NNP           1    
		Ground#NN            1    
		HITS#NN              1    
		HITS#NNP             1    
		Heuristic#NNP        1    
		Individual#NN        1    
		Individual#NNP       1    
		Integrated#NNP       1    
		International#JJ     1    
		JERs#NN              1    
		Jurors#NNP           1    
		Jury#NN              1    
		Jury#NNP             1    
		Kebab#NNP            1    
		Knapsack#NN          1    
		Knapsack#NNP         1    
		Majority#NNP         1    
		Micro#NNP            1    
		Minorities#NNS       1    
		Minority#NNP         1    
		Monotonicity#NN      1    
		Motivation#NNP       1    
		Normalize#NNP        1    
		Notations#NNPS       1    
		Overview#NNP         1    
		Panagiotis#NN        1    
		Permission#NN        1    
		Prec#NN              1    
		Proceedings#NNP      1    
		Program#NNP          1    
		Programming#NNP      1    
		Pruning#NN           1    
		Pruning#NNP          1    
		Quadratic#JJ         1    
		Quality#NNP          1    
		Ranking#NN           1    
		Reset#NN             1    
		Reset#NNP            1    
		SELECTION#NN         1    
		Scheme#NNP           1    
		Services#NNPS        1    
		Size#NN              1    
		Sketch#VB            1    
		Such#JJ              1    
		Summary#NNP          1    
		Synthetic#NNP        1    
		Team#NNP             1    
		Tong#NNP             1    
		Total#NNP            1    
		Traits#NNS           1    
		Transform#NN         1    
		Very#RB              1    
		Water#NNP            1    
		What#WP              1    
		Whom#WP              1    
		Will#MD              1    
		Windows#NNP          1    
		accelerate#VBP       1    
		accept#VBP           1    
		accessibility#NN     1    
		adequate#JJ          1    
		aggregate#NN         1    
		alone#RB             1    
		altruistic#JJ        1    
		anchoring#NN         1    
		annotation#NN        1    
		array#NN             1    
		assertion#NN         1    
		attend#VB            1    
		authoritative#JJ     1    
		automatic#JJ         1    
		banquet#NN           1    
		blog#NN              1    
		blog#NNP             1    
		brevity#NN           1    
		budget#NN            1    
		budgeting#NN         1    
		budgets#NNS          1    
		bunch#NN             1    
		button#NN            1    
		calculation#NN       1    
		careless#JJ          1    
		celebrities#NNS      1    
		characters#NNS       1    
		citizens#NNS         1    
		cleaning#NN          1    
		come#VB              1    
		comply#VB            1    
		computers#NNS        1    
		conceptual#JJ        1    
		conduct#VB           1    
		conquer#VBP          1    
		cost#VBD             1    
		court#NN             1    
		creative#JJ          1    
		creativity#NN        1    
		credible#JJ          1    
		credits#NNS          1    
		crowd#VBP            1    
		crowds#NNS           1    
		culture#NN           1    
		defect#NN            1    
		differentiate#VBP    1    
		disasters#NNS        1    
		discern#VBP          1    
		discerning#JJ        1    
		document#NN          1    
		eVALUATION#NN        1    
		emails#NNS           1    
		enhance#VBP          1    
		enhancement#NN       1    
		enlargement#NN       1    
		enroll#VB            1    
		enroll#VBP           1    
		entail#VBP           1    
		experienced#JJ       1    
		extremes#NNS         1    
		feel#VBP             1    
		financial#JJ         1    
		findings#NNS         1    
		flat#JJ              1    
		follower#NN          1    
		followers#NNS        1    
		forwarding#NN        1    
		genome#NN            1    
		glorious#JJ          1    
		gust#NN              1    
		humans#NNS           1    
		iPhone#NNP           1    
		imply#VB             1    
		incentive#NN         1    
		income#NN            1    
		incur#VBP            1    
		induce#VBP           1    
		inefficient#JJ       1    
		influential#JJ       1    
		insignificant#JJ     1    
		intellectual#JJ      1    
		intelligence#NN      1    
		intrinsic#JJ         1    
		jERs#NN              1    
		jURY#NN              1    
		judgement#NN         1    
		juries#NNS           1    
		jurisdiction#NN      1    
		juror#NN             1    
		jurors#NNS           1    
		jury#NN              1    
		latent#JJ            1    
		legal#JJ             1    
		legend#NN            1    
		linear#NN            1    
		lower#RBR            1    
		lowest#JJS           1    
		macro#NN             1    
		magic#JJ             1    
		mainstream#NN        1    
		malfunction#NN       1    
		market#NN            1    
		markets#NNS          1    
		markup#NN            1    
		maxmin#NN            1    
		meanwhile#RB         1    
		merge#VBP            1    
		micro#JJ             1    
		middle#JJ            1    
		mistaken#JJ          1    
		mistakes#NNS         1    
		monetary#JJ          1    
		monotonicity#NN      1    
		motivated#JJ         1    
		multimedia#NNS       1    
		news#NN              1    
		niche#NN             1    
		optional#JJ          1    
		organizations#NNS    1    
		organize#VB          1    
		overhead#JJ          1    
		overview#NN          1    
		pARAMETER#NN         1    
		pace#NN              1    
		participate#VB       1    
		participate#VBP      1    
		participation#NN     1    
		payments#NNS         1    
		pervasive#JJ         1    
		plenty#RB            1    
		prevailing#JJ        1    
		produce#VBP          1    
		productivity#NN      1    
		promise#VBP          1    
		promote#VB           1    
		propose#VB           1    
		prototype#NN         1    
		public#JJ            1    
		publish#VB           1    
		reconcile#VBP        1    
		recursive#JJ         1    
		redefine#VBP         1    
		reduce#VBP           1    
		registration#NN      1    
		reluctant#JJ         1    
		resemblance#NN       1    
		retrieve#VB          1    
		retrieve#VBP         1    
		return#VBP           1    
		rhetoric#NN          1    
		rise#NN              1    
		rumors#NNS           1    
		running#JJ           1    
		sarcasm#NN           1    
		select#VB            1    
		shoulder#NN          1    
		skills#NNS           1    
		sort#VB              1    
		spam#NN              1    
		spend#VB             1    
		split#VB             1    
		spread#VBN           1    
		subjective#JJ        1    
		succeed#VB           1    
		superiority#NN       1    
		synthesis#NN         1    
		taste#NN             1    
		terminology#NN       1    
		thinking#NN          1    
		thoughts#NNS         1    
		tide#NN              1    
		topics#NNS           1    
		track#VBP            1    
		traits#NNS           1    
		trial#NN             1    
		tweet#NN             1    
		tweet#VBP            1    
		username#NN          1    
		usernames#NNS        1    
		utilize#VBP          1    
		validate#VB          1    
		virtual#JJ           1    
		vote#VB              1    
		wisdom#NN            1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 3
		Types: 3
		Monotonicity#NN      1    
		maxmin#NN            1    
		monotonicity#NN      1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 60685/4676
Size of tokens/types in Md: 4260/1164
Size of diff: 77

NOT in Collection (FWL):   3
In Collection (FWL):       23
In Collection (FWL + POS): 51

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Monotonicity          |  NN       |  -1       |  false   |  false         |  false  
maxmin                |  NN       |  -1       |  false   |  false         |  false  
monotonicity          |  NN       |  -1       |  false   |  false         |  false  

Algorithm             |  FW       |  12149    |  false   |  false         |  true   
Altruism              |  NNP      |  20887    |  false   |  false         |  true   
Altruistic            |  NNP      |  22317    |  false   |  false         |  true   
Binomial              |  NNP      |  45112    |  false   |  false         |  true   
Chile                 |  NNP      |  13656    |  false   |  false         |  true   
Conquer               |  NNP      |  13829    |  false   |  false         |  true   
Delete                |  VB       |  20116    |  false   |  false         |  true   
Greedy                |  NNP      |  12297    |  false   |  false         |  true   
Heuristic             |  NNP      |  27992    |  false   |  false         |  true   
Kebab                 |  NNP      |  51187    |  false   |  false         |  true   
Knapsack              |  NNP      |  27189    |  false   |  false         |  true   
Micro                 |  NNP      |  14795    |  false   |  false         |  true   
Normalize             |  NNP      |  30154    |  false   |  false         |  true   
Notations             |  NNPS     |  33644    |  false   |  false         |  true   
Pruning               |  NNP      |  20475    |  false   |  false         |  true   
Reset                 |  NN       |  23917    |  false   |  false         |  true   
Reset                 |  NNP      |  23917    |  false   |  false         |  true   
Tong                  |  NNP      |  46528    |  false   |  false         |  true   
enroll                |  VB       |  12883    |  false   |  false         |  true   
forwarding            |  NN       |  37662    |  false   |  false         |  true   
iPhone                |  NNP      |  15858    |  false   |  false         |  true   
micro                 |  JJ       |  14795    |  false   |  false         |  true   
validate              |  VB       |  16156    |  false   |  false         |  true   

Carelessness          |  NN       |  28722    |  false   |  true          |  true   
Convolution           |  NN       |  81992    |  false   |  true          |  true   
ESTIMATION            |  NN       |  13806    |  false   |  true          |  true   
Extract               |  NN       |  10465    |  false   |  true          |  true   
Knapsack              |  NN       |  27189    |  false   |  true          |  true   
Pruning               |  NN       |  20475    |  false   |  true          |  true   
Quadratic             |  JJ       |  38805    |  false   |  true          |  true   
accelerate            |  VBP      |  11078    |  false   |  true          |  true   
accessibility         |  NN       |  13737    |  false   |  true          |  true   
aggregate             |  NN       |  24213    |  false   |  true          |  true   
altruistic            |  JJ       |  22317    |  false   |  true          |  true   
annotation            |  NN       |  49991    |  false   |  true          |  true   
authoritative         |  JJ       |  11999    |  false   |  true          |  true   
banquet               |  NN       |  13214    |  false   |  true          |  true   
brevity               |  NN       |  30025    |  false   |  true          |  true   
budgeting             |  NN       |  23780    |  false   |  true          |  true   
careless              |  JJ       |  13526    |  false   |  true          |  true   
conquer               |  VBP      |  13829    |  false   |  true          |  true   
defect                |  NN       |  13717    |  false   |  true          |  true   
discern               |  VBP      |  12602    |  false   |  true          |  true   
discerning            |  JJ       |  21621    |  false   |  true          |  true   
enhancement           |  NN       |  12027    |  false   |  true          |  true   
enlargement           |  NN       |  20590    |  false   |  true          |  true   
enroll                |  VBP      |  12883    |  false   |  true          |  true   
entail                |  VBP      |  16153    |  false   |  true          |  true   
follower              |  NN       |  24209    |  false   |  true          |  true   
gust                  |  NN       |  19698    |  false   |  true          |  true   
incur                 |  VBP      |  23224    |  false   |  true          |  true   
induce                |  VBP      |  12005    |  false   |  true          |  true   
inefficient           |  JJ       |  13116    |  false   |  true          |  true   
insignificant         |  JJ       |  12307    |  false   |  true          |  true   
judgement             |  NN       |  23831    |  false   |  true          |  true   
latent                |  JJ       |  14303    |  false   |  true          |  true   
macro                 |  NN       |  30266    |  false   |  true          |  true   
malfunction           |  NN       |  24724    |  false   |  true          |  true   
markup                |  NN       |  33387    |  false   |  true          |  true   
merge                 |  VBP      |  12900    |  false   |  true          |  true   
pARAMETER             |  NN       |  17585    |  false   |  true          |  true   
reconcile             |  VBP      |  11956    |  false   |  true          |  true   
recursive             |  JJ       |  31985    |  false   |  true          |  true   
redefine              |  VBP      |  17414    |  false   |  true          |  true   
resemblance           |  NN       |  10627    |  false   |  true          |  true   
sarcasm               |  NN       |  18619    |  false   |  true          |  true   
spam                  |  NN       |  16844    |  false   |  true          |  true   
superiority           |  NN       |  10424    |  false   |  true          |  true   
terminology           |  NN       |  12433    |  false   |  true          |  true   
tweet                 |  NN       |  37934    |  false   |  true          |  true   
tweet                 |  VBP      |  35601    |  false   |  true          |  true   
username              |  NN       |  80159    |  false   |  true          |  true   
usernames             |  NNS      |  96026    |  false   |  true          |  true   
utilize               |  VBP      |  11420    |  false   |  true          |  true   


Unique words (size: 68, lower case) without POS:
accelerate
accessibility
aggregate
algorithm
altruism
altruistic
annotation
authoritative
banquet
binomial
brevity
budgeting
careless
carelessness
chile
conquer
convolution
defect
delete
discern
discerning
enhancement
enlargement
enroll
entail
estimation
extract
follower
forwarding
greedy
gust
heuristic
incur
induce
inefficient
insignificant
iphone
judgement
kebab
knapsack
latent
macro
malfunction
markup
maxmin
merge
micro
monotonicity
normalize
notations
parameter
pruning
quadratic
reconcile
recursive
redefine
resemblance
reset
sarcasm
spam
superiority
terminology
tong
tweet
username
usernames
utilize
validate

