
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 129
		Types: 120
		ablation#NN          2    
		actionable#JJ        1    
		adjuvant#JJ          1    
		aggressiveness#NN    1    
		anatomic#JJ          1    
		androgen#NN          1    
		anterior#JJ          1    
		antigen#NN           2    
		apical#JJ            1    
		axial#JJ             1    
		biochemical#JJ       2    
		biopsy#              1    
		biopsy#NN            2    
		biopsy#NNS           2    
		brachytherapy#       1    
		capsule#NN           1    
		caucasian#JJ         1    
		clinician#NN         1    
		clinician#NNS        1    
		coil#                1    
		coil#NN              1    
		coil#NNS             1    
		comorbidities#NNS    1    
		confer#VBP           1    
		confirmatory#JJ      1    
		contour#NNS          1    
		contraindication#NNS 1    
		defer#               1    
		delineation#NN       1    
		deprivation#NN       1    
		diffusion#NN         1    
		discontinuation#NN   1    
		distorted#JJ         1    
		embark#VBP           1    
		enhancement#NN       1    
		equivocal#JJ         1    
		erectile#JJ          1    
		evasive#JJ           1    
		excision#NN          1    
		facile#JJ            1    
		fossa#NN             1    
		fuse#                1    
		gadolinium#NN        1    
		gantry#NN            1    
		glandular#JJ         1    
		granulation#NN       1    
		hemorrhage#NN        1    
		histopathology#NN    1    
		hotspot#NN           1    
		hyperplasia#NN       1    
		hypertension#NN      1    
		inappropriately#RB   1    
		inconvenience#NN     1    
		indolent#JJ          1    
		instrumentation#     1    
		intravenous#JJ       1    
		invasiveness#NN      1    
		kinetics#            1    
		laterality#NN        1    
		lesion#NN            1    
		lesion#NNS           2    
		localization#NN      1    
		localize#            1    
		longevity#NN         1    
		lymph#NN             1    
		malignancy#NN        1    
		mapping#NN           1    
		metastatic#JJ        1    
		microscopic#JJ       1    
		midline#NN           1    
		modality#NNS         1    
		oncologist#NNS       1    
		oncology#            1    
		overdiagnosis#NN     1    
		overtreatment#NN     1    
		parenchyma#NN        1    
		pathologic#JJ        1    
		pathology#NN         1    
		pinto#               1    
		portend#VBP          1    
		posterior#JJ         1    
		postoperative#JJ     1    
		preclude#            1    
		predictive#JJ        1    
		predominant#JJ       1    
		prognosis#NN         1    
		prostatectomy#NN     1    
		prostatic#JJ         1    
		prostatitis#NN       1    
		protracted#JJ        1    
		rad#                 2    
		radiology#           1    
		rectal#              1    
		rectal#JJ            2    
		recurrence#NN        1    
		recurrent#JJ         1    
		robotic#JJ           1    
		salami#              1    
		salvage#NN           1    
		scanner#NN           1    
		scanner#NNS          1    
		sepsis#NN            1    
		serum#NN             1    
		specificity#NN       1    
		staging#NN           1    
		stroma#NN            1    
		suggestive#JJ        1    
		surrogate#           1    
		tailor#VBP           1    
		tomography#NN        1    
		transducer#          1    
		ultrasound#          1    
		ultrasound#NN        2    
		undetectable#JJ      1    
		upgrade#             1    
		urethra#NN           1    
		urinary#             1    
		urologic#            1    
		vesicle#NN           1    
		washout#NN           1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 4
		Types: 4
		incurable#JJ         1    
		reversible#JJ        1    
		unassisted#JJ        1    
		untraceable#JJ       1    
	NOT_IN_USER_MODEL
		Tokens: 440
		Types: 440
		ADCs#NNS             1    
		Ablation#NN          1    
		Active#JJ            1    
		African#JJ           1    
		Agreement#NNP        1    
		American#JJ          1    
		American#NNP         1    
		Antigen#NN           1    
		Approximately#RB     1    
		Aside#RB             1    
		Assessment#NNP       1    
		Biochemical#JJ       1    
		Biopsies#NNS         1    
		Biopsy#NN            1    
		Biopsy#NNP           1    
		Brachytherapy#NNP    1    
		CANCER#NNP           1    
		COIL#NNP             1    
		Cancer#NN            1    
		Cancer#NNP           1    
		Caucasian#JJ         1    
		Charitable#JJ        1    
		Chief#NNP            1    
		Clinical#JJ          1    
		Clinician#NN         1    
		Clinicians#NNS       1    
		College#NNP          1    
		Company#NNP          1    
		Cooperative#NNP      1    
		Decision#NNP         1    
		Decisions#NNS        1    
		Definitive#JJ        1    
		Dental#NNP           1    
		Development#NNP      1    
		Device#NNP           1    
		Director#NNP         1    
		Distinct#JJ          1    
		Distribution#NN      1    
		Drive#VB             1    
		Duke#NNP             1    
		Elevated#JJ          1    
		Epstein#NN           1    
		Examination#NNP      1    
		FIGURE#NN            1    
		Fusing#NNP           1    
		Fusion#NN            1    
		Guide#NNP            1    
		HIGH#NN              1    
		Health#NNP           1    
		High#JJ              1    
		Image#NN             1    
		Images#NNPS          1    
		Imaging#NNP          1    
		Impact#NN            1    
		Initial#JJ           1    
		Institutes#NNP       1    
		Instrumentation#NNP  1    
		Integrated#NNP       1    
		Lesions#NNS          1    
		Level#NN             1    
		Long#JJ              1    
		MONTH#NN             1    
		MONTH#NNP            1    
		Magnetic#JJ          1    
		Male#NNP             1    
		Mapping#NN           1    
		Medical#NNP          1    
		Molecular#JJ         1    
		Navigation#NNP       1    
		Needle#NNP           1    
		Negative#JJ          1    
		Oncology#NNP         1    
		Patent#NN            1    
		Patient#NN           1    
		Patient#NNP          1    
		Patients#NNS         1    
		Populations#NNPS     1    
		Populations#NNS      1    
		Predicting#NNP       1    
		Preventive#NNP       1    
		Previous#JJ          1    
		Procedure#NNP        1    
		Program#NNP          1    
		Prostate#JJ          1    
		Prostate#NN          1    
		Prostate#NNP         1    
		RADS#NN              1    
		RADS#NNPS            1    
		Radiation#NNP        1    
		Radiology#NNP        1    
		Real#NNP             1    
		Rectal#JJ            1    
		Rectal#NNP           1    
		Recurrence#NN        1    
		Recurrent#JJ         1    
		Repeat#NN            1    
		Reporting#NNP        1    
		Resonance#NN         1    
		Resonance#NNP        1    
		Rise#NN              1    
		Risk#NN              1    
		Risk#NNP             1    
		Room#NN              1    
		Salami#NNP           1    
		Scholars#NNPS        1    
		Specific#JJ          1    
		Staff#NN             1    
		States#NNPS          1    
		Surgery#NNP          1    
		Surgical#NNP         1    
		Symptoms#NNPS        1    
		TABLE#VBP            1    
		TRUS#NN              1    
		TRUS#NNS             1    
		Therapy#NN           1    
		Trackable#JJ         1    
		Tract#NNP            1    
		Transducer#NNP       1    
		Treatment#NN         1    
		Treatment#NNP        1    
		Tumors#NNS           1    
		Ultrasound#NN        1    
		Ultrasound#NNP       1    
		Undergo#NNP          1    
		United#NNP           1    
		Urinary#NNP          1    
		Urologic#NNP         1    
		Utility#NN           1    
		VALUE#NN             1    
		VOLUME#NNP           1    
		Version#NNP          1    
		WITH#IN              1    
		Year#NN              1    
		ablation#NN          1    
		abnormal#JJ          1    
		acquisition#NN       1    
		actionable#JJ        1    
		adjuvant#JJ          1    
		adverse#JJ           1    
		advocate#VBP         1    
		ages#NNS             1    
		aggressive#JJ        1    
		aggressiveness#NN    1    
		alter#VBP            1    
		altered#JJ           1    
		anatomic#JJ          1    
		anatomy#NN           1    
		androgen#NN          1    
		anterior#JJ          1    
		antigen#NN           1    
		anxiety#NN           1    
		apical#JJ            1    
		architecture#NN      1    
		arrival#NN           1    
		assisted#JJ          1    
		autopsy#NN           1    
		axial#JJ             1    
		barely#RB            1    
		benefit#VBP          1    
		benign#JJ            1    
		best#RB              1    
		biochemical#JJ       1    
		biological#JJ        1    
		biopsies#NNS         1    
		biopsy#NN            1    
		bore#VBN             1    
		breast#NN            1    
		bright#JJ            1    
		brown#JJ             1    
		bundle#NN            1    
		bundles#NNS          1    
		burden#NN            1    
		cancer#NN            1    
		cancers#NNS          1    
		capsule#NN           1    
		care#NN              1    
		cells#NNS            1    
		centers#NNS          1    
		challenges#NNS       1    
		circumstances#NNS    1    
		clinical#JJ          1    
		clues#NNS            1    
		cohort#NN            1    
		coil#NN              1    
		coils#NNS            1    
		colleagues#NNS       1    
		comorbidities#NNS    1    
		comprehensive#JJ     1    
		confer#VBP           1    
		confidence#NN        1    
		confirmatory#JJ      1    
		conflicting#JJ       1    
		contours#NNS         1    
		contraindications#NNS 1    
		controversies#NNS    1    
		controversy#NN       1    
		cores#NNS            1    
		cryotherapy#NN       1    
		curable#JJ           1    
		damage#NN            1    
		death#NN             1    
		deaths#NNS           1    
		debate#NN            1    
		debate#VB            1    
		defer#VB             1    
		definitive#JJ        1    
		delay#VBP            1    
		delineation#NN       1    
		deprivation#NN       1    
		determination#NN     1    
		diagnostic#JJ        1    
		diffusion#NN         1    
		diffusivity#NN       1    
		digital#JJ           1    
		disappointing#JJ     1    
		discomfort#NN        1    
		discontinuation#NN   1    
		discontinuous#JJ     1    
		disease#NN           1    
		diseases#NNS         1    
		distal#JJ            1    
		distant#JJ           1    
		distorted#JJ         1    
		dominant#JJ          1    
		dose#NN              1    
		early#RB             1    
		education#NN         1    
		efficacy#NN          1    
		electroporation#NN   1    
		elevated#JJ          1    
		eligible#JJ          1    
		embark#VBP           1    
		enable#VBP           1    
		enhancement#NN       1    
		equipment#NN         1    
		equivocal#JJ         1    
		erectile#JJ          1    
		evasive#JJ           1    
		evolve#VB            1    
		examination#NN       1    
		excision#NN          1    
		expectancy#NN        1    
		facile#JJ            1    
		focal#JJ             1    
		fossa#NN             1    
		frequency#NN         1    
		gadolinium#NN        1    
		gantry#NN            1    
		generous#JJ          1    
		glandular#JJ         1    
		grade#NN             1    
		granulation#NN       1    
		great#JJ             1    
		guide#VB             1    
		guidelines#NNS       1    
		hands#NNS            1    
		harbor#VBP           1    
		hazard#NN            1    
		healing#NN           1    
		health#NN            1    
		hemorrhage#NN        1    
		histopathology#NN    1    
		hopefully#RB         1    
		hotspot#NN           1    
		hyperplasia#NN       1    
		hypertension#NN      1    
		images#NNS           1    
		imaging#NN           1    
		imperfect#JJ         1    
		inappropriately#RB   1    
		inconvenience#NN     1    
		individual#NN        1    
		indolent#JJ          1    
		injection#NN         1    
		institution#NN       1    
		institutions#NNS     1    
		intellectual#JJ      1    
		intensity#NN         1    
		intravenous#JJ       1    
		invasion#NN          1    
		invasive#JJ          1    
		invasiveness#NN      1    
		invisible#JJ         1    
		involvement#NN       1    
		irreversible#JJ      1    
		kinetics#NNS         1    
		laser#NN             1    
		laterality#NN        1    
		lesion#NN            1    
		lesions#NNS          1    
		lethal#JJ            1    
		life#NN              1    
		localization#NN      1    
		localize#VB          1    
		longevity#NN         1    
		lymph#NN             1    
		magnet#NN            1    
		magnetic#JJ          1    
		malignancy#NN        1    
		mandatory#JJ         1    
		meanwhile#RB         1    
		metallic#JJ          1    
		metastatic#JJ        1    
		microscopic#JJ       1    
		midline#NN           1    
		modalities#NNS       1    
		monitor#NN           1    
		monitor#VB           1    
		month#NN             1    
		needle#NN            1    
		needles#NNS          1    
		nerve#NN             1    
		nests#NNS            1    
		nonmagnetic#JJ       1    
		nonspecific#JJ       1    
		older#JJR            1    
		oncologists#NNS      1    
		onset#NN             1    
		operate#VBP          1    
		overdiagnosis#NN     1    
		oversample#NN        1    
		overtreatment#NN     1    
		packed#JJ            1    
		paradox#NN           1    
		parenchyma#NN        1    
		partnership#NN       1    
		patent#NN            1    
		pathologic#JJ        1    
		pathology#NN         1    
		patient#JJ           1    
		patient#NN           1    
		patients#NNS         1    
		penda#NN             1    
		peripheral#JJ        1    
		physicians#NNS       1    
		pinto#NNP            1    
		policy#NN            1    
		populations#NNS      1    
		portend#VBP          1    
		posterior#JJ         1    
		postoperative#JJ     1    
		preclude#VB          1    
		predictive#JJ        1    
		predictor#NN         1    
		predominant#JJ       1    
		premium#NN           1    
		preparation#NN       1    
		professional#JJ      1    
		prognosis#NN         1    
		progress#VB          1    
		progressive#JJ       1    
		prone#JJ             1    
		prostate#NN          1    
		prostatectomy#NN     1    
		prostates#NNS        1    
		prostatic#JJ         1    
		prostatitis#NN       1    
		protracted#JJ        1    
		public#JJ            1    
		pulse#NN             1    
		race#NN              1    
		radiation#NN         1    
		radical#JJ           1    
		rectal#JJ            1    
		register#VBP         1    
		reliable#JJ          1    
		report#VBP           1    
		reporting#NN         1    
		residual#JJ          1    
		resonance#NN         1    
		robot#NN             1    
		robotic#JJ           1    
		safety#NN            1    
		salvage#NN           1    
		sampling#NN          1    
		satisfactory#JJ      1    
		scanner#NN           1    
		scanners#NNS         1    
		scar#NN              1    
		screening#NN         1    
		sepsis#NN            1    
		serum#NN             1    
		signal#JJ            1    
		site#NN              1    
		socioeconomic#JJ     1    
		specificity#NN       1    
		specimens#NNS        1    
		staging#NN           1    
		stand#VBP            1    
		standardized#JJ      1    
		standing#NN          1    
		stroke#NN            1    
		stroma#NN            1    
		substitute#VBP       1    
		suggest#VB           1    
		suggestive#JJ        1    
		suite#NN             1    
		surface#NN           1    
		surgery#NN           1    
		surgical#JJ          1    
		surprise#NN          1    
		surrogate#JJ         1    
		surveillance#NN      1    
		survival#NN          1    
		suspicion#NN         1    
		suspicious#JJ        1    
		symptoms#NNS         1    
		tailor#VBP           1    
		teach#VB             1    
		technologies#NNS     1    
		texture#NN           1    
		therapeutic#JJ       1    
		therapy#NN           1    
		thermometry#NN       1    
		tissue#NN            1    
		tomography#NN        1    
		treat#VBP            1    
		treated#JJ           1    
		truly#RB             1    
		tumor#NN             1    
		tumors#NNS           1    
		ultrasound#NN        1    
		uncomfortable#JJ     1    
		uncommon#JJ          1    
		undergo#VB           1    
		undergo#VBP          1    
		undetectable#JJ      1    
		unguided#JJ          1    
		upgrading#NN         1    
		urethra#NN           1    
		vesicle#NN           1    
		washout#NN           1    
		weeks#NNS            1    
		wives#NNS            1    
		wood#NN              1    
		year#NN              1    
		young#JJ             1    
		younger#JJR          1    
		zonal#JJ             1    
		zone#NN              1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 8
		Types: 8
		ADCs#NNS             1    
		TRUS#NN              1    
		TRUS#NNS             1    
		cryotherapy#NN       1    
		diffusivity#NN       1    
		electroporation#NN   1    
		oversample#NN        1    
		thermometry#NN       1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 147220/5426
Size of tokens/types in Md: 3439/1115
Size of diff: 141

NOT in Collection (FWL):   8
In Collection (FWL):       22
In Collection (FWL + POS): 111

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
ADCs                  |  NNS      |  -1       |  false   |  false         |  false  
TRUS                  |  NN       |  -1       |  false   |  false         |  false  
TRUS                  |  NNS      |  -1       |  false   |  false         |  false  
cryotherapy           |  NN       |  -1       |  false   |  false         |  false  
diffusivity           |  NN       |  -1       |  false   |  false         |  false  
electroporation       |  NN       |  -1       |  false   |  false         |  false  
oversample            |  NN       |  -1       |  false   |  false         |  false  
thermometry           |  NN       |  -1       |  false   |  false         |  false  

Biopsy                |  NNP      |  15859    |  false   |  false         |  true   
Brachytherapy         |  NNP      |  99850    |  false   |  false         |  true   
COIL                  |  NNP      |  18649    |  false   |  false         |  true   
Fusing                |  NNP      |  33588    |  false   |  false         |  true   
Instrumentation       |  NNP      |  16376    |  false   |  false         |  true   
Oncology              |  NNP      |  25817    |  false   |  false         |  true   
RADS                  |  NN       |  72869    |  false   |  false         |  true   
RADS                  |  NNPS     |  72869    |  false   |  false         |  true   
Radiology             |  NNP      |  35603    |  false   |  false         |  true   
Rectal                |  NNP      |  30846    |  false   |  false         |  true   
Salami                |  NNP      |  28925    |  false   |  false         |  true   
Transducer            |  NNP      |  28539    |  false   |  false         |  true   
Ultrasound            |  NNP      |  15881    |  false   |  false         |  true   
Urinary               |  NNP      |  20950    |  false   |  false         |  true   
Urologic              |  NNP      |  76557    |  false   |  false         |  true   
defer                 |  VB       |  17709    |  false   |  false         |  true   
kinetics              |  NNS      |  31047    |  false   |  false         |  true   
localize              |  VB       |  49223    |  false   |  false         |  true   
pinto                 |  NNP      |  27953    |  false   |  false         |  true   
preclude              |  VB       |  17048    |  false   |  false         |  true   
surrogate             |  JJ       |  13872    |  false   |  false         |  true   
upgrading             |  NN       |  16830    |  false   |  false         |  true   

Ablation              |  NN       |  44242    |  false   |  true          |  true   
Antigen               |  NN       |  29313    |  false   |  true          |  true   
Biochemical           |  JJ       |  20193    |  false   |  true          |  true   
Biopsies              |  NNS      |  34018    |  false   |  true          |  true   
Biopsy                |  NN       |  15859    |  false   |  true          |  true   
Caucasian             |  JJ       |  15181    |  false   |  true          |  true   
Clinician             |  NN       |  22615    |  false   |  true          |  true   
Clinicians            |  NNS      |  14391    |  false   |  true          |  true   
Lesions               |  NNS      |  14237    |  false   |  true          |  true   
Mapping               |  NN       |  11655    |  false   |  true          |  true   
Rectal                |  JJ       |  30846    |  false   |  true          |  true   
Recurrence            |  NN       |  14843    |  false   |  true          |  true   
Recurrent             |  JJ       |  14455    |  false   |  true          |  true   
Trackable             |  JJ       |  99194    |  false   |  true          |  true   
Ultrasound            |  NN       |  15881    |  false   |  true          |  true   
ablation              |  NN       |  44242    |  false   |  true          |  true   
actionable            |  JJ       |  39963    |  false   |  true          |  true   
adjuvant              |  JJ       |  43649    |  false   |  true          |  true   
aggressiveness        |  NN       |  22384    |  false   |  true          |  true   
anatomic              |  JJ       |  36718    |  false   |  true          |  true   
androgen              |  NN       |  59172    |  false   |  true          |  true   
anterior              |  JJ       |  15390    |  false   |  true          |  true   
antigen               |  NN       |  29313    |  false   |  true          |  true   
apical                |  JJ       |  55746    |  false   |  true          |  true   
assisted              |  JJ       |  18210    |  false   |  true          |  true   
axial                 |  JJ       |  23946    |  false   |  true          |  true   
biochemical           |  JJ       |  20193    |  false   |  true          |  true   
biopsies              |  NNS      |  34018    |  false   |  true          |  true   
biopsy                |  NN       |  15859    |  false   |  true          |  true   
capsule               |  NN       |  12318    |  false   |  true          |  true   
coil                  |  NN       |  18649    |  false   |  true          |  true   
coils                 |  NNS      |  19014    |  false   |  true          |  true   
comorbidities         |  NNS      |  49402    |  false   |  true          |  true   
confer                |  VBP      |  18431    |  false   |  true          |  true   
confirmatory          |  JJ       |  28424    |  false   |  true          |  true   
contours              |  NNS      |  14112    |  false   |  true          |  true   
contraindications     |  NNS      |  57315    |  false   |  true          |  true   
curable               |  JJ       |  35287    |  false   |  true          |  true   
delineation           |  NN       |  35857    |  false   |  true          |  true   
deprivation           |  NN       |  12216    |  false   |  true          |  true   
diffusion             |  NN       |  14432    |  false   |  true          |  true   
discontinuation       |  NN       |  54793    |  false   |  true          |  true   
distorted             |  JJ       |  15248    |  false   |  true          |  true   
embark                |  VBP      |  19133    |  false   |  true          |  true   
enhancement           |  NN       |  12027    |  false   |  true          |  true   
equivocal             |  JJ       |  30627    |  false   |  true          |  true   
erectile              |  JJ       |  33109    |  false   |  true          |  true   
evasive               |  JJ       |  23895    |  false   |  true          |  true   
excision              |  NN       |  23786    |  false   |  true          |  true   
facile                |  JJ       |  29823    |  false   |  true          |  true   
fossa                 |  NN       |  33449    |  false   |  true          |  true   
gadolinium            |  NN       |  66697    |  false   |  true          |  true   
gantry                |  NN       |  43140    |  false   |  true          |  true   
glandular             |  JJ       |  51985    |  false   |  true          |  true   
granulation           |  NN       |  38450    |  false   |  true          |  true   
hemorrhage            |  NN       |  25840    |  false   |  true          |  true   
histopathology        |  NN       |  57585    |  false   |  true          |  true   
hotspot               |  NN       |  44104    |  false   |  true          |  true   
hyperplasia           |  NN       |  43642    |  false   |  true          |  true   
hypertension          |  NN       |  12601    |  false   |  true          |  true   
inappropriately       |  RB       |  23264    |  false   |  true          |  true   
inconvenience         |  NN       |  16855    |  false   |  true          |  true   
indolent              |  JJ       |  43501    |  false   |  true          |  true   
intravenous           |  JJ       |  17862    |  false   |  true          |  true   
invasiveness          |  NN       |  61185    |  false   |  true          |  true   
irreversible          |  JJ       |  17817    |  false   |  true          |  true   
laterality            |  NN       |  78017    |  false   |  true          |  true   
lesion                |  NN       |  18106    |  false   |  true          |  true   
lesions               |  NNS      |  14237    |  false   |  true          |  true   
localization          |  NN       |  32149    |  false   |  true          |  true   
longevity             |  NN       |  11785    |  false   |  true          |  true   
lymph                 |  NN       |  18770    |  false   |  true          |  true   
malignancy            |  NN       |  31719    |  false   |  true          |  true   
metastatic            |  JJ       |  31137    |  false   |  true          |  true   
microscopic           |  JJ       |  13231    |  false   |  true          |  true   
midline               |  NN       |  41599    |  false   |  true          |  true   
modalities            |  NNS      |  21878    |  false   |  true          |  true   
oncologists           |  NNS      |  44021    |  false   |  true          |  true   
overdiagnosis         |  NN       |  86012    |  false   |  true          |  true   
overtreatment         |  NN       |  94805    |  false   |  true          |  true   
parenchyma            |  NN       |  86701    |  false   |  true          |  true   
pathologic            |  JJ       |  37470    |  false   |  true          |  true   
pathology             |  NN       |  14409    |  false   |  true          |  true   
portend               |  VBP      |  38588    |  false   |  true          |  true   
posterior             |  JJ       |  18543    |  false   |  true          |  true   
postoperative         |  JJ       |  15509    |  false   |  true          |  true   
predictive            |  JJ       |  12343    |  false   |  true          |  true   
predominant           |  JJ       |  15742    |  false   |  true          |  true   
prognosis             |  NN       |  15745    |  false   |  true          |  true   
prostatectomy         |  NN       |  51385    |  false   |  true          |  true   
prostatic             |  JJ       |  54862    |  false   |  true          |  true   
prostatitis           |  NN       |  62861    |  false   |  true          |  true   
protracted            |  JJ       |  16230    |  false   |  true          |  true   
rectal                |  JJ       |  30846    |  false   |  true          |  true   
robotic               |  JJ       |  13004    |  false   |  true          |  true   
salvage               |  NN       |  20498    |  false   |  true          |  true   
scanner               |  NN       |  14012    |  false   |  true          |  true   
scanners              |  NNS      |  19705    |  false   |  true          |  true   
sepsis                |  NN       |  42952    |  false   |  true          |  true   
serum                 |  NN       |  12000    |  false   |  true          |  true   
specificity           |  NN       |  14703    |  false   |  true          |  true   
staging               |  NN       |  14148    |  false   |  true          |  true   
stroma                |  NN       |  59432    |  false   |  true          |  true   
suggestive            |  JJ       |  14649    |  false   |  true          |  true   
tailor                |  VBP      |  23845    |  false   |  true          |  true   
tomography            |  NN       |  24797    |  false   |  true          |  true   
ultrasound            |  NN       |  15881    |  false   |  true          |  true   
undetectable          |  JJ       |  34149    |  false   |  true          |  true   
urethra               |  NN       |  44443    |  false   |  true          |  true   
vesicle               |  NN       |  94842    |  false   |  true          |  true   
washout               |  NN       |  50285    |  false   |  true          |  true   


Unique words (size: 127, lower case) without POS:
ablation
actionable
adcs
adjuvant
aggressiveness
anatomic
androgen
anterior
antigen
apical
assisted
axial
biochemical
biopsies
biopsy
brachytherapy
capsule
caucasian
clinician
clinicians
coil
coils
comorbidities
confer
confirmatory
contours
contraindications
cryotherapy
curable
defer
delineation
deprivation
diffusion
diffusivity
discontinuation
distorted
electroporation
embark
enhancement
equivocal
erectile
evasive
excision
facile
fossa
fusing
gadolinium
gantry
glandular
granulation
hemorrhage
histopathology
hotspot
hyperplasia
hypertension
inappropriately
inconvenience
indolent
instrumentation
intravenous
invasiveness
irreversible
kinetics
laterality
lesion
lesions
localization
localize
longevity
lymph
malignancy
mapping
metastatic
microscopic
midline
modalities
oncologists
oncology
overdiagnosis
oversample
overtreatment
parenchyma
pathologic
pathology
pinto
portend
posterior
postoperative
preclude
predictive
predominant
prognosis
prostatectomy
prostatic
prostatitis
protracted
radiology
rads
rectal
recurrence
recurrent
robotic
salami
salvage
scanner
scanners
sepsis
serum
specificity
staging
stroma
suggestive
surrogate
tailor
thermometry
tomography
trackable
transducer
trus
ultrasound
undetectable
upgrading
urethra
urinary
urologic
vesicle
washout

