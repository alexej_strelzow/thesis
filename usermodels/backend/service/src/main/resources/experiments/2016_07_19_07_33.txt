
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	NAMED_ENTITY
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_4
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 31
		Types: 29
		algorithmic#         1    
		align#               1    
		align#VBG            1    
		classifier#          1    
		consequent#JJ        1    
		curvature#           1    
		descriptor#          1    
		frontal#JJ           2    
		henceforth#          1    
		infer#               1    
		interpenetration#    1    
		localization#        1    
		normalization#       1    
		outlier#             1    
		outperform#VBZ       1    
		preexisting#         1    
		preprocessing#       2    
		rotate#VBD           1    
		rotate#VBN           1    
		satisfactorily#      1    
		segregate#           1    
		sift#                1    
		simulated#JJ         1    
		state-of-the-art#JJ  1    
		triangulate#VBN      1    
		triangulated#JJ      1    
		unregistered#JJ      1    
		y-axis#              1    
		z-axis#              1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	NOT_IN_COLLECTION_NO_NE
		Tokens: 1
		Types: 1
		diff#NN              1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 43510/4565
Size of tokens/types in Md: 1438/514
Size of diff: 32

NOT in Collection (FWL):   1
In Collection (FWL):       12
In Collection (FWL + POS): 19

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
diff                  |  NN       |  -1       |  false   |  false         |  false  

ALGORITHMIC           |  NNP      |  53685    |  false   |  false         |  true   
Curvature             |  NNP      |  27951    |  false   |  false         |  true   
Henceforth            |  NN       |  25562    |  false   |  false         |  true   
Interpenetration      |  NNP      |  59331    |  false   |  false         |  true   
Preprocessing         |  VBG      |  82734    |  false   |  false         |  true   
SIFT                  |  NN       |  18336    |  false   |  false         |  true   
Z-axis                |  NNP      |  99443    |  false   |  false         |  true   
align                 |  VB       |  15414    |  false   |  false         |  true   
infer                 |  VB       |  18405    |  false   |  false         |  true   
preexisting           |  VBG      |  20854    |  false   |  false         |  true   
preprocessing         |  VBG      |  82734    |  false   |  false         |  true   
segregate             |  VB       |  34970    |  false   |  false         |  true   

Frontal               |  JJ       |  14551    |  false   |  true          |  true   
Rotated               |  VBN      |  25515    |  false   |  true          |  true   
Simulated             |  JJ       |  15115    |  false   |  true          |  true   
Y-axis                |  NN       |  65169    |  false   |  true          |  true   
aligning              |  VBG      |  27020    |  false   |  true          |  true   
classifier            |  NN       |  85881    |  false   |  true          |  true   
consequent            |  JJ       |  20663    |  false   |  true          |  true   
descriptor            |  NN       |  43431    |  false   |  true          |  true   
frontal               |  JJ       |  14551    |  false   |  true          |  true   
localization          |  NN       |  32149    |  false   |  true          |  true   
normalization         |  NN       |  22758    |  false   |  true          |  true   
outliers              |  NNS      |  34873    |  false   |  true          |  true   
outperforms           |  VBZ      |  62584    |  false   |  true          |  true   
rotated               |  VBD      |  27894    |  false   |  true          |  true   
satisfactorily        |  RB       |  27011    |  false   |  true          |  true   
state-of-the-art      |  JJ       |  13071    |  false   |  true          |  true   
triangulated          |  JJ       |  70751    |  false   |  true          |  true   
triangulated          |  VBN      |  81561    |  false   |  true          |  true   
unregistered          |  JJ       |  38637    |  false   |  true          |  true   


Unique words (size: 28, lower case) without POS:
algorithmic
align
aligning
classifier
consequent
curvature
descriptor
diff
frontal
henceforth
infer
interpenetration
localization
normalization
outliers
outperforms
preexisting
preprocessing
rotated
satisfactorily
segregate
sift
simulated
state-of-the-art
triangulated
unregistered
y-axis
z-axis

