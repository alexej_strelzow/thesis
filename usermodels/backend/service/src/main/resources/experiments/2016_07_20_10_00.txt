
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	NAMED_ENTITY
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_4
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 44
		Types: 31
		dataset#             1    
		embedding#           1    
		estimation#          1    
		fictitious#JJ        1    
		hash#                3    
		his/her#             1    
		informative#JJ       1    
		lout#                1    
		metabolic#JJ         1    
		nested#JJ            1    
		null#                2    
		operand#             2    
		predicate#           1    
		prohibitive#JJ       1    
		prune#VBN            1    
		pruning#             1    
		query#               6    
		reachability#        1    
		redistribute#        1    
		relational#JJ        1    
		republish#           1    
		s/he#                1    
		selectivity#         2    
		tame#JJR             1    
		theorem#             1    
		traversal#           2    
		twig#                1    
		usefulness#          1    
		verification#        1    
		vertex#              3    
		well-studied#JJ      1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	NOT_IN_COLLECTION_NO_NE
		Tokens: 1
		Types: 1
		shortest-path#NN     1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 70358/4567
Size of tokens/types in Md: 2266/699
Size of diff: 45

NOT in Collection (FWL):   1
In Collection (FWL):       16
In Collection (FWL + POS): 28

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
shortest-path         |  NN       |  -1       |  false   |  false         |  false  

NULL                  |  NN       |  19000    |  false   |  false         |  true   
NULL                  |  NNP      |  19000    |  false   |  false         |  true   
QUERY                 |  NNP      |  18048    |  false   |  false         |  true   
Query                 |  NNP      |  18048    |  false   |  false         |  true   
Theorems              |  NNPS     |  48005    |  false   |  false         |  true   
Vertex                |  NNP      |  57064    |  false   |  false         |  true   
Vertices              |  NNPS     |  62477    |  false   |  false         |  true   
hash                  |  JJ       |  23658    |  false   |  false         |  true   
hash                  |  VB       |  23658    |  false   |  false         |  true   
his/her               |  NN       |  15856    |  false   |  false         |  true   
query                 |  VB       |  18048    |  false   |  false         |  true   
redistribute          |  VB       |  31921    |  false   |  false         |  true   
republish             |  VB       |  78225    |  false   |  false         |  true   
s/he                  |  NN       |  32364    |  false   |  false         |  true   
traversal             |  JJ       |  68860    |  false   |  false         |  true   
vertex                |  VB       |  57064    |  false   |  false         |  true   

Estimation            |  NN       |  13806    |  false   |  true          |  true   
Lout                  |  NN       |  47582    |  false   |  true          |  true   
Query                 |  NN       |  18048    |  false   |  true          |  true   
Selectivity           |  NN       |  31040    |  false   |  true          |  true   
datasets              |  NNS      |  54849    |  false   |  true          |  true   
embedding             |  NN       |  37457    |  false   |  true          |  true   
fictitious            |  JJ       |  21520    |  false   |  true          |  true   
hash                  |  NN       |  23658    |  false   |  true          |  true   
informative           |  JJ       |  14224    |  false   |  true          |  true   
metabolic             |  JJ       |  14460    |  false   |  true          |  true   
nested                |  JJ       |  34745    |  false   |  true          |  true   
operand               |  NN       |  64186    |  false   |  true          |  true   
operands              |  NNS      |  50545    |  false   |  true          |  true   
predicates            |  NNS      |  61170    |  false   |  true          |  true   
prohibitive           |  JJ       |  26167    |  false   |  true          |  true   
pruned                |  VBN      |  46606    |  false   |  true          |  true   
pruning               |  NN       |  20475    |  false   |  true          |  true   
queries               |  NNS      |  18734    |  false   |  true          |  true   
query                 |  NN       |  18048    |  false   |  true          |  true   
reachability          |  NN       |  84041    |  false   |  true          |  true   
relational            |  JJ       |  13835    |  false   |  true          |  true   
selectivity           |  NN       |  31040    |  false   |  true          |  true   
tamer                 |  JJR      |  47396    |  false   |  true          |  true   
traversal             |  NN       |  68860    |  false   |  true          |  true   
twig                  |  NN       |  21169    |  false   |  true          |  true   
usefulness            |  NN       |  13095    |  false   |  true          |  true   
verification          |  NN       |  14492    |  false   |  true          |  true   
well-studied          |  JJ       |  58446    |  false   |  true          |  true   


Unique words (size: 35, lower case) without POS:
datasets
embedding
estimation
fictitious
hash
his/her
informative
lout
metabolic
nested
null
operand
operands
predicates
prohibitive
pruned
pruning
queries
query
reachability
redistribute
relational
republish
s/he
selectivity
shortest-path
tamer
theorems
traversal
twig
usefulness
verification
vertex
vertices
well-studied

