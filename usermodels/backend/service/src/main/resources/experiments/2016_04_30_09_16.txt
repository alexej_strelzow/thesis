
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 44
		Types: 42
		anatomical#JJ        1    
		authentication#      1    
		authentication#NN    1    
		classifier#NN        1    
		classify#            1    
		cosmetics#NNS        1    
		disguise#            1    
		disguise#NNS         1    
		disguised#JJ         1    
		emeritus#            1    
		extraction#NN        1    
		fingerprint#NN       1    
		forgery#NN           1    
		frontal#JJ           1    
		geometrically#RB     1    
		ghostly#JJ           1    
		histogram#NN         1    
		illumination#NN      1    
		inference#NNS        1    
		interpolation#NN     1    
		modality#NNS         1    
		moustache#NN         1    
		neural#              1    
		neural#JJ            2    
		neuron#NNS           1    
		perceptual#JJ        1    
		pertinent#JJ         1    
		pixel#               1    
		pixel#NN             1    
		pixel#NNS            1    
		radial#              1    
		radial#JJ            2    
		regularization#NN    1    
		retina#NN            1    
		seamless#JJ          1    
		simulated#JJ         1    
		specificity#NN       1    
		synergism#NN         1    
		synergistic#JJ       1    
		undesired#JJ         1    
		unifying#JJ          1    
		weighting#NN         1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 5
		Types: 5
		autogenous#JJ        1    
		nonhierarchical#JJ   1    
		skinless#JJ          1    
		supervised#JJ        1    
		unsupervised#JJ      1    
	NOT_IN_USER_MODEL
		Tokens: 233
		Types: 233
		ACADEMY#NN           1    
		ACADEMY#NNP          1    
		Authentication#NNP   1    
		Back#RB              1    
		Basis#NN             1    
		Beyond#IN            1    
		CROP#NN              1    
		Classes#NNS          1    
		Classification#NN    1    
		Common#JJ            1    
		DISCUSSIONS#NNS      1    
		Development#NNP      1    
		EXPERIMENTAL#NNP     1    
		Effect#NN            1    
		Eigen#NN             1    
		Eigenfaces#NNS       1    
		Emeritus#NNP         1    
		Every#DT             1    
		Face#NNP             1    
		Fellow#NNP           1    
		Fusion#NN            1    
		Geometrically#RB     1    
		Government#NNP       1    
		Human#JJ             1    
		Human#NNP            1    
		INTRODUCTION#NNP     1    
		Image#NN             1    
		Images#NNPS          1    
		Images#NNS           1    
		Infrared#JJ          1    
		Momentum#NN          1    
		Neural#JJ            1    
		Neural#NNP           1    
		OVERVIEW#NNP         1    
		Object#NNP           1    
		Online#NNP           1    
		PUBLISHER#NN         1    
		PUBLISHER#NNP        1    
		Perception#NNP       1    
		Picture#NNP          1    
		Pixel#NNP            1    
		Radial#JJ            1    
		Radial#NNP           1    
		Recognition#NN       1    
		Relevant#JJ          1    
		Research#NN          1    
		Science#NNP          1    
		Spectrum#NN          1    
		Technique#NN         1    
		Thermal#JJ           1    
		Those#DT             1    
		Tracking#NNP         1    
		Visible#JJ           1    
		Visual#JJ            1    
		West#NNP             1    
		While#IN             1    
		acceptance#NN        1    
		affairs#NNS          1    
		alarms#NNS           1    
		ambiguities#NNS      1    
		ambiguous#JJ         1    
		anatomical#JJ        1    
		angle#NN             1    
		authentication#NN    1    
		automated#JJ         1    
		automatic#JJ         1    
		bankcard#NN          1    
		beard#VBD            1    
		black#JJ             1    
		blood#NN             1    
		body#NN              1    
		boundaries#NNS       1    
		camera#NN            1    
		card#NN              1    
		centers#NNS          1    
		classifier#NN        1    
		classify#VB          1    
		color#NN             1    
		combine#VBP          1    
		connection#NN        1    
		constitute#VBP       1    
		contrary#JJ          1    
		cosmetics#NNS        1    
		dark#JJ              1    
		darkness#NN          1    
		database#NN          1    
		detail#NN            1    
		disguise#VB          1    
		disguised#JJ         1    
		disguises#NNS        1    
		display#VB           1    
		eigenface#NN         1    
		eigenfaces#NNS       1    
		emit#VBP             1    
		engineering#NN       1    
		everyday#JJ          1    
		experimental#JJ      1    
		expert#NN            1    
		extraction#NN        1    
		eyes#NNS             1    
		facial#JJ            1    
		false#JJ             1    
		fields#NNS           1    
		fingerprint#NN       1    
		forgery#NN           1    
		frontal#JJ           1    
		fusion#NN            1    
		fuzzy#JJ             1    
		ghostly#JJ           1    
		glasses#NNS          1    
		hair#NN              1    
		head#NN              1    
		heat#NN              1    
		heterogeneous#JJ     1    
		hierarchical#JJ      1    
		histogram#NN         1    
		historical#JJ        1    
		human#JJ             1    
		humans#NNS           1    
		identification#NN    1    
		illuminating#JJ      1    
		illumination#NN      1    
		image#NN             1    
		imagery#NN           1    
		images#NNS           1    
		imaging#NN           1    
		imprecise#JJ         1    
		individual#NN        1    
		individuals#NNS      1    
		inferences#NNS       1    
		informative#JJ       1    
		infrared#JJ          1    
		integrate#VBP        1    
		intelligence#NN      1    
		internal#JJ          1    
		interpolation#NN     1    
		interpretation#NN    1    
		investigation#NN     1    
		lack#VBP             1    
		learning#NN          1    
		lighting#NN          1    
		location#NN          1    
		logical#JJ           1    
		machine#NN           1    
		mean#VBP             1    
		mechanism#NN         1    
		modalities#NNS       1    
		modules#NNS          1    
		monitoring#NN        1    
		moustache#NN         1    
		neural#JJ            1    
		neurons#NNS          1    
		normally#RB          1    
		object#NN            1    
		occur#VB             1    
		optical#JJ           1    
		organization#NN      1    
		outdoor#JJ           1    
		passive#NN           1    
		people#NNS           1    
		perception#NN        1    
		perceptual#JJ        1    
		person#NN            1    
		persons#NNS          1    
		pertinent#JJ         1    
		pixel#NN             1    
		pixels#NNS           1    
		play#VBP             1    
		pose#VB              1    
		pose#VBP             1    
		preferred#JJ         1    
		principal#JJ         1    
		produce#VBP          1    
		prone#JJ             1    
		quantitative#JJ      1    
		radial#JJ            1    
		reflect#VBP          1    
		regularization#NN    1    
		retina#NN            1    
		rise#NN              1    
		rotation#NN          1    
		scene#NN             1    
		scenes#NNS           1    
		scientific#JJ        1    
		seamless#JJ          1    
		select#VBP           1    
		senses#NNS           1    
		sensors#NNS          1    
		sensory#JJ           1    
		shots#NNS            1    
		simulated#JJ         1    
		skin#NN              1    
		skinned#JJ           1    
		solid#JJ             1    
		sort#NN              1    
		specificity#NN       1    
		stimulus#NN          1    
		subjects#NNS         1    
		success#NN           1    
		sunglasses#NNS       1    
		supervised#JJ        1    
		surface#NN           1    
		surfaces#NNS         1    
		surveillance#NN      1    
		synergism#NN         1    
		synergistic#JJ       1    
		technology#NN        1    
		temperature#NN       1    
		test#VBP             1    
		texture#NN           1    
		thankful#IN          1    
		thorough#JJ          1    
		tissue#NN            1    
		touch#RB             1    
		trained#JJ           1    
		transport#NN         1    
		trends#NNS           1    
		twins#NNS            1    
		uncontrolled#JJ      1    
		undesired#JJ         1    
		unifying#JJ          1    
		universal#JJ         1    
		unsupervised#JJ      1    
		upright#JJ           1    
		variability#NN       1    
		vein#NN              1    
		vessels#NNS          1    
		viable#JJ            1    
		visible#JJ           1    
		visual#JJ            1    
		warm#JJ              1    
		weighting#NN         1    
		wish#VBP             1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 3
		Types: 3
		Eigenfaces#NNS       1    
		eigenface#NN         1    
		eigenfaces#NNS       1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 1738/675
Size of diff: 52

NOT in Collection (FWL):   3
In Collection (FWL):       7
In Collection (FWL + POS): 42

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
Eigenfaces            |  NNS      |  -1       |  false   |  false         |  false  
eigenface             |  NN       |  -1       |  false   |  false         |  false  
eigenfaces            |  NNS      |  -1       |  false   |  false         |  false  

Authentication        |  NNP      |  35660    |  false   |  false         |  true   
Emeritus              |  NNP      |  14292    |  false   |  false         |  true   
Neural                |  NNP      |  13448    |  false   |  false         |  true   
Pixel                 |  NNP      |  22125    |  false   |  false         |  true   
Radial                |  NNP      |  22679    |  false   |  false         |  true   
classify              |  VB       |  16032    |  false   |  false         |  true   
disguise              |  VB       |  15953    |  false   |  false         |  true   

Geometrically         |  RB       |  46854    |  false   |  true          |  true   
Neural                |  JJ       |  13448    |  false   |  true          |  true   
Radial                |  JJ       |  22679    |  false   |  true          |  true   
anatomical            |  JJ       |  18978    |  false   |  true          |  true   
authentication        |  NN       |  35660    |  false   |  true          |  true   
classifier            |  NN       |  85881    |  false   |  true          |  true   
cosmetics             |  NNS      |  13504    |  false   |  true          |  true   
disguised             |  JJ       |  27597    |  false   |  true          |  true   
disguises             |  NNS      |  39397    |  false   |  true          |  true   
extraction            |  NN       |  14092    |  false   |  true          |  true   
fingerprint           |  NN       |  17758    |  false   |  true          |  true   
forgery               |  NN       |  28571    |  false   |  true          |  true   
frontal               |  JJ       |  14551    |  false   |  true          |  true   
ghostly               |  JJ       |  15325    |  false   |  true          |  true   
heterogeneous         |  JJ       |  17331    |  false   |  true          |  true   
hierarchical          |  JJ       |  10993    |  false   |  true          |  true   
histogram             |  NN       |  51052    |  false   |  true          |  true   
illumination          |  NN       |  15837    |  false   |  true          |  true   
inferences            |  NNS      |  19632    |  false   |  true          |  true   
interpolation         |  NN       |  50505    |  false   |  true          |  true   
modalities            |  NNS      |  21878    |  false   |  true          |  true   
moustache             |  NN       |  23936    |  false   |  true          |  true   
neural                |  JJ       |  13448    |  false   |  true          |  true   
neurons               |  NNS      |  14442    |  false   |  true          |  true   
perceptual            |  JJ       |  13710    |  false   |  true          |  true   
pertinent             |  JJ       |  13346    |  false   |  true          |  true   
pixel                 |  NN       |  22125    |  false   |  true          |  true   
pixels                |  NNS      |  18580    |  false   |  true          |  true   
radial                |  JJ       |  22679    |  false   |  true          |  true   
regularization        |  NN       |  66305    |  false   |  true          |  true   
retina                |  NN       |  23304    |  false   |  true          |  true   
seamless              |  JJ       |  16735    |  false   |  true          |  true   
simulated             |  JJ       |  15115    |  false   |  true          |  true   
skinned               |  JJ       |  36561    |  false   |  true          |  true   
specificity           |  NN       |  14703    |  false   |  true          |  true   
supervised            |  JJ       |  36443    |  false   |  true          |  true   
synergism             |  NN       |  76957    |  false   |  true          |  true   
synergistic           |  JJ       |  35177    |  false   |  true          |  true   
undesired             |  JJ       |  47608    |  false   |  true          |  true   
unifying              |  JJ       |  19059    |  false   |  true          |  true   
unsupervised          |  JJ       |  30155    |  false   |  true          |  true   
weighting             |  NN       |  28638    |  false   |  true          |  true   


Unique words (size: 45, lower case) without POS:
anatomical
authentication
classifier
classify
cosmetics
disguise
disguised
disguises
eigenface
eigenfaces
emeritus
extraction
fingerprint
forgery
frontal
geometrically
ghostly
heterogeneous
hierarchical
histogram
illumination
inferences
interpolation
modalities
moustache
neural
neurons
perceptual
pertinent
pixel
pixels
radial
regularization
retina
seamless
simulated
skinned
specificity
supervised
synergism
synergistic
undesired
unifying
unsupervised
weighting

