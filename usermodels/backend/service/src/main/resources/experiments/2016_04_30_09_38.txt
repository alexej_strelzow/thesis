
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 9
		Types: 8
		clique#NN            1    
		clone#NN             1    
		conformal#           1    
		heuristics#NNS       1    
		kernel#NN            1    
		parameter#           1    
		swap#NN              1    
		vertex#              2    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	NOT_IN_USER_MODEL
		Tokens: 68
		Types: 68
		Above#NNP            1    
		Completion#NNP       1    
		Does#NNP             1    
		Instance#NNP         1    
		Interval#NNP         1    
		Introduction#NN      1    
		Labs#NNPS            1    
		Observe#NNP          1    
		Parameter#NNP        1    
		Preliminary#JJ       1    
		Problems#NNS         1    
		Profile#NNP          1    
		Question#NN          1    
		Road#NNP             1    
		Royal#NNP            1    
		Rule#NN              1    
		Rules#NNP            1    
		Science#NN           1    
		Science#NNP          1    
		Several#JJ           1    
		South#NNP            1    
		Steps#NNS            1    
		Value#NNP            1    
		Vertex#NNP           1    
		archaeology#NN       1    
		areas#NNS            1    
		artificial#JJ        1    
		associate#VBP        1    
		bijection#NN         1    
		biology#NN           1    
		bridge#NN            1    
		bridges#NNS          1    
		clique#NN            1    
		clone#NN             1    
		concise#JJ           1    
		conformal#NN         1    
		connect#VBP          1    
		cycle#NN             1    
		deal#VB              1    
		department#NN        1    
		heuristics#NNS       1    
		hypothesis#NN        1    
		kaplan#NN            1    
		kernel#NN            1    
		least#RBS            1    
		maximal#JJ           1    
		miny#FW              1    
		minz#FW              1    
		neighbor#NN          1    
		neighborhood#NN      1    
		neighbors#NNS        1    
		ourselves#PRP        1    
		parametrization#NN   1    
		prfM#NN              1    
		profile#NN           1    
		profile#VB           1    
		recall#NN            1    
		repeat#VB            1    
		representations#NNS  1    
		rules#NNS            1    
		somewhat#RB          1    
		supergraph#NN        1    
		swap#NN              1    
		technical#JJ         1    
		transform#VB         1    
		vacuously#RB         1    
		vertex#NNP           1    
		wxxz#NN              1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 6
		Types: 6
		bijection#NN         1    
		miny#FW              1    
		minz#FW              1    
		parametrization#NN   1    
		supergraph#NN        1    
		vacuously#RB         1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 1484/370
Size of diff: 15

NOT in Collection (FWL):   6
In Collection (FWL):       4
In Collection (FWL + POS): 5

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
bijection             |  NN       |  -1       |  false   |  false         |  false  
miny                  |  FW       |  -1       |  false   |  false         |  false  
minz                  |  FW       |  -1       |  false   |  false         |  false  
parametrization       |  NN       |  -1       |  false   |  false         |  false  
supergraph            |  NN       |  -1       |  false   |  false         |  false  
vacuously             |  RB       |  -1       |  false   |  false         |  false  

Parameter             |  NNP      |  17585    |  false   |  false         |  true   
Vertex                |  NNP      |  57064    |  false   |  false         |  true   
conformal             |  NN       |  74746    |  false   |  false         |  true   
vertex                |  NNP      |  57064    |  false   |  false         |  true   

clique                |  NN       |  28952    |  false   |  true          |  true   
clone                 |  NN       |  17435    |  false   |  true          |  true   
heuristics            |  NNS      |  46799    |  false   |  true          |  true   
kernel                |  NN       |  23559    |  false   |  true          |  true   
swap                  |  NN       |  24146    |  false   |  true          |  true   


Unique words (size: 14, lower case) without POS:
bijection
clique
clone
conformal
heuristics
kernel
miny
minz
parameter
parametrization
supergraph
swap
vacuously
vertex

