
Removed words by operation:
	BABELNET
		Tokens: 0
		Types: 0
	LENGTH_GT_1_LT_3
		Tokens: 0
		Types: 0
	BLACKLIST
		Tokens: 0
		Types: 0
	FWL
		Tokens: 0
		Types: 0
	FREQ_EQ_1
		Tokens: 0
		Types: 0

Unknown words by operation:
	NOT_IN_COLLECTION
		Tokens: 0
		Types: 0
	LEMMA_RANK_GT_AVG_RANK
		Tokens: 103
		Types: 99
		abdomen#NN           1    
		abnormality#NNS      1    
		adenocarcinoma#NN    1    
		adjuvant#JJ          1    
		alkaline#            1    
		analogue#NN          1    
		anemia#              1    
		anemia#NN            1    
		antibody#NNS         1    
		anticancer#JJ        1    
		antitumor#           1    
		antitumor#JJ         1    
		apoptosis#NN         1    
		ascites#NNS          1    
		asterisk#NNS         1    
		attribution#         1    
		bile#NN              1    
		cattleman#           1    
		codon#NN             1    
		colorectal#JJ        2    
		commons#             1    
		constipation#NN      1    
		cutoff#NN            1    
		cytotoxic#JJ         1    
		diarrhea#            1    
		diarrhea#NN          1    
		disposition#NN       1    
		dosing#NN            1    
		duct#NN              1    
		electrocardiogram#NN 1    
		elicit#              1    
		endothelial#JJ       1    
		endpoint#NN          1    
		epidermal#JJ         1    
		escalation#NN        1    
		expectancy#NN        1    
		febrile#             2    
		frontline#NN         1    
		gastrointestinal#JJ  1    
		hematologic#         1    
		hematologic#JJ       1    
		hematology#NN        1    
		histologically#RB    1    
		hydrochloride#NN     1    
		incorporation#NN     1    
		inhibition#NN        1    
		inhibitor#NN         1    
		keyword#             1    
		lactating#           1    
		lesion#NN            1    
		lesion#NNS           1    
		malignancy#NN        1    
		marrow#NN            1    
		metastasis#NN        1    
		metastatic#JJ        2    
		methicillin#NN       1    
		modestly#RB          1    
		monoclonal#JJ        1    
		mutant#              1    
		mutation#NNS         1    
		nausea#NN            2    
		neutropenia#         1    
		neutropenia#NN       1    
		neutrophil#NN        1    
		nucleoside#NN        1    
		oncology#            1    
		palliative#JJ        1    
		pathologically#RB    1    
		phosphatase#NN       1    
		placebo#NN           1    
		preclinical#JJ       1    
		prophylactic#        1    
		prophylactic#JJ      1    
		radiotherapy#NN      1    
		receptor#NN          1    
		recourse#NN          1    
		refractory#JJ        1    
		regimen#NN           1    
		resumption#NN        1    
		septic#JJ            1    
		sequential#JJ        1    
		sequentially#RB      1    
		shrink#              1    
		shrinkage#NN         1    
		stabilization#NN     1    
		staphylococcus#      1    
		stenosis#NN          1    
		strand#NN            1    
		tablet#NNS           1    
		terminology#         1    
		thrombocytopenia#    1    
		thrombocytopenia#NN  1    
		tolerability#NN      1    
		toxicity#NN          1    
		triphosphate#NN      1    
		unrestricted#JJ      1    
		vomit#               1    
		vouch#VBP            1    
		waterfall#           1    
	WORD_RANK_GT_AVG_RANK
		Tokens: 0
		Types: 0
	ANTONYM_RANK_GT_AVG_RANK
		Tokens: 4
		Types: 4
		avascular#JJ         1    
		intolerable#JJ       1    
		uncoated#JJ          1    
		unmanageable#JJ      1    
	NOT_IN_USER_MODEL
		Tokens: 375
		Types: 375
		ARTICLE#NN           1    
		Accepted#NNP         1    
		Adverse#JJ           1    
		Agents#NNPS          1    
		American#NNP         1    
		Anemia#NNP           1    
		Attribution#NNP      1    
		August#NNP           1    
		Avenue#NNP           1    
		Blood#NN             1    
		Cancer#NN            1    
		Cancer#NNP           1    
		Cannon#NNP           1    
		Characteristic#JJ    1    
		China#NNP            1    
		City#NNP             1    
		Clinical#JJ          1    
		Colorectal#JJ        1    
		Common#NNP           1    
		Commons#NNPS         1    
		Compliance#NN        1    
		Conflict#NN          1    
		Constipation#NN      1    
		Creative#NNP         1    
		Criteria#NNP         1    
		DLTs#NNS             1    
		Declaration#NN       1    
		Diarrhea#NNP         1    
		Dictionary#NNP       1    
		Discussion#NNP       1    
		Dose#NN              1    
		Dose#NNP             1    
		Efficacy#NN          1    
		Efficacy#NNP         1    
		Elevated#JJ          1    
		Eligible#JJ          1    
		Expansion#NN         1    
		Exposure#NN          1    
		FOLFIRI#NN           1    
		Fatigue#NNP          1    
		Febrile#NN           1    
		Female#JJ            1    
		Gender#NN            1    
		Good#JJ              1    
		Grade#NNP            1    
		Growth#NN            1    
		Hematologic#NNP      1    
		Institute#NNP        1    
		Introduction#NN      1    
		Japanese#JJ          1    
		Japanese#NNPS        1    
		Keywords#NNP         1    
		License#NNP          1    
		Male#JJ              1    
		Male#NN              1    
		Median#JJ            1    
		Medical#JJ           1    
		Metastatic#JJ        1    
		Methods#NNS          1    
		Months#NNS           1    
		Mutations#NNS        1    
		National#NNP         1    
		Nausea#NN            1    
		Neutropenia#NNP      1    
		North#NNP            1    
		ORIGINAL#NNP         1    
		Observations#NNS     1    
		Oncology#NNP         1    
		Open#NNP             1    
		Other#JJ             1    
		Patient#NN           1    
		Patients#NNS         1    
		Pharmaceutical#NNP   1    
		Physician#NN         1    
		Plaza#NNP            1    
		Practice#NN          1    
		Prior#RB             1    
		Progression#NN       1    
		Purpose#NN           1    
		RECIST#NN            1    
		RECOURSE#NN          1    
		Race#NN              1    
		Regulatory#NNP       1    
		Risk#NN              1    
		Road#NNP             1    
		Role#NN              1    
		Safety#NN            1    
		Shrink#VB            1    
		Society#NNP          1    
		Staphylococcus#FW    1    
		Statistical#JJ       1    
		Study#NNP            1    
		Suite#NNP            1    
		Terminology#NNP      1    
		Three#CD             1    
		Thrombocytopenia#NNP 1    
		Treatment#NN         1    
		Tumor#NN             1    
		United#NNP           1    
		Vomiting#NNP         1    
		Waterfall#NNP        1    
		Western#JJ           1    
		Wild#NNP             1    
		abdomen#NN           1    
		abnormalities#NNS    1    
		activity#NN          1    
		acute#JJ             1    
		adenocarcinoma#NN    1    
		adequate#JJ          1    
		adjuvant#JJ          1    
		administration#NN    1    
		advanced#JJ          1    
		adverse#JJ           1    
		aged#JJ              1    
		aggressive#JJ        1    
		alive#JJ             1    
		alkaline#NN          1    
		analogue#NN          1    
		analyses#NNS         1    
		anemia#NN            1    
		antibodies#NNS       1    
		anticancer#JJ        1    
		antineoplastic#JJ    1    
		antitumor#JJ         1    
		antitumor#NN         1    
		apoptosis#NN         1    
		appetite#NN          1    
		approval#NN          1    
		ascites#NNS          1    
		assess#VB            1    
		assistance#NN        1    
		asterisks#NNS        1    
		aureus#FW            1    
		baseline#NN          1    
		bendall#NN           1    
		bile#NN              1    
		blind#JJ             1    
		board#NN             1    
		body#NN              1    
		bone#NN              1    
		brain#NN             1    
		breaks#NNS           1    
		breast#NN            1    
		cancell#NN           1    
		cancer#NN            1    
		cardiovascular#JJ    1    
		cattlemen#NN         1    
		cause#NN             1    
		centers#NNS          1    
		chemistry#NN         1    
		chemotherapies#NNS   1    
		chemotherapy#NN      1    
		chest#NN             1    
		clinical#JJ          1    
		coated#JJ            1    
		codon#NN             1    
		cohort#NN            1    
		colony#NN            1    
		colorectal#JJ        1    
		compliance#NN        1    
		concentrations#NNS   1    
		conduct#NN           1    
		confidence#NN        1    
		conflict#NN          1    
		consent#NN           1    
		controlled#JJ        1    
		count#NN             1    
		count#VB             1    
		credit#NN            1    
		cutoff#NN            1    
		cycle#NN             1    
		cycles#NNS           1    
		cytotoxic#JJ         1    
		dLTs#NNS             1    
		daily#JJ             1    
		daily#RB             1    
		days#NNS             1    
		death#NN             1    
		deaths#NNS           1    
		declare#VB           1    
		delays#NNS           1    
		demographics#NNS     1    
		diarrhea#NN          1    
		discretion#NN        1    
		disease#NN           1    
		disposition#NN       1    
		dose#NN              1    
		dosing#NN            1    
		draft#NN             1    
		drug#NN              1    
		duct#NN              1    
		efficacy#NN          1    
		electrocardiogram#NN 1    
		elicit#VB            1    
		eligible#JJ          1    
		employee#NN          1    
		endothelial#JJ       1    
		endpoint#NN          1    
		ents#NNS             1    
		epidermal#JJ         1    
		escalation#NN        1    
		ethical#JJ           1    
		examination#NN       1    
		expectancy#NN        1    
		fatigue#NN           1    
		febrile#JJ           1    
		febrile#NN           1    
		felt#VBD             1    
		female#NN            1    
		fifty#CD             1    
		film#NN              1    
		findings#NNS         1    
		fluorouracil#NN      1    
		formal#JJ            1    
		fourteen#CD          1    
		frontline#NN         1    
		future#JJ            1    
		gastrointestinal#JJ  1    
		grade#NN             1    
		granulocyte#NN       1    
		guidance#NN          1    
		guidelines#NNS       1    
		hematologic#JJ       1    
		hematology#NN        1    
		histologically#RB    1    
		hydrochloride#NN     1    
		hypothesis#NN        1    
		images#NNS           1    
		imaging#NN           1    
		incorporation#NN     1    
		infection#NN         1    
		inhibition#NN        1    
		inhibitor#NN         1    
		initiation#NN        1    
		institution#NN       1    
		institutional#JJ     1    
		instrumental#JJ      1    
		investigator#NN      1    
		irinotecan#NN        1    
		lactating#NN         1    
		lesion#NN            1    
		lesions#NNS          1    
		lesser#JJR           1    
		leucovorin#NN        1    
		license#NN           1    
		life#NN              1    
		maintenance#NN       1    
		malignancy#NN        1    
		manageable#JJ        1    
		manuscript#NN        1    
		markers#NNS          1    
		marrow#NN            1    
		mechanism#NN         1    
		median#JJ            1    
		median#NN            1    
		medical#JJ           1    
		medication#NN        1    
		medications#NNS      1    
		meet#VB              1    
		metastasis#NN        1    
		metastatic#JJ        1    
		methicillin#NN       1    
		modest#JJ            1    
		modestly#RB          1    
		molar#JJ             1    
		monoclonal#JJ        1    
		months#NNS           1    
		morning#NN           1    
		mutant#JJ            1    
		national#JJ          1    
		nausea#NN            1    
		neutropenia#NN       1    
		neutrophil#NN        1    
		nine#CD              1    
		nucleoside#NN        1    
		online#JJ            1    
		oral#JJ              1    
		organ#NN             1    
		oxaliplatin#NN       1    
		palliative#JJ        1    
		patel#NN             1    
		pathologically#RB    1    
		patient#JJ           1    
		patient#NN           1    
		patients#NNS         1    
		percent#NN           1    
		personnel#NNS        1    
		phosphatase#NN       1    
		phosphorylase#NN     1    
		placebo#NN           1    
		planned#JJ           1    
		population#NN        1    
		populations#NNS      1    
		post#NN              1    
		potent#JJ            1    
		preclinical#JJ       1    
		pregnant#JJ          1    
		principles#NNS       1    
		produce#VBP          1    
		profile#NN           1    
		progressive#JJ       1    
		prophylactic#JJ      1    
		prophylactic#NN      1    
		proportion#NN        1    
		publication#NN       1    
		radiotherapy#NN      1    
		range#VBP            1    
		receptor#NN          1    
		reductions#NNS       1    
		refractory#JJ        1    
		regimen#NN           1    
		regorafenib#NN       1    
		regulatory#JJ        1    
		release#NN           1    
		reproduction#NN      1    
		resistant#JJ         1    
		resumption#NN        1    
		rosen#NN             1    
		safety#NN            1    
		schedule#NN          1    
		secondary#JJ         1    
		septic#JJ            1    
		sequential#JJ        1    
		sequentially#RB      1    
		serious#JJ           1    
		shock#NN             1    
		shrinkage#NN         1    
		signs#NNS            1    
		site#NN              1    
		sixty#CD             1    
		solid#JJ             1    
		sponsor#NN           1    
		stabilization#NN     1    
		status#NN            1    
		stenosis#NN          1    
		strand#NN            1    
		submit#VBP           1    
		surface#NN           1    
		surgery#NN           1    
		survival#NN          1    
		sustained#JJ         1    
		systemic#JJ          1    
		tablets#NNS          1    
		temperature#NN       1    
		thank#VBP            1    
		thanks#NNS           1    
		therapy#NN           1    
		thrombocytopenia#NN  1    
		thymidine#NN         1    
		tolerability#NN      1    
		tolerable#JJ         1    
		toxicity#NN          1    
		treated#JJ           1    
		treatment#NN         1    
		trial#NN             1    
		trials#NNS           1    
		trifluridine#NN      1    
		triphosphate#NN      1    
		tumor#NN             1    
		tumors#NNS           1    
		unacceptable#JJ      1    
		uncontrolled#JJ      1    
		unrelated#JJ         1    
		unresponsive#JJ      1    
		unrestricted#JJ      1    
		vascular#JJ          1    
		visit#NN             1    
		vital#JJ             1    
		vouch#VBP            1    
		week#NN              1    
		weeks#NNS            1    
		wild#JJ              1    
		women#NNS            1    
		writer#NN            1    
		xenograft#NN         1    
	NOT_IN_COLLECTION_NO_NE
		Tokens: 16
		Types: 16
		DLTs#NNS             1    
		antineoplastic#JJ    1    
		aureus#FW            1    
		bendall#NN           1    
		chemotherapies#NNS   1    
		dLTs#NNS             1    
		fluorouracil#NN      1    
		granulocyte#NN       1    
		irinotecan#NN        1    
		leucovorin#NN        1    
		oxaliplatin#NN       1    
		phosphorylase#NN     1    
		regorafenib#NN       1    
		thymidine#NN         1    
		trifluridine#NN      1    
		xenograft#NN         1    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Diff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Size of tokens/types in Mu: 58485/3464
Size of tokens/types in Md: 1770/638
Size of diff: 123

NOT in Collection (FWL):   16
In Collection (FWL):       23
In Collection (FWL + POS): 84

Word                  |  POS      |  Rank     |  in Mu    |  in Mc (POS)   |  in Mc  
-------------------------------------------------------------------------------------------------------
DLTs                  |  NNS      |  -1       |  false   |  false         |  false  
antineoplastic        |  JJ       |  -1       |  false   |  false         |  false  
aureus                |  FW       |  -1       |  false   |  false         |  false  
bendall               |  NN       |  -1       |  false   |  false         |  false  
chemotherapies        |  NNS      |  -1       |  false   |  false         |  false  
dLTs                  |  NNS      |  -1       |  false   |  false         |  false  
fluorouracil          |  NN       |  -1       |  false   |  false         |  false  
granulocyte           |  NN       |  -1       |  false   |  false         |  false  
irinotecan            |  NN       |  -1       |  false   |  false         |  false  
leucovorin            |  NN       |  -1       |  false   |  false         |  false  
oxaliplatin           |  NN       |  -1       |  false   |  false         |  false  
phosphorylase         |  NN       |  -1       |  false   |  false         |  false  
regorafenib           |  NN       |  -1       |  false   |  false         |  false  
thymidine             |  NN       |  -1       |  false   |  false         |  false  
trifluridine          |  NN       |  -1       |  false   |  false         |  false  
xenograft             |  NN       |  -1       |  false   |  false         |  false  

Anemia                |  NNP      |  20057    |  false   |  false         |  true   
Attribution           |  NNP      |  18093    |  false   |  false         |  true   
Commons               |  NNPS     |  18807    |  false   |  false         |  true   
Diarrhea              |  NNP      |  14425    |  false   |  false         |  true   
Febrile               |  NN       |  44240    |  false   |  false         |  true   
Hematologic           |  NNP      |  72281    |  false   |  false         |  true   
Keywords              |  NNP      |  17233    |  false   |  false         |  true   
Neutropenia           |  NNP      |  88293    |  false   |  false         |  true   
Oncology              |  NNP      |  25817    |  false   |  false         |  true   
Shrink                |  VB       |  11086    |  false   |  false         |  true   
Staphylococcus        |  FW       |  37884    |  false   |  false         |  true   
Terminology           |  NNP      |  12433    |  false   |  false         |  true   
Thrombocytopenia      |  NNP      |  76659    |  false   |  false         |  true   
Vomiting              |  NNP      |  15258    |  false   |  false         |  true   
Waterfall             |  NNP      |  14219    |  false   |  false         |  true   
alkaline              |  NN       |  28418    |  false   |  false         |  true   
antitumor             |  NN       |  89175    |  false   |  false         |  true   
cattlemen             |  NN       |  41682    |  false   |  false         |  true   
elicit                |  VB       |  14230    |  false   |  false         |  true   
febrile               |  NN       |  44240    |  false   |  false         |  true   
lactating             |  NN       |  66214    |  false   |  false         |  true   
mutant                |  JJ       |  20917    |  false   |  false         |  true   
prophylactic          |  NN       |  35553    |  false   |  false         |  true   

Colorectal            |  JJ       |  32240    |  false   |  true          |  true   
Constipation          |  NN       |  29308    |  false   |  true          |  true   
Metastatic            |  JJ       |  31137    |  false   |  true          |  true   
Mutations             |  NNS      |  17271    |  false   |  true          |  true   
Nausea                |  NN       |  12487    |  false   |  true          |  true   
RECOURSE              |  NN       |  16070    |  false   |  true          |  true   
abdomen               |  NN       |  13322    |  false   |  true          |  true   
abnormalities         |  NNS      |  18840    |  false   |  true          |  true   
adenocarcinoma        |  NN       |  48854    |  false   |  true          |  true   
adjuvant              |  JJ       |  43649    |  false   |  true          |  true   
analogue              |  NN       |  34348    |  false   |  true          |  true   
anemia                |  NN       |  20057    |  false   |  true          |  true   
antibodies            |  NNS      |  15191    |  false   |  true          |  true   
anticancer            |  JJ       |  44561    |  false   |  true          |  true   
antitumor             |  JJ       |  89175    |  false   |  true          |  true   
apoptosis             |  NN       |  49406    |  false   |  true          |  true   
ascites               |  NNS      |  68423    |  false   |  true          |  true   
asterisks             |  NNS      |  48505    |  false   |  true          |  true   
bile                  |  NN       |  22818    |  false   |  true          |  true   
coated                |  JJ       |  39715    |  false   |  true          |  true   
codon                 |  NN       |  50723    |  false   |  true          |  true   
colorectal            |  JJ       |  32240    |  false   |  true          |  true   
cutoff                |  NN       |  16052    |  false   |  true          |  true   
cytotoxic             |  JJ       |  65163    |  false   |  true          |  true   
diarrhea              |  NN       |  14425    |  false   |  true          |  true   
disposition           |  NN       |  11661    |  false   |  true          |  true   
dosing                |  NN       |  33100    |  false   |  true          |  true   
duct                  |  NN       |  14362    |  false   |  true          |  true   
electrocardiogram     |  NN       |  48641    |  false   |  true          |  true   
endothelial           |  JJ       |  34885    |  false   |  true          |  true   
endpoint              |  NN       |  36976    |  false   |  true          |  true   
epidermal             |  JJ       |  47502    |  false   |  true          |  true   
escalation            |  NN       |  17478    |  false   |  true          |  true   
expectancy            |  NN       |  11225    |  false   |  true          |  true   
frontline             |  NN       |  18705    |  false   |  true          |  true   
gastrointestinal      |  JJ       |  19555    |  false   |  true          |  true   
hematologic           |  JJ       |  72281    |  false   |  true          |  true   
hematology            |  NN       |  72693    |  false   |  true          |  true   
histologically        |  RB       |  50521    |  false   |  true          |  true   
hydrochloride         |  NN       |  62746    |  false   |  true          |  true   
incorporation         |  NN       |  14869    |  false   |  true          |  true   
inhibition            |  NN       |  21438    |  false   |  true          |  true   
inhibitor             |  NN       |  30048    |  false   |  true          |  true   
lesion                |  NN       |  18106    |  false   |  true          |  true   
lesions               |  NNS      |  14237    |  false   |  true          |  true   
malignancy            |  NN       |  31719    |  false   |  true          |  true   
manageable            |  JJ       |  14464    |  false   |  true          |  true   
marrow                |  NN       |  13218    |  false   |  true          |  true   
metastasis            |  NN       |  29815    |  false   |  true          |  true   
metastatic            |  JJ       |  31137    |  false   |  true          |  true   
methicillin           |  NN       |  84792    |  false   |  true          |  true   
modestly              |  RB       |  16178    |  false   |  true          |  true   
monoclonal            |  JJ       |  37357    |  false   |  true          |  true   
nausea                |  NN       |  12487    |  false   |  true          |  true   
neutropenia           |  NN       |  88293    |  false   |  true          |  true   
neutrophil            |  NN       |  76674    |  false   |  true          |  true   
nucleoside            |  NN       |  88258    |  false   |  true          |  true   
palliative            |  JJ       |  25917    |  false   |  true          |  true   
pathologically        |  RB       |  51415    |  false   |  true          |  true   
phosphatase           |  NN       |  69332    |  false   |  true          |  true   
placebo               |  NN       |  12654    |  false   |  true          |  true   
preclinical           |  JJ       |  66654    |  false   |  true          |  true   
prophylactic          |  JJ       |  35553    |  false   |  true          |  true   
radiotherapy          |  NN       |  26805    |  false   |  true          |  true   
receptor              |  NN       |  20653    |  false   |  true          |  true   
refractory            |  JJ       |  40873    |  false   |  true          |  true   
regimen               |  NN       |  11880    |  false   |  true          |  true   
resumption            |  NN       |  27396    |  false   |  true          |  true   
septic                |  JJ       |  19088    |  false   |  true          |  true   
sequential            |  JJ       |  15077    |  false   |  true          |  true   
sequentially          |  RB       |  29428    |  false   |  true          |  true   
shrinkage             |  NN       |  31661    |  false   |  true          |  true   
stabilization         |  NN       |  14009    |  false   |  true          |  true   
stenosis              |  NN       |  37356    |  false   |  true          |  true   
strand                |  NN       |  12169    |  false   |  true          |  true   
tablets               |  NNS      |  13989    |  false   |  true          |  true   
thrombocytopenia      |  NN       |  76659    |  false   |  true          |  true   
tolerability          |  NN       |  76608    |  false   |  true          |  true   
tolerable             |  JJ       |  22735    |  false   |  true          |  true   
toxicity              |  NN       |  17251    |  false   |  true          |  true   
triphosphate          |  NN       |  74450    |  false   |  true          |  true   
unrestricted          |  JJ       |  18851    |  false   |  true          |  true   
vascular              |  JJ       |  15468    |  false   |  true          |  true   
vouch                 |  VBP      |  31839    |  false   |  true          |  true   


Unique words (size: 111, lower case) without POS:
abdomen
abnormalities
adenocarcinoma
adjuvant
alkaline
analogue
anemia
antibodies
anticancer
antineoplastic
antitumor
apoptosis
ascites
asterisks
attribution
aureus
bendall
bile
cattlemen
chemotherapies
coated
codon
colorectal
commons
constipation
cutoff
cytotoxic
diarrhea
disposition
dlts
dosing
duct
electrocardiogram
elicit
endothelial
endpoint
epidermal
escalation
expectancy
febrile
fluorouracil
frontline
gastrointestinal
granulocyte
hematologic
hematology
histologically
hydrochloride
incorporation
inhibition
inhibitor
irinotecan
keywords
lactating
lesion
lesions
leucovorin
malignancy
manageable
marrow
metastasis
metastatic
methicillin
modestly
monoclonal
mutant
mutations
nausea
neutropenia
neutrophil
nucleoside
oncology
oxaliplatin
palliative
pathologically
phosphatase
phosphorylase
placebo
preclinical
prophylactic
radiotherapy
receptor
recourse
refractory
regimen
regorafenib
resumption
septic
sequential
sequentially
shrink
shrinkage
stabilization
staphylococcus
stenosis
strand
tablets
terminology
thrombocytopenia
thymidine
tolerability
tolerable
toxicity
trifluridine
triphosphate
unrestricted
vascular
vomiting
vouch
waterfall
xenograft

